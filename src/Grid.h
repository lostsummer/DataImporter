#ifndef GRID_H
#define GRID_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <set>

#include "RWLock.h"

//////////////////////////////////////////////////////////////////////
class CGoods;
class CGridFilter;
class CGrid
{ 
public:
	CGrid(char cGroup, CString& strGroup, short sSortID, char cSort, BOOL GZQHFilter);
	~CGrid();

	void Refresh();
	short Copy(CPtrArray& aGoods, short sOffset, short sNum, short& sTotal);
	short Copy(std::set<DWORD>& aGoods, short sOffset, short sNum, short& sTotal);
	DWORD GetLTGoodsID(); // 取龙头股ID,只有按10日涨幅排的GRID才有龙头股，其余返回0 [1/29/2013 frantqian]
	void  GetSortGoodsID(std::vector<DWORD>& vecGoodsID);
	void  GetSortGoodsIDBackWard(std::vector<DWORD>& vecGoodsID);
	short CopyBackWard(CPtrArray& aGoods, short sOffset, short sNum, short& sTotal);//取列表倒叙sOffset只开始算sNum只股票
	short CopyBackWard(std::set<DWORD>& aGoods, short sOffset, short sNum, short& sTotal);
private:
	inline bool CheckFilter(CGoods* pGoods, CGridFilter *pFilter);
public:
	char m_cGroup;
	CString m_strGroup;
	short m_sSortID;
	char m_cSort;
	BOOL m_bQZQHFilter;

private:
	RWLock m_wrGoods;
	CPtrArray m_aGoods;

	volatile DWORD m_dwRefreshSecond;
	volatile DWORD m_dwValueTime;
};

//////////////////////////////////////////////////////////////////////

class CGridAll
{
public:
	CGridAll();	
	~CGridAll();

	CGrid* GetGrid(char cGroup, CString& strGroup, short sSortID, char cSort, BOOL GZQHFilter);

private:
	void DeleteGrid();

public:
	DWORD m_dwRefreshInterval;

	RWLock m_wrGrid;
	CPtrArray m_aGrid;
};

extern CGridAll g_grid;

class CGridFilter
{
public:
	virtual bool PassFilter(CGoods *pGoods){return TRUE;};
};

class CGridGZQHFilter:public CGridFilter
{
public:
	CGridGZQHFilter(BOOL enable);
	virtual bool PassFilter(CGoods *pGoods);
private:
	BOOL m_bEnableFilter;	
};

#endif


