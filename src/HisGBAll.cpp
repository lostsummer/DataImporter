// GoodsJBM.cpp: implementation of the CGoodsJBM class.

#include "MDS.h"
#include "HisGBAll.h"
//////////////////////////////////////////////////////////////////////




BOOL operator > (CHisGB& first, CHisGB& second)
{
	return first.m_dwDate > second.m_dwDate;
}
BOOL operator == (CHisGB& first, CHisGB& second)
{
	return first.m_dwDate == second.m_dwDate;
}
BOOL operator < (CHisGB& first, CHisGB& second)
{
	return first.m_dwDate < second.m_dwDate;
}



CGoodsHisGB::CGoodsHisGB()
{
	m_dwGoodsID = 0;
}

CGoodsHisGB::CGoodsHisGB(const CGoodsHisGB& ref)
{
	m_dwGoodsID = ref.m_dwGoodsID;
	m_aHisGB.Copy(ref.m_aHisGB);
}

CGoodsHisGB::~CGoodsHisGB()
{
}

CGoodsHisGB& CGoodsHisGB::operator=(const CGoodsHisGB& another)
{
	m_dwGoodsID = another.m_dwGoodsID;
	m_aHisGB.SetSize(0);
	m_aHisGB.Copy(another.m_aHisGB);
	return *this;
}



BOOL operator > (CGoodsHisGB& first, CGoodsHisGB& second)
{
	return first.m_dwGoodsID > second.m_dwGoodsID;
}
BOOL operator == (CGoodsHisGB& first, CGoodsHisGB& second)
{
	return first.m_dwGoodsID == second.m_dwGoodsID;
}

BOOL operator < (CGoodsHisGB& first, CGoodsHisGB& second)
{
	return first.m_dwGoodsID < second.m_dwGoodsID;
}


//////////////////////////////////////////////////////////////////////

CHisGBAll::CHisGBAll()
{
	m_dwDate = m_dwTime = 0;
	m_aGoodsHisGB.SetSize(0,4000);
}

CHisGBAll::~CHisGBAll()
{
	m_aGoodsHisGB.RemoveAll();
}

void CHisGBAll::Read()
{
	//CString strFile = theApp.m_strSharePath+"L2Data\\HisGB.dat";
	CString strFile = theApp.m_strSharePath+"Data/hisgb.dat";

	CBuffer buffer;
	buffer.m_bSingleRead = true;
	buffer.Initialize(4096, true);

	buffer.FileRead(strFile);

	try
	{
		short sVer;
		buffer.Read(&sVer, 2);

		buffer.Read(&m_dwDate, 4);
		buffer.Read(&m_dwTime, 4);

		int nSize;
		buffer.Read(&nSize, 4);

		CGoodsHisGB ghgb;
		CHisGB hgb;
		for (int i=0; i<nSize; i++)
		{
			DWORD dwGoodsID = buffer.ReadInt();
			if( dwGoodsID == 603288 ){
				LOG_ERR("HTWY");
			}
			if (ghgb.m_dwGoodsID != dwGoodsID)
			{

				if (ghgb.m_dwGoodsID != 0)
					m_aGoodsHisGB.Add(ghgb);

				ghgb.m_dwGoodsID = dwGoodsID;
				ghgb.m_aHisGB.RemoveAll();
			}
			hgb.m_dwDate = buffer.ReadInt();
			hgb.m_xLTG = buffer.ReadXInt();
			ghgb.m_aHisGB.Add(hgb);
		}
		if (ghgb.m_dwGoodsID != 0)
			m_aGoodsHisGB.Add(ghgb);
	}
	catch(int)
	{
	}
}

void CHisGBAll::Get( DWORD dwGoodsID, CGoodsHisGB& goodsHisGB)
{
	goodsHisGB.m_dwGoodsID = dwGoodsID;
	int nPos = m_aGoodsHisGB.Find(goodsHisGB);
	if (nPos >= 0)
		goodsHisGB = m_aGoodsHisGB[nPos];
	else
		goodsHisGB.m_aHisGB.SetSize(0);
}


