// QMatrixCompress.h: interface for the CQMatrixCompress class.
//
//////////////////////////////////////////////////////////////////////
#ifndef QMATRIXCOMPRESS_H
#define QMATRIXCOMPRESS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include "Goods.h"
#include "BitStream.h"

class CQMatrixCompress  
{
public:
	CQMatrixCompress();
	virtual ~CQMatrixCompress();

	DWORD	m_dwLastTimeBuy;
	DWORD	m_dwLastTimeSell;

	DWORD	m_dwMinPriceBuy;
	DWORD	m_dwMaxPriceSell;

	DWORD	m_dwPriceBuy1;
	DWORD	m_dwPriceSell1;

	static int ExpandMatrix(CGoods* pGoods, CBitStream& stream, WORD wVersion);

	int CompressQueueMatrix(CQueueMatrix& qMatrix, BOOL bActiveOnly, CBitStream& stream);
	static int ExpandQueueMatrix(CQueueMatrix& qMatrix, CBitStream& stream);

	static int CompressStatCounts(CStatCounts& sc, CBitStream& stream);
	static int ExpandStatCounts(CStatCounts& sc, CBitStream& stream);

	static int CompressStatCountsDiff(CStatCounts& scNew, CStatCounts& scOld, CBitStream& stream);
	static int ExpandStatCountsDiff(CStatCounts& sc, CBitStream& stream);

private:
	int CompressHisQ(CHisOrderQueue& hisQ, BOOL bBuySell1, DWORD dwLastTime, CBitStream& stream);
	static int ExpandHisQ(CHisOrderQueue& hisQ, DWORD dwLastTime, CBitStream& stream);

	int CompressBuySell1(CHisOrderQueue& hisQ, CBitStream& stream);
	static int ExpandBuySell1(CHisOrderQueue& hisQ, CBitStream& stream);
	static int ClearHisQBuySell1(CHisOrderQueue& hisQ);

	static void CompressQTime(DWORD dwTime, DWORD dwLastTime, CBitStream& stream);
	static DWORD ExpandQTime(DWORD dwLastTime, CBitStream& stream);

	static int ExpandHisOrder(CHisOrderQueueSArray& aHisOrder, CBitStream& stream, DWORD dwTimeOld, DWORD dwTime, DWORD PRICE_DIVIDE, BOOL bBuy, WORD wVersion);
	static int ExpandHisOrder(CHisOrderQueue& ho, CBitStream& stream, DWORD dwTimeOld, DWORD dwTime, WORD wVersion);
};

#endif


