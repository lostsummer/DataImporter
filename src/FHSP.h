#ifndef FHSP_H
#define FHSP_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "Buffer.h"
#include "SortArray.h"
#include "RWLock.h"

//////////////////////////////////////////////////////////////////////

class CFHSP
{
public:
	CFHSP();

	void Read(CBuffer&, short);
	void Write(CBuffer&, short);

	void DoPriceCQ(char, double&, double&);// char为除权方式，2,为不除权，1为上除权，0为下除权 [11/12/2012 frantqian]

public:
	DWORD m_dwGoodsID;		//股票ID
	DWORD m_dwDate;			//除权日
	
	INT64 m_hHL;			//每10股红利（*1000000）
	INT64 m_hSG;			//每10股送股（*1000000）
	INT64 m_hZZ;			//每10股转增（*1000000）
	INT64 m_hPG;			//每10股配股（*1000000）
	DWORD m_dwPGJ;			//每10股配股价（*1000）

	DWORD m_dwUpdateDate;	//更新日期 yyyymmdd
	DWORD m_dwUpdateTime;	//更新时间 hhmmss
};

BOOL operator > (const CFHSP&, const CFHSP&);
BOOL operator == (const CFHSP&, const CFHSP&);
BOOL operator < (const CFHSP&, const CFHSP&);

//////////////////////////////////////////////////////////////////////

class CGoodsFHSP
{
public:
	CGoodsFHSP();
	CGoodsFHSP(const CGoodsFHSP&);
	~CGoodsFHSP();

	const CGoodsFHSP& operator=(const CGoodsFHSP&);

public:
	DWORD m_dwGoodsID;
	CSortArray<CFHSP> m_aFHSP;
};

BOOL operator > (const CGoodsFHSP&, const CGoodsFHSP&);
BOOL operator == (const CGoodsFHSP&, const CGoodsFHSP&);
BOOL operator < (const CGoodsFHSP&, const CGoodsFHSP&);

//////////////////////////////////////////////////////////////////////

class CFHSPAll
{
public:
	CFHSPAll();
	~CFHSPAll();

	bool Read();

	void AddFHSP(const CFHSP& fhsp);

	void Get(DWORD dwGoodsID, CSortArray<CFHSP>& aFHSP);
	void Get(DWORD, DWORD, CSortArray<CFHSP>&);

public:
	RWLock m_wrFHSP;

	CSortArray<CFHSP> m_aFHSP;
	CSortArray<CGoodsFHSP> m_aGoodsFHSP;

	DWORD m_dwDate;
	DWORD m_dwTime;
};

#endif


