#ifndef __SORT_ARRAY_H__
#define __SORT_ARRAY_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "Array.h"

template <class TYPE> class CSortArray : public CArray<TYPE, TYPE&>
{
public:
	CSortArray();

	int Add(TYPE);
	int Find(TYPE);
	int Change(TYPE);

	void Copy(const CSortArray& src);

public:
	bool m_bAscend;
};

template <class TYPE> CSortArray<TYPE>::CSortArray()
{
	m_bAscend = true;
}

template <class TYPE> int CSortArray<TYPE>::Add(TYPE newElement)
{
	int nLeft = 0;
	int nRight = CArray<TYPE, TYPE&>::m_nSize-1;
	if (nRight<nLeft)
	{
		CArray<TYPE, TYPE&>::Add(newElement);
		return 0;
	}
	for ( ; nLeft<=nRight; )
	{
		if (newElement==CArray<TYPE, TYPE&>::m_pData[nLeft])
        	return -1;
		else if ((m_bAscend==true && newElement<CArray<TYPE, TYPE&>::m_pData[nLeft]) || (m_bAscend==false && newElement>CArray<TYPE, TYPE&>::m_pData[nLeft]))
		{
			CArray<TYPE, TYPE&>::InsertAt(nLeft, newElement);
			return nLeft;
		}
        
		if (newElement==CArray<TYPE, TYPE&>::m_pData[nRight])
        	return -1;
		else if ((m_bAscend==true && newElement>CArray<TYPE, TYPE&>::m_pData[nRight]) || (m_bAscend==false && newElement<CArray<TYPE, TYPE&>::m_pData[nRight]))
		{
			CArray<TYPE, TYPE&>::InsertAt(nRight+1, newElement);
			return nRight+1;
		}

		int nMid = (nLeft+nRight)/2;
		if (newElement==CArray<TYPE, TYPE&>::m_pData[nMid])
        	return -1;
		else if ((m_bAscend==true && newElement>CArray<TYPE, TYPE&>::m_pData[nMid]) || (m_bAscend==false && newElement<CArray<TYPE, TYPE&>::m_pData[nMid]))
			nLeft = nMid+1;
        else
			nRight = nMid-1;
	}
	return -1;
}

template <class TYPE> int CSortArray<TYPE>::Find(TYPE newElement)
{
	int nLeft = 0;
	int nRight = CArray<TYPE, TYPE&>::m_nSize-1;
	for ( ; nLeft<=nRight; )
	{
		int nMid = (nLeft+nRight)/2;
		if (newElement==CArray<TYPE, TYPE&>::m_pData[nMid])
			return nMid;
		else if ((m_bAscend==true && newElement<CArray<TYPE, TYPE&>::m_pData[nMid]) || (m_bAscend==false && newElement>CArray<TYPE, TYPE&>::m_pData[nMid]))
			nRight = nMid-1;
		else
			nLeft = nMid+1;
	}
	return -1;
}

template <class TYPE> int CSortArray<TYPE>::Change(TYPE newElement)
{
	int nFind = Find(newElement);
	if (nFind>=0)
		CArray<TYPE, TYPE&>::m_pData[nFind] = newElement;
	else
		nFind = Add(newElement);
	return nFind;
}

template <class TYPE> void CSortArray<TYPE>::Copy(const CSortArray& src)
{
	m_bAscend = src.m_bAscend;
	CArray<TYPE, TYPE&>::Copy(src);
}

#endif	// __SORT_ARRAY_H__


