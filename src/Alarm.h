#ifndef _ALARM_H_
#define _ALARM_H_

#include "GlobalData.h"
#include "DataFile.h"

#include "RWLock.h"
#include "String.h"

class CAlarmAll
{
public:
	CAlarmAll()
	{
		m_dwClearCount = 0;
	}

	void Init(CString strPath);
	void Exit();

	void AddAlarm(CAlarmArray& aAlarm);
	void ClearAlarm();

public:
	DWORD m_dwClearCount;
	CAlarmData m_alarmTemp;

	CAlarmData m_alarm;
	CDataFile_Alarm m_dfAlarm;
};

//专业版所需的操盘线预警 等数据,放在这里

struct CYJGoods
{
	DWORD  m_dwGoodsID;
	char  m_cPeriod; 
	char  m_cBS;  

	CYJGoods()
	{
		m_dwGoodsID = 0;
		m_cBS = 0;
		m_cPeriod = 0;;
	}   
	const CYJGoods& operator=(const CYJGoods& src);
};
BOOL operator > (CYJGoods &first, CYJGoods &second);
BOOL operator == (CYJGoods &first, CYJGoods &second);
BOOL operator < (CYJGoods &first, CYJGoods &second);

#pragma  pack(push,1)
struct CASAlertItem
{
	CASAlertItem()
	{
		m_sAlertID = 0;
		m_cState = 0;
		m_dwGoodID = 0;
		m_dwTime = 0;
		//m_dwDate = 0;  // add
		m_fPrice = 0;
	}
	short   m_sAlertID;
	char    m_cState;
	DWORD   m_dwGoodID;
	DWORD   m_dwTime;
	//DWORD   m_dwDate;           // add //yxf 去掉,没用
	double  m_fPrice;
};

typedef CArray<CASAlertItem,CASAlertItem&> CASAlertArray;

struct CASAlerts
{
	CASAlerts()
	{
		m_dwValueDate = 0;
		//m_dwTotalCount = 0;
	}
	DWORD   m_dwValueDate;
	//DWORD   m_dwTotalCount;
	//CASAlertItem m_aiItem[1];
	RWLock m_wrAlarm;
	CASAlertArray m_aiItem;
};

///////////////////////////////////////
//指数和板块的操盘线状态统计
struct CCPXStat
{
	DWORD m_dwGoodsID;
	DWORD m_dwDate;
	WORD m_wOverSell;
	WORD m_wOverBuy;
	WORD m_wBuy;
	WORD m_wSell;
};

#pragma pack(pop)

BOOL operator > (CCPXStat &first, CCPXStat &second);
BOOL operator == (CCPXStat &first, CCPXStat &second);
BOOL operator < (CCPXStat &first, CCPXStat &second);

typedef CSortArray<CCPXStat> CCPXStatArray;

class CGoodsCPXStat
{
public:
	CGoodsCPXStat();
	~CGoodsCPXStat();
	DWORD m_dwUpdateDate;
	DWORD m_dwUpdateTime;
	CCPXStatArray m_aGoodsCpxStat[3];//0 sh;1:sz;2:bk
	CCPXStat m_aCPXStat[3];
	RWLock m_wrCpxStat;
	bool Read();
	void Write(BOOL bFree = FALSE);
	void Get(DWORD dwDate,CCPXStatArray &aCpxStat);
private:
	void AddToday();
};

extern CASAlerts g_Alerts;
extern CGoodsCPXStat g_CpxStat;

#endif

//////////////////////////////////////////////////////////////////////////


