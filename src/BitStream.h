// BitStream.h: interface for the CBitStream class.
//
//////////////////////////////////////////////////////////////////////

#ifndef BITSTREAM_H
#define BITSTREAM_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "XInt32.h"
#include "Buffer.h"

const int ERROR_BITSTREAM_NOMEM = -10001;
const int ERROR_BITSTREAM_NODATA = -10002;
const int ERROR_BITSTREAM_NOCODE = -10003;
const int ERROR_BITSTREAM_NOBUFFER = -10004;

struct CBitCode
{
	WORD	m_wCodeBits;
	BYTE	m_cCodeLen;
	CHAR	m_cDataType;	// 'C'-const, 'D'-DWORD, 'I'-INT32, 'X'-XInt32, 'H'-hundred DWORD, 'h'-hundred INT
	BYTE	m_cDataLen;
	DWORD	m_dwDataBias;
#ifdef _DEBUG
	DWORD	m_dwCodeCount;
#endif
};

class CBitStream
{
public:
	CBitStream();
	CBitStream(CBuffer*, BOOL bRead);
	CBitStream(PBYTE pcBuf, int nBufLen);
	CBitStream(int nBufLen);

	virtual ~CBitStream();

	void SetBuffer(PBYTE pcBuf, int nBufLen);

	int  GetCurPos()	{ return m_nCurPos; }

	BYTE  WriteDWORD(DWORD dw, int nBit);
	DWORD ReadDWORD(int nBit, BOOL bMovePos = TRUE);

	BYTE EncodeData(DWORD dwData, CBitCode* pBitCodes, int nNumOfCodes, PDWORD pdwBase=NULL, BOOL bReverse=FALSE);
	BYTE DecodeData(DWORD& dwData, CBitCode* pBitCodes, int nNumOfCodes, PDWORD pdwBase=NULL, BOOL bReverse=FALSE);

	BYTE EncodeXInt32(XInt32 xData, CBitCode* pBitCodes, int nNumOfCodes, PXInt32 pxBase=NULL, BOOL bReverse=FALSE);
	BYTE DecodeXInt32(XInt32& xData, CBitCode* pBitCodes, int nNumOfCodes, PXInt32 pxBase=NULL, BOOL bReverse=FALSE);

	BYTE  WriteBOOL(BOOL bValue);

private:
	int ReallocBuf(int nBits);
	CBitCode* MatchCodeEncode(DWORD& dw, CBitCode* pBitCodes, int nNumOfCodes);
	CBitCode* MatchCodeDecode(DWORD& dw, BYTE& cBytes, CBitCode* pBitCodes, int nNumOfCodes);

private:
	char		m_cBufType;		// 0: CBitStream(CBuffer*)
								// 1: CBitStream(PBYTE pcBuf, int nBufLen) or SetBuffer(PBYTE pcBuf, int nBufLen)
								// 2: CBitStream(int nBufLen) 内存自己管
	PBYTE		m_pcBuf;
	int			m_nBitLen;

	CBuffer*	m_pBuffer;
	UINT		m_uStartPos;

	int			m_nCurPos;
};
#endif // !defined(AFX_BITSTREAM_H__136F4CCC_314C_4C8C_996F_90C9509120E7__INCLUDED_)



