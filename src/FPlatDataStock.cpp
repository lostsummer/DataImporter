#include "FPlatDataStock.h"
#include <math.h>
#include "GoodsJBM.h"
#include "MDS.h"

#define ARRSIZE(A) (sizeof(A)/sizeof(A[0]))
//2013-6-5 Wang.YJ 新财务画法
const int		V_FIN_DRAW_BEGIN  = 59;
const int 		V_STOCK		=		V_FIN_DRAW_BEGIN +1;		//1001 A股  1002 B股
const int 		V_IsFinacial		=		V_FIN_DRAW_BEGIN +2;		// 1 为金融类   0 为非金融类  
const int 		V_YYYYMMDD =		V_FIN_DRAW_BEGIN +3;		// (年月日 例: 20130331)  
const int		V_10KK			 =		V_FIN_DRAW_BEGIN +4;		// 1%(0.01) 的 1000000倍 
const int		V_BIGINT				=		V_FIN_DRAW_BEGIN +5;		// 大数字，需要根据值转成万或者亿
const int		V_GU_10K		=		V_FIN_DRAW_BEGIN +6;		//股的 10000倍 
const int		V_GE				=		V_FIN_DRAW_BEGIN +7;		//个数
const int		V_YUAN			=		V_FIN_DRAW_BEGIN +8;		//元
const int		V_YUAN_10K	=		V_FIN_DRAW_BEGIN +9;		// 元的 10000倍
const int		V_DATABASE	=		V_FIN_DRAW_BEGIN +10;		// 与数据库相同
const int		V_FEN_10K	=		V_FIN_DRAW_BEGIN +11;		// 份的 10000倍
const int		V_TIAN	=		V_FIN_DRAW_BEGIN +12;		// 天数  为负值时显示已到期
const int 		V_BLANK		=		V_FIN_DRAW_BEGIN +13;		//空白行
const int 		V_M_10K		=		V_FIN_DRAW_BEGIN +14;		// 月数 的10000倍
const int 		V_ORGI		=		V_FIN_DRAW_BEGIN +15;		//原始值
const int 		V_FUNDTYPE		=		V_FIN_DRAW_BEGIN +16;		//基金类型 
//4001	股票型 4002	债券型 4003	混合型 4004	货币型 4005	封闭式 4006	基金型
const int		V_TZFG				=  V_FIN_DRAW_BEGIN +17;		//投资风格
const int		V_FHSP				=  V_FIN_DRAW_BEGIN +18;		//投资风格

CFPlatData_All g_FPlatData;

CFieldMap::CFieldMap(CString strShortName,CString strFullName,short IDNew, short IDOld, short idxStock,short idxHK,
					 short idxFund,short idxBond,UCHAR cValueType,int nDrawType,UCHAR nPower,CString strUnit,UCHAR ucDigit,
					 EColorType ect,CString strShowQuarter,UCHAR nOldPower)
{
	m_strShortName = strShortName;
	m_strFullName = strFullName;

	m_IDNew = IDNew;
	m_IDOld = IDOld;
	m_IdxStock = idxStock;
	m_idx_HKStock = idxHK;
	m_idx_Fund =idxFund;
	m_idxBond = idxBond;

	m_cValueType = cValueType;
	m_nDrawType = nDrawType;

	m_nPower = nPower;
	m_nOldPower = nOldPower;
	m_strUnit = strUnit;
	m_ucDigit = ucDigit;

	m_ect = ect;
	m_strShowQuarter = strShowQuarter;
}

CFieldMap::CFieldMap()
{
	m_strShortName = "";
	m_strFullName = "";

	m_IDNew = -1;
	m_IDOld = -1;
	m_cValueType = 0;
	m_IdxStock = -1;
	m_idx_HKStock = -1;
	m_idx_Fund =-1;
	m_idxBond = -1;

	m_cValueType = -1;
	m_nDrawType = V_BLANK;

	m_nPower = 0;
	m_nOldPower = 0;
	m_strUnit = "";

	m_ucDigit = 0;

	m_ect = ECT_DEFAULT;
	m_strShowQuarter = "0000";
}

CFieldMap s_map_FieldMap[JN_END - JN_BEGIN];

//2013-6-4 Wang.YJ 新旧基本面字段映射表
// static CFieldMap s_Array_FieldMap[] =
// {
// 		CFieldMap("","",JN_BLANK,-1,S_EndDate,H_EndDate,F_EndDate,-1,0,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0),    //空白
// 
// 		//2013-6-18 Wang.YJ 基本面分组
// 		CFieldMap("","",JN_CAL_YJ,-1,-1,-1,-1,-1,1,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0),
// 		CFieldMap("","",JN_CAL_GB,-1,-1,-1,-1,-1,1,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0), 
// 		CFieldMap("","",JN_CAL_ZYZB,-1,-1,-1,-1,-1,1,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0),
// 
// 		CFieldMap("","",JN_CAL_B_FXCS,-1,-1,-1,-1,-1,0,V_ORGI,4,"次",0,ECT_DEFAULT,"0000",0),    //债券付息次数
// 		CFieldMap("最新送转","最新送转",JN_CAL_ZZ_BL,-1,S_ZZ_BL,-1,-1,-1,0,V_FHSP,4,"股",0,ECT_RED,"3000",0),    //V_FHSP
// 
// 
// 		//2013-6-17 Wang.YJ 以下为新财文件中读取出的数据
// 		CFieldMap("报告日期","报告日期",JN_DATE,J_DATE,S_EndDate,H_EndDate,F_EndDate,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("总股本","总股本",JN_ZGB,J_ZGB,S_ZGB,H_ZGB,-1,-1,0,V_BIGINT,4,"股",0,ECT_VOL,"0000",4),    //V_BIGINT
// 		CFieldMap("国家股","国家股",JN_GJG,J_GJG,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("法人股","法人股",JN_FRG,J_FRG,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("A 股","A 股",JN_AG,J_AG,S_AG,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("B 股","B 股",JN_BG,J_BG,S_BG,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("H 股","H 股",JN_HG,J_HG,S_HG,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("职工股","职工股",JN_ZGG,J_ZGG,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("总资产","总资产",JN_ZZC,J_ZZC,-1,H_ZZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("流动资产","流动资产",JN_LDZC,J_LDZC,-1,H_LDZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("固定资产","固定资产",JN_GDZC,J_GDZC,-1,H_GDZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("无形资产","无形资产",JN_WXZC,J_WXZC,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("长期投资","长期投资",JN_CQTZ,J_CQTZ,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("流动负债","流动负债",JN_LDFZ,J_LDFZ,-1,H_LDFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("长期负债","长期负债",JN_CQFZ,J_CQFZ,-1,H_CQFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("资本公积金","资本公积金",JN_ZBGJJ,J_ZBGJJ,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("每股公积金","每股公积金",JN_MGGJJ,J_MGGJJ,S_MGGJJ,-1,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"0000",-2),    //V_YUAN_10K
// 		CFieldMap("股东权益","股东权益",JN_GDQY,J_GDQY,-1,H_GDQY,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("主营收入","主营收入",JN_ZYSR,J_ZYSR,S_ZYSR,H_ZYSR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("主营利润","主营利润",JN_ZYLR,J_ZYLR,S_ZYLR,H_ZYLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("其他利润","其他利润",JN_QTLR,J_QTLR,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("营业利润","营业利润",JN_YYLR,J_YYLR,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("投资收益","投资收益",JN_TZSY,J_TZSY,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("补贴收入","补贴收入",JN_BTSR,J_BTSR,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("营业外收支","营业外收支",JN_YYWSZ,J_YYWSZ,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("以前年度损益调整","以前年度损益调整",JN_NDSY,J_NDSY,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("利润总额","利润总额",JN_LRZE,J_LRZE,-1,H_ZLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("净利润","净利润",JN_JLR,J_JLR,-1,H_JLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("未分配利润","未分配利润",JN_WFPLR,J_WFPLR,-1,-1,-1,-1,0,V_BIGINT,0,"元",4,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("每股未分配利润","每股未分配利润",JN_MGWFPLR,J_MGWFPLR,S_MGWFPLR,-1,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"0000",-2),    //V_YUAN_10K
// 		CFieldMap("每股收益","每股收益",JN_MGSY,J_MGSY,S_MGSY,H_MGSY,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"0000",-2),    //V_YUAN_10K
// 		CFieldMap("每股净资产","每股净资产",JN_MGJZC,J_MGJZC,S_MGJZC,H_MGJZC,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"0000",-2),    //V_YUAN_10K
// 		CFieldMap("调整后的每股净资产","调整后的每股净资产",JN_TZMGJZC,J_TZMGJZC,-1,-1,-1,-1,0,V_BLANK,0,"元",3,ECT_DEFAULT,"0000",-2),    //
// 		CFieldMap("股东权益比","股东权益比",JN_GDQYB,J_GDQYB,S_GDQYB,H_GDQYB,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",2),    //V_10KK
// 		CFieldMap("净资产收益率","净资产收益率",JN_JZCSYL,J_JZCSYL,S_JZCSYL,H_JZCSYL,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",2),    //V_10KK
// 		CFieldMap("限售A 股","限售A 股",JN_AG_XS,J_AG_XS,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("限售B 股","限售B 股",JN_BG_XS,J_BG_XS,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("股东户数","股东户数",JN_GDRS,J_GDRS,-1,-1,-1,-1,0,V_BLANK,0,"户",3,ECT_DEFAULT,"0000",0),    //
// 		CFieldMap("户均持股","户均持有效流通股",JN_S_HJCG,J_RJCG,S_HJCG,-1,-1,-1,0,V_GU_10K,4,"股",0,ECT_DEFAULT,"0000",4),    //V_GU_10K
// 		CFieldMap("证券类型","证券类型",JN_S_SecuType,-1,S_SecuType,H_SecuType,-1,-1,0,V_STOCK,0,"",3,ECT_DEFAULT,"0000",0),    //V_STOCK
// 		CFieldMap("是否金融类","是否金融类",JN_S_IsFinacial,-1,S_IsFinacial,-1,-1,-1,0,V_IsFinacial,0,"",3,ECT_DEFAULT,"0000",0),    //V_IsFinacial
// 		CFieldMap("交易日期","交易日期",JN_S_TradeDate,-1,S_TradeDate,-1,-1,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("市盈率","市盈率",JN_S_SYL,-1,S_SYL,H_SYL,-1,-1,0,V_10KK,4,"",2,ECT_AMT,"1100",0),    //V_10KK
// 		CFieldMap("市净率","市净率",JN_S_SJL,-1,S_SJL,H_SJL,-1,-1,0,V_10KK,4,"",2,ECT_AMT,"1100",0),    //V_10KK
// 		CFieldMap("每股经营现金流","每股经营现金流",JN_S_MGJYXJL,-1,S_MGJYXJL,-1,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"0000",0),    //V_YUAN_10K
// 		CFieldMap("流通股本","流通股本",JN_S_LTGB,-1,S_LTGB,H_ZGB,-1,-1,0,V_BIGINT,4,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("有效流通股本","有效流通股本",JN_S_YXLTGB,-1,S_YXLTGB,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("户均持股环比","户均持股环比",JN_S_HJCGTB,-1,S_HJCG_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("股东户数环比","股东户数环比",JN_S_GDHSB,-1,S_GDHS_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("主营收入同比","主营收入同比",JN_S_ZYSRTB,-1,S_ZYSRTB,H_ZYSRTB,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("主营利润同比","主营利润同比",JN_S_ZYLRTB,-1,S_ZYLRTB,H_ZYLRTB,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("净利润同比","净利润同比",JN_S_JLRTB,-1,S_JLRTB,H_JLRTB,F_JLR_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
// 		CFieldMap("销售毛利率","销售毛利率",JN_S_XSMLL,-1,S_XSMLL,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("净利息收益率","净利息收益率",JN_S_JLXSYL,-1,S_JLXSYL,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("资产负债率","资产负债率",JN_S_ZCFZL,-1,S_ZCFZL,H_ZCFZL,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("股息率","股息率",JN_S_GuXiL,-1,S_GuXiL,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("机构持股家数","机构持股家数",JN_S_JGCGJS,-1,S_JGCGJS,-1,-1,-1,0,V_GE,0,"家",0,ECT_DEFAULT,"0000",0),    //V_GE
// 		CFieldMap("基金持股家数","基金持股家数",JN_S_JJCGJS,-1,S_JJCGJS,-1,-1,-1,0,V_GE,0,"家",0,ECT_DEFAULT,"0000",0),    //V_GE
// 		CFieldMap("机构持股占有效流通比","机构持股占有效流通比",JN_S_JGCGZB,-1,S_JGCGZB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("基金持股占有效流通比","基金持股占有效流通比",JN_S_JJN_ZYXLTB,-1,S_JJ_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("QFII持股占有效流通比","QFII持股占有效流通比",JN_S_QF_ZYXLTB,-1,S_QF_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("保险持股占有效流通比","保险持股占有效流通比",JN_S_BX_ZYXLTB,-1,S_BX_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("券商持股占有效流通比","券商持股占有效流通比",JN_S_QS_ZYXLTB,-1,S_QS_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("信托持股占有效流通比","信托持股占有效流通比",JN_S_XT_ZYXLTB,-1,S_XT_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("经营活动现金流","经营活动现金流",JN_S_JYHDXJL,-1,S_JYHDXJL,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("预付账款","预付账款",JN_S_YFZK,-1,S_YFZK,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("应收账款","应收账款",JN_S_YSZK,-1,S_YSZK,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("现金及央行存款","现金及存放中央银行款项",JN_S_XJYHKX,-1,S_XJYHKX,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("发放贷款及垫款","发放贷款及垫款",JN_S_FFDKDK,-1,S_FFDKDK,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("同业及其他存款","同业及其他金融机构存放款项",JN_S_TYCFKX,-1,S_TYCFKX,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("吸收存款","吸收存款",JN_S_XSKX,-1,S_XSKX,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("上市日期","上市日期",JN_S_SSRQ,-1,S_SSRQ,H_SSRQ,-1,B_SSRQ,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("股票简称","股票简称",JN_CS_Abbr,-1,CS_Abbr,-1,CS_Abbr,CS_Abbr,1,V_ORGI,0,"",3,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("最新分红","最新分红",JN_CS_ZXFH,-1,CS_ZXFH,-1,CS_ZXFH,-1,1,V_ORGI,0,"",3,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("所属行业","所属行业",JN_CS_S_Bkname,-1,CS_S_BKname,-1,-1,-1,1,V_ORGI,0,"",3,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("所属概念","所属概念",JN_CS_S_Concept,-1,CS_S_Concept,-1,-1,-1,1,V_ORGI,0,"",3,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("总负债","总负债",JN_H_ZFZ,-1,-1,H_ZFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("成立日期","成立日期",JN_F_CLRQ,-1,-1,-1,F_CLRQ,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("基金类型","基金类型",JN_F_FundType,-1,-1,-1,F_FundType,-1,0,V_FUNDTYPE,0,"",3,ECT_AMT,"0000",0),    //V_FUNDTYPE
// 		CFieldMap("投资风格","投资风格",JN_F_TZFG,-1,-1,-1,F_TZFG,-1,0,V_TZFG,0,"",3,ECT_AMT,"0000",0),    //V_TZFG
// 		CFieldMap("单位净值","单位净值",JN_F_DWJZ,-1,-1,-1,F_DWJZ,-1,0,V_YUAN_10K,4,"元",3,ECT_AMT,"0000",0),    //V_YUAN_10K
// 		CFieldMap("累计净值","累计净值",JN_F_LJJZ,-1,-1,-1,F_LJJZ,-1,0,V_YUAN_10K,4,"元",3,ECT_AMT,"0000",0),    //V_YUAN_10K
// 		CFieldMap("单位净值增长率","单位净值增长率",JN_F_DWJZ_ZZL,-1,-1,-1,F_DWJZ_ZZL,-1,0,V_10KK,4,"%",2,ECT_AMT,"0000",0),    //V_10KK
// 		CFieldMap("累计净值增长率","累计净值增长率",JN_F_LJJZ_ZZL,-1,-1,-1,F_LJJZ_ZZL,-1,0,V_10KK,4,"%",2,ECT_AMT,"0000",0),    //V_10KK
// 		CFieldMap("基金份额","基金份额",JN_F_JJFE,-1,-1,-1,F_JJFE_ZX,-1,0,V_BIGINT,4,"份",2,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("每万份单位收益","每万份单位收益",JN_F_WFDWSY,-1,-1,-1,F_WFDWSY,-1,0,V_YUAN_10K,4,"元",3,ECT_DEFAULT,"0000",0),    //V_YUAN_10K
// 		CFieldMap("7日年化收益率(%)","7日年化收益率(%)",JN_F_7NHSYL,-1,-1,-1,F_7NHSYL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("万份收益增长率(%)","万份收益增长率(%)",JN_F_WFSYZZL,-1,-1,-1,F_WFSYZZL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("7日年化收益增长率(%)","7日年化收益增长率(%)",JN_F_7NHSYZZL,-1,-1,-1,F_7NHSYZZL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("权益投资占比","权益类投资占比",JN_F_QY_TZZB,-1,-1,-1,F_QY_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("股票投资占比","股票投资占比",JN_F_GP_TZZB,-1,-1,-1,F_GP_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("存托凭证占比","存托凭证占比",JN_F_CT_TZZB,-1,-1,-1,F_CT_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("基金投资占比","基金投资占比",JN_F_JJN_TZZB,-1,-1,-1,F_JJ_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("固定收益投资占比","固定收益类投资占比",JN_F_GD_TZZB,-1,-1,-1,F_GD_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("债券投资占比","债券投资占比",JN_F_ZQ_TZZB,-1,-1,-1,F_ZQ_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("资产支持证券占比","资产支持证券占比",JN_F_ZC_TZZB,-1,-1,-1,F_ZC_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("金融衍生品投资占比","金融衍生品投资占比",JN_F_JR_TZZB,-1,-1,-1,F_JR_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("买入返售金融资产占比","买入返售金融资产占比",JN_F_FS_TZZB,-1,-1,-1,F_FS_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("银行存款和备付金占比","银行存款和结算备付金占比",JN_F_CK_TZZB,-1,-1,-1,F_CK_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("货币市场工具占比","货币市场工具占比",JN_F_HB_TZZB,-1,-1,-1,F_HB_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("其他资产占比","其他资产占比",JN_F_QT_TZZB,-1,-1,-1,F_QT_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
// 		CFieldMap("财报报告日期","财报报告日期",JN_F_CBBGRQ,-1,-1,-1,F_CBBGRQ,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("总利润同比","总利润同比",JN_F_ZLR_TB,-1,-1,-1,F_ZLR_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
// 		CFieldMap("总资产同比","总资产同比",JN_F_ZZC_TB,-1,-1,-1,F_ZZC_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
// 		CFieldMap("总负债同比","总负债同比",JN_F_ZFZ_TB,-1,-1,-1,F_ZFZ_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
// 		CFieldMap("存续起始日","存续起始日",JN_F_CXQSR,-1,-1,-1,F_CXQSR,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("存续终止日","存续终止日",JN_F_CXZZQ,-1,-1,-1,F_CXZZQ,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("据到期日","据到期日",JN_F_JDQR,-1,-1,-1,F_JDQR,-1,0,V_TIAN,0,"",0,ECT_DEFAULT,"0000",0),    //V_TIAN
// 		CFieldMap("存续期限","存续期限",JN_F_CXQX,-1,-1,-1,F_CXQX,-1,0,V_DATABASE,0,"",0,ECT_DEFAULT,"0000",0),    //V_DATABASE
// 		CFieldMap("托管费率(%)","托管费率(%)",JN_F_TGFL,-1,-1,-1,F_TGFL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("管理费率(%)","管理费率(%)",JN_F_GLFL,-1,-1,-1,F_GLFL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
// 		CFieldMap("基金市场","基金市场",JN_F_JJSC,-1,-1,-1,F_JJSC,-1,0,V_ORGI,0,"",0,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("基金名称","基金名称",JN_CS_Name,-1,-1,-1,CS_Name,CS_Name,1,V_ORGI,0,"",0,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("基金经理","基金经理",JN_CS_F_JLR,-1,-1,-1,CS_F_JLR,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("基金管理人","基金管理人",JN_CS_F_GLR,-1,-1,-1,CS_F_GLR,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("基金托管人","基金托管人",JN_CS_F_TGR,-1,-1,-1,CS_F_TGR,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("业绩比较基准","业绩比较基准",JN_CS_F_YJBJJZ,-1,-1,-1,CS_F_YJBJJZ,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("债券期限","债券期限",JN_B_ZQQX,-1,-1,-1,-1,B_ZQQX,0,V_DATABASE,4,"月",0,ECT_DEFAULT,"0000",0),    //V_DATABASE
// 		CFieldMap("付息频率","付息频率",JN_B_FXPL,-1,-1,-1,-1,B_FXPL,0,V_DATABASE,0,"",0,ECT_DEFAULT,"0000",0),    //V_DATABASE
// 		CFieldMap("发行量","发行量",JN_B_GZ_FXL,-1,-1,-1,-1,B_FX_FXL,0,V_BIGINT,0,"元",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("发行价格","发行价格",JN_B_GZ_FXJG,-1,-1,-1,-1,B_FX_FXJG,0,V_YUAN_10K,4,"元",0,ECT_AMT,"0000",0),    //V_YUAN_10K
// 		CFieldMap("单位面值","单位面值",JN_B_GZ_DWMZ,-1,-1,-1,-1,B_FX_DWMZ,0,V_YUAN_10K,4,"元",0,ECT_AMT,"0000",0),    //V_YUAN_10K
// 		CFieldMap("转股价格","转股价格",JN_B_ZGJG,-1,-1,-1,-1,B_ZGJG,0,V_YUAN_10K,4,"元",2,ECT_AMT,"0000",0),    //V_YUAN_10K
// 		CFieldMap("票面利率(%)","票面利率(%)",JN_B_PMLL,-1,-1,-1,-1,B_PMLL,0,V_10KK,4,"%",2,ECT_AMT,"0000",0),    //V_10KK
// 		CFieldMap("发行日期","发行日期",JN_B_FXRQ,-1,-1,-1,-1,B_FXRQ,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("起息日","起息日",JN_B_QXR,-1,-1,-1,-1,B_QXR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("到期日","到期日",JN_B_DQR,-1,-1,-1,-1,B_DQR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("兑付日","兑付日",JN_B_DFR,-1,-1,-1,-1,B_DFR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("转股截止日","转股截止日",JN_B_ZGJZR,-1,-1,-1,-1,B_ZGJZR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("债券剩余交易日","债券剩余交易日",JN_B_ZQSYJYR,-1,-1,-1,-1,B_ZQSYJYR,0,V_TIAN,0,"",0,ECT_DEFAULT,"0000",0),    //V_TIAN
// 		CFieldMap("债券种类名称","债券种类名称",JN_CS_B_ZLMC,-1,-1,-1,-1,CS_B_ZLMC,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("利率类型","利率类型",JN_CS_B_LLLX,-1,-1,-1,-1,CS_B_LLLX,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("计利方式","计利方式",JN_CS_B_JXFS,-1,-1,-1,-1,CS_B_JXFS,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("付息方式","付息方式",JN_CS_B_FXFS,-1,-1,-1,-1,CS_B_FXFS,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("发行人","发行人",JN_CS_B_FXR,-1,-1,-1,-1,CS_B_FXR,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("年度计息基准","年度计息基准",JN_CS_B_NDJXJZ,-1,-1,-1,-1,CS_B_NDJXJZ,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("52周涨幅","52周涨幅",JN_S_52Z_ZF,-1,S_52Z_ZF,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_ZDF,"0000",0),    //V_ORGI
// 		CFieldMap("分红报告期","分红报告期",JN_S_FH_BGQ,-1,S_FH_BGQ,-1,-1,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("派息比例(港币)","派息比例(港币)",JN_S_PXBL_HKD,-1,S_PXBL_HKD,-1,-1,-1,0,V_ORGI,4,"",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("派息比例(美元)","派息比例(美元)",JN_S_PXBL_USD,-1,S_PXBL_USD,-1,-1,-1,0,V_ORGI,4,"",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("最新派息","最新派息",JN_S_PXBL_RMB,-1,S_PXBL_RMB,-1,-1,-1,0,V_FHSP,4,"元",2,ECT_RED,"3000",0),    //V_FHSP
// 		CFieldMap("转增比例","转增比例",JN_S_ZZ_BL,-1,S_ZZ_BL,-1,-1,-1,0,V_FHSP,4,"股",0,ECT_RED,"3000",0),    //V_FHSP
// 		CFieldMap("送股比例","送股比例",JN_S_SG_BL,-1,S_SG_BL,-1,-1,-1,0,V_FHSP,4,"股",0,ECT_RED,"3000",0),    //V_FHSP
// 		CFieldMap("方案进度编码","方案进度编码",JN_S_FAJD_BM,-1,S_FAJD_BM,-1,-1,-1,0,V_ORGI,0,"",0,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("业绩预告报告期","业绩预告报告期",JN_S_YJYG_BGQ,-1,S_YJYG_BGQ,-1,-1,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("财务报告期","财务报告期",JN_S_CW_BGQ,-1,S_CW_BGQ,-1,-1,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
// 		CFieldMap("营业利润","营业利润",JN_S_YYLR,J_YYLR,S_YYLR,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("其他利润","其他利润",JN_S_QTLR,J_QTLR,S_QTLR,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("营业外收支","营业外收支",JN_S_YYWSZ,J_YYWSZ,S_YYWSZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("补贴收入","补贴收入",JN_S_BTSR,J_BTSR,S_BTSR,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("投资收益","投资收益",JN_S_TZSY,J_TZSY,S_TZSY,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("损益调整","损益调整",JN_S_SYTZ,J_NDSY,S_SYTZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("利润总额","利润总额",JN_S_LRZE,J_LRZE,S_LRZE,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("净利润","净利润",JN_S_JLR,J_JLR,S_JLR,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("总资产","总资产",JN_S_ZZC,J_ZZC,S_ZZC,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("流动资产","流动资产",JN_S_LDZC,J_LDZC,S_LDZC,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("无形资产","无形资产",JN_S_WXZC,J_WXZC,S_WXZC,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("固定资产","固定资产",JN_S_GDZC,J_GDZC,S_GDZC,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("长期投资","长期投资",JN_S_CQTZ,J_CQTZ,S_CQTZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("应付账款","应付账款",JN_S_YFu_ZK,-1,S_YFu_ZK,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("预收账款","预收账款",JN_S_YS_ZK,-1,S_YS_ZK,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",0),    //V_BIGINT
// 		CFieldMap("流动负债","流动负债",JN_S_LD_FZ,J_LDFZ,S_LD_FZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("长期负债","长期负债",JN_S_CQ_FZ,J_CQFZ,S_CQ_FZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("净资产","净资产",JN_S_JZC,J_GDQY,S_JZC,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("股东权益","股东权益",JN_S_GDQY,J_GDQY,S_GDQY,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("资本公积金","资本公积金",JN_S_ZB_GJJ,J_ZBGJJ,S_ZB_GJJ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("未分配利润","未分配利润",JN_S_F_WFPLR,J_WFPLR,S_F_WFPLR,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"0000",4),    //V_BIGINT
// 		CFieldMap("每股收益(年化)","每股收益(年化)",JN_S_NH_MGSY,-1,S_NH_MGSY,-1,-1,-1,0,V_ORGI,4,"元",4,ECT_ZDF,"0000",0),    //V_ORGI
// 		CFieldMap("净利润率","净利润率",JN_S_JLRL,-1,S_JLRL,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("市销率","市销率",JN_S_SXL,-1,S_SXL,-1,-1,-1,0,V_ORGI,4,"",1,ECT_AMT,"1000",0),    //V_ORGI
// 		CFieldMap("主营收入环比","主营收入环比",JN_S_ZYSR_HB,-1,S_ZYSR_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("主营利润环比","主营利润环比",JN_S_ZYLR_HB,-1,S_ZYLR_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("净利润环比","净利润环比",JN_S_JLR_HB,-1,S_JLR_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("三年复合增长","三年复合增长",JN_S_Y3_FHZZ,-1,S_Y3_FHZZ,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("股东户数","股东户数",JN_S_GD_HS,J_GDRS,S_GD_HS,-1,-1,-1,0,V_ORGI,0,"户",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("基金持股","基金持股",JN_S_JJ_CG,-1,S_JJ_CG,-1,-1,-1,0,V_BIGINT,4,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("基金持股市值","基金持股市值",JN_S_JJ_CG_SZ,-1,S_JJ_CG_SZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_VOL,"0000",0),    //V_BIGINT
// 		CFieldMap("机构持股占流通股比","机构持股占流通股比",JN_S_ZLTGB_JG,-1,S_ZLTGB_JG,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("基金持股占流通股比","基金持股占流通股比",JN_S_ZLTGB_JJ,-1,S_ZLTGB_JJ,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("QFII持股占流通股比","QFII持股占流通股比",JN_S_ZLTGB_QF2,-1,S_ZLTGB_QF2,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("保险持股占流通股比","保险持股占流通股比",JN_S_ZLTGB_BX,-1,S_ZLTGB_BX,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("券商持股占流通股比","券商持股占流通股比",JN_S_ZLTGB_QS,-1,S_ZLTGB_QS,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("信托持股占流通股比","信托持股占流通股比",JN_S_ZLTGB_XT,-1,S_ZLTGB_XT,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
// 		CFieldMap("方案进度","方案进度",JN_CS_S_FAJD,-1,CS_S_FAJD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
// 		CFieldMap("业绩预告","业绩预告",JN_CS_S_YJYG,-1,CS_S_YJYG,-1,-1,-1,1,V_ORGI,0,"",0,ECT_AMT,"4000",0),    //V_ORGI
// 		CFieldMap("业绩大增幅度","业绩大增幅度",JN_CS_S_YJDZ_FD,-1,CS_S_YJDZ_FD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_RED,"0000",0),    //V_ORGI
// 		CFieldMap("业绩增长幅度","业绩增长幅度",JN_CS_S_YJZZ_FD,-1,CS_S_YJZZ_FD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_RED,"0000",0),    //V_ORGI
// 		CFieldMap("业绩稳定幅度","业绩稳定幅度",JN_CS_S_YJWD_FD,-1,CS_S_YJWD_FD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_RED,"0000",0),    //V_ORGI
// 
// };
//20130913 替换
CFieldMap s_Array_FieldMap[] =
{
	CFieldMap("","",JN_BLANK,-1,S_EndDate,H_EndDate,F_EndDate,-1,0,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0),    //空白

	//2013-6-18 Wang.YJ 基本面分组
	CFieldMap("","",JN_CAL_YJ,-1,-1,-1,-1,-1,1,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0),
	CFieldMap("","",JN_CAL_GB,-1,-1,-1,-1,-1,1,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0), 
	CFieldMap("","",JN_CAL_ZYZB,-1,-1,-1,-1,-1,1,V_BLANK,0,"",0,ECT_DEFAULT,"0000",0),

	CFieldMap("","",JN_CAL_B_FXCS,-1,-1,-1,-1,-1,0,V_ORGI,4,"次",0,ECT_DEFAULT,"0000",0),    //债券付息次数
	CFieldMap("最新送转","最新送转",JN_CAL_ZZ_BL,-1,S_ZZ_BL,-1,-1,-1,0,V_FHSP,4,"股",0,ECT_RED,"3000",0),    //V_FHSP

	//2013-6-17 Wang.YJ 以下为新财文件中读取出的数据
	CFieldMap("报告日期","报告日期",JN_DATE,J_DATE,S_EndDate,H_EndDate,F_EndDate,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("总股本","总股本",JN_ZGB,J_ZGB,S_ZGB,H_ZGB,-1,-1,0,V_BIGINT,4,"股",0,ECT_VOL,"0000",4),    //V_BIGINT
	CFieldMap("国家股","国家股",JN_GJG,J_GJG,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("法人股","法人股",JN_FRG,J_FRG,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("全部A股","全部A股",JN_AG,-1,S_AG,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("限售A股","限售A股",JN_AG_XS,J_AG_XS,S_AG_XS,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("B 股","B 股",JN_BG,J_BG,S_BG,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("H 股","H 股",JN_HG,J_HG,S_HG,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("职工股","职工股",JN_ZGG,J_ZGG,-1,-1,-1,-1,0,V_BIGINT,0,"股",3,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("总资产","总资产",JN_ZZC,J_ZZC,S_ZZC,H_ZZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("流动资产","流动资产",JN_LDZC,J_LDZC,S_LDZC,H_LDZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("固定资产","固定资产",JN_GDZC,J_GDZC,S_GDZC,H_GDZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("无形资产","无形资产",JN_WXZC,J_WXZC,S_WXZC,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("长期投资","长期投资",JN_CQTZ,J_CQTZ,S_CQTZ,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("流动负债","流动负债",JN_LDFZ,J_LDFZ,S_LD_FZ,H_LDFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("长期负债","长期负债",JN_CQFZ,J_CQFZ,S_CQ_FZ,H_CQFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("资本公积金","资本公积金",JN_ZBGJJ,J_ZBGJJ,S_ZB_GJJ,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("每股公积金","每股公积金",JN_MGGJJ,J_MGGJJ,S_MGGJJ,-1,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"1000",-2),    //V_YUAN_10K
	CFieldMap("股东权益","股东权益",JN_GDQY,J_GDQY,S_GDQY,H_GDQY,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("主营收入","主营收入",JN_ZYSR,J_ZYSR,S_ZYSR,H_ZYSR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("主营利润","主营利润",JN_ZYLR,J_ZYLR,S_ZYLR,H_ZYLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("其他利润","其他利润",JN_QTLR,J_QTLR,S_QTLR,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("营业利润","营业利润",JN_YYLR,J_YYLR,S_YYLR,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("投资收益","投资收益",JN_TZSY,J_TZSY,S_TZSY,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("补贴收入","补贴收入",JN_BTSR,J_BTSR,S_BTSR,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("营业外收支","营业外收支",JN_YYWSZ,J_YYWSZ,S_YYWSZ,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("以前年度损益调整","以前年度损益调整",JN_NDSY,J_NDSY,-1,-1,-1,-1,0,V_BIGINT,0,"元",3,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("利润总额","利润总额",JN_LRZE,J_LRZE,S_LRZE,H_ZLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("净利润","净利润",JN_JLR,J_JLR,S_JLR,H_JLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("未分配利润","未分配利润",JN_WFPLR,J_WFPLR,S_F_WFPLR,-1,-1,-1,0,V_BIGINT,4,"元",4,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("每股未分配利润","每股未分配利润",JN_MGWFPLR,J_MGWFPLR,S_MGWFPLR,-1,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"1000",-2),    //V_YUAN_10K
	CFieldMap("每股收益","每股收益",JN_MGSY,J_MGSY,S_MGSY,H_MGSY,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"1000",-2),    //V_YUAN_10K
	CFieldMap("每股净资产","每股净资产",JN_MGJZC,J_MGJZC,S_MGJZC,H_MGJZC,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"1000",-2),    //V_YUAN_10K
	CFieldMap("调整后的每股净资产","调整后的每股净资产",JN_TZMGJZC,J_TZMGJZC,-1,-1,-1,-1,0,V_BLANK,0,"元",3,ECT_DEFAULT,"1000",-2),    //
	CFieldMap("股东权益比","股东权益比",JN_GDQYB,J_GDQYB,S_GDQYB,H_GDQYB,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",2),    //V_10KK
	CFieldMap("净资产收益率","净资产收益率",JN_JZCSYL,J_JZCSYL,S_JZCSYL,H_JZCSYL,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",2),    //V_10KK
	CFieldMap("限售B 股","限售B 股",JN_BG_XS,J_BG_XS,-1,-1,-1,-1,0,V_BIGINT,0,"股",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
	CFieldMap("股东户数","股东户数",JN_GDRS,J_GDRS,S_GD_HS,-1,-1,-1,0,V_ORGI,0,"户",0,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("户均持股","户均流通A股",JN_S_HJCG,J_RJCG,S_ZX_HJLTA,-1,-1,-1,0,V_ORGI,4,"股",0,ECT_DEFAULT,"1000",4),    //V_ORGI
	CFieldMap("证券类型","证券类型",JN_S_SecuType,-1,S_SecuType,H_SecuType,-1,-1,0,V_STOCK,0,"",3,ECT_DEFAULT,"0000",0),    //V_STOCK
	CFieldMap("是否金融类","是否金融类",JN_S_IsFinacial,-1,S_IsFinacial,-1,-1,-1,0,V_IsFinacial,0,"",3,ECT_DEFAULT,"0000",0),    //V_IsFinacial
	CFieldMap("交易日期","交易日期",JN_S_TradeDate,-1,S_TradeDate,-1,-1,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("市净率","市净率",JN_S_SJL,-1,S_SJL,H_SJL,-1,-1,0,V_10KK,4,"",2,ECT_AMT,"1100",0),    //V_10KK
	CFieldMap("每股经营现金流","每股经营现金流",JN_S_MGJYXJL,-1,S_MGJYXJL,-1,-1,-1,0,V_YUAN_10K,4,"元",4,ECT_ZDF,"1000",0),    //V_YUAN_10K
	CFieldMap("流通A股","流通A股",JN_S_LTGB,J_AG,S_LTGB,H_ZGB,-1,-1,0,V_BIGINT,4,"股",4,ECT_VOL,"0000",4),    //V_BIGINT
	CFieldMap("有效流通A股","有效流通A股",JN_S_YXLTGB,-1,S_YXLTGB,-1,-1,-1,0,V_BIGINT,0,"股",0,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("户均持股环比","户均流通A股环比",JN_S_HJCGTB,-1,S_HJ_LTA_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("股东户数环比","股东户数环比",JN_S_GDHSB,-1,S_AG_HS_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("主营收入同比","主营收入同比",JN_S_ZYSRTB,-1,S_ZYSRTB,H_ZYSRTB,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("主营利润同比","主营利润同比",JN_S_ZYLRTB,-1,S_ZYLRTB,H_ZYLRTB,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("净利润同比","净利润同比",JN_S_JLRTB,-1,S_JLRTB,H_JLRTB,F_JLR_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1020",0),    //V_10KK
	CFieldMap("销售毛利率","销售毛利率",JN_S_XSMLL,-1,S_XSMLL,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("净利息收益率","净利息收益率",JN_S_JLXSYL,-1,S_JLXSYL,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("资产负债率","资产负债率",JN_S_ZCFZL,-1,S_ZCFZL,H_ZCFZL,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("股息率","股息率",JN_S_GuXiL,-1,S_GuXiL,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("机构持股家数","机构持股家数",JN_S_JGCGJS,-1,S_JGCGJS,-1,-1,-1,0,V_GE,0,"家",0,ECT_DEFAULT,"1000",0),    //V_GE
	CFieldMap("基金持股家数","基金持股家数",JN_S_JJCGJS,-1,S_JJCGJS,-1,-1,-1,0,V_GE,0,"家",0,ECT_DEFAULT,"1000",0),    //V_GE
	CFieldMap("机构持股占有效流通比","机构持股占有效流通比",JN_S_JGCGZB,-1,S_JGCGZB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("基金持股占有效流通比","基金持股占有效流通比",JN_S_JJN_ZYXLTB,-1,S_JJ_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("QFII持股占有效流通比","QFII持股占有效流通比",JN_S_QF_ZYXLTB,-1,S_QF_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("保险持股占有效流通比","保险持股占有效流通比",JN_S_BX_ZYXLTB,-1,S_BX_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("券商持股占有效流通比","券商持股占有效流通比",JN_S_QS_ZYXLTB,-1,S_QS_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("信托持股占有效流通比","信托持股占有效流通比",JN_S_XT_ZYXLTB,-1,S_XT_ZYXLTB,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	CFieldMap("经营活动现金流","经营活动现金流",JN_S_JYHDXJL,-1,S_JYHDXJL,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("预付账款","预付账款",JN_S_YFZK,-1,S_YFZK,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("应收账款","应收账款",JN_S_YSZK,-1,S_YSZK,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("现金及央行存款","现金及存放中央银行款项",JN_S_XJYHKX,-1,S_XJYHKX,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("发放贷款及垫款","发放贷款及垫款",JN_S_FFDKDK,-1,S_FFDKDK,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("同业及其他存款","同业及其他金融机构存放款项",JN_S_TYCFKX,-1,S_TYCFKX,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("吸收存款","吸收存款",JN_S_XSKX,-1,S_XSKX,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("上市日期","上市日期",JN_S_SSRQ,-1,S_SSRQ,H_SSRQ,-1,B_SSRQ,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("股票简称","股票简称",JN_CS_Abbr,-1,CS_Abbr,-1,CS_Abbr,CS_Abbr,1,V_ORGI,0,"",3,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("最新分红","最新分红",JN_CS_ZXFH,-1,CS_ZXFH,-1,CS_ZXFH,-1,1,V_ORGI,0,"",3,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("所属行业","所属行业",JN_CS_S_Bkname,-1,CS_S_BKname,-1,-1,-1,1,V_ORGI,0,"",3,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("所属概念","所属概念",JN_CS_S_Concept,-1,CS_S_Concept,-1,-1,-1,1,V_ORGI,0,"",3,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("总负债","总负债",JN_H_ZFZ,-1,-1,H_ZFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"0000",0),    //V_BIGINT
	CFieldMap("成立日期","成立日期",JN_F_CLRQ,-1,-1,-1,F_CLRQ,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("基金类型","基金类型",JN_F_FundType,-1,-1,-1,F_FundType,-1,0,V_FUNDTYPE,0,"",3,ECT_AMT,"0000",0),    //V_FUNDTYPE
	CFieldMap("投资风格","投资风格",JN_F_TZFG,-1,-1,-1,F_TZFG,-1,0,V_TZFG,0,"",3,ECT_AMT,"0000",0),    //V_TZFG
	CFieldMap("单位净值","单位净值",JN_F_DWJZ,-1,-1,-1,F_DWJZ,-1,0,V_YUAN_10K,4,"元",3,ECT_AMT,"0000",0),    //V_YUAN_10K
	CFieldMap("累计净值","累计净值",JN_F_LJJZ,-1,-1,-1,F_LJJZ,-1,0,V_YUAN_10K,4,"元",3,ECT_AMT,"0000",0),    //V_YUAN_10K
	CFieldMap("单位净值增长率","单位净值增长率",JN_F_DWJZ_ZZL,-1,-1,-1,F_DWJZ_ZZL,-1,0,V_10KK,4,"%",2,ECT_AMT,"0000",0),    //V_10KK
	CFieldMap("累计净值增长率","累计净值增长率",JN_F_LJJZ_ZZL,-1,-1,-1,F_LJJZ_ZZL,-1,0,V_10KK,4,"%",2,ECT_AMT,"0000",0),    //V_10KK
	CFieldMap("基金份额","基金份额",JN_F_JJFE,-1,-1,-1,F_JJFE_ZX,-1,0,V_BIGINT,4,"份",2,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("每万份单位收益","每万份单位收益",JN_F_WFDWSY,-1,-1,-1,F_WFDWSY,-1,0,V_YUAN_10K,4,"元",3,ECT_DEFAULT,"0000",0),    //V_YUAN_10K
	CFieldMap("7日年化收益率(%)","7日年化收益率(%)",JN_F_7NHSYL,-1,-1,-1,F_7NHSYL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
	CFieldMap("万份收益增长率(%)","万份收益增长率(%)",JN_F_WFSYZZL,-1,-1,-1,F_WFSYZZL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
	CFieldMap("7日年化收益增长率(%)","7日年化收益增长率(%)",JN_F_7NHSYZZL,-1,-1,-1,F_7NHSYZZL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
	CFieldMap("权益投资占比","权益类投资占比",JN_F_QY_TZZB,-1,-1,-1,F_QY_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("股票投资占比","股票投资占比",JN_F_GP_TZZB,-1,-1,-1,F_GP_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("存托凭证占比","存托凭证占比",JN_F_CT_TZZB,-1,-1,-1,F_CT_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("基金投资占比","基金投资占比",JN_F_JJN_TZZB,-1,-1,-1,F_JJ_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("固定收益投资占比","固定收益类投资占比",JN_F_GD_TZZB,-1,-1,-1,F_GD_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("债券投资占比","债券投资占比",JN_F_ZQ_TZZB,-1,-1,-1,F_ZQ_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("资产支持证券占比","资产支持证券占比",JN_F_ZC_TZZB,-1,-1,-1,F_ZC_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("金融衍生品投资占比","金融衍生品投资占比",JN_F_JR_TZZB,-1,-1,-1,F_JR_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("买入返售金融资产占比","买入返售金融资产占比",JN_F_FS_TZZB,-1,-1,-1,F_FS_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("银行存款和备付金占比","银行存款和结算备付金占比",JN_F_CK_TZZB,-1,-1,-1,F_CK_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("货币市场工具占比","货币市场工具占比",JN_F_HB_TZZB,-1,-1,-1,F_HB_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("其他资产占比","其他资产占比",JN_F_QT_TZZB,-1,-1,-1,F_QT_TZZB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0010",0),    //V_10KK
	CFieldMap("财报报告日期","财报报告日期",JN_F_CBBGRQ,-1,-1,-1,F_CBBGRQ,-1,0,V_YYYYMMDD,0,"",3,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("总利润同比","总利润同比",JN_F_ZLR_TB,-1,-1,-1,F_ZLR_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
	CFieldMap("总资产同比","总资产同比",JN_F_ZZC_TB,-1,-1,-1,F_ZZC_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
	CFieldMap("总负债同比","总负债同比",JN_F_ZFZ_TB,-1,-1,-1,F_ZFZ_TB,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0020",0),    //V_10KK
	CFieldMap("存续起始日","存续起始日",JN_F_CXQSR,-1,-1,-1,F_CXQSR,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("存续终止日","存续终止日",JN_F_CXZZQ,-1,-1,-1,F_CXZZQ,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("据到期日","据到期日",JN_F_JDQR,-1,-1,-1,F_JDQR,-1,0,V_TIAN,0,"",0,ECT_DEFAULT,"0000",0),    //V_TIAN
	CFieldMap("存续期限","存续期限",JN_F_CXQX,-1,-1,-1,F_CXQX,-1,0,V_DATABASE,0,"",0,ECT_DEFAULT,"0000",0),    //V_DATABASE
	CFieldMap("托管费率(%)","托管费率(%)",JN_F_TGFL,-1,-1,-1,F_TGFL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
	CFieldMap("管理费率(%)","管理费率(%)",JN_F_GLFL,-1,-1,-1,F_GLFL,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"0000",0),    //V_10KK
	CFieldMap("基金市场","基金市场",JN_F_JJSC,-1,-1,-1,F_JJSC,-1,0,V_ORGI,0,"",0,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("基金名称","基金名称",JN_CS_Name,-1,-1,-1,CS_Name,CS_Name,1,V_ORGI,0,"",0,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("基金经理","基金经理",JN_CS_F_JLR,-1,-1,-1,CS_F_JLR,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("基金管理人","基金管理人",JN_CS_F_GLR,-1,-1,-1,CS_F_GLR,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("基金托管人","基金托管人",JN_CS_F_TGR,-1,-1,-1,CS_F_TGR,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("业绩比较基准","业绩比较基准",JN_CS_F_YJBJJZ,-1,-1,-1,CS_F_YJBJJZ,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("债券期限","债券期限",JN_B_ZQQX,-1,-1,-1,-1,B_ZQQX,0,V_DATABASE,4,"月",0,ECT_DEFAULT,"0000",0),    //V_DATABASE
	CFieldMap("付息频率","付息频率",JN_B_FXPL,-1,-1,-1,-1,B_FXPL,0,V_DATABASE,0,"",0,ECT_DEFAULT,"0000",0),    //V_DATABASE
	CFieldMap("发行量","发行量",JN_B_GZ_FXL,-1,-1,-1,-1,B_FX_FXL,0,V_BIGINT,0,"元",0,ECT_VOL,"0000",0),    //V_BIGINT
	CFieldMap("发行价格","发行价格",JN_B_GZ_FXJG,-1,-1,-1,-1,B_FX_FXJG,0,V_YUAN_10K,4,"元",0,ECT_AMT,"0000",0),    //V_YUAN_10K
	CFieldMap("单位面值","单位面值",JN_B_GZ_DWMZ,-1,-1,-1,-1,B_FX_DWMZ,0,V_YUAN_10K,4,"元",0,ECT_AMT,"0000",0),    //V_YUAN_10K
	CFieldMap("转股价格","转股价格",JN_B_ZGJG,-1,-1,-1,-1,B_ZGJG,0,V_YUAN_10K,4,"元",2,ECT_AMT,"0000",0),    //V_YUAN_10K
	CFieldMap("票面利率(%)","票面利率(%)",JN_B_PMLL,-1,-1,-1,-1,B_PMLL,0,V_10KK,4,"%",2,ECT_AMT,"0000",0),    //V_10KK
	CFieldMap("发行日期","发行日期",JN_B_FXRQ,-1,-1,-1,-1,B_FXRQ,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("起息日","起息日",JN_B_QXR,-1,-1,-1,-1,B_QXR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
	CFieldMap("到期日","到期日",JN_B_DQR,-1,-1,-1,-1,B_DQR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
	CFieldMap("兑付日","兑付日",JN_B_DFR,-1,-1,-1,-1,B_DFR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
	CFieldMap("转股截止日","转股截止日",JN_B_ZGJZR,-1,-1,-1,-1,B_ZGJZR,0,V_YYYYMMDD,0,"",0,ECT_AMT,"0000",0),    //V_YYYYMMDD
	CFieldMap("债券剩余交易日","债券剩余交易日",JN_B_ZQSYJYR,-1,-1,-1,-1,B_ZQSYJYR,0,V_TIAN,0,"",0,ECT_DEFAULT,"0000",0),    //V_TIAN
	CFieldMap("债券种类名称","债券种类名称",JN_CS_B_ZLMC,-1,-1,-1,-1,CS_B_ZLMC,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("利率类型","利率类型",JN_CS_B_LLLX,-1,-1,-1,-1,CS_B_LLLX,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("计利方式","计利方式",JN_CS_B_JXFS,-1,-1,-1,-1,CS_B_JXFS,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("付息方式","付息方式",JN_CS_B_FXFS,-1,-1,-1,-1,CS_B_FXFS,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("发行人","发行人",JN_CS_B_FXR,-1,-1,-1,-1,CS_B_FXR,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("年度计息基准","年度计息基准",JN_CS_B_NDJXJZ,-1,-1,-1,-1,CS_B_NDJXJZ,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("52周涨幅","52周涨幅",JN_S_52Z_ZF,-1,S_52Z_ZF,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_ZDF,"0000",0),    //V_ORGI
	CFieldMap("分红报告期","分红报告期",JN_S_FH_BGQ,-1,S_FH_BGQ,-1,-1,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("派息比例(港币)","派息比例(港币)",JN_S_PXBL_HKD,-1,S_PXBL_HKD,-1,-1,-1,0,V_ORGI,4,"",2,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("派息比例(美元)","派息比例(美元)",JN_S_PXBL_USD,-1,S_PXBL_USD,-1,-1,-1,0,V_ORGI,4,"",2,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("最新派息","最新派息",JN_S_PXBL_RMB,-1,S_PXBL_RMB,-1,-1,-1,0,V_FHSP,4,"元",2,ECT_RED,"3000",0),    //V_FHSP
	CFieldMap("转增比例","转增比例",JN_S_ZZ_BL,-1,S_ZZ_BL,-1,-1,-1,0,V_FHSP,4,"股",0,ECT_RED,"3000",0),    //V_FHSP
	CFieldMap("送股比例","送股比例",JN_S_SG_BL,-1,S_SG_BL,-1,-1,-1,0,V_FHSP,4,"股",0,ECT_RED,"3000",0),    //V_FHSP
	CFieldMap("方案进度编码","方案进度编码",JN_S_FAJD_BM,-1,S_FAJD_BM,-1,-1,-1,0,V_ORGI,0,"",0,ECT_DEFAULT,"0000",0),    //V_ORGI
	CFieldMap("业绩预告报告期","业绩预告报告期",JN_S_YJYG_BGQ,-1,S_YJYG_BGQ,-1,-1,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("财务报告期","财务报告期",JN_S_CW_BGQ,-1,S_CW_BGQ,-1,-1,-1,0,V_YYYYMMDD,0,"",0,ECT_DEFAULT,"0000",0),    //V_YYYYMMDD
	CFieldMap("营业利润","营业利润",JN_S_YYLR,J_YYLR,S_YYLR,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("其他利润","其他利润",JN_S_QTLR,J_QTLR,S_QTLR,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("营业外收支","营业外收支",JN_S_YYWSZ,J_YYWSZ,S_YYWSZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("补贴收入","补贴收入",JN_S_BTSR,J_BTSR,S_BTSR,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("投资收益","投资收益",JN_S_TZSY,J_TZSY,S_TZSY,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("损益调整","损益调整",JN_S_SYTZ,J_NDSY,S_SYTZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("利润总额","利润总额",JN_S_LRZE,J_LRZE,S_LRZE,H_ZLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("净利润","净利润",JN_S_JLR,J_JLR,S_JLR,H_JLR,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("总资产","总资产",JN_S_ZZC,J_ZZC,S_ZZC,H_ZZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("流动资产","流动资产",JN_S_LDZC,J_LDZC,S_LDZC,H_LDZC,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("无形资产","无形资产",JN_S_WXZC,J_WXZC,S_WXZC,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("固定资产","固定资产",JN_S_GDZC,J_GDZC,S_GDZC,H_GDZC,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("长期投资","长期投资",JN_S_CQTZ,J_CQTZ,S_CQTZ,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("应付账款","应付账款",JN_S_YFu_ZK,-1,S_YFu_ZK,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("预收账款","预收账款",JN_S_YS_ZK,-1,S_YS_ZK,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",0),    //V_BIGINT
	CFieldMap("流动负债","流动负债",JN_S_LD_FZ,J_LDFZ,S_LD_FZ,H_LDFZ,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("长期负债","长期负债",JN_S_CQ_FZ,J_CQFZ,S_CQ_FZ,H_CQFZ,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("净资产","净资产",JN_S_JZC,J_GDQY,S_JZC,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("股东权益","股东权益",JN_S_GDQY,J_GDQY,S_GDQY,H_GDQY,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("资本公积金","资本公积金",JN_S_ZB_GJJ,J_ZBGJJ,S_ZB_GJJ,-1,-1,-1,0,V_BIGINT,4,"元",3,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("未分配利润","未分配利润",JN_S_F_WFPLR,J_WFPLR,S_F_WFPLR,-1,-1,-1,0,V_BIGINT,4,"元",4,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("每股收益(年化)","每股收益(年化)",JN_S_NH_MGSY,-1,S_NH_MGSY,-1,-1,-1,0,V_ORGI,4,"元",4,ECT_ZDF,"1000",0),    //V_ORGI
	CFieldMap("净利润率","净利润率",JN_S_JLRL,-1,S_JLRL,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("市销率","市销率",JN_S_SXL,-1,S_SXL,-1,-1,-1,0,V_ORGI,4,"",1,ECT_AMT,"1000",0),    //V_ORGI
	CFieldMap("主营收入环比","主营收入环比",JN_S_ZYSR_HB,-1,S_ZYSR_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("主营利润环比","主营利润环比",JN_S_ZYLR_HB,-1,S_ZYLR_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("净利润环比","净利润环比",JN_S_JLR_HB,-1,S_JLR_HB,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("三年复合增长","三年复合增长",JN_S_Y3_FHZZ,-1,S_Y3_FHZZ,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("股东户数","股东户数",JN_S_GD_HS,J_GDRS,S_GD_HS,-1,-1,-1,0,V_ORGI,0,"户",3,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("基金持股","基金持股",JN_S_JJ_CG,-1,S_JJ_CG,-1,-1,-1,0,V_BIGINT,4,"股",0,ECT_VOL,"1000",0),    //V_BIGINT
	CFieldMap("基金持股市值","基金持股市值",JN_S_JJ_CG_SZ,-1,S_JJ_CG_SZ,-1,-1,-1,0,V_BIGINT,4,"元",0,ECT_VOL,"1000",0),    //V_BIGINT
	CFieldMap("机构持股占流通股比","机构持股占流通股比",JN_S_ZLTGB_JG,-1,S_ZLTGB_JG,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("基金持股占流通股比","基金持股占流通股比",JN_S_ZLTGB_JJ,-1,S_ZLTGB_JJ,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("QFII持股占流通股比","QFII持股占流通股比",JN_S_ZLTGB_QF2,-1,S_ZLTGB_QF2,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("保险持股占流通股比","保险持股占流通股比",JN_S_ZLTGB_BX,-1,S_ZLTGB_BX,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("券商持股占流通股比","券商持股占流通股比",JN_S_ZLTGB_QS,-1,S_ZLTGB_QS,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("信托持股占流通股比","信托持股占流通股比",JN_S_ZLTGB_XT,-1,S_ZLTGB_XT,-1,-1,-1,0,V_ORGI,4,"%",2,ECT_DEFAULT,"1000",0),    //V_ORGI
	CFieldMap("方案进度","方案进度",JN_CS_S_FAJD,-1,CS_S_FAJD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_AMT,"0000",0),    //V_ORGI
	CFieldMap("业绩预告","业绩预告",JN_CS_S_YJYG,-1,CS_S_YJYG,-1,-1,-1,1,V_ORGI,0,"",0,ECT_AMT,"4000",0),    //V_ORGI
	CFieldMap("业绩大增幅度","业绩大增幅度",JN_CS_S_YJDZ_FD,-1,CS_S_YJDZ_FD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_RED,"4000",0),    //V_ORGI
	CFieldMap("业绩增长幅度","业绩增长幅度",JN_CS_S_YJZZ_FD,-1,CS_S_YJZZ_FD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_RED,"4000",0),    //V_ORGI
	CFieldMap("业绩稳定幅度","业绩稳定幅度",JN_CS_S_YJWD_FD,-1,CS_S_YJWD_FD,-1,-1,-1,1,V_ORGI,0,"",0,ECT_RED,"4000",0),    //V_ORGI

	CFieldMap("近一年每股派息额","近一年每股派息额",JN_S_RY_MGPXE,-1,S_RY_MGPXE,-1,-1,-1,0,V_10KK,4,"%",2,ECT_DEFAULT,"1000",0),    //V_10KK
	//20151110 新增计算 市盈率TTM = 市值/TTM归属母公司净利润 | 市销率TTM = 市值/TTM营业收入
	CFieldMap("TTM净利润","TTM净利润",JN_S_TTM_JLR,-1,S_TTM_JLR,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT
	CFieldMap("TTM主营收入 ","TTM主营收入",JN_S_TTM_ZYSR,-1,S_TTM_ZYSR,-1,-1,-1,0,V_BIGINT,4,"元",2,ECT_DEFAULT,"1000",4),    //V_BIGINT

};

static CString s_Quator[] = {"①","②","③","④"};

BOOL CFPlatDataStock::ExtractOldJBM( CGoodsJBM & jbm )
{
	jbm.m_dwGoodsID = m_lGoodsID;
	jbm.m_dwUpdateDate = m_dwUpdateDate;
	jbm.m_dwUpdateTime = m_dwUpdateTime;
	DataToOld(jbm);

	jbm.m_dwDate = m_dwUpdateDate;

	return TRUE;
}

BOOL CFPlatDataStock::DataToOld( CGoodsJBM & jbm )
{	
	for (int i = 0;i < ARRSIZE(s_Array_FieldMap);i++)
	{
		int nIndex = -1;
		int IDOld = s_Array_FieldMap[i].m_IDOld;

		if (IDOld == -1)
			continue;
		
		CFieldMap & fm =  s_Array_FieldMap[i];

		if (m_cType == FPD_Type_Stock)
			nIndex =fm.m_IdxStock;
		else if (m_cType == FPD_Type_HKStock)
			nIndex = fm.m_idx_HKStock;
		else if (m_cType == FPD_Type_Fund)
			nIndex = fm.m_idx_Fund;
		else if (m_cType == FPD_Type_Bond)
			nIndex = fm.m_idxBond;

		if (nIndex != -1 && nIndex < PX_COL_TOTAL)
		{
			double f = double(m_pxValue[nIndex])/pow(10.0,fm.m_nOldPower);
			jbm.SetValue(INT64(f),IDOld);
		}
	}
	return TRUE;
}

void CFPlatDataStock::Clear()
{
	m_cType = FPD_Type_Stock;
	m_lGoodsID = 0;
	m_dwUpdateDate = 0;
	m_dwUpdateTime = 0;
	for(int k = 0; k < CS_COL_TOTAL; ++k)
		m_csValue[k] = "";
	memset(m_pxValue, 0, PX_COL_TOTAL*4);
}

CString CFPlatDataStock::Field2String(short fieldID)
{
	if (JN_CAL_B_FXCS==fieldID)
	{
		return "债券付息次数";
	}

	const CFieldMap * pFM = GetFieldMap(fieldID);
	if (pFM)
		return pFM->m_strShortName;
	else
		return "未定义";
}

short CFPlatDataStock::GetValueType(const short fieldID)
{
	short retVal = -1;
	CFieldMap fm;

	const CFieldMap * pFM = GetFieldMap(fieldID);
	if (pFM)
		retVal = pFM->m_nDrawType;

	return retVal;
}


XInt32 CFPlatDataStock::GetValue( short FID ) const
{
	UCHAR cValueType;
	SHORT index;
	if (FID == JN_CAL_B_FXCS)
	{
		//付息次数=债券期限/付息频率
		INT64 hVal1,hVal2;
		hVal1 = GetValue(JN_B_ZQQX);
		hVal2 = GetValue(JN_B_FXPL);
		if (hVal2)
			return hVal1 / hVal2;
		else
			return hVal1;
	}
	else if (FID == JN_CAL_ZZ_BL)
	{
		//分红送转 = 转增比例 + 送股比例
		return GetValue(JN_S_ZZ_BL) + GetValue(JN_S_SG_BL);
	}
	else if (FID2Local(FID,cValueType,index))
	{
		if (cValueType == EVT_PX && index < PX_COL_TOTAL)
		{
			return m_pxValue[index];
		}
		else if (cValueType == EVT_STRING)
		{
			ASSERT("CFPlatDataStock::GetValue is a string id");
		}
	}

	XInt32 retVal = 0;
	return retVal;
}

double CFPlatDataStock::GetFValue( const short FID ) const
{
	INT64 hValue = GetValue(FID);

	const CFieldMap * pFM = GetFieldMap(FID);
	if (pFM)
	{
		const CFieldMap& fm = *pFM;
		return hValue  /  static_cast<double>(pow(10.0,fm.m_nPower)); 
	}
	return 0;
}

CString CFPlatDataStock::GetStrValue(const short fieldID ) const
{
	if (JN_CAL_YJ == fieldID)
		return "业绩";
	else if (JN_CAL_GB == fieldID)
		return "股本";
	else if (JN_CAL_ZYZB == fieldID)
		return "主要指标";
	else if (JN_CAL_ZZ_BL == fieldID)
	{
		const CFieldMap * pFM = GetFieldMap(fieldID);
		if (pFM)
		{
			INT64 hVal1,hVal2;
			hVal1 = GetValue(JN_S_ZZ_BL)/INT64(pow(10.0,pFM->m_nPower));
			hVal2 = GetValue(JN_S_SG_BL)/INT64(pow(10.0,pFM->m_nPower));

			CString str,str1,str2;
			if (hVal1)
				str1.Format("转%lld",hVal1);
			if (hVal2)
				str2.Format("送%lld",hVal2);

			str = str1 + str2;
			str.Trim();
			UCHAR ucMonth;
			BOOL bDrawQuater = GetQuaterInfo(fieldID,ucMonth) != 0;
			if (str != "" && bDrawQuater && ucMonth < 4 )
			{
				str = "10" + str + "股";
				str += s_Quator[ucMonth];
			}
			return str;
		}
		else
			return "-";
	}

	UCHAR cValueType;
	SHORT index;

	CString strRet = "";
	CString strValue = "";

	const CFieldMap * pFM = GetFieldMap(fieldID);
	if (pFM)
	{
		const CFieldMap &fm =*pFM;
		cValueType = fm.m_cValueType;
		int ucDrawType = fm.m_nDrawType;
		if (cValueType == EVT_STRING && FID2Local(fieldID,cValueType,index))
		{
			if (index < CS_COL_TOTAL)
			{
				strValue = m_csValue[index];
				strValue.Trim();
			}
		}
		else if (cValueType == EVT_PX)
		{
			INT64 nVal =  GetValue(fieldID);
			strValue =  GetStrVal(fieldID,nVal);
		}			
	}

	strRet = strValue;

	//2013-6-20 Wang.YJ 最后加上季度，数值和字符型都可能加季度
	UCHAR ucMonth = 5;
	BOOL bDrawQuater = GetQuaterInfo(fieldID,ucMonth) != 0;
	if (strRet != "-" && bDrawQuater && ucMonth < 4 )
	{
		strRet +=" ";
		strRet += s_Quator[ucMonth];
	}

	return strRet;
}


CString CFPlatDataStock::GetFundType( const INT64 nID)
{
	if (nID == 3001)
	{
		return "封闭式基金";
	}
	else if (nID == 3002)
	{
		return "开放式基金";
	}
	else if (nID == 3003)
	{
		return "LOF";
	}
	else if (nID == 3004)
	{
		return "ETF";
	}
	else if (nID == 3007)
	{
		return "老基金";
	}
	else if (nID == 3008)
	{
		return "QDII";
	}
	else if (nID == 4001)
	{
		return "股票型";
	}
	else if (nID == 4002)
	{
		return "债券型";
	}
	else if (nID == 4003)
	{
		return "混合型";
	}
	else if (nID == 4004)
	{
		return "货币型";
	}
	else if (nID == 4005)
	{
		return "封闭式";
	}
	else if (nID == 4006)
	{
		return "基金型";
	}
	else
		return "未知基金";

}

CString CFPlatDataStock::GetFundTZFZ( const INT64  nID )
{
	if (nID == 5001)
	{
		return "股票式进取型";
	}
	else if (nID == 5002)
	{
		return "股票式价值型";
	}
	else if (nID == 5003)
	{
		return "股票式指数型";
	}
	else if (nID == 5004)
	{
		return "混合式偏股型";
	}
	else if (nID == 5005)
	{
		return "混合式偏债型";
	}
	else if (nID == 5006)
	{
		return "债券式收益型";
	}
	else if (nID == 5007)
	{
		return "债券式保守型";
	}
	else if (nID == 5008)
	{
		return "货币型";
	}
	else if (nID == 5009)
	{
		return "大盘";
	}
	else if (nID == 5010)
	{
		return "中小盘";
	}
	else if (nID == 5011)
	{
		return "基金型";
	}
	else
	{
		return "未定义";
	}
}

const EReportType CFPlatDataStock::GetReportType() const
{
	EReportType ert = ERT_NOTDEFINE;

	//	ERT_S_B,  //B股
	//	ERT_S_H, // 港股
	//ERT_S_A_JL, // A股金融
	//	ERT_S_A_NOTJL, // A股非金融
	if (FPD_Type_Stock == m_cType || FPD_Type_HKStock == m_cType)
	{
		INT64  hIsFin = GetValue(JN_S_IsFinacial);
		INT64 hS_SecuType = GetValue(JN_S_SecuType);
		if (hS_SecuType == 1002)
		{
			ert = ERT_S_B;
		}
		else if (hS_SecuType == 1009)
		{
			ert = ERT_S_H;
		}
		else if (hIsFin == 1)
		{
			ert = ERT_S_A_JL;
		}
		else if (hIsFin == 0)
		{
			ert = ERT_S_A_NOTJL;
		}
	}
	else if (FPD_Type_Fund == m_cType)
	{
		//	ERT_F_CLOSE, // 封闭式基金
		//	ERT_F_OPEN, //开放式基金
		//	ERT_F_MONEY, //货币基金,目前没有
		INT64  hF_FundType = GetValue(JN_F_FundType);
		if (3001 == hF_FundType)
			ert = ERT_F_CLOSE;
		else
			ert = ERT_F_OPEN;
	}
	else if (FPD_Type_Bond == m_cType)
	{
		//	ERT_B_GZ, // 国债
		//	ERT_B_KZZ, //可转债
		//	ERT_B_QYZ, // 企业债
		CString  strCS_B_ZLMC = GetStrValue(JN_CS_B_ZLMC);
		if ("国债" == strCS_B_ZLMC || "地方政府债" == strCS_B_ZLMC)
			ert = ERT_B_GZ;
		else if ("可转债" == strCS_B_ZLMC || "可分离可转债" == strCS_B_ZLMC)
			ert = ERT_B_KZZ;
		// if ("企业债" == strCS_B_ZLMC || "国债" == strCS_B_ZLMC || "国债" == strCS_B_ZLMC)
		else
			ert = ERT_B_QYZ;
	}

	return ert;
}

const DWORD CFPlatDataStock::GetQuaterInfo(const short fieldID,UCHAR& ucShort) const
{
	DWORD dwDate = 0;
	const CFieldMap * pFM = GetFieldMap(fieldID);
	if (pFM)
	{
		const CFieldMap& fm= *pFM;
		const CString & str = fm.m_strShowQuarter;
		if (str.GetLength() < 4)
			return FALSE;

		if (m_cType == FPD_Type_Stock)
		{
			ucShort = str[0];
		}
		else if (m_cType == FPD_Type_HKStock)
		{
			ucShort = str[1];
		}
		else if (m_cType == FPD_Type_Fund)
		{
			ucShort = str[2];
		}
		else if (m_cType == FPD_Type_Bond)
		{
			ucShort = str[3];
		}

		if (ucShort != '0')
		{
			if (ucShort == '1')
				dwDate = (DWORD)GetValue(JN_DATE);  // 报告日期
			else if (ucShort == '2')
				dwDate = (DWORD)GetValue(JN_F_CBBGRQ); //  财务报告期
			else if (ucShort == '3')
				dwDate = (DWORD)GetValue(JN_S_FH_BGQ);// 分红报告期
			else if (ucShort == '4')
				dwDate = (DWORD)GetValue(JN_S_YJYG_BGQ);// 业绩预告报告期

			ucShort =(UCHAR)(dwDate % 10000/100-1)/3;
		}
	}

	return dwDate;
}

CString CFPlatDataStock::GetStrVal( const short fieldID,INT64 hVal )
{
	CString strValue = "";
	const CFieldMap * pFM = GetFieldMap(fieldID);
	if (pFM)
	{
		const CFieldMap &fm =*pFM;
		int ucDrawType = fm.m_nDrawType;

		INT64 nVal = hVal;
		double fValue = (double)nVal;

		//2013-6-14 Wang.YJ 传输中放大的倍数，需要缩小回来。
		if (fm.m_nPower > 0) 
		{
			fValue = nVal  /  static_cast<double>(pow(10.0,fm.m_nPower));
			nVal = (INT64)fValue;
		}

		//2013-6-26 Wang.YJ 处理小数点
		if (fabs(fValue) < 0.000001)
			strValue = "-";
		else if (fm.m_ucDigit > 0)
		{
			CString strFormat;
			strFormat.Format("%%.%df",fm.m_ucDigit);
			strValue.Format(strFormat,fValue);
		}
		else //2013-6-26 Wang.YJ 原值显示
		{
			strValue.Format("%64Id",nVal);
		}

		//2013-6-26 Wang.YJ 零值不处理
		if (strValue != "-")
		{
			strValue.Trim();
			if (ucDrawType == V_STOCK)
			{
				switch (static_cast<int>(nVal))
				{	
				case 1001:
					strValue = "A股";
					break;
				case 1002:
					strValue = "B股";
					break;
				case 1009:
					strValue = "港股";
					break;
				default:
					strValue = "未定义";
					break;
				}
			}
			else if (ucDrawType == V_IsFinacial)
			{
				switch (static_cast<int>(nVal))
				{	
				case 1:
					strValue = "金融类";
					break;
				case 0:
					strValue = "非金融类";
					break;
				default:
					strValue = "未定义";
					break;
				}
			}
			else if (ucDrawType == V_FUNDTYPE)
				strValue = GetFundType(nVal);
			else if (ucDrawType == V_TZFG)
				strValue = GetFundTZFZ(nVal);
			else if (ucDrawType == V_TIAN)
			{
				if (nVal < 0)
					strValue = "已到期";
				else
					strValue.Format("%I64d天",nVal);
			}
			else if (ucDrawType == V_BIGINT)
			{
				//CString strOutValue,strText;
				//2013-6-14 Wang.YJ 转成万或者亿形式的值
				//g_Value2String(nVal, strOutValue, strText);
				//strValue =  strOutValue+ strText;
				strValue = "";
			}
			else if (ucDrawType == V_YYYYMMDD)
			{
				// 20130617
				DWORD dwDate = static_cast<DWORD>(nVal);
				strValue.Format("%d/%02d/%02d",dwDate/10000,dwDate % 10000/100,dwDate%100);
			}
			else if (ucDrawType == V_FHSP)
			{
				if (JN_S_PXBL_RMB == fieldID)
				{
					strValue = "10派" + strValue;
				}
				else if (JN_S_ZZ_BL == fieldID)
				{
					strValue = "10转" + strValue;
				}
				else if (JN_S_SG_BL == fieldID)
				{
					strValue = "10送" + strValue;
				}
			}
			else if (ucDrawType == V_BLANK)
			{
				strValue = "";
			}

			//2013-6-26 Wang.YJ 加上显示单位
			strValue += fm.m_strUnit;
		}
	}

	return strValue;
}

CFPlatData_All::CFPlatData_All()
{

}

CFPlatData_All::~CFPlatData_All()
{
	m_aFPD.RemoveAll();
}

void CFPlatData_All::Read()
{
	CString strFile = theApp.m_strSharePath+"Data/update/FPlat.dat";

	CBuffer buffer;
	buffer.m_bSingleRead = true;

	try
	{
		buffer.Initialize(1024*1024, true);
		buffer.FileRead(strFile);
		buffer.Read(&m_usVersion, 2);
		buffer.Read(&m_dwDate, 4);
		buffer.Read(&m_dwTime, 4);

		buffer.Read(&m_dwRows, 4);
		buffer.Read(&m_dwStrCols, 4);
		buffer.Read(&m_dwNumCols, 4);

		theApp.m_jbm.m_dwNumCols = m_dwNumCols;
		theApp.m_jbm.m_dwStrCols = m_dwStrCols;
		theApp.m_jbm.m_dwNewDate = m_dwDate;
		theApp.m_jbm.m_dwNewTime = m_dwTime;

		for (DWORD i=0; i<m_dwRows; i++)
		{
			CGoodsJBM jbm;

			jbm.m_fpd.Read(buffer, m_dwStrCols, m_dwNumCols);

			if (jbm.m_fpd.m_lGoodsID==0)
				continue;

			jbm.m_dwGoodsID = jbm.m_fpd.m_lGoodsID;

			if (jbm.m_fpd.m_lGoodsID == 1150153)
			{
				int ppos = 0;
				ppos = 11;
			}

			int nPos = theApp.m_jbm.m_aJBM.Find(jbm);
			if (nPos < 0)
			{
				jbm.m_fpd.ExtractOldJBM(jbm);

				if (jbm.m_fpd.m_cType == 'F')
					jbm.m_pxJBM[J_ZGB] = jbm.m_pxJBM[J_AG] = jbm.m_fpd.GetValue(JN_F_JJFE)/10000;

				theApp.m_jbm.m_aJBM.Add(jbm);
				
			}
			else
			{
				CGoodsJBM &jbmRef = theApp.m_jbm.m_aJBM[nPos];

				jbmRef.m_fpd = jbm.m_fpd;

				if (jbm.m_fpd.m_cType == 'F')
					jbmRef.m_pxJBM[J_ZGB] = jbmRef.m_pxJBM[J_AG] = jbm.m_fpd.GetValue(JN_F_JJFE)/10000;
			}

#ifdef DEBUG
			if (jbm.m_dwGoodsID >= 600000 && jbm.m_dwGoodsID <= 600010)
			{
				//				OutputDebugString("\r\n");
				CString str;
				str.Format("%d ",jbm.m_dwGoodsID);
				for (int m = 0;m < 38;m++)
				{
					CString strTemp ;
					strTemp.Format( "[%d]%I64d\t",m,(INT64)jbm.m_pxJBM[m] );
					str += strTemp;
				}
				LOG_DEBUG( str.GetData() );
				//				OutputDebugString(str);
			}
#endif
		}
	}
	catch(int)
	{
        LOG_ERR("Read FPlat.dat ERROR!");
	}
}

void CFPlatData_All::Write()
{

}

BOOL CFPlatData_All::Get( DWORD dwGoodsID, CFPlatDataStock& fpds)
{
	int nSize = int(m_aFPD.GetSize());
	CFPlatDataStock gjFind;

	gjFind.m_lGoodsID = dwGoodsID;
	int nFind = m_aFPD.Find(gjFind);
	if (nFind>=0)
	{
		fpds = m_aFPD[nFind];
		return TRUE;
	}
	else
		fpds.Clear();

	return FALSE;
}

const CFieldMap * CFPlatDataStock::GetFieldMap(const short jbmID)
{
	CFieldMap * pRetVal = NULL;
	static BOOL bInit = FALSE;
	if (bInit == FALSE)
	{
		bInit = TRUE;
		for (int i = 0;i < ARRSIZE(s_map_FieldMap);i++)
			s_map_FieldMap[i] = CFieldMap();

		for (int i = 0;i < ARRSIZE(s_Array_FieldMap);i++)
		{
			CFieldMap & fm1 = s_Array_FieldMap[i];
			s_map_FieldMap[fm1.m_IDNew - JN_BEGIN] = fm1;
		}
	}

	int nIndex = jbmID - JN_BEGIN;
	if (nIndex >=0)
	{
		pRetVal  = &s_map_FieldMap[nIndex];
		if (pRetVal->m_IDNew == -1)
			pRetVal = NULL;
	}

	return pRetVal;
}

INT64 CFPlatDataStock::GetValueForCompare( short FID ) const
{
	BOOL bAddQuater = FALSE;

	//分红送转 需要把最新的季度放在最前
	if (FID == JN_CAL_ZZ_BL || FID == JN_S_PXBL_RMB)
	{
		UCHAR ucQ;
		DWORD dwDate = 0;

		INT64 xVal = GetValue(FID);
		if (xVal != 0)
		{
			xVal /= 100;
			dwDate = GetQuaterInfo(FID,ucQ);
			if (dwDate != 0)
			{
				dwDate = dwDate%10000000 * 1000;
			}
			xVal += dwDate;
		}
		return xVal;
	}
	else
		return GetValue(FID);
}
