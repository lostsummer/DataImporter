#ifndef MYDEFINE_H
#define MYDEFINE_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#define _DATA_Check_				1

//////////////////////////////////////////////////////////////////////////
//{DS<-->DM
#define _DATA_L2DM_Login_			10007
#define _DATA_L2DM_Value_			10145
#define _DATA_L2DM_Minute_			10243
#define _DATA_L2DM_Bargain_			10343

#define _DATA_L2DM_Trade_			10443
#define _DATA_L2DM_OrderCancel_		10501
#define _DATA_L2DM_IndustryRank_	10601
#define _DATA_L2DM_PartOrder_		10743
#define _DATA_L2DM_Group_			10805
#define _DATA_L2DM_Alarm_			10901
#define _DATA_L2DM_StatCount_		11001
#define _DATA_L2DM_Bulletin_		11101
#define _DATA_L2DM_Order_			11229

#define _DATA_L2DM_Pool1_			12009
#define _DATA_L2DM_GroupStat_		12201 //闪电版 新增
#define _DATA_L2DM_StaticData_		12403 //闪电版 新增

#define _DATA_L2DM_GoodCheckData_	12503 //DS 新增 日线数据校验 当日校验和下载
#define _DATA_L2DM_DayCheckData_	12601 //DS 新增 日线数据校验 校验文件下载
#define _DATA_L2DM_DayData_			12701 //DS 新增 日线数据校验 日线下载

#define _DATA_L2DM_SysAlarm_		13001
#define _DATA_L2DM_CPX_				13103
#define _DATA_L2DM_IndGoods_		13201
//}

///20120319 sjp
#define _DATA_L2DM_Value5_			15005
#define _DATA_L2DM_Static5_			15201
#define _DATA_L2DM_Minute5_			15305
#define _DATA_L2DM_Alarm5_			15401
#define _DATA_L2DM_Bargain5_		15505
#define _DATA_L2DM_Group5_			15601
#define _DATA_L2DM_GroupStat5_		15701
#define _DATA_L2DM_Trade5_			15801
#define _DATA_L2DM_PartOrder5_		15901
#define _DATA_L2DM_StatCount5_		16001

#define _DATA_L2DM_ExpValue_		16401

#define _DATA_L2DM_Matrix_			16701
///20120319 sjp
#define _DATA_L2DM_Value6_			17001
#define _DATA_L2DM_Minute6_			17101
#define _DATA_L2DM_Bargain6_		17201
//////////////////////////////////////////////////////////////////////////
//{DS->LB
#define _DATA_LB_Login_				32001//登录LB
//}

//////////////////////////////////////////////////////////////////////////
//{Client<-->DS
#define _DATA_MDS_Quote_			211//25//23//21//19//17//15//13//11//09//07//05//03 盘口,分时,分笔,K线 新增(延时港股不刷新)新增一个小版本
#define _DATA_MDS_His_				212//25//23//21//19//17//15//13
#define _DATA_MDS_Cur_				213//19//17//15//13//11//09//07
#define _DATA_MDS_Grid_				214//37//35//33//29//27//25//23//21//19//17//15//13//11(Y版)
#define _DATA_MDS_Bargain_			215
#define _DATA_MDS_JBM_				216
#define _DATA_MDS_DPTJ_				217

//add by liuchengzhu 20100119
#define _DATA_MDS_L2_Quote_			218	//05//03//21801
#define _DATA_MDS_OrderCounts_		219	//21901
//end add by liuchengzhu 20100119

#define _DATA_MDS_Grid_L2_			220//11//22001

#define _DATA_MDS_GROUP_ASSOCIATE_	230//23001

#define _DATA_MDS_UPDATE_			240//24001

#define _DATA_MDS_FindGoods_		250//25007//25005

#define _DATA_MDS_DAPAN_		    251//25101首页大盘数据

#define _DATA_MDS_ZongHe_		    252//01综合包//03新增版块
#define _DATA_MDS_ZJDX_				253//01资金动向
#define _DATA_MDS_DYNAGRID_			254//07//02//01 GRID动态列表
#define _DATA_MDS_QUICK_SelGoods_	255//01 快速选股
#define _DATA_MDS_PLATFORM_CWX_	    256//01 平台版仓位显示
 
#define _DATA_MDS_Tip_				8//11//809//805
#define _DATA_MDS_InfoTitle_		9//909//907
#define _DATA_MDS_InfoText_			10//1003 信息地雷
#define _DATA_MDS_Ask_				11//1101
//年卡缴费
#define _DATA_MDS_Card_				12//1203//1201
//到期日
#define _DATA_MDS_EndDate_			13//1305//1303//1301

#define _DATA_MDS_MainInfo_			15//1501
#define _DATA_MDS_AllInfoTitle_		16//1601

#define _DATA_MDS_Referal_			19//1901//1903
#define _DATA_MDS_Query_Loyalty_Points_			20 // 2001
#define _DATA_MDS_Exchange_Loyalty_Points_		21 // 2101
#define _DATA_MDS_Query_Action_Types_			22 // 2201
#define _DATA_MDS_Query_Scroll_Info_			23 // 2301

#define _DATA_MDS_Query_Trader_Info_				24 //2403//2401
#define _DATA_MDS_Query_TraderAreas_Info_			25 //2503//2501
#define _DATA_MDS_Query_TraderBranches_Info_		26 //2603//2601
#define _DATA_MDS_Query_TraderWapAddr_Info_			27 // 2701
//修改密码
#define _DATA_MDS_Change_PWD_						28//2803//2801
//查询自选股
#define	_DATA_MDS_Download_ReferalGoods_Info_		29//2907//2905//2901
//更新自选股
#define	_DATA_MDS_Update_ReferalGoods_Info_			30//3007//3005//3001

#define _DATA_MDS_Query_Screen_Logo_				31//3101

#define _DATA_MDS_HisBK_							32//3201

#define _DATA_MDS_Diagnosis_						35//3501

//查询自选股 平台版
#define	_DATA_MDS_Download_ReferalGoods_Info_Platform_		37//3701
//更新自选股 平台版
#define	_DATA_MDS_Update_ReferalGoods_Info_Platform_		38//3803, 区分追加和插入//3801

#define _DATA_MDS_SetAlarm_							41//01

//Y版设置取消推送信息
#define  _DATA_MDS_SetCancel_PushInfo_				42//01
//}
// weibo,qq绑定手机号 [10/9/2012 frantqian]
#define  _DATA_MDS_BINDPHONE_WEIBOANDQQ_				43//01

#define _DATA_DS_Suggestion_					51//01

#define  _DATA_DS_TradeInfo_					52//5201 交易信息
#define  _DATA_DS_Zijin							53//5301 资金
#define _DATA_MDS_Cur_5_						54//5401 五日分时
#define _DATA_ZYHG_FULL_                        55//5501 质押回购所有产品(湘财)
#define _DATA_ZYHG_BEST_                        56//5601 质押回购精选产品列表 (湘财)

//登录包
#define  _DATA_MDS_LOGIN_                       1
//////////////////////////////////////////////////////////////////////////
//{DS<-->FM
//add by liuchengzhu 20091126
#define _DATA_FM_Check_						1
//add by liuchengzhu 20100416

#define _DATA_FM_Login_						101
#define _DATA_FM_UserLogin_					207
#define _DATA_FM_UserLogin_Ex_				211
#define _DATA_FM_Card_						301
#define _DATA_FM_EndDate_					402//401 // 新版本使用，老的DS发401，灰度发布使用 [10/31/2012 frantqian]
#define _DATA_FM_EndDate_EX_				403//add 20131128 新DS兼容老的fee
#define _DATA_FM_Referal_					501
#define _DATA_FM_Referal_Set_				503
#define _DATA_FM_Query_Loyalty_Points_		701
#define _DATA_FM_Exchange_Loyalty_Points_	601
#define _DATA_FM_Query_Action_Types_		801
#define _DATA_FM_CHANGE_PWD_				901
#define _DATA_FM_CHANGE_PWD_EX_				902//add 20131128 新DS兼容老的fee
#define _DATA_FM_DOWNLOAD_REFERAL_GOODS_	1002//1001// 新版本使用，老的DS还是发1001，等外网运行稳定后再改回老的 [10/29/2012 frantqian]
#define _DATA_FM_DOWNLOAD_REFERAL_GOODS_EX_	1003//add 20131128 新DS兼容老的fee
#define _DATA_FM_DOWNLOAD_REFERAL_GOODS_PLATFORM_	1004
#define _DATA_FM_UPDATE_REFERAL_GOODS_		1102//1101// 新版本使用，老的DS还是发1102，等外网运行稳定后再改回老的 [10/29/2012 frantqian]
#define _DATA_FM_UPDATE_REFERAL_GOODS_EX_	1103//add 20131128 新DS兼容老的fee
#define _DATA_FM_UPDATE_REFERAL_GOODS_PLATFORM_	1104
#define _DATA_FM_UserLogout_				1201

#define _DATA_FM_PushInfo_					1401
#define _DATA_FM_BINDPHONE_WEIBOANDQQ_		1801 // 微博，QQ绑定手机号 [9/29/2012 frantqian]
#define	_DATA_FM_UserLogin_Platform_		1315
//}

#define _DATA_FM_SetAlarm_					2101
#define _DATA_FM_SetAlarmNew_				2201// 114预警修改 [12/26/2012 frantqian]
#define  _DATA_FM_Suggestion_				2301// 114建议 [12/26/2012 frantqian]



//////////////////////////////////////////////////////////////////////////
//Other Const VAR
#define MEMO_UPDATE			102
#define MEMO_USER_MANUL		104
#define MEMO_HELP			105
#define MEMO_SMS_FEE		201
#define MEMO_CAR_FEE		202
#define MEMO_MAIL_FEE		203
#define MEMO_BANK_FEE		204
#define MEMO_NET_FEE		205
#define MEMO_LOYALTY_RULE   206
#define MEMO_SMS_CHANNEL1   207
#define MEMO_SMS_CHANNEL2   208
#define MEMO_CPX			209
#define MEMO_TIP			210
#define MEMO_DPTJ			211
#define MEMO_MJDJ			212

#define MEMO_AMT_TICK		213 //分时净流
#define MEMO_BY_TICK		214 //分时博弈
#define MEMO_AMT_K			215 //K线资金流变
#define MEMO_BY_K			216 //K线资金博弈
#define MEMO_BL_K			217 //K线大单比率
#define MEMO_ORDER_COUNT    218 //龙虎看盘
#define MEMO_QUOTEL2		219 //买卖十档
#define MEMO_GRIDL2		    220 //主力统计排名
#define MEMO_CJZJ		    221 //超级资金
#define MEMO_CMJS		    222 //筹码聚散
//////////////////////////////////////////////////////////////////////////
// 114资讯使用 [1/8/2013 frantqian]
#define MEMO_GRIDL2_0		220 //板块主力统计排名
#define MEMO_GRIDL2_1		223 //个股主力统计排名
#define MEMO_GRIDL2_2		224 //主力强买主力统计排名

#define _STORE_114					 114
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
//{
#define EM_1						 0//益盟
#define EM_2						 264//益盟

#define OVI_STORE					 148//OVI
#define CT_STORE					 303//电信

#define MOTO						 483//MOTO
#define ZTE							 142//中兴 
#define SAMSUNG						 537//三星
#define HISENSE                      406//海信
//}

//////////////////////////////////////////////////////////////////////////
//{Client<-->DS : Special date type
#define _OVIStore_LimitData_		 99//01
#define _OVIStore_InfoText_			 98//01
#define _OVIStore_Query_Scroll_Info_ 97//01
#define _OVIStore_InfoTitle_		 96//01
#define _OVIStore_MainInfo_			 95//01
#define _OVIStore_AllInfoTitle_		 94//01
//}
#define DYNAMIC_GROUP_MAX			100

#endif


