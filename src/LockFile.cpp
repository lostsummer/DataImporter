#include <errno.h>

#include "LockFile.h"
#include "AppLogger.h"

#define	PIDFILE	"/tmp/MDS.pid"
#define	FILE_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

static int lock_reg(int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
	struct flock lock;

	lock.l_type = type;		/* F_RDLCK, F_WRLCK, F_UNLCK */
	lock.l_start = offset;	/* byte offset, relative to l_whence */
	lock.l_whence = whence;	/* SEEK_SET, SEEK_CUR, SEEK_END */
	lock.l_len = len;		/* #bytes (0 means to EOF) */

	return(fcntl(fd, cmd, &lock));
}

#define    read_lock(fd, offset, whence, len) \
	lock_reg(fd, F_SETLK, F_RDLCK, offset, whence, len)
#define    readw_lock(fd, offset, whence, len) \
	lock_reg(fd, F_SETLKW, F_RDLCK, offset, whence, len)
#define    write_lock(fd, offset, whence, len) \
	lock_reg(fd, F_SETLK, F_WRLCK, offset, whence, len)
#define    writew_lock(fd, offset, whence, len) \
	lock_reg(fd, F_SETLKW, F_WRLCK, offset, whence, len)
#define    un_lock(fd, offset, whence, len) \
	lock_reg(fd, F_SETLK, F_UNLCK, offset, whence, len)

CLockFile::CLockFile()
{
}

CLockFile::~CLockFile()
{
}

bool CLockFile::AlreadyExist()
{
	int fd =0, val = 0;
	char buf[10] = {0};

	do 
	{
		if ((fd = open(PIDFILE, O_WRONLY | O_CREAT, FILE_MODE)) < 0) {
			LOG_ERR("Open pgrogram file mutex error !!!");
			break;
		}

		if (write_lock(fd, 0, SEEK_SET, 0) < 0) {
			if (errno == EACCES || errno == EAGAIN)
				LOG_ERR("The Program already is running !!!");
			else
				LOG_ERR("Add write_lock to the program file mutex error !!!");
			break;
		}

		if (ftruncate(fd, 0) < 0) {
			LOG_ERR("ftruncate the program file mutex error !!!");
			break;
		}

		sprintf(buf, "%d\n", getpid());
		if (write(fd, buf, strlen(buf)) != strlen(buf)) {
			LOG_ERR("Write id of the program to file mutex error !!!");
			break;
		}

		if ((val = fcntl(fd, F_GETFD, 0)) < 0) {
			LOG_ERR("fcntl F_GETFD for set close-on-exec flag error !!!");
			break;
		}

		val |= FD_CLOEXEC;
		if (fcntl(fd, F_SETFD, val) < 0) {
			LOG_ERR("fcntl F_SETFD for set close-on-exec flag error !!!");
			break;
		}

		return false;
	} while (0);
	
	return true;
}

