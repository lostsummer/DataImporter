/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file Condition.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef _CONDITION_H_
#define _CONDITION_H_

#include <pthread.h>
#include "Noncopyable.h"
#include "Mutex.h"

/**
* @brief class Condition define
*/
class Condition : private Noncopyable
{
public:
	/**
	* @brief constructor
	*/
	inline Condition()
	{
		::pthread_cond_init(&cond, NULL);
	}

	/**
	* @brief destructor
	*/
	inline ~Condition()
	{
		::pthread_cond_destroy(&cond);
	}

	/**
	* @brief Signal
	*/
	inline void Signal()
	{
		::pthread_cond_signal(&cond);
	}

	/**
	* @brief Signal all events
	*/
	inline void Broadcast()
	{
		::pthread_cond_broadcast(&cond);
	}

	/**
	* @brief wait until mutex unlock
	*/
	inline void Wait(IMutex& mutex)
	{
		::pthread_cond_wait(&cond, &mutex.mutex);
	}

private:
	pthread_cond_t cond;
};


#endif

