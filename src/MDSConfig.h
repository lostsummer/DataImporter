#ifndef MDSCONFIG_H
#define MDSCONFIG_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)
#include <string>

#include "SysGlobal.h"
#include "String.h"
#include "ConfigManageSystem.h"


#define INI_FILE_NAME "MDS.ini"

// add by panlishen

class CMDSConfig
{
public:
	CMDSConfig(void);
	~CMDSConfig(void);

	//获取数值型配置
	int GetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault);

	//获取字符串型配置
	CString GetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry,LPCTSTR lpszDefault);

	//{ add by panlshen 2009-12-10
	void SetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nVal);
	void SetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszVal);
	//}

	//加载配置
	int Load_Config(const std::string& configFile);// modify by panlishen 2009-12-2

	//输出所有设置
	void ListAll(void);

private:
	//配置变量
	ConfigData		cfg_data;
	ConfigLoader	cfg_loader;
	ConfigManager	cfg_mgr;
public:
	//  保存配置
	int Save_Config(void);
};


#endif
