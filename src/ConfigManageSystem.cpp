/********************************************************
 * 版权所有 (C)2004－2008, 
 * 
 * 文件名称：ConfigManageSystem.cpp
 * 文件标识：
 * 模块名称：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者：刘成柱
 * 完成日期：2006-8-7 18:20:57
 * 
 * 修改记录：
 *   修改日期：
 *   版 本 号：
 *   修 改 人：
 *   修改内容：
 *********************************************************/
#include <sstream>


//{ add by panlishen 2009-12-08
#ifndef WIN32
#include <strings.h>
#define stricmp strcasecmp
#define strnicmp strncasecmp

#endif
//} add by panlishen 2009-12-08

#include "ConfigManageSystem.h"
#include "string.h"
#include "stdio.h"

using namespace std;

/**函数名称:ConfigData::ConfigData
***功能说明:
* @return : 
*/
ConfigData::ConfigData()
{
}


/**函数名称:ConfilgLoader::SetFileName
***功能说明:
* @param : char *filename
* @return : void 
*/
void ConfigLoader::SetFileName(const std::string& filename)
{
	m_FileName = filename;
}


/**函数名称:ConfilgLoader::SetpData
***功能说明:
* @param : ConfigData *pData
* @return : void 
*/
void ConfigLoader::SetpData(ConfigData *pData)
{
	m_pData=pData;
}


/**函数名称:ConfilgLoader::Load
***功能说明:
* @return : void 
*/
void ConfigLoader::Load()
{
	f_LoadData();
}

//{ add by panlishen 2010-01-04
void ConfigLoader::Trim(std::string& str)
{
	std::string::size_type off = str.find(" ");
	while(off != std::string::npos)
	{
		str.erase(off, 1);
		off = str.find(" ");
	}
}

void ConfigLoader::TrimLeft(std::string& str)
{
	while(str.size() && (*str.begin() == ' ' || *str.begin() == '\t'))
		str.erase(str.begin());
}
//} add by panlishen 2010-01-04

/**函数名称:ConfilgLoader::f_LoadData
***功能说明:
* @return : void 
*/
void ConfigLoader::f_LoadData()
{
//{ modify by panlishen 2010-01-04
	std::string group;
	std::string item;
	std::string t_value;
	int nTotal = 0;

	FILE* fp = fopen(m_FileName.c_str(), "r");
	if(fp)
	{
		char * buf;
		int length;

		fseek(fp, 0, SEEK_END);
		length = ftell(fp);
		buf = new(std::nothrow) char[length + 1];
		memset(buf, 0, length+1);
		fseek(fp, 0, SEEK_SET);

		fread(buf, length, 1, fp);
		buf[length] = '\0';
		std::string buffer = std::string(buf);
		delete [] buf;

		fclose(fp);

		while(true)
		{
			std::string::size_type end = buffer.find("\n");
			if (end == std::string::npos) 
				break;

			std::string line = buffer.substr(0, end);
			buffer.erase(0, end + 1);
			
			std::string::size_type cr = line.find("\r");
			if (cr != std::string::npos)
			{
				line.erase(cr, 1);
			}

			//跳过空行
			if (!line.size())
				continue;
			
			//去掉行首空格
			TrimLeft(line);

			//跳过注释
			if (line[0] == '/' || line[0] == '#')
				continue;

			nTotal++;

			//首先判断本行是否为组名行[GROUP],再判断是否为项名行ITEM=VALUE
			if(line[0] == '[')
			{
				std::string::size_type pos = line.find("]");
				if (pos != std::string::npos);
				{
					//取组名
					group = line.substr(1, pos - 1);

					//去掉组名中所有空格
					Trim(group);
				}
			}
			else
			{
				std::string::size_type pos = line.find("=");
				if (pos != std::string::npos)
				{
					//得到项名
					item = line.substr(0, pos);

					//去掉项名中所有空格
					Trim(item);

					//得到值
					t_value = line.substr(pos + 1, line.length() - pos -1);

					//去掉值的左边空格
					TrimLeft(t_value);

					//将解析得到的组名-项名-项值配置保存到配置集合	
					ConfigDataItem data(group, item, t_value);
					m_pData->m_listitem.push_back(data);
				}
			}

			if (nTotal > MAXCOUNT)
			{
				break;
			}
		}
	}

//} modify by panlishen 2010-01-04
}



/**函数名称:ConfilgLoader::Load
***功能说明:
* @return : void 
*/
void ConfigLoader::Save()
{
	f_SaveData();
}


/**函数名称:ConfilgLoader::f_LoadData
***功能说明:
* @return : void 
*/
void ConfigLoader::f_SaveData()
{
//{ modify by panlishen 2010-01-05
	if (m_pData == NULL) return;

	std::string group;
	std::stringstream ss;
	
	for(vector<ConfigDataItem>::const_iterator cit = m_pData->m_listitem.begin(); cit != m_pData->m_listitem.end(); ++cit)
	{
		bool bNewGroup=false;
		if(group != cit->m_group)
		{
			bNewGroup=true;

			group = cit->m_group;
		}

		if(bNewGroup==true)
		{
			ss <<"[" << cit->m_group <<"]\r\n";
		}

		ss << cit->m_item <<"=" << cit->m_value << "\r\n";
	}	

	FILE* fp = fopen(m_FileName.c_str(), "w+");
	if(fp)
	{
		fwrite(ss.str().c_str(), sizeof(char), ss.str().length(), fp);
		fclose(fp);
	}
//} modify by panlishen 2010-01-05
}



/**函数名称:ConfigManager::ConfigManager
***功能说明:
* @return : 
*/
ConfigManager::ConfigManager()
{
	m_pData = NULL;
}


/**函数名称:ConfigManager::SetpData
***功能说明:
* @param : ConfigData *pData
* @return : void 
*/
void ConfigManager::SetpData(ConfigData *pData)
{
	m_pData=pData;
}


/**函数名称:ConfigManager::SetConfig
***功能说明:
* @param : char *group
* @param : char *item
* @param : char *t_value
* @return : void 
*/
void ConfigManager::SetConfig(const char *group, const char *item, const char *t_value)
{
	for(vector<ConfigDataItem>::iterator it = m_pData->m_listitem.begin(); it != m_pData->m_listitem.end(); ++it)
	{
		// modify by panlishen 2009-12-08 不区分大小写
		if(stricmp(it->m_group.c_str(), group)==0 
			&& stricmp(it->m_item.c_str(), item)==0)
		{
				it->m_value = t_value;
				break;
		}
	}
}


/**函数名称:ConfigManager::GetConfig
***功能说明:
* @param : char *group
* @param : char *item
* @param : char *t_value
* @return : void 
*/
void ConfigManager::GetConfig(const char *group, const char *item, std::string& t_value)
{
	for(vector<ConfigDataItem>::const_iterator cit = m_pData->m_listitem.begin(); cit != m_pData->m_listitem.end(); ++cit)
	{
		// modify by panlishen 2009-12-08 不区分大小写
		if(stricmp(cit->m_group.c_str(), group)==0 
			&& stricmp(cit->m_item.c_str(), item)==0)
		{
				t_value = cit->m_value;
				break;
		}
	}
}



