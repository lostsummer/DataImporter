// XInt32.h: interface for the XInt32 class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __XINT32_H__
#define __XINT32_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "SysGlobal.h"

class XInt32  
{
public:
	XInt32();
	XInt32(const INT64 n);

	static INT32	m_nMaxBase;
	static INT32	m_nMinBase;
	static BYTE		m_cMaxMul;

	int 			m_nBase:29;
	unsigned int	m_cMul:3;

	INT64 GetValue() const;
	operator INT64() const { return GetValue();}

	XInt32 operator=(const XInt32 x);

	XInt32 operator=(const INT64 n);
	XInt32 operator+=(const INT64 n);

	INT32 GetRawData();
	void SetRawData(INT32 n);

	BOOL operator==(const XInt32 x);
};

typedef XInt32*  PXInt32;

#endif


