#include <errno.h>
#include <arpa/inet.h>
#include <sys/select.h>
#include <unistd.h>
#include <signal.h>

#include "MyClientThread.h"
#include "MDS.h"


CMyClientThread::CMyClientThread() : CMyThread(), m_pSocket(0), m_dwFailConnet(0)
{
}

CMyClientThread::~CMyClientThread()
{
	if (m_pSocket)
	{
		delete m_pSocket;
		m_pSocket = 0;
	}
}

BOOL CMyClientThread::DoMessage()
{
	return FALSE;
}

CString CMyClientThread::SocketError(int nErrorCode)
{
	if (nErrorCode)
	{
		CString strCode;
		strCode.Format("(%d)", nErrorCode);

		CString strError;

		switch(nErrorCode)
		{
		case EMFILE:
			strError = "EMFILE";
			break;
		case ENOBUFS:
		case ENOMEM:
			strError = "ENOMEM";
			break;
		case ECONNREFUSED:
			strError = "ECONNREFUSED";
			break;
		case ENETUNREACH:
			strError = "ENETUNREACH";
			break;
		case ETIMEDOUT:
			strError = "ETIMEDOUT";
			break;
		default:
			strError = "";
		}

		return strError+strCode;
	}
	else
		return "";
}

void CMyClientThread::Run()
{
	if(m_pSocket == NULL)
		return;

	LogApp("%s start", Name());

	m_pSocket->m_socket = -1;

	BOOL bExit = FALSE;
	BOOL bSysError = FALSE;

	try
	{
		while(m_pSocket && bExit==FALSE && bSysError==FALSE)
		{
			struct timespec ts;
			clock_gettime(CLOCK_REALTIME, &ts);
			ts.tv_sec += 1;

			if (sem_timedwait(&m_semExit, &ts) != 0)
			{
				if(errno != ETIMEDOUT && errno != EINTR)
					break;
			}
			else
			{
				bExit = TRUE;
				break;
			}

			m_pSocket->m_strMessage = "";

			BOOL bConnected = FALSE;

			do
			{
				if (m_dwFailConnet > 100)
				{
					if (m_pSocket->m_strCurAddr == m_pSocket->m_strAddress1)
					{
						m_pSocket->m_strCurAddr = m_pSocket->m_strAddress2;
						m_pSocket->m_wCurPort = m_pSocket->m_wPort2;
					}
					else
					{
						m_pSocket->m_strCurAddr = m_pSocket->m_strAddress1;
						m_pSocket->m_wCurPort = m_pSocket->m_wPort1;
					}
					m_dwFailConnet = 0;
				}

				if (m_pSocket->m_strCurAddr.IsEmpty())
				{
					m_pSocket->m_strCurAddr = m_pSocket->m_strAddress1;
					m_pSocket->m_wCurPort = m_pSocket->m_wPort1;
				}

				in_addr_t lAddress = inet_addr(m_pSocket->m_strCurAddr.GetData());
				if(lAddress == INADDR_NONE)
				{
					LOG_ERR("%s inet_addr() failed!", Name());
					break;
				}

				m_pSocket->m_socket = socket(AF_INET, SOCK_STREAM, 0);
				if (m_pSocket->m_socket < 0)
				{
					LOG_ERR("%s socket() failed! error msg:%s", Name(), SocketError(errno).GetData());
					bSysError = TRUE;

					break;
				}

				fcntl(m_pSocket->m_socket,F_SETFD, fcntl(m_pSocket->m_socket,F_GETFD,0) | FD_CLOEXEC);
				if(g_SetNonBlocking(m_pSocket->m_socket) != 0)
				{
					LOG_ERR("%s SetNonBlocking failed", Name());
					bSysError = TRUE;
					break;
				}

				struct sockaddr_in sockAddr;
				memset(&sockAddr, 0, SIZEOF_SOCKADDR);

				sockAddr.sin_family = AF_INET;
				sockAddr.sin_addr.s_addr = lAddress;
				sockAddr.sin_port = htons(m_pSocket->m_wCurPort);

				m_pSocket->m_wStatus = CMySocket::WantAll;
				if(connect(m_pSocket->m_socket, (struct sockaddr *)&sockAddr, sizeof(sockAddr)) != 0 && errno != EINPROGRESS)
				{
					LogApp("%s connect to %s:%d failed! err msg:%s", Name(), m_pSocket->m_strCurAddr.GetData(), m_pSocket->m_wCurPort, SocketError(errno).GetData());
					break;
				}

				while(!bExit && m_pSocket && m_pSocket->m_socket > 0)
				{
					if (sem_trywait(&m_semExit)==0)
					{
						bExit = TRUE;

						m_pSocket->m_wStatus = CMySocket::WantDelete;
						LogApp("%s SysShutDown", Name());
						break;
					}

					ForThreadCheck();

					//删除标识删除的处理包
					m_pSocket->CheckData();

					fd_set fsRead, fsWrite, fsExcept;
					timeval tv;

					FD_ZERO(&fsRead);
					FD_ZERO(&fsWrite);
					FD_ZERO(&fsExcept);

					FD_SET(m_pSocket->m_socket, &fsRead);
					FD_SET(m_pSocket->m_socket, &fsWrite);
					FD_SET(m_pSocket->m_socket, &fsExcept);

					//多路选择超时时间
					tv.tv_sec = 0;
					tv.tv_usec = 200000;

					//多路选择
					int nRet = select(m_pSocket->m_socket+1, &fsRead, &fsWrite, &fsExcept, &tv);
					if(nRet > 0)
					{
						if(FD_ISSET(m_pSocket->m_socket, &fsExcept)) 
						{
							LOG_ERR("%s Select except!", Name());
							bConnected = FALSE;
							break;
						}
						else
						{
							if(FD_ISSET(m_pSocket->m_socket, &fsWrite)) 
							{
								if(bConnected == FALSE)
								{
									LogApp("%s connect to %s:%d successfully!", Name(), m_pSocket->m_strCurAddr.GetData(), m_pSocket->m_wCurPort);

									bConnected = TRUE;
									m_pSocket->AfterConnect();
								}

								if(m_pSocket->m_wStatus==CMySocket::WantSend || m_pSocket->m_wStatus==CMySocket::WantAll)
								{
									m_pSocket->DoSend(&m_rThreadData);
								}
							}

							if(FD_ISSET(m_pSocket->m_socket, &fsRead)) 
							{
								m_pSocket->DoRecv(&m_rThreadData);
							}
						}

						if (m_pSocket->m_wStatus == CMySocket::WantDelete)
						{
							LOG_ERR("%s Socket WantDelete", Name());
							break;
						}
					}
					else if(nRet < 0 && errno != EINTR)
					{
						LOG_ERR("%s select error", Name());
						break;
					}

					DoMessage();

					if(!bExit)
					{
						struct timespec time;
						time.tv_sec = m_delaySec;
						time.tv_nsec = m_delayNSec;
						nanosleep(&time, NULL);
					}
				}

				m_dwFailConnet++;
				LogApp("%s disconnect from %s:%d(%d) !", Name(), m_pSocket->m_strCurAddr.GetData(), m_pSocket->m_wCurPort, m_dwFailConnet);
			}while(0);

			m_pSocket->Close();
		}
	}
	catch (...)
	{
		m_pSocket->Close();
		LOG_ERR("%s 线程异常结束！！", Name());
		return;
	}
	

	LogApp("%s end", Name());
}


