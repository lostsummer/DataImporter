#include "RWLock.h"

RWLock::RWLock() 
{
	pthread_rwlockattr_t attr;
	::pthread_rwlockattr_init(&attr);
	::pthread_rwlockattr_setkind_np(&attr, PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP);//写优先
	::pthread_rwlock_init(&rwlock, &attr);
	::pthread_rwlockattr_destroy(&attr);
}

RWLock::~RWLock() 
{ 
	::pthread_rwlock_destroy(&rwlock);
}

int RWLock::TryAcquireReadLock() 
{ 
	return ::pthread_rwlock_tryrdlock(&rwlock);
}

//可能会对同时获得的线程数有限制，一般需检查返回
int RWLock::AcquireReadLock() 
{ 
	return ::pthread_rwlock_rdlock(&rwlock);
}

int RWLock::TryAcquireWriteLock() 
{ 
	return ::pthread_rwlock_trywrlock(&rwlock);
}

int RWLock::AcquireWriteLock() 
{ 
	return ::pthread_rwlock_wrlock(&rwlock);
}

int RWLock::unlock() 
{ 
	return ::pthread_rwlock_unlock(&rwlock);
}

