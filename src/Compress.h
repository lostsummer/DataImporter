#ifndef COMPRESS_H
#define COMPRESS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "SysGlobal.h"

#define _LZH_N			4096
#define _LZH_F			60
#define _LZH_THRESHOLD	2
#define _LZH_NIL		_LZH_N
#define _LZH_N_CHAR  	(256 - _LZH_THRESHOLD + _LZH_F)
#define _LZH_T	 		(_LZH_N_CHAR * 2 - 1)
#define _LZH_R 			(_LZH_T - 1)
#define _LZH_MAX_FREQ	0x8000

class CCompress  
{
public:
	CCompress();
	CCompress(int lLenOfOutBuf);
	virtual ~CCompress();

	char * m_pcIn;
	int m_lIn;
	char * m_pcOut;
	int m_lOut;
	BOOL Encode();
	BOOL Decode();

private:
	int m_lInCount;
	int m_lOutCount;
	BYTE m_pbyBuffer[_LZH_N + _LZH_F - 1];
	short m_nMatchPos;
	short m_nMatchLen;
	short m_pnLeftSon[_LZH_N + 1];
	short m_pnRightSon[_LZH_N + 257];
	short m_pnDad[_LZH_N + 1];
	WORD m_pwFreq[_LZH_T + 1];
	short m_pnParent[_LZH_T + _LZH_N_CHAR];
	short m_pnSon[_LZH_T];
	WORD m_wGetBuf;
	BYTE m_byGetLen;
	WORD m_wPutBuf;
	BYTE m_byPutLen;
	WORD m_wCode;
	WORD m_wLen;

	int m_lLenOfOutBuf;

	void StartHuff();
	short DecodeChar();
	short DecodePosition();
	short GetBit();
	short GetByte();
	void Update(short);
	void ReConstruct();
	void InitTree();
	void InsertNode(short);
	void DeleteNode(short);
	void EncodeChar(WORD);
	void EncodePosition(WORD);
	void EncodeEnd();
	void PutCode(short, WORD);

	void CheckOutBuff(int nCurPos);
};

#endif


