#include "CalcGroup.h"
#include "MDS.h"


CCalcGroup::CCalcGroup()
{
	m_dwGroupID = 0;
	m_strGroup = "";

	Initial();
}

CCalcGroup::~CCalcGroup()
{
}

void CCalcGroup::Initial()
{
	m_Mutex.Lock();

	m_dwCount = 0;
	ZeroMemory(m_pxValue, 4*G_SIZE);

	m_Mutex.UnLock();
}

char CCalcGroup::Compare(CCalcGroup* pGroup, short sID, char cSort)
{
	if (sID==-1)
		cSort = 1;

	if (cSort==0)
		return 0;
	else if (sID>=0 && sID<G_SIZE)
	{
		INT64 hThis = GetValueForCompare(sID);
		INT64 hGroup = pGroup->GetValueForCompare(sID);

		if (hThis>hGroup)
			return cSort;
		else if (hThis<hGroup)
			return -cSort;
		else
			return Compare(pGroup, -1, 1);
	}
	else
	{
		if (m_dwGroupID>pGroup->m_dwGroupID)
			return cSort;
		else if (m_dwGroupID<pGroup->m_dwGroupID)
			return -cSort;
		else
			return 0;
	}
}

INT64 CCalcGroup::GetValueForCompare(short sID)
{
	if (sID==G_CPX)
	{
		int nCPX_Day = (int)m_pxValue[sID];
		if (nCPX_Day==1)	// B 20110621
			return 1000000; //
		else if (nCPX_Day==-1) // S 20110621
			return -1000000; //
		else
			return nCPX_Day;
	}

	return GetValue(sID);
}

INT64 CCalcGroup::GetValue(short sID)
{
	if (sID>=0 && sID<G_SIZE)
		return m_pxValue[sID];
	else
		return 0;
}

void CCalcGroup::Read(CBuffer& buf, WORD wVersion)
{
	buf.ReadString(m_strGroup);
	buf.Read(m_pxValue, 4*G_SIZE);
	m_dwCount++;
}

void CCalcGroup::Write(CBuffer& buf, WORD wVersion)
{
	buf.WriteString(m_strGroup);
	buf.Write(m_pxValue, 4*G_SIZE);
}

BOOL CCalcGroup::IsBad()
{
	if (CMDSApp::g_dwSysHMS>94000 && m_pxValue[G_VOLUME].m_nBase==0)
		return TRUE;
	return FALSE;
}

void CCalcGroup::SaveValue2Goods()
{
	DWORD dwGoodsID = 2000000+m_dwGroupID;
	CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
	ASSERT(pGoods);

	if (pGoods==NULL)
		return;

	//pGoods->m_rwGood.AcquireWriteLock();
	// 更换锁 [3/5/2015 frant.qian]
	RWLock_scope_wrlock  rwlock(pGoods->m_rwGood);	
	pGoods->m_rwGoodcount[4] += 1;
	try
	{	
		pGoods->m_xSZ = m_pxValue[G_SZ];
		pGoods->m_xLTSZ = m_pxValue[G_LTSZ];

		pGoods->m_GroupValue.m_dwNumAll = (DWORD)m_pxValue[G_GPJS];
		pGoods->m_GroupValue.m_dwNumRise = (DWORD)m_pxValue[G_SZJS];
		pGoods->m_GroupValue.m_dwNumFall = (DWORD)m_pxValue[G_XDJS];

		pGoods->m_GroupValue.m_dwID_QS = (DWORD)m_pxValue[G_QSG];
		pGoods->m_GroupValue.m_dwID_QS5 = (DWORD)m_pxValue[G_QSG5];
		pGoods->m_GroupValue.m_dwID_RS = (DWORD)m_pxValue[G_RSG];
		pGoods->m_GroupValue.m_dwID_RS5 = (DWORD)m_pxValue[G_RSG5];
	}
	catch(...)
	{
		LOG_ERR("CCalcGroup::SaveValue2Goods fail!!");
	}
	pGoods->m_rwGoodcount[4] -= 1;
	//pGoods->m_rwGood.ReleaseWriteLock();
}
