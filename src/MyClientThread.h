#ifndef MYCLIENTTHREAD_H
#define MYCLIENTTHREAD_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <pthread.h>

#include "MyThread.h"
#include "MySocket.h"

class CMyClientThread : public CMyThread
{
public:
	CMyClientThread();
	virtual ~CMyClientThread();

	virtual const char* Name() { return "CMyClientThread"; }

	virtual void ForThreadCheck(){return;};

	virtual BOOL DoMessage();

public:
	CMySocket* m_pSocket;
	CSockThreadData m_rThreadData;

protected:
	virtual void Run();

	volatile DWORD m_dwFailConnet;

private:
	CString SocketError(int nErrorCode);
};


#endif

