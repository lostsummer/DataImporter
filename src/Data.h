#ifndef DATA_H
#define DATA_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "Goods.h"

#define	MINUTE_DATA		2
#define	MIN_DATA		3
#define	DAY_DATA		4
#define	HISMIN_DATA		5
#define	MULTI_HM_DATA	6

#define	TRADE_DATA		11
#define	PARTORDER_DATA	12
#define	BARGAIN_DATA	13

#define	DAY_PERIOD		1
#define	WEEK_PERIOD		2
#define	MONTH_PERIOD	3
#define	QUARTER_PERIOD	4
#define	HALF_PERIOD		5
#define	YEAR_PERIOD		6

#define MIN_1			1
#define MIN_5			5
#define MIN_15			15
#define MIN_30			30
#define MIN_60			60

//////////////////////////////////////////////////////////////////////

class CData  
{
public:
	CData();
	virtual ~CData();

	virtual void FreeData();
	virtual void LoadData();
	virtual void ClearData();
	virtual void DeleteData();

	BOOL TestWantFree();
	BOOL WantFree();
	BOOL CheckFree(WORD wDataType, WORD wPeriod);

	void AcquireLock(bool rd=true)
	{
		if (m_pWR == NULL) return;

		if (rd)
			m_pWR->AcquireReadLock();
		else
			m_pWR->AcquireWriteLock();
	}

	void ReleaseLock(bool rd=true)
	{
		if (m_pWR == NULL) return;

		if (rd)
			m_pWR->ReleaseReadLock();
		else
			m_pWR->ReleaseWriteLock();
	}

	virtual void Trace(const char* append = " ");
	void TraceCache();

public:
	CGoods* m_pGoods;
	RWLock* m_pWR;

	WORD m_wDataType;
	WORD m_wPeriod;

	volatile int m_nUseCount;
	DWORD m_dwCount;
	DWORD m_dwClearCount;

	DWORD m_dwActiveSecond;

	char m_cLoadOK;		//==0表示还没加载数据 ==1加载数据结束 ==-1wrong
	int m_lLoadThreadID;

	DWORD m_dwDateOfGoods;
};

//////////////////////////////////////////////////////////////////////

class CDataDay : public CData
{
public:
	CDataDay();
	virtual ~CDataDay();

	virtual void LoadData();
	virtual void SaveData();
    virtual void SaveTodayMin1();
	virtual void DeleteData();

	void Add(const CDay&);

	void AddMinutes(CSortArrayMinute&);
	void AddMinute(const CMinute&);
	
	void LoadToday();

	// 添加从指定.DAT文件中读取数据功能,Y需要 [11/7/2012 frantqian]
	void	LoadDayFromSpecialFile(void* pDF);

	//Steven Change to public 
	void AddEmptyMinute();
public:
	CDay* m_pDay;
	DWORD m_dwDay;
	std::map<DWORD, DWORD> m_mapHisHSL;
	CArray<DWORD,DWORD &> m_aHisHSL;
	//CArray < TYPE,ARG_TYPE > :: CArray()
	DWORD m_dwBuffer;

	DWORD m_dwClearCount;	//20110626

private:
	void DeleteDay();

	CDay m_daySave;
	CMinute m_minSave;
};

//////////////////////////////////////////////////////////////////////

class CDataMinute : public CData
{
public:
	CDataMinute();
	virtual ~CDataMinute();

	virtual void LoadData();
	virtual void ClearData();
	virtual void DeleteData();

	void AddMinutes(CSortArrayMinute&);
	void AddMinute(const CMinute&, BOOL bCheckVolume);	//20110627
	//Steven Change to Public
	void AddEmptyMinute();

public:
	CSortArray<CMinute> m_aMinute;

	DWORD m_dwClose;
	DWORD m_dwHigh;
	DWORD m_dwLow;

	DWORD m_dwClearCount;	//20110626

private:
	void Add(const CMinute&);

	CMinute m_minSave;
};

//////////////////////////////////////////////////////////////////////

class CDataHisMin : public CData
{
public:
	CDataHisMin();
	virtual ~CDataHisMin();

	virtual void LoadData();
	virtual void DeleteData();

	void AddMinutes(CSortArrayMinute&);
public:
	DWORD m_dwDate;

	DWORD m_dwClose;
	DWORD m_dwHigh;
	DWORD m_dwLow;
	CHisMinArray m_aHisMin;

	BOOL m_bCheckData;
private:
	void AddHisMin(CHisMin&);
	void Add(CHisMin&);
	
	CHisMin m_hmSave;
};


//////////////////////////////////////////////////////////////////////

template <class TYPE> class CDataType : public CData
{
public:
	CDataType();
	virtual ~CDataType();

	virtual void LoadData() = 0;
	virtual void ClearData();
	virtual void DeleteData();

	virtual void AllocData(DWORD);
	void AddData(const TYPE&);

public:
	TYPE* m_pBuffer;
	DWORD m_dwMaxSize;
	DWORD m_dwSize;
};

template <class TYPE> CDataType<TYPE>::CDataType()
{
	m_pBuffer = NULL;
	m_dwMaxSize = 0;
	m_dwSize = 0;
}

template <class TYPE> CDataType<TYPE>::~CDataType()
{
	DeleteData();
}

template <class TYPE> void CDataType<TYPE>::DeleteData()
{
	CData::DeleteData();

	if (m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
	m_dwMaxSize = 0;
	m_dwSize = 0;
}

template <class TYPE> void CDataType<TYPE>::AllocData(DWORD dwNewSize)
{
	if (dwNewSize>m_dwMaxSize)
	{
		TYPE* pSave = m_pBuffer;

		DWORD dwBuffer = (dwNewSize*sizeof(TYPE)+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE;
		m_pBuffer = (TYPE*)new(std::nothrow) BYTE[dwBuffer];
		ASSERT(m_pBuffer);

		m_dwMaxSize = dwBuffer/sizeof(TYPE);
		ASSERT(dwNewSize<=m_dwMaxSize);

		if (pSave)
		{
			if (m_dwSize>0)
				CopyMemory(m_pBuffer, pSave, m_dwSize*sizeof(TYPE));
			delete [] pSave;
		}
	}
}

template <class TYPE> void CDataType<TYPE>::AddData(const TYPE& type)
{
	AllocData(m_dwSize+1);
	m_pBuffer[m_dwSize++] = type;
}

template <class TYPE> void CDataType<TYPE>::ClearData()
{
	m_dwSize = 0;
}

//////////////////////////////////////////////////////////////////////
class CDataBargain : public CDataType <CBargain>
{
public:
	virtual void LoadData();
};

//////////////////////////////////////////////////////////////////////////
class CLoadData
{
public:
	CLoadData();
	~CLoadData();

	void DeleteAll();

	void Initialize(DWORD dwThread);

	int DoLoadData(CMyThread& thLoadData);

	void AddData(CData* pData);

private:
	typedef std::map<int, CPtrArray> mapLoadData;
	mapLoadData m_mapData;
	RWLock m_rwData;
};

extern CLoadData g_LoadData;


#endif

