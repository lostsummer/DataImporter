#include "MDS.h"
#include "Data.h"
#include "AppGlobal.h"

CLoadData g_LoadData;
extern int g_nLoadThread;

extern DWORD g_dwFreeDataDelay;
extern BOOL g_bFreeDayData;
extern BOOL g_bFreeMin1Data;
extern BOOL g_bFreeMin5Data;
extern BOOL g_bFreeMin30Data;
extern BOOL g_bFreeMinuteData;
extern BOOL g_bFreeBargainData;
extern BOOL g_bFreeHisMinData;

extern DWORD g_dwSaveMin1;
extern DWORD g_dwSaveMin5;
extern DWORD g_dwSaveMin30;
extern DWORD g_dwSaveHisMin;
extern DWORD g_dwDayGrowBy;

//////////////////////////////////////////////////////////////////////

CData::CData()
{
    m_pGoods = NULL;
    m_pWR = NULL;

    m_wDataType = 0;
    m_wPeriod = 0;

    m_nUseCount = 0;    
    m_dwCount = 0;
    m_dwClearCount = 0;

    m_dwActiveSecond = 0;

    m_cLoadOK = 0;
    m_lLoadThreadID = 0;

    m_dwDateOfGoods = 0;
}

CData::~CData()
{
    DeleteData();
}

void CData::FreeData()
{
    if (m_pGoods/* && m_pWR*/)
        m_pGoods->FreeData(this);
    else
        delete this;
}

void CData::LoadData()
{
}

void CData::ClearData()
{
}

void CData::DeleteData()
{
    m_cLoadOK = 0;
    m_lLoadThreadID = 0;
    m_dwActiveSecond = 0;
}

void CData::Trace(const char* append)
{
    DWORD dwGoodID = m_pGoods ? m_pGoods->GetID() : 0;
    switch (m_wDataType)
    {
    case DAY_DATA:
        LogDbg("%s %d 日线数据 type=%d period=%d", append, dwGoodID, m_wDataType, m_wPeriod);
        break;
    case MIN_DATA:
        if (m_wPeriod%30 == 0)  
            LogDbg("%s %d 30分钟线数据 type=%d period=%d", append, dwGoodID, m_wDataType, m_wPeriod);
        else if (m_wPeriod%5 == 0)
            LogDbg("%s %d 5分钟线线数据 type=%d period=%d", append, dwGoodID, m_wDataType, m_wPeriod);
        else
            LogDbg("%s %d 1分钟线线数据 type=%d period=%d", append, dwGoodID, m_wDataType, m_wPeriod);
        break;
    case BARGAIN_DATA:
        LogDbg("%s %d 分笔数据 type=%d period=%d", append, dwGoodID, m_wDataType, m_wPeriod);
        break;
    case MINUTE_DATA:
        LogDbg("%s %d 分时数据 type=%d period=%d", append, dwGoodID, m_wDataType, m_wPeriod);
        break;
    }
}


void CData::TraceCache()
{
    switch (m_wDataType)
    {
    case DAY_DATA:
        CMyThread::atomicInc(&CGoods::s_nCacheDay);
        break;
    case MIN_DATA:
        if (m_wPeriod%30 == 0)
            CMyThread::atomicInc(&CGoods::s_nCacheMin30);
        else if (m_wPeriod%5 == 0)
            CMyThread::atomicInc(&CGoods::s_nCacheMin5);
        else
            CMyThread::atomicInc(&CGoods::s_nCacheMin1);
        break;
    case BARGAIN_DATA:
        CMyThread::atomicInc(&CGoods::s_nCacheBargain);
        break;
    case MINUTE_DATA:
        if (m_wPeriod == 0)
            CMyThread::atomicInc(&CGoods::s_nCacheMinute0);
        else
            CMyThread::atomicInc(&CGoods::s_nCacheMinute1);
        break;
    }
}

BOOL CData::CheckFree(WORD wDataType, WORD wPeriod)
{
    switch (wDataType)
    {
    case DAY_DATA:
        return g_bFreeDayData;
    case MIN_DATA:
        if (wPeriod%30 == 0)
            return g_bFreeMin30Data;
        else if (wPeriod%5 == 0)
            return g_bFreeMin5Data;
        else
            return g_bFreeMin1Data;
    case BARGAIN_DATA:
        return g_bFreeBargainData;
    case MINUTE_DATA:
        return g_bFreeMinuteData;
    case HISMIN_DATA:
        return g_bFreeHisMinData;
    }

    return TRUE;
}

BOOL CData::TestWantFree()
{
    return (m_nUseCount<=0 && m_cLoadOK);
}

BOOL CData::WantFree()
{
    return (m_nUseCount<=0 && m_cLoadOK &&
        (CheckFree(m_wDataType, m_wPeriod) ||
        (g_dwFreeDataDelay>0 && m_dwActiveSecond+g_dwFreeDataDelay<CMDSApp::g_dwSysSecond) ) );
}

//////////////////////////////////////////////////////////////////////

void debug_minutes(CDay * days, int nSize){
    for(int i=0; i<nSize; i++){
        CDay day = days[i];
        LOG_DBG("debug_minute T:%d %d %d %d %d", day.m_dwTime, day.m_dwOpen, day.m_dwHigh, day.m_dwLow, day.m_dwClose);
    }
}

CDataDay::CDataDay()
{
    m_pDay = NULL;
    m_dwDay = 0;
    m_dwBuffer = 0;

    m_dwClearCount = 0;
}

CDataDay::~CDataDay()
{
    DeleteData();
}

void CDataDay::DeleteData()
{
    CData::DeleteData();

    DeleteDay();
}

void CDataDay::DeleteDay()
{
    if (m_pDay)
    {
        delete [] m_pDay;
        m_pDay = NULL;
        m_dwDay = 0;
        m_dwBuffer = 0;

        m_dwClearCount = 0;
        m_daySave.Clear();
        m_minSave.Clear();
    }
    m_aHisHSL.RemoveAll();

    m_mapHisHSL.clear();
}

void CDataDay::LoadData()
{
    DeleteDay();

    if (m_pGoods==NULL)
    {
        m_cLoadOK = -1;
        return;
    }


    BYTE cStation = m_pGoods->m_cStation;
    if (m_pGoods->m_cStation==3)    // //改成station==5的
        cStation = 5;

    CGoodsCode gcGoodsID(g_GetGoodsID(cStation, m_pGoods->m_dwNameCode), m_pGoods->m_pcNameCode);

    if (m_wDataType==MIN_DATA)
    {
        int nMaxDay = 0;

        CDataFile_Day* pDF = theApp.GetDataFile(cStation, m_wDataType, m_wPeriod);

        if (m_wPeriod && m_wPeriod%30==0)
        {
            if (g_dwSaveMin30>0)
                nMaxDay = m_pGoods->MinInDay()/30*(g_dwSaveMin30-1);
        }
        else if (m_wPeriod && m_wPeriod%5==0)
        {
            if (g_dwSaveMin5>0)
                nMaxDay = m_pGoods->MinInDay()/5*(g_dwSaveMin5-1);
        }
        else
        {
            if (g_dwSaveMin1>0)
                nMaxDay = m_pGoods->MinInDay()*(g_dwSaveMin1-1);
        }

        if (pDF)
        {
            CArray<CDay, CDay&> aDayLoad;               //20110625

            if (pDF->LoadData(gcGoodsID, aDayLoad)==0)  //20110625
            {
                int i = 0;                              //20110625
                CSortArray<CDay> aDay;                  //20110625

                int nDayLoad = (int)aDayLoad.GetSize(); //20110625
                for (i=0; i<nDayLoad; i++)              //20110625
                    aDay.Change(aDayLoad[i]);           //20110625

                DWORD PRICE_DIVIDE = m_pGoods->GetPriceDiv2();

                DWORD dwDateValue = m_pGoods->GetDateValue();
                DWORD dwStartDate = 0;
                if (m_pGoods->IsIF())
                    dwStartDate = g_dwStartDateGZQH;
                else if (m_pGoods->IsIC())
                    dwStartDate = g_dwStartDateGZIC;
                else if (m_pGoods->IsIH())
                    dwStartDate = g_dwStartDateGZIH;
                else if (m_pGoods->IsTF())
                    dwStartDate = g_dwStartDateTF;
                else if (m_pGoods->IsT())
                    dwStartDate = g_dwStartDateT;
                else if (m_pGoods->m_cSecurityType==OPTIONS)
                    dwStartDate = g_dwStartDateOptions;
                else if (m_pGoods->m_cStation==1 && m_pGoods->m_dwNameCode/10000==30)
                    dwStartDate = 20091030;
                else if (gcGoodsID.m_dwGoodsID==5002988)
                    dwStartDate = 20140514;

                int nDay = int(aDay.GetSize());
                if (nMaxDay>0 && nDay>nMaxDay)
                    i = nDay-nMaxDay;
                else
                    i = 0;  //20110625

                DWORD dwLastClose = 0;


                for (; i<nDay; i++)
                {
                    CDay& day = aDay[i];
                    DWORD dwDate = day.m_dwTime/10000+20000000;

                    if (g_IsDateGood(dwDate) && dwDate<dwDateValue && day.m_dwTime>=dwStartDate)
                    {
                        ///???if (day.m_dwTime%10000<=1600)
                        {
                            if (PRICE_DIVIDE==10)
                            {
                                day.m_dwOpen = (day.m_dwOpen+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                                day.m_dwHigh = (day.m_dwHigh+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                                day.m_dwLow = (day.m_dwLow+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                                day.m_dwClose = (day.m_dwClose+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                            }

                            if (day.m_dwOpen==0 && day.m_dwHigh==0 && day.m_dwLow==0 && day.m_dwClose==0)
                            {
                                day.m_dwOpen = dwLastClose;
                                day.m_dwHigh = dwLastClose;
                                day.m_dwLow = dwLastClose;
                                day.m_dwClose = dwLastClose;
                            }
                            else
                                dwLastClose = day.m_dwClose;

                            if (day.m_xVolume.m_nBase<0)
                                day.m_xVolume.m_nBase = 0;
                            if (day.m_xAmount.m_nBase<0)
                                day.m_xAmount.m_nBase = 0;
                                
                            Add(day);
                        }
                    }
                    else
                        break;
                }//for
            }
        }

        CSortArrayMinute aMinute;
        m_pGoods->GetMinutes(aMinute, FALSE);

        int nSize = (int)aMinute.GetSize();
        if (nSize>0)
        {
            for (int i=0; i<nSize; i++)
            {
                CMinute& min = aMinute[i];
                //if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)   //20110625
                
                // 修改熔断机制后的K线显示问题 [1/5/2016 qianyifan]
                //if (min.m_dwClose>0 && m_pGoods->CheckVolume(min.m_xVolume))
                if (min.m_dwClose>0)    
                    AddMinute(min);
            }

            if( m_wPeriod > 1 ){ //do NOT send empty minute data for 1 minute line to force client to refresh last two K lines. -Sam Liu 2015/04/28
                AddEmptyMinute();
            }
        }
    }
    else if (m_wDataType==DAY_DATA)
    {
        CDataFile_Day* pDF = theApp.GetDataFile(cStation, m_wDataType, DAY_PERIOD);
        if (pDF)
        {
            CArray<CDay, CDay&> aDayLoad;

            if (pDF->LoadData(gcGoodsID, aDayLoad)==0)  //20110625
            {
                //int i = 0;                                //20110625
                CSortArray<CDay> aDay;                  //20110625

                int nDayLoad = (int)aDayLoad.GetSize(); //20110625
                for (int i=0; i<nDayLoad; i++)              //20110625
                    aDay.Change(aDayLoad[i]);           //20110625

                DWORD PRICE_DIVIDE = m_pGoods->GetPriceDiv2();

                DWORD dwDateValue = m_pGoods->GetDateValue();
                DWORD dwStartDate = 19000101;
                if (m_pGoods->IsIF())
                    dwStartDate = g_dwStartDateGZQH;
                else if (m_pGoods->IsIC())
                    dwStartDate = g_dwStartDateGZIC;
                else if (m_pGoods->IsIH())
                    dwStartDate = g_dwStartDateGZIH;
                else if (m_pGoods->IsTF())
                    dwStartDate = g_dwStartDateTF;
                else if (m_pGoods->IsT())
                    dwStartDate = g_dwStartDateT;
                else if (m_pGoods->m_cSecurityType==OPTIONS)
                    dwStartDate = g_dwStartDateOptions;
                else if (m_pGoods->m_cStation==1 && m_pGoods->m_dwNameCode/10000==30)
                    dwStartDate = 20091030;
                else if (gcGoodsID.m_dwGoodsID==5002988)
                    dwStartDate = 20140514;

                CDay day;

                int nDay = int(aDay.GetSize());
                for (int i=0; i<nDay; i++)
                {
                    CDay& day = aDay[i];
                    if (g_IsDateGood(day.m_dwTime) && day.m_dwTime<dwDateValue && 
                        day.m_dwTime>=dwStartDate && m_pGoods->IsDayGood(day))
                    {
                        if (PRICE_DIVIDE==10)
                        {
                            day.m_dwOpen = (day.m_dwOpen+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                            day.m_dwHigh = (day.m_dwHigh+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                            day.m_dwLow = (day.m_dwLow+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                            day.m_dwClose = (day.m_dwClose+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                        }

                        Add(day);
                    }
                }
            }
        }

        if (m_pGoods->WantSaveToday(FALSE))
        {
            CDay day;
            m_pGoods->GetToday(day);
            Add(day);
        }
    }


    //添加历史流通股本，计算历史换手率
    m_mapHisHSL.clear();

    m_aHisHSL.SetSize(m_dwDay);
    
    CGoodsHisGB hisGB;
    DWORD dwGoodsID = m_pGoods->GetID();
    theApp.m_hisGB.Get(dwGoodsID, hisGB);
    XInt32 xLastLTG = m_pGoods->m_xLTG;

    for (int i=m_dwDay-1; i>=0; i--)
    {
        DWORD dwDate;
        DWORD dwTime;
        INT64 vol;
        if (m_wDataType==DAY_DATA){
            dwDate = m_pDay[i].m_dwTime;
            dwTime = dwDate;
        }
        else{
            dwTime = m_pDay[i].m_dwTime;
            dwDate = 20000000+dwTime/10000;
        }
        vol = m_pDay[i].m_xVolume.GetValue();
        xLastLTG = m_pGoods->GetHisLTG(hisGB, dwDate, xLastLTG);
        INT64 ltg = m_pGoods->TransVol2Hand(xLastLTG.GetValue());
        if( ltg == 0 ){
            //发生过一次ltg除法异常，进行保护
            m_mapHisHSL[dwTime] = 0;
            m_aHisHSL[i] = 0;
        }
        else{
            DWORD dwHSL = DWORD((vol*10000 + ltg/2)/ltg);
            m_mapHisHSL[dwTime] = dwHSL;
            m_aHisHSL[i] = dwHSL;
        }
    }

    m_dwCount = 1;
    m_cLoadOK = 1;
//  debug_minutes(this->m_pDay, m_dwDay);
#ifdef _DEBUG
    LOG_DBG("LoadData:%d,%d",this->m_pGoods->GetID(),m_wDataType);
#endif
}

void CDataDay::LoadToday()
{
    DeleteDay();

    if (m_pGoods==NULL)
        return;

    //DWORD dwGoodsID = m_pGoods->GetID();

    if (m_wDataType==MIN_DATA)      
    {
        CSortArrayMinute aMinute;
        m_pGoods->GetMinutes(aMinute, FALSE);

        int nSize = (int)aMinute.GetSize();
        for (int i=0; i<nSize; i++)
        {
            CMinute& min = aMinute[i];
            //if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)   //20110625
            // 修改熔断机制后的K线显示问题 [1/5/2016 qianyifan]
            //if (min.m_dwClose>0 && m_pGoods->CheckVolume(min.m_xVolume))
            if (min.m_dwClose>0)            
                AddMinute(min);
        }
        //AddEmptyMinute();
    }
}

void CDataDay::SaveData()
{
    if (m_pGoods==NULL || m_pDay==NULL)
        return;

    if (m_pGoods->m_cStation==3)    // H股不存了，读的时候用m_pGoods->m_cStation==5的
        return;

    CDataFile_Day* pDF = theApp.GetDataFile(m_pGoods->m_cStation, m_wDataType, m_wPeriod);

    if (m_wDataType==MIN_DATA)
    {
        WORD wLastMin =  m_pGoods->GetDateValue()%1000000*10000 + m_pGoods->LastMin();
        if (m_minSave.m_wTime<wLastMin)
        {
            CMinute min;
            min.m_wTime = wLastMin;
            min.CopyEmptyMinute(m_minSave, m_pGoods->IsGZQH());

            AddMinute(min);
        }
    }

    if (pDF)
        pDF->SaveData(m_pGoods->GetID(), m_pDay, m_dwDay, m_pGoods->GetDateValue(), FALSE);
}

void CDataDay::SaveTodayMin1()
{
    if (m_pGoods==NULL || m_pDay==NULL)
        return;

    if (m_pGoods->m_cStation==3)    // H股不存了，读的时候用m_pGoods->m_cStation==5的
        return;

    CDataFile_Day* pDF = &(theApp.m_dfTodayMin1);

    if (m_wDataType==MIN_DATA)
    {
        WORD wLastMin =  m_pGoods->GetDateValue()%1000000*10000 + m_pGoods->LastMin();
        if (m_minSave.m_wTime<wLastMin)
        {
            CMinute min;
            min.m_wTime = wLastMin;
            min.CopyEmptyMinute(m_minSave, m_pGoods->IsGZQH());

            AddMinute(min);
        }
    }

    if (pDF)
        pDF->SaveData(m_pGoods->GetID(), m_pDay, m_dwDay, m_pGoods->GetDateValue(), FALSE);
}
void CDataDay::AddMinutes(CSortArrayMinute& aMinuteAdd)
{
    if (m_pGoods == NULL)
        return;

    int nSize = aMinuteAdd.GetSize();
    if (nSize==0)
        return;

    DWORD minTime = m_pGoods->GetMinuteTime();

    CMinute& minFirst = aMinuteAdd[0];
    if (minFirst.m_wTime<m_minSave.m_wTime)
    {
        if (m_dwDay>0)
        {
            DWORD dwYMMDD = m_pGoods->GetDateValue()%1000000;
            int i = 0;
            for (i=m_dwDay-1; i>=0; i--)
            {
                if (m_pDay[i].m_dwTime/10000!=dwYMMDD)
                    break;
            }
            m_dwDay = i+1;
        }

        m_daySave.Clear();

        m_dwClearCount++;   //20110626

        m_minSave.Clear();
        m_minSave.m_dwClose = m_pGoods->m_dwClose;  //20110626
        m_minSave.m_dwAve = m_pGoods->m_dwClose;    //20110626
        if (m_pGoods->IsQH())
            m_minSave.m_dwTradeNum = m_pGoods->m_xPreOpenInterest.GetRawData();

        CSortArrayMinute aMinute;
        m_pGoods->GetMinutes(aMinute, FALSE);

        int nSize = (int)aMinute.GetSize();
        if (nSize>0)
        {
            for (int i=0; i<nSize; i++)
            {
                CMinute& min = aMinute[i];
                //if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)   //20110625
                
                // 修改熔断机制后的K线显示问题 [1/5/2016 qianyifan]
                //if (min.m_dwClose>0 && m_pGoods->CheckVolume(min.m_xVolume))
                if (min.m_dwClose>0)
                {
                    AddMinute(min);

                }

            }
        }
    }
    else
    {
        for (int i=0; i<nSize; i++)
        {
            CMinute& min = aMinuteAdd[i];
            //if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)   //20110625
            
            // 修改熔断机制后的K线显示问题 [1/5/2016 qianyifan]
            //if (min.m_dwClose>0 && m_pGoods->CheckVolume(min.m_xVolume))
            if (min.m_dwClose>0)
            {
                AddMinute(min);
            }
        }
    }

    //AddEmptyMinute();
}

void CDataDay::AddMinute(const CMinute& minAdd)
{
    if (m_pGoods==NULL || minAdd.m_wTime==0 || m_wDataType!=MIN_DATA)
        return;
    
    WORD wAmountDiv = m_pGoods->AmountDiv();

    WORD wPos = m_pGoods->Min2Pos(WORD(minAdd.m_wTime%10000), FALSE);
    DWORD dwTime = m_pGoods->GetDateValue()%1000000*10000+m_pGoods->Pos2Min(wPos, FALSE);
    if (dwTime<m_daySave.m_dwTime)
        return;
    

    BOOL bIsQH = m_pGoods->IsQH();

    WORD wPosSave;
    DWORD dwCloseSave;
    DWORD dwOI_Save = 0;

//  if (m_pGoods->GetDateValue()%1000000==m_daySave.m_dwTime/10000)
    if (m_daySave.m_dwTime>0)
    {
        wPosSave = m_pGoods->Min2Pos(WORD(m_daySave.m_dwTime%10000), FALSE)+1;
        dwCloseSave = m_daySave.m_dwClose;
        if (bIsQH)
            dwOI_Save = m_daySave.m_dwTradeNum;
    }
    else
    {
        wPosSave = 0;
        dwCloseSave = m_pGoods->m_dwClose;
        if (bIsQH)
            dwOI_Save = m_pGoods->m_xPreOpenInterest.GetRawData();
    }

    CDay day;

    for (WORD w=wPosSave; w<wPos; w++)
    {
        if (m_wPeriod>1 && w/m_wPeriod==wPos/m_wPeriod)
            continue;

        day.Clear();
        day.m_dwTime = m_pGoods->GetDateValue()%1000000*10000+m_pGoods->Pos2Min(w, FALSE);
        day.m_dwOpen = dwCloseSave;
        day.m_dwHigh = dwCloseSave;
        day.m_dwLow = dwCloseSave;
        day.m_dwClose = dwCloseSave;
        if (bIsQH)
            day.m_dwTradeNum = dwOI_Save;
        Add(day);
    }

    day.Clear();
    day.m_dwTime = m_pGoods->GetDateValue()%1000000*10000+m_pGoods->Pos2Min(wPos, FALSE);
    day.m_dwOpen = minAdd.m_dwOpen;
    day.m_dwHigh = minAdd.m_dwHigh;
    day.m_dwLow = minAdd.m_dwLow;
    day.m_dwClose = minAdd.m_dwClose;
    day.m_dwTradeNum = minAdd.m_dwTradeNum;     
    day.m_xVolume = m_pGoods->TransVol2Hand(minAdd.m_xVolume);
    day.m_xAmount = minAdd.m_xAmount/wAmountDiv;

    for (int i=0; i<4; i++)
    {
        day.m_dwNumOfBuy += minAdd.m_ocOrder.m_pdwNumOfBuy[i];
        day.m_dwNumOfSell += minAdd.m_ocOrder.m_pdwNumOfSell[i];

        if (i>0)
        {
            day.m_pxVolOfBuy[i-1] = m_pGoods->TransVol2Hand(minAdd.m_ocOrder.m_pxVolOfBuy[i]);
            day.m_pxVolOfSell[i-1] = m_pGoods->TransVol2Hand(minAdd.m_ocOrder.m_pxVolOfSell[i]);

            day.m_pxAmtOfBuy[i-1] = minAdd.m_ocOrder.m_pxAmtOfBuy[i]/wAmountDiv;
            day.m_pxAmtOfSell[i-1] = minAdd.m_ocOrder.m_pxAmtOfSell[i]/wAmountDiv;
        }

        ///20110814 //      day.m_xNeiPan += minAdd.m_ocTrade.m_pxVolOfSell[i]; //Q000912
        day.m_xNeiPan += m_pGoods->TransVol2Hand(minAdd.m_ocTrade.m_pxVolOfSell[i]);
    }
    ///20110814 //day.m_xNeiPan = m_pGoods->TransVol2Hand(day.m_xNeiPan);   //Q000912

    Add(day);

    //  if ((minAdd.m_xVolume.m_nBase>0  || minAdd.m_dwClose>0) && m_minSave.m_wTime<=minAdd.m_wTime)   //20110625
    if (m_minSave.m_wTime<=minAdd.m_wTime)
        m_minSave = minAdd;
}

void CDataDay::Add(const CDay& dayAdd)
{
    if (m_pGoods==NULL)
        return;

    if (m_dwDay>=m_dwBuffer)
    {
        DWORD dwDayGrowBy = g_dwDayGrowBy;
        if (dwDayGrowBy<50)
            dwDayGrowBy = 50;

        CDay* p = (CDay*)new(std::nothrow) BYTE[(m_dwBuffer+dwDayGrowBy)*sizeof(CDay)];
        if (p)
        {
            if (m_pDay)
            {
                CopyMemory(p, m_pDay, m_dwDay*sizeof(CDay));
                delete [] m_pDay;
                m_pDay = NULL;
            }
            m_dwBuffer += dwDayGrowBy;
            m_pDay = p;
        }
    }

    if (m_pDay==NULL || m_dwDay>=m_dwBuffer)
        return;

    CDay dayNew = dayAdd;
    if (m_wDataType==MIN_DATA)
    {
        WORD wPos = m_pGoods->Min2Pos(WORD(dayAdd.m_dwTime%10000), FALSE);
        dayNew.m_dwTime = dayAdd.m_dwTime/10000*10000+m_pGoods->Pos2Min(wPos/m_wPeriod*m_wPeriod+m_wPeriod-1, FALSE);
    }

    if (m_dwDay==0 || (m_wDataType==MIN_DATA && dayAdd.m_dwTime/10000!=m_daySave.m_dwTime/10000))
    {
        m_pDay[m_dwDay++] = dayNew;
    }
    else
    {
        CDay& day = m_pDay[m_dwDay-1];

        if (m_daySave.m_dwTime==dayAdd.m_dwTime)
        {
//          if (m_wPeriod==0 || m_wPeriod==1 || (day.m_xVolume.m_nBase==0 && day.m_dwClose==0)) //20110625
            if (m_wPeriod==0 || m_wPeriod==1 || day.m_dwClose==0 || m_pGoods->CheckVolume(day.m_xVolume)==FALSE)
            {
                day = dayNew;
            }
            else
            {
                if (day.m_dwOpen==0)
                    day.m_dwOpen = dayAdd.m_dwOpen;
                if (day.m_dwHigh<dayAdd.m_dwHigh)
                    day.m_dwHigh = dayAdd.m_dwHigh;
                if (day.m_dwLow==0 || day.m_dwLow>dayAdd.m_dwLow)
                    day.m_dwLow = dayAdd.m_dwLow;
                day.m_dwClose = dayAdd.m_dwClose; 

                if (m_pGoods->IsGZQH())
                    day.m_dwTradeNum = dayAdd.m_dwTradeNum;
                else
                    day.m_dwTradeNum += dayAdd.m_dwTradeNum-m_daySave.m_dwTradeNum;
                day.m_xVolume += dayAdd.m_xVolume-m_daySave.m_xVolume;
                day.m_xAmount += dayAdd.m_xAmount-m_daySave.m_xAmount;

                day.m_dwNumOfBuy += dayAdd.m_dwNumOfBuy-m_daySave.m_dwNumOfBuy;
                day.m_dwNumOfSell += dayAdd.m_dwNumOfSell-m_daySave.m_dwNumOfSell;

                for (int i=0; i<3; i++)
                {
                    day.m_pxVolOfBuy[i] += dayAdd.m_pxVolOfBuy[i]-m_daySave.m_pxVolOfBuy[i];
                    day.m_pxVolOfSell[i] += dayAdd.m_pxVolOfSell[i]-m_daySave.m_pxVolOfSell[i];

                    day.m_pxAmtOfBuy[i] += dayAdd.m_pxAmtOfBuy[i]-m_daySave.m_pxAmtOfBuy[i];
                    day.m_pxAmtOfSell[i] += dayAdd.m_pxAmtOfSell[i]-m_daySave.m_pxAmtOfSell[i];
                }

                day.m_xNeiPan += dayAdd.m_xNeiPan-m_daySave.m_xNeiPan;
            }
        }
        else
        {
            BOOL bInSamePeriod;

            if (m_wDataType==MIN_DATA)
                bInSamePeriod = m_pGoods->InSamePeriod(m_daySave.m_dwTime, dayAdd.m_dwTime, m_wDataType, m_wPeriod);
            else
                bInSamePeriod = FALSE;

            if (bInSamePeriod)
            {
                if (day.m_xVolume.m_nBase==0 && day.m_dwClose==0)
                {
                    day = dayNew;
                }
                else
                {
                    if (day.m_dwOpen==0)
                        day.m_dwOpen = dayAdd.m_dwOpen;
                    if (day.m_dwHigh<dayAdd.m_dwHigh)
                        day.m_dwHigh = dayAdd.m_dwHigh;
                    if (day.m_dwLow==0 || day.m_dwLow>dayAdd.m_dwLow)
                        day.m_dwLow = dayAdd.m_dwLow;
                    day.m_dwClose = dayAdd.m_dwClose; 

                    if (m_pGoods->IsGZQH())
                        day.m_dwTradeNum = dayAdd.m_dwTradeNum;
                    else
                        day.m_dwTradeNum += dayAdd.m_dwTradeNum;
                    day.m_xVolume += dayAdd.m_xVolume;
                    day.m_xAmount += dayAdd.m_xAmount;

                    day.m_dwNumOfBuy += dayAdd.m_dwNumOfBuy;
                    day.m_dwNumOfSell += dayAdd.m_dwNumOfSell;

                    for (int i=0; i<3; i++)
                    {
                        day.m_pxVolOfBuy[i] += dayAdd.m_pxVolOfBuy[i];
                        day.m_pxVolOfSell[i] += dayAdd.m_pxVolOfSell[i];

                        day.m_pxAmtOfBuy[i] += dayAdd.m_pxAmtOfBuy[i];
                        day.m_pxAmtOfSell[i] += dayAdd.m_pxAmtOfSell[i];
                    }

                    day.m_xNeiPan += dayAdd.m_xNeiPan;
                }
            }
            else
            {
                m_pDay[m_dwDay++] = dayNew;
            }
        }
    }

    if (m_wDataType==DAY_DATA)
        m_daySave = dayAdd;
    //  else if (dayAdd.m_xVolume.m_nBase>0 && m_daySave.m_dwTime<=dayAdd.m_dwTime)
    else if (m_daySave.m_dwTime<=dayAdd.m_dwTime)
        m_daySave = dayAdd;

    m_dwCount++;
}

void CDataDay::AddEmptyMinute()
{
    if (m_pGoods==NULL || m_pGoods->m_dwPrice==0 || m_pGoods->CheckVolume(m_pGoods->m_xVolume)==FALSE)  //20110625
        return;

    if (m_minSave.m_dwClose==0)
        m_minSave.m_dwClose = m_pGoods->m_dwClose;
    if (m_minSave.m_dwAve==0)
        m_minSave.m_dwAve = m_pGoods->m_dwClose;
    if (m_minSave.m_dwTradeNum==0 && m_pGoods->IsQH())
        m_minSave.m_dwTradeNum = m_pGoods->m_xPreOpenInterest.GetRawData();


    CMinute min;

    // BUG,添加空K线有问题，修正 [1/7/2016 qianyifan]
    //min.m_wTime = m_pGoods->Time2Min(m_pGoods->GetTimeValue(), FALSE);
    min.m_wTime = m_pGoods->GetMinuteTime();
    if (min.m_wTime>m_minSave.m_wTime)
    {
        min.CopyEmptyMinute(m_minSave, m_pGoods->IsQH());

        AddMinute(min);
    }
}

void CDataDay::LoadDayFromSpecialFile(void* pVoidDF)
{
    CDataFile_Day* pDF = (CDataFile_Day*)pVoidDF;
    DWORD dwGoodsID = m_pGoods->GetID();
    if (pDF)
    {
        CArray<CDay, CDay&> aDayLoad;

        DWORD dwLoadID;
        if (m_pGoods->m_cStation==3)
            dwLoadID = dwGoodsID+2000000;   //改成station==5的
        else
            dwLoadID = dwGoodsID;

        if (pDF->LoadData(dwLoadID, aDayLoad)==0)
        {
            //int i = 0;                                //20110625
            CSortArray<CDay> aDay;                  //20110625

            int nDayLoad = (int)aDayLoad.GetSize(); //20110625
            for (int i=0; i<nDayLoad; i++)              //20110625
                aDay.Change(aDayLoad[i]);           //20110625

            DWORD PRICE_DIVIDE = m_pGoods->GetPriceDiv2();

            DWORD dwDateValue = m_pGoods->GetDateValue();
            DWORD dwStartDate = 19000101;
            if (m_pGoods->IsGZQH()){
                if(m_pGoods->IsIF()){
                    dwStartDate = g_dwStartDateGZQH;
                }
                else if( m_pGoods->IsIC() ){
                    dwStartDate = g_dwStartDateGZIC;
                }
                else if( m_pGoods->IsIH() ){
                    dwStartDate = g_dwStartDateGZIH;
                }
            }
            else if (m_pGoods->m_cStation==1 && m_pGoods->m_dwNameCode/10000==30)
                dwStartDate = 20091030;

            CDay day;

            int nDay = int(aDay.GetSize());
            for (int i=0; i<nDay; i++)
            {
                CDay& day = aDay[i];
                if (g_IsDateGood(day.m_dwTime) && day.m_dwTime<dwDateValue && 
                    day.m_dwTime>=dwStartDate && m_pGoods->IsDayGood(day))
                {
                    if (PRICE_DIVIDE==10)
                    {
                        day.m_dwOpen = (day.m_dwOpen+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                        day.m_dwHigh = (day.m_dwHigh+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                        day.m_dwLow = (day.m_dwLow+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                        day.m_dwClose = (day.m_dwClose+5)/PRICE_DIVIDE*PRICE_DIVIDE;
                    }

                    Add(day);
                }
            }
        }
    }

}
//////////////////////////////////////////////////////////////////////

CDataMinute::CDataMinute()
{
    m_dwClose = 0;
    m_dwHigh = 0;
    m_dwLow = 0;

    m_dwClearCount = 0; //20110626

    m_aMinute.SetSize(0, 60);
}

CDataMinute::~CDataMinute()
{
    DeleteData();
}

void CDataMinute::ClearData()
{
    m_aMinute.RemoveAt(0, m_aMinute.GetSize());
    m_minSave.Clear();

    m_dwClearCount++;
}

void CDataMinute::DeleteData()
{
    CData::DeleteData();

    m_aMinute.RemoveAll();
    m_minSave.Clear();
}

void CDataMinute::LoadData()
{
    m_aMinute.RemoveAt(0, m_aMinute.GetSize());

    if (m_pGoods==NULL || m_wDataType!=MINUTE_DATA)
    {
        m_cLoadOK = -1;
        return;
    }

    m_dwClose = m_pGoods->m_dwClose;
    m_dwHigh = m_pGoods->m_dwHigh;
    m_dwLow = m_pGoods->m_dwLow;

    CSortArrayMinute aMinute;
    m_pGoods->GetMinutes(aMinute, TRUE);

    int nSize = (int)aMinute.GetSize();
    if (nSize>0)
    {
        for (int i=0; i<nSize; i++)
        {
            CMinute& min = aMinute[i];
//          if (min.m_xVolume.m_nBase>0)
//          if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)
                AddMinute(min, TRUE);
        }

        AddEmptyMinute();
    }

    m_dwCount = 1;
    m_cLoadOK = 1;
}

void CDataMinute::AddMinutes(CSortArrayMinute& aMinuteAdd)
{
    if (m_pGoods==NULL) //20110625
        return;
    if (m_pGoods->GetID()%1000000 == 927){
        LOG_DBG("AddMinutes");
    }

    int nSize = aMinuteAdd.GetSize();
    if (nSize==0)
        return;

    CMinute& minFirst = aMinuteAdd[0];
    if (minFirst.m_wTime<m_minSave.m_wTime)
    {
        m_aMinute.RemoveAt(0, m_aMinute.GetSize());

        m_dwClearCount++;   //20110626

        m_minSave.Clear();
        m_minSave.m_dwClose = m_pGoods->m_dwClose;  //20110626
        m_minSave.m_dwAve = m_pGoods->m_dwClose;    //20110626

        CSortArrayMinute aMinute;
        m_pGoods->GetMinutes(aMinute, TRUE);

        int nSize = (int)aMinute.GetSize();
        if (nSize>0)
        {
            for (int i=0; i<nSize; i++)
            {
                CMinute& min = aMinute[i];
                //if (min.m_xVolume.m_nBase>0)
                //if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)
                    AddMinute(min, TRUE);
            }
        }
    }
    else
    {
        for (int i=0; i<nSize; i++)
        {
            CMinute& min = aMinuteAdd[i];
            //if (min.m_xVolume.m_nBase>0)
            //if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)
                AddMinute(min, TRUE);
        }
    }

    AddEmptyMinute();
}

void CDataMinute::AddMinute(const CMinute& minAdd, BOOL bCheckVolume)
{
    if (m_pGoods==NULL || minAdd.m_wTime==0 || minAdd.m_dwClose==0 || m_wDataType!=MINUTE_DATA) //20110627
        return;

    BOOL bIncludeVirtual = (m_wPeriod==1 && m_pGoods->m_cStation<=1 && m_pGoods->IsIndex()==FALSE);
    if (bCheckVolume && bIncludeVirtual==FALSE && m_pGoods->CheckVolume(minAdd.m_xVolume)==FALSE)
        return;

    BOOL bIsGZQH = m_pGoods->IsGZQH();

    WORD wPos = m_pGoods->Min2Pos(WORD(minAdd.m_wTime%10000), bIncludeVirtual);

    if (m_minSave.m_wTime>minAdd.m_wTime)
        return;

    if (m_minSave.m_wTime<minAdd.m_wTime)
    {
        WORD wPosSave;
        if (m_minSave.m_wTime==0)
            wPosSave = 0;
        else
            wPosSave = m_pGoods->Min2Pos(WORD(m_minSave.m_wTime%10000), bIncludeVirtual)+1;

        if (m_minSave.m_dwClose == 0)
            m_minSave.m_dwClose = m_dwClose;
        if (m_minSave.m_dwAve == 0)
            m_minSave.m_dwAve = m_dwClose;

        for (WORD w=wPosSave; w<wPos; w++)
        {
            CMinute minNew;
            minNew.m_wTime = m_pGoods->Pos2Min(w, bIncludeVirtual); 
            minNew.CopyEmptyMinute(m_minSave, bIsGZQH);

            Add(minNew);
        }
    }

    CMinute minNew = minAdd;
    minNew.m_wTime = m_pGoods->Pos2Min(wPos, bIncludeVirtual);

    if (minNew.m_dwSellPrice==0)
        minNew.m_dwSellPrice = m_minSave.m_dwSellPrice;
    if (minNew.m_dwBuyPrice==0)
        minNew.m_dwBuyPrice = m_minSave.m_dwBuyPrice;

    if (minNew.m_dwSellPrice==0)
        minNew.m_dwSellPrice = m_pGoods->m_dwClose;
    if (minNew.m_dwBuyPrice==0)
        minNew.m_dwBuyPrice = m_pGoods->m_dwClose;

    Add(minNew);
}

void CDataMinute::Add(const CMinute& minAdd)
{
    m_aMinute.Change(minAdd);

    //  if (minAdd.m_xVolume.m_nBase>0 && m_minSave.m_wTime<=minAdd.m_wTime)
    //  if ((minAdd.m_xVolume.m_nBase>0 || minAdd.m_dwClose>0) && m_minSave.m_wTime<=minAdd.m_wTime)    //20110625
    if (m_minSave.m_wTime<=minAdd.m_wTime)
        m_minSave = minAdd;

    m_dwCount++;
}

void CDataMinute::AddEmptyMinute()
{
    if (m_pGoods==NULL || m_pGoods->m_dwPrice==0 || m_pGoods->CheckVolume(m_pGoods->m_xVolume)==FALSE)  //20110625
        return;

    if (m_minSave.m_dwClose==0)
        m_minSave.m_dwClose = m_pGoods->m_dwClose;
    if (m_minSave.m_dwAve==0)
        m_minSave.m_dwAve = m_pGoods->m_dwClose;

    CMinute min;
    min.m_wTime = m_pGoods->Time2Min(m_pGoods->GetTimeValue(), FALSE);
    if (min.m_wTime>m_minSave.m_wTime)
    {
        min.CopyEmptyMinute(m_minSave, m_pGoods->IsGZQH());

        AddMinute(min, FALSE);  //20110627
    }
}
//////////////////////////////////////////////////////////////////////////
CDataHisMin::CDataHisMin()
{
    m_dwDate = 0;

    m_dwClose = 0;
    m_dwHigh = 0;
    m_dwLow = 0;

    m_aHisMin.SetSize(0, 270);

    m_bCheckData = TRUE;//支持老版本补全分钟线
}

CDataHisMin::~CDataHisMin()
{
    DeleteData();
}

void CDataHisMin::DeleteData()
{
    CData::DeleteData();

    m_aHisMin.RemoveAll();
    m_hmSave.Clear();
}

void CDataHisMin::AddMinutes(CSortArrayMinute& aMinuteAdd)
{
    m_dwClose = m_pGoods->m_dwClose;
    m_dwHigh  = m_pGoods->m_dwHigh;
    m_dwLow   = m_pGoods->m_dwLow;
    

    BOOL bIsQH = m_pGoods->IsQH();

    int nSize = (int)aMinuteAdd.GetSize();
    if (nSize>0)
    {
        DWORD YYMMDD = m_pGoods->m_dwDateMinute%1000000;
        DWORD YYMMDD_base = YYMMDD*10000;
        CHisMin hm;

        for (int i=0; i<nSize; i++)
        {
            CMinute& min = aMinuteAdd[i];
            //if (min.m_dwClose>0 && m_pGoods->CheckVolume(min.m_xVolume))
            if (min.m_dwClose>0)
            {
                hm.m_dwTime = min.m_wTime%10000 + YYMMDD_base;
                hm.m_dwPrice = min.m_dwClose;
                hm.m_dwAve = min.m_dwAve;
                hm.m_xVolume = min.m_xVolume;

                if (bIsQH)
                    hm.m_xZJJL.SetRawData(min.m_dwTradeNum);
                else
                    hm.m_xZJJL = (min.m_ocOrder.m_pxAmtOfBuy[3]+min.m_ocOrder.m_pxAmtOfBuy[2]-min.m_ocOrder.m_pxAmtOfSell[3]-min.m_ocOrder.m_pxAmtOfSell[2])/1000;

                AddHisMin(hm);
            }
        }

        CMinute& min = aMinuteAdd[nSize -1];
        LOG_DBG("CDataHisMin::AddMinutes %d %d", min.m_wTime, min.m_dwClose);
    }
}

void CDataHisMin::LoadData()
{
    m_aHisMin.RemoveAt(0, m_aHisMin.GetSize());
    
    m_dwClose = 0;
    m_dwHigh = 0;
    m_dwLow = 0;

    if (m_pGoods==NULL || m_wDataType!=HISMIN_DATA)
    {
        m_cLoadOK = -1;
        return;
    }
    DWORD YYMMDD = m_pGoods->m_dwDateMinute%1000000;
    DWORD YYMMDD_base = YYMMDD*10000;
    //pGoods->m_dwDateSave!=pGoods->m_dwDate
    CGoodsCode gcGoodsID = m_pGoods->GetGoodsCode();
    CHisMinArray aHisMin;
    CDataFile_HisMin* pHisMinDF = theApp.GetHisMinFile(m_pGoods->m_cStation, m_pGoods->m_dwNameCode);
    pHisMinDF->LoadData(gcGoodsID, aHisMin);
    int nSize = aHisMin.GetSize();
    for(int i=0; i<nSize; i++){
        CHisMin& hm=aHisMin[i];
        if( hm.m_dwTime/10000 != YYMMDD ){
            Add(aHisMin[i]);
        }
    }
    //append new minute data
    CDataMinute* pMinData = NULL;
    {
        pMinData = (CDataMinute*)m_pGoods->TryLoadData(MINUTE_DATA, 0);
    }

    BOOL bIsQH = m_pGoods->IsQH();

    if( pMinData ){
        nSize = (int)pMinData->m_aMinute.GetSize();
        if (nSize>0)
        {
            CHisMin hm;
            for (int i=0; i<nSize; i++)
            {
                CMinute& min = pMinData->m_aMinute[i];
                //if (min.m_dwClose>0 && m_pGoods->CheckVolume(min.m_xVolume))
                if (min.m_dwClose>0)
                {
                    hm.m_dwTime = min.m_wTime + YYMMDD_base;
                    hm.m_dwPrice = min.m_dwClose;
                    hm.m_dwAve = min.m_dwAve;
                    hm.m_xVolume = min.m_xVolume;

                    if (bIsQH)
                        hm.m_xZJJL.SetRawData(min.m_dwTradeNum);
                    else
                        hm.m_xZJJL = (min.m_ocOrder.m_pxAmtOfBuy[3]+min.m_ocOrder.m_pxAmtOfBuy[2]-min.m_ocOrder.m_pxAmtOfSell[3]-min.m_ocOrder.m_pxAmtOfSell[2])/1000;

                    AddHisMin(hm);
                }
            }
        }
        pMinData->FreeData();
    }

    LOG_DBG("CDataHisMin::LoadData %d", m_pGoods->GetID());
    for(int i=0; i<m_aHisMin.GetSize(); i++){
        CHisMin& min = m_aHisMin[i];
        LogErr("m_aHisMin[%d] dwTime:%d price:%d zjjl:%lld", i, min.m_dwTime, min.m_dwPrice, min.m_xZJJL.GetValue());
    }

    m_dwCount = 1;

    m_cLoadOK = 1;

    //InterlockedIncrement(&g_lLoadData2);
}


void CDataHisMin::AddHisMin(CHisMin& hm)
{
    if (m_bCheckData==FALSE)
    {
        Add(hm);
        return;
    }

    DWORD dwDate = hm.m_dwTime/10000;
    DWORD dwTime = hm.m_dwTime%10000;
    //hm.m_dwTime %= 10000;//兼容老版本

    WORD wPos = m_pGoods->Min2Pos(WORD(dwTime), FALSE);

    if (m_hmSave.m_dwTime>hm.m_dwTime)
        return;

    if (m_hmSave.m_dwTime<hm.m_dwTime)
    {
        WORD wPosSave;
        if (m_hmSave.m_dwTime==0)
            wPosSave = 0;
        else
            wPosSave = m_pGoods->Min2Pos(WORD(m_hmSave.m_dwTime%10000), FALSE)+1;

        CHisMin hmNew = m_hmSave;
        hmNew.m_xVolume = 0;
        hmNew.m_xZJJL = 0;

        for (WORD w=wPosSave; w<wPos; w++)
        {
            hmNew.m_dwTime = dwDate*10000 + m_pGoods->Pos2Min(w, FALSE);
            Add(hmNew);
        }
    }

    CHisMin hmNew = hm;
    hm.m_dwTime = dwDate*10000 + m_pGoods->Pos2Min(wPos, FALSE);
    
    Add(hmNew);
}

void CDataHisMin::Add(CHisMin& hmAdd)
{
    m_aHisMin.Change(hmAdd);

//  if (m_hmSave.m_dwTime<=hmAdd.m_dwTime && hmAdd.m_dwPrice>0 && m_pGoods && m_pGoods->CheckVolume(hmAdd.m_xVolume))
    if (m_hmSave.m_dwTime<=hmAdd.m_dwTime && hmAdd.m_dwPrice>0)
        m_hmSave = hmAdd;

    if( hmAdd.m_dwPrice > m_dwHigh )
        m_dwHigh = hmAdd.m_dwPrice;
    if( hmAdd.m_dwPrice < m_dwLow || m_dwLow == 0 )
        m_dwLow = hmAdd.m_dwPrice;
    
    m_dwClose = hmAdd.m_dwPrice;
}

//////////////////////////////////////////////////////////////////////////
void CDataBargain::LoadData()
{
    if (m_pGoods==NULL)
    {
        m_cLoadOK = -1;
        return;
    }

    try
    {
        m_dwSize = 0;

        ASSERT(m_pGoods);

        ASSERT(m_pGoods->m_dwBargainTotal>=m_pGoods->m_wBargain);
        DWORD dwSize = m_pGoods->m_dwBargainTotal-m_pGoods->m_wBargain;
        if (dwSize>0)
        {
            CDataFile_Bargain* pDF = theApp.GetBargainFile(m_pGoods->m_cStation, m_pGoods->m_dwNameCode);
            if (pDF)
                pDF->LoadData(m_pGoods->GetGoodsCode(), m_pBuffer, m_dwSize);

            ASSERT(m_dwSize>=dwSize);
            ///???      m_dwSize = dwSize;
        }

        if (m_pGoods->m_wBargain>0)
        {
            ///???      ASSERT(m_pGoods->m_dwBargainTotal==m_dwSize+m_pGoods->m_wBargain);
            AllocData(m_dwSize+m_pGoods->m_wBargain);

            CopyMemory(m_pBuffer+m_dwSize, m_pGoods->m_pBargainBuffer, m_pGoods->m_wBargain*sizeof(CBargain));
            m_dwSize += m_pGoods->m_wBargain;
        }
    }
    catch(...)
    {
        ASSERT(FALSE);
    }

    m_cLoadOK = 1;
}

//////////////////////////////////////////////////////////////////////////

CLoadData::CLoadData()
{
}

CLoadData::~CLoadData()
{
    DeleteAll();
}

void CLoadData::DeleteAll()
{
    m_mapData.clear();
}

void CLoadData::Initialize(DWORD dwThread)
{
    CPtrArray aPtr;
    m_mapData[dwThread] = aPtr;
}

int CLoadData::DoLoadData(CMyThread& thLoadData)
{
    std::set<CData*> setData;

    if (m_rwData.AcquireWriteLock() == 0)
    {
        for (mapLoadData::iterator it = m_mapData.begin(); it != m_mapData.end(); ++it)
        {
            int num = it->second.GetSize();
            for (int i = 0; i < num; i++)
            {
                CData* pData = (CData*)it->second[i];
                if (setData.find(pData) == setData.end())
                    setData.insert(pData);
            }
            
            it->second.RemoveAll();
        }

        m_rwData.ReleaseWriteLock();
    }

    for (std::set<CData*>::const_iterator cit = setData.begin(); cit != setData.end(); ++cit)
    {
        CData* pData = (CData*)*cit;
        if (pData->m_pWR->TryAcquireReadLock() == 0)
        {
            if (pData->m_nUseCount >= 0 && pData->m_cLoadOK == 0)
            {
                pData->LoadData();
                pData->Trace("Load");
                pData->TraceCache();
            }
            pData->m_pWR->ReleaseReadLock();
        }

        if (!thLoadData.isAlive())
            return -1;
    }

    if (!thLoadData.isAlive())
        return -1;

    return 0;
}

void CLoadData::AddData(CData* pData)
{
    if (pData && pData->m_pGoods && pData->m_pWR)
    {
        if (m_rwData.TryAcquireReadLock() == 0)
        {
            DWORD dwThread = CMyThread::getCurrentThreadId();
            CPtrArray* pAry = &m_mapData[dwThread];
            pAry->Add(pData);
            pData->Trace("Add to Load queue");
            m_rwData.ReleaseReadLock();
        }
    }
}






