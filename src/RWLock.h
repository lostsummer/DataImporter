/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file RWLock.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef _RWLOCK_H_
#define _RWLOCK_H_

#include <pthread.h>
#include "Noncopyable.h"

class RWLock : private Noncopyable
{
public:
	RWLock();

	~RWLock();

	int TryAcquireReadLock();
	int AcquireReadLock();

	int TryAcquireWriteLock();
	int AcquireWriteLock();

	int ReleaseReadLock() { return unlock();}
	int ReleaseWriteLock() { return unlock();}

protected:
	int unlock();

private:
	pthread_rwlock_t rwlock;
};


class RWLock_scope_rdlock : private Noncopyable
{
public:
	RWLock_scope_rdlock(RWLock &m):rwlock(m) { rwlock.AcquireReadLock();}

	~RWLock_scope_rdlock() { rwlock.ReleaseReadLock();}

private:
	RWLock &rwlock;
};

class RWLock_scope_wrlock : private Noncopyable
{
public:
	RWLock_scope_wrlock(RWLock &m): rwlock(m) { rwlock.AcquireWriteLock();}

	~RWLock_scope_wrlock() { rwlock.ReleaseWriteLock();}
private:
	RWLock &rwlock;
};

template <bool lock = true>
class RWLocker
{
public:
	void AcquireReadLock() { rwlock.AcquireReadLock();}

	void AcquireWriteLock() { rwlock.AcquireWriteLock();}

	void ReleaseReadLock() { unlock();}

	void ReleaseWriteLock() { unlock();}

protected:
	void unlock() { rwlock.unlock();}

private:
	RWLock rwlock;
};

template<>
class RWLocker<false>
{
public:
	void AcquireReadLock() {}

	void AcquireWriteLock() {}

	void ReleaseReadLock() { unlock();}

	void ReleaseWriteLock() { unlock();}

protected:
	void unlock() {}
};


#endif

