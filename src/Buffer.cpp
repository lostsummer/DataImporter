#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <iconv.h>

#include "SysGlobal.h"
#include "AppGlobal.h"
#include "SimpleFile.h"
#include "Buffer.h"

DWORD CBuffer::m_dwPageSize = 0;

CBuffer::CBuffer(bool bNetOder)
{
	m_bNetworkOrder = bNetOder;

	m_nSize = 0;

	m_bSustainSize = false;
	m_bSingleRead = false;
	m_bNoAlloc = false;
	m_nReadPos = 0;

	m_pBase =  0;
	m_nDataSize = 0;

	m_nInitSize = 0;
	m_nMoreBytes = 0;

	if(m_dwPageSize == 0)
		m_dwPageSize = getpagesize();
	if(m_dwPageSize == 0)
		m_dwPageSize = 8192;
}

void CBuffer::Initialize(UINT nInitsize, bool bSustain, UINT nMoreBytes)
{
	m_bSustainSize = bSustain;
	m_nMoreBytes = nMoreBytes;

	ReAllocateBuffer(nInitsize);
	m_nInitSize = m_nSize;
}


CBuffer::~CBuffer()
{
	if (m_bNoAlloc)
		return;

	if(m_pBase)
		delete [] m_pBase;

	m_pBase = 0;
}
	

UINT CBuffer::GetMemSize() 
{
	return m_nSize;
}

UINT CBuffer::GetBufferLen() 
{
	if(!m_pBase) 
		return 0;

	if (m_bSingleRead == true)
		return m_nDataSize-m_nReadPos;
	else
		return m_nDataSize;
}

PBYTE CBuffer::GetBuffer(UINT nPos)
{
	if(!m_pBase) 
		return 0;

	return m_pBase + nPos;
}

UINT CBuffer::ReAllocateBuffer(UINT nRequestedSize)
{
	if (m_bNoAlloc)
		return 0;

	if(nRequestedSize <= m_nSize)
		return 0;

	UINT nNewSize = m_nSize;
	if(nNewSize < m_dwPageSize)
		nNewSize = m_dwPageSize;

	while(nRequestedSize > nNewSize)
		nNewSize *= 2;

	ASSERT(m_nDataSize < nNewSize);

	try
	{
		if (m_pBase)
		{
			PBYTE pNewBuffer = new(std::nothrow) BYTE[nNewSize];
			if (pNewBuffer)
			{
				if (m_nDataSize > 0)
					memmove(pNewBuffer, m_pBase, m_nDataSize);
				delete [] m_pBase;
				m_pBase = pNewBuffer;
				m_nSize = nNewSize;
			}
		}
		else
		{
			m_pBase = new(std::nothrow) BYTE[nNewSize];
			if (m_pBase)
			{
				m_nSize = nNewSize;
			}
		}
	}
	catch(...)
	{

	}


	return m_nSize;
}


UINT CBuffer::DeAllocateBuffer(UINT nRequestedSize)
{
	if (m_bNoAlloc)
		return 0;

	ASSERT(m_nSize >= 0);

	if(m_bSustainSize)
		return 0;

	if(m_nSize <= m_nInitSize)
		return 0;

	if(nRequestedSize < m_nDataSize)
		return 0;

	if(nRequestedSize < m_nInitSize)
		nRequestedSize = m_nInitSize;

	UINT nNewSize = m_nSize;
	while(nNewSize && nRequestedSize && nNewSize >= nRequestedSize * 2)
		nNewSize /= 2;

	if(nNewSize == m_nSize)
		return 0;

	if (nNewSize == 0)
		return 0;

	ASSERT(m_nDataSize <= nNewSize);

	try
	{
		if (m_pBase)
		{
			PBYTE pNewBuffer = new(std::nothrow) BYTE[nNewSize];
			if (pNewBuffer)
			{
				if (m_nDataSize > 0)
					memmove(pNewBuffer, m_pBase, m_nDataSize);
				delete [] m_pBase;
				m_pBase = pNewBuffer;
				m_nSize = nNewSize;
			}
		}
		else
		{
			m_pBase = new(std::nothrow) BYTE[nNewSize];
			if (m_pBase)
			{
				m_nSize = nNewSize;
			}
		}
	}
	catch(...)
	{

	}


	return m_nSize;
}

UINT CBuffer::Add(UINT nSize)
{
	if (nSize)
	{
		ReAllocateBuffer(nSize + m_nDataSize + m_nMoreBytes);

		// Advance Pointer
		m_nDataSize += nSize;
	}

	return nSize;
}

UINT CBuffer::Write(const void* pData, UINT nSize)
{
	if(pData && nSize)
	{
		ReAllocateBuffer(nSize + m_nDataSize + m_nMoreBytes);

		//memcpy(m_pBase+m_nDataSize, pData, nSize);
		memmove(m_pBase+m_nDataSize, pData, nSize);
		// Advance Pointer
		m_nDataSize += nSize;
	}

	return nSize;
}

UINT CBuffer::Insert(const void* pData, UINT nSize)
{
	if (pData && nSize)
	{
		ReAllocateBuffer(nSize + m_nDataSize + m_nMoreBytes);

		memmove(m_pBase+nSize, m_pBase, m_nDataSize);
		//memcpy(m_pBase, pData, nSize);
		memmove(m_pBase, pData, nSize);

		// Advance Pointer
		m_nDataSize += nSize;
	}

	return nSize;
}

UINT CBuffer::Read(void* pData, UINT nSize)
{
	if(!pData)	return 0;

	if (nSize)
	{
		if (m_bSingleRead)
		{
			if (nSize+m_nReadPos > m_nDataSize)
			{
				throw DATA_LACK;
				return 0;
			}

			try
			{
				//memcpy(pData, m_pBase+m_nReadPos, nSize);
				memmove(pData, m_pBase+m_nReadPos, nSize);
				m_nReadPos += nSize;
			}
			catch (...)
			{
				throw DATA_LACK;
				return 0;
			}
		}
		else
		{
			if (nSize > m_nDataSize)
			{
				throw DATA_LACK;
				return 0;
			}

			m_nDataSize -= nSize;

			//memcpy(pData, m_pBase, nSize);
			memmove(pData, m_pBase, nSize);
			if (m_nDataSize > 0)
				memmove(m_pBase, m_pBase+nSize, m_nDataSize);
		}
	}
		
	DeAllocateBuffer(m_nDataSize + m_nMoreBytes);

	return nSize;
}

UINT CBuffer::SkipData(int nSize)
{
	ASSERT(m_bSingleRead);

	if(m_bSingleRead)
	{
		if (nSize+m_nReadPos > m_nDataSize)
		{
			throw DATA_LACK;
			return 0;
		}

		m_nReadPos += nSize;

		return nSize;
	}

	return 0;
}

UINT CBuffer::ReadString(CString& strData)
{
	strData = "";

	WORD wSize;
	Read(&wSize, 2);

	if(wSize == 0)
		return 2;

	char* pcData = new(std::nothrow) char[wSize+1];
	if (pcData)
	{
		Read(pcData, wSize);
		pcData[wSize] = 0;

		strData = pcData;

		delete [] pcData;
	}

	return (wSize+2);
}

UINT CBuffer::WriteString(CString& strData)
{
	WORD wSize = strData.GetLength();
	Write(&wSize, 2);

	if (wSize)
		Write(strData.GetData(), wSize);
	
	return (UINT)(wSize+2);
}

UINT CBuffer::ReadString(std::string& strData)
{
	strData = "";

	WORD wSize;
	Read(&wSize, 2);

	if(wSize == 0)
		return 2;

	char* pcData = new(std::nothrow) char[wSize+1];
	if (pcData)
	{
		Read(pcData, wSize);
		pcData[wSize] = 0;

		strData = pcData;

		delete [] pcData;
	}

	return (wSize+2);
}

UINT CBuffer::WriteString(const std::string& strData)
{
	WORD wSize = strData.length();
	Write(&wSize, 2);

	if (wSize)
		Write(strData.c_str(), wSize);

	return (UINT)(wSize+2);
}

void CBuffer::ClearBuffer()
{
	// Force the buffer to be empty
	m_nDataSize = 0;
	m_nReadPos = 0;

	DeAllocateBuffer(0);
}

void CBuffer::FileWrite(const CString& strFileName)
{
	if(!m_pBase || !m_nDataSize)
		return;

	FILE* pFile = fopen(strFileName, "w");
	if (pFile)
	{
		try
		{
			fwrite(m_pBase, 1, m_nDataSize, pFile);
		}
		catch(...)
		{
		}

		fclose(pFile);
	}
}

void CBuffer::FileRead(const CString& strFileName)
{
	FILE* pFile = fopen(strFileName.GetData(), "r");
	if (pFile)
	{
		char* pcFileData = 0;

		try
		{
			int nLen = SimpleFile::GetFileSize(strFileName);
			if (nLen>0)
			{
				pcFileData = new(std::nothrow) char[nLen];
				if (pcFileData)
				{
					fread(pcFileData, 1, nLen, pFile);
					Write(pcFileData, nLen);

					delete [] pcFileData;
				}
			}
		}
		catch(...)
		{
			if (pcFileData)
			{
				delete [] pcFileData;
			}
		}

		fclose(pFile);
	}
}


UINT CBuffer::Delete(UINT nSize)
{
	if(nSize == 0)
		return nSize;

	if (nSize > m_nDataSize)
		nSize = m_nDataSize;

	m_nDataSize -= nSize;

	if(m_nDataSize > 0)
		memmove(m_pBase, m_pBase+nSize, m_nDataSize);
		
	DeAllocateBuffer(m_nDataSize + m_nMoreBytes);

	return nSize;
}

const CBuffer& CBuffer::operator+(CBuffer& buff)
{
	Write(buff.GetBuffer(), buff.GetBufferLen());

	return* this;
}

UINT CBuffer::DeleteEnd(UINT nSize)
{
	if(nSize > m_nDataSize)
		nSize = m_nDataSize;
		
	if(nSize)
	{
		m_nDataSize -= nSize;
		DeAllocateBuffer(m_nDataSize + m_nMoreBytes);
	}
		
	return nSize;
}

void CBuffer::WriteName(char* pcName)
{
	wchar_t pwcName[8];
	memset(pwcName, 0, 8*sizeof(wchar_t));

	char cName = mbstowcs(pwcName, pcName, 8);
	Write(&cName, 1);

	for (int c=0; c<cName; c++)
		WriteShort(pwcName[c]);
}

void CBuffer::WriteInt(int nValue)
{
	int n;
	if (m_bNetworkOrder)
		n = htonl(nValue);
	else
		n = nValue;
	Write(&n, 4);
}

XInt32 CBuffer::ReadXInt()
{
	XInt32 xValue;
	xValue.SetRawData(ReadInt());
	return xValue;
}

void CBuffer::WriteXInt(XInt32 xValue)
{
	WriteInt(xValue.GetRawData());
}

void CBuffer::WriteShort(short sValue)
{
	short s;
	if (m_bNetworkOrder)
		s = htons(sValue);
	else
		s = sValue;
	Write(&s, 2);
}

void CBuffer::WriteWORD(WORD wValue)
{
	WORD w;
	if (m_bNetworkOrder)
		w = htons(wValue);
	else
		w = wValue;
	Write(&w, 2);
}

void CBuffer::WriteChar(char cValue)
{
	Write(&cValue, 1);
}

void CBuffer::WriteUChar(unsigned char cValue)
{
	Write(&cValue, 1);
}

int CBuffer::ReadInt()
{
	int nValue;
	Read(&nValue, 4);

	if (m_bNetworkOrder)
		return ntohl(nValue);
	else
		return nValue;
}

short CBuffer::ReadShort()
{
	short sValue;
	Read(&sValue, 2);

	if (m_bNetworkOrder)
		return ntohs(sValue);
	else
		return sValue;
}

WORD CBuffer::ReadWORD()
{
	WORD wValue;
	Read(&wValue, 2);

	if (m_bNetworkOrder)
		return ntohs(wValue);
	else
		return wValue;
}

char CBuffer::ReadChar()
{
	char cValue;
	Read(&cValue, 1);

	return cValue;
}

unsigned char CBuffer::ReadUChar()
{
	unsigned char cValue;
	Read(&cValue, 1);

	return cValue;
}

void CBuffer::WriteUnicodeString(const CString& str)
{
	WORD wSize = (WORD)str.GetLength();
	if (wSize>0)
	{
		WORD wUnicodeSize = 0;
		WORD* pwUnicode = new(std::nothrow) WORD[wSize+1];
		if (pwUnicode)
		{
			char* pcGB2312 = str.GetData();
			int nDestLen = g_CodeConvert("GB18030", "UNICODE//TRANSLIT", pcGB2312, wSize, (char*)pwUnicode, wSize*2+2);
			if (nDestLen>2)
				wUnicodeSize = nDestLen/2-1;

			// 修 [8/10/2012 frantqian]
			if (nDestLen == 0)
			{
				wUnicodeSize = wSize/2; //wcslen((wchar_t *)pwUnicode) + 1;
			}

			WriteShort(wUnicodeSize);
			for (WORD w=0; w<wUnicodeSize; w++)
				WriteShort(pwUnicode[w+1]);

			delete[] pwUnicode;	
		}
		else
			WriteShort(0);
	}
	else
		WriteShort(0);
}

void CBuffer::WriteUnicodeStringEx(const std::string& str, const char* charsetFrom)
{
	WORD wSize = (WORD)str.length();
	if (wSize>0)
	{
		WORD wUnicodeSize = 0;
		WORD* pwUnicode = new(std::nothrow) WORD[wSize+1];
		if (pwUnicode)
		{
			const char* pcGB2312 = str.c_str();
			int nDestLen = g_CodeConvert(charsetFrom, "UNICODE//TRANSLIT", const_cast<char*>(pcGB2312), wSize, (char*)pwUnicode, wSize*2+2);
			if (nDestLen>2)
				wUnicodeSize = nDestLen/2-1;

			WriteWORD(wUnicodeSize);
			for (WORD w=0; w<wUnicodeSize; w++)
				WriteWORD(pwUnicode[w+1]);

			delete[] pwUnicode;	
		}
		else
			WriteWORD(0);
	}
	else
		WriteWORD(0);
}

CString CBuffer::ReadUnicodeString()
{
	CString strRet = "";

	WORD wSize = ReadWORD();
	if (wSize>0)
	{
		WORD* pwUnicode = new(std::nothrow) WORD[wSize+1];
		if (pwUnicode)
		{
			char* pcUnicode = (char*)pwUnicode;
			pcUnicode[0] = 0xff;
			pcUnicode[1] = 0xfe;

			for (WORD w=0; w<wSize; w++)
				pwUnicode[w+1] = ReadWORD();

			char* pcGB2312 = new(std::nothrow) char[wSize*2+2];
			if (pcGB2312)
			{
				int nDestLen = g_CodeConvert("UNICODE", "GB18030//TRANSLIT", pcUnicode, wSize*2+2, pcGB2312, wSize*2+2);
				if (nDestLen>0)
					strRet = pcGB2312;

				delete[] pcGB2312;
			}

			delete[] pwUnicode;
		}
		else
		{
			for (WORD w=0; w<wSize; w++)
				ReadWORD();
		}
	}

	return strRet;
}

void CBuffer::ReadUnicodeString(std::string& str)
{
	WORD wSize = ReadWORD();
	if (wSize>0)
	{
		WORD* pwUnicode = new(std::nothrow) WORD[wSize+1];
		if (pwUnicode)
		{
			char* pcUnicode = (char*)pwUnicode;
			pcUnicode[0] = 0xff;
			pcUnicode[1] = 0xfe;

			for (WORD w=0; w<wSize; w++)
				pwUnicode[w+1] = ReadWORD();

			char* pcGB18030 = new(std::nothrow) char[wSize*2+2];
			if (pcGB18030)
			{
				int nDestLen = g_CodeConvert("UNICODE", "GB18030//TRANSLIT", pcUnicode, wSize*2+2, pcGB18030, wSize*2+2);
				if (nDestLen>0)
					str = pcGB18030;

				delete[] pcGB18030;
			}

			delete[] pwUnicode;
		}
		else
		{
			for (WORD w=0; w<wSize; w++)
				ReadWORD();
		}
	}
}

void CBuffer::WriteUnicodeString(const std::string& str)
{
	WORD wSize = (WORD)str.length();
	if (wSize>0)
	{
		WORD wUnicodeSize = 0;
		WORD* pwUnicode = new(std::nothrow) WORD[wSize+1];
		if (pwUnicode)
		{
			const char* pcGB18030 = str.c_str();
			int nDestLen = g_CodeConvert("GB18030", "UNICODE//TRANSLIT", const_cast<char*>(pcGB18030), wSize, (char*)pwUnicode, wSize*2+2);
			if (nDestLen>2)
				wUnicodeSize = nDestLen/2-1;

			WriteWORD(wUnicodeSize);
			for (WORD w=0; w<wUnicodeSize; w++)
				WriteWORD(pwUnicode[w+1]);

			delete[] pwUnicode;	
		}
		else
			WriteWORD(0);
	}
	else
		WriteWORD(0);
}

void CBuffer::SetBuffer(PBYTE pData, UINT uSize)
{
	m_bNoAlloc = true;
	m_pBase = pData;
	m_nDataSize = m_nSize = uSize;
}

INT64 htonll(INT64 value)
{
	INT64 ret;
	for (int i=0;i<8;i++)
		reinterpret_cast<BYTE*>(&ret)[i] = reinterpret_cast<BYTE*>(&value)[7-i];
	return ret;
}

INT64 ntohll(INT64 value)
{
	INT64 ret;
	for (int i=0;i<8;i++)
		reinterpret_cast<BYTE*>(&ret)[i] = reinterpret_cast<BYTE*>(&value)[7-i];
	return ret;
}

void CBuffer::WriteInt64(INT64 nValue)
{
	INT64 n;
	if (m_bNetworkOrder)
		n = htonll(nValue);
	else
		n = nValue;
	Write(&n, 8);
}
INT64 CBuffer::ReadInt64()
{
	INT64 nValue;
	Read(&nValue, 8);

	if (m_bNetworkOrder)
		return ntohll(nValue);
	else
		return nValue;
}

