// DynaCompress.cpp: implementation of the CDynaCompress class.
//
//////////////////////////////////////////////////////////////////////

#include "MDS.h"
#include "SysGlobal.h"
#include "DynaCompress.h"
#include "HisCompress.h"
#include "MyTime.h"

CBitCode BC_DYNA_TIMEDIFF[] = 
{
	{0x00,	1,	'C',	0,		3},			// 0				= 3s
	{0x06,	3,	'C',	0,		6},			// 110				= 6s
	{0x02,	2,	'D',	4,		0},			// 10	+4Bit		= 4Bit
	{0x07,	3,	'O',	18,		0},			// 111	+18Bit		= 18Bit Org
};
int BCNUM_DYNA_TIMEDIFF = sizeof(BC_DYNA_TIMEDIFF)/sizeof(CBitCode);

static CBitCode BC_DYNA_OTHER[] = 
{
	{0x02,	2,	'D',	8,		0},			//10	+8Bit		= 8Bit
	{0x00,	1,	'D',   16,		256},		//0		+16Bit		= 16Bit+256
	{0x06,	3,	'D',   24,		65792},		//110	+24Bit		= 24Bit+65536+256
	{0x07,	3,	'O',   32,		0},			//111	+32Bit		= 32Bit Org
};
static int BCNUM_DYNA_OTHER = sizeof(BC_DYNA_OTHER)/sizeof(CBitCode);

CBitCode BC_DYNA_PRICE_DIFFS[] = 
{
	{0x00,	1,	'I',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'I',	6,		8},			// 10	+6Bit		= 6Bit+8
	{0x06,	3,	'I',	8,		40},		// 110	+8Bit		= 8Bit+32+8
	{0x0E,	4,	'I',	16,		168},		// 1110	+16Bit		= 16Bit+128+32+8
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
int BCNUM_DYNA_PRICE_DIFFS = sizeof(BC_DYNA_PRICE_DIFFS)/sizeof(CBitCode);

CBitCode BC_DYNA_PRICE_DIFF[] = 
{
	{0x00,	1,	'D',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'D',	6,		16},		// 10	+6Bit		= 6Bit+16
	{0x06,	3,	'D',	8,		80},		// 110	+8Bit		= 8Bit+64+16
	{0x0E,	4,	'D',	16,		336},		// 1110+16Bit		= 16Bit+256+64+16
	{0x0F,	4,	'O',	32,		0},			// 1111+32Bit		= 32Bit Org
};
int BCNUM_DYNA_PRICE_DIFF = sizeof(BC_DYNA_PRICE_DIFF)/sizeof(CBitCode);

CBitCode BC_DYNA_VOL[] = 
{
	{0x06,	3,	'D',	12,		0},			// 110	+12Bit		= 12Bit
	{0x02,	2,	'D',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x00,	1,	'D',	24,		69632},		// 0	+24Bit		= 24Bit+65536+4096
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
int BCNUM_DYNA_VOL = sizeof(BC_DYNA_VOL)/sizeof(CBitCode);

CBitCode BC_DYNA_VOL_DIFF[] = 
{
	{0x02,	2,	'H',	4,		0},			// 10	+4Bit		= (4Bit)*100
	{0x00,	1,	'H',	8,		16},		// 0	+8Bit		= (8Bit+16)*100
	{0x06,	3,	'H',	12,		272},		// 110	+12Bit		= (12Bit+256+16)*100
	{0x1C,	5,	'D',	12,		0},			// 11100+12Bit		= 12Bit
	{0x1D,	5,	'D',	16,		4096},		// 11101+16Bit		= 16Bit+4096
	{0x1E,	5,	'D',	24,		69632},		// 11110+24Bit		= 24Bit+65536+4096
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
int BCNUM_DYNA_VOL_DIFF = sizeof(BC_DYNA_VOL_DIFF)/sizeof(CBitCode);

CBitCode BC_DYNA_VOL_DIFFS[] = 
{
	{0x00,	1,	'h',	6,		0},			// 0	+6Bit		= (6Bit)*100
	{0x02,	2,	'h',	8,		32},		// 10	+8Bit		= (8Bit+32)*100
	{0x06,	3,	'h',	12,		160},		// 110	+12Bit		= (12Bit+128+32)*100
	{0x1C,	5,	'I',	12,		0},			// 11100+12Bit		= 12Bit
	{0x1D,	5,	'I',	16,		2048},		// 11101+16Bit		= 16Bit+2048
	{0x1E,	5,	'I',	24,		34816},		// 11110+24Bit		= 24Bit+32768+2048
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
int BCNUM_DYNA_VOL_DIFFS = sizeof(BC_DYNA_VOL_DIFFS)/sizeof(CBitCode);

CBitCode BC_DYNA_AMNT[] = 
{
	{0x00,	2,	'I',	16,		0},			// 00	+16Bit		= 16Bit
	{0x01,	2,	'I',	24,		32768},		// 01	+24Bit		= 24Bit+32768
	{0x02,	2,	'I',	28,		8421376},	// 10	+28Bit		= 28Bit+8388608+32768
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_AMNT = sizeof(BC_DYNA_AMNT)/sizeof(CBitCode);

CBitCode BC_DYNA_AMNT_DIFF[] = 
{
	{0x00,	2,	'I',	4,		0},			// 00	+4Bit		= 4Bit
	{0x01,	2,	'I',	8,		8},			// 01	+8Bit		= 8Bit+8
	{0x04,	3,	'I',	12,		136},		// 100	+12Bit		= 12Bit+128+8
	{0x05,	3,	'I',	16,		2184},		// 101	+16Bit		= 16Bit+2048+128+8
	{0x06,	3,	'I',	24,		34952},		// 110	+24Bit		= 24Bit+32768+2048+128+8
	{0x07,	3,	'O',	32,		0},			// 111	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_AMNT_DIFF = sizeof(BC_DYNA_AMNT_DIFF)/sizeof(CBitCode);

CBitCode BC_DYNA_QH_VOL_DIFF[] = 
{
	{0x06,	3,	'D',	12,		0},			// 110	+12Bit		= 12Bit
	{0x02,	2,	'D',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x00,	1,	'D',	24,		69632},		// 0	+24Bit		= 24Bit+65536+4096
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
int BCNUM_DYNA_QH_VOL_DIFF = sizeof(BC_DYNA_QH_VOL_DIFF)/sizeof(CBitCode);

CBitCode BC_DYNA_QH_OI_DIFF[] = 
{
	{0x00,	1,	'I',	12,		0},			// 0	+12Bit		= 12Bit
	{0x02,	2,	'I',	16,		2048},		// 10	+16Bit		= 16Bit+2048
	{0x06,	3,	'I',	24,		34816},		// 110	+24Bit		= 24Bit+32768+2048
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
int BCNUM_DYNA_QH_OI_DIFF = sizeof(BC_DYNA_QH_OI_DIFF)/sizeof(CBitCode);

CBitCode BC_DYNA_QH_VOL_DIFFS[] = 
{
	{0x00,	1,	'I',	12,		0},			// 0	+12Bit		= 12Bit
	{0x02,	2,	'I',	16,		2048},		// 10	+16Bit		= 16Bit+2048
	{0x06,	3,	'I',	24,		34816},		// 110	+24Bit		= 24Bit+32768+2048
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
int BCNUM_DYNA_QH_VOL_DIFFS = sizeof(BC_DYNA_QH_VOL_DIFFS)/sizeof(CBitCode);

CBitCode BC_DYNA_TRADENUM[] = 
{
	{0x02,	2,	'D',	8,		0},			// 10	+8Bit		= 8Bit
	{0x00,	1,	'D',	12,		256},		// 0	+12Bit		= 12Bit+256
	{0x06,	3,	'D',	16,		4352},		// 110	+16Bit		= 16Bit+4096+256
	{0x0E,	4,	'D',	24,		69888},		// 1110	+24Bit		= 24Bit+65536+4096+256
	{0x0F,	4,	'O',	32,		0},			// 1111	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_TRADENUM = sizeof(BC_DYNA_TRADENUM)/sizeof(CBitCode);

CBitCode BC_DYNA_TRADENUM_OLD[] = 
{
	{0x02,	2,	'D',	8,		0},			// 10	+8Bit		= 8Bit
	{0x00,	1,	'D',	12,		256},		// 0	+12Bit		= 12Bit+256
	{0x06,	3,	'D',	16,		4352},		// 110	+16Bit		= 16Bit+4096+256
	{0x07,	3,	'O',	24,		0},			// 111	+24Bit		= 24Bit Org
};
int BCNUM_DYNA_TRADENUM_OLD = sizeof(BC_DYNA_TRADENUM_OLD)/sizeof(CBitCode);

CBitCode BC_DYNA_TRADENUM_DIFF[] = 
{
	{0x00,	2,	'C',	0,		0},			// 00				= 0
	{0x04,	3,	'C',	0,		1},			// 100				= 1
	{0x05,	3,	'C',	0,		2},			// 101				= 2
	{0x01,	2,	'D',	4,		3},			// 01	+4Bit		= 4Bit+3
	{0x06,	3,	'D',	6,		19},		// 110	+6Bit		= 6Bit+16+3
	{0x0E,	4,	'D',	10,		83},		// 1110	+10Bit		= 10Bit+64+16+3
	{0x0F,	4,	'O',	32,		0},			// 1111	+24Bit		= 24Bit Org
};
int BCNUM_DYNA_TRADENUM_DIFF = sizeof(BC_DYNA_TRADENUM_DIFF)/sizeof(CBitCode);

CBitCode BC_DYNA_TRADENUM_DIFF_OLD[] = 
{
	{0x00,	2,	'C',	0,		0},			// 00				= 0
	{0x04,	3,	'C',	0,		1},			// 100				= 1
	{0x05,	3,	'C',	0,		2},			// 101				= 2
	{0x01,	2,	'D',	4,		3},			// 01	+4Bit		= 4Bit+3
	{0x06,	3,	'D',	6,		19},		// 110	+6Bit		= 6Bit+16+3
	{0x0E,	4,	'D',	10,		83},		// 1110	+10Bit		= 10Bit+64+16+3
	{0x0F,	4,	'O',	24,		0},			// 1111	+24Bit		= 24Bit Org
};
int BCNUM_DYNA_TRADENUM_DIFF_OLD = sizeof(BC_DYNA_TRADENUM_DIFF_OLD)/sizeof(CBitCode);

CBitCode BC_DYNA_PRICE_INS[] = 
{
	{0x0E,	4,	'C',	0,	999999},		// 1110				= 0
	{0x00,	1,	'C',	0,		1},			// 0				= 1
	{0x02,	2,	'D',	4,		2},			// 10	+4Bit		= 4Bit+2
	{0x06,	3,	'D',	16,		18},		// 110  +16Bit		= 16Bit+16+2
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
int BCNUM_DYNA_PRICE_INS = sizeof(BC_DYNA_PRICE_INS)/sizeof(CBitCode);

CBitCode BC_DYNA_PRICE_MMP[] = 
{
	{0x00,	2,	'C',	0,	999999},		// 00				= Psell1 = Pnew
	{0x01,	2,	'C',	0,		0},			// 01				= Pbuy1 = Pnew
	{0x02,	2,	'I',	4,		0},			// 10	+4Bit		= 4Bit
	{0x06,	3,	'I',	8,		8},			// 110  +8Bit		= 8Bit+8
	{0x07,	3,	'O',	32,		0},			// 111	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_PRICE_MMP = sizeof(BC_DYNA_PRICE_MMP)/sizeof(CBitCode);

CBitCode BC_DYNA_PRICE_GAP[] = 
{
	{0x00,	1,	'C',	0,		2},			// 0				= 2
	{0x02,	2,	'D',	3,		3},			// 10	+3Bit		= 3Bit+3
	{0x06,	3,	'D',	8,		11},		// 110  +8Bit		= 8Bit+8+3
	{0x07,	3,	'O',	32,		0},			// 111	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_PRICE_GAP = sizeof(BC_DYNA_PRICE_GAP)/sizeof(CBitCode);

CBitCode BC_DYNA_STRONG[] = 
{
	{0x00,	2,	'I',	8,		0},			// 00	+8Bit		= 8Bit
	{0x01,	2,	'I',	11,		128},		// 01	+11Bit		= 11Bit+128
	{0x02,	2,	'I',	13,		1152},		// 10	+13Bit		= 13Bit+1024+128
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_STRONG = sizeof(BC_DYNA_STRONG)/sizeof(CBitCode);

CBitCode BC_DYNA_STRONG_DIFF[] = 
{
	{0x00,	1,	'I',	3,		0},			// 0	+3Bit		= 3Bit
	{0x02,	2,	'I',	6,		4},			// 10	+6Bit		= 6Bit+4
	{0x06,	3,	'I',	10,		36},		// 110	+10Bit		= 10Bit+32+4
	{0x0E,	4,	'I',	14,		548},		// 1110	+14Bit		= 14Bit+512+32+4
	{0x0F,	4,	'O',	32,		0},			// 1111	+32Bit		= 32Bit Org
};
int BCNUM_DYNA_STRONG_DIFF = sizeof(BC_DYNA_STRONG_DIFF)/sizeof(CBitCode);

static CBitCode BC_DYNA_GROUPID[] = 
{
	{0x00,	1,	'D',	6,		0},			// 0	+6Bit		= 6Bit
	{0x02,	2,	'D',	8,		64},		// 10	+8Bit		= 8Bit+64
	{0x03,	2,	'O',	16,		0},			// 11   +16Bit		= 16Bit Org
};
static int BCNUM_DYNA_GROUPID = sizeof(BC_DYNA_GROUPID)/sizeof(CBitCode);

CBitCode BC_DYNA_ZLZC[] = 
{
	{0x00,	1,	'D',	5,		0},			// 0	+5Bit		= 5Bit
	{0x01,	1,	'O',	8,		0},			// 1    +8Bit		= 8Bit Org
};
int BCNUM_DYNA_ZLZC = sizeof(BC_DYNA_ZLZC)/sizeof(CBitCode);

CBitCode BC_DYNA_DDBL[] = 
{
	{0x00,	2,	'I',	8,		0},			// 00	+8Bit		= 8Bit
	{0x01,	2,	'I',	11,		128},		// 01	+11Bit		= 11Bit+128
	{0x02,	2,	'I',	13,		1152},		// 10	+13Bit		= 13Bit+1024+128
	{0x03,	2,	'O',	32,		0},			// 11	+16Bit		= 16Bit Org
};
int BCNUM_DYNA_DDBL = sizeof(BC_DYNA_DDBL)/sizeof(CBitCode);

CBitCode BC_DYNA_CPX[] = 
{
	{0x00,	1,	'I',	5,		0},			// 00	+5Bit		= 5Bit
	{0x01,	1,	'I',	8,		0},			// 01   +8Bit		= 8Bit Org
	{0x02,	1,	'O',	16,		0},			// 10   +16Bit		= 16Bit Org
};
int BCNUM_DYNA_CPX = sizeof(BC_DYNA_CPX)/sizeof(CBitCode);

//sam copy from pc client begin
static CBitCode BC_DYNA_DATE[] = 
{
	{0x00,	1,	'C',	0,		1},					//0				= 1天
	{0x02,	2,	'C',	0,		3},					//10			= 3天
	{0x0C,	4,	'C',	0,		2},					//1100			= 2天
	{0x0D,	4,	'D',	4,		4},					//1101	+4Bit	= 4Bit+4天
	{0x0E,	4,	'D',	8,		20},				//1110	+8Bit	= 8Bit+20天
	{0x0F,	4,	'O',	16,		0},					//1111	+16Bit	= 16Bit天
};
static int BCNUM_DYNA_DATE = sizeof(BC_DYNA_DATE)/sizeof(CBitCode);
//sam copy from pc client end


//////////////////////////////////////////////////////////////////////

int CDynaCompress::EncodeAllMMPPrice(CBitStream& stream, int NUM_OF_MMP, int nNumBuy, int nNumSell, PDWORD adwMMPPrice, DWORD dwPrice)
{
	if(nNumBuy < 0 || nNumSell < 0 || (NUM_OF_MMP != 5 && NUM_OF_MMP != 10)
			|| nNumBuy > NUM_OF_MMP || nNumSell > NUM_OF_MMP
			|| adwMMPPrice == NULL)
		return -1;

	int i;

	int POS_BITS = 4;
	if(NUM_OF_MMP == 10)
		POS_BITS = 5;

	// gap position
	DWORD dwGap = 0;
	int nNumOfGap = 0;
	int nGapPos = -1;
	for(i = NUM_OF_MMP-nNumBuy; i < NUM_OF_MMP+nNumSell-1; i++)
	{
		if(adwMMPPrice[i+1] - adwMMPPrice[i] != 1)
		{
			dwGap |= (1 << i);

			if(nGapPos < 0)
				nGapPos = i;

			nNumOfGap++;
		}
	}

	if(nNumOfGap == 0)
		stream.WriteDWORD(0, 2);
	else if(nNumOfGap == 1)
		stream.WriteDWORD(1, 2);
	else
		stream.WriteDWORD(2, 2);

	// buy1 / sell1
	int nBasePos = NUM_OF_MMP-1;	// buy1
	DWORD dwEncodePrice = adwMMPPrice[nBasePos];

	if(adwMMPPrice[NUM_OF_MMP] == dwPrice && dwPrice > 0)
	{
		dwEncodePrice = dwPrice+999999;
		nBasePos = NUM_OF_MMP;
	}
	else if(nNumBuy == 0)
	{
		dwEncodePrice = adwMMPPrice[NUM_OF_MMP];
		nBasePos = NUM_OF_MMP;
	}
	stream.EncodeData(dwEncodePrice, BC_DYNA_PRICE_MMP, BCNUM_DYNA_PRICE_MMP, &dwPrice);

	if(nNumOfGap == 1)
	{
		stream.WriteDWORD(nGapPos, POS_BITS);

		if(nGapPos < nBasePos)
			stream.EncodeData(adwMMPPrice[nGapPos], BC_DYNA_PRICE_GAP, BCNUM_DYNA_PRICE_GAP, &adwMMPPrice[nGapPos+1], TRUE);
		else
			stream.EncodeData(adwMMPPrice[nGapPos+1], BC_DYNA_PRICE_GAP, BCNUM_DYNA_PRICE_GAP, &adwMMPPrice[nGapPos]);
	}
	else if(nNumOfGap > 1)
	{
		stream.WriteDWORD((dwGap>>(NUM_OF_MMP-nNumBuy)), nNumBuy+nNumSell-1);

		for(i = nBasePos-1; i >= NUM_OF_MMP-nNumBuy; i--)
		{
			if(dwGap & (1 << i))
				stream.EncodeData(adwMMPPrice[i], BC_DYNA_PRICE_GAP, BCNUM_DYNA_PRICE_GAP, &adwMMPPrice[i+1], TRUE);
		}

		for(i = nBasePos; i < NUM_OF_MMP+nNumSell-1; i++)
		{
			if(dwGap & (1 << i))
				stream.EncodeData(adwMMPPrice[i+1], BC_DYNA_PRICE_GAP, BCNUM_DYNA_PRICE_GAP, &adwMMPPrice[i]);
		}
	}

	return 0;
}

int CDynaCompress::DecodeAllMMPPrice(CBitStream& stream, int NUM_OF_MMP, int nNumBuy, int nNumSell, PDWORD adwMMPPrice, DWORD dwPrice)
{
	if(nNumBuy < 0 || nNumSell < 0 || (NUM_OF_MMP != 5 && NUM_OF_MMP != 10)
			|| nNumBuy > NUM_OF_MMP || nNumSell > NUM_OF_MMP
			|| adwMMPPrice == NULL)
		return -1;

	int i;

	int POS_BITS = 4;
	if(NUM_OF_MMP == 10)
		POS_BITS = 5;

	int nNumOfGap = stream.ReadDWORD(2);

	// Pbuy1 / Psell1
	DWORD dwEncodePrice = 0;
	stream.DecodeData(dwEncodePrice, BC_DYNA_PRICE_MMP, BCNUM_DYNA_PRICE_MMP, &dwPrice);

	int nBasePos = 0;
	if(dwEncodePrice == dwPrice+999999 && dwPrice > 0)
	{
		nBasePos = NUM_OF_MMP;
		adwMMPPrice[nBasePos] = dwPrice;
	}
	else
	{
		if(nNumBuy == 0)
			nBasePos = NUM_OF_MMP;
		else
			nBasePos = NUM_OF_MMP-1;
		adwMMPPrice[nBasePos] = dwEncodePrice;
	}

	DWORD dwGap = 0;
	if(nNumOfGap == 1)
	{
		int nGapPos = stream.ReadDWORD(POS_BITS);
		dwGap = (1 << nGapPos);
	}
	else if(nNumOfGap == 2)
	{
		dwGap = stream.ReadDWORD(nNumBuy+nNumSell-1);
		dwGap <<= (NUM_OF_MMP - nNumBuy);
	}

	for(i = nBasePos-1; i >= NUM_OF_MMP-nNumBuy; i--)
	{
		if(dwGap & (1 << i))
			stream.DecodeData(adwMMPPrice[i], BC_DYNA_PRICE_GAP, BCNUM_DYNA_PRICE_GAP, &adwMMPPrice[i+1], TRUE);
		else
			adwMMPPrice[i] = adwMMPPrice[i+1] - 1;
	}

	for(i = nBasePos; i < NUM_OF_MMP+nNumSell-1; i++)
	{
		if(dwGap & (1 << i))
			stream.DecodeData(adwMMPPrice[i+1], BC_DYNA_PRICE_GAP, BCNUM_DYNA_PRICE_GAP, &adwMMPPrice[i]);
		else
			adwMMPPrice[i+1] = adwMMPPrice[i] + 1;
	}

	return 0;
}

int CDynaCompress::CompressDynamic(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP, BOOL bNew)
{
	if (pGoods == NULL || pDyna == NULL)
		return ERR_COMPRESS_PARAM;

	if (bL2Encode && pGoods->m_cLevel < 2)
		bL2Encode = FALSE;

	if (pOldDyna!=NULL && (bNeedMMP==FALSE || pDyna->m_cVirtual==pOldDyna->m_cVirtual))
		return CompressDynaData(stream, pGoods, pDyna, pOldDyna, bL2Encode, bNeedMMP, bNew);
	else
		return CompressDynaData(stream, pGoods, pDyna, bL2Encode, bNeedMMP, bNew);
}

int CDynaCompress::CompressDynamicH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	if (pGoods == NULL || pDyna == NULL)
		return ERR_COMPRESS_PARAM;

	if (pOldDyna!=NULL && pDyna->m_cVirtual==pOldDyna->m_cVirtual)
		return CompressDynaDataH(stream, pGoods, pDyna, pOldDyna);
	else
		return CompressDynaDataH(stream, pGoods, pDyna);
}

int CDynaCompress::CompressDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP, BOOL bNew)
{
	if (pGoods==NULL || pOldDyna==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	if (bL2Encode && pGoods->m_cLevel<2)
		bL2Encode = FALSE;

	BOOL bIndex = pGoods->IsIndex() || pGoods->m_cStation==2;	// 指数或者板块指数

	try
	{
		stream.WriteBOOL(bL2Encode);

		stream.WriteBOOL(TRUE);	// bRelative
		
		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);

		// time
		DWORD dwSecondsDiff = GetSecondsDiff(pOldDyna->m_dwTime, pDyna->m_dwTime);
		stream.EncodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);

		// price
		DWORD dwNumPrice = 4;		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if(pDyna->m_dwOpen == pOldDyna->m_dwOpen)
		{
			if(pDyna->m_dwHigh == pOldDyna->m_dwHigh)
			{
				if(pDyna->m_dwLow == pOldDyna->m_dwLow)
				{
					if(pDyna->m_dwPrice == pOldDyna->m_dwPrice)
						dwNumPrice = 0;
					else
						dwNumPrice = 1;	// P
				}
				else
					dwNumPrice = 2;		// PL
			}
			else
			{
				if(pDyna->m_dwLow == pOldDyna->m_dwLow)
					dwNumPrice = 3;		// PH
				else
					dwNumPrice = 4;		// PLHO
			}
		}
		else
			dwNumPrice = 4;		// PLHO

		stream.WriteDWORD(dwNumPrice, 3);

		if(dwNumPrice >= 1)		// P
		{
			stream.EncodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.EncodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.EncodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.EncodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		if (bIndex)
		{
			stream.WriteBOOL(TRUE);

			if (pDyna->m_xVolume==pOldDyna->m_xVolume && pDyna->m_xAmount==pOldDyna->m_xAmount && pDyna->m_dwTradeNum == pOldDyna->m_dwTradeNum)
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);
				// volume, amount
				stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);
				stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &pOldDyna->m_xAmount);
				if (bNew)
					stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
				else
					stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF_OLD, BCNUM_DYNA_TRADENUM_DIFF_OLD, &pOldDyna->m_dwTradeNum);
			}
		}
		else
		{
			stream.WriteBOOL(FALSE);

			// volume, amount, tradenum
			if (pDyna->m_xVolume==pOldDyna->m_xVolume && pDyna->m_xAmount==pOldDyna->m_xAmount && pDyna->m_dwTradeNum == pOldDyna->m_dwTradeNum)
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);

				stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);

				XInt32 xBase = (INT64)pOldDyna->m_xAmount + 
						((INT64)pDyna->m_xVolume - (INT64)pOldDyna->m_xVolume) * pDyna->m_dwPrice * PRICE_DIVIDE;
				stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, &xBase);

				if (bNew)
					stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
				else
					stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF_OLD, BCNUM_DYNA_TRADENUM_DIFF_OLD, &pOldDyna->m_dwTradeNum);
			}

			// mmp
			if (bNeedMMP==FALSE || (pDyna->m_cVirtual==0 && pDyna->m_cNumBuy==0 && pDyna->m_cNumSell==0))	// no MMP
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);

				if (pDyna->m_cVirtual)
				{
					stream.WriteBOOL(TRUE);

					CompressVirtualMMP(stream, pDyna, pOldDyna);
				}
				else
				{
					stream.WriteBOOL(FALSE);
	
					CompressMMP(stream, pDyna, pOldDyna, pGoods->m_cStation, bL2Encode);
				}
			}// bHasMMP
		}

		BOOL bPV;

		// PBUY, VBUY, PSELL, VSELL
		if (bL2Encode || bIndex)
		{
			bPV = TRUE;

			stream.WriteBOOL(TRUE);

			stream.EncodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPBuy);
			stream.EncodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPSell);
			stream.EncodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
		else
		{
			bPV = FALSE;
			stream.WriteBOOL(FALSE);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		if (pDyna->m_xNeiPan==pOldDyna->m_xNeiPan && pDyna->m_xCurVol==pOldDyna->m_xCurVol && pDyna->m_cCurVol==pOldDyna->m_cCurVol)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.EncodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
			stream.WriteDWORD(pDyna->m_cCurVol+1, 2);		// -1 0 1
		}

		if (pDyna->m_dwPrice5==pOldDyna->m_dwPrice5)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);
			stream.EncodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		}

		stream.EncodeData(pDyna->m_nStrong, BC_DYNA_STRONG_DIFF, BCNUM_DYNA_STRONG_DIFF, (PDWORD)&pOldDyna->m_nStrong);

		if(bL2Encode)
		{
			CompressOrderCounts(stream, pDyna->m_ocSumOrder, pOldDyna->m_ocSumOrder, bNew, TRUE);
			CompressOrderCounts(stream, pDyna->m_ocSumTrade, pOldDyna->m_ocSumTrade, bNew, FALSE);
		}

		BYTE cCheckSum = pDyna->GetChecksum(bNeedMMP, bNew, bL2Encode, bPV);
		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, BOOL bL2Encode, BOOL bNeedMMP, BOOL bNew)
{
	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	if (bL2Encode && pGoods->m_cLevel<2)
		bL2Encode = FALSE;

	BOOL bIndex = pGoods->IsIndex() || pGoods->m_cStation==2;	// 指数或者板块指数

	try
	{
		stream.WriteBOOL(bL2Encode);

		stream.WriteBOOL(FALSE);	// !bRelative

		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);

		// time
		stream.WriteDWORD(pDyna->m_dwTime, 18);

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;

		stream.EncodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.EncodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.EncodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.EncodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		if (bIndex)
		{
			stream.WriteBOOL(TRUE);

			// volume, amount
			stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);

			if (bNew)
				stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
			else
				stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_OLD, BCNUM_DYNA_TRADENUM_OLD);
		}
		else
		{
			stream.WriteBOOL(FALSE);

			// volume, amount, tradenum
			stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);

			XInt32 xBase = (INT64)pDyna->m_xVolume * ((pDyna->m_dwHigh+pDyna->m_dwLow)/2) * PRICE_DIVIDE;
			stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xBase);

			if (bNew)
				stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
			else
				stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_OLD, BCNUM_DYNA_TRADENUM_OLD);

			// mmp
			if (bNeedMMP==FALSE || (pDyna->m_cVirtual==0 && pDyna->m_cNumBuy==0 && pDyna->m_cNumSell==0))	// no MMP
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);

				if (pDyna->m_cVirtual)
				{
					stream.WriteBOOL(TRUE);

					CompressVirtualMMP(stream, pDyna);
				}
				else
				{
					stream.WriteBOOL(FALSE);
	
					CompressMMP(stream, pDyna, pGoods->m_cStation, bL2Encode);
				}
			}
		}

		BOOL bPV;
		// PBUY, VBUY, PSELL, VSELL
		if (bL2Encode || bIndex)
		{
			bPV = TRUE;
			stream.WriteBOOL(TRUE);

			DWORD dwBase = pDyna->m_dwLow*PRICE_DIVIDE;
			stream.EncodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwBase, TRUE);
			stream.EncodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			dwBase = pDyna->m_dwHigh*PRICE_DIVIDE;
			stream.EncodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwBase);
			stream.EncodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
		else
		{
			bPV = FALSE;
			stream.WriteBOOL(FALSE);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		stream.EncodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.EncodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
		stream.WriteDWORD(pDyna->m_cCurVol+1, 2);		// -1 0 1

		stream.EncodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);
		stream.EncodeData(pDyna->m_nStrong, BC_DYNA_STRONG, BCNUM_DYNA_STRONG);

		if (bL2Encode)
		{
			CompressOrderCounts(stream, pDyna->m_ocSumOrder, bNew, TRUE);
			CompressOrderCounts(stream, pDyna->m_ocSumTrade, bNew, FALSE);
		}

		BYTE cCheckSum = pDyna->GetChecksum(bNeedMMP, bNew, bL2Encode, bPV);
		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		BOOL bL2Encode = stream.ReadDWORD(1);

		BOOL bRelative = stream.ReadDWORD(1);
		if (!bRelative)
			return ExpandDynaData(stream, pGoods, pDyna, bL2Encode);

		if (pOldDyna == NULL)
			return ERR_COMPRESS_PARAM;

		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		// time
		DWORD dwSecondsDiff = 0;
		stream.DecodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);
		pDyna->m_dwTime = AddSeconds(pOldDyna->m_dwTime, dwSecondsDiff);

		// price
		pDyna->m_dwOpen = pOldDyna->m_dwOpen;
		pDyna->m_dwHigh = pOldDyna->m_dwHigh;
		pDyna->m_dwLow = pOldDyna->m_dwLow;
		pDyna->m_dwPrice = pOldDyna->m_dwPrice;

		DWORD dwNumPrice = stream.ReadDWORD(3);		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if (dwNumPrice >= 1)		// P
		{
			stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		BOOL bIsIndex = stream.ReadDWORD(1);
		if (bIsIndex)	// 指数或者板块指数
		{
			BOOL bVATChanged = stream.ReadDWORD(1);
			if (bVATChanged)
			{
				// volume, amount
				stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);
				stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &pOldDyna->m_xAmount);
				stream.DecodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
			}
			else
			{
				pDyna->m_xVolume = pOldDyna->m_xVolume;
				pDyna->m_xAmount = pOldDyna->m_xAmount;
				pDyna->m_dwTradeNum = pOldDyna->m_dwTradeNum;
			}
		}
		else
		{		
			// volume, amount, tradenum
			BOOL bVATChanged = stream.ReadDWORD(1);
			if(bVATChanged)
			{
				stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);

				XInt32 xBase = (INT64)pOldDyna->m_xAmount + 
					((INT64)pDyna->m_xVolume - (INT64)pOldDyna->m_xVolume) * pDyna->m_dwPrice * PRICE_DIVIDE;
				stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, &xBase);

				stream.DecodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
			}
			else
			{
				pDyna->m_xVolume = pOldDyna->m_xVolume;
				pDyna->m_xAmount = pOldDyna->m_xAmount;
				pDyna->m_dwTradeNum = pOldDyna->m_dwTradeNum;
			}

			// mmp
			BOOL bHasMMPData = stream.ReadDWORD(1);
			if (bHasMMPData)
			{
				BOOL bVirtualMMP = stream.ReadDWORD(1);
				if (bVirtualMMP)
					ExpandVirtualMMP(stream, pDyna, pOldDyna);
				else
					ExpandMMP(stream, pDyna, pOldDyna, pGoods->m_cStation, bL2Encode);
			}
		}

		// PBUY, VBUY, PSELL, VSELL
		BOOL bPVBuySell = stream.ReadDWORD(1);
		if (bPVBuySell)
		{
			stream.DecodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPBuy);
			stream.DecodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.DecodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPSell);
			stream.DecodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
		else
		{
			pDyna->m_dwPBuy = pOldDyna->m_dwPBuy;
			pDyna->m_xVBuy = pOldDyna->m_xVBuy;
			pDyna->m_dwPSell = pOldDyna->m_dwPSell;
			pDyna->m_xVSell = pOldDyna->m_xVSell;
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		BOOL bNPVol = stream.ReadDWORD(1);
		if (bNPVol)
		{
			stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
			pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1
		}
		else
		{
			pDyna->m_xNeiPan = pOldDyna->m_xNeiPan;
			pDyna->m_xCurVol = pOldDyna->m_xCurVol;
			pDyna->m_cCurVol = pOldDyna->m_cCurVol;
		}

		BOOL bPrice5 = stream.ReadDWORD(1);
		if(bPrice5)
			stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		else
			pDyna->m_dwPrice5 = pOldDyna->m_dwPrice5;

		DWORD dw = 0;
		stream.DecodeData(dw, BC_DYNA_STRONG_DIFF, BCNUM_DYNA_STRONG_DIFF, (PDWORD)&pOldDyna->m_nStrong);
		pDyna->m_nStrong = dw;

		if(bL2Encode)
		{
			ExpandOrderCounts(stream, pDyna->m_ocSumOrder, pOldDyna->m_ocSumOrder, pDyna, TRUE);
			ExpandOrderCounts(stream, pDyna->m_ocSumTrade, pOldDyna->m_ocSumTrade, pDyna, FALSE);
		}

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		BYTE cCheckSum = pDyna->GetChecksum(TRUE, TRUE, TRUE, TRUE);
		if (cCheckSum!=cCheckSumRecv)
		{
			LOG_ERR("ExpandDynaData(%06d, %d) %d!=%d", pGoods->GetID(), pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, BOOL bL2Encode)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		// time
		pDyna->m_dwTime = stream.ReadDWORD(18);

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;

		stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		BOOL bIsIndex = stream.ReadDWORD(1);
		if (bIsIndex)	// 指数或者板块指数
		{
			// volume, amount
			stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
			stream.DecodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
		}
		else
		{
			// volume, amount, tradenum
			stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);

			XInt32 xBase = (INT64)pDyna->m_xVolume * ((pDyna->m_dwHigh+pDyna->m_dwLow)/2) * PRICE_DIVIDE;
			stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xBase);

			stream.DecodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);

			// mmp
			BOOL bHasMMPData = stream.ReadDWORD(1);
			if (bHasMMPData)
			{
				BOOL bVirtualMMP = stream.ReadDWORD(1);;
				if (bVirtualMMP)
					ExpandVirtualMMP(stream, pDyna);
				else
					ExpandMMP(stream, pDyna, pGoods->m_cStation, bL2Encode);
			}
		}

		// PBUY, VBUY, PSELL, VSELL
		BOOL bPVBuySell = stream.ReadDWORD(1);
		if (bPVBuySell)
		{
			DWORD dwBase = pDyna->m_dwLow*PRICE_DIVIDE;
			stream.DecodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwBase, TRUE);
			stream.DecodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			dwBase = pDyna->m_dwHigh*PRICE_DIVIDE;
			stream.DecodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwBase);
			stream.DecodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
		pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1

		stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);

		DWORD dw = 0;
		stream.DecodeData(dw, BC_DYNA_STRONG, BCNUM_DYNA_STRONG);
		pDyna->m_nStrong = dw;

		if (bL2Encode)
		{
			ExpandOrderCounts(stream, pDyna->m_ocSumOrder, pDyna, TRUE);
			ExpandOrderCounts(stream, pDyna->m_ocSumTrade, pDyna, FALSE);
		}

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		BYTE cCheckSum = pDyna->GetChecksum(TRUE, TRUE, TRUE, TRUE);
		if (cCheckSum!=cCheckSumRecv)
		{
			LOG_ERR("ExpandDynaData2(%06d, %d) %d!=%d", pGoods->GetID(), pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		BOOL bRelative = stream.ReadDWORD(1);
		if (!bRelative)
			return ExpandDynaDataH(stream, pGoods, pDyna);

		if (pOldDyna == NULL)
			return ERR_COMPRESS_PARAM;

		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		// time
		DWORD dwSecondsDiff = 0;
		stream.DecodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);
		pDyna->m_dwTime = AddSeconds(pOldDyna->m_dwTime, dwSecondsDiff);

		// price
		pDyna->m_dwOpen = pOldDyna->m_dwOpen;
		pDyna->m_dwHigh = pOldDyna->m_dwHigh;
		pDyna->m_dwLow = pOldDyna->m_dwLow;
		pDyna->m_dwPrice = pOldDyna->m_dwPrice;

		DWORD dwNumPrice = stream.ReadDWORD(3);		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if (dwNumPrice >= 1)		// P
		{
			stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		// volume, amount
		BOOL bVATChanged = stream.ReadDWORD(1);
		if(bVATChanged)
		{
			stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);

			XInt32 xBase = (INT64)pOldDyna->m_xAmount + 
				((INT64)pDyna->m_xVolume - (INT64)pOldDyna->m_xVolume) * pDyna->m_dwPrice * PRICE_DIVIDE;
			stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, &xBase);
		}
		else
		{
			pDyna->m_xVolume = pOldDyna->m_xVolume;
			pDyna->m_xAmount = pOldDyna->m_xAmount;
		}

		stream.DecodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwNorminal);

		BOOL bVirtualMMP = stream.ReadDWORD(1);
		if (bVirtualMMP)
			ExpandVirtualMMP(stream, pDyna, pOldDyna);
		else
		{
			stream.DecodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, pOldDyna->m_pdwMMP+9);
			stream.DecodeData(pDyna->m_pdwMMP[10], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, pOldDyna->m_pdwMMP+10);

			if (pDyna->m_pdwMMP[9]>0)
			{
				pDyna->m_pxMMPVol[9] = 1;
				pDyna->m_cNumBuy = 1;
			}
			if (pDyna->m_pdwMMP[10]>0)
			{
				pDyna->m_pxMMPVol[10] = 1;
				pDyna->m_cNumSell = 1;
			}
		}

		BOOL bNPVol = stream.ReadDWORD(1);
		if (bNPVol)
		{
			stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
			pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1
		}
		else
		{
			pDyna->m_xNeiPan = pOldDyna->m_xNeiPan;
			pDyna->m_xCurVol = pOldDyna->m_xCurVol;
			pDyna->m_cCurVol = pOldDyna->m_cCurVol;
		}

		BOOL bPrice5 = stream.ReadDWORD(1);
		if(bPrice5)
			stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		else
			pDyna->m_dwPrice5 = pOldDyna->m_dwPrice5;

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		BYTE cCheckSum = pDyna->GetChecksumH();
		if (cCheckSum!=cCheckSumRecv)
		{
			LOG_ERR("ExpandDynaDataH(%06d, %d) %d!=%d", pGoods->GetID(), pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		// time
		pDyna->m_dwTime = stream.ReadDWORD(18);

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;

		stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		// volume, amount
		stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);

		XInt32 xBase = (INT64)pDyna->m_xVolume * ((pDyna->m_dwHigh+pDyna->m_dwLow)/2) * PRICE_DIVIDE;
		stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xBase);

		stream.DecodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);

		BOOL bVirtualMMP = stream.ReadDWORD(1);
		if (bVirtualMMP)
			ExpandVirtualMMP(stream, pDyna);
		else
		{
			stream.DecodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			stream.DecodeData(pDyna->m_pdwMMP[10], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);

			if (pDyna->m_pdwMMP[9]>0)
			{
				pDyna->m_pxMMPVol[9] = 1;
				pDyna->m_cNumBuy = 1;
			}
			if (pDyna->m_pdwMMP[10]>0)
			{
				pDyna->m_pxMMPVol[10] = 1;
				pDyna->m_cNumSell = 1;
			}
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
		pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1

		stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		BYTE cCheckSum = pDyna->GetChecksumH();
		if (cCheckSum!=cCheckSumRecv)
		{
			LOG_ERR("ExpandDynaDataH2(%06d, %d) %d!=%d", pGoods->GetID(), pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaDataQH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		BOOL bRelative = stream.ReadDWORD(1);
		if (!bRelative)
			return ExpandDynaDataQH(stream, pGoods, pDyna);

		if (pOldDyna == NULL)
			return ERR_COMPRESS_PARAM;

		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		// time
		DWORD dwSecondsDiff = 0;
		stream.DecodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);
		pDyna->m_dwTime = AddSeconds(pOldDyna->m_dwTime, dwSecondsDiff);

		// price
		pDyna->m_dwOpen = pOldDyna->m_dwOpen;
		pDyna->m_dwHigh = pOldDyna->m_dwHigh;
		pDyna->m_dwLow = pOldDyna->m_dwLow;
		pDyna->m_dwPrice = pOldDyna->m_dwPrice;

		DWORD dwNumPrice = stream.ReadDWORD(3);		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if (dwNumPrice >= 1)		// P
		{
			stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		BOOL bVATChanged = stream.ReadDWORD(1);
		if (bVATChanged)
		{
			// volume, amount, oi
			stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_QH_VOL_DIFF, BCNUM_DYNA_QH_VOL_DIFF, &pOldDyna->m_xVolume);
			stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &pOldDyna->m_xAmount);
			stream.DecodeXInt32(pDyna->m_xOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF, &pOldDyna->m_xOpenInterest);
		}
		else
		{
			pDyna->m_xVolume = pOldDyna->m_xVolume;
			pDyna->m_xAmount = pOldDyna->m_xAmount;
			pDyna->m_xOpenInterest = pOldDyna->m_xOpenInterest;
		}

		// mmp
		BOOL bHasMMPData = stream.ReadDWORD(1);
		if (bHasMMPData)
		{
			ExpandMMP(stream, pDyna, pOldDyna, pGoods->m_cStation, FALSE);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_xCurOI
		BOOL bNPVol = stream.ReadDWORD(1);
		if (bNPVol)
		{
			stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_QH_VOL_DIFF, BCNUM_DYNA_QH_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_QH_VOL_DIFF, BCNUM_DYNA_QH_VOL_DIFF);
			pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1
			stream.DecodeXInt32(pDyna->m_xCurOI, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		}
		else
		{
			pDyna->m_xNeiPan = pOldDyna->m_xNeiPan;
			pDyna->m_xCurVol = pOldDyna->m_xCurVol;
			pDyna->m_cCurVol = pOldDyna->m_cCurVol;
			pDyna->m_xCurOI = pOldDyna->m_xCurOI;
		}

		BOOL bPrice5 = stream.ReadDWORD(1);
		if (bPrice5)
			stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		else
			pDyna->m_dwPrice5 = pOldDyna->m_dwPrice5;

		BYTE cCheckSum = (char)stream.ReadDWORD(8);
		BYTE cCheckSum2 = pDyna->GetChecksumQH(TRUE, TRUE);
		if (cCheckSum!=cCheckSum2)
		{
			LOG_ERR("ExpandDynaDataQH(%06d, %d) %d!=%d", pGoods->GetID(), pDyna->m_dwTime, cCheckSum, cCheckSum2);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaDataQH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		// time
		pDyna->m_dwTime = stream.ReadDWORD(18);

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;

		stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		// volume, amount, OI
		stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		stream.DecodeXInt32(pDyna->m_xOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);

		// mmp
		BOOL bHasMMPData = stream.ReadDWORD(1);
		if (bHasMMPData)
		{
			ExpandMMP(stream, pDyna, pGoods->m_cStation, FALSE);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_xCurOI, m_dwPrice5
		stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_QH_VOL_DIFF, BCNUM_DYNA_QH_VOL_DIFF);
		pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1
		stream.DecodeXInt32(pDyna->m_xCurOI, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);

		stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);

		BYTE cCheckSum = (char)stream.ReadDWORD(8);
		BYTE cCheckSum2 = pDyna->GetChecksumQH(TRUE, TRUE);
		if (cCheckSum!=cCheckSum2)
		{
			LOG_ERR("ExpandDynaDataQH2(%06d, %d) %d!=%d", pGoods->GetID(), pDyna->m_dwTime, cCheckSum, cCheckSum2);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}


void CDynaCompress::CompressOrderCounts(CBitStream& stream, COrderCounts& ocNew, BOOL bNew, BOOL bOrder)
{
	int i;

	if (bNew==FALSE)
	{
		for (i=0; i<4; i++)
		{
			stream.EncodeData(ocNew.m_pdwNumOfBuy[i], BC_DYNA_TRADENUM_OLD, BCNUM_DYNA_TRADENUM_OLD);
			stream.EncodeData(ocNew.m_pdwNumOfSell[i], BC_DYNA_TRADENUM_OLD, BCNUM_DYNA_TRADENUM_OLD);

			stream.EncodeXInt32(ocNew.m_pxVolOfBuy[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeXInt32(ocNew.m_pxVolOfSell[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
	}
	else
	{
		for (i=0; i<4; i++)
		{
			stream.EncodeData(ocNew.m_pdwNumOfBuy[i], BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
			if (i>0 || bOrder==TRUE)
				stream.EncodeData(ocNew.m_pdwNumOfSell[i], BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);

			if (i>0 || bOrder==FALSE)
			{
				stream.EncodeXInt32(ocNew.m_pxVolOfBuy[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
				stream.EncodeXInt32(ocNew.m_pxAmtOfBuy[i], BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
			}

			if (i>0)
			{
				stream.EncodeXInt32(ocNew.m_pxVolOfSell[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
				stream.EncodeXInt32(ocNew.m_pxAmtOfSell[i], BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
			}
		}
	}
}

void CDynaCompress::CompressOrderCounts(CBitStream& stream, COrderCounts& ocNew, COrderCounts& ocOld, BOOL bNew, BOOL bOrder)
{
	int i;

	if (bNew==FALSE)
	{
		for (i=0; i<4; i++)
		{
			stream.EncodeData(ocNew.m_pdwNumOfBuy[i], BC_DYNA_TRADENUM_DIFF_OLD, BCNUM_DYNA_TRADENUM_DIFF_OLD, ocOld.m_pdwNumOfBuy+i);
			stream.EncodeData(ocNew.m_pdwNumOfSell[i], BC_DYNA_TRADENUM_DIFF_OLD, BCNUM_DYNA_TRADENUM_DIFF_OLD, ocOld.m_pdwNumOfSell+i);

			stream.EncodeXInt32(ocNew.m_pxVolOfBuy[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, ocOld.m_pxVolOfBuy+i);
			stream.EncodeXInt32(ocNew.m_pxVolOfSell[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, ocOld.m_pxVolOfSell+i);
		}
	}
	else
	{
		for (i=0; i<4; i++)
		{
			stream.EncodeData(ocNew.m_pdwNumOfBuy[i], BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, ocOld.m_pdwNumOfBuy+i);
			if (i>0 || bOrder==TRUE)
				stream.EncodeData(ocNew.m_pdwNumOfSell[i], BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, ocOld.m_pdwNumOfSell+i);

			if (i>0 || bOrder==FALSE)
			{
				stream.EncodeXInt32(ocNew.m_pxVolOfBuy[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, ocOld.m_pxVolOfBuy+i);
				stream.EncodeXInt32(ocNew.m_pxAmtOfBuy[i], BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, ocOld.m_pxAmtOfBuy+i);
			}

			if (i>0)
			{
				stream.EncodeXInt32(ocNew.m_pxVolOfSell[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, ocOld.m_pxVolOfSell+i);
				stream.EncodeXInt32(ocNew.m_pxAmtOfSell[i], BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, ocOld.m_pxAmtOfSell+i);
			}
		}
	}
}

void CDynaCompress::ExpandOrderCounts(CBitStream& stream, COrderCounts& ocNew, CDynamicValue* pDyna, BOOL bOrder)
{
	int i;

	DWORD dwSell = 0;
	XInt32 xVolBuy = 0;
	XInt32 xVolSell = 0;
	XInt32 xAmtBuy = 0;
	XInt32 xAmtSell = 0;

	for (i=0; i<4; i++)
	{
		stream.DecodeData(ocNew.m_pdwNumOfBuy[i], BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
		dwSell += ocNew.m_pdwNumOfBuy[i];

		if (i>0 || bOrder==TRUE)
		{
			stream.DecodeData(ocNew.m_pdwNumOfSell[i], BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
			dwSell += ocNew.m_pdwNumOfSell[i];
		}

		if (i>0 || bOrder==FALSE)
		{
			stream.DecodeXInt32(ocNew.m_pxVolOfBuy[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.DecodeXInt32(ocNew.m_pxAmtOfBuy[i], BC_DYNA_AMNT, BCNUM_DYNA_AMNT);

			xVolBuy += ocNew.m_pxVolOfBuy[i];
			xAmtBuy += ocNew.m_pxAmtOfBuy[i];
		}

		if (i>0)
		{
			stream.DecodeXInt32(ocNew.m_pxVolOfSell[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.DecodeXInt32(ocNew.m_pxAmtOfSell[i], BC_DYNA_AMNT, BCNUM_DYNA_AMNT);

			xVolSell += ocNew.m_pxVolOfSell[i];
			xAmtSell += ocNew.m_pxAmtOfSell[i];
		}
	}

	if (bOrder==TRUE)
	{
		ocNew.m_pxVolOfBuy[0] = pDyna->m_xVolume-xVolBuy;
		ocNew.m_pxVolOfSell[0] = pDyna->m_xVolume-xVolSell;
		ocNew.m_pxAmtOfBuy[0] = pDyna->m_xAmount-xAmtBuy;
		ocNew.m_pxAmtOfSell[0] = pDyna->m_xAmount-xAmtSell;
	}
	else
	{
		if (pDyna->m_dwTradeNum>=dwSell)
			ocNew.m_pdwNumOfSell[0] = pDyna->m_dwTradeNum-dwSell;
		ocNew.m_pxVolOfSell[0] = pDyna->m_xVolume-(xVolSell+xVolBuy);
		ocNew.m_pxAmtOfSell[0] = pDyna->m_xAmount-(xAmtSell+xAmtBuy);
	}
}

void CDynaCompress::ExpandOrderCounts(CBitStream& stream, COrderCounts& ocNew, COrderCounts& ocOld, CDynamicValue* pDyna, BOOL bOrder)
{
	int i;

	DWORD dwSell = 0;
	XInt32 xVolBuy = 0;
	XInt32 xVolSell = 0;
	XInt32 xAmtBuy = 0;
	XInt32 xAmtSell = 0;

	for (i=0; i<4; i++)
	{
		stream.DecodeData(ocNew.m_pdwNumOfBuy[i], BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, ocOld.m_pdwNumOfBuy+i);
		dwSell += ocNew.m_pdwNumOfBuy[i];

		if (i>0 || bOrder==TRUE)
		{
			stream.DecodeData(ocNew.m_pdwNumOfSell[i], BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, ocOld.m_pdwNumOfSell+i);
			dwSell += ocNew.m_pdwNumOfSell[i];
		}

		if (i>0 || bOrder==FALSE)
		{
			stream.DecodeXInt32(ocNew.m_pxVolOfBuy[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, ocOld.m_pxVolOfBuy+i);
			stream.DecodeXInt32(ocNew.m_pxAmtOfBuy[i], BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, ocOld.m_pxAmtOfBuy+i);

			xVolBuy += ocNew.m_pxVolOfBuy[i];
			xAmtBuy += ocNew.m_pxAmtOfBuy[i];
		}

		if (i>0)
		{
			stream.DecodeXInt32(ocNew.m_pxVolOfSell[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, ocOld.m_pxVolOfSell+i);
			stream.DecodeXInt32(ocNew.m_pxAmtOfSell[i], BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, ocOld.m_pxAmtOfSell+i);

			xVolSell += ocNew.m_pxVolOfSell[i];
			xAmtSell += ocNew.m_pxAmtOfSell[i];
		}
	}

	if (bOrder==TRUE)
	{
		ocNew.m_pxVolOfBuy[0] = pDyna->m_xVolume-xVolBuy;
		ocNew.m_pxVolOfSell[0] = pDyna->m_xVolume-xVolSell;
		ocNew.m_pxAmtOfBuy[0] = pDyna->m_xAmount-xAmtBuy;
		ocNew.m_pxAmtOfSell[0] = pDyna->m_xAmount-xAmtSell;
	}
	else
	{
		if (pDyna->m_dwTradeNum>=dwSell)
			ocNew.m_pdwNumOfSell[0] = pDyna->m_dwTradeNum-dwSell;
		ocNew.m_pxVolOfSell[0] = pDyna->m_xVolume-(xVolSell+xVolBuy);
		ocNew.m_pxAmtOfSell[0] = pDyna->m_xAmount-(xAmtSell+xAmtBuy);
	}
}

void CDynaCompress::CompressMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bIsQH, BOOL bL2Encode)
{
	// mmp consts
	int NUM_OF_MMP = 5;
	int MAX_PCHANGE = 3;
	int MAX_VCHANGE = 2;
	int POS_BITS = 4;

	//if (cStation==4)
	//{
	//	if (bL2Encode==FALSE)
	//	{
	//		NUM_OF_MMP = 1;
	//		MAX_PCHANGE = 1;
	//		MAX_VCHANGE = 1;
	//		POS_BITS = 2;
	//	}
	//}
	//else 
/*	
	if (bIsQH)
	{
		if (bL2Encode==FALSE)
		{
			NUM_OF_MMP = 1;
			MAX_PCHANGE = 1;
			MAX_VCHANGE = 1;
			POS_BITS = 2;
		}
	}
	else
*/
	if (bL2Encode)
	{
		NUM_OF_MMP = 10;
		MAX_PCHANGE = 6;
		MAX_VCHANGE = 3;
		POS_BITS = 5;
	}

	int nArraySize = NUM_OF_MMP*2;

	// mmp arrays
	PDWORD  pdwMMP;
	XInt32  pxMMPVol[20];

	int nNumBuy = pDyna->m_cNumBuy;
	int nNumSell = pDyna->m_cNumSell;

	pdwMMP = pDyna->m_pdwMMP+(10-NUM_OF_MMP);
	CopyMemory(pxMMPVol, pDyna->m_pxMMPVol+(10-NUM_OF_MMP), nArraySize*sizeof(XInt32));

	if(nNumBuy > NUM_OF_MMP)
		nNumBuy = NUM_OF_MMP;
	if(nNumSell > NUM_OF_MMP)
		nNumSell = NUM_OF_MMP;

	// old mmp arrays
	PDWORD  adwLastMMPPrice;
	PXInt32 axLastMMPVol;

	int nNumLastBuy = pOldDyna->m_cNumBuy;
	int nNumLastSell = pOldDyna->m_cNumSell;

	adwLastMMPPrice = pOldDyna->m_pdwMMP+(10-NUM_OF_MMP);
	axLastMMPVol = pOldDyna->m_pxMMPVol+(10-NUM_OF_MMP);

	if(nNumLastBuy > NUM_OF_MMP)
		nNumLastBuy = NUM_OF_MMP;
	if(nNumLastSell > NUM_OF_MMP)
		nNumLastSell = NUM_OF_MMP;

	const BYTE MMP_NOCHANGE = 0;	// 00
	const BYTE MMP_VCHANGE = 1;		// 01
	const BYTE MMP_PVCHANGE = 2;	// 10
	const BYTE MMP_PPVCHANGE = 3;	// 11, encode whole price array

	if(memcmp(pdwMMP, adwLastMMPPrice, nArraySize*sizeof(DWORD)) == 0
		&& memcmp(pxMMPVol, axLastMMPVol, nArraySize*sizeof(XInt32)) == 0)
		stream.WriteDWORD(MMP_NOCHANGE, 2);
	else
	{
		INT64 phMMPVol[20];
		for (int i=0; i<20; i++)
			phMMPVol[i] = pxMMPVol[i];

		const BYTE MOD_INSERT = 0;		// 0
		const BYTE MOD_DELETE = 1;		// 1

		BYTE acPriceModPos[40];		// 2*20
		BYTE acPriceModType[40];

		int nNumOfPChange = 0;

		if(nNumLastBuy == 0 && nNumLastSell == 0)
			nNumOfPChange = nArraySize;		// encode whole price array
		else	// compare price array
		{
			int nPos = 0;
			int nLastPos = 0;

			int nTop = NUM_OF_MMP + nNumSell;
			int nLastTop = NUM_OF_MMP + nNumLastSell;

			while(nPos < nTop && nLastPos < nLastTop)
			{
				if(pdwMMP[nPos] == adwLastMMPPrice[nLastPos])
				{
					if(nPos < NUM_OF_MMP && nLastPos < NUM_OF_MMP
						|| nPos >= NUM_OF_MMP && nLastPos >= NUM_OF_MMP)
						phMMPVol[nPos] = phMMPVol[nPos] - axLastMMPVol[nLastPos].GetValue();

					nPos++;
					nLastPos++;
				}
				else if(pdwMMP[nPos] > adwLastMMPPrice[nLastPos])	// delete
				{
					acPriceModPos[nNumOfPChange] = nLastPos;
					acPriceModType[nNumOfPChange++] = MOD_DELETE;

					nLastPos++;
				}
				else	// pdwMMP[nPos] < adwLastMMPPrice[nLastPos] // insert
				{
					acPriceModPos[nNumOfPChange] = nPos;
					acPriceModType[nNumOfPChange++] = MOD_INSERT;

					nPos++;
				}
			}

			while(nPos < nTop)
			{
				acPriceModPos[nNumOfPChange] = nPos++;
				acPriceModType[nNumOfPChange++] = MOD_INSERT;
			}

			if(pdwMMP[NUM_OF_MMP-1] == pDyna->m_dwPrice)
				phMMPVol[NUM_OF_MMP-1] = phMMPVol[NUM_OF_MMP-1] + pDyna->m_xVolume.GetValue() - pOldDyna->m_xVolume.GetValue();
			else if(pdwMMP[NUM_OF_MMP] == pDyna->m_dwPrice)
				phMMPVol[NUM_OF_MMP] = phMMPVol[NUM_OF_MMP] + pDyna->m_xVolume.GetValue() - pOldDyna->m_xVolume.GetValue();
		}// price compare

		if(nNumOfPChange == 0 && nNumSell == nNumLastSell)	// v change
			stream.WriteDWORD(MMP_VCHANGE, 2);
		else						// pv/ppv change, price streaming
		{
			if(nNumOfPChange <= MAX_PCHANGE)
				stream.WriteDWORD(MMP_PVCHANGE, 2);
			else
				stream.WriteDWORD(MMP_PPVCHANGE, 2);

			// nNumBuy, nNumSell
			if(nNumBuy == NUM_OF_MMP && nNumSell == NUM_OF_MMP)
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);

				stream.WriteDWORD(nNumBuy, POS_BITS-1);
				stream.WriteDWORD(nNumSell, POS_BITS-1);
			}

			// price change
			if(nNumOfPChange <= MAX_PCHANGE)
			{
				int nModCode = 8;
				int nInsPos = -1;
				if(nNumOfPChange == 2)
				{
					if(acPriceModPos[0] == 0 && acPriceModType[0] == MOD_INSERT
						&& acPriceModPos[1] == nArraySize-1 && acPriceModType[1] == MOD_DELETE)
					{
						nModCode = 0;		// ins buy5/10, del sell 5/10
						nInsPos = 0;
					}
					else if(acPriceModPos[0] == NUM_OF_MMP && acPriceModType[0] == MOD_INSERT
						&& acPriceModPos[1] == nArraySize-1 && acPriceModType[1] == MOD_DELETE)
					{
						nModCode = 1;		// ins sell1,   del sell 5/10
						nInsPos = NUM_OF_MMP;
					}
					else if(acPriceModPos[0] == 0 && acPriceModType[0] == MOD_INSERT
						&& acPriceModPos[1] == NUM_OF_MMP-1 && acPriceModType[1] == MOD_DELETE)
					{
						nModCode = 2;		// ins buy5/10,   del buy1
						nInsPos = 0;
					}
					else if(acPriceModPos[0] == 0 && acPriceModType[0] == MOD_DELETE
						&& acPriceModPos[1] == NUM_OF_MMP-1 && acPriceModType[1] == MOD_INSERT)
					{
						nModCode = 3;		// del buy5/10,   ins buy1
						nInsPos = NUM_OF_MMP-1;
					}
					else if(acPriceModPos[0] == NUM_OF_MMP && acPriceModType[0] == MOD_DELETE
						&& acPriceModPos[1] == nArraySize-1 && acPriceModType[1] == MOD_INSERT)
					{
						nModCode = 4;		// del sell1,   ins sell5/10
						nInsPos = nArraySize-1;
					}
					else if(acPriceModPos[0] == 0 && acPriceModType[0] == MOD_DELETE
						&& acPriceModPos[1] == nArraySize-1 && acPriceModType[1] == MOD_INSERT)
					{
						nModCode = 5;		// del buy5/10,   ins sell5/10
						nInsPos = nArraySize-1;
					}
				}

				if(nModCode < 8)	// special case, just insert one price
				{
					stream.WriteDWORD(nModCode, 4);		// 0 + 3bit code

					DWORD dwPriceInsGap = 999999;		// price = 0
					if(pdwMMP[nInsPos] > 0)
					{
						if(nInsPos > 0)
							dwPriceInsGap = pdwMMP[nInsPos] - pdwMMP[nInsPos-1];
						else
							dwPriceInsGap = pdwMMP[1] - pdwMMP[0];
					}

					stream.EncodeData(dwPriceInsGap, BC_DYNA_PRICE_INS, BCNUM_DYNA_PRICE_INS);
				}
				else				// encode price change one by one
				{
					nModCode += nNumOfPChange;			// MAX_PCHANGE must < 8
					stream.WriteDWORD(nModCode, 4);		// 1 + 3bit nNumOfPChange

					for(int i = 0; i < nNumOfPChange; i++)
					{
						int nPricePos = acPriceModPos[i];
						stream.WriteDWORD(nPricePos, POS_BITS);
						stream.WriteDWORD(acPriceModType[i], 1);

						if(acPriceModType[i] == MOD_INSERT)
						{
							DWORD dwPriceInsGap = 999999;
							if(pdwMMP[nPricePos] > 0)
							{
								if(nPricePos > 0)
									dwPriceInsGap = pdwMMP[nPricePos] - pdwMMP[nPricePos-1];
								else		// pos == 0
									dwPriceInsGap = pDyna->m_dwPrice - pdwMMP[0];
							}

							stream.EncodeData(dwPriceInsGap, BC_DYNA_PRICE_INS, BCNUM_DYNA_PRICE_INS);
						}
					}
				}
			}// part price encoding
			else	// nNumOfPChange > MAX_PCHANGE, encode whole price array
			{
				EncodeAllMMPPrice(stream, NUM_OF_MMP, nNumBuy, nNumSell, pdwMMP, pDyna->m_dwPrice);
			}
		}// p changed

		// volume streaming
		DWORD dwVolChange = 0;
		int nNumOfVChange = 0;
		for(int i = 0; i < nArraySize; i++)
		{
			if(phMMPVol[i] != 0)
			{
				dwVolChange |= (1 << i);
				nNumOfVChange++;
			}
		}

		if(nNumOfVChange <= MAX_VCHANGE)
		{
			stream.WriteBOOL(FALSE);
			stream.WriteDWORD(nNumOfVChange, 2);	// MAX_VCHANGE <= 3

			for(int i = 0; i < nArraySize; i++)
			{
				if(dwVolChange & (1 << i))
				{
					stream.WriteDWORD(i, POS_BITS);
					XInt32 xVol = phMMPVol[i];
					if (bIsQH) // 股指期货
						stream.EncodeXInt32(xVol, BC_DYNA_QH_VOL_DIFFS, BCNUM_DYNA_QH_VOL_DIFFS);
					else
						stream.EncodeXInt32(xVol, BC_DYNA_VOL_DIFFS, BCNUM_DYNA_VOL_DIFFS);
				}
			}
		}
		else
		{
			stream.WriteBOOL(TRUE);

			stream.WriteDWORD((dwVolChange>>(NUM_OF_MMP - nNumBuy)), nNumBuy+nNumSell);

			for(int i = 0; i < nArraySize; i++)
			{
				if(dwVolChange & (1 << i))
				{
					XInt32 xVol = phMMPVol[i];
					if (bIsQH) // 股指期货
						stream.EncodeXInt32(xVol, BC_DYNA_QH_VOL_DIFFS, BCNUM_DYNA_QH_VOL_DIFFS);
					else
						stream.EncodeXInt32(xVol, BC_DYNA_VOL_DIFFS, BCNUM_DYNA_VOL_DIFFS);
				}
			}
		}
	}// mmp changed
}

void CDynaCompress::CompressMMP(CBitStream& stream, CDynamicValue* pDyna, BOOL bIsQH, BOOL bL2Encode)
{
	// mmp consts
	int NUM_OF_MMP = 5;
	int MAX_PCHANGE = 3;
	int MAX_VCHANGE = 2;
	int POS_BITS = 4;
/*
	if (bIsQH)
	{
		if (bL2Encode==FALSE)
		{
			NUM_OF_MMP = 1;
			MAX_PCHANGE = 1;
			MAX_VCHANGE = 1;
			POS_BITS = 2;
		}
	}
	else
*/
	if (bL2Encode)
	{
		NUM_OF_MMP = 10;
		MAX_PCHANGE = 6;
		MAX_VCHANGE = 3;
		POS_BITS = 5;
	}

	int nArraySize = NUM_OF_MMP*2;

	// mmp arrays
	PDWORD  adwMMPPrice;
	PXInt32 axMMPVol;

	int nNumBuy = pDyna->m_cNumBuy;
	int nNumSell = pDyna->m_cNumSell;

	adwMMPPrice = pDyna->m_pdwMMP+(10-NUM_OF_MMP);
	axMMPVol = pDyna->m_pxMMPVol+(10-NUM_OF_MMP);

	if(nNumBuy > NUM_OF_MMP)
		nNumBuy = NUM_OF_MMP;
	if(nNumSell > NUM_OF_MMP)
		nNumSell = NUM_OF_MMP;

	// nNumBuy, nNumSell
	if(nNumBuy == NUM_OF_MMP && nNumSell == NUM_OF_MMP)
		stream.WriteBOOL(FALSE);
	else
	{
		stream.WriteBOOL(TRUE);

		stream.WriteDWORD(nNumBuy, POS_BITS-1);
		stream.WriteDWORD(nNumSell, POS_BITS-1);
	}

	EncodeAllMMPPrice(stream, NUM_OF_MMP, nNumBuy, nNumSell, adwMMPPrice, pDyna->m_dwPrice);

	for(int i = NUM_OF_MMP-nNumBuy; i < NUM_OF_MMP+nNumSell; i++)
	{
		if (bIsQH) // 期货
			stream.EncodeXInt32(axMMPVol[i], BC_DYNA_QH_VOL_DIFF, BCNUM_DYNA_QH_VOL_DIFF);
		else
			stream.EncodeXInt32(axMMPVol[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
	}
}

void CDynaCompress::ExpandMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bIsQH, BOOL bL2Encode)
{
	// mmp consts
	int NUM_OF_MMP = 5;
	int MAX_PCHANGE = 3;
	int MAX_VCHANGE = 2;
	int POS_BITS = 4;
	if(bL2Encode)
	{
		NUM_OF_MMP = 10;
		MAX_PCHANGE = 6;
		MAX_VCHANGE = 3;
		POS_BITS = 5;
	}

	int nArraySize = NUM_OF_MMP*2;

	// mmp arrays
	PDWORD pdwMMP;
	PXInt32 pxMMPVol;

	if(!bL2Encode)
	{
		pdwMMP = pDyna->m_pdwMMP + 5;	
		pxMMPVol = pDyna->m_pxMMPVol + 5;
	}
	else
	{
		pdwMMP = pDyna->m_pdwMMP;
		pxMMPVol = pDyna->m_pxMMPVol;
	}

	BYTE& cNumBuy = pDyna->m_cNumBuy;
	BYTE& cNumSell = pDyna->m_cNumSell;

	// old mmp arrays
	PDWORD  adwLastMMPPrice;
	PXInt32 axLastMMPVol;

	BYTE cNumLastBuy = pOldDyna->m_cNumBuy;
	BYTE cNumLastSell = pOldDyna->m_cNumSell;

	if(!bL2Encode)
	{
		adwLastMMPPrice = pOldDyna->m_pdwMMP + 5;
		axLastMMPVol = pOldDyna->m_pxMMPVol + 5;

		if(cNumLastBuy > NUM_OF_MMP)
			cNumLastBuy = NUM_OF_MMP;
		if(cNumLastSell > NUM_OF_MMP)
			cNumLastSell = NUM_OF_MMP;
	}
	else
	{
		adwLastMMPPrice = pOldDyna->m_pdwMMP;
		axLastMMPVol = pOldDyna->m_pxMMPVol;
	}

	const BYTE MMP_NOCHANGE = 0;	// 00
	const BYTE MMP_VCHANGE = 1;		// 01
	const BYTE MMP_PVCHANGE = 2;	// 10
	const BYTE MMP_PPVCHANGE = 3;	// 11, encode whole price array

	BYTE cPVchange = (BYTE)stream.ReadDWORD(2);
	if(cPVchange == MMP_NOCHANGE)
	{
		CopyMemory(pdwMMP, adwLastMMPPrice, nArraySize*sizeof(DWORD));
		CopyMemory(pxMMPVol, axLastMMPVol, nArraySize*sizeof(XInt32));

		cNumBuy = cNumLastBuy;
		cNumSell = cNumLastSell;
	}
	else
	{
		INT64 phMMPVol[20];
		ZeroMemory(phMMPVol, sizeof(INT64)*20);

		if(cPVchange == MMP_VCHANGE)
		{
			CopyMemory(pdwMMP, adwLastMMPPrice, nArraySize*sizeof(DWORD));

			cNumBuy = cNumLastBuy;
			cNumSell = cNumLastSell;
		}
		else	// pv/ppv
		{
			BOOL bNumBuySell = stream.ReadDWORD(1);
			if(bNumBuySell)
			{
				cNumBuy = (BYTE)stream.ReadDWORD(POS_BITS-1);
				cNumSell = (BYTE)stream.ReadDWORD(POS_BITS-1);
			}
			else
			{
				cNumBuy = NUM_OF_MMP;
				cNumSell = NUM_OF_MMP;
			}

			if(cPVchange == MMP_PVCHANGE)
			{
				int nModCode = stream.ReadDWORD(4);
				if(nModCode < 8)	// special case, just insert one price
				{
					int nInsPos = -1;
					int nDelPos = -1;

					if(nModCode == 0)		// ins buy5/10, del sell 5/10
					{
						nInsPos = 0;
						nDelPos = nArraySize-1;
					}
					else if(nModCode == 1)	// ins sell1,   del sell 5/10
					{
						nInsPos = NUM_OF_MMP;
						nDelPos = nArraySize-1;
					}
					else if(nModCode == 2)	// ins buy5/10,   del buy1
					{
						nInsPos = 0;
						nDelPos = NUM_OF_MMP-1;
					}
					else if(nModCode == 3)	// del buy5/10,   ins buy1
					{
						nInsPos = NUM_OF_MMP-1;
						nDelPos = 0;
					}
					else if(nModCode == 4)	// del sell1,   ins sell5/10
					{
						nInsPos = nArraySize-1;
						nDelPos = NUM_OF_MMP;
					}
					else if(nModCode == 5)	// del buy5/10,   ins sell5/10
					{
						nInsPos = nArraySize-1;
						nDelPos = 0;
					}

					CopyMemory(pdwMMP, adwLastMMPPrice, nArraySize*sizeof(DWORD));
					if(nInsPos < nDelPos)
						MoveMemory(&pdwMMP[nInsPos+1], &pdwMMP[nInsPos], (nDelPos-nInsPos)*sizeof(DWORD));
					else
						MoveMemory(&pdwMMP[nDelPos], &pdwMMP[nDelPos+1], (nInsPos-nDelPos)*sizeof(DWORD));

					DWORD dwPriceInsGap = 0;
					stream.DecodeData(dwPriceInsGap, BC_DYNA_PRICE_INS, BCNUM_DYNA_PRICE_INS);
					if(dwPriceInsGap == 999999)
						pdwMMP[nInsPos] = 0;
					else
					{
						if(nInsPos > 0)
							pdwMMP[nInsPos] = pdwMMP[nInsPos-1] + dwPriceInsGap;
						else
							pdwMMP[0] = pdwMMP[1] - dwPriceInsGap;
					}

					for(int i = NUM_OF_MMP+cNumSell; i < nArraySize; i++)
						pdwMMP[i] = 0;
				}
				else	// decode price change one by one
				{
					int nNumOfPChange = nModCode - 8;

					if(nNumOfPChange == 0)
						CopyMemory(pdwMMP, adwLastMMPPrice, (NUM_OF_MMP+cNumSell)*sizeof(DWORD));
					else
					{
						const BYTE MOD_INSERT = 0;		// 0
						const BYTE MOD_DELETE = 1;		// 1

						int nChngPos = 0;

						int nPricePos = stream.ReadDWORD(POS_BITS);
						int nPriceModType = stream.ReadDWORD(1);
						DWORD dwPriceInsGap = 0;
						if(nPriceModType == MOD_INSERT)
							stream.DecodeData(dwPriceInsGap, BC_DYNA_PRICE_INS, BCNUM_DYNA_PRICE_INS);

						int nPos = 0;
						int nLastPos = 0;

						int nTop = NUM_OF_MMP + cNumSell;
						int nLastTop = NUM_OF_MMP + cNumLastSell;

						while(nPos < nTop)
						{
							BOOL bChange = FALSE;

							if(nPricePos == nLastPos && nPriceModType == MOD_DELETE)
							{
								nLastPos++;
								bChange = TRUE;
							}
							else if(nPricePos == nPos && nPriceModType == MOD_INSERT)
							{
								if(dwPriceInsGap == 999999)
									pdwMMP[nPos] = 0;
								else
								{
									if(nPos > 0)
										pdwMMP[nPos] = pdwMMP[nPos-1] + dwPriceInsGap;
									else
										pdwMMP[0] = pDyna->m_dwPrice - dwPriceInsGap;
								}

								nPos++;

								bChange = TRUE;
							}
							else
							{
								ASSERT(nLastPos < nLastTop);

								if(nLastPos < nLastTop)
									pdwMMP[nPos] = adwLastMMPPrice[nLastPos];

								nPos++;
								nLastPos++;
							}

							if(bChange)
							{
								nChngPos++;
								if(nChngPos < nNumOfPChange)	// read next one
								{
									nPricePos = stream.ReadDWORD(POS_BITS);
									nPriceModType = stream.ReadDWORD(1);
									dwPriceInsGap = 0;
									if(nPriceModType == MOD_INSERT)
										stream.DecodeData(dwPriceInsGap, BC_DYNA_PRICE_INS, BCNUM_DYNA_PRICE_INS);
								}
								else		// change list end
									nPricePos = -1;
							}
						}
					}// nNumOfPChange > 0
				}// decode price change one by one
			}// cPVchange == MMP_PVCHANGE
			else	// cPVchange == MMP_PPVCHANGE
			{
				DecodeAllMMPPrice(stream, NUM_OF_MMP, cNumBuy, cNumSell, pdwMMP, pDyna->m_dwPrice);
			}// cPVchange == MMP_PPVCHANGE
		}// p changed

		// volume decoding
		if(cNumLastBuy > 0 || cNumLastSell > 0)
		{
			int nPos = NUM_OF_MMP - cNumBuy;
			int nLastPos = NUM_OF_MMP - cNumLastBuy;

			while(nPos < NUM_OF_MMP + cNumSell && nLastPos < NUM_OF_MMP + cNumLastSell)
			{
				if(pdwMMP[nPos] == adwLastMMPPrice[nLastPos])
				{
					if(nPos < NUM_OF_MMP && nLastPos < NUM_OF_MMP
						|| nPos >= NUM_OF_MMP && nLastPos >= NUM_OF_MMP)
						phMMPVol[nPos] = axLastMMPVol[nLastPos];

					nPos++;
					nLastPos++;
				}
				else if(pdwMMP[nPos] > adwLastMMPPrice[nLastPos])	// delete
					nLastPos++;
				else	// pdwMMP[nPos] < adwLastMMPPrice[nLastPos] // insert
					nPos++;
			}

			if(pdwMMP[NUM_OF_MMP-1] == pDyna->m_dwPrice)
				phMMPVol[NUM_OF_MMP-1] = phMMPVol[NUM_OF_MMP-1] - (pDyna->m_xVolume.GetValue() - pOldDyna->m_xVolume.GetValue());
			else if(pdwMMP[NUM_OF_MMP] == pDyna->m_dwPrice)
				phMMPVol[NUM_OF_MMP] = phMMPVol[NUM_OF_MMP] - (pDyna->m_xVolume.GetValue() - pOldDyna->m_xVolume.GetValue());
		}

		XInt32 xV = 0;

		BOOL bMaxVChange = stream.ReadDWORD(1);
		if(!bMaxVChange)
		{
			int nNumOfVChange = stream.ReadDWORD(2);
			for(int i = 0; i < nNumOfVChange; i++)
			{
				int nVPos = stream.ReadDWORD(POS_BITS);
				if (bIsQH) // 期货
					stream.DecodeXInt32(xV, BC_DYNA_QH_VOL_DIFFS, BCNUM_DYNA_QH_VOL_DIFFS);
				else
					stream.DecodeXInt32(xV, BC_DYNA_VOL_DIFFS, BCNUM_DYNA_VOL_DIFFS);
				phMMPVol[nVPos] = phMMPVol[nVPos] + xV.GetValue();
			}
		}
		else
		{
			DWORD dwVolChange = stream.ReadDWORD(cNumBuy+cNumSell);
			dwVolChange <<= (NUM_OF_MMP - cNumBuy);

			for(int i = 0; i < nArraySize; i++)
			{
				if(dwVolChange & (1 << i))
				{
					if (bIsQH) // 期货
						stream.DecodeXInt32(xV, BC_DYNA_QH_VOL_DIFFS, BCNUM_DYNA_QH_VOL_DIFFS);
					else
						stream.DecodeXInt32(xV, BC_DYNA_VOL_DIFFS, BCNUM_DYNA_VOL_DIFFS);
					phMMPVol[i] = phMMPVol[i] + xV.GetValue();
				}
			}
		}

		for (int i=0; i<20; i++)
			pxMMPVol[i] = phMMPVol[i];
	}// mmp changed
}

void CDynaCompress::ExpandMMP(CBitStream& stream, CDynamicValue* pDyna, BOOL bIsQH, BOOL bL2Encode)
{
	// mmp consts
	int NUM_OF_MMP = 5;
	int MAX_PCHANGE = 3;
	int MAX_VCHANGE = 2;
	int POS_BITS = 4;
	if(bL2Encode)
	{
		NUM_OF_MMP = 10;
		MAX_PCHANGE = 6;
		MAX_VCHANGE = 3;
		POS_BITS = 5;
	}

	int nArraySize = NUM_OF_MMP*2;

	// mmp arrays
	PDWORD adwMMPPrice;
	PXInt32 axMMPVol;

	if(!bL2Encode)
	{
		adwMMPPrice = pDyna->m_pdwMMP + 5;
		axMMPVol = pDyna->m_pxMMPVol + 5;
	}
	else
	{
		adwMMPPrice = pDyna->m_pdwMMP;
		axMMPVol = pDyna->m_pxMMPVol;
	}

	BYTE& cNumBuy = pDyna->m_cNumBuy;
	BYTE& cNumSell = pDyna->m_cNumSell;

	// NumBuy, NumSell
	BOOL bNumBuySell = stream.ReadDWORD(1);
	if(bNumBuySell)
	{
		cNumBuy = (BYTE)stream.ReadDWORD(POS_BITS-1);
		cNumSell = (BYTE)stream.ReadDWORD(POS_BITS-1);
	}
	else
	{
		cNumBuy = NUM_OF_MMP;
		cNumSell = NUM_OF_MMP;
	}

	// whole price array
	DecodeAllMMPPrice(stream, NUM_OF_MMP, cNumBuy, cNumSell, adwMMPPrice, pDyna->m_dwPrice);

	for(int i = NUM_OF_MMP-cNumBuy; i < NUM_OF_MMP+cNumSell; i++)
	{
		if (bIsQH) // 期货
			stream.DecodeXInt32(axMMPVol[i], BC_DYNA_QH_VOL_DIFF, BCNUM_DYNA_QH_VOL_DIFF);
		else
			stream.DecodeXInt32(axMMPVol[i], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
	}
}

int CDynaCompress::CompressStatic(CBitStream& stream, CGoods* pGoods, WORD wVersion)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		if (PRICE_DIVIDE==1)
			stream.WriteBOOL(FALSE);
		else
			stream.WriteBOOL(TRUE);		

		BYTE cCheckSum = 0;

		if (pGoods->m_cLevel==2)
			stream.WriteBOOL(TRUE);
		else
			stream.WriteBOOL(FALSE);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_cLevel), 1);

		stream.EncodeXInt32(pGoods->m_xLTG, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		stream.EncodeXInt32(pGoods->m_xZFX, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &(pGoods->m_xLTG));
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xLTG), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xZFX), 4);

		DWORD dwDate = pGoods->m_dwFirstDate;
		CHisCompress::CompressDate(stream, dwDate);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwDate, 4);

		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;
		DWORD dwPrice;
		for (WORD w=0; w<5; w++)
		{	
			switch (w)
			{
			case 0:
				dwPrice = pGoods->m_dwPClose / PRICE_DIVIDE;
				break;
			case 1:
				dwPrice = pGoods->m_dwWClose / PRICE_DIVIDE;
				break;
			case 2:
				dwPrice = pGoods->m_dw5Close / PRICE_DIVIDE;
				break;
			case 3:
				dwPrice = pGoods->m_dwPriceZT / PRICE_DIVIDE;
				break;
			default:
				dwPrice = pGoods->m_dwPriceDT / PRICE_DIVIDE;
			}
			stream.EncodeData(dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
		}		

		stream.EncodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.EncodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		if (wVersion>=4)
		{
			stream.EncodeXInt32(pGoods->m_xSumBigAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
			cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumBigAmt4), 4);
		}

		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandStatic(CBitStream& stream, CGoods* pGoods)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE;
		char cDiv = (char)stream.ReadDWORD(1);
		if (cDiv==0)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		BYTE cCheckSum = 0;

		BOOL bLevel2 = stream.ReadDWORD(1);
		if (bLevel2)
			pGoods->m_cLevel = 2;
		else
			pGoods->m_cLevel = 1;
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_cLevel), 1);

		XInt32 xLTG,xZFX;

		stream.DecodeXInt32(xLTG, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		stream.DecodeXInt32(xZFX, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &(xLTG));
		cCheckSum += GetBytesCheckSum((PBYTE)&(xLTG), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(xZFX), 4);

		if (pGoods->m_jbm.m_fpd.m_cType != 'F')
		{
			pGoods->m_xLTG = xLTG;
			pGoods->m_xZFX = xZFX;
		}

		pGoods->m_dwFirstDate = CHisCompress::ExpandDate(stream);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_dwFirstDate), 4);

		if (pGoods->GetID()==601607)
			pGoods->m_dwFirstDate = 19940324;

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;
		DWORD dwPrice;
		for (WORD w=0; w<7; w++)
		{	
			stream.DecodeData(dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
			switch (w)
			{
			case 0:
				pGoods->m_dwPClose = dwPrice * PRICE_DIVIDE;
				break;
			case 1:
				pGoods->m_dwWClose = dwPrice * PRICE_DIVIDE;
				break;
			case 2:
				pGoods->m_dw5Close = dwPrice * PRICE_DIVIDE;
				break;
			case 3:
				pGoods->m_dwPriceZT = dwPrice * PRICE_DIVIDE;
				break;
			case 4:
				pGoods->m_dwPriceDT = dwPrice * PRICE_DIVIDE;
				break;
			case 5:
				pGoods->m_dwClose9 = dwPrice * PRICE_DIVIDE;
				break;
			default:
				pGoods->m_dwClose19 = dwPrice * PRICE_DIVIDE;
			}
		}		

		stream.DecodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		stream.DecodeXInt32(pGoods->m_xSumBigAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumBigAmt4), 4);

		DWORD dwValue;

		stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
		if (pGoods->m_wGroupHY!=(WORD)dwValue)
		{
			pGoods->m_wGroupHY = (WORD)dwValue;
			pGoods->m_strHYName = pGoods->GetHYName();
		}
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_wGroupHY), 2);

		stream.DecodeData(dwValue, BC_DYNA_ZLZC, BCNUM_DYNA_ZLZC);
		pGoods->m_bZLZC4 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum(&(pGoods->m_bZLZC4), 1);
		stream.DecodeData(dwValue, BC_DYNA_ZLZC, BCNUM_DYNA_ZLZC);
		pGoods->m_bZLZC9 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum(&(pGoods->m_bZLZC9), 1);
		stream.DecodeData(dwValue, BC_DYNA_ZLZC, BCNUM_DYNA_ZLZC);
		pGoods->m_bZLZC19 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum(&(pGoods->m_bZLZC19), 1);

		stream.DecodeData(dwValue, BC_DYNA_ZLZC, BCNUM_DYNA_ZLZC);
		pGoods->m_bZLQM = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum(&(pGoods->m_bZLQM), 1);

		stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
		pGoods->m_wGroupDQ = (WORD)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_wGroupDQ), 2);

		short sGN = (short)stream.ReadDWORD(4);
		cCheckSum += GetBytesCheckSum((PBYTE)&sGN, 2);

		if (sGN>0)
		{
			pGoods->m_aGroupGN.RemoveAt(0, pGoods->m_aGroupGN.GetSize());

			for (short s=0; s<sGN; s++)
			{
				stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
				cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 2);
				WORD wVal = dwValue;
				pGoods->m_aGroupGN.Add(wVal);
			}
		}

		///////////////////////////////////////////////
		dwValue = stream.ReadDWORD(7);
		//pGoods->m_DayStat.m_bJGQM = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);

		dwValue = stream.ReadDWORD(4);
		//pGoods->m_DayStat.m_bDKBY9 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);

		dwValue = stream.ReadDWORD(32);
		//pGoods->m_DayStat.m_xAmtOf1Super.SetRawData(dwValue);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 4);

		dwValue = stream.ReadDWORD(32);
		//pGoods->m_DayStat.m_xAmtOf4Super.SetRawData(dwValue);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 4);
		dwValue = stream.ReadDWORD(32);
		//pGoods->m_DayStat.m_xAmtOf9Super.SetRawData(dwValue);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 4);

		dwValue = stream.ReadDWORD(32);
		//pGoods->m_DayStat.m_xAmtOf1Big.SetRawData(dwValue);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 4);
		dwValue = stream.ReadDWORD(32);
		//pGoods->m_DayStat.m_xAmtOf4Big.SetRawData(dwValue);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 4);
		dwValue = stream.ReadDWORD(32);
		//pGoods->m_DayStat.m_xAmtOf9Big.SetRawData(dwValue);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 4);

		dwValue = stream.ReadDWORD(3);
		//pGoods->m_DayStat.m_bJGZC4 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);
		dwValue = stream.ReadDWORD(4);
		//pGoods->m_DayStat.m_bJGZC9 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);
		dwValue = stream.ReadDWORD(5);
		//pGoods->m_DayStat.m_bJGZC19 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);

		dwValue = stream.ReadDWORD(3);
		//pGoods->m_DayStat.m_bYZZC4 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);
		dwValue = stream.ReadDWORD(4);
		//pGoods->m_DayStat.m_bYZZC9 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);
		dwValue = stream.ReadDWORD(5);
		//pGoods->m_DayStat.m_bYZZC19 = (BYTE)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 1);
		///////////////////////////////////////////////

		stream.DecodeXInt32(pGoods->m_xSumAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumAmt4), 4);

		char c = (char)stream.ReadDWORD(1);
		stream.DecodeXInt32(pGoods->m_xSumBigVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		if (c==0)
			pGoods->m_xSumBigVol4 = -pGoods->m_xSumBigVol4;
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumBigVol4), 4);

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		if (cCheckSum!=cCheckSumRecv)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressStaticQH(CBitStream& stream, CGoods* pGoods, WORD wVersion)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		if (PRICE_DIVIDE==1)
			stream.WriteBOOL(FALSE);
		else
			stream.WriteBOOL(TRUE);		

		BYTE cCheckSum = 0;

		DWORD dwDate = pGoods->m_dwFirstDate;
		CHisCompress::CompressDate(stream, dwDate);
		cCheckSum += GetBytesCheckSum((PBYTE)&dwDate, 4);

		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;

		DWORD pdwPrice[] = {pGoods->m_dwPClose,		pGoods->m_dwWClose, 
			pGoods->m_dw5Close,		pGoods->m_dwPriceZT,
			pGoods->m_dwPriceDT,	pGoods->m_dwClose9,
			pGoods->m_dwClose19,	pGoods->m_dwPreSettlement,
			pGoods->m_dwSettlement
		};

		WORD wNum = WORD(sizeof(pdwPrice)/sizeof(DWORD));
		for (WORD w=0; w<wNum; w++)
		{	
			stream.EncodeData(pdwPrice[w], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)(pdwPrice+w), 4);
		}		

		stream.EncodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.EncodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		stream.EncodeXInt32(pGoods->m_xPreOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xPreOpenInterest), 4);

		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandStaticQH(CBitStream& stream, CGoods* pGoods)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE;
		char cDiv = (char)stream.ReadDWORD(1);
		if (cDiv==0)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		BYTE cCheckSum = 0;

		pGoods->m_dwFirstDate = CHisCompress::ExpandDate(stream);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_dwFirstDate), 4);

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;
		DWORD dwPrice;
		for (WORD w=0; w<9; w++)
		{	
			stream.DecodeData(dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
			switch (w)
			{
			case 0:
				pGoods->m_dwPClose = dwPrice * PRICE_DIVIDE;
				break;
			case 1:
				pGoods->m_dwWClose = dwPrice * PRICE_DIVIDE;
				break;
			case 2:
				pGoods->m_dw5Close = dwPrice * PRICE_DIVIDE;
				break;
			case 3:
				pGoods->m_dwPriceZT = dwPrice * PRICE_DIVIDE;
				break;
			case 4:
				pGoods->m_dwPriceDT = dwPrice * PRICE_DIVIDE;
				break;
			case 5:
				pGoods->m_dwClose9 = dwPrice * PRICE_DIVIDE;
				break;
			case 6:
				pGoods->m_dwClose19 = dwPrice * PRICE_DIVIDE;
				break;
			case 7:
				pGoods->m_dwPreSettlement = dwPrice * PRICE_DIVIDE;
				break;
			default:
				pGoods->m_dwSettlement = dwPrice * PRICE_DIVIDE;
				break;
			}
		}		

		stream.DecodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		stream.DecodeXInt32(pGoods->m_xPreOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xPreOpenInterest), 4);

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		if (cCheckSum!=cCheckSumRecv)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}


void CDynaCompress::CompressVirtualMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	stream.EncodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, pOldDyna->m_pdwMMP+9);

	stream.EncodeXInt32(pDyna->m_pxMMPVol[9], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, pOldDyna->m_pxMMPVol+9);

	XInt32* pBase;
	if (pOldDyna->m_pxMMPVol[8].m_nBase>0)
		pBase = pOldDyna->m_pxMMPVol+8;
	else
		pBase = pOldDyna->m_pxMMPVol+11;

	if (pDyna->m_pxMMPVol[8].m_nBase>0)
	{
		stream.WriteBOOL(TRUE);

		stream.EncodeXInt32(pDyna->m_pxMMPVol[8], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, pBase);
	}
	else
	{
		stream.WriteBOOL(FALSE);
		stream.EncodeXInt32(pDyna->m_pxMMPVol[11], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, pBase);
	}
}

void CDynaCompress::CompressVirtualMMP(CBitStream& stream, CDynamicValue* pDyna)
{
	stream.EncodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);

	stream.EncodeXInt32(pDyna->m_pxMMPVol[9], BC_DYNA_VOL, BCNUM_DYNA_VOL);

	if (pDyna->m_pxMMPVol[8].m_nBase>0)
	{
		stream.WriteBOOL(TRUE);
		stream.EncodeXInt32(pDyna->m_pxMMPVol[8], BC_DYNA_VOL, BCNUM_DYNA_VOL);
	}
	else
	{
		stream.WriteBOOL(FALSE);
		stream.EncodeXInt32(pDyna->m_pxMMPVol[11], BC_DYNA_VOL, BCNUM_DYNA_VOL);
	}
}

void CDynaCompress::ExpandVirtualMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	stream.DecodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, pOldDyna->m_pdwMMP+9);
	pDyna->m_pdwMMP[10] = pDyna->m_pdwMMP[9];

	stream.DecodeXInt32(pDyna->m_pxMMPVol[9], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, pOldDyna->m_pxMMPVol+9);
	pDyna->m_pxMMPVol[10] = pDyna->m_pxMMPVol[9];
	
	XInt32* pBase;
	if (pOldDyna->m_pxMMPVol[8].m_nBase>0)
		pBase = pOldDyna->m_pxMMPVol+8;
	else
		pBase = pOldDyna->m_pxMMPVol+11;

	BOOL b8 = stream.ReadDWORD(1);
	if (b8)
		stream.DecodeXInt32(pDyna->m_pxMMPVol[8], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, pBase);
	else
		stream.DecodeXInt32(pDyna->m_pxMMPVol[11], BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, pBase);

	pDyna->m_cVirtual = 1;
}

void CDynaCompress::ExpandVirtualMMP(CBitStream& stream, CDynamicValue* pDyna)
{
	stream.DecodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);
	pDyna->m_pdwMMP[10] = pDyna->m_pdwMMP[9];

	stream.DecodeXInt32(pDyna->m_pxMMPVol[9], BC_DYNA_VOL, BCNUM_DYNA_VOL);
	pDyna->m_pxMMPVol[10] = pDyna->m_pxMMPVol[9];

	BOOL b8 = stream.ReadDWORD(1);
	if (b8)
		stream.DecodeXInt32(pDyna->m_pxMMPVol[8], BC_DYNA_VOL, BCNUM_DYNA_VOL);
	else
		stream.DecodeXInt32(pDyna->m_pxMMPVol[11], BC_DYNA_VOL, BCNUM_DYNA_VOL);

	pDyna->m_cVirtual = 1;
}

int CDynaCompress::CompressDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna)
{
	if (pGoods==NULL || pOldDyna==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		stream.WriteBOOL(TRUE);	// bRelative

		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);
		
		// time
		DWORD dwSecondsDiff = GetSecondsDiff(pOldDyna->m_dwTime, pDyna->m_dwTime);
		stream.EncodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);

		// price
		DWORD dwNumPrice = 4;		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if (pDyna->m_dwOpen == pOldDyna->m_dwOpen)
		{
			if(pDyna->m_dwHigh == pOldDyna->m_dwHigh)
			{
				if(pDyna->m_dwLow == pOldDyna->m_dwLow)
				{
					if(pDyna->m_dwPrice == pOldDyna->m_dwPrice)
						dwNumPrice = 0;
					else
						dwNumPrice = 1;	// P
				}
				else
					dwNumPrice = 2;		// PL
			}
			else
			{
				if(pDyna->m_dwLow == pOldDyna->m_dwLow)
					dwNumPrice = 3;		// PH
				else
					dwNumPrice = 4;		// PLHO
			}
		}
		else
			dwNumPrice = 4;		// PLHO

		stream.WriteDWORD(dwNumPrice, 3);

		if(dwNumPrice >= 1)		// P
		{
			stream.EncodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.EncodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.EncodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.EncodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		if (pDyna->m_xVolume==pOldDyna->m_xVolume && pDyna->m_xAmount==pOldDyna->m_xAmount)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);
			// volume, amount
			stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);

			XInt32 xBase = (INT64)pOldDyna->m_xAmount + ((INT64)pDyna->m_xVolume - (INT64)pOldDyna->m_xVolume) * pDyna->m_dwPrice * PRICE_DIVIDE;
			stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, &xBase);
		}

		stream.EncodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwNorminal);

		if (pDyna->m_cVirtual)
		{
			stream.WriteBOOL(TRUE);

			CompressVirtualMMP(stream, pDyna, pOldDyna);
		}
		else
		{
			stream.WriteBOOL(FALSE);
	
			stream.EncodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, pOldDyna->m_pdwMMP+9);
			stream.EncodeData(pDyna->m_pdwMMP[10], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, pOldDyna->m_pdwMMP+10);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		if (pDyna->m_xNeiPan==pOldDyna->m_xNeiPan && pDyna->m_xCurVol==pOldDyna->m_xCurVol && pDyna->m_cCurVol==pOldDyna->m_cCurVol)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.EncodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
			stream.WriteDWORD(pDyna->m_cCurVol+1, 2);		// -1 0 1
		}

		if (pDyna->m_dwPrice5==pOldDyna->m_dwPrice5)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);
			stream.EncodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		}

		BYTE cCheckSum = pDyna->GetChecksumH();
		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna)
{
	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		stream.WriteBOOL(FALSE);	// !bRelative

		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);

		// time
		stream.WriteDWORD(pDyna->m_dwTime, 18);

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;

		stream.EncodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.EncodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.EncodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.EncodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		// volume, amount
		stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);

		XInt32 xBase = (INT64)pDyna->m_xVolume * ((pDyna->m_dwHigh+pDyna->m_dwLow)/2) * PRICE_DIVIDE;
		stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xBase);

		stream.EncodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);

		if (pDyna->m_cVirtual)
		{
			stream.WriteBOOL(TRUE);

			CompressVirtualMMP(stream, pDyna);
		}
		else
		{
			stream.WriteBOOL(FALSE);
	
			stream.EncodeData(pDyna->m_pdwMMP[9], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			stream.EncodeData(pDyna->m_pdwMMP[10], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		stream.EncodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.EncodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
		stream.WriteDWORD(pDyna->m_cCurVol+1, 2);		// -1 0 1

		stream.EncodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);

		BYTE cCheckSum = pDyna->GetChecksumH();
		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressQuoteStatic(CBitStream& stream, CGoods* pGoods, WORD wVersion)
{
	try
	{
		if (pGoods->IsGZQH())//股指期货
		{
			DWORD pdwPrice[] = {pGoods->m_dwPriceZT, pGoods->m_dwPriceDT,
				pGoods->m_dwPreSettlement,pGoods->m_dwSettlement
			};

			WORD wNum = WORD(sizeof(pdwPrice)/sizeof(DWORD));
			for (WORD w=0; w<wNum; w++)
			{	
				stream.EncodeData(pdwPrice[w], BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pGoods->m_dwClose);
			}		

			stream.EncodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);

			stream.EncodeXInt32(pGoods->m_xPreOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		}
		else
		{
			if( wVersion >= 13 ){
				stream.EncodeData(pGoods->m_dwPriceZT, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pGoods->m_dwClose);
				stream.EncodeData(pGoods->m_dwPriceDT, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pGoods->m_dwClose, TRUE);
			}
			stream.EncodeXInt32(pGoods->m_xLTG, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
			stream.EncodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressQuote(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP)
{
	if (pGoods == NULL || pDyna == NULL)
		return ERR_COMPRESS_PARAM;

	if (pOldDyna!=NULL && (bNeedMMP==FALSE || pDyna->m_cVirtual==pOldDyna->m_cVirtual))
		return CompressQuoteData(stream, pGoods, pDyna, pOldDyna, bL2Encode, bNeedMMP);
	else
		return CompressQuoteData(stream, pGoods, pDyna, bL2Encode, bNeedMMP);
}

int CDynaCompress::CompressQuoteData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDynaIn, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP)
{
	if (pGoods==NULL || pOldDyna==NULL || pDynaIn==NULL)
		return ERR_COMPRESS_PARAM;

	if (bL2Encode && pGoods->m_cLevel<2)
		bL2Encode = FALSE;

	BOOL bIndex = pGoods->IsIndex() || pGoods->m_cStation==2;	// 指数或者板块指数
	BOOL bGZQH = pGoods->IsGZQH();

	try
	{
		stream.WriteBOOL(TRUE);	// bRelative

		stream.WriteBOOL(bL2Encode);
		
		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		char cDiv = pGoods->m_cPriceDiv;
		DWORD dwDiv = pGoods->m_dwPriceDiv;
//		stream.WriteBOOL(PRICE_DIVIDE!=1);
		if (PRICE_DIVIDE==1)
			stream.WriteBOOL(FALSE);
		else
			stream.WriteBOOL(TRUE);

		CDynamicValue dyna;
		CDynamicValue* pDyna = &dyna;
		CopyMemory(pDyna, pDynaIn, sizeof(CDynamicValue));
		pDyna->Div(dwDiv);

		// time
		DWORD dwSecondsDiff = GetSecondsDiff(pOldDyna->m_dwTime, pDyna->m_dwTime);
		stream.EncodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);

		// price
		DWORD dwNumPrice = 4;		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if(pDyna->m_dwOpen == pOldDyna->m_dwOpen)
		{
			if(pDyna->m_dwHigh == pOldDyna->m_dwHigh)
			{
				if(pDyna->m_dwLow == pOldDyna->m_dwLow)
				{
					if(pDyna->m_dwPrice == pOldDyna->m_dwPrice)
						dwNumPrice = 0;
					else
						dwNumPrice = 1;	// P
				}
				else
					dwNumPrice = 2;		// PL
			}
			else
			{
				if(pDyna->m_dwLow == pOldDyna->m_dwLow)
					dwNumPrice = 3;		// PH
				else
					dwNumPrice = 4;		// PLHO
			}
		}
		else
			dwNumPrice = 4;		// PLHO

		stream.WriteDWORD(dwNumPrice, 3);

		if(dwNumPrice >= 1)		// P
		{
			stream.EncodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.EncodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.EncodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.EncodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		if (bIndex)
		{
			stream.WriteBOOL(TRUE);

			if (pDyna->m_xVolume==pOldDyna->m_xVolume && pDyna->m_xAmount==pOldDyna->m_xAmount && pDyna->m_dwTradeNum == pOldDyna->m_dwTradeNum)
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);
				// volume, amount
				stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);
				stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &pOldDyna->m_xAmount);

//				stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
			}

			//CIndex* pIndex = theApp.Goods2Index(pGoods->GetID());
			if (pGoods->m_pIndex)
			{
				stream.WriteBOOL(TRUE);

				stream.EncodeData(pGoods->m_pIndex->m_wRise, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
				stream.EncodeData(pGoods->m_pIndex->m_wEqual, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
				stream.EncodeData(pGoods->m_pIndex->m_wFall, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
			}
			else
				stream.WriteBOOL(FALSE);
		}
		else
		{
			stream.WriteBOOL(FALSE);

			// volume, amount, tradenum
			if (pDyna->m_xVolume==pOldDyna->m_xVolume && pDyna->m_xAmount==pOldDyna->m_xAmount && pDyna->m_dwTradeNum == pOldDyna->m_dwTradeNum)
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);

				stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);

				XInt32 xBase = (INT64)pOldDyna->m_xAmount + 
						((INT64)pDyna->m_xVolume - (INT64)pOldDyna->m_xVolume) * pDyna->m_dwPrice * PRICE_DIVIDE;
				stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, &xBase);

//				stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
			}

			// mmp
			if (bNeedMMP==FALSE || (pDyna->m_cVirtual==0 && pDyna->m_cNumBuy==0 && pDyna->m_cNumSell==0))	// no MMP
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);

				if (pDyna->m_cVirtual)
				{
					stream.WriteBOOL(TRUE);

					CompressVirtualMMP(stream, pDyna, pOldDyna);
				}
				else
				{
					stream.WriteBOOL(FALSE);
	
					CompressMMP(stream, pDyna, pOldDyna, bGZQH, bL2Encode);
				}
			}// bHasMMP
		}
/*
		// PBUY, VBUY, PSELL, VSELL
		if (bL2Encode || bIndex)
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPBuy);
			stream.EncodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPSell);
			stream.EncodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
		else
			stream.WriteBOOL(FALSE);
*/
		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		if (pDyna->m_xNeiPan==pOldDyna->m_xNeiPan && pDyna->m_xCurVol==pOldDyna->m_xCurVol && pDyna->m_cCurVol==pOldDyna->m_cCurVol)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.EncodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
			stream.WriteDWORD(pDyna->m_cCurVol+1, 2);		// -1 0 1
			if (bGZQH)
			{
				stream.EncodeXInt32(XInt32(pGoods->GetValue(CUR_OI)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);			//现增仓
				stream.EncodeXInt32(XInt32(pGoods->GetValue(TODAY_OI)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);		//日增仓
				stream.EncodeXInt32(XInt32(pGoods->GetValue(OPENINTEREST)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);	//持仓量
				stream.EncodeXInt32(XInt32(pGoods->GetValue(JICHA_PER)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);		//基差%
				stream.EncodeXInt32(XInt32(pGoods->GetValue(JICHA)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);			//基差
				LogDbg("增仓=>%d 日增仓=>%d 基差%%=>%d 基差=>%d", pGoods->GetValue(CUR_OI), pGoods->GetValue(TODAY_OI), pGoods->GetValue(JICHA_PER), pGoods->GetValue(JICHA_PER));
			}

			if (pGoods->IsHK())
			{
				DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;
				stream.EncodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			}
		}
/*
		if (pDyna->m_dwPrice5==pOldDyna->m_dwPrice5)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);
			stream.EncodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		}

		stream.EncodeData(pDyna->m_lStrong, BC_DYNA_STRONG_DIFF, BCNUM_DYNA_STRONG_DIFF, (PDWORD)&pOldDyna->m_lStrong);

		if(bL2Encode)
		{
			CompressOrderCounts(stream, pDyna->m_ocSumOrder, pOldDyna->m_ocSumOrder, bNew, TRUE);
			CompressOrderCounts(stream, pDyna->m_ocSumTrade, pOldDyna->m_ocSumTrade, bNew, FALSE);
		}

		BYTE cCheckSum = pDyna->GetChecksum(bNeedMMP, bNew);
		stream.WriteDWORD(cCheckSum, 8);
*/
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressQuoteData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDynaIn, BOOL bL2Encode, BOOL bNeedMMP)
{
	if (pGoods==NULL || pDynaIn==NULL)
		return ERR_COMPRESS_PARAM;

	if (bL2Encode && pGoods->m_cLevel<2)
		bL2Encode = FALSE;

	BOOL bIndex = pGoods->IsIndex() || pGoods->m_cStation==2;	// 指数或者板块指数
	BOOL bGZQH = pGoods->IsGZQH();

	try
	{
		stream.WriteBOOL(FALSE);	// !bRelative

		stream.WriteBOOL(bL2Encode);
		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		char cDiv = pGoods->m_cPriceDiv;
		DWORD dwDiv = pGoods->m_dwPriceDiv;

		if (PRICE_DIVIDE==1)
			stream.WriteBOOL(FALSE);
		else
			stream.WriteBOOL(TRUE);

		CDynamicValue dyna;
		CDynamicValue* pDyna = &dyna;
		CopyMemory(pDyna, pDynaIn, sizeof(CDynamicValue));
		pDyna->Div(dwDiv);

		// time
		stream.WriteDWORD(pDyna->m_dwTime, 18);

		// price
		DWORD dwClose = g_DivPrice(pGoods->m_dwClose, dwDiv);

		stream.EncodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.EncodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.EncodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.EncodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		if (bIndex)
		{
			stream.WriteBOOL(TRUE);

			// volume, amount
			stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);

			//CIndex* pIndex = theApp.Goods2Index(pGoods->GetID());
			if (pGoods->m_pIndex)
			{
				stream.WriteBOOL(TRUE);
				stream.EncodeData(pGoods->m_pIndex->m_wRise, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
				stream.EncodeData(pGoods->m_pIndex->m_wEqual, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
				stream.EncodeData(pGoods->m_pIndex->m_wFall, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
			}
			else
				stream.WriteBOOL(FALSE);
		}
		else 
		{
			stream.WriteBOOL(FALSE);

			// volume, amount, tradenum
			stream.EncodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);

			XInt32 xBase = (INT64)pDyna->m_xVolume * ((pDyna->m_dwHigh+pDyna->m_dwLow)/2) * PRICE_DIVIDE;
			stream.EncodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xBase);

//			stream.EncodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);

			// mmp
			if (bNeedMMP==FALSE || (pDyna->m_cVirtual==0 && pDyna->m_cNumBuy==0 && pDyna->m_cNumSell==0))	// no MMP
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);
				if (pDyna->m_cVirtual)
				{
					stream.WriteBOOL(TRUE);
					CompressVirtualMMP(stream, pDyna);
				}
				else
				{
					stream.WriteBOOL(FALSE);
					CompressMMP(stream, pDyna, bGZQH, bL2Encode);
				}
			}
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol, m_dwPrice5, m_lStrong
		stream.EncodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.EncodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
		stream.WriteDWORD(pDyna->m_cCurVol+1, 2);		// -1 0 1
		if (bGZQH)
		{
			stream.EncodeXInt32(XInt32(pGoods->GetValue(CUR_OI)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);			//现增仓
			stream.EncodeXInt32(XInt32(pGoods->GetValue(TODAY_OI)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);		//日增仓
			stream.EncodeXInt32(XInt32(pGoods->GetValue(OPENINTEREST)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);	//持仓量
			stream.EncodeXInt32(XInt32(pGoods->GetValue(JICHA_PER)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);		//基差%
			stream.EncodeXInt32(XInt32(pGoods->GetValue(JICHA)), BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);			//基差
			LogDbg("增仓=>%d 日增仓=>%d 基差%%=>%d 基差=>%d", pGoods->GetValue(CUR_OI), pGoods->GetValue(TODAY_OI), pGoods->GetValue(JICHA_PER), pGoods->GetValue(JICHA_PER));
		}
		
		if (pGoods->IsHK())
		{
			stream.EncodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressHLVA(CBitStream& stream, CGoods* pGoods, DWORD dwDiv, DWORD dwRecvHigh, DWORD dwRecvLow, XInt32 xRecvVolume, XInt32 xRecvAmount)
{
	try
	{
		// price
		DWORD dwClose = pGoods->IsGZQH() ? pGoods->m_dwPreSettlement : pGoods->m_dwClose;
		//dwClose /= PRICE_DIVIDE;
		dwClose = g_DivPrice(dwClose, dwDiv);

		//if (pGoods->m_dwHigh/PRICE_DIVIDE == dwRecvHigh/PRICE_DIVIDE)
		if (g_DivPrice(pGoods->m_dwHigh, dwDiv) == g_DivPrice(dwRecvHigh, dwDiv))			
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			DWORD dwHigh  = g_DivPrice(pGoods->m_dwHigh, dwDiv);
			stream.EncodeData(dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &dwClose);
		}

		if ( g_DivPrice(pGoods->m_dwLow, dwDiv) == g_DivPrice(dwRecvLow, dwDiv) )
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			DWORD dwLow   = g_DivPrice(pGoods->m_dwLow, dwDiv);
			stream.EncodeData(dwLow , BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &dwClose);
		}

		if (pGoods->m_xVolume == xRecvVolume && pGoods->m_xAmount == xRecvAmount)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeXInt32(pGoods->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &xRecvVolume);
			stream.EncodeXInt32(pGoods->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xRecvAmount);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}


int CDynaCompress::CompressVA(CBitStream& stream, CGoods* pGoods, XInt32 xRecvVolume, XInt32 xRecvAmount)
{
	try
	{
		if (pGoods->m_xVolume == xRecvVolume && pGoods->m_xAmount == xRecvAmount)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeXInt32(pGoods->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &xRecvVolume);
			stream.EncodeXInt32(pGoods->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xRecvAmount);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::CompressVA(CBitStream& stream, XInt32 xVolume, XInt32 xAmount, XInt32 xRecvVolume, XInt32 xRecvAmount)
{
	try
	{
		if (xVolume == xRecvVolume && xAmount == xRecvAmount)
			stream.WriteBOOL(FALSE);
		else
		{
			stream.WriteBOOL(TRUE);

			stream.EncodeXInt32(xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &xRecvVolume);
			stream.EncodeXInt32(xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &xRecvAmount);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}


//sam copy from pc client begin
DWORD CDynaCompress::ExpandDate(CBitStream& stream, DWORD dwDateBase)
{	
	DWORD dwCurDay = 0;
	stream.DecodeData(dwCurDay, BC_DYNA_DATE, BCNUM_DYNA_DATE);

	CTime tmBase(dwDateBase/10000, dwDateBase%10000/100, dwDateBase%100, 0, 0, 0);
	CTime tm = tmBase + CMyTimeSpan(dwCurDay, 0, 0, 0);
	
	DWORD dwDate = tm.GetYear()*10000 + tm.GetMonth()*100 + tm.GetDay();
	return dwDate;
}

//sam copy from pc client end

//Steven New Begin
///20120319 sjp

BYTE l_ExpandZC(CBitStream& stream, BYTE& bZC)
{
	DWORD dwValue;
	stream.DecodeData(dwValue, BC_DYNA_ZLZC, BCNUM_DYNA_ZLZC);
	bZC = (BYTE)dwValue;
	return GetBytesCheckSum(&bZC, 1);
}

BYTE l_ExpandBigAmount(CBitStream& stream, XInt32& xAmount)
{
	char c = (char)stream.ReadDWORD(1);
	stream.DecodeXInt32(xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
	if (c==0)
		xAmount = -xAmount;
	return GetBytesCheckSum((PBYTE)&xAmount, 4);
}

int CDynaCompress::ExpandStaticQH5(CBitStream& stream, CGoods* pGoods)
{
	return CDynaCompress::ExpandStaticQH(stream, pGoods);
}

int CDynaCompress::ExpandXGStatic5(CBitStream& stream, CGoods* pGoods)
{
	CXGStatData& xg = pGoods->m_XGStat;

	DWORD dwTemp = stream.ReadDWORD(1);	
	xg.m_bKXXT = (char)dwTemp;

	stream.DecodeData(dwTemp, BC_DYNA_OTHER, BCNUM_DYNA_OTHER);
	xg.m_psDDBL[0] = (short)dwTemp;
	stream.DecodeData(dwTemp, BC_DYNA_OTHER, BCNUM_DYNA_OTHER);
	xg.m_psDDBL[1] = (short)dwTemp;

	for (int i = 0;i < 4;i++)
	{
		stream.DecodeXInt32(xg.m_pxMinVol[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(xg.m_pxVol[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);

		stream.DecodeData(xg.m_pdwHighPrice[i], BC_DYNA_OTHER, BCNUM_DYNA_OTHER);
		stream.DecodeData(xg.m_pdwLowPrice[i], BC_DYNA_OTHER, BCNUM_DYNA_OTHER);
	}

	stream.DecodeData(xg.m_dwYOpen, BC_DYNA_OTHER, BCNUM_DYNA_OTHER);
	stream.DecodeData(xg.m_dwYHigh, BC_DYNA_OTHER, BCNUM_DYNA_OTHER);

	return 0;
}


int CDynaCompress::ExpandStatic5(CBitStream& stream, CGoods* pGoods)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		DWORD PRICE_DIVIDE;
		char cDiv = (char)stream.ReadDWORD(1);
		if (cDiv==0)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		pGoods->m_cPriceDiv = cDiv;
		pGoods->m_nPriceDiv = PRICE_DIVIDE;
		pGoods->m_dwPriceDiv = PRICE_DIVIDE;

		BYTE cCheckSum = 0;

		BOOL bLevel2 = stream.ReadDWORD(1);
		if (bLevel2)
			pGoods->m_cLevel = 2;
		else
			pGoods->m_cLevel = 1;
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_cLevel), 1);

		stream.DecodeXInt32(pGoods->m_xLTG, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		stream.DecodeXInt32(pGoods->m_xZFX, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &(pGoods->m_xLTG));
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xLTG), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xZFX), 4);

		pGoods->m_dwFirstDate = CHisCompress::ExpandDate(stream);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_dwFirstDate), 4);

		if (pGoods->GetID()==601607)
			pGoods->m_dwFirstDate = 19940324;

		// price
		DWORD dwClose = pGoods->m_dwClose / PRICE_DIVIDE;
		DWORD dwPrice;
		for (WORD w=0; w<7; w++)
		{	
			stream.DecodeData(dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
			switch (w)
			{
			case 0:
				pGoods->m_dwPClose = dwPrice * PRICE_DIVIDE;
				break;
			case 1:
				pGoods->m_dwWClose = dwPrice * PRICE_DIVIDE;
				break;
			case 2:
				pGoods->m_dw5Close = dwPrice * PRICE_DIVIDE;
				break;
			case 3:
				pGoods->m_dwPriceZT = dwPrice * PRICE_DIVIDE;
				break;
			case 4:
				pGoods->m_dwPriceDT = dwPrice * PRICE_DIVIDE;
				break;
			case 5:
				pGoods->m_dwClose9 = dwPrice * PRICE_DIVIDE;
				break;
			default:
				pGoods->m_dwClose19 = dwPrice * PRICE_DIVIDE;
			}
		}		

		stream.DecodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		stream.DecodeXInt32(pGoods->m_xSumBigAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumBigAmt4), 4);

		DWORD dwValue;

		stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
		if (pGoods->m_wGroupHY!=(WORD)dwValue)
		{
			pGoods->m_wGroupHY = (WORD)dwValue;
			pGoods->m_strHYName = pGoods->GetHYName();
		}
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_wGroupHY), 2);

		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLZC4);
		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLZC9);
		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLZC19);

		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLQM);

		stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
		pGoods->m_wGroupDQ = (WORD)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_wGroupDQ), 2);

		short sGN = (short)stream.ReadDWORD(4);
		cCheckSum += GetBytesCheckSum((PBYTE)&sGN, 2);

		if (sGN>0)
		{
			pGoods->m_aGroupGN.RemoveAt(0, pGoods->m_aGroupGN.GetSize());

			for (short s=0; s<sGN; s++)
			{
				stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
				cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 2);

				//				pGoods->m_aGroupGN.Add((WORD)dwValue);
				//Steven New Begin
				WORD wValue = (WORD)dwValue;
				//Steven New End
				pGoods->m_aGroupGN.Add(wValue);
			}
		}

		///////////////////////////////////////////////

		{
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGQM);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bDKBY9);
		}

		{
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf1Super);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf4Super);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf9Super);

			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf1Big);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf4Big);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf9Big);

			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGZC4);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGZC9);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGZC19);

			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bYZZC4);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bYZZC9);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bYZZC19);
		}

		///////////////////////////////////////////////

		stream.DecodeXInt32(pGoods->m_xSumAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumAmt4), 4);

		cCheckSum += l_ExpandBigAmount(stream, pGoods->m_xSumBigVol4);

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		if (cCheckSum!=cCheckSumRecv)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandExpValue(CBitStream& stream, CGoods* pGoods, CSortArray<CExpConfig>& aExpConfig)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	CSortArray<CExpValue> aExpValue;

	pGoods->m_aExpValue.RemoveAt(0, pGoods->m_aExpValue.GetSize());

	try
	{
		BYTE cCheckSum = 0;

		CExpValue ev;

		int nSize = (int)aExpConfig.GetSize();

		for (int i=0; i<nSize; i++)
		{
			CExpConfig& ec = aExpConfig[i];

			BOOL bNo0 = stream.ReadDWORD(1);
			if (bNo0==FALSE)
			{
				continue;
			}

			ev.m_sNo = (short)ec.m_nNo;

			BOOL bPositive = stream.ReadDWORD(1);

			XInt32 xValue;
			stream.DecodeXInt32(xValue, BC_DYNA_OTHER, BCNUM_DYNA_OTHER);

			if (bPositive==FALSE)
				xValue = -xValue;

			cCheckSum += GetBytesCheckSum((PBYTE)&xValue, 4);

			ev.m_xValue = xValue;

			aExpValue.Add(ev);
		}

		BYTE cRecvCheckSum = (BYTE)stream.ReadDWORD(8);

		if (cRecvCheckSum!=cCheckSum)
			return ERR_COMPRESS_CHECK;

		pGoods->m_aExpValue.Copy(aExpValue);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandXGStaticData(CBitStream& stream, CGoods* pGoods)
{
	CXGStatData &xg = pGoods->m_XGStat;

	DWORD dwTemp = stream.ReadDWORD(1);	
	xg.m_bKXXT = (char)dwTemp;
	dwTemp = stream.ReadDWORD(32);
	*(DWORD *)xg.m_psDDBL = dwTemp;
	for (int i = 0;i < 4;i++)
	{
		stream.DecodeXInt32(xg.m_pxMinVol[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(xg.m_pxVol[i], BC_DYNA_VOL, BCNUM_DYNA_VOL);
		//高低价不允许超过1000元
		dwTemp = stream.ReadDWORD(24);
		xg.m_pdwHighPrice[i] = dwTemp;
		dwTemp = stream.ReadDWORD(24);
		xg.m_pdwLowPrice[i] = dwTemp;
	}
	return 0;
}

int CDynaCompress::ExpandStatic6(CBitStream& stream, CGoods* pGoods)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		char cDiv = (char)stream.ReadDWORD(3);
		DWORD dwDiv = g_CheckDiv(cDiv);
		if (dwDiv==0)
			return ERR_COMPRESS_DIV;

		pGoods->m_cPriceDiv = cDiv;
		pGoods->m_dwPriceDiv = dwDiv;
		if (cDiv==0)
			pGoods->m_nPriceDiv = 1;
		else
			pGoods->m_nPriceDiv = 10;

		BYTE cCheckSum = 0;

		stream.DecodeXInt32(pGoods->m_xLTG, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		stream.DecodeXInt32(pGoods->m_xZFX, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &(pGoods->m_xLTG));
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xLTG), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xZFX), 4);

		pGoods->m_dwFirstDate = CHisCompress::ExpandDate(stream);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_dwFirstDate), 4);

		if (pGoods->GetID()==601607)
			pGoods->m_dwFirstDate = 19940324;

		// price
		DWORD dwClose = g_DivPrice(pGoods->m_dwClose, dwDiv);
		DWORD dwPrice;
		for (WORD w=0; w<7; w++)
		{	
			stream.DecodeData(dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
			switch (w)
			{
			case 0:
				pGoods->m_dwPClose = dwPrice * dwDiv;
				break;
			case 1:
				pGoods->m_dwWClose = dwPrice * dwDiv;
				break;
			case 2:
				pGoods->m_dw5Close = dwPrice * dwDiv;
				break;
			case 3:
				pGoods->m_dwPriceZT = dwPrice * dwDiv;
				break;
			case 4:
				pGoods->m_dwPriceDT = dwPrice * dwDiv;
				break;
			case 5:
				pGoods->m_dwClose9 = dwPrice * dwDiv;
				break;
			default:
				pGoods->m_dwClose19 = dwPrice * dwDiv;
			}
		}		

		stream.DecodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		stream.DecodeXInt32(pGoods->m_xSumBigAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumBigAmt4), 4);

		DWORD dwValue;

		stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
		if (pGoods->m_wGroupHY!=(WORD)dwValue)
		{
			pGoods->m_wGroupHY = (WORD)dwValue;
			pGoods->m_strHYName = pGoods->GetHYName();
		}
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_wGroupHY), 2);

		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLZC4);
		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLZC9);
		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLZC19);

		cCheckSum += l_ExpandZC(stream, pGoods->m_bZLQM);

		stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
		pGoods->m_wGroupDQ = (WORD)dwValue;
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_wGroupDQ), 2);

		short sGN = (short)stream.ReadDWORD(8);
		cCheckSum += GetBytesCheckSum((PBYTE)&sGN, 2);

		if (sGN>0)
		{
			pGoods->m_aGroupGN.RemoveAt(0, pGoods->m_aGroupGN.GetSize());

			for (short s=0; s<sGN; s++)
			{
				stream.DecodeData(dwValue, BC_DYNA_GROUPID, BCNUM_DYNA_GROUPID);
				cCheckSum += GetBytesCheckSum((PBYTE)&dwValue, 2);

				//				pGoods->m_aGroupGN.Add((WORD)dwValue);
				//Steven New Begin
				WORD wValue = (WORD)dwValue;
				//Steven New End
				pGoods->m_aGroupGN.Add(wValue);
			}
		}

		///////////////////////////////////////////////

		{
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGQM);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bDKBY9);
		}

		{
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf1Super);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf4Super);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf9Super);

			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf1Big);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf4Big);
			cCheckSum += l_ExpandBigAmount(stream, pGoods->m_DayStat.m_xAmtOf9Big);

			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGZC4);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGZC9);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bJGZC19);

			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bYZZC4);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bYZZC9);
			cCheckSum += l_ExpandZC(stream, pGoods->m_DayStat.m_bYZZC19);
		}

		///////////////////////////////////////////////

		stream.DecodeXInt32(pGoods->m_xSumAmt4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumAmt4), 4);

		cCheckSum += l_ExpandBigAmount(stream, pGoods->m_xSumBigVol4);

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		if (cCheckSum!=cCheckSumRecv)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandStaticQH6(CBitStream& stream, CGoods* pGoods)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		char cDiv = (char)stream.ReadDWORD(3);
		DWORD dwDiv = g_CheckDiv(cDiv);
		if (dwDiv==0)
			return ERR_COMPRESS_DIV;

		pGoods->m_cPriceDiv = cDiv;
		pGoods->m_dwPriceDiv = dwDiv;
		if (cDiv==0)
			pGoods->m_nPriceDiv = 1;
		else
			pGoods->m_nPriceDiv = 10;

		BYTE cCheckSum = 0;

		stream.DecodeXInt32(pGoods->m_xLTG, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		stream.DecodeXInt32(pGoods->m_xZFX, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &(pGoods->m_xLTG));
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xLTG), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xZFX), 4);

		pGoods->m_dwFirstDate = CHisCompress::ExpandDate(stream);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_dwFirstDate), 4);

		// price
		DWORD dwClose = g_DivPrice(pGoods->m_dwClose, dwDiv);
		DWORD dwPrice;
		for (WORD w=0; w<9; w++)
		{	
			stream.DecodeData(dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
			cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
			switch (w)
			{
			case 0:
				pGoods->m_dwPClose = dwPrice * dwDiv;
				break;
			case 1:
				pGoods->m_dwWClose = dwPrice * dwDiv;
				break;
			case 2:
				pGoods->m_dw5Close = dwPrice * dwDiv;
				break;
			case 3:
				pGoods->m_dwPriceZT = dwPrice * dwDiv;
				break;
			case 4:
				pGoods->m_dwPriceDT = dwPrice * dwDiv;
				break;
			case 5:
				pGoods->m_dwClose9 = dwPrice * dwDiv;
				break;
			case 6:
				pGoods->m_dwClose19 = dwPrice * dwDiv;
				break;
			case 7:
				pGoods->m_dwPreSettlement = dwPrice * dwDiv;
				break;
			default:
				pGoods->m_dwSettlement = dwPrice * dwDiv;
				break;
			}
		}		

		stream.DecodeXInt32(pGoods->m_xAveVol5, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pGoods->m_xSumVol4, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xAveVol5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xSumVol4), 4);

		stream.DecodeXInt32(pGoods->m_xPreOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		cCheckSum += GetBytesCheckSum((PBYTE)&(pGoods->m_xPreOpenInterest), 4);

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		if (cCheckSum!=cCheckSumRecv)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CDynaCompress::ExpandDynaData6(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDynaIn)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		BOOL bRelative = stream.ReadDWORD(1);
		if (!bRelative)
			return ExpandDynaData6(stream, pGoods, pDyna);

		if (pOldDynaIn == NULL)
			return ERR_COMPRESS_PARAM;

		BOOL bL2Encode = stream.ReadDWORD(1);

		char cDiv = (char)stream.ReadDWORD(3);
		DWORD dwDiv = g_CheckDiv(cDiv);
		if (dwDiv==0)
			return ERR_COMPRESS_DIV;

		pGoods->m_cPriceDiv = cDiv;
		pGoods->m_dwPriceDiv = dwDiv;
		if (cDiv==0)
			pGoods->m_nPriceDiv = 1;
		else
			pGoods->m_nPriceDiv = 10;

		CDynamicValue dynaOld;
		CDynamicValue* pOldDyna  = &dynaOld;

		CopyMemory(pOldDyna, pOldDynaIn, sizeof(CDynamicValue));
		pOldDyna->Div(dwDiv);

		//Date
		if (stream.ReadDWORD(1))
		{
			pDyna->m_dwDate = ExpandDate(stream, pOldDyna->m_dwDate);

			// time
			pDyna->m_dwTime = stream.ReadDWORD(18);
		}
		else
		{
			pDyna->m_dwDate = pOldDyna->m_dwDate;

			// time
			DWORD dwSecondsDiff = 0;
			stream.DecodeData(dwSecondsDiff, BC_DYNA_TIMEDIFF, BCNUM_DYNA_TIMEDIFF);
			pDyna->m_dwTime = AddSeconds(pOldDyna->m_dwTime, dwSecondsDiff);
		}
		//Date

		// price
		pDyna->m_dwOpen = pOldDyna->m_dwOpen;
		pDyna->m_dwHigh = pOldDyna->m_dwHigh;
		pDyna->m_dwLow = pOldDyna->m_dwLow;
		pDyna->m_dwPrice = pOldDyna->m_dwPrice;

		DWORD dwNumPrice = stream.ReadDWORD(3);		// 000 - no price, 001- P, 010 - PL, 011 - PH, 100 - PLHO

		if (dwNumPrice >= 1)		// P
		{
			stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice);
			if(dwNumPrice == 2 || dwNumPrice == 4)	// L
				stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwLow);
			if(dwNumPrice >= 3)		// H
			{
				stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwHigh);

				if(dwNumPrice == 4)		// O
					stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwOpen);
			}
		}

		BOOL bVATChanged = stream.ReadDWORD(1);
		if (bVATChanged)
		{
			// volume, amount
			stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xVolume);
			stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT, &pOldDyna->m_xAmount);
		}
		else
		{
			pDyna->m_xVolume = pOldDyna->m_xVolume;
			pDyna->m_xAmount = pOldDyna->m_xAmount;
		}

		if (stream.ReadDWORD(1))
			stream.DecodeData(pDyna->m_dwAvePrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwAvePrice);
		else
			pDyna->m_dwAvePrice = pOldDyna->m_dwAvePrice;

		if (stream.ReadDWORD(1))
			stream.DecodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM_DIFF, BCNUM_DYNA_TRADENUM_DIFF, &pOldDyna->m_dwTradeNum);
		else
			pDyna->m_dwTradeNum = pOldDyna->m_dwTradeNum;

		if (stream.ReadDWORD(1))
			stream.DecodeXInt32(pDyna->m_xOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF, &pOldDyna->m_xOpenInterest);
		else
			pDyna->m_xOpenInterest = pOldDyna->m_xOpenInterest;

		if (stream.ReadDWORD(1))
			stream.DecodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwNorminal);
		else
			pDyna->m_dwNorminal = pOldDyna->m_dwNorminal;

		// mmp
		BOOL bHasMMPData = stream.ReadDWORD(1);
		if (bHasMMPData)
		{
			BOOL bVirtualMMP = stream.ReadDWORD(1);
			if (bVirtualMMP)
				ExpandVirtualMMP(stream, pDyna, pOldDyna);
			else
			{
				BOOL bIsQH = stream.ReadDWORD(1);
				ExpandMMP(stream, pDyna, pOldDyna, bIsQH, bL2Encode);
			}
		}

		// PBUY, VBUY, PSELL, VSELL
		BOOL bPVBuySell = stream.ReadDWORD(1);
		if (bPVBuySell)
		{
			stream.DecodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPBuy);
			stream.DecodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			stream.DecodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPSell);
			stream.DecodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
		else
		{
			pDyna->m_dwPBuy = pOldDyna->m_dwPBuy;
			pDyna->m_xVBuy = pOldDyna->m_xVBuy;
			pDyna->m_dwPSell = pOldDyna->m_dwPSell;
			pDyna->m_xVSell = pOldDyna->m_xVSell;
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol
		BOOL bNPVol = stream.ReadDWORD(1);
		if (bNPVol)
		{
			stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF, &pOldDyna->m_xNeiPan);
			stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
			pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1
		}
		else
		{
			pDyna->m_xNeiPan = pOldDyna->m_xNeiPan;
			pDyna->m_xCurVol = pOldDyna->m_xCurVol;
			pDyna->m_cCurVol = pOldDyna->m_cCurVol;
		}

		if (stream.ReadDWORD(1))
			stream.DecodeXInt32(pDyna->m_xCurOI, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		else
			pDyna->m_xCurOI = 0;

		BOOL bPrice5 = stream.ReadDWORD(1);
		if (bPrice5)
			stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pOldDyna->m_dwPrice5);
		else
			pDyna->m_dwPrice5 = pOldDyna->m_dwPrice5;

		if (stream.ReadDWORD(1))
		{
			DWORD dw = 0;
			stream.DecodeData(dw, BC_DYNA_STRONG_DIFF, BCNUM_DYNA_STRONG_DIFF, (PDWORD)&pOldDyna->m_nStrong);
			pDyna->m_nStrong = dw;
		}
		else
			pDyna->m_nStrong = pOldDyna->m_nStrong;

		if (stream.ReadDWORD(1))
		{
			ExpandOrderCounts(stream, pDyna->m_ocSumOrder, pOldDyna->m_ocSumOrder, pDyna, TRUE);
			ExpandOrderCounts(stream, pDyna->m_ocSumTrade, pOldDyna->m_ocSumTrade, pDyna, FALSE);
		}
		else
		{
			pDyna->m_ocSumOrder = pOldDyna->m_ocSumOrder;
			pDyna->m_ocSumTrade = pOldDyna->m_ocSumTrade;
		}

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		BYTE cCheckSum = pDyna->GetChecksum6(TRUE);
		if (cCheckSum!=cCheckSumRecv)
		{
			DWORD dwGoodsID = pGoods->GetID();
			if (dwGoodsID!=300 && dwGoodsID!=1399300)
			{
				//				if (g_bError2Log)
				//				{
				//					CString str;
				//					str.Format("ExpandDynaData(%06d, %d) %d!=%d", dwGoodsID, pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
				//					theApp.m_log.Add(str, TRUE);
				//				}

				LOG_ERR("ExpandDynaData6(%06d, %d) %d!=%d", dwGoodsID, pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
				LOG_ERR(" %d %d(%d) = %d ",pOldDyna->m_dwHigh, pOldDyna->m_dwTime,pOldDynaIn->m_dwTime, pDyna->m_dwTime);
				return ERR_COMPRESS_CHECK;
			}
		}

		pDyna->Multi(dwDiv);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	return 0;
}

int CDynaCompress::ExpandDynaData6(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna)
{
	ZeroMemory(pDyna, sizeof(CDynamicValue));

	if (pGoods==NULL || pDyna==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		BOOL bL2Encode = stream.ReadDWORD(1);

		char cDiv = (char)stream.ReadDWORD(3);
		DWORD dwDiv = g_CheckDiv(cDiv);
		if (dwDiv==0)
			return ERR_COMPRESS_DIV;

		pGoods->m_cPriceDiv = cDiv;
		pGoods->m_dwPriceDiv = dwDiv;
		if (cDiv==0)
			pGoods->m_nPriceDiv = 1;
		else
			pGoods->m_nPriceDiv = 10;

		//Date
		if (stream.ReadDWORD(1))
			pDyna->m_dwDate = ExpandDate(stream, DYNA_BEGIN_DATE);

		// time
		pDyna->m_dwTime = stream.ReadDWORD(18);

		// price
		DWORD dwClose = g_DivPrice(pGoods->m_dwClose, dwDiv);

		stream.DecodeData(pDyna->m_dwOpen, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		stream.DecodeData(pDyna->m_dwHigh, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen);
		stream.DecodeData(pDyna->m_dwLow, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwOpen, TRUE);
		stream.DecodeData(pDyna->m_dwPrice, BC_DYNA_PRICE_DIFF, BCNUM_DYNA_PRICE_DIFF, &pDyna->m_dwLow);

		// volume, amount
		stream.DecodeXInt32(pDyna->m_xVolume, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pDyna->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);

		if (stream.ReadDWORD(1))
			stream.DecodeData(pDyna->m_dwAvePrice, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);
		else
			pDyna->m_dwAvePrice = 0;

		if (stream.ReadDWORD(1))
			stream.DecodeData(pDyna->m_dwTradeNum, BC_DYNA_TRADENUM, BCNUM_DYNA_TRADENUM);
		else
			pDyna->m_dwTradeNum = 0;

		if (stream.ReadDWORD(1))
			stream.DecodeXInt32(pDyna->m_xOpenInterest, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		else
			pDyna->m_xOpenInterest = 0;

		if (stream.ReadDWORD(1))
			stream.DecodeData(pDyna->m_dwNorminal, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwClose);
		else
			pDyna->m_dwNorminal = 0;

		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();

		// mmp
		BOOL bHasMMPData = stream.ReadDWORD(1);
		if (bHasMMPData)
		{
			BOOL bVirtualMMP = stream.ReadDWORD(1);
			if (bVirtualMMP)
				ExpandVirtualMMP(stream, pDyna);
			else
			{
				BOOL bIsQH = stream.ReadDWORD(1);
				ExpandMMP(stream, pDyna, bIsQH, bL2Encode);
			}
		}

		// PBUY, VBUY, PSELL, VSELL
		BOOL bPVBuySell = stream.ReadDWORD(1);
		if (bPVBuySell)
		{
			DWORD dwBase = pDyna->m_dwLow*dwDiv;
			stream.DecodeData(pDyna->m_dwPBuy, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwBase, TRUE);
			stream.DecodeXInt32(pDyna->m_xVBuy, BC_DYNA_VOL, BCNUM_DYNA_VOL);
			dwBase = pDyna->m_dwHigh*dwDiv;
			stream.DecodeData(pDyna->m_dwPSell, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &dwBase);
			stream.DecodeXInt32(pDyna->m_xVSell, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		}
		else
		{
			pDyna->m_dwPBuy = 0;
			pDyna->m_xVBuy = 0;
			pDyna->m_dwPSell = 0;
			pDyna->m_xVSell = 0;
		}

		// m_xNeiPan, m_xCurVol, m_cCurVol
		stream.DecodeXInt32(pDyna->m_xNeiPan, BC_DYNA_VOL, BCNUM_DYNA_VOL);
		stream.DecodeXInt32(pDyna->m_xCurVol, BC_DYNA_VOL_DIFF, BCNUM_DYNA_VOL_DIFF);
		pDyna->m_cCurVol = (BYTE)stream.ReadDWORD(2) - 1;		// -1 0 1

		if (stream.ReadDWORD(1))
			stream.DecodeXInt32(pDyna->m_xCurOI, BC_DYNA_QH_OI_DIFF, BCNUM_DYNA_QH_OI_DIFF);
		else
			pDyna->m_xCurOI = 0;

		stream.DecodeData(pDyna->m_dwPrice5, BC_DYNA_PRICE_DIFFS, BCNUM_DYNA_PRICE_DIFFS, &pDyna->m_dwPrice);

		if (stream.ReadDWORD(1))
		{
			DWORD dw = 0;
			stream.DecodeData(dw, BC_DYNA_STRONG, BCNUM_DYNA_STRONG);
			pDyna->m_nStrong = dw;
		}
		else
			pDyna->m_nStrong = 0;

		if (stream.ReadDWORD(1))
		{
			ExpandOrderCounts(stream, pDyna->m_ocSumOrder, pDyna, TRUE);
			ExpandOrderCounts(stream, pDyna->m_ocSumTrade, pDyna, FALSE);
		}
		else
		{
			pDyna->m_ocSumOrder.Clear();
			pDyna->m_ocSumTrade.Clear();
		}

		BYTE cCheckSumRecv = (char)stream.ReadDWORD(8);
		BYTE cCheckSum = pDyna->GetChecksum6(TRUE);
		if (cCheckSum!=cCheckSumRecv)
		{
			DWORD dwGoodsID = pGoods->GetID();
			if (dwGoodsID!=300 && dwGoodsID!=1399300)
			{
				//				if (g_bError2Log)
				//				{
				//					CString str;
				//					str.Format("ExpandDynaData6(%06d, %d) %d!=%d", dwGoodsID, pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
				//					theApp.m_log.Add(str, TRUE);
				//				}

				LOG_ERR("ExpandDynaData6(%06d, %d) %d!=%d", dwGoodsID, pDyna->m_dwTime, cCheckSum, cCheckSumRecv);
				return ERR_COMPRESS_CHECK;
			}
		}

		pDyna->Multi(dwDiv);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	return 0;
}
