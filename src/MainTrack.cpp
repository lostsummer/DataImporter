#include <new>
#include "MainTrack.h"
#include "Buffer.h"
#include "Compress.h"

CMainTrackAll g_mt;

//////////////////////////////////////////////////////////////////////

CMainTrackAll::CMainTrackAll()
{
	m_dwCountFresh = 0;
	m_dwCountCompress = 0;

	m_pcBuffer = NULL;
	m_dwBuffer = 0;
	m_dwTotal = 0;
}

CMainTrackAll::~CMainTrackAll()
{
	if (m_pcBuffer)
		delete [] m_pcBuffer;
}

void CMainTrackAll::Init()
{
	return;

	m_wr.AcquireWriteLock();

	m_aZC.RemoveAll();
	m_aQM.RemoveAll();

	m_dwTotal = 0;
	m_dwCountFresh = 0;
	m_dwCountCompress = 0;

	m_wr.ReleaseWriteLock();

	Compress();
}

void CMainTrackAll::Compress()
{
	m_wr.AcquireWriteLock();

	m_dwTotal = 0;

	int nSizeZC = (int)m_aZC.GetSize();
	int nSizeQM = (int)m_aQM.GetSize();

	CBuffer buf;
	buf.Initialize(2+4+4+(nSizeZC+nSizeQM)*9, true);

	try
	{
		buf.WriteShort(1);		// wVersion

		buf.WriteInt(nSizeZC);
		for (int i=0; i<nSizeZC; i++)
		{
			CMainTrack& mt = m_aZC[i];
			buf.WriteInt(mt.m_dwGoodsID);
			buf.WriteInt(mt.m_nValue);
			buf.WriteChar(mt.m_cGroupHY);
		}

		buf.WriteInt(nSizeQM);
		for (int i=0; i<nSizeQM; i++)
		{
			CMainTrack& mt = m_aQM[i];
			buf.WriteInt(mt.m_dwGoodsID);
			buf.WriteInt(mt.m_nValue);
			buf.WriteChar(mt.m_cGroupHY);
		}
	}
	catch(int)
	{
	}

	CCompress comp;
	comp.m_lIn = buf.GetBufferLen();
	comp.m_pcIn = (char*)buf.GetBuffer();

	if (comp.m_pcIn && comp.Encode() && comp.m_lOut>0 && comp.m_pcOut)
	{
		if (m_dwBuffer<(DWORD)comp.m_lOut)
		{
			if (m_pcBuffer)
				delete [] m_pcBuffer;

			m_dwBuffer = (comp.m_lOut+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE;
			m_pcBuffer = new(std::nothrow) char[m_dwBuffer];
			if (m_pcBuffer==NULL)
				m_dwBuffer = 0;
		}

		if (m_pcBuffer && m_dwBuffer>=(DWORD)comp.m_lOut)
		{
			CopyMemory(m_pcBuffer, comp.m_pcOut, comp.m_lOut);
			m_dwTotal = comp.m_lOut;
		}
	}

	m_dwCountCompress++;

	m_wr.ReleaseWriteLock();
}

void CMainTrackAll::AddZC(DWORD dwGoodsID, int nValue, char cGroupHY)
{
	m_wr.AcquireWriteLock();

	m_aZC.Change(CMainTrack(dwGoodsID, nValue, cGroupHY));
	m_dwCountFresh++;

	m_wr.ReleaseWriteLock();
}

void CMainTrackAll::ChangeZC(DWORD dwGoodsID, int nValue, char cGroupHY)
{
	m_wr.AcquireWriteLock();

	m_aZC.Change(CMainTrack(dwGoodsID, nValue, cGroupHY));
	m_dwCountFresh++;

	m_wr.ReleaseWriteLock();
}

void CMainTrackAll::DeleteZC(DWORD dwGoodsID)
{
	m_wr.AcquireWriteLock();

	int nFindPos = m_aZC.Find(CMainTrack(dwGoodsID, 0, 0));
	if (nFindPos>=0)
	{
		m_aZC.RemoveAt(nFindPos);
		m_dwCountFresh++;
	}

	m_wr.ReleaseWriteLock();
}

void CMainTrackAll::AddQM(DWORD dwGoodsID, int nValue, char cGroupHY)
{
	m_wr.AcquireWriteLock();

	m_aQM.Change(CMainTrack(dwGoodsID, nValue, cGroupHY));
	m_dwCountFresh++;

	m_wr.ReleaseWriteLock();
}

void CMainTrackAll::ChangeQM(DWORD dwGoodsID, int nValue, char cGroupHY)
{
	m_wr.AcquireWriteLock();

	m_aQM.Change(CMainTrack(dwGoodsID, nValue, cGroupHY));
	m_dwCountFresh++;

	m_wr.ReleaseWriteLock();
}

void CMainTrackAll::DeleteQM(DWORD dwGoodsID)
{
	m_wr.AcquireWriteLock();

	int nFindPos = m_aQM.Find(CMainTrack(dwGoodsID, 0, 0));
	if (nFindPos>=0)
	{
		m_aQM.RemoveAt(nFindPos);
		m_dwCountFresh++;
	}

	m_wr.ReleaseWriteLock();
}

