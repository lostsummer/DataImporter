#include <stdlib.h>
#include <string.h>

#include "MDSConfig.h"

#include "MyLogAdapter.h"
//{ add by panlsihen 2009-12-10
#ifndef WIN32
#define _snprintf snprintf 
#define _vsnprintf vsnprintf 
#endif
//}

CMDSConfig::CMDSConfig(void)
{
}

CMDSConfig::~CMDSConfig(void)
{
}

int CMDSConfig::GetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
//{ modify by panlishen 2010-01-05
	std::string value;

	cfg_mgr.GetConfig(lpszSection, lpszEntry, value);

	if(value.empty())
	{
		return nDefault;	
	}
	else
	{
		return atoi(value.c_str());
	}
//} modify by panlishen 2010-01-05
}

//{ add by panlsihen 2009-12-10
void CMDSConfig::SetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nVal)
{
	char buffer[64] = {0};
	_snprintf(buffer, sizeof(buffer), "%d", nVal);

	cfg_mgr.SetConfig(lpszSection, lpszEntry, buffer);
	Save_Config();
}

void CMDSConfig::SetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszVal)
{
	cfg_mgr.SetConfig(lpszSection, lpszEntry, lpszVal);
	Save_Config();
}

//}

CString CMDSConfig::GetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszDefault)
{
//{ modify by panlishen 2010-01-05
	std::string value;

	cfg_mgr.GetConfig(lpszSection, lpszEntry, value);

	if(value.empty())
	{
		return CString(lpszDefault);
	}
	else
	{
		return CString(value.c_str());
	}
//} modify by panlishen 2010-01-05
}

//加载配置
int CMDSConfig::Load_Config(const std::string& configFile) // modify by panlishen 2009-12-2
{
	int rc=0;
	try
	{
		cfg_loader.SetpData(&cfg_data);
		cfg_loader.SetFileName(configFile);
		cfg_loader.Load();

		cfg_mgr.SetpData(&cfg_data);
	}
	catch(...)
	{
		rc=-1;
	}
	return rc;
}


//输出所有配置
void CMDSConfig::ListAll(void)
{
	LogInfo("All Config Count=%d", cfg_data.m_listitem.size());
	for(std::vector<ConfigDataItem>::const_iterator cit = cfg_data.m_listitem.begin(); cit != cfg_data.m_listitem.end(); ++cit)
	{
		LogInfo("[%s]-%s=%s", cit->m_group.c_str(), cit->m_item.c_str(), 
			cit->m_value.c_str());
	}
}

//  保存配置
int CMDSConfig::Save_Config(void)
{
	int rc=0;
	try
	{
		cfg_loader.Save();
	}
	catch(...)
	{
		rc=-1;
	}
	return rc;
}


