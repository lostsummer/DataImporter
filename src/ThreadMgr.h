/** *************************************************************************
* MDS
* Copyright 2011 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file ThreadMgr.h
* @brief  
*
* $Author: panlishen$
* $Date: Thu Jul 28 10:13:45 UTC+0800 2011$
* $Revision$
*/
/* *********************************************************************** */
#ifndef _THREAD_MGR_H_
#define _THREAD_MGR_H_

#include <list>

#include "Util.h"
#include "RWLock.h"

class CMyThread;
/**
* 线程管理
*/
class ThreadMgr :  public YSingleton<ThreadMgr>
{
public:
	ThreadMgr();
	~ThreadMgr();

	/**
	* 添加线程
	*/
	void AddThread(CMyThread* entry);

	/**
	* 删除线程
	*/
	void RemoveThread(CMyThread* entry);
	
	/**
	* 线程状态
	*/
	void Status();

protected:
	typedef std::list<CMyThread*> Container;

private:
	Container m_lts;
	RWLock m_rwLock;
};





#endif



