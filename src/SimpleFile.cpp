/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file SimpleFile.cpp
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 18 19:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#include <errno.h>

#include "SimpleFile.h"

#ifdef _MSC_VER
#pragma warning(disable:4800)
#endif
/**
* @brief constructor
*/
SimpleFile::SimpleFile() 
	: m_hFile(-1)
	, m_OpenFlag(O_RDWR)
	, m_AccessMode(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)
	, m_FileSize(0)
	, m_LastError(0)
{
}

/**
* @brief destructor
*/
SimpleFile::~SimpleFile()
{
	Close();
}

/**
* @brief copy create param of file
* @param aFile
*/
void SimpleFile::CopyCreateParam(const SimpleFile& aFile)
{
	m_FileName = aFile.m_FileName;
	m_OpenFlag = aFile.m_OpenFlag;
	m_AccessMode = aFile.m_AccessMode;
}

/**
* @brief check a directory whether exists
* @param fn
* @return if specify directory exists return true else false 
*/
bool SimpleFile::IsDirectoryExists(const char* fn)
{
	struct stat st;
	int ret = stat(fn, &st);
	if (ret == 0 && S_ISDIR(st.st_mode))
		return true;
	else
		return false;
}

/**
* @brief check a file whether exists
* @param fn
* @return if specify file exists return true else false 
*/
bool SimpleFile::IsFileExists(const char* fn)
{
	struct stat st;
	int ret = stat(fn, &st);
	if (ret == 0 && S_ISREG(st.st_mode))
		return true;
	else
		return false;
}

UINT64 SimpleFile::GetFileSize(int fd)
{
	if (fd > 0)
	{
		struct stat st;
		int ret = fstat(fd, &st);
		if (ret == 0)
		{
			return st.st_size;
		}
	}

	return 0;
}

UINT64 SimpleFile::GetFileSize(const char* file)
{
	if (file != NULL)
	{
		struct stat st;
		int ret = stat(file, &st);
		if (ret == 0)
		{
			return st.st_size;
		}
	}

	return 0;
}

/**
* @brief get file size
* @return file size 
*/
size_t SimpleFile::GetFileSize()
{
	if (IsValid())
	{
		struct stat st;
		int ret = fstat(m_hFile, &st);
		if (ret == 0)
		{
			return st.st_size;
		}
	}
		
	return 0;
}

/**
* @brief close file
*/
void SimpleFile::Close()
{
	if (IsValid())
	{
		close(m_hFile);
		m_hFile = -1;
	}
}

/**
* @brief create file
* @return if successfully true else false
*/
bool SimpleFile::DoCreate()
{
	m_hFile = open(m_FileName.c_str(), m_OpenFlag, m_AccessMode);
	if (IsValid())
	{
		m_FileSize = GetFileSize();

		return true;
	}
	else 
	{
		m_LastError = errno;

		return false;
	}
}

/**
* @brief open a file that already existing
* @return if successfully true else false
*/
bool SimpleFile::OpenExisting()
{
	return DoCreate();
}

/**
* @brief open a file, if it not exist create it.
* @return if successfully true else false
*/
bool SimpleFile::OpenAlways()
{
	m_OpenFlag |= O_CREAT;

	return DoCreate();
}

/**
* @brief set file pointer
* @param pos
* @return if successfully true else false
*/
bool SimpleFile::SetFilePointer(int Pos)
{
	int ret = lseek(m_hFile, Pos, SEEK_SET);
	if (ret == -1)
	{
		m_LastError = errno;

		return false;
	}

	return true;
}

/**
* @brief read data from file
* @param buffer, buffer_size
* @return if successfully true else false
*/
bool SimpleFile::Read(void* buffer, DWORD buffer_size)
{
	int ret = read(m_hFile, buffer, buffer_size);
	if (ret != -1)
		return true;
	else
	{
		m_LastError = errno;

		return false;
	}
}

/**
* @brief write data to file
* @param buffer, buffer_size
* @return if successfully true else false
*/
bool SimpleFile::Write(void* buffer, DWORD buffer_size)
{
	int ret = write(m_hFile, buffer, buffer_size);
	if (ret != -1 && ret == (int)buffer_size)
		return true;
	else
	{
		m_LastError = errno;

		return false;
	}
}



