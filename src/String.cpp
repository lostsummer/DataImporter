// String.cpp: implementation of the CString class.
//
//////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <ctype.h>

#include "String.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CString::FreeData()
{
	if (m_pcData!=NULL)
	{
		delete[] m_pcData;
		m_pcData = NULL;
	}

	m_nAllocLength = 0;
}

TCHAR* CString::AllocBuffer(int nLen)
{
	if (m_nAllocLength<nLen)
	{
		FreeData();

		int nNewAlloc = (nLen+63)/64*64;
		m_pcData = new(std::nothrow) TCHAR[nNewAlloc];
		if (m_pcData!=NULL)
		{
			memset(m_pcData, 0, nNewAlloc);

			m_nAllocLength = nNewAlloc;
		}
	}
	return m_pcData;
}

void CString::AllocCopy(int nLen)
{
	if (m_nAllocLength<nLen)
	{
		int nNewAlloc = (nLen+63)/64*64;
		TCHAR* pcNewData = new(std::nothrow) TCHAR[nNewAlloc];
		if (pcNewData!=NULL)
		{
			memset(pcNewData, 0, nNewAlloc);

			if (m_pcData!=NULL)
			{
				_tcscpy(pcNewData, m_pcData);
				delete[] m_pcData;
			}
			m_pcData = pcNewData;
			m_nAllocLength = nNewAlloc;
		}
	}
}

const CString& CString::operator=(const CString& strSrc)
{
	int nLen = strSrc.GetLength();
	AllocBuffer(nLen+1);
	if (m_pcData!=NULL)
	{
		if (nLen==0)
			m_pcData[0] = 0;
		else
			_tcscpy(m_pcData, strSrc);
	}
	return *this;
}

const CString& CString::operator=(LPCTSTR lpsz)
{
	if (lpsz==NULL)
	{
		AllocBuffer(1);
		if (m_pcData!=NULL)
			m_pcData[0] = 0;
	}
	else
	{
		AllocBuffer(_tcslen(lpsz)+1);
		if (m_pcData!=NULL)
			_tcscpy(m_pcData, lpsz);
	}
	return *this;
}

const CString& CString::operator=(LPTSTR lpsz)
{
	if (lpsz==NULL)
	{
		AllocBuffer(1);
		if (m_pcData!=NULL)
			m_pcData[0] = 0;
	}
	else
	{
		AllocBuffer(_tcslen(lpsz)+1);
		if (m_pcData!=NULL)
			_tcscpy(m_pcData, lpsz);
	}
	return *this;
}

void CString::ConcatCopy(int nSrc1Len, LPCTSTR lpszSrc1Data, int nSrc2Len, LPCTSTR lpszSrc2Data)
{
  // -- master concatenation routine
  // Concatenate two sources
  // -- assume that 'this' is a new CString object

	int nNewLen = nSrc1Len + nSrc2Len;
	if (nNewLen != 0)
	{
		AllocBuffer(nNewLen);
		memcpy(m_pcData, lpszSrc1Data, nSrc1Len*sizeof(TCHAR));
		memcpy(m_pcData+nSrc1Len, lpszSrc2Data, nSrc2Len*sizeof(TCHAR));
	}
}

CString operator+(const CString& string1, const CString& string2)
{
	CString s;
	s.ConcatCopy(string1.GetLength(), string1.m_pcData,	string2.GetLength(), string2.m_pcData);
	return s;
}

CString operator+(const CString& string, LPCTSTR lpsz)
{
	CString s;
	s.ConcatCopy(string.GetLength(), string.m_pcData, CString::SafeStrlen(lpsz), lpsz);
	return s;
}

CString operator+(const CString& string, LPTSTR lpsz)
{
	CString s;
	s.ConcatCopy(string.GetLength(), string.m_pcData, CString::SafeStrlen(lpsz), lpsz);
	return s;
}

CString operator+(LPCTSTR lpsz, const CString& string)
{
	CString s;
	s.ConcatCopy(CString::SafeStrlen(lpsz), lpsz, string.GetLength(), string.m_pcData);
	return s;
}

CString operator+(LPTSTR lpsz, const CString& string)
{
	CString s;
	s.ConcatCopy(CString::SafeStrlen(lpsz), lpsz, string.GetLength(), string.m_pcData);
	return s;
}

void CString::ConcatInPlace(int nSrcLen, LPCTSTR lpszSrcData)
{
	if (m_pcData==NULL)
	{
		AllocBuffer(nSrcLen+1);
		if (m_pcData!=NULL)
			_tcscpy(m_pcData, lpszSrcData);
	}
	else
	{
		AllocCopy(nSrcLen+_tcslen(m_pcData)+1);
		if (m_pcData!=NULL)
			_tcscat(m_pcData, lpszSrcData);
	}
}

const CString& CString::operator+=(LPCTSTR lpsz)
{
	if (lpsz!=NULL)
		ConcatInPlace(_tcslen(lpsz), lpsz);
	return *this;
}

const CString& CString::operator+=(LPTSTR lpsz)
{
	if (lpsz!=NULL)
		ConcatInPlace(_tcslen(lpsz), lpsz);
	return *this;
}

const CString& CString::operator+=(const CString& strSrc)
{
	int nLen = strSrc.GetLength();
	if (nLen>0)
		ConcatInPlace(nLen, strSrc.m_pcData);
	return *this;
}

const CString& CString::operator+=(const TCHAR cSrc)
{
	TCHAR pc[2];
	pc[0] = cSrc;
	pc[1] = 0;
	ConcatInPlace(1, pc);
	return *this;
}

void CString::Format(LPCTSTR lpszFormat, ...)
{
	va_list argList;
	char buffer[1024];

	memset(buffer, 0, 1024);

	va_start(argList, lpszFormat);

	vsnprintf(buffer, 1023, lpszFormat, argList);

	va_end(argList);

	*this = buffer;
}

void CString::Format(LPTSTR lpszFormat, ...)
{
	va_list argList;
	char buffer[1024];

	memset(buffer, 0, 1024);

	va_start(argList, lpszFormat);

	vsnprintf(buffer, 1023, lpszFormat, argList);

	va_end(argList);

	*this = buffer;
}


int CString::Find(TCHAR ch) const
{
	return Find(ch, 0);
}

int CString::Find(TCHAR ch, int nStart) const
{
	int nLength = GetLength();
	if (nStart >= nLength)
		return -1;

	// find first single character
	LPTSTR lpsz = _tcschr(m_pcData + nStart, ch);

	// return -1 if not found and index otherwise
	return (lpsz == NULL) ? -1 : (int)(lpsz - m_pcData);
}

// find a sub-string (like strstr)
int CString::Find(LPCTSTR lpszSub) const
{
	return Find(lpszSub, 0);
}

int CString::Find(LPCTSTR lpszSub, int nStart) const
{
	int nLength = GetLength();
	if (nStart > nLength)
		return -1;

	// find first matching substring
	LPTSTR lpsz = _tcsstr(m_pcData + nStart, lpszSub);

	// return -1 for not found, distance from beginning otherwise
	return (lpsz == NULL) ? -1 : (int)(lpsz - m_pcData);
}

CString CString::Left( size_t len )
{
	char * pTemp;
	CString strRet;
	if( !m_pcData )
	{
		strRet.Empty();
		return strRet;
	}
	if( len >= strlen( m_pcData ) )
	{
		strRet = m_pcData;
	}
	else
	{
		pTemp = (char *)malloc( len +1 );
		bzero( pTemp, len+1 );
		memcpy( pTemp , m_pcData, len );
		strRet = pTemp;
		free( pTemp );
	}
	pTemp = NULL;
	return strRet;
}

CString CString::Right( size_t len )
{
	char * pTemp;
	CString strRet;
	if( !m_pcData )
	{
		strRet.Empty();
		return strRet;
	}
	size_t srcLen = strlen( m_pcData );
	if( len >= srcLen )
		strRet = m_pcData;
	else
	{
		pTemp = (char *)malloc( len +1 );
		bzero( pTemp, len+1 );
		memcpy( pTemp , m_pcData +( srcLen - len ), len );
		strRet = pTemp;
		free( pTemp );
	}
	pTemp = NULL;
	return strRet;
}

CString CString::Mid(unsigned int nStart, size_t len )
{
	char * pTemp;
	CString strRet;
	if( !m_pcData )
	{
		strRet.Empty();
		return strRet;
	}
	size_t srcLen = strlen( m_pcData );
	if( nStart >= srcLen )
	{
		strRet = "";
		return strRet;
	}
	if( len == 0 )
	{
		//		memcpy( pTemp , m_pcData +( nStart )-1, ( srcLen - nStart ) + 1 );
		strRet = m_pcData +( nStart );
	}
	else
	{
		pTemp = (char *)malloc(  ( srcLen - nStart ) + 2 );
		bzero( pTemp, ( srcLen - nStart ) + 2 );
		if( (len + nStart) >= srcLen )
			strRet = m_pcData + nStart ;
		else
		{
			memcpy( pTemp , m_pcData +( nStart ), len );
			strRet = pTemp;
		}
		free( pTemp );
	}
	pTemp = NULL;
	return strRet;
}

void CString::TrimLeft( LPCTSTR pSpace )
{
	char * pTemp;
	CString strRet;
	if( !m_pcData )
		return;
	size_t srcLen = SafeStrlen( m_pcData );
	int i = 1;
	while( i < srcLen && ( strchr( pSpace, m_pcData[i++] ) != NULL ) )
	{

	}
	i--;
	if( i > 1 )
	{
		printf("i:%d\n%X\n%X\n",i, m_pcData, m_pcData + i);
		memmove(m_pcData , m_pcData + i , srcLen - i );
		bzero( m_pcData + (srcLen - i),i );
	}
}

void CString::TrimRight( LPCTSTR pSpace )
{
	char * pTemp;
	CString strRet;
	if( !m_pcData )
		return;
	size_t srcLen = SafeStrlen( m_pcData );
	int i = srcLen -1;
	while( i > 0  && ( strchr( pSpace, m_pcData[i--] ) != NULL ) )
	{
		m_pcData[ i-- ] = '\0';
	}
}

void CString::Trim( LPCTSTR pSpace )
{
	TrimRight( pSpace );
	TrimLeft( pSpace );
}

CString CString::SpanExcluding(const char * pSubstr) 
{
	CString strSub;
	if( !m_pcData )
	{
		strSub.Empty();
		return strSub;
	}
	char * pRaw = (char *) m_pcData;
	size_t nPos = strcspn( pRaw, pSubstr );

	if( nPos == 0 )
	{
		strSub = m_pcData;
	}
	else
	{
		char  * newchar =(char *) malloc( nPos + 1 );
		strncpy( newchar, pRaw, nPos );
		strSub = newchar;
		free( newchar );
	}

	return strSub;
}

const CString &CString::Replace( const char orChar, const char newChar ) 
{
	if( !m_pcData )
	{
		return *this;
	}
	char * newData = new char [ m_nAllocLength ];
	bzero( newData, m_nAllocLength );
	size_t len = GetLength();
	memcpy( newData, m_pcData, len );
	for ( int i = 0; i <len ; i ++ )
	{
		if( newData[ i ] == orChar )
			newData[i] = newChar;
	}
	*this = newData;
	delete[] newData;
	return *this;
}


