#include "MDS.h"
#include "Grid.h"

int g_AddGoods(CGoods* pGoods, CPtrArray& aGoods, short sSortID, char cSort, int nMaxSize)
{
	int nSize = aGoods.GetSize();

	int nLeft = 0;
	int nRight = nSize-1;		
	if (nRight<nLeft)
	{
		aGoods.Add(pGoods);
		return 0;
	}

	if (nMaxSize>0 && nSize>=nMaxSize)
	{
		CGoods* pG = (CGoods*)aGoods[nRight];
		char c = pGoods->Compare(pG, sSortID, cSort);
		if (c>=0)
			return -1;
	}

	for ( ; nLeft<=nRight; )
	{
		CGoods* pG = (CGoods*)aGoods[nLeft];
		char c = pGoods->Compare(pG, sSortID, cSort);
		if (c<0)
		{
			aGoods.InsertAt(nLeft, pGoods);
			if (nMaxSize>0 && nSize>=nMaxSize)
				aGoods.RemoveAt(nMaxSize);
			return nLeft;
		}

        if (c==0)
        	return -1;
        
		pG = (CGoods*)aGoods[nRight];
		c = pGoods->Compare(pG, sSortID, cSort);
		if (c>0)
		{
			aGoods.InsertAt(nRight+1, pGoods);
			if (nMaxSize>0 && nSize>=nMaxSize)
				aGoods.RemoveAt(nMaxSize);
			return (nRight+1);
		}

        if (c==0)
        	return -1;

		int nMid = (nLeft + nRight)/2;
		pG = (CGoods*)aGoods[nMid];
		c = pGoods->Compare(pG, sSortID, cSort);
		if (c>0)
			nLeft = nMid+1;
        else if (c==0)
        	return -1;
        else
			nRight = nMid-1;
	}

	return -1;
}

//////////////////////////////////////////////////////////////////////

CGrid::CGrid(char cGroup, CString& strGroup, short sSortID, char cSort, BOOL GZQHFilter)
{
	m_cGroup = cGroup;
	m_strGroup = strGroup;

	m_sSortID = sSortID;
	m_cSort = cSort;

	m_dwRefreshSecond = 0;
	m_dwValueTime = 0;

	m_bQZQHFilter = GZQHFilter;
}

CGrid::~CGrid()
{
	m_wrGoods.AcquireWriteLock();

	m_aGoods.RemoveAll();
	
	m_wrGoods.ReleaseWriteLock();
}

void CGrid::Refresh()
{
	if (CMDSApp::g_dwSysSecond-m_dwRefreshSecond >= g_grid.m_dwRefreshInterval && m_dwValueTime!=theApp.m_dwTime)
	{
		CGridFilter *pFilter = NULL;

		if( m_bQZQHFilter ){
			pFilter = new CGridGZQHFilter(m_bQZQHFilter);
		}
			
		CPtrArray aGoods;

		if (m_cGroup>=0)
		{
			CPtrArray aGoodsAll;

			theApp.m_wrGoods.AcquireReadLock();
			aGoodsAll.Copy(theApp.m_aGoods);
			theApp.m_wrGoods.ReleaseReadLock();
			
			int nGoods = aGoodsAll.GetSize();
			for (int i=0; i<nGoods; i++)
			{
				CGoods* pGoods = (CGoods*)aGoodsAll[i];
				if (pGoods && pGoods->InMobileGroup(m_cGroup) && CheckFilter(pGoods, pFilter) )
				{
					if (m_sSortID==JJJZ_N || m_sSortID==JJJZ_RFPER)
					{
						if (pGoods->GetValue(JJJZ_N)>=0)
							g_AddGoods(pGoods, aGoods, m_sSortID, m_cSort, 0);
					}
					else
						g_AddGoods(pGoods, aGoods, m_sSortID, m_cSort, 0);
				}
			}

			aGoodsAll.RemoveAll();	
		}
		else
		{
			CString strClass;
			if (m_cGroup == -15)
				strClass = "行业板块2";
			else if (m_cGroup == -16)
				strClass = "地区板块";
			else if (m_cGroup == -14)
			{
				strClass = "概念板块";
			}
			else if (m_cGroup == -25 || m_cGroup == -44)
			{
				strClass = "三级概念";
			}
			else if (m_cGroup == -26)
			{
				strClass = "热点板块";
			}
			else if (m_cGroup == -46)
			{	//2005xxx 小行业
				strClass = "三级行业";
			}
			else if (m_cGroup == -45)
			{
				//2006xxx 
				strClass = "三级概念";
			}
			else
				strClass = "益盟板块";

			if (m_cGroup == -14 && m_strGroup.Compare("创业板") == 0)
			{
				CPtrArray aGoodsAll;

				theApp.m_wrGoods.AcquireReadLock();
				aGoodsAll.Copy(theApp.m_aGoods);
				theApp.m_wrGoods.ReleaseReadLock();

				int nGoods = aGoodsAll.GetSize();
				for (int i=0; i<nGoods; i++)
				{
					CGoods* pGoods = (CGoods*)aGoodsAll[i];
					if (pGoods && pGoods->InMobileGroup(22) && CheckFilter(pGoods, pFilter) )
					{
						g_AddGoods(pGoods, aGoods, m_sSortID, m_cSort, 0);
					}
				}

				aGoodsAll.RemoveAll();	
			}
			else
			{
				
				CGroup* pGroup = theApp.GetGroup(strClass, m_strGroup);
				if (pGroup)
				{
					// 防止多线程野指针问题，需要添加读锁 [11/6/2012 frantqian]
					//theApp.m_wrGoods.AcquireReadLock();
					int nGoods = pGroup->m_aGoods.GetSize();
					for (int i=0; i<nGoods; i++)
					{
						CGoods* pGoods = (CGoods*)pGroup->m_aGoods[i];
						if( CheckFilter(pGoods, pFilter) && !pGoods->IsBad() ){
							g_AddGoods(pGoods, aGoods, m_sSortID, m_cSort, 0);
						}
					}
					//theApp.m_wrGoods.ReleaseReadLock();
				}
				
			}

		}


		if( pFilter != NULL ){
			delete pFilter;
		}
			

		m_wrGoods.AcquireWriteLock();

		m_aGoods.Copy(aGoods);
		
		m_wrGoods.ReleaseWriteLock();

		aGoods.RemoveAll();

		m_dwRefreshSecond = CMDSApp::g_dwSysSecond;
		m_dwValueTime = theApp.m_dwTime;
	}
}

short CGrid::Copy(CPtrArray& aGoods, short sOffset, short sNum, short& sTotal)
{
	Refresh();

	m_wrGoods.AcquireReadLock();

	sTotal = (short)m_aGoods.GetSize();
	short sRet = 0;
	if (sOffset>=0 && sOffset<sTotal)
	{
		for (short s=sOffset; s<sTotal; s++)
		{
			if (sRet<sNum)
			{
				aGoods.Add(m_aGoods[s]);
				sRet++;
			}
			else
			{
				break;
			}
		}
	}

	m_wrGoods.ReleaseReadLock();

	return sRet;
}

short CGrid::Copy(std::set<DWORD>& aGoods, short sOffset, short sNum, short& sTotal)
{
	Refresh();

	m_wrGoods.AcquireReadLock();

	sTotal = (short)m_aGoods.GetSize();
	short sRet = 0;
	if (sOffset>=0 && sOffset<sTotal)
	{
		for (short s=sOffset; s<sTotal; s++)
		{
			if (sNum < 0 || sRet<sNum)
			{
				aGoods.insert(((CGoods*)m_aGoods[s])->GetID());
				sRet++;
			}
			else
			{
				break;
			}
		}
	}

	m_wrGoods.ReleaseReadLock();

	return sRet;
}
//////////////////////////////////////////////////////////////////////////

DWORD CGrid::GetLTGoodsID()
{
	// 按10日涨跌幅排序的GRID才需要取龙头股 [1/29/2013 frantqian]
	if(ZDF10 != m_sSortID && -1 != m_cSort )
		return 0;

	Refresh();

	DWORD dwGoodsID = 0;

	m_wrGoods.AcquireReadLock();

	if(0 == m_aGoods.GetSize() )
	{
		dwGoodsID = 0;
	}
	else
	{
		dwGoodsID = ((CGoods*)m_aGoods[0])->GetID();
	}

	m_wrGoods.ReleaseReadLock();

	return dwGoodsID;
}

void CGrid::GetSortGoodsID( std::vector<DWORD>& vecGoodsID )
{
	for (int i = 0; i < m_aGoods.GetSize(); i++)
	{
		vecGoodsID.push_back(((CGoods*)m_aGoods[i])->GetID());
	}
}

short CGrid::CopyBackWard( CPtrArray& aGoods, short sOffset, short sNum, short& sTotal)
{
	Refresh();

	m_wrGoods.AcquireReadLock();

	sTotal = (short)m_aGoods.GetSize();
	short sRet = 0;
	if (sOffset>=0 && sOffset<sTotal)
		{
		for (short s=sOffset; s<sTotal; s++)
			{
			if (sRet<sNum)
				{
					aGoods.Add(m_aGoods[sTotal-1-s]);
					sRet++;
				}
			else
				{
				break;
				}
			}
		}

	m_wrGoods.ReleaseReadLock();

	return sRet;
}

short CGrid::CopyBackWard( std::set<DWORD>& aGoods, short sOffset, short sNum, short& sTotal)
{
	Refresh();

	m_wrGoods.AcquireReadLock();

	sTotal = (short)m_aGoods.GetSize();
	short sRet = 0;
	if (sOffset>=0 && sOffset<sTotal)
		{
		for (short s=sOffset; s<sTotal; s++)
			{
			if (sNum < 0 || sRet<sNum)
				{
					aGoods.insert(((CGoods*)m_aGoods[sTotal-1-s])->GetID());
					sRet++;
				}
			else
				{
					break;
				}
			}
		}

	m_wrGoods.ReleaseReadLock();

	return sRet;
}

void CGrid::GetSortGoodsIDBackWard( std::vector<DWORD>& vecGoodsID )
{
	for (int i = m_aGoods.GetSize()-1; i >=0; i--)
	{
		vecGoodsID.push_back(((CGoods*)m_aGoods[i])->GetID());
	}
}


bool CGrid::CheckFilter(CGoods* pGoods, CGridFilter *pFilter)
{
	if( pFilter == NULL ){
		return TRUE;
	}
	return pFilter->PassFilter(pGoods);
}

//////////////////////////////////////////////////////////////////////

CGridAll g_grid;

CGridAll::CGridAll()
{
	m_dwRefreshInterval = 5;
}

CGridAll::~CGridAll() 
{ 
	DeleteGrid(); 
};

void CGridAll::DeleteGrid()
{
	m_wrGrid.AcquireWriteLock();

	int nGrid = m_aGrid.GetSize();
	for(int i=0; i<nGrid; i++)
    {
		CGrid* p = (CGrid*)m_aGrid[0];
		m_aGrid.RemoveAt(0);
		delete p;
    }

	m_wrGrid.ReleaseWriteLock();
}

CGrid* CGridAll::GetGrid(char cGroup, CString& strGroup, short sSortID, char cSort, BOOL GZQHFilter)
{
	CGrid* pRet = NULL;

	m_wrGrid.AcquireReadLock();

	int nGrid = m_aGrid.GetSize();
	for(int i=0; i<nGrid; i++)
    {
		CGrid* pGrid = (CGrid*)m_aGrid[i];
		if (pGrid->m_cGroup==cGroup && pGrid->m_sSortID==sSortID && pGrid->m_cSort==cSort && pGrid->m_strGroup==strGroup && pGrid->m_bQZQHFilter == GZQHFilter)
		{
			pRet = pGrid;
			break;
		}
    }

	m_wrGrid.ReleaseReadLock();

	if (pRet==NULL)
	{
		m_wrGrid.AcquireWriteLock();

		pRet = new CGrid(cGroup, strGroup, sSortID, cSort, GZQHFilter);
		if (pRet)
			m_aGrid.Add(pRet);

		m_wrGrid.ReleaseWriteLock();
	}
	
	return pRet;
}


CGridGZQHFilter::CGridGZQHFilter(BOOL enable)
{
	m_bEnableFilter = enable;
}


bool CGridGZQHFilter::PassFilter(CGoods *pGoods)
{
	if( !m_bEnableFilter ){
		return TRUE;
	}

	if( pGoods->IsIC() ){
		return FALSE;
	}

	if( pGoods->IsIH() ){
		return FALSE;
	}

	return TRUE;
}


