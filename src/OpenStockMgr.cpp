#include <errno.h>

#include "OpenStockMgr.h"
#include "MDS.h"
#include "AppLogger.h"
#include "Goods.h"
#include "Grid.h"


OpenStockMgr::OpenStockMgr(void)
{
}

OpenStockMgr::~OpenStockMgr(void)
{
}

bool OpenStockMgr::IsOpen(DWORD dwGoodID)
{
	bool bret = false;
	m_rwGoods.AcquireReadLock();
	bret = (m_setGoods.find(dwGoodID) != m_setGoods.end());
	m_rwGoods.ReleaseReadLock();
	return bret;
}

void OpenStockMgr::Run()
{
	LogApp("%s start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	//ts.tv_sec += m_delaySec;
	//ts.tv_nsec += m_delayNSec;
	
	/////////////////////////////////////////////////
	//  [9/16/2014 %zhaolin%]
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);
	////////////////////////////////////////////

	while(sem_timedwait(&m_semExit, &ts)!=0)
	{
		if(errno != ETIMEDOUT && errno != EINTR)
			break;

		if (m_dwTime != theApp.m_dwTime)
		{
			char cGroup = 1;
			CString strGroup;
			short sSortID = ZDF;
			char cSort = -1;
			short sOffset = 0;
			short sNum = 10;
			short sTotal;

			std::set<DWORD> setGoods;

			//沪A
			CGrid* pGridSH = g_grid.GetGrid(cGroup, strGroup, sSortID, -1*abs(cSort), FALSE);
			if (pGridSH)
			{
				//pGridSH->Copy(setGoods, sOffset, sNum, sTotal);

				if (cSort < 0)
					{
						pGridSH->Copy(setGoods, sOffset, sNum, sTotal);
					}
				else
					{
						pGridSH->CopyBackWard(setGoods, sOffset, sNum, sTotal);
					}
			}

			//深A
			cGroup = 3;
			CGrid* pGridSZ = g_grid.GetGrid(cGroup, strGroup, sSortID, -1*abs(cSort), FALSE);
			if (pGridSZ)
			{
				//pGridSZ->Copy(setGoods, sOffset, sNum, sTotal);
				if (cSort < 0)
					{
						pGridSZ->Copy(setGoods, sOffset, sNum, sTotal);
					}
				else
					{
						pGridSZ->CopyBackWard(setGoods, sOffset, sNum, sTotal);
					}
			}

			//中小
			cGroup = 9;
			CGrid* pGridZX = g_grid.GetGrid(cGroup, strGroup, sSortID, -1*abs(cSort), FALSE);
			if (pGridZX)
			{
				//pGridZX->Copy(setGoods, sOffset, sNum, sTotal);
				if (cSort < 0)
				{
					pGridZX->Copy(setGoods, sOffset, sNum, sTotal);
				}
				else
				{
					pGridZX->CopyBackWard(setGoods, sOffset, sNum, sTotal);
				}
			}

			//创业
			strGroup = "创业板";
			cGroup = -14;
			CGrid* pGridCY = g_grid.GetGrid(cGroup, strGroup, sSortID, -1*abs(cSort), FALSE);
			if (pGridCY)
			{
				//pGridCY->Copy(setGoods, sOffset, sNum, sTotal);
				if (cSort < 0)
				{
					pGridCY->Copy(setGoods, sOffset, sNum, sTotal);
				} 
				else
				{
					pGridCY->CopyBackWard(setGoods, sOffset, sNum, sTotal);
				}
			}

			//牛股堂
			strGroup = "牛股堂涨";
			cGroup = -17;
			sNum = -1;
			CGrid* pGridNZ = g_grid.GetGrid(cGroup, strGroup, sSortID, -1*abs(cSort), FALSE);
			if (pGridNZ)
			{
				//pGridNZ->Copy(setGoods, sOffset, sNum, sTotal);
				if (cSort < 0)
				{
					pGridNZ->Copy(setGoods, sOffset, sNum, sTotal);
				} 
				else
				{
					pGridNZ->CopyBackWard(setGoods, sOffset, sNum, sTotal);
				}
			}

			strGroup = "牛股堂跌";
			CGrid* pGridND = g_grid.GetGrid(cGroup, strGroup, sSortID, -1*abs(cSort), FALSE);
			if (pGridND)
			{
				//pGridND->Copy(setGoods, sOffset, sNum, sTotal);
				if (cSort < 0)
				{
					pGridND->Copy(setGoods, sOffset, sNum, sTotal);
				} 
				else
				{
					pGridND->CopyBackWard(setGoods, sOffset, sNum, sTotal);
				}
			}

			m_rwGoods.AcquireWriteLock();
			m_setGoods.swap(setGoods);
			m_rwGoods.ReleaseWriteLock();

			m_dwTime = theApp.m_dwTime;
		}

		ts.tv_sec += m_delaySec;
		ts.tv_nsec += m_delayNSec;
	}

	LogApp("%s end", Name());
}


