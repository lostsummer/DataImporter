//#include "StdAfx.h"
#include "FPlatData.h"
#include "SysGlobal.h"
#include "AppLogger.h"

CFPlatData::CFPlatData(void)
{
	m_cType = FPD_Type_Stock;
	m_lGoodsID = 0;
	m_dwUpdateDate = 0;
	m_dwUpdateTime = 0;
	for(int k = 0; k < CS_COL_TOTAL; ++k)
		m_csValue[k] = "";
	memset(m_pxValue, 0, PX_COL_TOTAL*4);
}


CFPlatData::~CFPlatData(void)
{
}

BOOL operator > (CFPlatData& first, CFPlatData& second)
{
	if (first.m_cType == second.m_cType)
		return first.m_lGoodsID > second.m_lGoodsID;
	else
		return first.m_cType > second.m_cType;
}
BOOL operator == (CFPlatData& first, CFPlatData& second)
{
	if (first.m_cType == second.m_cType)
		return first.m_lGoodsID == second.m_lGoodsID;
	else
		return FALSE;
}
BOOL operator < (CFPlatData& first, CFPlatData& second)
{
	if (first.m_cType == second.m_cType)
		return first.m_lGoodsID < second.m_lGoodsID;
	else
		return first.m_cType < second.m_cType;
}

// cs_max和px_max的值是从FPlat.dat的文件头中提取的
BOOL CFPlatData::Read(CBuffer& buffer, long cs_max, long px_max)
{	
	if(cs_max < CS_COL_TOTAL || px_max < PX_COL_TOTAL)
		return FALSE;	//不能读老格式的数据，但可以读新格式的数据
	buffer.Read(&m_cType, 1);
	buffer.Read(&m_lGoodsID, 4);
	buffer.Read(&m_dwUpdateDate, 4);
	buffer.Read(&m_dwUpdateTime, 4);
	long cs = 0;
	for( ; cs < CS_COL_TOTAL; ++cs )
	{
		buffer.ReadString(m_csValue[cs]);
	}
	for( ; cs < cs_max; ++cs)
	{// 略过多余的字段 CString  保证老版本可以读新格式数据
		CString tt;
		buffer.ReadString(tt);
	}
	buffer.Read(m_pxValue, PX_COL_TOTAL*4);
	for(long px = PX_COL_TOTAL; px < px_max; ++px)
	{// 略过多余的字段 XInt32   保证老版本可以读新格式数据
		long tt;
		buffer.Read(&tt, 4);
	}
	return TRUE;
}

void CFPlatData::Write(CBuffer& buffer)
{
	buffer.Write(&m_cType, 1);
	buffer.Write(&m_lGoodsID, 4);
	buffer.Write(&m_dwUpdateDate, 4);
	buffer.Write(&m_dwUpdateTime, 4);
	for(long cs = 0; cs < CS_COL_TOTAL; ++cs )
		buffer.WriteString(m_csValue[cs]);
	buffer.Write(m_pxValue, PX_COL_TOTAL*4);
}

const CFPlatData& CFPlatData::operator=(const CFPlatData& src)
{
	m_cType = src.m_cType;
	m_lGoodsID = src.m_lGoodsID;
	m_dwUpdateDate = src.m_dwUpdateDate;
	m_dwUpdateTime = src.m_dwUpdateTime;
	for(long cs = 0; cs < CS_COL_TOTAL; ++cs )
		m_csValue[cs] = src.m_csValue[cs];
	memcpy(m_pxValue, src.m_pxValue, PX_COL_TOTAL*4);
	return *this;
}

