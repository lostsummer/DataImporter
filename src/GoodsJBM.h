#ifndef GOODSJBM_H
#define GOODSJBM_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "XInt32.h"
#include "Buffer.h"
#include "SortArray.h"
#include "RWLock.h"
#include "FPlatDataStock.h"

#define	JBM_TOTAL	38

#define J_NAME		-1	// 名称
#define J_CODE		-2	// 代码
#define J_DATE		-3	// 日期

#define J_ZGB		0	// 总股本	1
#define J_GJG		1	// 国家股	2
#define J_FRG		2	// 法人股	3
#define J_AG		3	// A 股		4
#define J_BG		4	// B 股		5
#define J_HG		5	// H 股		6
#define J_ZGG		6	// 职工股	7

#define J_ZZC		7	// 总资产		8
#define J_LDZC		8	// 流动资产		9
#define J_GDZC		9	// 固定资产		10
#define J_WXZC		10	// 无形资产		11
#define J_CQTZ		11	// 长期投资		12
#define J_LDFZ		12	// 流动负债		13
#define J_CQFZ		13	// 长期负债		14
#define J_ZBGJJ		14	// 资本公积金	15
#define J_MGGJJ		15	// 每股公积金	16
#define J_GDQY		16	// 股东权益		17

#define J_ZYSR		17	// 主营收入		18
#define J_ZYLR		18	// 主营利润		19
#define J_QTLR		19	// 其他利润		20
#define J_YYLR		20	// 营业利润		21
#define J_TZSY		21	// 投资收益		22
#define J_BTSR		22	// 补贴收入		23
#define J_YYWSZ		23	// 营业外收支	24
#define J_NDSY		24	// 以前年度损益调整	25
#define J_LRZE		25	// 利润总额		26
#define J_JLR		26	// 净利润		27
#define J_WFPLR		27	// 未分配利润	28
#define J_MGWFPLR	28	// 每股未分配利润	29

#define J_MGSY		29	// 每股收益		30
#define J_MGJZC		30	// 每股净资产	31
#define J_TZMGJZC	31	// 调整后的每股净资产	32
#define J_GDQYB		32	// 股东权益比	33
#define J_JZCSYL	33	// 净资产收益率	34

#define J_AG_XS		34	// 限售A 股	35
#define J_BG_XS		35	// 限售B 股	36
#define J_GDRS		36	// 股东户数	37
#define J_RJCG		37	// 户均持股	38

class CGoodsJBM  
{
public:
	CGoodsJBM();
	void Clear();

	DWORD m_dwGoodsID;
	DWORD m_dwDate;

	XInt32 m_pxJBM[JBM_TOTAL];

	DWORD m_dwUpdateDate;	//更新日期 yyyymmdd
	DWORD m_dwUpdateTime;	//更新时间 hhmmss

	DWORD m_dwDateFHSP;
	CString m_strFHSP;
	CFPlatDataStock m_fpd;
public:
	const CGoodsJBM& operator=(const CGoodsJBM&);

	void Read(CBuffer&, short);
	void ReadFHSP(CBuffer& buffer, short);
	void Write(CBuffer&, short);
    double GetFValue(short sID);

	XInt32 GetValue(short);
	void SetValue(XInt32 xValue, short sID);
};

BOOL operator > (CGoodsJBM&, CGoodsJBM&);
BOOL operator == (CGoodsJBM&, CGoodsJBM&);
BOOL operator < (CGoodsJBM&, CGoodsJBM&);

//////////////////////////////////////////////////////////////////////

class CJBMAll
{
public:
	CJBMAll();
	~CJBMAll();

	bool Read();
	bool ReadFHSP();

	BOOL Get(DWORD dwGoodsID, CGoodsJBM&);
	void Get(DWORD, DWORD, CSortArray<CGoodsJBM>&);

public:
	RWLock m_wrJBM;

	CSortArray<CGoodsJBM> m_aJBM;

	DWORD m_dwDate;
	DWORD m_dwTime;

	DWORD m_dwNewDate;
	DWORD m_dwNewTime;
	DWORD m_dwNumCols;
	DWORD m_dwStrCols;
};

//////////////////////////////////////////////////////////////////////

class CGoodsDPTJ
{
public:
	CGoodsDPTJ();
	void Clear();

	DWORD m_dwGoodsID;

	INT64 m_hCGSZ;
	INT64 m_hCGSL;
	short m_sJJJS;
	short m_sBL;

	INT64 m_hCGSZ0;
	INT64 m_hCGSL0;
	short m_sJJJS0;
	short m_sBL0;

public:
	void Read(CBuffer&, short);
};

BOOL operator > (CGoodsDPTJ&, CGoodsDPTJ&);
BOOL operator == (CGoodsDPTJ&, CGoodsDPTJ&);
BOOL operator < (CGoodsDPTJ&, CGoodsDPTJ&);

//////////////////////////////////////////////////////////////////////

class CDPTJAll
{
public:
	CDPTJAll();
	~CDPTJAll();

	bool Read();

	BOOL Get(DWORD dwGoodsID, CGoodsDPTJ&);

public:
	RWLock m_wrDPTJ;
	CSortArray<CGoodsDPTJ> m_aDPTJ;
};

#endif

