/** *************************************************************************
* MDS
* Copyright 2011 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file ThreadMgr.cpp
* @brief  
*
* $Author: panlishen$
* $Date: Thu Jul 28 10:13:45 UTC+0800 2011$
* $Revision$
*/
/* *********************************************************************** */
#include "ThreadMgr.h"
#include "AppLogger.h"
#include "MyThread.h"

ThreadMgr::ThreadMgr()
{

}

ThreadMgr::~ThreadMgr()
{

}

void ThreadMgr::AddThread(CMyThread* entry)
{
	m_rwLock.AcquireWriteLock();
	m_lts.push_back(entry);
	m_rwLock.ReleaseWriteLock();
}

void ThreadMgr::RemoveThread(CMyThread* entry)
{
	m_rwLock.AcquireWriteLock();
	for (Container::iterator it = m_lts.begin(); it != m_lts.end(); ++it)
	{
		if (*it == entry)
		{
			m_lts.erase(it);
			break;
		}
	}
	m_rwLock.ReleaseWriteLock();
}

void ThreadMgr::Status()
{
	int nIndex = 0;
	LogInfo("**begin********************Thread status******************************");
	m_rwLock.AcquireReadLock();
	for (Container::const_iterator cit = m_lts.begin(); cit != m_lts.end(); ++cit)
	{
		CMyThread* pThread = *cit;
		LogInfo("[%d]%ld %s %s %s", ++nIndex, pThread->getMyThreadId(), pThread->Name(), 
			pThread->isAlive()?"alive":"die", pThread->isJoinable()?"join able":"not join able");
	}
	m_rwLock.ReleaseReadLock();
	LogInfo("**end**********************Thread status******************************");
}











