#ifndef MYTIMER_H
#define MYTIMER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

/************************************
  REVISION LOG ENTRY
    Revision By: 刘成柱
	  Revised on 2009-12-4 16:20:53
	    Comments: 跨平台的日期时间类
		 ************************************/

#ifdef WIN32
typedef long long __time64_t;
#else
typedef long __time64_t;
#endif


typedef long long LONGLONG;
typedef long LONG;


class CMyTimeSpan
{
public:
	CMyTimeSpan() ;
	CMyTimeSpan( __time64_t time ) ;
	CMyTimeSpan( int lDays, int nHours, int nMins, int nSecs ) ;

	LONGLONG GetDays() ;
	LONGLONG GetTotalHours() ;
	int GetHours() ;
	LONGLONG GetTotalMinutes() ;
	int GetMinutes() ;
	LONGLONG GetTotalSeconds() ;
	int GetSeconds() ;

	__time64_t GetTimeSpan() ;

	CMyTimeSpan operator+( CMyTimeSpan span ) ;
	CMyTimeSpan operator-( CMyTimeSpan span ) ;
	CMyTimeSpan& operator+=( CMyTimeSpan span ) ;
	CMyTimeSpan& operator-=( CMyTimeSpan span ) ;
	bool operator==( CMyTimeSpan span ) ;
	bool operator!=( CMyTimeSpan span ) ;
	bool operator<( CMyTimeSpan span ) ;
	bool operator>( CMyTimeSpan span ) ;
	bool operator<=( CMyTimeSpan span ) ;
	bool operator>=( CMyTimeSpan span ) ;


private:
	__time64_t m_timeSpan;
};

class CMyTime
{
public:
	static CMyTime GetCurrentTime() ;

	CMyTime() ;
	CMyTime( __time64_t time ) ;
	CMyTime( int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec,
		int nDST = -1 );

	CMyTime& operator=( __time64_t time ) ;

	CMyTime& operator+=( CMyTimeSpan span ) ;
	CMyTime& operator-=( CMyTimeSpan span ) ;

	CMyTimeSpan operator-( CMyTime time ) ;
	CMyTime operator-( CMyTimeSpan span ) ;
	CMyTime operator+( CMyTimeSpan span ) ;

	bool operator==( CMyTime time ) ;
	bool operator!=( CMyTime time ) ;
	bool operator<( CMyTime time ) ;
	bool operator>( CMyTime time ) ;
	bool operator<=( CMyTime time ) ;
	bool operator>=( CMyTime time ) ;

	struct tm* GetLocalTm( struct tm* ptm = 0 );

	__time64_t GetTime() ;

	int GetYear() ;
	int GetMonth() ;
	int GetDay() ;
	int GetHour() ;
	int GetMinute() ;
	int GetSecond() ;
	int GetDayOfWeek() ;

private:
	__time64_t m_time;
#ifndef WIN32
    struct tm m_tms;
#endif
};

typedef CMyTime CTime ;

#endif

