#pragma once
#include "Noncopyable.h"

class CLockFile : private  Noncopyable
{
public:
	CLockFile();
	~CLockFile();

	bool AlreadyExist();
};
