#ifndef HISGB_ALL_H
#define HISGB_ALL_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "XInt32.h"
#include "Buffer.h"
#include "SortArray.h"
#include "RWLock.h"

class CHisGB
{
	public:
		DWORD m_dwDate;
		XInt32 m_xLTG;
};

BOOL operator > (CHisGB&, CHisGB&);
BOOL operator == (CHisGB&, CHisGB&);
BOOL operator < (CHisGB&, CHisGB&);

typedef CSortArray<CHisGB> CSortArrayHisGB;


class CGoodsHisGB
{

	public:
		CGoodsHisGB();
		CGoodsHisGB(const CGoodsHisGB& ref);
		~CGoodsHisGB();
		CGoodsHisGB& operator=(const CGoodsHisGB& another);		
		DWORD m_dwGoodsID;
		CSortArrayHisGB m_aHisGB;
};

BOOL operator > (CGoodsHisGB&, CGoodsHisGB&);
BOOL operator == (CGoodsHisGB&, CGoodsHisGB&);
BOOL operator < (CGoodsHisGB&, CGoodsHisGB&);



typedef CSortArray<CGoodsHisGB> CSortArrayGoodsHisGB;


class CHisGBAll
{
	public:
		CHisGBAll();
		~CHisGBAll();
		void Read();
		void Get( DWORD dwGoodsID, CGoodsHisGB& goodsHisGB);
	private:
		DWORD m_dwDate;
		DWORD m_dwTime;
		CSortArrayGoodsHisGB m_aGoodsHisGB;
};



#endif

