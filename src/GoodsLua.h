#ifndef GOODS_LUA_H_
#define GOODS_LUA_H_

#include "Goods.h"
#include "lua_tinker.h"

class CRedisImporter
{
public:
    CRedisImporter();
    ~CRedisImporter();
    bool WriteNameCode(CGoods * pGoods);
    void WriteKline(CGoods * pGoods, CFDayMobile &min, const char *pcDataType);
    bool ExecutePipeline();
    bool WriteQuote(CGoods * pGoods);
    bool WriteSpec();
    bool WriteCodeInfo();
    bool ClearKeysInDB(const char *, int);
    bool ClearCQ();
    bool ClearKM();
    bool ClearKL();
protected:
    lua_State * m_pLuaState;
public:
    CString m_cScriptPath;
    CString m_cScriptName;
};

#endif
