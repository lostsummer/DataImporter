#ifndef SOCKETS_H
#define SOCKETS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "MySocket.h"

//////////////////////////////////////////////////////////////////////

class CL2DMSocket : public CMySocket  
{
public:
	CL2DMSocket(){m_cCheckSumDW = 0;};
	virtual ~CL2DMSocket(){};

	virtual void AfterConnect();
	virtual void ReadPacket(CDataHead& head, CBuffer& bufTemp);

public:
	char m_cCheckSumDW;
	//Steven New Begin
	short m_sVersion;
	//Steven New End
};


//////////////////////////////////////////////////////////////////////
struct CCheckItem
{
	char m_cIndex;
	int m_nMinVal;
	//int m_nMaxVal;//鏈敤
};

class CMSSocket : public CMySocket
{
public:
	CMSSocket();
	virtual ~CMSSocket();
	virtual void AfterConnect();
	virtual void ReadPacket(CDataHead& head, CBuffer& bufTemp);

public:	//20120220 澧炲姞鑷娴嬪姛鑳
	BOOL m_bCheckItem; 
	CSortArray<CCheckItem> m_aCheckItem;

	void ChangeCheckItem(CString &strItem);
	void InitCheckItem();
	void DoCheckItem();

public:
	DWORD m_dwLastRecvPack;
	DWORD m_dwLastRecvPackTime;
	int m_nChannel;
	int m_nLogOut;
};

//////////////////////////////////////////////////////////////////////////
#endif


