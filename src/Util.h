/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file Util.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef __UTIL_H__
#define __UTIL_H__


#include <pthread.h>
//////////////////////////////////////////////////////////////////////////

template<typename T>
class YSingleton
{
public:
	static T* Instance()
	{
		pthread_once(&ponce_, &YSingleton::MakeInstance);

		return m_Singleton;
	}

	static void MakeInstance()
	{
		if (!m_Singleton) 
		m_Singleton = new T();
	}

	static T& GetMe()
	{
		return *Instance();
	}

	static void DelMe()
	{
		if (m_Singleton) 
		{
			delete m_Singleton; 
			m_Singleton = 0;
		}
	}

protected:
	YSingleton(void)
	{
	}

	~YSingleton(void)
	{
	}

	static pthread_once_t ponce_;
	static T* m_Singleton;

private:
	YSingleton(const YSingleton& rth);
	const YSingleton& operator=(const YSingleton& rth);
};

template<typename T>
pthread_once_t YSingleton<T>::ponce_ = PTHREAD_ONCE_INIT;
template<typename T>
T* YSingleton<T>::m_Singleton = 0;
//////////////////////////////////////////////////////////////////////////

template <class T>
void DeleteAndNull(T*& p)
{
	T* tmp = p;
	p = 0;
	delete tmp;
}

#define SAFE_DELETE(x)\
do\
{\
	try{\
		if(x != NULL)\
		{\
			delete x;\
			x = NULL;\
		}\
	}\
	catch(...)\
	{\
	}\
}while(0)


#endif

