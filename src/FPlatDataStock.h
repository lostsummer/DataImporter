//#pragma  once
#ifndef _F_PLAT_DATA_STOCK_H__
#define _F_PLAT_DATA_STOCK_H__
//#include <atlstr.h>
#include "FPlatData.h"
#include "SortArray.h"
#include "SysGlobal.h"

enum EColorType
{
	ECT_DEFAULT,
	ECT_ZDF,
	ECT_VOL,
	ECT_AMT,
	ECT_1,
	ECT_RED,
};

//2013-6-6 Wang.YJ 所有ID在升级时，都不得变动
#define JN_BEGIN		1150
#define JN_BLANK  (JN_BEGIN+0)

#define JN_DATE  (JN_BEGIN + 1)    //报告日期  V_YYYYMMDD
#define JN_ZGB  (JN_BEGIN + 2)    //总股本  V_BIGINT
#define JN_GJG  (JN_BEGIN + 3)    //国家股  
#define JN_FRG  (JN_BEGIN + 4)    //法人股  
#define JN_AG  (JN_BEGIN + 5)    //A 股  V_BIGINT
#define JN_BG  (JN_BEGIN + 6)    //B 股  V_BIGINT
#define JN_HG  (JN_BEGIN + 7)    //H 股  V_BIGINT
#define JN_ZGG  (JN_BEGIN + 8)    //职工股  
#define JN_ZZC  (JN_BEGIN + 9)    //总资产  V_BIGINT
#define JN_LDZC  (JN_BEGIN + 10)    //流动资产  V_BIGINT
#define JN_GDZC  (JN_BEGIN + 11)    //固定资产  V_BIGINT
#define JN_WXZC  (JN_BEGIN + 12)    //无形资产  
#define JN_CQTZ  (JN_BEGIN + 13)    //长期投资  
#define JN_LDFZ  (JN_BEGIN + 14)    //流动负债  V_BIGINT
#define JN_CQFZ  (JN_BEGIN + 15)    //长期负债  V_BIGINT
#define JN_ZBGJJ  (JN_BEGIN + 16)    //资本公积金  
#define JN_MGGJJ  (JN_BEGIN + 17)    //每股公积金  V_YUAN_10K
#define JN_GDQY  (JN_BEGIN + 18)    //股东权益  V_BIGINT
#define JN_ZYSR  (JN_BEGIN + 19)    //主营收入  V_BIGINT
#define JN_ZYLR  (JN_BEGIN + 20)    //主营利润  V_BIGINT
#define JN_QTLR  (JN_BEGIN + 21)    //其他利润  
#define JN_YYLR  (JN_BEGIN + 22)    //营业利润  
#define JN_TZSY  (JN_BEGIN + 23)    //投资收益  
#define JN_BTSR  (JN_BEGIN + 24)    //补贴收入  
#define JN_YYWSZ  (JN_BEGIN + 25)    //营业外收支  
#define JN_NDSY  (JN_BEGIN + 26)    //以前年度损益调整  
#define JN_LRZE  (JN_BEGIN + 27)    //利润总额  V_BIGINT
#define JN_JLR  (JN_BEGIN + 28)    //净利润  V_BIGINT
#define JN_WFPLR  (JN_BEGIN + 29)    //未分配利润  
#define JN_MGWFPLR  (JN_BEGIN + 30)    //每股未分配利润  V_YUAN_10K
#define JN_MGSY  (JN_BEGIN + 31)    //每股收益  V_YUAN_10K
#define JN_MGJZC  (JN_BEGIN + 32)    //每股净资产  V_YUAN_10K
#define JN_TZMGJZC  (JN_BEGIN + 33)    //调整后的每股净资产  
#define JN_GDQYB  (JN_BEGIN + 34)    //股东权益比  V_10KK
#define JN_JZCSYL  (JN_BEGIN + 35)    //净资产收益率  V_10KK
#define JN_AG_XS  (JN_BEGIN + 36)    //限售A 股  
#define JN_BG_XS  (JN_BEGIN + 37)    //限售B 股  
#define JN_GDRS  (JN_BEGIN + 38)    //股东户数  
#define JN_S_HJCG  (JN_BEGIN + 39)    //户均持有效流通股  V_GU_10K
#define JN_S_SecuType  (JN_BEGIN + 40)    //证券类型  V_STOCK
#define JN_S_IsFinacial  (JN_BEGIN + 41)    //是否金融类  V_IsFinacial
#define JN_S_TradeDate  (JN_BEGIN + 42)    //交易日期  V_YYYYMMDD
#define JN_S_SYL  (JN_BEGIN + 43)    //市盈率  V_10KK
#define JN_S_SJL  (JN_BEGIN + 44)    //市净率  V_10KK
#define JN_S_MGJYXJL  (JN_BEGIN + 45)    //每股经营现金流  V_YUAN_10K
#define JN_S_LTGB  (JN_BEGIN + 46)    //流通股本  V_BIGINT
#define JN_S_YXLTGB  (JN_BEGIN + 47)    //有效流通股本  V_BIGINT
#define JN_S_HJCGTB  (JN_BEGIN + 48)    //户均持股环比  V_ORGI
#define JN_S_GDHSB  (JN_BEGIN + 49)    //股东户数环比  V_ORGI
#define JN_S_ZYSRTB  (JN_BEGIN + 50)    //主营收入同比  V_10KK
#define JN_S_ZYLRTB  (JN_BEGIN + 51)    //主营利润同比  V_10KK
#define JN_S_JLRTB  (JN_BEGIN + 52)    //净利润同比  V_10KK
#define JN_S_XSMLL  (JN_BEGIN + 53)    //销售毛利率  V_10KK
#define JN_S_JLXSYL  (JN_BEGIN + 54)    //净利息收益率  V_10KK
#define JN_S_ZCFZL  (JN_BEGIN + 55)    //资产负债率  V_10KK
#define JN_S_GuXiL  (JN_BEGIN + 56)    //股息率  V_10KK
#define JN_S_JGCGJS  (JN_BEGIN + 57)    //机构持股家数  V_GE
#define JN_S_JJCGJS  (JN_BEGIN + 58)    //基金持股家数  V_GE
#define JN_S_JGCGZB  (JN_BEGIN + 59)    //机构持股占有效流通比  V_10KK
#define JN_S_JJN_ZYXLTB  (JN_BEGIN + 60)    //基金持股占有效流通比  V_10KK
#define JN_S_QF_ZYXLTB  (JN_BEGIN + 61)    //QFII持股占有效流通比  V_10KK
#define JN_S_BX_ZYXLTB  (JN_BEGIN + 62)    //保险持股占有效流通比  V_10KK
#define JN_S_QS_ZYXLTB  (JN_BEGIN + 63)    //券商持股占有效流通比  V_10KK
#define JN_S_XT_ZYXLTB  (JN_BEGIN + 64)    //信托持股占有效流通比  V_10KK
#define JN_S_JYHDXJL  (JN_BEGIN + 65)    //经营活动现金流  V_BIGINT
#define JN_S_YFZK  (JN_BEGIN + 66)    //预付账款  V_BIGINT
#define JN_S_YSZK  (JN_BEGIN + 67)    //应收账款  V_BIGINT
#define JN_S_XJYHKX  (JN_BEGIN + 68)    //现金及存放中央银行款项  V_BIGINT
#define JN_S_FFDKDK  (JN_BEGIN + 69)    //发放贷款及垫款  V_BIGINT
#define JN_S_TYCFKX  (JN_BEGIN + 70)    //同业及其他金融机构存放款项  V_BIGINT
#define JN_S_XSKX  (JN_BEGIN + 71)    //吸收存款  V_BIGINT
#define JN_S_SSRQ  (JN_BEGIN + 72)    //上市日期  V_YYYYMMDD
#define JN_CS_Abbr  (JN_BEGIN + 73)    //股票简称  V_ORGI
#define JN_CS_ZXFH  (JN_BEGIN + 74)    //最新分红  V_ORGI
#define JN_CS_S_Bkname  (JN_BEGIN + 75)    //所属行业  V_ORGI
#define JN_CS_S_Concept  (JN_BEGIN + 76)    //所属概念  V_ORGI
#define JN_H_ZFZ  (JN_BEGIN + 77)    //总负债  V_BIGINT
#define JN_F_CLRQ  (JN_BEGIN + 78)    //成立日期  V_YYYYMMDD
#define JN_F_FundType  (JN_BEGIN + 79)    //基金类型  V_FUNDTYPE
#define JN_F_TZFG  (JN_BEGIN + 80)    //投资风格  V_TZFG
#define JN_F_DWJZ  (JN_BEGIN + 81)    //单位净值  V_YUAN_10K
#define JN_F_LJJZ  (JN_BEGIN + 82)    //累计净值  V_YUAN_10K
#define JN_F_DWJZ_ZZL  (JN_BEGIN + 83)    //单位净值增长率  V_10KK
#define JN_F_LJJZ_ZZL  (JN_BEGIN + 84)    //累计净值增长率  V_10KK
#define JN_F_JJFE  (JN_BEGIN + 85)    //基金份额  V_BIGINT
#define JN_F_WFDWSY  (JN_BEGIN + 86)    //每万份单位收益  V_YUAN_10K
#define JN_F_7NHSYL  (JN_BEGIN + 87)    //7日年化收益率(%)  V_10KK
#define JN_F_WFSYZZL  (JN_BEGIN + 88)    //万份收益增长率(%)  V_10KK
#define JN_F_7NHSYZZL  (JN_BEGIN + 89)    //7日年化收益增长率(%)  V_10KK
#define JN_F_QY_TZZB  (JN_BEGIN + 90)    //权益类投资占比  V_10KK
#define JN_F_GP_TZZB  (JN_BEGIN + 91)    //股票投资占比  V_10KK
#define JN_F_CT_TZZB  (JN_BEGIN + 92)    //存托凭证占比  V_10KK
#define JN_F_JJN_TZZB  (JN_BEGIN + 93)    //基金投资占比  V_10KK
#define JN_F_GD_TZZB  (JN_BEGIN + 94)    //固定收益类投资占比  V_10KK
#define JN_F_ZQ_TZZB  (JN_BEGIN + 95)    //债券投资占比  V_10KK
#define JN_F_ZC_TZZB  (JN_BEGIN + 96)    //资产支持证券占比  V_10KK
#define JN_F_JR_TZZB  (JN_BEGIN + 97)    //金融衍生品投资占比  V_10KK
#define JN_F_FS_TZZB  (JN_BEGIN + 98)    //买入返售金融资产占比  V_10KK
#define JN_F_CK_TZZB  (JN_BEGIN + 99)    //银行存款和结算备付金占比  V_10KK
#define JN_F_HB_TZZB  (JN_BEGIN + 100)    //货币市场工具占比  V_10KK
#define JN_F_QT_TZZB  (JN_BEGIN + 101)    //其他资产占比  V_10KK
#define JN_F_CBBGRQ  (JN_BEGIN + 102)    //财报报告日期  V_YYYYMMDD
#define JN_F_ZLR_TB  (JN_BEGIN + 103)    //总利润同比  V_10KK
#define JN_F_ZZC_TB  (JN_BEGIN + 104)    //总资产同比  V_10KK
#define JN_F_ZFZ_TB  (JN_BEGIN + 105)    //总负债同比  V_10KK
#define JN_F_CXQSR  (JN_BEGIN + 106)    //存续起始日  V_YYYYMMDD
#define JN_F_CXZZQ  (JN_BEGIN + 107)    //存续终止日  V_YYYYMMDD
#define JN_F_JDQR  (JN_BEGIN + 108)    //据到期日  V_TIAN
#define JN_F_CXQX  (JN_BEGIN + 109)    //存续期限  V_DATABASE
#define JN_F_TGFL  (JN_BEGIN + 110)    //托管费率(%)  V_10KK
#define JN_F_GLFL  (JN_BEGIN + 111)    //管理费率(%)  V_10KK
#define JN_F_JJSC  (JN_BEGIN + 112)    //基金市场  V_ORGI
#define JN_CS_Name  (JN_BEGIN + 113)    //基金名称  V_ORGI
#define JN_CS_F_JLR  (JN_BEGIN + 114)    //基金经理  V_ORGI
#define JN_CS_F_GLR  (JN_BEGIN + 115)    //基金管理人  V_ORGI
#define JN_CS_F_TGR  (JN_BEGIN + 116)    //基金托管人  V_ORGI
#define JN_CS_F_YJBJJZ  (JN_BEGIN + 117)    //业绩比较基准  V_ORGI
#define JN_B_ZQQX  (JN_BEGIN + 118)    //债券期限  V_DATABASE
#define JN_B_FXPL  (JN_BEGIN + 119)    //付息频率  V_DATABASE
#define JN_B_GZ_FXL  (JN_BEGIN + 120)    //发行量  V_BIGINT
#define JN_B_GZ_FXJG  (JN_BEGIN + 121)    //发行价格  V_YUAN_10K
#define JN_B_GZ_DWMZ  (JN_BEGIN + 122)    //单位面值  V_YUAN_10K
#define JN_B_ZGJG  (JN_BEGIN + 123)    //转股价格  V_YUAN_10K
#define JN_B_PMLL  (JN_BEGIN + 124)    //票面利率(%)  V_10KK
#define JN_B_FXRQ  (JN_BEGIN + 125)    //发行日期  V_YYYYMMDD
#define JN_B_QXR  (JN_BEGIN + 126)    //起息日  V_YYYYMMDD
#define JN_B_DQR  (JN_BEGIN + 127)    //到期日  V_YYYYMMDD
#define JN_B_DFR  (JN_BEGIN + 128)    //兑付日  V_YYYYMMDD
#define JN_B_ZGJZR  (JN_BEGIN + 129)    //转股截止日  V_YYYYMMDD
#define JN_B_ZQSYJYR  (JN_BEGIN + 130)    //债券剩余交易日  V_TIAN
#define JN_CS_B_ZLMC  (JN_BEGIN + 131)    //债券种类名称  V_ORGI
#define JN_CS_B_LLLX  (JN_BEGIN + 132)    //利率类型  V_ORGI
#define JN_CS_B_JXFS  (JN_BEGIN + 133)    //计利方式  V_ORGI
#define JN_CS_B_FXFS  (JN_BEGIN + 134)    //付息方式  V_ORGI
#define JN_CS_B_FXR  (JN_BEGIN + 135)    //发行人  V_ORGI
#define JN_CS_B_NDJXJZ  (JN_BEGIN + 136)    //年度计息基准  V_ORGI
#define JN_S_52Z_ZF  (JN_BEGIN + 137)    //52周涨幅  V_ORGI
#define JN_S_FH_BGQ  (JN_BEGIN + 138)    //分红报告期  V_YYYYMMDD
#define JN_S_PXBL_HKD  (JN_BEGIN + 139)    //派息比例(港币)  V_ORGI
#define JN_S_PXBL_USD  (JN_BEGIN + 140)    //派息比例(美元)  V_ORGI
#define JN_S_PXBL_RMB  (JN_BEGIN + 141)    //派息比例(人民币)  V_ORGI
#define JN_S_ZZ_BL  (JN_BEGIN + 142)    //转增比例  V_ORGI
#define JN_S_SG_BL  (JN_BEGIN + 143)    //送股比例  V_ORGI
#define JN_S_FAJD_BM  (JN_BEGIN + 144)    //方案进度编码        V_ORGI
#define JN_S_YJYG_BGQ  (JN_BEGIN + 145)    //业绩预告报告期  V_YYYYMMDD
#define JN_S_CW_BGQ  (JN_BEGIN + 146)    //财务报告期  V_YYYYMMDD
#define JN_S_YYLR  (JN_BEGIN + 147)    //营业利润  V_BIGINT
#define JN_S_QTLR  (JN_BEGIN + 148)    //其他利润  V_BIGINT
#define JN_S_YYWSZ  (JN_BEGIN + 149)    //营业外收支  V_BIGINT
#define JN_S_BTSR  (JN_BEGIN + 150)    //补贴收入  V_BIGINT
#define JN_S_TZSY  (JN_BEGIN + 151)    //投资收益  V_BIGINT
#define JN_S_SYTZ  (JN_BEGIN + 152)    //损益调整  V_BIGINT
#define JN_S_LRZE  (JN_BEGIN + 153)    //利润总额  V_BIGINT
#define JN_S_JLR  (JN_BEGIN + 154)    //净利润  V_BIGINT
#define JN_S_ZZC  (JN_BEGIN + 155)    //总资产  V_BIGINT
#define JN_S_LDZC  (JN_BEGIN + 156)    //流动资产  V_BIGINT
#define JN_S_WXZC  (JN_BEGIN + 157)    //无形资产  V_BIGINT
#define JN_S_GDZC  (JN_BEGIN + 158)    //固定资产  V_BIGINT
#define JN_S_CQTZ  (JN_BEGIN + 159)    //长期投资  V_BIGINT
#define JN_S_YFu_ZK  (JN_BEGIN + 160)    //应付账款  V_BIGINT
#define JN_S_YS_ZK  (JN_BEGIN + 161)    //预收账款  V_BIGINT
#define JN_S_LD_FZ  (JN_BEGIN + 162)    //流动负债  V_BIGINT
#define JN_S_CQ_FZ  (JN_BEGIN + 163)    //长期负债  V_BIGINT
#define JN_S_JZC  (JN_BEGIN + 164)    //净资产  V_BIGINT
#define JN_S_GDQY  (JN_BEGIN + 165)    //股东权益  V_BIGINT
#define JN_S_ZB_GJJ  (JN_BEGIN + 166)    //资本公积金  V_BIGINT
#define JN_S_F_WFPLR  (JN_BEGIN + 167)    //非未分配利润  V_BIGINT
#define JN_S_NH_MGSY  (JN_BEGIN + 168)    //每股收益(年化)  V_ORGI
#define JN_S_JLRL  (JN_BEGIN + 169)    //净利润率  V_ORGI
#define JN_S_SXL  (JN_BEGIN + 170)    //市销率  V_ORGI
#define JN_S_ZYSR_HB  (JN_BEGIN + 171)    //主营收入环比  V_ORGI
#define JN_S_ZYLR_HB  (JN_BEGIN + 172)    //主营利润环比  V_ORGI
#define JN_S_JLR_HB  (JN_BEGIN + 173)    //净利润环比  V_ORGI
#define JN_S_Y3_FHZZ  (JN_BEGIN + 174)    //三年复合增长  V_ORGI
#define JN_S_GD_HS  (JN_BEGIN + 175)    //股东户数  V_ORGI
#define JN_S_JJ_CG  (JN_BEGIN + 176)    //基金持股  V_BIGINT
#define JN_S_JJ_CG_SZ  (JN_BEGIN + 177)    //基金持股市值  V_BIGINT
#define JN_S_ZLTGB_JG  (JN_BEGIN + 178)    //机构持股占流通股比  V_ORGI
#define JN_S_ZLTGB_JJ  (JN_BEGIN + 179)    //基金持股占流通股比  V_ORGI
#define JN_S_ZLTGB_QF2  (JN_BEGIN + 180)    //QFII持股占流通股比  V_ORGI
#define JN_S_ZLTGB_BX  (JN_BEGIN + 181)    //保险持股占流通股比  V_ORGI
#define JN_S_ZLTGB_QS  (JN_BEGIN + 182)    //券商持股占流通股比  V_ORGI
#define JN_S_ZLTGB_XT  (JN_BEGIN + 183)    //信托持股占流通股比  V_ORGI
#define JN_CS_S_FAJD  (JN_BEGIN + 184)    //方案进度        V_ORGI
#define JN_CS_S_YJYG  (JN_BEGIN + 185)    //业绩预告        V_ORGI
#define JN_CS_S_YJDZ_FD  (JN_BEGIN + 186)    //业绩大增幅度    V_ORGI
#define JN_CS_S_YJZZ_FD  (JN_BEGIN + 187)    //业绩增长幅度    V_ORGI
#define JN_CS_S_YJWD_FD  (JN_BEGIN + 188)    //业绩稳定幅度    V_ORGI

#define JN_S_RY_MGPXE   (JN_BEGIN +191)		//近一年每股派息额	20140302
#define JN_S_TTM_JLR	(JN_BEGIN +192)		//TTM净利润
#define JN_S_TTM_ZYSR	(JN_BEGIN +193)		//TTM主营收入


//计算项目ID从尾部过来

#define JN_CAL_ZZ_BL  (JN_END -6)    //  送转
#define JN_CAL_SDFX  (JN_END -5)    //  深度分析
#define JN_CAL_YJ  (JN_END -4)    //  业绩
#define JN_CAL_GB  (JN_END -3)    //  股本
#define JN_CAL_ZYZB  (JN_END -2)    //  主要指标

#define JN_CAL_B_FXCS  (JN_END -1)    //  债券付息次数  V_ORGI

#define JN_END		1500

//By Steven on 2013-08-29
typedef           short SHORT;
typedef  unsigned short USHORT;

enum EReportType
{
	ERT_NOTDEFINE, // 未定义
	ERT_S_A_JL, // A股金融
	ERT_S_A_NOTJL, // A股非金融
	ERT_S_B,  //B股
	ERT_S_H, // 港股

	ERT_F_CLOSE, // 封闭式基金
	ERT_F_OPEN, //开放式基金
	ERT_F_MONEY, //货币基金

	ERT_B_GZ, // 国债
	ERT_B_KZZ, //可转债
	ERT_B_QYZ, // 企业债
};

enum EValueType
{
	EVT_PX			 = 0,
	EVT_STRING	 = 1,
};

enum EGoodsType
{
	EGT_STOCK =	0,
	EGT_HK			= 1,
	EGT_FUND		= 2,
	EGT_BUND		= 3,
	EGT_Common = 4, //2013-6-6 Wang.YJ 如 空白字段
};

//2013-6-5 Wang.YJ 前向声明，在函数参数中使用了该类型
class CGoodsJBM;

//2013-6-4 Wang.YJ 新的ID
class  CFieldMap
{
public:
	CFieldMap();
	CFieldMap(CString strShortName,CString strFullName, short IDNew,short IDOld, short idxStock,short idxHK,
		short idxFund,short idxBond,UCHAR cValueType,int nDrawType,UCHAR nPower,CString strUnit,UCHAR ucDigit,
		EColorType ect,CString strShowQuarter,UCHAR nOldPower);

	inline CFieldMap& operator=(const CFieldMap& rhs);

	CString m_strFullName;
	CString m_strShortName;

	short m_IDNew;
	short m_IDOld;
	short m_IdxStock;
	short m_idx_HKStock;
	short m_idx_Fund;
	short m_idxBond;

	UCHAR m_cValueType; //2013-6-7 Wang.YJ 1:数值，2.文本
	int m_nDrawType;
	char m_nPower; //2013-7-3 Wang.YJ 数据文件对数据库值放大的倍数,，可正，可负
	char m_nOldPower; //2013-7-3 Wang.YJ 和老数据转换的单位,，可正，可负
	CString m_strUnit;

	UCHAR m_ucDigit;
	EColorType m_ect; //2013-6-18 Wang.YJ 配置显示颜色

	CString  m_strShowQuarter; // 4个字符，0不使用，1表示报告日期，2表示基金财报日期 如‘0000’
};

inline CFieldMap& CFieldMap::operator=( const CFieldMap& rhs )
{
	m_strFullName = rhs.m_strFullName;
	m_strShortName = rhs.m_strShortName;

	m_IDNew = rhs.m_IDNew;
	m_IDOld = rhs.m_IDOld;

	m_IdxStock = rhs.m_IdxStock;
	m_idx_HKStock = rhs.m_idx_HKStock;
	m_idx_Fund = rhs.m_idx_Fund;
	m_idxBond = rhs.m_idxBond;

	m_cValueType = rhs.m_cValueType;
	m_nDrawType = rhs.m_nDrawType;

	m_nPower = rhs.m_nPower;
	m_nOldPower = rhs.m_nOldPower;
	m_strUnit = rhs.m_strUnit;
	m_ucDigit = rhs.m_ucDigit;

	m_ect = rhs.m_ect;
	m_strShowQuarter = rhs.m_strShowQuarter;

	return (*this);
}

class CFPlatDataStock : public CFPlatData
{
public:
	BOOL ExtractOldJBM(CGoodsJBM & jbm);
	BOOL DataToOld(CGoodsJBM & jbm);

	//2013-6-4 Wang.YJ 新加的代码
	void Clear();
	XInt32 GetValue(short FID) const ;
	INT64 GetValueForCompare(short FID) const;	//20110621 为了特殊处理BS
	double GetFValue(const short FID) const;
	static CString GetFundType(const INT64 nID) ;
	static CString GetFundTZFZ(const INT64  nID);
	static CString Field2String(SHORT fieldID);
	static short	  GetValueType(const SHORT fieldID);
	//2013-6-8 Wang.YJ 获取字段映射表
	static const CFieldMap * GetFieldMap(const short fieldID);

	CString GetStrValue(const short FID) const;

	static CString GetStrVal(const short FID,INT64 hVal);

	const EReportType GetReportType() const; // 获取 个财中报表字段类型
	const DWORD GetQuaterInfo(const short FID,UCHAR& ucShort) const;
private:
	inline  BOOL FID2Local(SHORT fieldID,UCHAR &cValueType,SHORT &index)  const;
};

inline BOOL CFPlatDataStock::FID2Local(short fieldID,UCHAR &cValueType,SHORT &index ) const
{
	const CFieldMap * pFM = GetFieldMap(fieldID);
	index = -1;
	if (pFM)
	{
		const CFieldMap &fm = *pFM;
		cValueType = pFM->m_cValueType;
		if  (m_cType == FPD_Type_Stock)
			index = fm.m_IdxStock;
		else if  (m_cType == FPD_Type_HKStock)
			index = fm.m_idx_HKStock;
		else if  (m_cType == FPD_Type_Fund)
			index = fm.m_idx_Fund;
		else if  (m_cType == FPD_Type_Bond)
			index = fm.m_idxBond;
	}

	return index != -1;
}

class CFPlatData_All
{
public:
	CFPlatData_All();
	~CFPlatData_All();

	void Read();
	void Write();

	BOOL Get(DWORD dwGoodsID, CFPlatDataStock&);
public:
	CSortArray<CFPlatDataStock> m_aFPD;
	USHORT m_usVersion;
	DWORD m_dwDate;
	DWORD m_dwTime;

	DWORD m_dwRows;
	DWORD m_dwNumCols;
	DWORD m_dwStrCols;
};

extern CFPlatData_All g_FPlatData;

//extern CFieldMap s_map_FieldMap;

//2013-6-4 Wang.YJ 新旧基本面字段映射表
extern CFieldMap s_Array_FieldMap[];
#endif
