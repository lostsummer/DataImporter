#include "MDS.h"
#include "Goods.h"
#include "Data.h"
#include "GoodsJBM.h"
#include "HisGBAll.h"
#include "FHSP.h"
#include "DynaCompress.h"
#include "MainTrack.h"

extern DWORD g_dwSaveHisMin;

//////////////////////////////////////////////////////////////////////////
const int CDayCheck::DAY_BEGIN_YEAR = 1990;
DWORD CDayCheck::WordToDate( WORD wDay )
{
	struct tm tmBase;
	time_t ttBase, ttDate;

	tmBase.tm_year = DAY_BEGIN_YEAR - 1900;
	tmBase.tm_mon = 0;
	tmBase.tm_mday = 1;
	tmBase.tm_hour = 0;
	tmBase.tm_min = 0;
	tmBase.tm_sec = 0;
	tmBase.tm_isdst = 0;

	ttBase = kernel_mktime(&tmBase);
	ttDate = ttBase+wDay*86400;
	struct tm tm_s;
	struct tm * pDate = localtime_r(&ttDate, &tm_s);

	return (pDate->tm_year+1900)*10000 + (pDate->tm_mon+1)*100 + pDate->tm_mday;
}

WORD CDayCheck::DateToWord( DWORD dwDate )
{
	DWORD dwBase = DAY_BEGIN_YEAR*10000+101;	// 19900101
	if (dwDate<dwBase)
		dwDate = dwBase;

	struct tm tmBase, tmDate;
	time_t ttBase, ttDate;

	tmBase.tm_year = DAY_BEGIN_YEAR - 1900;
	tmBase.tm_mon = 0;
	tmBase.tm_mday = 1;
	tmBase.tm_hour = 0;
	tmBase.tm_min = 0;
	tmBase.tm_sec = 0;
	tmBase.tm_isdst = 0;

	tmDate.tm_year = dwDate/10000 - 1900;
	tmDate.tm_mon = dwDate%10000/100 - 1;
	tmDate.tm_mday = dwDate%100;
	tmDate.tm_hour = 0;
	tmDate.tm_min = 0;
	tmDate.tm_sec = 0;
	tmDate.tm_isdst = 0;

	ttBase = kernel_mktime(&tmBase);
	ttDate = kernel_mktime(&tmDate);

	return (ttDate-ttBase)/86400;
}

const CHisOrderQueue& CHisOrderQueue::operator=(const CHisOrderQueue& src)
{
	m_cActive = src.m_cActive;

	m_dwTime = src.m_dwTime;
	m_dwPrice = src.m_dwPrice;
	m_cBuySell = src.m_cBuySell;
	m_dwNumOfOrders = src.m_dwNumOfOrders;

	//m_adwOrderVol.Copy(src.m_adwOrderVol);
	m_adwOrderVol = src.m_adwOrderVol;

	m_dwOriginalVol = src.m_dwOriginalVol;
	m_dwPartTradeVol = src.m_dwPartTradeVol;
	m_dwPartNewVol = src.m_dwPartNewVol;

	m_nNumOfRcntTrade = src.m_nNumOfRcntTrade;
	CopyMemory(m_adwRcntTrade, src.m_adwRcntTrade, NUMOF_RCT_TRADEORDER*sizeof(DWORD));

	//m_aOrderStatus.Copy(src.m_aOrderStatus);
	m_aOrderStatus = src.m_aOrderStatus;

	return *this;
}

void CHisOrderQueue::Read(CBuffer& buf)
{
	short s, sSize;
	DWORD dwVol;

	//m_adwOrderVol.RemoveAll();
	//m_aOrderStatus.RemoveAll();
	m_adwOrderVol.clear();
	m_aOrderStatus.clear();

	buf.Read(&m_cActive, 1);

	buf.Read(&m_dwTime, 4);
	buf.Read(&m_dwPrice, 4);
	buf.Read(&m_cBuySell, 1);
	buf.Read(&m_dwNumOfOrders, 4);

	buf.Read(&sSize, 2);
	for (s=0; s<sSize; s++)
	{
		buf.Read(&dwVol, 4);
		//m_adwOrderVol.Add(dwVol);
		m_adwOrderVol.push_back(dwVol);
	}

	if (m_cActive==1)
	{
		buf.Read(&m_dwOriginalVol, 4);
		buf.Read(&m_dwPartTradeVol, 4);
		buf.Read(&m_dwPartNewVol, 4);

		buf.Read(&m_nNumOfRcntTrade, 4);
		buf.Read(&m_adwRcntTrade, 4*m_nNumOfRcntTrade);

		buf.Read(&sSize, 2);
		COrderStatus os;
		for (s=0; s<sSize; s++)
		{
			os.Read(buf);
			//m_aOrderStatus.Add(os);
			m_aOrderStatus.push_back(os);
		}
	}
}

void CHisOrderQueue::Write(CBuffer& buf)
{
	short s, sSize;
	DWORD dwVol;
	DWORD dwNumOfOrders = m_dwNumOfOrders>50 ? 50:m_dwNumOfOrders;

	buf.Write(&m_cActive, 1);

	buf.Write(&m_dwTime, 4);
	buf.Write(&m_dwPrice, 4);
	buf.Write(&m_cBuySell, 1);
	buf.Write(&m_dwNumOfOrders, 4);

	sSize = short(m_adwOrderVol.size());
	if( sSize>50 ){
		sSize = 50;
	}
	buf.Write(&sSize, 2);
	for (s=0; s<sSize; s++)
	{
		dwVol = m_adwOrderVol[s];
		buf.Write(&dwVol, 4);
	}

	if (m_cActive==1)
	{
		DWORD dwNumOfRcntTrade = m_nNumOfRcntTrade>50 ? 50:m_nNumOfRcntTrade;
		buf.Write(&m_dwOriginalVol, 4);
		buf.Write(&m_dwPartTradeVol, 4);
		buf.Write(&m_dwPartNewVol, 4);

		buf.Write(&dwNumOfRcntTrade, 4);
		buf.Write(&m_adwRcntTrade, 4*dwNumOfRcntTrade);

		sSize = short(m_aOrderStatus.size());
		if( sSize>50 ){
			sSize = 50;
		}
		buf.Write(&sSize, 2);
		for (s=0; s<sSize; s++)
		{
			m_aOrderStatus[s].Write(buf);
		}
	}
}

void CHisOrderQueue::Write2OldStock(CBuffer& buf, char cBuySell1)
{
	short s, sSize;
	DWORD dwVol;

	buf.Write(&m_cActive, 1);

	DWORD dwTime = m_dwTime/100;
	buf.Write(&dwTime, 4);
	buf.Write(&m_dwPrice, 4);
	buf.Write(&m_cBuySell, 1);
	buf.Write(&m_dwNumOfOrders, 4);

	sSize = short(m_adwOrderVol.size());
	buf.Write(&sSize, 2);
	for (s=0; s<sSize; s++)
	{
		dwVol = m_adwOrderVol[s];
		buf.Write(&dwVol, 4);
	}

	if (m_cActive==1)
	{
		if (cBuySell1==1)
		{
			buf.Write(&m_dwOriginalVol, 4);
			buf.Write(&m_dwPartTradeVol, 4);
			buf.Write(&m_dwPartNewVol, 4);

			buf.Write(&m_nNumOfRcntTrade, 4);
			buf.Write(&m_adwRcntTrade, 4*m_nNumOfRcntTrade);

			sSize = short(m_aOrderStatus.size());
			buf.Write(&sSize, 2);
			for (s=0; s<sSize; s++)
			{
				m_aOrderStatus[s].Write(buf);
			}
		}
		else
		{
			DWORD dw0 = 0;
			WORD w0 = 0;

			buf.Write(&dw0, 4);
			buf.Write(&dw0, 4);
			buf.Write(&dw0, 4);

			buf.Write(&m_nNumOfRcntTrade, 4);
			buf.Write(&m_adwRcntTrade, 4*m_nNumOfRcntTrade);

			buf.Write(&w0, 2);
		}
	}
}

BYTE CHisOrderQueue::GetCheckSum(BOOL bBuySell1)
{
	BYTE cCheckSum = 0;

	cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPrice, 4);
	cCheckSum += GetBytesCheckSum((PBYTE)&m_dwTime, 4);
	cCheckSum += GetBytesCheckSum((PBYTE)&m_dwNumOfOrders, 4);

	int nNumQOrder = m_adwOrderVol.size();
	cCheckSum += GetBytesCheckSum((PBYTE)&nNumQOrder, 4);

	for(int i = 0; i < nNumQOrder; i++)
		cCheckSum += GetBytesCheckSum((PBYTE)&m_adwOrderVol[i], 4);

	if(bBuySell1)
	{
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwOriginalVol, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPartTradeVol, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPartNewVol, 4);

		cCheckSum += GetBytesCheckSum((PBYTE)&m_nNumOfRcntTrade, 4);
		for(int i = 0; i < m_nNumOfRcntTrade; i++)
			cCheckSum += GetBytesCheckSum((PBYTE)&m_adwRcntTrade[i], 4);

		int nNumOrderStatus = m_aOrderStatus.size();
		cCheckSum += GetBytesCheckSum((PBYTE)&nNumOrderStatus, 4);

		for(int i = 0; i < nNumOrderStatus; i++)
		{
			COrderStatus& os = m_aOrderStatus[i];

			cCheckSum += GetBytesCheckSum((PBYTE)&os.m_dwOrderVol, 4);
			cCheckSum += GetBytesCheckSum((PBYTE)&os.m_status, 1);
		}
	}

	return cCheckSum;
}

//////////////////////////////////////////////////////////////////////////
CQueueMatrix::CQueueMatrix()
{
	m_pGoods = 0;
	m_dwCount = 0;

	m_dwDynamicTimeBuy = 0;
	m_dwDynamicTimeSell = 0;
	m_saHisOrderBuyArray.SetSize(theApp.m_nMaxNumOfHisQSend);
	m_saHisOrderSellArray.SetSize(theApp.m_nMaxNumOfHisQSend);
	m_saHisOrderSellArray.m_bAscend = FALSE;
}

CQueueMatrix::~CQueueMatrix()
{
	m_saHisOrderBuyArray.RemoveAll();
	m_saHisOrderSellArray.RemoveAll();
}

void CQueueMatrix::Clear()
{
	m_dwCount = 0;

	m_dwDynamicTimeBuy = m_dwDynamicTimeSell = 0;

	m_saHisOrderBuyArray.RemoveAt(0, m_saHisOrderBuyArray.GetSize());//RemoveAll();
	m_saHisOrderSellArray.RemoveAt(0, m_saHisOrderSellArray.GetSize());//RemoveAll();

	m_ocSumOrder.Clear();
	m_ocSumTrade.Clear();

	m_scSumOrder.Clear();
	m_scSumTrade.Clear();
}

void CQueueMatrix::Read(CBuffer& buf)
{
	short s, sSize;
	CHisOrderQueue hoq;

	//RWLock_scope_wrlock  wrlock(m_MatrixLock);

	m_saHisOrderSellArray.RemoveAt(0, m_saHisOrderSellArray.GetSize());//RemoveAll();
	m_saHisOrderBuyArray.RemoveAt(0, m_saHisOrderBuyArray.GetSize());//RemoveAll();

	buf.Read(&m_dwCount, 4);

	buf.Read(&sSize, 2);
	for (s=0; s<sSize; s++)
	{
		hoq.Read(buf);
		m_saHisOrderSellArray.Add(hoq);
	}
	buf.Read(&m_dwDynamicTimeSell, 4);

	buf.Read(&sSize, 2);
	for (s=0; s<sSize; s++)
	{
		hoq.Read(buf);
		m_saHisOrderBuyArray.Add(hoq);
	}
	buf.Read(&m_dwDynamicTimeBuy, 4);

	buf.Read(&m_ocSumOrder, sizeof(COrderCounts));
	buf.Read(&m_ocSumTrade, sizeof(COrderCounts));

	buf.Read(&m_scSumOrder, sizeof(CStatCounts));
	buf.Read(&m_scSumTrade, sizeof(CStatCounts));
}

void CQueueMatrix::Write(CBuffer& buf)
{
//	RWLock_scope_rdlock  rdlock(m_MatrixLock);

	buf.Write(&m_dwCount, 4);

	short sSizeSell = short(m_saHisOrderSellArray.GetSize());
	buf.Write(&sSizeSell, 2);
	for (short s=0; s<sSizeSell; s++)
		m_saHisOrderSellArray[s].Write(buf);
	buf.Write(&m_dwDynamicTimeSell, 4);

	short sSizeBuy = short(m_saHisOrderBuyArray.GetSize());
	buf.Write(&sSizeBuy, 2);
	for (short s=0; s<sSizeBuy; s++)
		m_saHisOrderBuyArray[s].Write(buf);
	buf.Write(&m_dwDynamicTimeBuy, 4);

	buf.Write(&m_ocSumOrder, sizeof(COrderCounts));
	buf.Write(&m_ocSumTrade, sizeof(COrderCounts));

	buf.Write(&m_scSumOrder, sizeof(CStatCounts));
	buf.Write(&m_scSumTrade, sizeof(CStatCounts));
}

void CQueueMatrix::Write2OldStock(CBuffer& buf)
{
	buf.Write(&m_dwCount, 4);

	short sSizeBuy = short(m_saHisOrderBuyArray.GetSize());
	short sSizeSell = short(m_saHisOrderSellArray.GetSize());

	short sSize = sSizeBuy+sSizeSell;
	buf.Write(&sSize, 2);

	for (short s=0; s<sSizeSell; s++)
		m_saHisOrderSellArray[s].Write2OldStock(buf, s==0);
	for (short s=0; s<sSizeBuy; s++)
		m_saHisOrderBuyArray[s].Write2OldStock(buf, s==0);

	buf.Write(&(m_pGoods->m_dwTime), 4);
	buf.Write(&m_ocSumOrder, sizeof(COrderCounts));
	buf.Write(&(m_pGoods->m_dwTime), 4);
	buf.Write(&m_ocSumTrade, sizeof(COrderCounts));

	buf.Write(&m_scSumOrder, sizeof(CStatCounts));
	buf.Write(&m_scSumTrade, sizeof(CStatCounts));
}

BYTE CQueueMatrix::GetCheckSum(BYTE cBuySell, BOOL bActiveOnly)
{
	DWORD dwMTime = 0;
	CHisOrderQueueSArray* psaHisOrder = NULL;
	CHisOrderQueueSArray aHisOrderNull;

	if(cBuySell == ORDER_BID)
	{
		dwMTime = m_dwDynamicTimeBuy;
		if (m_pGoods->m_xBuyVol.m_nBase)
			psaHisOrder = &m_saHisOrderBuyArray;
		else
			psaHisOrder = &aHisOrderNull;
	}
	else
	{
		dwMTime = m_dwDynamicTimeSell;
		if (m_pGoods->m_xSellVol.m_nBase)
			psaHisOrder = &m_saHisOrderSellArray;
		else
			psaHisOrder = &aHisOrderNull;
	}

	CHisOrderQueueSArray& saHisOrder = *psaHisOrder;

	BYTE cCheckSum = 0;
	cCheckSum += GetBytesCheckSum((PBYTE)&dwMTime, 4);

	int nNumQ = saHisOrder.GetSize();
	if(nNumQ > theApp.m_nMaxNumOfHisQSend)
		nNumQ = theApp.m_nMaxNumOfHisQSend;

	for(int i = 0; i < nNumQ; i++)
	{
		CHisOrderQueue& hisQ = saHisOrder[i];

		if (bActiveOnly && hisQ.m_cActive==0)
			break;

		cCheckSum += hisQ.GetCheckSum(i == 0);
	}

	return cCheckSum;
}


//////////////////////////////////////////////////////////////////////////
CFDayMobile CGoods::Day2Mobile(CDay& day)
{
	CFDayMobile fd;

	fd.m_dwTime = day.m_dwTime;
	fd.m_fVolume = double(day.m_xVolume);
	fd.m_fAmount = double(day.m_xAmount);

	fd.m_fOpen = double(day.m_dwOpen)/1000.0;
	fd.m_fHigh = double(day.m_dwHigh)/1000.0;
	fd.m_fLow = double(day.m_dwLow)/1000.0;
	fd.m_fClose = double(day.m_dwClose)/1000.0;

	fd.m_dwNumOfBuy = day.m_dwNumOfBuy;
	fd.m_dwNumOfSell = day.m_dwNumOfSell;

	for (int i = 0; i < 3; i++)
	{
		if ((m_cGroup != 1 && m_cGroup != 3 && m_cGroup != 9 && m_cGroup != 14 && fd.m_dwNumOfBuy > 0 && fd.m_dwNumOfSell > 0)
			|| (m_cGroup == 1 && fd.m_dwTime >= 20070701 && fd.m_dwNumOfBuy > 0 && fd.m_dwNumOfSell > 0)//上A
			|| (m_cGroup == 3 && fd.m_dwTime >= 20091026 && fd.m_dwNumOfBuy > 0 && fd.m_dwNumOfSell > 0)//深A
			|| (m_cGroup == 9 && fd.m_dwTime >= 20091026 && fd.m_dwNumOfBuy > 0 && fd.m_dwNumOfSell > 0)//中小板
			|| (m_cGroup == 14 && fd.m_dwTime >= 20091030 && fd.m_dwNumOfBuy > 0 && fd.m_dwNumOfSell > 0))//创业板
		{
			fd.m_fAmtOfBuy[i] = double(day.m_pxAmtOfBuy[i]);
			fd.m_fAmtOfSell[i] = double(day.m_pxAmtOfSell[i]);
			fd.m_fVolOfBuy[i] = double(day.m_pxVolOfBuy[i]);
			fd.m_fVolOfSell[i] = double(day.m_pxVolOfSell[i]);
		}
		else
		{
			fd.m_fAmtOfBuy[i] = 0.0;
			fd.m_fAmtOfSell[i] = 0.0;
			fd.m_fVolOfBuy[i] = 0.0;
			fd.m_fVolOfSell[i] = 0.0;
		}
	}

	return fd;
}

CDayMobile CGoods::DayMobile_F2L(CFDayMobile& fd, char cIndType, char MaExp, char VMAExp, char IndExp)
{
	CDayMobile day;

	day.m_dwTime = fd.m_dwTime;

	day.m_xVolume = INT64(fd.m_fVolume+0.5);
	day.m_xAmount = INT64(fd.m_fAmount+0.5);

	day.m_dwOpen = DWORD(fd.m_fOpen*1000.0+0.5);
	day.m_dwHigh = DWORD(fd.m_fHigh*1000.0+0.5);
	day.m_dwLow = DWORD(fd.m_fLow*1000.0+0.5);
	day.m_dwClose = DWORD(fd.m_fClose*1000.0+0.5);

	int Num = Max(MaExp, VMAExp);
	Num = Max(Num, IndExp);
	for (int i=0; i<Num; i++)
	{
		if (i < MaExp)
			day.m_pdwMA[i] = DWORD(fd.m_pfMA[i]*1000.0+0.5);
		if (i < VMAExp)
			day.m_pxVMA[i] = INT64(fd.m_pfVMA[i]+0.5);
		
		if (i < IndExp)
		{
			switch (cIndType)
			{
			case 2 :
				day.m_pxInd[i] = INT64(fd.m_pfInd[i]+0.5);
				break;
			case 9:
			case 10:
			case 11:
			case 12:
			case 32:
			case 33:
				{
					double tmp = fd.m_pfInd[i];
					if (IsIndex())
						tmp /= 100;
					day.m_pxInd[i] = (INT64)(tmp >= 0 ? (tmp+0.5) : (tmp-0.5));
				}
				break;
			case 8:
			case 18:
				{
					day.m_pxInd[i] = (INT64)(fd.m_pfInd[i] >= 0 ? (fd.m_pfInd[i]+0.5) : (fd.m_pfInd[i]-0.5));
				}
				break;
			case 21:
				{
					if (i < 1)
					{
						if (fd.m_pfInd[i]>=0)
							day.m_pxInd[i] = INT64(fd.m_pfInd[i]*1000+0.5);
						else
							day.m_pxInd[i] = INT64(fd.m_pfInd[i]*1000-0.5);
					}
					else
						day.m_pxInd[i] = INT64(fd.m_pfInd[i]);
				}
				break;
			case 127:
				day.m_pxInd[i] = INT64(fd.m_pfInd[i]);
				break;
			default :
				if (fd.m_pfInd[i]>=0)
					day.m_pxInd[i] = INT64(fd.m_pfInd[i]*1000+0.5);
				else
					day.m_pxInd[i] = INT64(fd.m_pfInd[i]*1000-0.5);
			}
		}
	}

	day.m_cCPX = fd.m_cCPX;

	return day;
}

//////////////////////////////////////////////////////////////////////
volatile int CGoods::s_nCacheDay = 0;
volatile int CGoods::s_nCacheMin30 = 0;
volatile int CGoods::s_nCacheMin5 = 0;
volatile int CGoods::s_nCacheMin1 = 0;
volatile int CGoods::s_nCacheMinute0 = 0;
volatile int CGoods::s_nCacheMinute1 = 0;
volatile int CGoods::s_nCacheBargain = 0;
volatile int CGoods::s_nCacheHisMin = 0;

//////////////////////////////////////////////////////////////////////////
RWLock CGoods::m_rwBK;
CGoods::CGoods()
{
	m_pIndex = NULL;

	m_matrix.m_pGoods = this;

	m_nPriceDiv = 0;
	m_dwVolDiv = 0;

	m_cStation = 0;
	m_dwNameCode = 0;
	ZeroMemory(m_pcNameCode, LEN_STOCKCODE+1);

	m_dwA_H = 0;

	m_wGroupHY = 0;
	m_wGroupDQ = 0;

	m_cLevel = 1;

	m_dwClose = 0;
	m_dwPriceZT = 0;
	m_dwPriceDT = 0;

	ZeroMemory(m_pcName, LEN_STOCKNAME);
	ZeroMemory(m_pcCode, LEN_STOCKCODE+1);
	ZeroMemory(m_pcPreName, 4);

	m_sTimeZone = 0;

	m_cDotShow = -1;
	m_cDotData = -1;
	m_wVolInHand = 0;

	m_cPriceDiv = 0;
	m_dwPriceDiv = 1;
	m_cDecimalPosition = 2;

	m_strEName.Empty();

	m_pcBufferBase = NULL;

	m_pcBargainParam = NULL;
	m_pBargainBuffer = NULL;
	m_wBargainBuffer = 0;
	m_wBargain = 0;
	m_dwBargainTotal = 0;
	m_cBargainCheckSum = 0;
	m_dwBargainCheckSum = 0;
	m_dwDateBargain = 0;

	m_pcMinuteParam = NULL;
	m_pMinuteBuffer = NULL;
	m_wMinuteBuffer = 0;
	m_wMinute = 0;
	m_wMin1_s = 0;
    m_dwMin30_s = 0;
    m_dwMin60_s = 0;
    m_dwDay_s = 0;
    m_dwWeek_s = 0;
    m_dwMonth_s = 0;
	m_wMinuteInDisk = 0;
	m_wLastMinInDisk = 0;
	m_cMinuteCheckSumInDisk = 0;
	m_dwMinuteCheckSumInDisk = 0;
	m_dwDateMinute = 0;

	m_dwDate = 0;
	m_dwTime = 0;
	m_dwDateSave = 0;

	m_dwCountStatic = 0;
	m_dwCountDynamic = 0;
	m_dwCountMinute = 0;
	m_dwCountPartOrder = 0;


	m_xLTG = 0;
	m_xZFX = 0;

	m_dwDateSave = 0;

	m_dwFirstDate = 0;
	m_dwLastDate = 0;

	m_cGroup = 0;
	//m_dwGroup = 0;
	m_hMaskGroup = 0;

	m_cPool1 = 0;
	m_dwPricePool1 = 0;
	m_dwDatePool1 = 0;
	m_dwPricePool1_2 = 0;
	m_dwDatePool1_2 = 0;
	m_dwHighPool1_2 = 0;
	m_dwCountPool1 = 0;

	m_dwCountCPX = 1;

	m_nCPX_Day = 0;
	m_dwCPX_Close = 0;

	m_nCPX_Week = 0;
	m_nCPX_Month = 0;

	m_nCPX_Min1 = 0;
	m_nCPX_Min5 = 0;
	m_nCPX_Min15 = 0;
	m_nCPX_Min30 = 0;
	m_nCPX_Min60 = 0;

	m_dwCountPool2 = 0;
	m_cPool2 = 0;
	m_dwDatePool2 = 0;
	m_dwPricePool2 = 0;
	m_dwDatePool2_2 = 0;
	m_dwPricePool2_2 = 0;
	m_dwHighPool2_2 = 0;

	m_strPreName.Empty();
	m_strHYName.Empty();

	m_dwI_Num = 0;
	m_dwI_Close = 0;
	m_dwI_Ave = 0;
	m_dwI_Amount = 0;
	m_dwI_AveGB = 0;
	m_dwI_SumSZ = 0;
	m_dwI_Percent = 0;
	m_dwI_SYL = 0;
	m_dwI_JBBS = -1;

	ZeroMemory(&m_XGStat, sizeof(m_XGStat));

	m_xSZ = 0;
	m_xLTSZ = 0;

	m_dwCPX_Close = 0;
	m_dwCheckCnt = m_dwCheckSum = 0;
	m_dwCheckOK = FALSE;

	m_dwSysAlert = 0;
	m_dwXGStatus = 0;

	m_pDataDay = NULL;
	m_pDataMin30 = NULL;
	m_pDataMin5 = NULL;
	m_pDataMin1 = NULL;

	m_pDataMinute0 = NULL;
	m_pDataMinute1 = NULL;

	m_pDataBargain = NULL;

	m_pDataHisMin = NULL;

	m_pwrTrade = NULL;
	m_pwrPartOrder = NULL;
	
	m_goodsScore = "无";
	memset(&m_rwGoodcount,0,100*sizeof(int));


///20120319 sjp
	m_aExpValue.SetSize(0, 20);
///20120319 sjp

	m_cTradingStatus = 0;
	m_cSecurityProperties = 'N';

	m_cSecurityType = -2;	//未知


	m_dwAuctionPrice = 0;
	m_hAuctionQty = 0;

	memset(m_pcTradeCode, ' ', 4);
	
	InitDay();
}

CGoods::~CGoods()
{
	DeleteData();

	if (m_pwrTrade)
		delete m_pwrTrade;
	if (m_pwrPartOrder)
		delete m_pwrPartOrder;

	if (m_pIndex)
		delete m_pIndex;
	m_pIndex = NULL;
}


CString CGoods::GetName()
{
	char pcBuf[LEN_STOCKNAME+1] = {0};
	
	if (GetID()/10000 == 401 && strlen(m_pcName) == 0) 
			snprintf(pcBuf, 9, "TF%d", GetID()%10000);
//	else if(IsGZQH() && GetID()%1000000 != 1 && GetID()%1000000 != 2 && GetID()%1000000 != 3 && GetID()%1000000 != 4 && GetID()/10000 != 401)
	else if(IsGZQH() && strlen(m_pcName) == 0)
			snprintf(pcBuf, 9, "股指%d", GetID()%1000000);
	else
		memcpy(pcBuf, m_pcName, LEN_STOCKNAME);
	pcBuf[LEN_STOCKNAME] = '\0';

	return pcBuf;
}

CString CGoods::GetPreName()
{
	CString str = "";

	CStringArray aPreName;
	GetPreName(aPreName);

	int nSize = (int)aPreName.GetSize();
	for (int i=0; i<nSize; i++)
	{
		if (i)
			str += "  ";
		str += aPreName[i];
	}

	return str;
}

void CGoods::GetPreName(CStringArray& aPreName)
{
	CString str;
	if (m_pcPreName[0]=='Y')
	{
		str = "整天停牌";
		aPreName.Add(str);
	}
	else if (m_pcPreName[0]=='H')
	{
		str = "暂停交易";
		aPreName.Add(str);
	}

	if (m_pcPreName[1]=='C')
	{
		str = "仅可融资";
		aPreName.Add(str);
	}
	else if (m_pcPreName[1]=='S')
	{
		str = "仅可融券";
		aPreName.Add(str);
	}
	else if (m_pcPreName[1]=='A')
	{
		str = "可融资融券";
		aPreName.Add(str);
	}

	if (m_pcPreName[2]=='N')
	{
		str = "上市首日";
		aPreName.Add(str);
	}
	else if (m_pcPreName[2]=='M')
	{
		str = "恢复上市首日";
		aPreName.Add(str);
	}
	else if (m_pcPreName[2]=='I')
	{
		str = "处于发行期间";
		aPreName.Add(str);
	}
	else if (m_pcPreName[2]=='J')
	{
		str = "再融资发行申购期间";
		aPreName.Add(str);
	}
	else if (m_pcPreName[2]=='X')
	{
		str = "除权除息";
		aPreName.Add(str);
	}
	else if (m_pcPreName[2]=='W')
	{
		str = "W";
		aPreName.Add(str);
	}
	else if (m_pcPreName[2]=='V')
	{
		str = "当日可网络投票";
		aPreName.Add(str);
	}

	if (m_pcPreName[3]=='A')
	{
		str = "收市前发布公告提示";
		aPreName.Add(str);
	}
	else if (m_pcPreName[3]=='C')
	{
		str = "封闭式基金";
		aPreName.Add(str);
	}
	else if (m_pcPreName[3]=='O')
	{
		str = "开放式基金";
		aPreName.Add(str);
	}
	else if (m_pcPreName[3]=='F')
	{
		str = "非交易型基金";
		aPreName.Add(str);
	}
	else if (m_pcPreName[3]=='L')
	{
		str = "上市开放式基金";
		aPreName.Add(str);
	}
	else if (m_pcPreName[3]=='E')
	{
		str = "ETF基金";
		aPreName.Add(str);
	}
}

CString pstrTradeCode[] = {"开市前", "开盘集合竞价", "连续竞价阶段", "盘中临时停盘", "收盘集合竞价", "集合竞价闭市", "协议转让结束", "闭市"};

CString CGoods::GetTradeCode()
{
	if (m_cTradeCode>=0 && m_cTradeCode<=7)
		return pstrTradeCode[m_cTradeCode];
	else
		return "";
}

CString CGoods::GetHYName()
{
	CString str = "";

	if (m_wGroupHY>0)
	{
		//CCalcGroup* pCalcGroup = m_pChannel->GetCalcGroup(2000+m_wGroupHY);
		//if (pCalcGroup)
		//	str = pCalcGroup->m_strGroup;
	}

	return str;
}


void CGoods::InitDay()
{
	ZeroMemory(m_pcPreName, 4);
	m_cTradeCode = -1;
	m_dwCountStatus = 0;

	//m_dwClose = 0;
	//m_dwPriceZT = 0;
	//m_dwPriceDT = 0;

	m_dwPreSettlement = 0;
	m_xPreOpenInterest = 0;

	m_dwSettlement = 0;

	m_dwPClose = 0;
	m_dwWClose = 0;
	m_dw5Close = 0;

	m_xAveVol5 = 0;
	m_xSumVol4 = 0;

	m_xSumBigVol4 = 0;
	m_xSumBigAmt4 = 0;

	m_bZLZC4 = 0;
	m_bZLZC9 = 0;
	m_bZLZC19 = 0;
	m_bZLQM = 0;
	m_dwClose9 = 0;
	m_dwClose19 = 0;

	InitDynamic();

	m_matrix.Clear();

	m_dwCountStatic = 0;
	m_dwCountDynamic = 0;
	m_dwCountMinute = 0;
	m_dwCountPartOrder = 0;

	m_nOldZLZC10 = 0;
	m_nOldZLQM = 0;

	ZeroMemory(m_aDynamicValue, sizeof(m_aDynamicValue));	// dynaCompress
	SaveNewDynamicValue(); // dynaCompress

	if (m_pIndex)
		m_pIndex->InitDay();

	m_dwCheckCnt = m_dwCheckSum = 0;
	m_dwCheckOK = FALSE;

	m_dwSysAlert = 0;
	m_dwXGStatus = 0;

	LoadJBM();
	///////////////////////////////////////////////////////////
	// 初始化的时候删除K线缓存 [11/18/2013 frantqian]
 	if (m_pDataDay != NULL)
 	{
 		m_pDataDay->DeleteData();
 		CMyThread::atomicDec(&s_nCacheDay);
 	}
 	
 	if (m_pDataMin30 != NULL)
 	{
 		m_pDataMin30->DeleteData();
 		CMyThread::atomicDec(&s_nCacheMin30);
 	}
 
 
 	if (m_pDataMin5 != NULL)
 	{
 		m_pDataMin5->DeleteData();
 		CMyThread::atomicDec(&s_nCacheMin5);
 	}
 
 
 	if (m_pDataMin1 != NULL)
 	{
 		m_pDataMin1->DeleteData();
 		CMyThread::atomicDec(&s_nCacheMin1);
 	}
 
 	if (m_pDataMinute0 != NULL)
 	{
 		m_pDataMinute0->DeleteData();
 		CMyThread::atomicDec(&s_nCacheMinute0);
 	}
 
 	if (m_pDataMinute1 != NULL)
 	{
 		m_pDataMinute1->DeleteData();
 		CMyThread::atomicDec(&s_nCacheMinute1);
 	}

 	if (m_pDataBargain != NULL)
 	{
 		m_pDataBargain->DeleteData();
 		CMyThread::atomicDec(&s_nCacheBargain);
 	}

	if (m_pDataHisMin != NULL)
	{
		if (m_rwHisMin.TryAcquireWriteLock() == 0)
		{
			if (m_pDataHisMin->TestWantFree())
			{
				m_pDataHisMin->Trace("释放");
				m_pDataHisMin->DeleteData();
				CMyThread::atomicDec(&s_nCacheHisMin);
			}
			m_rwHisMin.ReleaseWriteLock();
		}
	}
}

void CGoods::InitDynamic()
{
	m_dwCountDynamic = 0;
	m_dwTime = 0;

	m_dwNorminal = 0;

	m_dwOpen = 0;
	m_dwHigh = 0;
	m_dwLow = 0;
	m_dwPrice = 0;

	m_xVolume = 0;
	m_xAmount = 0;

	m_dwTradeNum = 0;

	m_xOpenInterest = 0;

	ZeroMemory(m_pdwMMP, 4*20);
	ZeroMemory(m_pxMMPVol, 4*20);

	m_dwBuyPrice = 0;
	m_dwSellPrice = 0;

	m_xBuyVol = 0;
	m_xSellVol = 0;

	m_xNeiPan = 0;
	m_xCurVol = 0;
	m_cCurVol = 0;
	m_xCurOI = 0;

	m_dwPrice5 = 0;

	m_nStrong = 0;
	m_nBKStrong = 0;
}


void CGoods::Goods2Group()
{
	if (m_cStation==L1SH_STATION)
	{
		m_cGroup = -1;
		m_hMaskGroup = 0;
		
		return;
	}

	//0:指数 1:上证Ａ股 2:上证Ｂ股 3:深证Ａ股 4:深证Ｂ股
	//5:上证基金 6:深证基金 7:上证债券 8:深证债券 9:中小板
	//10:全部股票 11:全部Ａ股 12:权证  13:香港H股 14:创业板
	//15:股指期货 16:港股 17:国债期货 20 新三板

	UINT64 hMaskCode = 0, hMaskName = 0;

	CString strName = GetName();
	if (strName.GetLength()>2)
	{
		if (m_cStation==0)
		{
			if (strName[0]=='N')
				hMaskName = GROUP_SH_N;
			if (strName.Find("ST")>=0)
				hMaskName = GROUP_SH_ST;
			if (strName.Find("退市")>=0)
				hMaskName = GROUP_SH_Z;
		}
		else if (m_cStation==1)
		{
			DWORD xsb = m_dwNameCode/10000;
			if( xsb != 43 && xsb!=83){ //为了解决新三板ST奥贝克被误认为GROUP_SZ_ST的bug
				if (strName[0]=='N')
					hMaskName = GROUP_SZ_N;
				if (strName.Find("ST")>=0)
					hMaskName = GROUP_SZ_ST;
			}
		}
	}

	if (m_cStation==0 || m_cStation==1)
	{
		if (m_cSecurityType<-1)
		{
			if (IsIndex())
				m_cGroup = 0;
			else if (m_cStation==0)
			{
				int n6 = m_dwNameCode/100000;
				if (n6==3 || n6==6)
					m_cGroup = 1;
				else if (n6==9)
					m_cGroup = 2;
				else if (n6==5)
				{
					if (m_dwNameCode/10000==58)
						m_cGroup = 12;
					else
						m_cGroup = 5;
				}
				else
					m_cGroup = 7;
			}
			else if (m_cStation==1)
			{
				if (m_cSecurityProperties=='Z')
				{
					//深圳退市整理不计入主板
				}
				else
				{
					int n5 = m_dwNameCode/10000;
					int n4 = m_dwNameCode/100000;

					if (n5==0)
					{
						if (m_dwNameCode>=2001 && m_dwNameCode<=2999)
							m_cGroup = 9;
						else
							m_cGroup = 3;
					}
					else if (n5==20)
						m_cGroup = 4;
					else if (n5==30)
						m_cGroup = 14;
					else if (n5>=15 && n5<=18)
						m_cGroup = 6;
					else if (n5==3)
						m_cGroup = 12;
					else if(n4 == 4)
						m_cGroup = 20;
					else
						m_cGroup = 8;
				}
			}
		}
		else
		{
			if (m_cStation==0)
			{
				if (m_cSecurityType==INDEX)
					m_cGroup = 0;
				else if (m_cSecurityType==EQUITY)
				{
					if (m_dwNameCode>=900000)
						m_cGroup = 2;
					else
						m_cGroup = 1;
				}
				else if (m_cSecurityType==FUND)
					m_cGroup = 5;
				else if (m_cSecurityType==BOND || m_cSecurityType==CONVERTIBLE || m_cSecurityType==REPO)
					m_cGroup = 7;
			}
			else
			{
				if (m_cSecurityProperties=='Z')
				{
					//深圳退市整理不计入主板
				}
				else if (m_cSecurityType==INDEX)
					m_cGroup = 0;
				else if (m_cSecurityType==EQUITY)
				{
					DWORD dw3 = m_dwNameCode/1000;
					if (dw3>=2 && dw3<=4)	// 2-4 中小企业板股票
						m_cGroup = 9;
					else if (dw3>=200 && dw3<=209)
						m_cGroup = 4;
					else if (dw3>=300 && dw3<=309)
						m_cGroup = 14;
					else
						m_cGroup = 3;
				}
				else if (m_cSecurityType==FUND)
					m_cGroup = 6;
				else if (m_cSecurityType==BOND || m_cSecurityType==CONVERTIBLE || m_cSecurityType==REPO)
					m_cGroup = 8;
			}
		}
	}
	else if (m_cStation==2)
	{
		long l = m_dwNameCode/1000;
		switch(l)
		{
		case 1:
			m_cGroup = 41;
			hMaskCode = GROUP_GNBK; 
			break;
		case 2:
			m_cGroup = 42;
			hMaskCode = GROUP_HYBK;
			break;
		case 3:
			m_cGroup = 43;
			hMaskCode = GROUP_DQBK;
			break;
		case 4:
			m_cGroup = 44;
			hMaskCode = GROUP_RDBK;
			break;
		case 6: 					//56是三级概念行业,新版块用
			m_cGroup = 45;
			hMaskCode = GROUP_XGNBK; 
			break;
		case 5:
			m_cGroup = 46;
			hMaskCode = GROUP_XHYBK; 
			break;
		default:
			m_cGroup = 99;
		}		
	}
	else if (m_cStation==3)
	{
		m_cGroup = 13;
		//hMaskCode = GROUP_HK; //不再将股票标记为AH股，而是将股票放入队列，等待后续检查后对相应HK股票做AH股标记. liu yuguang 2016/Otc/14
		theApp.AddGoodsAH(this);
	}
	else if (m_cStation==4)
	{
		DWORD dw = m_dwNameCode/10000;
		if (dw==1 || (dw>=31 && dw<=60))
		{
			m_cGroup = 17;
			hMaskCode = GROUP_TF;
		}
		else if (dw>=61 && dw<=90)
		{
			hMaskCode = GROUP_WHQH;
		}
		else if (dw<99)
		{
			m_cGroup = 15;
			hMaskCode = GROUP_GZQH;
		}
		else
		{
			CString strCode = m_pcNameCode;
			CString strType1 = strCode.Left(1);
			CString strType2 = strCode.Left(2);

			if (strType1=="T")
			{
				m_cGroup = 17;
				hMaskCode = GROUP_TF;
			}
			else if (strType2=="IO" || strType2=="HO")
			{
				hMaskCode = GROUP_OPTIONS;
			}
			else if (strType1=="I")
			{
				m_cGroup = 15;
				hMaskCode = GROUP_GZQH;
			}
			else
			{
				hMaskCode = GROUP_WHQH;
			}
		}

		dw = GetID()/10000;
		if( dw == 400 || dw == 402 || dw == 403 ){
			hMaskCode = GROUP_GZQH;
		}
		if( dw == 401 ){
			hMaskCode = GROUP_TF;
		}
	}
	else if (m_cStation==5)
	{
		m_cGroup = 16;
		hMaskCode = GROUP_HKZB;
	}
	else if (m_cStation==6)
	{
		m_cGroup = 17;
		hMaskCode = GROUP_WI;
	}
	else if (m_cStation==7)
	{
		int n = m_dwNameCode/10000;
		if (n==1)
			hMaskCode = GROUP_SHFE;
		else if (n==2)
			hMaskCode = GROUP_DCE;
		else if (n==3)
			hMaskCode = GROUP_CZCE;
	}
	else if (m_cStation==8)
		hMaskCode = GROUP_WPQH;
	else if (m_cStation==9 || m_cStation==10)
	{
		hMaskCode = GROUP_US;
	}
	else if (m_cStation==11)
		hMaskCode = GROUP_WH;
	else
		m_cGroup = 99;

	if (m_cStation==0)
	{
		if (hMaskName & GROUP_SH_Z)
		{
			//上海退市整理和ST不计入主板
		}
		else if ((hMaskName & GROUP_SH_ST) /*&& g_bSHSTinA==FALSE */) //on pc side, g_bSHSTinA is an option gotten by GetProfileInt("System", "SHSTinA", 0);
		{
			//上海ST不计入主板
		}
		else if (m_cSecurityType<-1)
		{
			if (IsIndex())
				hMaskCode = GROUP_SH_INDEX;
			else if (m_dwNameCode>=600000 && m_dwNameCode<=699999)
				hMaskCode = GROUP_SH_A;
			else if (m_dwNameCode>=900000 && m_dwNameCode<=900999)
				hMaskCode = GROUP_SH_B;
	//		else if (m_dwNameCode>=580000 && m_dwNameCode<=589999)
	//			hMaskCode = GROUP_QZ;
			else if (m_dwNameCode>=500000 && m_dwNameCode<=599999)
				hMaskCode = GROUP_SH_JJ;
	//		else if (m_dwNameCode>=30 && m_dwNameCode<=99999)
	//			hMaskCode = GROUP_SH_ZQ;
			else if (m_dwNameCode>=120000 && m_dwNameCode<=129999)
				hMaskCode = GROUP_SH_ZQ;
			else if (m_dwNameCode>=100000 && m_dwNameCode<=119999)
				hMaskCode = GROUP_SH_ZZ;
			else if (m_dwNameCode>=201000 && m_dwNameCode<=204999)
				hMaskCode = GROUP_SH_HG;
			else if (m_dwNameCode>=751800 && m_dwNameCode<=751899)
				hMaskCode = GROUP_SH_WIT;
			else if (m_dwNameCode>=1000001)
				hMaskCode = GROUP_OPTIONS;
			else
				hMaskCode = GROUP_SH_OTHER;
		}
		else
		{
			if (m_cSecurityType==INDEX)
				hMaskCode = GROUP_SH_INDEX;
			else if (m_cSecurityType==EQUITY)
			{
				if (m_dwNameCode>=900000)
					hMaskCode = GROUP_SH_B;
				else
					hMaskCode = GROUP_SH_A;
			}
			else if (m_cSecurityType==FUND)
				hMaskCode = GROUP_SH_JJ;
			else if (m_cSecurityType==BOND)
				hMaskCode = GROUP_SH_ZQ;
			else if (m_cSecurityType==CONVERTIBLE)
				hMaskCode = GROUP_SH_ZZ;
			else if (m_cSecurityType==REPO)
				hMaskCode = GROUP_SH_HG;
			else if (m_cSecurityType==WIT)
				hMaskCode = GROUP_SH_WIT;
			else if (m_cSecurityType==OPTIONS)
				hMaskCode = GROUP_OPTIONS;
			else
				hMaskCode = GROUP_SH_OTHER;
		}
	}
	else if (m_cStation==1)
	{
		if (m_cSecurityProperties=='Z')
		{
			//深圳退市整理不计入主板
			hMaskCode = GROUP_SZ_Z;
		}
		else
		{
			if (m_cSecurityType<-1)
			{
				long l = m_dwNameCode/10000;
				long n = m_dwNameCode/100000;

				if (IsIndex())
					hMaskCode = GROUP_SZ_INDEX;
				else if (l==0)
				{
					if (m_dwNameCode>=2001 && m_dwNameCode<=2999)
						hMaskCode = GROUP_SZ_ZXB;
					else
						hMaskCode = GROUP_SZ_A;
				}
				else if (l==20)
					hMaskCode = GROUP_SZ_B;
				else if (l==30)
					hMaskCode = GROUP_SZ_CYB;
				else if (l>=15 && l<=18)
					hMaskCode = GROUP_SZ_JJ;
				else if (l==10 || l==11)
					hMaskCode = GROUP_SZ_ZQ;
				else if (l==12)
					hMaskCode = GROUP_SZ_ZZ;
				else if (l==13)
					hMaskCode = GROUP_SZ_HG;
		//		else if (l==3)
		//			hMaskCode = GROUP_QZ;
				else if(n == 4 || n == 8)
				{
					hMaskCode = GROUP_SZ_STB;

					if (l==40 || l==42)
						hMaskCode |= GROUP_GZXT1;
					else if( l == 43 || l == 83 )
						hMaskCode |= GROUP_SZ_XSB;
					else
						hMaskCode |= GROUP_GZXT2;
				}
				else
					hMaskCode = GROUP_SZ_OTHER;
			
			}
			else
			{
				if (m_cSecurityType==INDEX)
					hMaskCode = GROUP_SZ_INDEX;
				else if (m_cSecurityType==EQUITY)
				{
					DWORD dw3 = m_dwNameCode/1000;
					if (dw3>=2 && dw3<=4)	// 2-4 中小企业板股票
						hMaskCode = GROUP_SZ_ZXB;
					else if (dw3>=200 && dw3<=209)
						hMaskCode = GROUP_SZ_B;
					else if (dw3>=300 && dw3<=309)
						hMaskCode = GROUP_SZ_CYB;
					else
						hMaskCode = GROUP_SZ_A;
				}
				else if (m_cSecurityType==FUND)
					hMaskCode = GROUP_SZ_JJ;
				else if (m_cSecurityType==BOND)
					hMaskCode = GROUP_SZ_ZQ;
				else if (m_cSecurityType==CONVERTIBLE)
					hMaskCode = GROUP_SZ_ZZ;
				else if (m_cSecurityType==REPO)
					hMaskCode = GROUP_SZ_HG;
				else
					hMaskCode = GROUP_SZ_OTHER;
			}
		}
	}

	if (m_pcPreName[3]=='C')
		hMaskCode |= GROUP_CEF;
	else if (m_pcPreName[3]=='F')
		hMaskCode |= GROUP_OEF;
	else if (m_pcPreName[3]=='E')
		hMaskCode |= GROUP_ETF;
	else if (m_pcPreName[3]=='L')
		hMaskCode |= GROUP_LOF;
/*
	else if (hMaskCode & GROUP_SH_JJ)
	{
		DWORD dw3 = m_dwNameCode/1000;

		if (dw3==500)
			hMaskCode |= GROUP_CEF;
		else if (dw3==510)
			hMaskCode |= GROUP_ETF;
		else if (dw3==519)
			hMaskCode |= GROUP_OEF;
	}
	else if (hMaskCode & GROUP_SZ_JJ)
	{
		DWORD dw3 = m_dwNameCode/1000;

		if (dw3==184)
			hMaskCode |= GROUP_CEF;
		else if (dw3==159)
			hMaskCode |= GROUP_ETF;
		else
			hMaskCode |= GROUP_OEF;
	}
*/
/*
	if (m_cStation==0 && m_dwNameCode==300)
		hMaskCode |= GROUP_GZQH;
*/
	m_hMaskGroup = hMaskCode|hMaskName;
}


CString CGoods::GetString()
{
	CString str;
	str.Format("%06ld", m_dwNameCode);
	return str;
}

void CGoods::Read(CBuffer& buf, WORD wVersion)
{
	//if (m_rwGood.TryAcquireWriteLock() == 0)
	//{
	// 更换锁 [3/5/2015 frant.qian]
	RWLock_scope_wrlock  rwlock(this->m_rwGood);	
	this->m_rwGoodcount[1] += 1;
	try
	{
		m_cLevel = buf.ReadChar();
		//Static
		m_dwDate = buf.ReadInt();
		m_dwTime = buf.ReadInt();
		m_dwDateSave = buf.ReadInt();
		m_dwA_H = buf.ReadInt();

		buf.Read(m_pcName, LEN_STOCKNAME);
		buf.Read(m_pcCode, LEN_STOCKCODE);

		m_sTimeZone = buf.ReadShort();

		m_tradeSession.m_cNum = buf.ReadChar();
		if (m_tradeSession.m_cNum>0 && m_tradeSession.m_cNum<=MAX_TRADING_SESSION)
		{
			for (int c=0; c<m_tradeSession.m_cNum; c++)
			{
				m_tradeSession.m_pwTimeBegin[c] = buf.ReadShort();
				m_tradeSession.m_pwTimeEnd[c] = buf.ReadShort();
			}
		}
		else
			m_tradeSession.m_cNum = 0;

		m_cPriceDiv = buf.ReadChar();
		m_dwPriceDiv = g_CheckDiv(m_cPriceDiv);
		
		m_cDecimalPosition = buf.ReadChar();

		m_dwClose = buf.ReadInt();

		m_xLTG = buf.ReadXInt();
		m_xZFX = buf.ReadXInt();

		m_dwPriceZT = buf.ReadInt();
		m_dwPriceDT = buf.ReadInt();

		m_dwFirstDate = buf.ReadInt();

		m_dwPClose = buf.ReadInt();
		m_dwWClose = buf.ReadInt();
		m_dw5Close = buf.ReadInt();

		m_xAveVol5 = buf.ReadXInt();
		m_xSumVol4 = buf.ReadXInt();

		m_xSumBigAmt4 = buf.ReadXInt();
		//Static

		//Dynamic
		m_dwNorminal = buf.ReadInt();

		m_dwOpen = buf.ReadInt();
		m_dwHigh = buf.ReadInt();
		m_dwLow = buf.ReadInt();
		m_dwPrice = buf.ReadInt();

		m_xVolume = buf.ReadXInt();
		m_xAmount = buf.ReadXInt();

		m_dwTradeNum = buf.ReadInt();

		buf.Read(m_pdwMMP, 4*20);
		buf.Read(m_pxMMPVol, 4*20);

		m_dwBuyPrice = buf.ReadInt();
		m_dwSellPrice = buf.ReadInt();

		m_xBuyVol = buf.ReadXInt();
		m_xSellVol = buf.ReadXInt();

		m_xNeiPan = buf.ReadXInt();
		m_xCurVol = buf.ReadXInt();
		m_cCurVol = buf.ReadChar();

		m_nStrong = buf.ReadInt();
		//Dynamic

		//Matrix
		m_matrix.Read(buf);
		//Matrix

		m_dwCountStatic = buf.ReadInt();
		m_dwCountDynamic = buf.ReadInt();
		m_dwCountMinute = buf.ReadInt();
		m_dwCountPartOrder = buf.ReadInt();

		m_dwCountStatus = buf.ReadInt();

		buf.ReadString(m_strEName);
		buf.Read(m_pcPreName, 4);
		m_cTradeCode = buf.ReadChar();

		m_wGroupHY = buf.ReadShort();
		m_bZLZC4 = buf.ReadChar();
		m_bZLZC9 = buf.ReadChar();
		m_bZLZC19 = buf.ReadChar();
		m_dwClose9 = buf.ReadInt();
		m_dwClose19 = buf.ReadInt();

		m_dwPreSettlement = buf.ReadInt();
		m_xPreOpenInterest = buf.ReadXInt();

		m_xOpenInterest = buf.ReadXInt();

		m_xCurOI = buf.ReadXInt();

		m_dwSettlement = buf.ReadInt();

		m_bZLQM = buf.ReadChar();

		m_cPool1 = buf.ReadChar();
		m_dwPricePool1 = buf.ReadInt();
		m_dwDatePool1 = buf.ReadInt();

		m_nCPX_Day = buf.ReadInt();
		m_nCPX_Min60 = buf.ReadInt();

		m_dwPricePool1_2 = buf.ReadInt();
		m_dwDatePool1_2 = buf.ReadInt();

		m_dwHighPool1_2 = buf.ReadInt();

		m_cPool2 = buf.ReadChar();
		m_dwDatePool2 = buf.ReadInt();
		m_dwPricePool2 = buf.ReadInt();
		m_dwDatePool2_2 = buf.ReadInt();
		m_dwPricePool2_2 = buf.ReadInt();
		m_dwHighPool2_2 = buf.ReadInt();

		m_dwCountPool2++;

		m_wGroupDQ = buf.ReadShort();

		short sGN = buf.ReadShort();
		if (sGN>0 && sGN<128)
		{
			for (short s=0; s<sGN; s++)
			{
				WORD wGN = buf.ReadShort();
				m_aGroupGN.Add(wGN);
			}
		}

		m_dwI_Num = buf.ReadInt();
		if (m_dwI_Num>0)
		{
			m_dwI_Close = buf.ReadInt();
			m_dwI_Ave = buf.ReadInt();
			m_dwI_Amount = buf.ReadInt();
			m_dwI_AveGB = buf.ReadInt();
			m_dwI_SumSZ = buf.ReadInt();
			m_dwI_Percent = buf.ReadInt();
			m_dwI_SYL = buf.ReadInt();
			m_dwI_JBBS = buf.ReadInt();
		}

		char cIndex = buf.ReadChar();
		if (cIndex)
		{
			if (m_pIndex == NULL)
				m_pIndex = new CIndex;

			if (m_pIndex)
				m_pIndex->Read(buf);
			else
			{
				CIndex index;
				index.Read(buf);
			}
		}

		m_nPriceDiv = buf.ReadInt();

		m_nBKStrong = buf.ReadInt();

		buf.Read(&m_DayStat, sizeof(CDayStatData));

		m_dwCPX_Close = buf.ReadInt();

		buf.Read(&m_XGStat, sizeof(m_XGStat));

		m_xSumAmt4 = buf.ReadXInt();
		m_xSumBigVol4 = buf.ReadXInt();

		m_nCPX_Week = buf.ReadInt();
		m_nCPX_Month = buf.ReadInt();

		m_nCPX_Min1 = buf.ReadInt();
		m_nCPX_Min5 = buf.ReadInt();
		m_nCPX_Min15 = buf.ReadInt();
		m_nCPX_Min30 = buf.ReadInt();

	}
	catch(...)
	{
		LOG_ERR("CGoods::Read fail!!");
	}

	Goods2Group();
	MakePY();

	m_strPreName = GetPreName();
	m_strHYName = GetHYName();

	ZeroMemory(m_aDynamicValue, sizeof(m_aDynamicValue));	// dynaCompress
	SaveNewDynamicValue();		// dynaCompress

	CheckMainTrack();

	this->m_rwGoodcount[1] -= 1;
		//m_rwGood.ReleaseWriteLock();
	//}
	//else
	//	LOG_DBG("%d %s AcquireWriteLock faild !!!", GetID(), GetName().GetData());
}

void CGoods::Write(CBuffer& buf, WORD wVersion)
{
	//if (m_rwGood.TryAcquireReadLock() == 0)
	//{
	// 更换锁 [3/5/2015 frant.qian]
	RWLock_scope_rdlock  rdlock(this->m_rwGood);
	this->m_rwGoodcount[2] += 1;
		try
		{
			buf.WriteChar(m_cLevel);

			//Static
			buf.WriteInt(m_dwDate);
			buf.WriteInt(m_dwTime);
			buf.WriteInt(m_dwDateSave);

			buf.WriteInt(m_dwA_H);

			buf.Write(m_pcName, LEN_STOCKNAME);
			buf.Write(m_pcCode, LEN_STOCKCODE);

			buf.WriteShort(m_sTimeZone);
			if (m_tradeSession.m_cNum<0 || m_tradeSession.m_cNum>MAX_TRADING_SESSION)
				m_tradeSession.m_cNum = 0;
			buf.WriteChar(m_tradeSession.m_cNum);
			for (int c=0; c<m_tradeSession.m_cNum; c++)
			{
				buf.WriteShort(m_tradeSession.m_pwTimeBegin[c]);
				buf.WriteShort(m_tradeSession.m_pwTimeEnd[c]);
			}

			buf.WriteChar(m_cPriceDiv);

			buf.WriteChar(m_cDecimalPosition);

			buf.WriteInt(m_dwClose);

			buf.WriteXInt(m_xLTG);
			buf.WriteXInt(m_xZFX);

			buf.WriteInt(m_dwPriceZT);
			buf.WriteInt(m_dwPriceDT);

			buf.WriteInt(m_dwFirstDate);

			buf.WriteInt(m_dwPClose);
			buf.WriteInt(m_dwWClose);
			buf.WriteInt(m_dw5Close);

			buf.WriteXInt(m_xAveVol5);
			buf.WriteXInt(m_xSumVol4);

			buf.WriteXInt(m_xSumBigAmt4);
			//Static

			//Dynamic
			buf.WriteInt(m_dwNorminal);

			buf.WriteInt(m_dwOpen);
			buf.WriteInt(m_dwHigh);
			buf.WriteInt(m_dwLow);
			buf.WriteInt(m_dwPrice);

			buf.WriteInt(m_xVolume);
			buf.WriteInt(m_xAmount);

			buf.WriteInt(m_dwTradeNum);

			buf.Write(m_pdwMMP, 4*20);
			buf.Write(m_pxMMPVol, 4*20);

			buf.WriteInt(m_dwBuyPrice);
			buf.WriteInt(m_dwSellPrice);

			buf.WriteXInt(m_xBuyVol);
			buf.WriteXInt(m_xSellVol);

			buf.WriteXInt(m_xNeiPan);
			buf.WriteXInt(m_xCurVol);
			buf.WriteChar(m_cCurVol);

			buf.WriteInt(m_nStrong);
			//Dynamic

			//Matrix
			m_matrix.Write(buf);
			//Matrix

			buf.WriteInt(m_dwCountStatic);
			buf.WriteInt(m_dwCountDynamic);
			buf.WriteInt(m_dwCountMinute);
			buf.WriteInt(m_dwCountPartOrder);

			buf.WriteInt(m_dwCountStatus);

			buf.WriteString(m_strEName);
			buf.Write(m_pcPreName, 4);
			buf.WriteChar(m_cTradeCode);

			buf.WriteShort(m_wGroupHY);
			buf.WriteChar(m_bZLZC4);
			buf.WriteChar(m_bZLZC9);
			buf.WriteChar(m_bZLZC19);
			buf.WriteInt(m_dwClose9);
			buf.WriteInt(m_dwClose19);

			buf.WriteInt(m_dwPreSettlement);
			buf.WriteXInt(m_xPreOpenInterest);

			buf.WriteXInt(m_xOpenInterest);

			buf.WriteXInt(m_xCurOI);

			buf.WriteInt(m_dwSettlement);

			buf.WriteChar(m_bZLQM);

			buf.WriteChar(m_cPool1);
			buf.WriteInt(m_dwPricePool1);
			buf.WriteInt(m_dwDatePool1);

			buf.WriteInt(m_nCPX_Day);
			buf.WriteInt(m_nCPX_Min60);

			buf.WriteInt(m_dwPricePool1_2);
			buf.WriteInt(m_dwDatePool1_2);

			buf.WriteInt(m_dwHighPool1_2);

			buf.WriteChar(m_cPool2);
			buf.WriteInt(m_dwDatePool2);
			buf.WriteInt(m_dwPricePool2);
			buf.WriteInt(m_dwDatePool2_2);
			buf.WriteInt(m_dwPricePool2_2);
			buf.WriteInt(m_dwHighPool2_2);

			buf.WriteShort(m_wGroupDQ);

			short sGN = (short)m_aGroupGN.GetSize();
			if (sGN>0 && sGN<128)
			{
				buf.WriteShort(sGN);
				for (short s=0; s<sGN; s++)
					buf.WriteShort(m_aGroupGN[s]);
			}
			else
				buf.WriteShort(0);

			buf.WriteInt(m_dwI_Num);
			if (m_dwI_Num>0)
			{
				buf.WriteInt(m_dwI_Close);
				buf.WriteInt(m_dwI_Ave);
				buf.WriteInt(m_dwI_Amount);
				buf.WriteInt(m_dwI_AveGB);
				buf.WriteInt(m_dwI_SumSZ);
				buf.WriteInt(m_dwI_Percent);
				buf.WriteInt(m_dwI_SYL);
				buf.WriteInt(m_dwI_JBBS);
			}

			if (m_pIndex)
			{
				buf.WriteChar(1);
				m_pIndex->Write(buf);
			}
			else
				buf.WriteChar(0);

			buf.WriteInt(m_nPriceDiv);

			buf.WriteInt(m_nBKStrong);

			buf.Write(&m_DayStat, sizeof(CDayStatData));

			buf.WriteInt(m_dwCPX_Close);

			buf.Write(&m_XGStat, sizeof(m_XGStat));

			buf.WriteXInt(m_xSumAmt4);
			buf.WriteXInt(m_xSumBigVol4);

			buf.WriteInt(m_nCPX_Week);
			buf.WriteInt(m_nCPX_Month);

			buf.WriteInt(m_nCPX_Min1);
			buf.WriteInt(m_nCPX_Min5);
			buf.WriteInt(m_nCPX_Min15);
			buf.WriteInt(m_nCPX_Min30);
		}
		catch(...)
		{
			LOG_ERR("CGoods::Write faild !!!");
		}
	this->m_rwGoodcount[2] -= 1;
	//	m_rwGood.ReleaseReadLock();
	//}
	//else
	//	LOG_DBG("%d %s AcquireReadLock faild !!!", GetID(), GetName().GetData());
}


void CGoods::ClearBargain(BOOL bClearFile, DWORD dwDate)
{
	//if (m_dwBargainTotal!=0)
	//	int iii = 0;

	m_wBargain = 0;
	m_dwBargainTotal = 0;
	m_cBargainCheckSum = 0;
	m_dwBargainCheckSum = 0;
	m_dwDateBargain = dwDate;

	WriteBargainParam();

	if (bClearFile)
	{
		CDataFile_Bargain* pDF = theApp.GetBargainFile(m_cStation, m_dwNameCode);
		if (pDF)
			pDF->ClearData(GetGoodsCode());
	}

	if (m_pDataBargain){
		m_pDataBargain->ClearData();
		m_pDataBargain->m_dwDateOfGoods = m_dwDateBargain;
	}
}


void CGoods::AddBargain(CBargain& bar)
{
	if (m_pBargainBuffer==NULL)
		return;

	if( m_pBargainBuffer )
	{
		ASSERT(m_wBargain<=m_wBargainBuffer);
			
		if (m_wBargain==m_wBargainBuffer)
		{
			CDataFile_Bargain* pDF = theApp.GetBargainFile(m_cStation, m_dwNameCode);
			if (pDF)
				pDF->SaveData(GetGoodsCode(), m_pBargainBuffer, m_wBargain, m_dwBargainTotal, TRUE);
			m_wBargain = 0;
		}

		m_pBargainBuffer[m_wBargain++] = bar;
		m_dwBargainTotal++;

		m_cBargainCheckSum += bar.GetCheckSum(1);
		m_dwBargainCheckSum += bar.GetCheckSumDW(1);

		WriteBargainParam();
	}

	if (m_pDataBargain && m_pDataBargain->m_cLoadOK==1)
		((CDataBargain*)m_pDataBargain)->AddData(bar);

	//DWORD dwGoodsID = GetID();
	DWORD _dwGoodsID = GetID();
	DWORD dwYMD = theApp.g_dwSysYMD % 10000 * 1000000;
}


void CGoods::ReadBargainParam()
{
	if (m_pcBargainParam)
	{
		CopyMemory(&m_wBargain, m_pcBargainParam, 2);
		CopyMemory(&m_dwBargainTotal, m_pcBargainParam+2, 4);
		CopyMemory(&m_cBargainCheckSum, m_pcBargainParam+6, 1);
		CopyMemory(&m_dwDateBargain, m_pcBargainParam+7, 4);
		CopyMemory(&m_dwBargainCheckSum, m_pcBargainParam+11, 4);
	}
}

void CGoods::WriteBargainParam()
{
	if (m_pcBargainParam)
	{
		CopyMemory(m_pcBargainParam, &m_wBargain, 2);
		CopyMemory(m_pcBargainParam+2, &m_dwBargainTotal, 4);
		CopyMemory(m_pcBargainParam+6, &m_cBargainCheckSum, 1);
		CopyMemory(m_pcBargainParam+7, &m_dwDateBargain, 4);
		CopyMemory(m_pcBargainParam+11, &m_dwBargainCheckSum, 4);
	}
}


void CGoods::SaveBuffer()
{
	CGoodsCode gcGoodsID = GetGoodsCode();

	if (m_pBargainBuffer && m_wBargain>0)
	{
		CDataFile_Bargain* pDF = theApp.GetBargainFile(m_cStation, m_dwNameCode);
		if (pDF)
			pDF->SaveData(gcGoodsID, m_pBargainBuffer, m_wBargain, m_dwBargainTotal, TRUE);
		m_wBargain = 0;
		WriteBargainParam();
	}
}

void CGoods::AddMinutes(CSortArrayMinute& aMinute)
{
	int nSizeNew = (int)aMinute.GetSize();

	// 增加上证指数的LOG [5/5/2014 %qianyifan%]
	//if (this->GetID() == 1)
	//{ 
	//	LOG_DBG("AddMinutes() for 1");
	//}
	//else if (this->GetID() == 1399001)
	//{ 
	//	LOG_DBG("AddMinutes() for 1399001");
	//}
	if (nSizeNew==0)
		return;

	//DWORD _dwGoodsID = GetID();
	//DWORD dwYMD = theApp.g_dwSysHMS % 100;// * 1000000;
	//DWORD dwTime = dwYMD + (aMinute[nSizeNew-1].m_wTime%100000000)*100;

	//////////////////////////////////////////
	//if (nSizeNew>10)
	//	int iii = 0;
	CGoodsCode gcGoodsID = GetGoodsCode();

	if (m_pMinuteBuffer && m_wMinuteBuffer>0)
	{
		CSortArrayMinute aMin;   //获取当天的分钟线

		int i = 0;
		WORD w = 0;

		for (w = 0; w < m_wMinute; w++)
			aMin.Add(m_pMinuteBuffer[w]);

		for (i=0; i<nSizeNew; i++)
			aMin.Change(aMinute[i]);   //比对，更新成最新的分钟线

		int nNumMin = (int)aMin.GetSize();
		if (nNumMin>0)
		{
			if(aMin[0].m_wTime <= m_wLastMinInDisk || nNumMin >= m_wMinuteBuffer)
			{
				CSortArrayMinute aMinInDisk;
				CDataFile_Minute* pDF = theApp.GetMinuteFile(m_cStation, m_dwNameCode);
				if( pDF ){
					pDF->LoadData(gcGoodsID, aMinInDisk);
				}

				for (w=0; w<nNumMin; w++)
					aMinInDisk.Change(aMin[w]);  //把最新的MERGE 到磁盘数据

				int nNumInDisk = (int)aMinInDisk.GetSize();

				m_pMinuteBuffer[0] = aMinInDisk[nNumInDisk-1];
				m_wMinute = 1;

				aMinInDisk.RemoveAt(nNumInDisk-1);
				nNumInDisk--;

				if (nNumInDisk>0)
				{
					m_wMinuteInDisk = (WORD)nNumInDisk;
					CDataFile_Minute* pDF = theApp.GetMinuteFile(m_cStation, m_dwNameCode);
					if(pDF)
						pDF->SaveData(gcGoodsID, aMinInDisk, 0, FALSE);

					m_wLastMinInDisk = aMinInDisk[nNumInDisk-1].m_wTime;

					BOOL bIsGZQH = IsGZQH();
					BOOL bIndex = (m_pIndex != NULL);
					BOOL bIsLevel2 = (m_cLevel == 2);

					m_cMinuteCheckSumInDisk = 0;
					m_dwMinuteCheckSumInDisk = 0;
					for (i=0; i<nNumInDisk; i++)
					{
						CMinute& min = aMinInDisk[i];
						m_cMinuteCheckSumInDisk += min.GetCheckSum(bIsGZQH, bIndex, bIsLevel2, 1);
						m_dwMinuteCheckSumInDisk += min.GetCheckSumDW(bIsGZQH, bIndex, bIsLevel2, 1);
					}
				}
			}
			else
			{
				CopyMemory(m_pMinuteBuffer, aMin.GetData(), nNumMin*sizeof(CMinute));
				m_wMinute = nNumMin;
			}

			WriteMinuteParam();
		}
	}

	if (m_pDataMinute0 && m_pDataMinute0->m_cLoadOK==1)
		((CDataMinute*)m_pDataMinute0)->AddMinutes(aMinute);
	if (m_pDataMinute1 && m_pDataMinute1->m_cLoadOK==1)
		((CDataMinute*)m_pDataMinute1)->AddMinutes(aMinute);
	if (m_pDataMin30 && m_pDataMin30->m_cLoadOK==1)
		((CDataDay*)m_pDataMin30)->AddMinutes(aMinute);
	if (m_pDataMin5 && m_pDataMin5->m_cLoadOK==1)
		((CDataDay*)m_pDataMin5)->AddMinutes(aMinute);
	if (m_pDataMin1 && m_pDataMin1->m_cLoadOK==1)
		((CDataDay*)m_pDataMin1)->AddMinutes(aMinute);
	if (m_pDataHisMin && m_pDataHisMin->m_cLoadOK==1 )
		((CDataHisMin*)m_pDataHisMin)->AddMinutes(aMinute);
}

void l_ReloadData(CData* pData)
{
	if (pData && pData->m_cLoadOK)
	{
		pData->m_cLoadOK = 0;
		pData->m_lLoadThreadID = 0;
		pData->m_dwClearCount++;

		g_LoadData.AddData(pData);
	}
}


void CGoods::ClearMinute(BOOL bClearFile, DWORD dwDate)
{
	//if (m_wMinuteInDisk>0)
	//	int iii = 0;

	m_wMinute = 0;
	m_wMinuteInDisk = 0;
	m_wLastMinInDisk = 0;
	m_cMinuteCheckSumInDisk = 0;
	m_dwMinuteCheckSumInDisk = 0;
	m_dwDateMinute = dwDate;

	m_dwBargainCheckSum = 0;
	m_dwDateBargain = dwDate;

	WriteMinuteParam();	

	if (bClearFile)
	{
		CGoodsCode gcGoodsID = GetGoodsCode();
		CDataFile_Minute* pDF = theApp.GetMinuteFile(m_cStation, m_dwNameCode);
		if( pDF )
			pDF->ClearData(gcGoodsID);
	}

	if (m_pDataMinute0)
	{
		m_pDataMinute0->ClearData();
		m_pDataMinute0->m_dwDateOfGoods = m_dwDateMinute;
	}

	if (m_pDataMinute1)
	{
		m_pDataMinute1->ClearData();
		m_pDataMinute1->m_dwDateOfGoods = m_dwDateMinute;
	}
	
	if (dwDate>0)
	{		
		l_ReloadData(m_pDataMin30);
		l_ReloadData(m_pDataMin5);
		l_ReloadData(m_pDataMin1);

		//l_ReloadData(m_pDataMultiHisMin);
	}
	
}

void CGoods::ReadMinuteParam()
{
	if (m_pcMinuteParam)
	{
		CopyMemory(&m_wMinute, m_pcMinuteParam, 2);
		CopyMemory(&m_wMinuteInDisk, m_pcMinuteParam+2, 2);
		CopyMemory(&m_wLastMinInDisk, m_pcMinuteParam+4, 2);
		CopyMemory(&m_cMinuteCheckSumInDisk, m_pcMinuteParam+6, 1);
		CopyMemory(&m_dwDateMinute, m_pcMinuteParam+7, 4);
		CopyMemory(&m_dwMinuteCheckSumInDisk, m_pcMinuteParam+11, 4);
	}
}

void CGoods::WriteMinuteParam()
{
	if (m_pcMinuteParam)
	{
		CopyMemory(m_pcMinuteParam, &m_wMinute, 2);
		CopyMemory(m_pcMinuteParam+2, &m_wMinuteInDisk, 2);
		CopyMemory(m_pcMinuteParam+4, &m_wLastMinInDisk, 2);
		CopyMemory(m_pcMinuteParam+6, &m_cMinuteCheckSumInDisk, 1);
		CopyMemory(m_pcMinuteParam+7, &m_dwDateMinute, 4);
		CopyMemory(m_pcMinuteParam+11, &m_dwMinuteCheckSumInDisk, 4);
	}
}

void CGoods::GetMinutes(CSortArrayMinute& aMinute, BOOL bIncludeVirtual)
{
	aMinute.RemoveAt(0, aMinute.GetSize());

	
	CArrayMinute aMin;
	CDataFile_Minute* pDF = theApp.GetMinuteFile(m_cStation, m_dwNameCode);
	if( pDF ){
		pDF->LoadData(GetGoodsCode(), aMin);
	}

	int nSize = (int)aMin.GetSize();
	for (int i=0; i<nSize; i++)
		aMinute.Change(aMin[i]);

	for (int i=0; i<int(m_wMinute); i++)
		aMinute.Change(m_pMinuteBuffer[i]);

	if ((m_cStation==0 || m_cStation==1) && bIncludeVirtual==FALSE)
	{
		CTradeSession* pTS = theApp.GetTradeSession(m_cStation, FALSE);

		int nSize = (int)aMinute.GetSize();
		int nNum = 0;
		for (int i=0; i<nSize; i++)
		{
			if (short(aMinute[i].m_wTime%10000)<=pTS->m_pwTimeBegin[0])
				nNum++;
			else
				break;
		}
		if (nNum>0)
			aMinute.RemoveAt(0, nNum);
	}
}

void CGoods::AddDay()
{
	if (WantSaveToday(FALSE))
	{
		CDay day;
		GetToday(day);

		if (m_pDataDay && m_pDataDay->m_cLoadOK==1)
			((CDataDay*)m_pDataDay)->Add(day);
/*
		int nData = m_aData.GetSize();
		for (int i=0; i<nData; i++)
		{
			CData* p = (CData*)m_aData[i];
			if (p->m_wDataType==DAY_DATA)
				((CDataDay*)p)->Add(day);
		}
*/
	}
}

void CGoods::GetToday(CDay& day)
{
	WORD wAmountDiv = AmountDiv();

	day.Clear();

	day.m_dwTime = GetDateValue();
	if (m_dwOpen)
		day.m_dwOpen = m_dwOpen;
	else
		day.m_dwOpen = m_dwPrice;
	if (m_dwHigh)
		day.m_dwHigh = m_dwHigh;
	else
		day.m_dwHigh = m_dwPrice;
	if (m_dwLow)
		day.m_dwLow = m_dwLow;
	else
		day.m_dwLow = m_dwPrice;
	day.m_dwClose = m_dwPrice;

	if (IsGZQH())
		day.m_dwTradeNum = m_xOpenInterest.GetRawData();
	else
		day.m_dwTradeNum = m_dwTradeNum;

	day.m_xVolume = TransVol2Hand(m_xVolume);
	day.m_xAmount = m_xAmount/wAmountDiv;

	day.m_xNeiPan = TransVol2Hand(m_xNeiPan);

//	RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);

	for (int i=0; i<4; i++)
	{
		day.m_dwNumOfBuy += m_matrix.m_ocSumOrder.m_pdwNumOfBuy[i];
		day.m_dwNumOfSell += m_matrix.m_ocSumOrder.m_pdwNumOfSell[i];

		if (i>0)
		{
			day.m_pxVolOfBuy[i-1] = TransVol2Hand(m_matrix.m_ocSumOrder.m_pxVolOfBuy[i]);
			day.m_pxVolOfSell[i-1] = TransVol2Hand(m_matrix.m_ocSumOrder.m_pxVolOfSell[i]);

			day.m_pxAmtOfBuy[i-1] = m_matrix.m_ocSumOrder.m_pxAmtOfBuy[i]/wAmountDiv;
			day.m_pxAmtOfSell[i-1] = m_matrix.m_ocSumOrder.m_pxAmtOfSell[i]/wAmountDiv;
		}
	}

	if (m_pIndex)
	{
		day.m_wRise = m_pIndex->m_wRise;
		day.m_wFall = m_pIndex->m_wFall;
	}
}

BOOL CGoods::WantSaveToday(BOOL bSPZY)
{
	DWORD dwID = GetID();
	if (m_cStation>=5 || (dwID>=1160000 && dwID<=1169999) || (dwID>=519000 && dwID<=519999) || dwID/100000 == 11)
		return (m_dwPrice>0);
	else if (bSPZY)
		return (m_dwPrice>0 && m_xVolume>0);
	else
		return (m_dwPrice>0 && (m_xVolume>0 || (IsVirtual())));
}

BOOL CGoods::IsDayGood(CDay& day)
{
	DWORD dwID = GetID();
	if (m_cStation>=5 || (dwID>=1160000 && dwID<=1169999) || (dwID>=519000 && dwID<=519999) || dwID/100000 == 11)
		return (day.m_dwClose>0);
	else
		return (day.m_dwClose>0 && day.m_xVolume.m_nBase>0);
}

WORD CGoods::MinInDay()
{
	CTradeSession* pTS = CheckTS();
	if (pTS==NULL)
		return 0;

	WORD wSize = 0;
	for (int c=0; c<pTS->m_cNum; c++)
	{
		wSize += (pTS->m_pwTimeEnd[c]/100-pTS->m_pwTimeBegin[c]/100)*60+(pTS->m_pwTimeEnd[c]%100-pTS->m_pwTimeBegin[c]%100);
	}
	return wSize;
}

WORD CGoods::Time2Min(DWORD dwTime, BOOL bIncludeVirtual)
{
	CTradeSession* pTS = CheckTS(bIncludeVirtual);
	if (pTS==NULL)
		return 0;

	WORD wHour = WORD(dwTime/10000);
	WORD wMin = WORD(dwTime/100%100);
	WORD wSec = WORD(dwTime%100);

	if (wSec)
	{ //进到下一分钟
		if (wMin==59)
		{
			wHour++;
			wMin = 0;
		}
		else
			wMin++;
	}

	WORD wMinute = wHour*100+wMin;

	if (wMinute>=pTS->m_pwTimeEnd[pTS->m_cNum-1])
		return pTS->m_pwTimeEnd[pTS->m_cNum-1];
	else
	{
		for (int c=0; c<pTS->m_cNum; c++)
		{
			if (c==0)
			{
				if (wMinute<pTS->m_pwTimeBegin[c]+1)
					return pTS->m_pwTimeBegin[c]+1;
			}
			else
			{
				///20110815				if (wMinute<=pTS->m_pwTimeBegin[c])
				///20110815					return pTS->m_pwTimeEnd[c-1];
				///20110815
				if (wMinute<=pTS->m_pwTimeBegin[c])
				{
					if (wMinute==pTS->m_pwTimeBegin[c] && wSec==0)	//13:00:00特殊处理, 归到13:01
						return wMinute+1;
					else
						return pTS->m_pwTimeEnd[c-1];
				}
			}

			if (wMinute<=pTS->m_pwTimeEnd[c])
				return wMinute;
		}
	}

	return wMinute;
}

WORD CGoods::Min2Pos(WORD wMin, BOOL bIncludeVirtual)
{
	CTradeSession* pTS = CheckTS(bIncludeVirtual);
	if (pTS==NULL)
		return 0;

	WORD wPosSum = 0;
	int c = 0;
	for (c=0; c<pTS->m_cNum; c++)
	{
		if (c==0)
		{
			if (wMin<pTS->m_pwTimeBegin[c]+1)
				return wPosSum;
		}
		else
		{
			if (wMin<pTS->m_pwTimeBegin[c]+1)
				return wPosSum-1;
		}

		if (wMin<=pTS->m_pwTimeEnd[c])
			return wPosSum+(wMin/100-pTS->m_pwTimeBegin[c]/100)*60+(wMin%100-pTS->m_pwTimeBegin[c]%100)-1;

		wPosSum += (pTS->m_pwTimeEnd[c]/100-pTS->m_pwTimeBegin[c]/100)*60+(pTS->m_pwTimeEnd[c]%100-pTS->m_pwTimeBegin[c]%100);
	}

	if (c==pTS->m_cNum)
		return wPosSum-1;

	return 0;
}

WORD CGoods::Pos2Min(WORD wPos, BOOL bIncludeVirtual)
{
	CTradeSession* pTS = CheckTS(bIncludeVirtual);
	if (pTS==NULL)
		return 0;

	WORD wH = 0, wM = 0;

	WORD wPosSum = 0, wSize = 0;
	int c = 0;
	for (c=0; c<pTS->m_cNum; c++)
	{
		wSize = (pTS->m_pwTimeEnd[c]/100-pTS->m_pwTimeBegin[c]/100)*60+(pTS->m_pwTimeEnd[c]%100-pTS->m_pwTimeBegin[c]%100);

		if (wPos<wPosSum+wSize)
		{
			wH = pTS->m_pwTimeBegin[c]/100+(wPos-wPosSum+pTS->m_pwTimeBegin[c]%100+1)/60;
			wM = (wPos-wPosSum+pTS->m_pwTimeBegin[c]%100+1)%60;
			break;
		}

		wPosSum += wSize;
	}

	if (c==pTS->m_cNum)
	{
		wH = pTS->m_pwTimeEnd[pTS->m_cNum-1]/100;
		wM = pTS->m_pwTimeEnd[pTS->m_cNum-1]%100;
	}

	if (wM==60)
	{
		wH++;
		wM = 0;
	}

	return wH*100+wM;
}

BOOL CGoods::InSamePeriod(DWORD dwPrev, DWORD dwNow, WORD wDataType, WORD wPeriod)
{
	if (dwPrev==dwNow)
		return TRUE;
	else if (wDataType==DAY_DATA)
	{
		if (wPeriod==WEEK_PERIOD)
		{
			if (g_IsDateGood(dwPrev) && g_IsDateGood(dwNow))
			{
				struct tm tmPrev, tmNow;
				time_t ttPrev, ttNow;

				tmPrev.tm_year = dwPrev/10000 - 1900;
				tmPrev.tm_mon = dwPrev%10000/100 - 1;
				tmPrev.tm_mday = dwPrev%100;
				tmPrev.tm_hour = 0;
				tmPrev.tm_min = 0;
				tmPrev.tm_sec = 0;
				tmPrev.tm_isdst = 0;

				tmNow.tm_year = dwNow/10000 - 1900;
				tmNow.tm_mon = dwNow%10000/100 - 1;
				tmNow.tm_mday = dwNow%100;
				tmNow.tm_hour = 0;
				tmNow.tm_min = 0;
				tmNow.tm_sec = 0;
				tmNow.tm_isdst = 0;

				ttPrev = kernel_mktime(&tmPrev);
				ttNow = kernel_mktime(&tmNow);

				tmNow.tm_wday = CaculateWeekDay(tmNow.tm_year+1900,tmNow.tm_mon+1, tmNow.tm_mday);
				tmPrev.tm_wday = CaculateWeekDay(tmPrev.tm_year+1900,tmPrev.tm_mon+1, tmPrev.tm_mday);

				return (tmNow.tm_wday>tmPrev.tm_wday && ttNow-ttPrev<86400*7);
			}
		}
		else if (wPeriod==MONTH_PERIOD)
		{
			int nPrevMonth = int(dwPrev%10000/100);
			int nMonth = int(dwNow%10000/100);

			return nMonth==nPrevMonth;
		}
		else if (wPeriod==QUARTER_PERIOD)
		{
			int nPrevJi = int(dwPrev%10000/100-1)/3;
			int nJi = int(dwNow%10000/100-1)/3;

			return nJi==nPrevJi;
		}
		else if (wPeriod==HALF_PERIOD)
		{
			int nPrevJi = int(dwPrev%10000/100-1)/6;
			int nJi = int(dwNow%10000/100-1)/6;

			return nJi==nPrevJi;
		}
		else if (wPeriod==YEAR_PERIOD)
		{
			int nPrevYear = int(dwPrev/10000);
			int nYear = int(dwNow/10000);

			return nYear==nPrevYear;
		}
	}
	else if (wDataType==MIN_DATA)
	{
		if (wPeriod>1)
		{
			if (dwNow/10000==dwPrev/10000)
			{
				WORD wPosPrev = Min2Pos(WORD(dwPrev%10000), FALSE);
				WORD wPosNow = Min2Pos(WORD(dwNow%10000), FALSE);
				return wPosNow/wPeriod==wPosPrev/wPeriod;		// same period
			}
		}
	}

	return FALSE;
}

BOOL CGoods::IsIndex(DWORD dwGoodsID)
{
	return (dwGoodsID/1000==0 || dwGoodsID/1000==1399 || dwGoodsID/1000==5500);
}

BOOL CGoods::IsIndex()
{
	return CGoods::IsIndex(GetID());
}

BOOL CGoods::IsSHB(DWORD dwGoodsID)
{
	int n5 = dwGoodsID/100000;
	int n4 = dwGoodsID/10000;
	return ((n5==2 || n5==9 || n5==5) || ((n4>=115 && n4<=118) || n4==103 || n5==14));
}

BOOL CGoods::IsSHB()
{
	return CGoods::IsSHB(GetID());
}

BOOL CGoods::IsQZ(DWORD dwGoodsID)
{
	return (dwGoodsID/10000==58 || dwGoodsID/10000==103);
}

BOOL CGoods::IsQZ()
{
	return CGoods::IsQZ(GetID());
}

BOOL CGoods::IsSHZQ(DWORD dwGoodsID)
{
	return (dwGoodsID>=1000 && dwGoodsID<=299999);
}

BOOL CGoods::IsSHZQ()
{
	return CGoods::IsSHZQ(GetID());
}

BOOL CGoods::IsSZZQ(DWORD dwGoodsID)
{
	//return (dwGoodsID>=1100000 && dwGoodsID<1400000);
	return (dwGoodsID>=1100000 && dwGoodsID<1140000);//20110921
}

BOOL CGoods::IsSZZQ()
{
	return CGoods::IsSZZQ(GetID());
}

BOOL CGoods::IsHK()
{
	return m_cStation==3 || m_cStation==5;
}

BOOL CGoods::IsGZQH(DWORD dwGoodsID)
{
	return (dwGoodsID/1000000==4);
}

BOOL CGoods::IsGZQH()
{
	return m_cStation==4;
}

BOOL CGoods::IsIF(DWORD dwGoodsID)
{
	return (dwGoodsID/10000==400);
}

BOOL CGoods::IsIF()
{
	return (m_cStation==4 && m_dwNameCode/10000==0);
}

BOOL CGoods::IsTF(DWORD dwGoodsID)
{
	return (dwGoodsID/10000==401);
}

BOOL CGoods::IsTF()
{
	return (m_cStation==4 && m_dwNameCode/10000==1);
}

BOOL CGoods::IsT(DWORD dwGoodsID)
{
	return (dwGoodsID/10000==431);
}

BOOL CGoods::IsT()
{
	return (m_cStation==4 && m_dwNameCode/10000==31);
}


BOOL CGoods::IsIC()
{
	DWORD id = GetID();
	if( id/10000 == 403 ){
		return TRUE;
	}
	return FALSE;
}

BOOL CGoods::IsIH()
{
	DWORD id = GetID();
	if( id/10000 == 402 ){
		return TRUE;
	}
	return FALSE;
}

BOOL CGoods::IsWorldIndex(DWORD dwGoodsID)
{
	return (dwGoodsID/1000000==6);
}

BOOL CGoods::IsWorldIndex()
{
	return m_cStation==6;
}

BOOL CGoods::IsChinaA(DWORD dwGoodsID)
{
	return  (dwGoodsID>=600000 && dwGoodsID<=699999) ||		// 上证Ａ股
		(dwGoodsID>=1000001 && dwGoodsID<=1009999) ||	// 深证Ａ股/中小板
		(dwGoodsID>=1300001 && dwGoodsID<=1309999);		// 创业板
}

BOOL CGoods::IsChinaB(DWORD dwGoodsID)
{
	return  (dwGoodsID>=900000 && dwGoodsID<=999999) ||		// 上证Ｂ股
		(dwGoodsID>=1200001 && dwGoodsID<=1209999);		// 深证Ｂ股
}

BOOL CGoods::IsChinaJJ(DWORD dwGoodsID)
{
	return  (dwGoodsID>=500000 && dwGoodsID<=579999) ||		// 上证基金
		(dwGoodsID>=1150001 && dwGoodsID<=1189999);		// 深证基金
}

BOOL CGoods::IsJJ()
{
	return (m_cGroup == 5 || m_cGroup == 6);
}

BOOL CGoods::IsZQ()
{
	return (m_cGroup == 7 || m_cGroup == 8);
}

INT64 CGoods::TransVol2Hand( INT64 hVolume, DWORD dwGoodsID )
{
	if (IsGZQH(dwGoodsID) || IsIndex(dwGoodsID) || IsSHZQ(dwGoodsID))
		return hVolume;
	else if (IsSZZQ(dwGoodsID))
		return (hVolume+5)/10;
	else
		return (hVolume+50)/100;
}

double CGoods::Long2Float( DWORD dwGoodsID, short sID, INT64 hValue )
{
	double fValue;
	switch (sID)
	{
	case CLOSE:
	case OPEN:
	case PRICE:
	case HIGH:
	case LOW:
	case PBUY1:
	case PBUY2:
	case PBUY3:
	case PBUY4:
	case PBUY5:
	case PBUY6:
	case PBUY7:
	case PBUY8:
	case PBUY9:
	case PBUY10:
	case PSELL1:
	case PSELL2:
	case PSELL3:
	case PSELL4:
	case PSELL5:
	case PSELL6:
	case PSELL7:
	case PSELL8:
	case PSELL9:
	case PSELL10:
	case AVEPRICE:
	case ZHANGDIE:
	case ZHANGSU:
	case PBUY:
	case PSELL:
	case NORMINAL:
	case P_ZT://  [10/15/2013 nidalei]
	case P_DT://  [10/15/2013 nidalei]
		// 单位：元
		fValue = hValue/1000.0;
		break;
	case LIANGBI:
	case WEIBI1:
	case ZHENFU:
	case ZDF:
	case ZDF5:
	case WZDF:
	case PZDF:
	case ZDF10:
	case ZDF20:
	case SYL:
	case TTM_SYL:
	case HSL:
	case HSL5:
	case SJL:
		fValue = hValue/100.0;
		break;
	case VOLUME:
	case VBUY1:
	case VBUY2:
	case VBUY3:
	case VBUY4:
	case VBUY5:
	case VBUY6:
	case VBUY7:
	case VBUY8:
	case VBUY9:
	case VBUY10:
	case VSELL1:
	case VSELL2:
	case VSELL3:
	case VSELL4:
	case VSELL5:
	case VSELL6:
	case VSELL7:
	case VSELL8:
	case VSELL9:
	case VSELL10:
	case NEIPAN:
	case WAIPAN:
	case VBUY:
	case VSELL:
	case CURVOL:
	case AVEVOL:
		// 单位：手
		fValue = CGoods::TransVol2Hand(hValue, dwGoodsID)/1.0;
		break;
	case AMOUNT:
		// 单位：元
		fValue = hValue/1.0/CGoods::AmountDiv(dwGoodsID);
		break;
	case DDBL:
	case DDBL5:
	case STRONG:
		// 单位：百分比
		fValue = hValue/1000.0;
		break;
	case SY:
	case JZC:
	case GJJ:
		fValue = hValue/1.0e6;
		break;
	default:
		fValue = hValue/1.0;
	}

	return fValue;
}

double CGoods::GetFValue( short sID )
{
	double fValue = 0;
	if (sID < 1000)
		fValue = CGoods::Long2Float(GetID(), sID, GetValue(sID));
	else
		fValue = m_jbm.GetFValue(sID-1000);

	return fValue;
}

WORD CGoods::AmountDiv()
{
	return AmountDiv(GetID());
}

WORD CGoods::AmountDiv(DWORD dwGoodsID)
{
	if (CGoods::IsGZQH(dwGoodsID))
		return 1;
	else
		return 1000;
}

WORD CGoods::LastMin()
{
	CTradeSession* pTS = CheckTS();
	if (pTS==NULL)
		return 0;

	return pTS->m_pwTimeEnd[pTS->m_cNum-1];
}

INT64 CGoods::TransVol2Hand(INT64 hVolume)	// 股变手
{
	if (IsGZQH() || IsIndex() || IsSHZQ())
		return hVolume;
	else if (IsSZZQ())
		return (hVolume+5)/10;
	else
		return (hVolume+50)/100;
}
char CGoods::Compare(CGoods* pGoods, short sSortID, char cSort)
{
	if (sSortID==-1)
		cSort = 1;

	if (cSort==0)
		return 0;
	else
	{
		if (sSortID==-1)
		{
			INT64 lThis = m_cStation*10*1000000l+m_cGroup*1000000l+m_dwNameCode;
			INT64 lGoods = pGoods->m_cStation*10*1000000l+pGoods->m_cGroup*1000000l+pGoods->m_dwNameCode;
			if (lThis>lGoods)
				return cSort;
			else if (lThis<lGoods)
				return -cSort;
			else
				return 0;
		}
		else
		{
			INT64 lThis = GetValue(sSortID);
			INT64 lGoods = pGoods->GetValue(sSortID);
			if ( (sSortID == CPX_DAY) || (sSortID == CPX_WEEK) ) // B,S点等于1或-1的乘以一个超大值,排序在最前面,byzhaolin 20140723
			{
				if (lThis == 1 || lThis == -1) lThis *= (m_cStation*10*1000000l+m_cGroup*1000000l+m_dwNameCode);
				if (lGoods == 1 || lGoods == -1) lGoods *= (pGoods->m_cStation*10*1000000l+pGoods->m_cGroup*1000000l+pGoods->m_dwNameCode);
			}

			if (lThis>lGoods)
				return cSort;
			else if (lThis<lGoods)
				return -cSort;
			else
				return Compare(pGoods, -1, 1);
		}
	}
}

DWORD CGoods::GetDateValue()
{
	if (m_dwDate)
		return m_dwDate;
	else if (m_cStation==5)
		return theApp.m_dwDateValueHK;
	else
		return theApp.m_dwDateValue;
}

DWORD CGoods::GetTimeValue()
{
	if (m_cStation==5)
		return theApp.m_dwTimeValueHK;
	else if (m_cStation<=3) //20110622
		return theApp.m_dwTimeValue;
	else
		return m_dwTime;
}

BOOL CGoods::CrossDay()
{
	CTradeSession* pTS = CheckTS(FALSE);

	return (pTS && pTS->m_pwTimeBegin[0]<0);
}


///20120319 sjp

CTradeSession* CGoods::CheckTS(BOOL bIncludeVirtual)
{
	CTradeSession* pTS = NULL;
	if (m_tradeSession.m_cNum>0 && m_tradeSession.m_cNum<=MAX_TRADING_SESSION)
		pTS = &m_tradeSession;
	else
		pTS = theApp.GetTradeSession(m_cStation, bIncludeVirtual);

	if (pTS->m_cNum>0 && pTS->m_cNum<=MAX_TRADING_SESSION)
		return pTS;
	else
		return NULL;
}

BOOL CGoods::CheckVolume(XInt32 xVolume)	//20110625
{
	DWORD dwID = GetID();
	if (m_cStation>=5 || (dwID>=1160000 && dwID<=1169999) || (dwID>=519000 && dwID<=519999))
		return TRUE;
	else
		return xVolume.m_nBase>0;
}

BOOL CGoods::IsValueXInt32(short sID)
{
	return sID>=500 || sID<=-500;
}

INT64 CGoods::GetValue(short sID)
{
	if (sID<1000)
	{
		INT64 hValue = 0;

		switch (sID)
		{
			case CLOSE:
				hValue = m_dwClose;
				break;

			case OPEN:
				hValue = m_dwOpen;
				break;
			case HIGH:
				hValue = m_dwHigh;
				break;
			case LOW:
				hValue = m_dwLow;
				break;
			case PRICE:
				hValue = m_dwPrice;
				if (IsQZ() || IsSHB() || IsJJ() || IsZQ())
					hValue /= 100;
				if (IsGZQH())
					hValue *= 100;
				break;
			case TRADENUM:
				hValue = m_dwTradeNum;
				break;
			case PBUY:
				hValue = m_dwBuyPrice;
				break;
			case PSELL:
				hValue = m_dwSellPrice;
				break;
			case P_ZT:
				hValue = m_dwPriceZT;
				break;
			case P_DT:
				hValue = m_dwPriceDT;
				break;
			case PCLOSE:
				hValue = m_dwPClose;
				break;
			case WCLOSE:
				hValue = m_dwWClose;
				break;
			case CLOSE5:
				hValue = m_dw5Close;
				break;

			case STRONG:
				hValue = m_nStrong;
				break;

			case PRICE5:
				hValue = m_dwPrice5;
				break;

			case VOLUME:
				hValue = m_xVolume;
				if (IsGZQH() || IsIndex())//for sort
					hValue *= 100;
				break;
			case AMOUNT:
				hValue = m_xAmount;
				if (IsGZQH())//fort sort
					hValue *= 1000;
				break;
			case VBUY:
				hValue = m_xBuyVol;
				break;
			case VSELL:
				hValue = m_xSellVol;
				break;

			case ZGB:
				hValue = m_xZFX;
				break;
			case LTG:
				hValue = m_xLTG;
				break;
		
			case NEIPAN:
				hValue = m_xNeiPan;
				break;
			case CURVOL:
				hValue = m_xCurVol;
				break;

			case SUMVOL4:
				hValue = m_xSumVol4;
				break;
			case AVEVOL5:
				hValue = m_xAveVol5;
				break;
			case SUMBIGAMT4:
				hValue = m_xSumBigAmt4;
				break;

			case NAMECODE:
				hValue = m_dwNameCode;
				break;

			case JJJZ_N:
				if (m_cStation == 0)
					hValue = m_dwPrice / 100;
				else if (m_xVolume.GetValue() > 0)
					hValue = m_dwPrice;
				else
					hValue = GetValue(SYL) / 100;
				break;

			case JJJZ_Z:
				if (m_cStation == 0)
					hValue = m_dwClose / 100;
				else if (m_xVolume.GetValue() > 0)
					hValue = m_dwClose;
				else
					hValue = m_jbm.m_pxJBM[J_MGGJJ] / 100;
				break;

			case JJJZ_RFPER:
				{
					int lLast = GetValue(JJJZ_Z);
					int lNow = GetValue(JJJZ_N);
					if (lLast>0 && lNow>0)
					{
						if (lNow>lLast)
							hValue = (100000*(lNow-lLast)/lLast+5)/10;
						else if (lNow<lLast)
							hValue = (100000*(lNow-lLast)/lLast-5)/10;
					}
				}
				break;

			case AVEPRICE:
				if (m_xVolume>0)
				{
					INT64 hVolume = m_xVolume;
					INT64 hAmount = m_xAmount;
					if (hVolume>0)
					{
						if (IsIF() || IsIC() || IsIH())
							hValue = hAmount/hVolume/30;
						else if (IsTF() || IsT())
							hValue = hAmount/hVolume/10;
						if (m_cStation==0 && CGoods::IsSHZQ(GetID()))
							hValue = hAmount/hVolume/10;
						else
							hValue = hAmount/hVolume;
					}
				}
				break;
			case CURBS:
				hValue = m_cCurVol;
				break;
			case PRICE_BASE:
				if ( IsIF() && theApp.m_p300 )
				{
					hValue = theApp.m_p300->m_dwPrice;
					if (hValue==0)
						hValue = theApp.m_p300->m_dwClose;
				}
				else if ( IsIH() && theApp.m_p016 )
				{
					hValue = theApp.m_p016->m_dwPrice;
					if (hValue==0)
						hValue = theApp.m_p016->m_dwClose;
				}
				else if ( IsIC() && theApp.m_p905 )
				{
					hValue = theApp.m_p905->m_dwPrice;
					if (hValue==0)
						hValue = theApp.m_p905->m_dwClose;
				}
				break;
			case ZHANGDIE:
				if (IsGZQH())
				{
					if (m_dwPrice>0 && m_dwPreSettlement>0)
						hValue = 100*(INT64(m_dwPrice)-m_dwPreSettlement);
				}
				else
				{
					if (m_dwPrice>0 && m_dwClose>0)
						hValue = INT64(m_dwPrice)-m_dwClose;
				}
				break;

			case ZHANGSU:
				if (m_dwPrice>0 && m_dwPrice5>0)
					hValue = INT64(m_dwPrice)-m_dwPrice5;
				break;
			case JICHA:
			case JICHA_PER:
				if (m_dwPrice>0)
				{
					CGoods* pBaseGoods = NULL;
					if( IsIF() && theApp.m_p300 ){
						pBaseGoods = theApp.m_p300;
					}
					else if( IsIH() && theApp.m_p016 ){
						pBaseGoods = theApp.m_p016;
					}
					else if( IsIC() && theApp.m_p905){
						pBaseGoods = theApp.m_p905;
					}
					if( pBaseGoods != NULL )
					{
						DWORD dwBase = pBaseGoods->m_dwPrice;
						if (dwBase==0)
							dwBase = pBaseGoods->m_dwClose;
					
						if (dwBase>0)
						{
							if (sID==JICHA)
								hValue = (dwBase-INT64(m_dwPrice*100))/100;
							else
							{
								INT64 hPrice = m_dwPrice*100;
								INT64 hClose = dwBase;
								if (hClose>hPrice)
									hValue = (100000*(hClose-hPrice)/hClose+5)/10;
								else if (hClose<hPrice)
									hValue = (100000*(hClose-hPrice)/hClose-5)/10;
							}
						}
					}
				}
				break;
			case ZDF:
			case ZSF:
			case ZDF5:
			case ZDF10:
			case ZDF20:
			case WZDF:
			case PZDF:
				{
					INT64 hClose;
					INT64 hPrice = m_dwPrice;

					switch (sID)
					{
					case ZDF5:
						hClose = m_dw5Close;
						break;
					case ZDF10:
						hClose = m_dwClose9;
						break;
					case ZDF20:
						hClose = m_dwClose19;
						break;
					case WZDF:
						hClose = m_dwWClose;
						break;
					case PZDF:
						hClose = m_dwPClose;
						hPrice = m_dwClose;
						break;
					case ZSF:
						hClose = m_dwPrice5;
						break;
					default:
						hClose = IsGZQH() ? m_dwPreSettlement : m_dwClose;
					}
					if (hPrice>0 && hClose>0)
					{
						if (hPrice>hClose)
							hValue = (100000*(hPrice-hClose)/hClose+5)/10;
						else if (hPrice<hClose)
							hValue = (100000*(hPrice-hClose)/hClose-5)/10;
					}
				}
				break;

			case ZHENFU:
				if (m_dwClose>0 && m_dwHigh>0 && m_dwLow>0)
				{
					if (m_dwHigh>m_dwLow)
						return (100000*(INT64(m_dwHigh)-m_dwLow)/m_dwClose+5)/10;
					else if (m_dwHigh<m_dwLow)
						return (100000*(INT64(m_dwHigh)-m_dwLow)/m_dwClose-5)/10;
				}
				break;

			case LIANGBI:
				if (m_xAveVol5>0)
				{
					CTradeSession* pTS = NULL;
					if (m_tradeSession.m_cNum>0 && m_tradeSession.m_cNum<=MAX_TRADING_SESSION)
						pTS = &m_tradeSession;
					else
						pTS = theApp.GetTradeSession(m_cStation, m_cStation);

					// 9:15-9:25时段没有K线 ,MIN2POS国内股市每天有250个POS，实际只有240根分钟线[1/29/2013 frantqian]
					int nPos = Min2Pos(Time2Min(m_dwTime, m_cStation), m_cStation);
					if ((m_hMaskGroup&(GROUP_SH_INDEX|GROUP_SZ_INDEX| GROUP_GNBK| GROUP_HYBK| GROUP_DQBK| GROUP_RDBK | GROUP_ALL_A
						| GROUP_SZ_B | GROUP_SZ_JJ | GROUP_SZ_ZQ)) != 0 )
					{
						// 目前看深圳含早晨的集合竞价时段，所以要减去这个10分钟 [5/10/2013 frantqian]
						if (pTS != NULL && pTS->m_cNum >= 3)
						{
							nPos -= 10;
						}
					}
		
					if(nPos < 0)
						nPos = 0;

					return INT64(double(m_xVolume.GetValue())*MinInDay()/(nPos+1)/m_xAveVol5.GetValue()*100.0+0.5);
				}
				break;

			case SYL:
				{
					INT64 hSY = m_jbm.m_pxJBM[J_MGSY];
					WORD wMonth = WORD(m_jbm.m_dwDate%10000/100);
					if (wMonth>0)
						hSY = hSY*12/wMonth;
					if (hSY!=0)
					{
						INT64 hPrice = m_dwPrice;
						if (hPrice==0)
							hPrice = m_dwClose;
						hValue = hPrice*100000/hSY;
					}
				}
				break;

			case HSL:
			case HSL5:
				if (m_xLTG>0)
				{
					if (sID==HSL)
						hValue = INT64(10000.0*m_xVolume/m_xLTG+0.5);
					else
						hValue = INT64(10000.0*(m_xVolume+m_xSumVol4)/m_xLTG+0.5);
				}
				break;

			case BIGAMT:
				{
//					RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);
					hValue = (m_matrix.m_ocSumOrder.m_pxAmtOfBuy[3]
						+m_matrix.m_ocSumOrder.m_pxAmtOfBuy[2]
						-m_matrix.m_ocSumOrder.m_pxAmtOfSell[3]
						-m_matrix.m_ocSumOrder.m_pxAmtOfSell[2])
						/1000;
				}
				break;

			case BIGAMT5:
				{
//					RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);
					hValue = m_xSumBigAmt4
						+(m_matrix.m_ocSumOrder.m_pxAmtOfBuy[3]
						+m_matrix.m_ocSumOrder.m_pxAmtOfBuy[2]
						-m_matrix.m_ocSumOrder.m_pxAmtOfSell[3]
						-m_matrix.m_ocSumOrder.m_pxAmtOfSell[2])
						/1000;
				}
				break;

			case DDBL:
				{
					INT64 hLTG = m_xLTG;
					if (hLTG>0)
					{
//						RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);
						hValue = m_matrix.m_ocSumOrder.m_pxVolOfBuy[3]+m_matrix.m_ocSumOrder.m_pxVolOfBuy[2]-m_matrix.m_ocSumOrder.m_pxVolOfSell[3]-m_matrix.m_ocSumOrder.m_pxVolOfSell[2];
						if (hValue>0)
							hValue = (hValue*1000000/hLTG+5)/10;
						else if (hValue<0)
							hValue = (hValue*1000000/hLTG-5)/10;
					}
				}
				break;

			case DDBL5:
				{
					INT64 hLTG = m_xLTG;
					if (hLTG>0)
					{
//						RWLock_scope_wrlock  rdlock(m_matrix.m_MatrixLock);
						hValue = m_matrix.m_ocSumOrder.m_pxVolOfBuy[3]+m_matrix.m_ocSumOrder.m_pxVolOfBuy[2]-m_matrix.m_ocSumOrder.m_pxVolOfSell[3]-m_matrix.m_ocSumOrder.m_pxVolOfSell[2];
						hValue += m_xSumBigVol4;
						if (hValue>0)
							hValue = (hValue*1000000/hLTG+5)/10;
						else if (hValue<0)
							hValue = (hValue*1000000/hLTG-5)/10;
					}
				}
				break;

			case ZLZC5:	//5日增仓
			case ZLZC10://10日增仓
			case ZLZC20://20日增仓
				{
//					RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);
					hValue = m_matrix.m_ocSumOrder.m_pxAmtOfBuy[3]+m_matrix.m_ocSumOrder.m_pxAmtOfBuy[2]-m_matrix.m_ocSumOrder.m_pxAmtOfSell[3]-m_matrix.m_ocSumOrder.m_pxAmtOfSell[2];
					if (hValue>0)
						hValue = 1;
					else
						hValue = 0;
					if (sID==ZLZC5)
						hValue += m_bZLZC4;
					else if (sID==ZLZC10)
						hValue += m_bZLZC9;
					else
						hValue += m_bZLZC19;
				}
				break;

			case ZLQM://主力强买
				{
//					RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);
					hValue = m_matrix.m_ocSumOrder.m_pxAmtOfBuy[3]+m_matrix.m_ocSumOrder.m_pxAmtOfBuy[2]-m_matrix.m_ocSumOrder.m_pxAmtOfSell[3]-m_matrix.m_ocSumOrder.m_pxAmtOfSell[2];
					if (hValue>0)
						hValue = m_bZLQM+1;
					else
						hValue = 0;
				}
				break;

			case CPX_DAY:
				hValue = m_nCPX_Day;
				break;
			case CPX_WEEK:
				hValue = m_nCPX_Week;
				break;
			case CPX_MONTH:
				hValue = m_nCPX_Month;
				break;
			case CPX_MIN60:
				hValue = m_nCPX_Min60;
				break;
			case CPX_MIN30:
				hValue = m_nCPX_Min30;
				break;
			case CPX_MIN15:
				hValue = m_nCPX_Min15;

			case XJ_CPX:
				{
					if (m_nCPX_Day < -1) 
						hValue = 1;
					else if (m_nCPX_Day == -1) 
						hValue = 2;
					else if (m_nCPX_Day == 1) 
						hValue = 3;
					else if (m_nCPX_Day > 1) 
						hValue = 4;
				}
				break;

			case XG_CJZJ_ASC:
				hValue = m_dwXGStatus;//(m_dwXGStatus & 0x01);
				break;
			case XG_VOL_BURST:
				hValue = (m_dwXGStatus & 0x02);
				break;

			case SJL:
				hValue = m_jbm.m_pxJBM[J_TZMGJZC];
				if (hValue>0)
					hValue = INT64(m_dwPrice)*100000/hValue;
				else
					hValue = 0;
				break;

			case WEIBI1:
				{
					INT64 hBuy;
					INT64 hSell;
					if (m_cLevel==2)
					{
						hBuy = m_xBuyVol;
						hSell = m_xSellVol;
					}
					else
					{
						hBuy = m_pxMMPVol[BUY1]+m_pxMMPVol[BUY2]+m_pxMMPVol[BUY3]+m_pxMMPVol[BUY4]+m_pxMMPVol[BUY5];
						hSell = m_pxMMPVol[SELL1]+m_pxMMPVol[SELL2]+m_pxMMPVol[SELL3]+m_pxMMPVol[SELL4]+m_pxMMPVol[SELL5];
					}
					INT64 hAdd = hBuy+hSell;
					if (hAdd>0)
					{
						if (hBuy>hSell)
							hValue = INT64(10000.0*(hBuy-hSell)/hAdd+0.5);
						else if (hBuy<hSell)
							hValue = -INT64(10000.0*(hSell-hBuy)/hAdd+0.5);
					}
				}
				break;
/*///???			
			case RISE:
			case EQUAL:
			case FALL:
				{
					CIndex* pIndex = theApp.Goods2Index(GetID());
					if (pIndex)
					{
						if (sID==RISE)
							hValue = pIndex->m_wRise;
						else if (sID==EQUAL)
							hValue = pIndex->m_wEqual;
						else
							hValue = pIndex->m_wFall;
					}
				}
				break;
*////???					
			case SY:
				hValue = m_jbm.m_pxJBM[J_MGSY];
				break;
			case GJJ:
				hValue = m_jbm.m_pxJBM[J_MGGJJ];
				break;
			case JZC:
				hValue = m_jbm.m_pxJBM[J_TZMGJZC];
				break;

			case WAIPAN:
				hValue = m_xVolume-m_xNeiPan;
				if (hValue<0)
					hValue = 0;
                break;

			case AVEVOL:
				if (m_dwTradeNum>0)
					hValue = m_xVolume/m_dwTradeNum;
				break;

			case WEIBI2:
				{
					INT64 hBuy;
					INT64 hSell;
					if (m_cLevel==2)
					{
						hBuy = m_xBuyVol;
						hSell = m_xSellVol;
					}
					else
					{
						hBuy = m_pxMMPVol[BUY1]+m_pxMMPVol[BUY2]+m_pxMMPVol[BUY3]+m_pxMMPVol[BUY4]+m_pxMMPVol[BUY5];
						hSell = m_pxMMPVol[SELL1]+m_pxMMPVol[SELL2]+m_pxMMPVol[SELL3]+m_pxMMPVol[SELL4]+m_pxMMPVol[SELL5];
					}
					hValue = hBuy-hSell;
				}
				break;

			case ZSZ:
			case LTSZ:
				{
					// 板块类的传0 [11/9/2012 frantqian]
					if ((m_hMaskGroup&(GROUP_SH_INDEX|GROUP_SZ_INDEX| GROUP_GNBK| GROUP_HYBK| GROUP_DQBK| GROUP_RDBK )) != 0)
					{
						hValue = 0;
					}
					else
					{
						DWORD dwPrice = m_dwPrice;
						if (dwPrice==0)
							dwPrice = m_dwClose;
						INT64 hGB;
						if (sID==ZSZ)
							hGB = m_xZFX;
						else
							hGB = m_xLTG;
						hValue = hGB*dwPrice/1000;
					}
				}
				break;

			case TRADECODE:
				hValue = m_cTradeCode;
				break;

			case OPENINTEREST:
				hValue = m_xOpenInterest;
				break;			
			case TODAY_OI:
				hValue = m_xOpenInterest-m_xPreOpenInterest;
				break;
			case CUR_OI:
				hValue = m_xCurOI;
				break;

			case SETTLEMENT:
				hValue = m_dwSettlement;
				break;
			case PRE_SETTLEMENT:
				hValue = m_dwPreSettlement;
				break;
			case PJ:
				{//评级在此处只能用来做排序用，具体内容需要从m_goodsScore中取得
					if(m_goodsScore == "A+")	hValue = 100;
					else if(m_goodsScore == "A")	hValue = 99;
					else if(m_goodsScore == "A-")	hValue = 98;
					else if(m_goodsScore == "B+")	hValue = 97;
					else if(m_goodsScore == "B")	hValue = 96;
					else if(m_goodsScore == "B-")	hValue = 95;
					else if(m_goodsScore == "C+")	hValue = 94;
					else if(m_goodsScore == "C")	hValue = 93;
					else if(m_goodsScore == "C-")	hValue = 92;
					else if(m_goodsScore == "D+")	hValue = 91;
					else if(m_goodsScore == "D")	hValue = 90;
					else if(m_goodsScore == "D-")	hValue = 89;
					else	hValue = 0;
					break;
				}
			case TTM_SYL:
			case TTM_SXL:
				{
					INT64 hVal = m_jbm.m_fpd.GetValue((sID==TTM_SYL)?JN_S_TTM_JLR:JN_S_TTM_ZYSR);
					INT64 hZSZ = (INT64)m_xZFX;
					hZSZ *= ((m_dwPrice!=0)?m_dwPrice:m_dwClose);
					if (hVal>0)//单位：元放大10000倍
						hValue = (hZSZ/(hVal/10000)+5)/10;
					else if (hVal<0)
						hValue = (hZSZ/(hVal/10000)-5)/10;
					//LOG_INFO("Calc TTM:");
					//LOG_INFO("hVal:[%lld], hZSZ:[%lld], hValue:[%lld]", hVal, hZSZ, hValue);
				}
				break;
			default:
				if (sID>=PSELL10 && sID<=PBUY10)
					hValue = m_pdwMMP[sID-PSELL10];
				else if (sID>=VSELL10 && sID<=VBUY10)
					hValue = m_pxMMPVol[sID-VSELL10];
		}

		return hValue;
	}
	else
		return m_jbm.GetValue(sID-1000);
}


void CGoods::SaveToday()
{
	SaveBuffer();

	if (WantSaveToday(TRUE))
	{
		// 收盘方式修改，防止校验反复出错问题 [5/6/2013 frantqian] 按原始数据收盘，否则同步会覆盖数据，跟收盘数据小数位上不同
		CDataFile_Day* pDF = theApp.GetDataFile(m_cStation, DAY_DATA, DAY_PERIOD);
		if (pDF != NULL)
		{
			CArray<CDay, CDay&> aDayLoad;				
			pDF->LoadData(GetID(), aDayLoad);

			CSortArray<CDay> aDay;
			DWORD dwCurDate = GetDateValue();

			int nDayLoad = (int)aDayLoad.GetSize();
			for (int i=0; i<nDayLoad; i++)
			{
				if (aDayLoad[i].m_dwTime > 19900101 && aDayLoad[i].m_dwTime <= dwCurDate)
					aDay.Change(aDayLoad[i]);
			}

			CDay day;
			GetToday(day);
			aDay.Change(day);

			pDF->SaveData(GetID(), aDay.GetData(), aDay.GetSize(), dwCurDate, FALSE);
		}

 		CDataDay* pData = NULL;

		pData = (CDataDay*)TryLoadToday();
		if (pData)
		{
			pData->SaveTodayMin1();
			delete pData;
		}

		pData = (CDataDay*)LoadData2(MIN_DATA, 1);
		if (pData)
		{
			pData->SaveData();
			delete pData;
		}
		else
		{
			if ((m_hMaskGroup&GROUP_ALL_A) != 0)
			{
				LOG_ERR("LoadData2(MIN_DATA,1) is null,goodsid=%d",GetID());
			}
		}

		pData = (CDataDay*)LoadData2(MIN_DATA, 5);
		if (pData)
		{
			pData->SaveData();
			delete pData;
		}
		else
		{
			if ((m_hMaskGroup&GROUP_ALL_A) != 0)
			{
				LOG_ERR("LoadData2(MIN_DATA,5) is null,goodsid=%d",GetID());
			}
		}

		pData = (CDataDay*)LoadData2(MIN_DATA, 30);
		if (pData)
		{
			pData->SaveData();
			delete pData;
		}
		else
		{
			if ((m_hMaskGroup&GROUP_ALL_A) != 0)
			{
				LOG_ERR("LoadData2(MIN_DATA,30) is null,goodsid=%d",GetID());
			}
		}
	}
	else
	{
		if ((m_hMaskGroup&GROUP_ALL_A) != 0)
		{
			LOG_ERR("WantSaveToday=false,goodsid=%d",GetID());
		}
		
	}
}

void CGoods::DeleteData()
{
	delete m_pDataBargain;
	delete m_pDataMinute1;
	delete m_pDataMinute0;
	delete m_pDataDay;
	delete m_pDataMin30;
	delete m_pDataMin5;
	delete m_pDataMin1;
	delete m_pDataHisMin;
	m_pDataBargain = NULL;
	m_pDataMinute1 = NULL;
	m_pDataMinute0 = NULL;
	m_pDataDay = NULL;
	m_pDataMin30 = NULL;
	m_pDataMin5 = NULL;
	m_pDataMin1 = NULL;
	m_pDataHisMin = NULL;
}


void CGoods::CreateData()
{
	m_pDataDay = CreateData(DAY_DATA, 1);
	m_pDataMin30 = CreateData(MIN_DATA, 30);
	m_pDataMin5 = CreateData(MIN_DATA, 5);
	m_pDataMin1 = CreateData(MIN_DATA, 1);

	m_pDataMinute0 = CreateData(MINUTE_DATA, 0,TRUE);
	m_pDataMinute1 = CreateData(MINUTE_DATA, 1,TRUE);

	m_pDataBargain = CreateData(BARGAIN_DATA, 1,TRUE);

	m_pDataHisMin = CreateData(HISMIN_DATA, 1, TRUE);
}

void CGoods::CreateWR()
{
}

RWLock* CGoods::GetWR(WORD wDataType)
{
	switch (wDataType)
	{
	case BARGAIN_DATA:
		return &m_rwBargain;
	case MINUTE_DATA:
	case MIN_DATA:
		return &m_rwMinute;
	case DAY_DATA:
		return &m_rwGood;
	case HISMIN_DATA:
		return &m_rwHisMin;
	}
	return NULL;
}


CData* CGoods::CreateData(WORD wDataType, WORD wPeriod, bool bGuard)
{
	RWLock* pWR = GetWR(wDataType);

	CData* pData = NULL;

	switch (wDataType)
	{
	case BARGAIN_DATA:
		pData = new CDataBargain;
		break;
	case MINUTE_DATA:
		pData = new CDataMinute;
		break;
	case MIN_DATA:
	case DAY_DATA:
		pData = new CDataDay;
		break;
	case HISMIN_DATA:
		pData = new CDataHisMin;
	}

	if (pData)
	{
		pData->m_pGoods = this;
		pData->m_pWR = bGuard ? pWR : NULL;
		pData->m_wDataType = wDataType;
		pData->m_wPeriod = wPeriod;
	}

	return pData;
}

CData** CGoods::GetDataPtr(WORD wDataType, WORD wPeriod)
{
	switch (wDataType)
	{
	case BARGAIN_DATA:
        return &m_pDataBargain;
	case MINUTE_DATA:
		if (wPeriod==1)
			return &m_pDataMinute1;
		else
			return &m_pDataMinute0;
	case DAY_DATA:
		return &m_pDataDay;
	case MIN_DATA:
		if (wPeriod%30 == 0)
			return &m_pDataMin30;
		else if (wPeriod%5 == 0)
			return &m_pDataMin5;
		else
			return &m_pDataMin1;
	case HISMIN_DATA:
		return &m_pDataHisMin;
	}

	return NULL;
}


void CGoods::ClearDay()
{
	l_ReloadData(m_pDataDay);
}

void CGoods::FreeData(CData* pData)
{
	if (pData==NULL)
		return;

	//WORD wDataType = pData->m_wDataType;
	//WORD wPeriod = pData->m_wPeriod;

	//RWLock* pWR = GetWR(wDataType);
	//CData** ppDataPtr = GetDataPtr(wDataType, wPeriod);
	//if (pWR && ppDataPtr)
	//{
	//	CData*& pDataPtr = *ppDataPtr;
	//	if (pData==pDataPtr)
	//	{
	//		///???pWR->BeginRead();

	//		///???if (pDataPtr)
	//		CMyThread::atomicDec(&(pDataPtr->m_nUseCount));
	//		///???pWR->EndRead();
	//	}
	//}

	if (pData->m_pWR)
	{
		CMyThread::atomicDec(&(pData->m_nUseCount));
	}
	else
		delete pData;
}
// 释放内存线程专用 [11/14/2013 frantqian]
void CGoods::FreeData(CMyThread* pthDelete)
{
	if (!pthDelete->isAlive())
		return;

	if (m_pDataDay->WantFree())
	{
		//if (m_rwGood.TryAcquireWriteLock() == 0)
		//{
		// 更换锁 [3/5/2015 frant.qian]
		RWLock_scope_wrlock  rwlock(this->m_rwGood);
		this->m_rwGoodcount[3] += 1;
		try
		{
			if (m_pDataDay->TestWantFree())
			{
				m_pDataDay->Trace("释放");
				m_pDataDay->DeleteData();
				CMyThread::atomicDec(&s_nCacheDay);
			}
		}
		catch (...)
		{
			LOG_ERR("CGoods::FreeData fail!!");
		}
		this->m_rwGoodcount[3] -= 1;
		//	m_rwGood.ReleaseWriteLock();
		//}
	}

	if (!pthDelete->isAlive())
		return;

	if (m_pDataMin30->WantFree() || m_pDataMin5->WantFree()||
		m_pDataMin1->WantFree() || m_pDataMinute0->WantFree() || m_pDataMinute1->WantFree())
	{
		if (m_rwMinute.TryAcquireWriteLock() == 0)
		{	
			if (m_pDataMin30->TestWantFree())
			{
				m_pDataMin30->Trace("释放");
				m_pDataMin30->DeleteData();
				CMyThread::atomicDec(&s_nCacheMin30);
			}
			
			if (m_pDataMin5->TestWantFree())
			{
				m_pDataMin5->Trace("释放");
				m_pDataMin5->DeleteData();
				CMyThread::atomicDec(&s_nCacheMin5);
			}

			if (m_pDataMin1->TestWantFree())
			{
				m_pDataMin1->Trace("释放");
				m_pDataMin1->DeleteData();
				CMyThread::atomicDec(&s_nCacheMin1);
			}

			if (m_pDataMinute0->TestWantFree())
			{
				m_pDataMinute0->Trace("释放");
				m_pDataMinute0->DeleteData();
				CMyThread::atomicDec(&s_nCacheMinute0);
			}

			if (m_pDataMinute1->TestWantFree())
			{
				m_pDataMinute1->Trace("释放");
				m_pDataMinute1->DeleteData();
				CMyThread::atomicDec(&s_nCacheMinute1);
			}

			m_rwMinute.ReleaseWriteLock();
		}
	}

	if (!pthDelete->isAlive())
		return;

	if (m_pDataBargain->WantFree())
	{
		if (m_rwBargain.TryAcquireWriteLock() == 0)
		{
			if (m_pDataBargain->TestWantFree())
			{
				m_pDataBargain->Trace("释放");
				m_pDataBargain->DeleteData();
				CMyThread::atomicDec(&s_nCacheBargain);
			}

			m_rwBargain.ReleaseWriteLock();
		}
	}

	if (m_pDataHisMin && m_pDataHisMin->WantFree())
	{
		if (m_rwHisMin.TryAcquireWriteLock() == 0)
		{
			if (m_pDataHisMin->TestWantFree())
			{
				m_pDataHisMin->Trace("释放");
				m_pDataHisMin->DeleteData();
				CMyThread::atomicDec(&s_nCacheHisMin);
			}
			m_rwHisMin.ReleaseWriteLock();
		}
	}

}

void CGoods::SaveHisMin(CHisMinDF* pHMDF, DWORD dwDate)
{
/*
	if (WantSaveToday(TRUE)==FALSE)
		return;
*/
	CGoodsCode gcGoodsID = GetGoodsCode();

	BOOL bIsQH = IsQH();

	CHisMinArray aHisMinToday;
	
	DWORD dwCurYYMMDD = dwDate%1000000;
	DWORD dwCurYYMMDD0000 = dwCurYYMMDD*10000;

	CDataMinute* pMinData = (CDataMinute*)TryLoadData(MINUTE_DATA, 0);
	if( pMinData == NULL ){
		return;
	}

	int nSize = (int)pMinData->m_aMinute.GetSize();

	CHisMin hm;

	for (int i=0; i<nSize; i++)
	{
		CMinute& min = pMinData->m_aMinute[i];
///		if (min.m_xVolume.m_nBase>0)
		if (min.m_xVolume.m_nBase>0 || min.m_dwClose>0)
		{
			if (min.m_wTime/10000==0)
				hm.m_dwTime = min.m_wTime+dwCurYYMMDD0000;
			else
				hm.m_dwTime = min.m_wTime;

			hm.m_dwPrice = min.m_dwClose;
			hm.m_dwAve = min.m_dwAve;
			hm.m_xVolume = min.m_xVolume;

			if (bIsQH)
				hm.m_xZJJL.SetRawData(min.m_dwTradeNum);
			else
				hm.m_xZJJL = (min.m_ocOrder.m_pxAmtOfBuy[3]+min.m_ocOrder.m_pxAmtOfBuy[2]-min.m_ocOrder.m_pxAmtOfSell[3]-min.m_ocOrder.m_pxAmtOfSell[2])/1000;

			aHisMinToday.Add(hm);
		}
	}
	pMinData->FreeData();

	hm.m_dwTime = 9999+dwCurYYMMDD0000;
	hm.m_dwPrice = m_dwClose;
	hm.m_dwAve = m_dwHigh;
	hm.m_xVolume.SetRawData(m_dwLow);
	aHisMinToday.Add(hm);

	int nHisMinToday = int(aHisMinToday.GetSize());

	if (pHMDF)
	{
		CHisMinArray aHisMin;
		pHMDF->m_dfHisMin.LoadData(gcGoodsID, aHisMin);
		
		for (int i=0; i<nHisMinToday; i++)
			aHisMin.Change(aHisMinToday[i]);

		pHMDF->m_dfHisMin.SaveData(gcGoodsID, aHisMin, dwDate, FALSE);
	}

	if( m_xVolume.m_nBase == 0 ){
		//当日无交易，忽略此日的数据
		return;
	}

	//写入公共文件
	CHisMinArray aHisMin;
	CDataFile_HisMin* pHisMinDF = theApp.GetHisMinFile(m_cStation, m_dwNameCode);
	pHisMinDF->LoadData(gcGoodsID, aHisMin);

	DWORD dwYYMMDDSum = 0;
	DWORD dwYYMMDDSave = 0;

	int nHisMin = int(aHisMin.GetSize());
	for (int i=nHisMin-1; i>=0; i--)
	{
		CHisMin& hm = aHisMin[i];
		DWORD dwYYMMDD = hm.m_dwTime/10000;
		if (dwYYMMDD==dwCurYYMMDD)
		{
			aHisMin.RemoveAt(i);
			continue;
		}

		if (dwYYMMDDSave!=dwYYMMDD)
		{
			dwYYMMDDSave = dwYYMMDD;
			dwYYMMDDSum++;
		}

		if (dwYYMMDDSum>g_dwSaveHisMin)
			aHisMin.RemoveAt(i);
	}

	aHisMin.Append(aHisMinToday);

	pHisMinDF->SaveData(gcGoodsID, aHisMin, dwDate, FALSE);
}


CData* CGoods::LoadData2(WORD wDataType, WORD wPeriod)
{
	CData* pData = NULL;

	if (wDataType==MINUTE_DATA)
		pData = new CDataMinute;
	else
		pData = new CDataDay;

	if (pData)
	{
		pData->m_pGoods = this;
		pData->m_wDataType = wDataType;
		pData->m_wPeriod = wPeriod;
		pData->LoadData();
	}

	return pData;
}

void CGoods::TraceCache()
{
	LogInfo("***********************************************************************");
	LogInfo("日线		cache count=%d", s_nCacheDay);
	LogInfo("30分钟线	cache count=%d", s_nCacheMin30);
	LogInfo("5分钟线	cache count=%d", s_nCacheMin5);
	LogInfo("1分钟线	cache count=%d", s_nCacheMin1);
	LogInfo("分时0		cache count=%d", s_nCacheMinute0);
	LogInfo("分时1		cache count=%d", s_nCacheMinute1);
	LogInfo("分笔		cache count=%d", s_nCacheBargain);
	LogInfo("5日分时	cache count=%d", s_nCacheHisMin);
	LogInfo("***********************************************************************");
}
 
CData* CGoods::TryLoadData(WORD wDataType, WORD wPeriod)
{
	CData* pDataRet = NULL;

	CData** ppDataCache = GetDataPtr(wDataType, wPeriod);
	if (ppDataCache)
	{
		CData*& pDataCache = *ppDataCache;

		pDataCache->AcquireLock();
			
		///???if (pDataPtr)
		{
			pDataCache->m_dwActiveSecond = CMDSApp::g_dwSysSecond;
			CMyThread::atomicInc(&(pDataCache->m_nUseCount));
			if (pDataCache->m_cLoadOK==0)
			{
				// 检查checkfree后才决定是否需要异步线程加载 [11/18/2013 frantqian]
				if (!pDataCache->CheckFree(pDataCache->m_wDataType, pDataCache->m_wPeriod))
				{
					g_LoadData.AddData(pDataCache);
				}
				
  				pDataRet = CreateData(wDataType, wPeriod, false);
  				if (pDataRet)
  				{
  					pDataRet->LoadData();
  					CMyThread::atomicInc(&(pDataRet->m_nUseCount));
  					pDataRet->Trace("UnHit");
 					//pDataRet->m_cIsNeedDel = 1;
  				}

				CMyThread::atomicDec(&(pDataCache->m_nUseCount));
			}
			else
			{
				pDataRet = pDataCache;
				pDataRet->Trace("Hit");
			}
		}

		pDataCache->ReleaseLock();
	}

	return pDataRet;
}


CData* CGoods::TryLoadData2(WORD wDataType, WORD wPeriod)
{
	CData* pDataRet = NULL;

	RWLock* pRWLock = GetWR(wDataType);
	if (pRWLock)
	{
		pRWLock->AcquireReadLock();	
		pDataRet = CreateData(wDataType, wPeriod, false);
		if (pDataRet)
		{
			pDataRet->LoadData();
		}
		pRWLock->ReleaseReadLock();
	}

	return pDataRet;
}

CData* CGoods::TryLoadToday()
{
	CDataDay* pDataRet = NULL;
	
	RWLock* pRWLock = GetWR(MIN_DATA);
    if (pRWLock)
    {
        pRWLock->AcquireReadLock();	
        pDataRet = (CDataDay *)CreateData(MIN_DATA, MIN_1, false);
        if (pDataRet)
        {
            pDataRet->LoadToday();
        }
		pRWLock->ReleaseReadLock();
	}
	return pDataRet;
}

// dynaCompress
int CGoods::GetPriceDiv2()
{
	if (m_nPriceDiv==0 || IsTF() || IsT() )
		return GetPriceDivide();
	else
		return m_nPriceDiv;
}

int CGoods::GetPriceDivide()
{
	if(IsSHB() || IsIndex() || m_cStation==2 || IsGZQH() || m_cStation == 5)
		return 1;
	else
		return 10;
}

int CGoods::GetPriceDivideOld()
{
	if (IsSHB() || IsIndex() || m_dwNameCode/100000 == 2)
		return 1;
	else
		return 10;
}

BOOL CGoods::IsVirtual()	// 集合竞价
{
	return	(m_pdwMMP[9]==m_pdwMMP[10] && m_pdwMMP[9]>0) &&
		(m_pxMMPVol[9].m_nBase==m_pxMMPVol[10].m_nBase && m_pxMMPVol[9].m_nBase>0) && 
		(m_pxMMPVol[8].m_nBase==0 || m_pxMMPVol[11].m_nBase==0) && 
		(m_pdwMMP[8]==0 && m_pdwMMP[11]==0);
}

void CGoods::SaveNewDynamicValue()
{
	MoveMemory(m_aDynamicValue, m_aDynamicValue+1, (NUMOF_DYNAS-1)*sizeof(CDynamicValue));
	SaveDataToDynamicValue(m_aDynamicValue[NUMOF_DYNAS-1]);
}

void CGoods::SaveDataToDynamicValue(CDynamicValue& dyna)
{
	ZeroMemory(&dyna, sizeof(CDynamicValue));

	DWORD PRICE_DIVIDE = GetPriceDiv2();

	dyna.m_dwCountValue = m_dwCountDynamic;
	dyna.m_dwTime = m_dwTime;

	dyna.m_dwOpen = g_MultiPrice(m_dwOpen, m_dwPriceDiv) / PRICE_DIVIDE;
	dyna.m_dwHigh = g_MultiPrice(m_dwHigh, m_dwPriceDiv) / PRICE_DIVIDE;
	dyna.m_dwLow = g_MultiPrice(m_dwLow, m_dwPriceDiv) / PRICE_DIVIDE;
	dyna.m_dwPrice = g_MultiPrice(m_dwPrice, m_dwPriceDiv) / PRICE_DIVIDE;

	dyna.m_xVolume = m_xVolume;
	dyna.m_xAmount = m_xAmount;
	dyna.m_dwTradeNum = m_dwTradeNum;
	dyna.m_xOpenInterest = m_xOpenInterest;

	BOOL bIndex = (m_cStation==2 || IsIndex());	// 指数或者板块指数
	BOOL bIsHK = IsHK();
	BOOL bIsGZQH = IsGZQH();

	if (bIndex==FALSE)
	{
		if (IsVirtual())
		{
			dyna.m_cVirtual = 1;

			dyna.m_pdwMMP[9] = dyna.m_pdwMMP[10] = g_MultiPrice(m_pdwMMP[10], m_dwPriceDiv)/PRICE_DIVIDE;
			dyna.m_pxMMPVol[9] = dyna.m_pxMMPVol[10] = m_pxMMPVol[10];
			if (m_pxMMPVol[11].m_nBase>0)
				dyna.m_pxMMPVol[8] = m_pxMMPVol[11];
			else
				dyna.m_pxMMPVol[11] = m_pxMMPVol[8];
		}
		else
		{
			dyna.m_cVirtual = 0;

			if (bIsHK)
			{
				dyna.m_dwNorminal = g_MultiPrice(m_dwNorminal, m_dwPriceDiv) / PRICE_DIVIDE;
				dyna.m_pdwMMP[9] = g_MultiPrice(m_pdwMMP[10], m_dwPriceDiv) / PRICE_DIVIDE;
				dyna.m_pdwMMP[10] = g_MultiPrice(m_pdwMMP[9], m_dwPriceDiv)/ PRICE_DIVIDE;
			}
			else
			{
				int NUM_OF_MMP = 5;
				if (bIsGZQH==FALSE && m_cLevel==2)
					NUM_OF_MMP = 10;

				int i, j;
				for(i = 9, j = 10; i >= 10-NUM_OF_MMP; i--, j++)			// buy price, vol
				{
					if(m_pdwMMP[j] > 0 && m_pxMMPVol[j].GetValue() > 0)
					{
						dyna.m_pdwMMP[i] = g_MultiPrice(m_pdwMMP[j], m_dwPriceDiv) / PRICE_DIVIDE;
						dyna.m_pxMMPVol[i] = m_pxMMPVol[j];

						dyna.m_cNumBuy++;
					}
					else
						break;
				}

				for(i = 10, j = 9; i < 10+NUM_OF_MMP; i++, j--)				// sell price, vol
				{
					if(m_pdwMMP[j] > 0 && m_pxMMPVol[j].GetValue() > 0)
					{
						dyna.m_pdwMMP[i] = g_MultiPrice(m_pdwMMP[j], m_dwPriceDiv) / PRICE_DIVIDE;
						dyna.m_pxMMPVol[i] = m_pxMMPVol[j];

						dyna.m_cNumSell++;
					}
					else
						break;
				}
			}
		}
	}

	dyna.m_xNeiPan = m_xNeiPan;
	dyna.m_xCurVol = m_xCurVol;
	dyna.m_cCurVol = m_cCurVol;
	dyna.m_xCurOI = m_xCurOI;

	if (m_dwPrice5==0)
		dyna.m_dwPrice5 = g_MultiPrice(m_dwClose, m_dwPriceDiv) / PRICE_DIVIDE;
	else
		dyna.m_dwPrice5 = g_MultiPrice(m_dwPrice5, m_dwPriceDiv) / PRICE_DIVIDE;

	if (bIsHK==FALSE && bIsGZQH==FALSE)
	{
		if (m_cLevel==2 || bIndex)
		{
			dyna.m_dwPBuy = m_dwBuyPrice;
			dyna.m_xVBuy = m_xBuyVol;
			dyna.m_dwPSell = m_dwSellPrice;
			dyna.m_xVSell = m_xSellVol;
		}

		dyna.m_nStrong = m_nStrong;

//		RWLock_scope_rdlock  rdlock(m_matrix.m_MatrixLock);
		dyna.m_ocSumOrder = m_matrix.m_ocSumOrder;
		dyna.m_ocSumTrade = m_matrix.m_ocSumTrade;
	}
}

void CGoods::LoadDataFromDynamicValue(CDynamicValue& dyna)
{
	InitDynamic();

	DWORD PRICE_DIVIDE = GetPriceDiv2();

	m_dwCountDynamic = dyna.m_dwCountValue;
	m_dwTime = dyna.m_dwTime;

	m_dwOpen = g_DivPrice(dyna.m_dwOpen, m_dwPriceDiv) * PRICE_DIVIDE;
	m_dwHigh = g_DivPrice(dyna.m_dwHigh, m_dwPriceDiv) * PRICE_DIVIDE;
	m_dwLow = g_DivPrice(dyna.m_dwLow, m_dwPriceDiv)* PRICE_DIVIDE;
	m_dwPrice = g_DivPrice(dyna.m_dwPrice, m_dwPriceDiv)* PRICE_DIVIDE;

	m_xVolume = dyna.m_xVolume;
	m_xAmount = dyna.m_xAmount;
	m_dwTradeNum = dyna.m_dwTradeNum;
	m_xOpenInterest = dyna.m_xOpenInterest;

	BOOL bIndex = (m_cStation==2 || IsIndex());	// 指数或者板块指数
	BOOL bIsHK = IsHK();
	BOOL bIsGZQH = IsGZQH();

	if (bIndex==FALSE)
	{
		if (dyna.m_cVirtual)
		{
			m_pdwMMP[9] = m_pdwMMP[10] = g_DivPrice(dyna.m_pdwMMP[10], m_dwPriceDiv)*PRICE_DIVIDE;
			m_pxMMPVol[9] = m_pxMMPVol[10] = dyna.m_pxMMPVol[10];
			if (dyna.m_pxMMPVol[11].m_nBase>0)
				m_pxMMPVol[8] = dyna.m_pxMMPVol[11];
			else
				m_pxMMPVol[11] = dyna.m_pxMMPVol[8];
		}
		else
		{
			if (bIsHK)
			{
				m_dwNorminal = g_DivPrice(dyna.m_dwNorminal, m_dwPriceDiv) * PRICE_DIVIDE;
				m_pdwMMP[9] = g_DivPrice(dyna.m_pdwMMP[10], m_dwPriceDiv)* PRICE_DIVIDE;
				m_pdwMMP[10] = g_DivPrice(dyna.m_pdwMMP[9], m_dwPriceDiv)* PRICE_DIVIDE;
			}
			else
			{
				int NUM_OF_MMP = 5;
				if (bIsGZQH==FALSE && m_cLevel==2)
					NUM_OF_MMP = 10;

				int i, j;
				for(i = 9, j = 10; i >= 10-NUM_OF_MMP; i--, j++)			// buy price, vol
				{
					if(dyna.m_pdwMMP[i] > 0 && dyna.m_pxMMPVol[i].GetValue() > 0)
					{
						m_pdwMMP[j] = g_DivPrice(dyna.m_pdwMMP[i], m_dwPriceDiv) * PRICE_DIVIDE;
						m_pxMMPVol[j] = dyna.m_pxMMPVol[i];
					}
					else
						break;
				}

				for(i = 10, j = 9; i < 10+NUM_OF_MMP; i++, j--)				// sell price, vol
				{
					if(dyna.m_pdwMMP[i] > 0 && dyna.m_pxMMPVol[i].GetValue() > 0)
					{
						m_pdwMMP[j] = g_DivPrice(dyna.m_pdwMMP[i], m_dwPriceDiv) * PRICE_DIVIDE;
						m_pxMMPVol[j] = dyna.m_pxMMPVol[i];
					}
					else
						break;
				}
			}
		}
	}

	m_xNeiPan = dyna.m_xNeiPan;
	m_xCurVol = dyna.m_xCurVol;
	m_cCurVol = dyna.m_cCurVol;
	m_xCurOI = dyna.m_xCurOI;

	if (dyna.m_dwPrice5==0)
		m_dwPrice5 = m_dwClose;
	else
		m_dwPrice5 = g_DivPrice(dyna.m_dwPrice5, m_dwPriceDiv) * PRICE_DIVIDE;

	if (bIsHK==FALSE && bIsGZQH==FALSE)
	{
		if (m_cLevel==2 || bIndex)
		{
			m_dwBuyPrice = dyna.m_dwPBuy;
			m_xBuyVol = dyna.m_xVBuy;
			m_dwSellPrice = dyna.m_dwPSell;
			m_xSellVol = dyna.m_xVSell;
		}

		m_nStrong = dyna.m_nStrong;

//		RWLock_scope_wrlock  wrlock(m_matrix.m_MatrixLock);
		m_matrix.m_ocSumOrder = dyna.m_ocSumOrder;
		m_matrix.m_ocSumTrade = dyna.m_ocSumTrade;
	}

	if (m_cStation==2 && m_dwNameCode/1000==2)
		CMyThread::atomicInc(&theApp.m_nCountCurBK);
}

int CGoods::ExpandDynamicValue(CBitStream& stream)
{
	CDynamicValue dynaDecode;
	CDynamicValue* pDyna = &dynaDecode;

	CDynamicValue* pOldDyna = m_aDynamicValue+NUMOF_DYNAS-1;
///???sjp20100420	if (pOldDyna->m_dwTime == 0)
///???sjp20100420		pOldDyna = NULL;

	int nRet;
	if (IsGZQH())
		nRet = CDynaCompress::ExpandDynaDataQH(stream, this, pDyna, pOldDyna);
	else if (IsHK())
		nRet = CDynaCompress::ExpandDynaDataH(stream, this, pDyna, pOldDyna);
	else
		nRet = CDynaCompress::ExpandDynaData(stream, this, pDyna, pOldDyna);
	if (nRet==0)
	{
		pDyna->m_dwCountValue = ++m_dwCountDynamic;

		LoadDataFromDynamicValue(*pDyna);

		MoveMemory(m_aDynamicValue, m_aDynamicValue+1, (NUMOF_DYNAS-1)*sizeof(CDynamicValue));
		CopyMemory(m_aDynamicValue+NUMOF_DYNAS-1, pDyna, sizeof(CDynamicValue));
	}

	return nRet;
}

// dynaCompress

void CGoods::NewGoods()
{
	LoadJBM();
}

void CGoods::LoadJBM()
{
	DWORD dwGoodsID = GetID();

	theApp.m_jbm.Get(dwGoodsID, m_jbm);

	if (m_jbm.m_dwGoodsID==dwGoodsID && m_jbm.m_dwDate>19890101)
	{
		m_xZFX = m_jbm.m_pxJBM[J_ZGB];
		if (m_cGroup==2 || m_cGroup==4)
			m_xLTG = m_jbm.m_pxJBM[J_BG];
		else
			m_xLTG = m_jbm.m_pxJBM[J_AG];

		if (m_jbm.m_fpd.m_cType == 'F') //基金处理
			m_xZFX = m_xLTG = m_jbm.m_pxJBM[J_AG];
	}
	else
	{
		//LOG_INFO("LoadJBM err:%d,%d,%d",m_jbm.m_dwGoodsID,dwGoodsID,m_jbm.m_dwDate);
	}

	theApp.m_dptj.Get(dwGoodsID, m_dptj);
}

BOOL CGoods::IsBad()
{
	if (m_dwFirstDate==0)
		return TRUE;

	if (m_dwFirstDate==GetDateValue() && m_dwPrice==0)
		return TRUE;

	if (m_cStation==2 || m_cStation == 4)	// 板块, 股指期货
	{
		if (theApp.m_dwTimeValue>94000 && m_xVolume.m_nBase==0)
			return TRUE;
	}

	//////////////////////////////////////////////////////////////////////////
	// 解决东华测试（300354）无法被搜索到的问题 [9/24/2012 frantqian]
	if (GetName().IsEmpty() || GetID() == 0)
		return TRUE;

	if (GetName().Find("测试") >= 0 && (m_dwNameCode != 300354))
		return TRUE;
	//////////////////////////////////////////////////////////////////////////

	return FALSE;
}

CString CGoods::Name2String(char* pcName)
{
	char pcBuf[9];

	memcpy(pcBuf, pcName, 8);
	pcBuf[8] = '\0';
	
	return pcBuf;
}

//////////////////////////////////////////////////////////////////////


int CGoods::CompressQuote(CBitStream& stream, DWORD dwLastCount, DWORD dwCount, BOOL bL2Encode, BOOL bNeedMMP)
{
	CDynamicValue* pDyna = NULL;
	CDynamicValue* pOldDyna = NULL;
	for (int i=NUMOF_DYNAS-1; i>=0; i--)
	{
		CDynamicValue* pD = m_aDynamicValue+i;

		if (dwCount>0 && pD->m_dwCountValue==dwCount)
			pDyna = pD;
		else if (dwLastCount>0 && pD->m_dwCountValue==dwLastCount)
			pOldDyna = pD;
	}

	if (pDyna == NULL)
		return ERR_COMPRESS_NODATA;

	return CDynaCompress::CompressQuote(stream, this, pDyna, pOldDyna, bL2Encode, bNeedMMP);
}

int CGoods::LoadDay(CArrayFDayMobile& aFDay, WORD wDataType, WORD wPeriod, char isNeedCQ)
{
	int nDay = 0;
	CFDayMobile* pFDay = NULL;

	CDataDay* pData = (CDataDay*)TryLoadData(wDataType, wPeriod);
	if (pData)
	{
		pData->AcquireLock();

		if (pData->m_cLoadOK == 1 && pData->m_dwDay > 0)
		{
			nDay = pData->m_dwDay;
			pFDay = new(std::nothrow) CFDayMobile[nDay];
			if (pFDay)
			{
				for (int i=0; i<nDay; i++)
				{
					pFDay[i] = Day2Mobile(pData->m_pDay[i]);
				}
			}
		}

		pData->ReleaseLock();

		pData->FreeData();
	}
 
	if (pFDay)
	{
		if(isNeedCQ == 0)// 1为下除权处理 [11/12/2012 frantqian]
		{
			// 日线下除权处理 [11/9/2012 frantqian]
			CSortArray<CFHSP> aFHSP;
			theApp.m_fhsp.Get(GetID(), aFHSP);
			int nFHSP = int(aFHSP.GetSize());
			if (nFHSP>0)
			{
				double fCQA = 1;
				double fCQB = 0;
				DWORD dwSaveDate = 0;
				DWORD dwLastDate = 0;

				int nF = nFHSP-1;
				int nCQ = -1;

				for (int i=nDay-1; i>=0; i--)
				{					
					DWORD dwDate;
					if (wDataType==DAY_DATA)
						dwDate = pFDay[i].m_dwTime;
					else
						dwDate = 20000000+pFDay[i].m_dwTime/10000;
					if (dwLastDate==0)
						dwLastDate = dwDate;

					if (dwSaveDate!=dwDate)
					{
						dwSaveDate = dwDate;

						if (nCQ>=0 && i>0 && pFDay[i].m_fClose>0)
						{
							double fA, fB;

							aFHSP[nCQ].DoPriceCQ(2, fA, fB);

							fCQB += fCQA*fB;
							fCQA *= fA;

							nCQ = -1;
						}
						else 
						{
							for (; nF>=0; nF--)
							{
								CFHSP& fhsp = aFHSP[nF];

								if (fhsp.m_dwDate>dwLastDate)
									continue;

								if (fhsp.m_dwDate<dwDate)
									break;
								else
								{
									if (fhsp.m_dwDate==dwDate)
										nCQ = nF;
									else
									{
										double fA, fB;
										fhsp.DoPriceCQ(2, fA, fB);

										fCQB += fCQA*fB;
										fCQA *= fA;
									}
								}
							}
						}
					}

					pFDay[i].m_fOpen  = pFDay[i].m_fOpen *fCQA+fCQB;
					pFDay[i].m_fHigh  = pFDay[i].m_fHigh *fCQA+fCQB;
					pFDay[i].m_fLow   = pFDay[i].m_fLow  *fCQA+fCQB;
					pFDay[i].m_fClose = pFDay[i].m_fClose*fCQA+fCQB;

					pFDay[i].m_fVolume /= fCQA;

					for (int j=0; j<3; j++)
					{
						pFDay[i].m_fVolOfBuy[j] /= fCQA;
						pFDay[i].m_fVolOfSell[j] /= fCQA;
					}

					//fd.m_fNeiPan /= fCQA;
				}	
			}

			aFHSP.RemoveAll();
		}
		else if (isNeedCQ == 1)
		{
			// 日线上除权处理 [11/9/2012 frantqian]
			CSortArray<CFHSP> aXFHSP;
			theApp.m_fhsp.Get(GetID(), aXFHSP);
			int nXFHSP = int(aXFHSP.GetSize());
			if (nXFHSP>0)
			{
				double fCQA = 1;
				double fCQB = 0;
				DWORD dwSaveDate = 0;
				DWORD dwLastDate = 0;

				int nF = 0;
				int nCQ = -1;

				for (int i=0; i <= nDay-1; i++)
				{					
					DWORD dwDate;
					if (wDataType==DAY_DATA)
						dwDate = pFDay[i].m_dwTime;
					else
						dwDate = 20000000+pFDay[i].m_dwTime/10000;

					if (dwLastDate==0)
						dwLastDate = dwDate;

					if (dwSaveDate!=dwDate)
					{
						dwSaveDate = dwDate;

						if (nCQ>=0 && i>0 && pFDay[i].m_fClose>0)
						{
							double fA, fB;

							aXFHSP[nCQ].DoPriceCQ(1, fA, fB);

							fCQB += fCQA*fB;
							fCQA *= fA;

							nCQ = -1;
						}
						else 
						{
							for (; nF<nXFHSP; nF++)
							{
								CFHSP& fhsp = aXFHSP[nF];

								if (fhsp.m_dwDate<dwLastDate)
									continue;

								if (fhsp.m_dwDate>dwDate)
									break;
								else
								{
									if (fhsp.m_dwDate==dwDate)
										nCQ = nF;
									else
									{
										double fA, fB;
										fhsp.DoPriceCQ(1, fA, fB);

										fCQB += fCQA*fB;
										fCQA *= fA;
									}
								}
							}
						}
					}

					pFDay[i].m_fOpen  = pFDay[i].m_fOpen *fCQA+fCQB;
					pFDay[i].m_fHigh  = pFDay[i].m_fHigh *fCQA+fCQB;
					pFDay[i].m_fLow   = pFDay[i].m_fLow  *fCQA+fCQB;
					pFDay[i].m_fClose = pFDay[i].m_fClose*fCQA+fCQB;

					pFDay[i].m_fVolume /= fCQA;

					for (int j=0; j<3; j++)
					{
						pFDay[i].m_fVolOfBuy[j] /= fCQA;
						pFDay[i].m_fVolOfSell[j] /= fCQA;
					}

					//fd.m_fNeiPan /= fCQA;
				}	
			}

			aXFHSP.RemoveAll();

		}

		// 周线，月线除权处理,这部分还要算 [11/9/2012 frantqian]
		CFDayMobile fd, fdLast;
		for (int i=0; i<nDay; i++)
		{
			if (i==0)
				fd = pFDay[i];
			else if (InSamePeriod(fdLast.m_dwTime, pFDay[i].m_dwTime, wDataType, wPeriod)==FALSE)
			{
				if (fd.m_dwTime>0 && fd.m_fOpen>0 && fd.m_fHigh>0 && fd.m_fLow>0 && fd.m_fClose>0)
					aFDay.Add(fd);

				fd = pFDay[i];
			}
			else
			{
				fd.m_dwTime = pFDay[i].m_dwTime; 
				fd.m_fClose = pFDay[i].m_fClose; 
				if (fd.m_fHigh<pFDay[i].m_fHigh)
					fd.m_fHigh = pFDay[i].m_fHigh; 
				if (fd.m_fLow>pFDay[i].m_fLow)
					fd.m_fLow = pFDay[i].m_fLow; 
				fd.m_fVolume += pFDay[i].m_fVolume;	
				fd.m_fAmount += pFDay[i].m_fAmount;// 计算周，月的成交金额 [5/10/2013 frantqian]
				
				fd.m_dwNumOfBuy += pFDay[i].m_dwNumOfBuy;
				fd.m_dwNumOfSell += pFDay[i].m_dwNumOfSell;
				for (int j = 0; j < 3; j++)
				{
					fd.m_fAmtOfBuy[j] += pFDay[i].m_fAmtOfBuy[j];
					fd.m_fAmtOfSell[j] += pFDay[i].m_fAmtOfSell[j];
					fd.m_fVolOfBuy[j] += pFDay[i].m_fVolOfBuy[j];
					fd.m_fVolOfSell[j] += pFDay[i].m_fVolOfSell[j];
				}
			}

			fdLast = pFDay[i];
		}

		if (fd.m_dwTime>0 && fd.m_fOpen>0 && fd.m_fHigh>0 && fd.m_fLow>0 && fd.m_fClose>0)
		{
			// 60分钟K线最后一根时间，切换后显示错误问题处理 [8/1/2013 frantqian]
			if (wDataType == MIN_DATA)
			{
				WORD wPos = this->Min2Pos(WORD(fd.m_dwTime%10000), FALSE);
				fd.m_dwTime = fd.m_dwTime/10000*10000+this->Pos2Min(wPos/wPeriod*wPeriod+wPeriod-1, FALSE);
			}
			aFDay.Add(fd);
		}

		delete [] pFDay;
		pFDay = 0;
	}

	return aFDay.GetSize();
}

int CGoods::LoadToday(CArrayFDayMobile& aFDay)
{
	int nDay = 0;
	CFDayMobile* pFDay = NULL;

	CDataDay* pData = (CDataDay*)TryLoadToday();
	if (pData)
	{
		pData->AcquireLock();

		if (pData->m_dwDay > 0)
		{
			nDay = pData->m_dwDay;
			pFDay = new(std::nothrow) CFDayMobile[nDay];
			if (pFDay)
			{
				for (int i=0; i<nDay; i++)
				{
					pFDay[i] = Day2Mobile(pData->m_pDay[i]);
				}
			}
		}

		pData->ReleaseLock();

		pData->FreeData();
	}
 
	if (pFDay)
	{
        CFDayMobile fd, fdLast;
        for (int i=0; i<nDay; i++)
        {
            if (i==0)
                fd = pFDay[i];
            else if (InSamePeriod(fdLast.m_dwTime, pFDay[i].m_dwTime, MIN_DATA, MIN_1)==FALSE)
            {
                if (fd.m_dwTime>0 && fd.m_fOpen>0 && fd.m_fHigh>0 && fd.m_fLow>0 && fd.m_fClose>0)
                    aFDay.Add(fd);

                fd = pFDay[i];
            }
            else
            {
                fd.m_dwTime = pFDay[i].m_dwTime;
                fd.m_fClose = pFDay[i].m_fClose;
                if (fd.m_fHigh<pFDay[i].m_fHigh)
                    fd.m_fHigh = pFDay[i].m_fHigh;
                if (fd.m_fLow>pFDay[i].m_fLow)
                    fd.m_fLow = pFDay[i].m_fLow;
                fd.m_fVolume += pFDay[i].m_fVolume;
                fd.m_fAmount += pFDay[i].m_fAmount;// 计算周，月的成交金额 [5/10/2013 frantqian]

                fd.m_dwNumOfBuy += pFDay[i].m_dwNumOfBuy;
                fd.m_dwNumOfSell += pFDay[i].m_dwNumOfSell;
                for (int j = 0; j < 3; j++)
                {
                    fd.m_fAmtOfBuy[j] += pFDay[i].m_fAmtOfBuy[j];
                    fd.m_fAmtOfSell[j] += pFDay[i].m_fAmtOfSell[j];
                    fd.m_fVolOfBuy[j] += pFDay[i].m_fVolOfBuy[j];
                    fd.m_fVolOfSell[j] += pFDay[i].m_fVolOfSell[j];
                }
            }

            fdLast = pFDay[i];
        }

        if (fd.m_dwTime>0 && fd.m_fOpen>0 && fd.m_fHigh>0 && fd.m_fLow>0 && fd.m_fClose>0)
        {
			WORD wPos = this->Min2Pos(WORD(fd.m_dwTime%10000), FALSE);
			fd.m_dwTime = fd.m_dwTime/10000*10000+this->Pos2Min(wPos/MIN_1*MIN_1+MIN_1-1, FALSE);
			aFDay.Add(fd);
        }


		delete [] pFDay;
		pFDay = 0;
	}

	return aFDay.GetSize();
}

bool CGoods::IsClosed(DWORD dwHHMMSS)
{	
	if (theApp.g_dwSysWeek == 0 || theApp.g_dwSysWeek == 6)
		return true;
	else if (IsGZQH())
		return (dwHHMMSS<=91200 || dwHHMMSS>=151800 || (dwHHMMSS>=113200 && dwHHMMSS<=125800));
	else if (m_cStation == 3)//AH股
		return (dwHHMMSS<=91300 || dwHHMMSS>=160200 || (dwHHMMSS>=120200 && dwHHMMSS<=125800));
	else if (m_cStation == 5)//延时港股
		return (dwHHMMSS<=92800 || dwHHMMSS>=161700 || (dwHHMMSS>=121700 && dwHHMMSS<=131300));
	else
		return (dwHHMMSS<=91300 || dwHHMMSS>=150200 || (dwHHMMSS>=113200 && dwHHMMSS<=125800));
}

bool CGoods::IsClosed(char cStation, DWORD dwHHMMSS)
{
	if (theApp.g_dwSysWeek == 0 || theApp.g_dwSysWeek == 6)
		return true;
	else if (cStation == 4)
		return (dwHHMMSS<=91200 || dwHHMMSS>=151800 || (dwHHMMSS>=113200 && dwHHMMSS<=125800));
	else if (cStation == 3)//AH股
		return (dwHHMMSS<=91300 || dwHHMMSS>=160200 || (dwHHMMSS>=120200 && dwHHMMSS<=125800));
	else if (cStation == 5)//延时港股
		return (dwHHMMSS<=92800 || dwHHMMSS>=161700 || (dwHHMMSS>=121700 && dwHHMMSS<=131300));
	else
		return (dwHHMMSS<=91300 || dwHHMMSS>=150200 || (dwHHMMSS>=113200 && dwHHMMSS<=125800));
}

bool CGoods::InMobileGroup(char cGroup)
{
	//0:指数 1:上证Ａ股 2:上证Ｂ股 3:深证Ａ股 4:深证Ｂ股
	//5:上证基金 6:深证基金 7:上证债券 8:深证债券 9:创业证券(中小板)
	//10:全部股票 11:全部Ａ股 12:权证 13:香港H股 
	//14:概念板块 15:行业板块  16:地区板块
	//17:开放基金 18:所有板块(概念+行业+地区+热点) 19:股指期货  
	//23: 三板板块 三板市场的股票代码一般是4字开头的，老三板(A股)股票是400开头的，B股股票是420开头的(B股统一用美元作交易货币)，新三板股票是430开头的。
	//25：三级概念板块
	
	if (cGroup < 10)
	{
		if (cGroup == 3)
			return (m_hMaskGroup&(GROUP_SZ_A|GROUP_SZ_ZXB|GROUP_SZ_CYB)) && IsBad() == FALSE;
		else if (cGroup == 7)
			return (m_hMaskGroup&(GROUP_SH_ZQ|GROUP_SH_ZZ|GROUP_SH_HG));
		else if (cGroup == 8)
			return (m_hMaskGroup&(GROUP_SZ_ZQ|GROUP_SZ_ZZ));
		else
			return m_cGroup==cGroup && IsBad() == FALSE;
	}
	else if (cGroup == 10)
		return true;
	else if (cGroup == 11)
		return m_hMaskGroup&GROUP_ALL_A;
	else if (cGroup == 12)
		return false;//m_dwGroup&GROUP_QZ;权证已经没有了
	else if (cGroup == 13)//A+H
		return m_hMaskGroup&GROUP_HK;
	else if (cGroup == 14)
		return m_hMaskGroup&GROUP_GNBK && IsBad() == FALSE;
	else if (cGroup == 15)
		return m_hMaskGroup&GROUP_HYBK;
	else if (cGroup == 16)
		return m_hMaskGroup&GROUP_DQBK;
	else if (cGroup == 17)
	{
		DWORD dwGoodsID = GetID();
		return (dwGoodsID>=519000 && dwGoodsID<=519999) || (dwGoodsID>=1150000 && dwGoodsID<=1169999);
	}
	else if (cGroup == 18)
		return (m_hMaskGroup&GROUP_GNBK && IsBad() == FALSE) || m_hMaskGroup&GROUP_HYBK || m_hMaskGroup&GROUP_DQBK ||  m_hMaskGroup&GROUP_RDBK;
	else if (cGroup == 19)
		return m_hMaskGroup&GROUP_GZQH && IsBad() == FALSE;
	else if (cGroup == 21)//H
		return m_hMaskGroup&GROUP_HKZB;
	else if (cGroup == 22)
		return m_hMaskGroup&GROUP_SZ_CYB;
	else if (cGroup == 23)//三板市场的股票代码一般是4字开头的，老三板(A股)股票是400开头的，B股股票是420开头的(B股统一用美元作交易货币)，新三板股票是430开头的。
		return m_hMaskGroup&GROUP_SZ_STB;
	else if (cGroup == 25)//H
		return m_hMaskGroup&GROUP_RDBK;
	//三级小概念与三级小行业begin
	else if (cGroup == 45)//
		return m_hMaskGroup&GROUP_XGNBK;
	else if (cGroup == 46)//H
		return m_hMaskGroup&GROUP_XHYBK;
	//三级小概念与三级小行业end
	else if (cGroup == _group_tf)
		return m_hMaskGroup&GROUP_TF && IsBad() == FALSE;
	else if (cGroup == _group_xsb )//新三板
		return m_hMaskGroup&GROUP_SZ_XSB;
	else
		return false;
}

bool CGoods::QuerySystemGroup(CBuffer& buf)
{
	std::map<short, std::string> mapSysBK;
	//if (m_cGroup == 1 || m_cGroup == 3 || m_cGroup == 9)
	//	mapSysBK[0] = "沪深A股";
	if (m_cGroup == 1)
		mapSysBK[1] = "上证A股";
	if (m_cGroup == 3)
		mapSysBK[3] = "深证A股";
	if (m_cGroup == 14)
		mapSysBK[-14] = "创业板";
	if (m_cGroup == 9)
		mapSysBK[9] = "中小板";
	if (m_cGroup == 12)
		mapSysBK[12] = "权证";
	if (m_cGroup == 5)
		mapSysBK[5] = "上证基金";
	if (m_cGroup == 6)
		mapSysBK[6] = "深证基金";
	if (m_cGroup == 7)
		mapSysBK[7] = "上证债券";
	if (m_cGroup == 8)
		mapSysBK[8] = "深证债券";
	if (m_cGroup == 2)
		mapSysBK[2] = "上证B股";
	if (m_cGroup == 4)
		mapSysBK[4] = "深证B股";
	DWORD dwGoodsID = GetID();
	if ((dwGoodsID>=519000 && dwGoodsID<=519999) || (dwGoodsID>=1150000 && dwGoodsID<=1169999))
		mapSysBK[17] = "开放基金";
	if (m_hMaskGroup&GROUP_GZQH)
		mapSysBK[19] = "股指期货";
	if (m_cGroup == 13)
		mapSysBK[20] = "AH股";
	if (m_cStation == 5)
		mapSysBK[21] = "香港主板";
	if (m_cGroup == 0)
		mapSysBK[11] = "指数";

	short nSys = mapSysBK.size();
	buf.WriteShort(nSys);
	buf.WriteUnicodeString(std::string("系统板块"));
	for (std::map<short, std::string>::const_iterator cit = mapSysBK.begin(); cit != mapSysBK.end(); ++cit)
	{
		buf.WriteShort(cit->first);
		buf.WriteUnicodeString(cit->second);
	}
	
	return true;
}

bool CGoods::QueryDefineGroup(CBuffer& buf)
{
	m_rwBK.AcquireReadLock();
	
	short sIndex = m_mapBK.size();
	short sContent = m_mapSubBK.size();
	if (sIndex == sContent)
	{
		buf.WriteShort(sIndex);
		std::map<int, std::string, std::greater<int> >::const_iterator cit = m_mapBK.begin();
		for (; cit != m_mapBK.end(); ++cit)
		{
			buf.WriteShort(cit->first);
			buf.WriteUnicodeString(cit->second);

			buf.WriteShort(m_mapSubBK[cit->first].size());
			for (std::set<std::string>::const_iterator citSub = m_mapSubBK[cit->first].begin(); citSub != m_mapSubBK[cit->first].end(); ++citSub)
			{
				buf.WriteUnicodeString(*citSub);
			}
		}
	}
	else
	{
		buf.WriteShort(0);
	}

	m_rwBK.ReleaseReadLock();

	return true;
}

void CGoods::AddBKInfo(int BKType, const std::string& strClass, const std::string& strGroup)
{
	m_rwBK.AcquireWriteLock();

	m_mapBK[BKType] = strClass;
	m_mapSubBK[BKType].insert(strGroup);

	m_rwBK.ReleaseWriteLock();
}

void CGoods::ClearBKInfo()
{
	m_rwBK.AcquireWriteLock();

	m_mapBK.clear();
	m_mapSubBK.clear();

	m_rwBK.ReleaseWriteLock();
}

void CGoods::GetCurDayNews(DWORD& dwTime, INT64& llNewsID)
{
	dwTime = 0;
	llNewsID = 0;
}

char* g_pPYDM[] = {  "啊澳ＡA", "芭怖ＢB", "擦错ＣC", "搭堕ＤD", "蛾贰ＥE",
					 "发咐ＦF", "噶过ＧG", "哈祸ＨH", "击骏ＪJ", "喀阔ＫK",
					 "垃络ＬL", "妈穆ＭM", "拿诺ＮN", "哦沤ＯO", "啪瀑ＰP",
					 "期群ＱQ", "然弱ＲR", "撒所ＳS", "塌唾ＴT", "挖误ＷW",
					 "昔迅ＸX", "压孕ＹY", "匝座ＺZ" } ;

void CGoods::AddPY(char cPY)
{
	int nPY = int(m_aPY.GetSize());
	for (int i=0; i<nPY; i++)
		m_aPY[i] += cPY;
}

void CGoods::AddPY(CByteArray& aChar)
{
	int nChar = int(aChar.GetSize());
	if (nChar==1)
		AddPY(char(aChar[0]));
	else if (nChar>1)
	{
		CStringArray aOldPY;
		aOldPY.Copy(m_aPY);
		m_aPY.RemoveAll();

		int nPY = int(aOldPY.GetSize());

		for (int i=0; i<nChar; i++)
		{
			char cChar = char(aChar[i]);
			for (int j=0; j<nPY; j++)
			{
				CString strPY = aOldPY[j];
				strPY += cChar;
				m_aPY.Add(strPY);
			}
		}
	}
}

void CGoods::MakePY()
{
	m_aPY.RemoveAll();
	CString strEmpty = "";
	m_aPY.Add(strEmpty);
	
	int nHZ = int(theApp.m_aHZ.GetSize());
	int nDYZ = int(theApp.m_aDYZ.GetSize());

	if ( strlen(m_pcName) == 0 )
	{
		memcpy(m_pcName,GetName(),LEN_STOCKNAME);
		m_pcName[LEN_STOCKNAME] = '\0';
	}

	for (short s=0; s<LEN_STOCKNAME; s++)
	{
		if (m_pcName[s]==0)
			break;
		else if (m_pcName[s]>0)
		{
			if ((m_pcName[s]>='A' && m_pcName[s]<='Z') || (m_pcName[s]>='0' && m_pcName[s]<='9'))
				AddPY(m_pcName[s]);
			else if (m_pcName[s]>='a' && m_pcName[s]<='z')
				AddPY(char(m_pcName[s]-'a'+'A'));
		}
		else if (s<LEN_STOCKNAME-1)
		{
			CByteArray aChar;

			BOOL bFound = FALSE;
			for (int i=0; i<23; i++)
			{
				if (strncmp(g_pPYDM[i], m_pcName+s, 2)<=0 &&
						strncmp(&(g_pPYDM[i][2]), m_pcName+s, 2)>=0)
				{
					aChar.Add(g_pPYDM[i][6]);
					bFound = TRUE;
					break;
				}
				else if (strncmp(&(g_pPYDM[i][4]), m_pcName+s, 2)==0)
				{
					aChar.Add(g_pPYDM[i][6]);
					bFound = TRUE;
					break;
				}
			}

			CString str = "";
			str += m_pcName[s];
			str += m_pcName[s+1];

			if (bFound==FALSE)
			{
				for (int i=0; i<nHZ; i++)
				{
					CString& strHZ = theApp.m_aHZ[i];
					// 修改汉字匹配问题，防止由前一个汉字的后半段和后一个汉字的前半段拼起来的匹配情况，如珈这样的汉字 [1/6/2016 qianyifan]
					int nHzPos= strHZ.Find(str);
					if (strHZ.GetLength()>1 && nHzPos>=0 && 1 == nHzPos%2)
					{
						char c = strHZ[0];
						aChar.Add(c);
						break;
					}
				}
			}

			for (int i=0; i<nDYZ; i++)
			{
				CString& strDYZ = theApp.m_aDYZ[i];
				if (strDYZ.GetLength()>1 && strDYZ.Find(str)>=0)
				{
					char c = strDYZ[0];
					aChar.Add(c);
					break;
				}
			}
			
			AddPY(aChar);

			s++;
		}
	}
}

double CGoods::GetZDTBL()
{
	double fBL = 0.1;
	for (int i=0; i<8; i++)
	{
		if (m_pcName[i]=='S')
		{
			fBL = 0.05;
			break;
		}
	}

	return fBL;
}

BYTE CDynamicValue::GetDynaChecksumOld(BOOL bIsIndex, BOOL bIsSimple)
{
	CDynamicValue dyna;

	if (bIsIndex)
	{
		dyna.m_dwTime = m_dwTime;

		dyna.m_dwOpen = m_dwOpen;
		dyna.m_dwHigh = m_dwHigh;
		dyna.m_dwLow = m_dwLow;
		dyna.m_dwPrice = m_dwPrice;

		dyna.m_xVolume = m_xVolume;
		dyna.m_xAmount = m_xAmount;
		dyna.m_dwTradeNum = m_dwTradeNum;

		dyna.m_xVBuy = m_xVBuy;
		dyna.m_xVSell = m_xVSell;

		dyna.m_dwPBuy = m_dwPBuy;
		dyna.m_dwPSell = m_dwPSell;

		dyna.m_xCurVol = m_xCurVol;
	}
	else if (bIsSimple)
	{
		dyna.m_dwTime = m_dwTime;

		dyna.m_dwPrice = m_dwPrice;

		for(int i = 8; i < 12; i++)
			dyna.m_pxMMPVol[i] = m_pxMMPVol[i];
	}
	else
		CopyMemory(&dyna, this, sizeof(CDynamicValue));

	PBYTE pcStart = PBYTE(&(dyna.m_dwTime));
	PBYTE pcEnd = PBYTE(&(dyna.m_ocSumOrder));

	DWORD dwSum = 0;

	for(PBYTE pc = pcStart; pc < pcEnd; pc++)
		dwSum += (*pc);

	return (BYTE)(dwSum % 256);
}



BYTE CDynamicValue::GetChecksumPush(BOOL bQH, WORD wVersion)
{
	BYTE cCheckSum = 0;

	DWORD* pdw = &m_dwTime;
	for (int i=0; i<7; i++)
		cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

	if (bQH)
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwTradeNum), 4);
	else
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xOpenInterest), 4);

	return cCheckSum;
}


void CDynamicValue::CreateOldDynamic(CDynamicValue* pDyna, int nDiv, int nDivOld)
{
	CopyMemory(this, pDyna, sizeof(CDynamicValue));

	m_dwOpen = pDyna->m_dwOpen*nDiv/nDivOld;
	m_dwHigh = pDyna->m_dwHigh*nDiv/nDivOld;
	m_dwLow = pDyna->m_dwLow*nDiv/nDivOld;
	m_dwPrice = pDyna->m_dwPrice*nDiv/nDivOld;

	for (int i=0; i<20; i++)
		m_pdwMMP[i] = pDyna->m_pdwMMP[i]*nDiv/nDivOld;

	m_dwPrice5 = pDyna->m_dwPrice5*nDiv/nDivOld;
}

void CDynamicValue::Multi(DWORD dwDiv)
{
	if (dwDiv<=1)
		return;

	m_dwOpen *= dwDiv;
	m_dwHigh *= dwDiv;
	m_dwLow *= dwDiv;
	m_dwPrice *= dwDiv;

	for (int i=0; i<20; i++)
	{
		m_pdwMMP[i] *= dwDiv;
	}

	m_dwPrice5 *= dwDiv;

	m_dwAvePrice *= dwDiv;

	m_dwNorminal *= dwDiv;
}

void CDynamicValue::Div(DWORD dwDiv)
{
	if (dwDiv<=1)
		return;

	m_dwOpen = g_DivPrice(m_dwOpen, dwDiv);
	m_dwHigh = g_DivPrice(m_dwHigh, dwDiv);
	m_dwLow = g_DivPrice(m_dwLow, dwDiv);
	m_dwPrice = g_DivPrice(m_dwPrice, dwDiv);

	for (int i=0; i<20; i++)
	{
		m_pdwMMP[i] = g_DivPrice(m_pdwMMP[i], dwDiv);
	}

	m_dwPrice5 = g_DivPrice(m_dwPrice5, dwDiv);

	m_dwAvePrice = g_DivPrice(m_dwAvePrice, dwDiv);

	m_dwNorminal = g_DivPrice(m_dwNorminal, dwDiv);
}

BYTE CDynamicValue::GetChecksum6(BOOL bNeedMMP)
{
	BYTE cCheckSum = 0;

	cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwDate), 4);

	DWORD* pdw = &m_dwTime;
	for (int i=0; i<9; i++)
		cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

	if (bNeedMMP)
	{
		for (int i=0; i<20; i++)
		{
			cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwMMP+i), 4);
			cCheckSum += GetBytesCheckSum((PBYTE)(m_pxMMPVol+i), 4);
		}
	}

	pdw = &m_dwPBuy;
	for (int i=0; i<6; i++)
		cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

	cCheckSum += GetBytesCheckSum((PBYTE)(&m_cCurVol), 1);
	cCheckSum += GetBytesCheckSum((PBYTE)(&m_xCurOI), 4);
	cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwPrice5), 4);
	cCheckSum += GetBytesCheckSum((PBYTE)(&m_nStrong), 4);

	cCheckSum += m_ocSumOrder.GetChecksum(TRUE, TRUE);
	cCheckSum += m_ocSumTrade.GetChecksum(TRUE, FALSE);

	cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwNorminal), 4);

	cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwAvePrice), 4);

	return cCheckSum;
}

void CDynamicValue::CheckMMP(int nNum)
{
	if (nNum<10)
	{
		for (int i=0; i<10-nNum; i++)
		{
			m_pdwMMP[i] = 0;
			m_pxMMPVol[i] = 0;
		}

		for (int i=10+nNum; i<20; i++)
		{
			m_pdwMMP[i] = 0;
			m_pxMMPVol[i] = 0;
		}
	}
}

void CDynamicValue::DeletePV()
{
	m_dwPBuy = 0;
	m_xVBuy = 0;
	m_dwPSell = 0;
	m_xVSell = 0;
}

void CDynamicValue::DeleteOC()
{
	m_ocSumOrder.Clear();
	m_ocSumTrade.Clear();
}

CIndex::CIndex()
{
	InitDay();
}

void CIndex::InitDay()
{
	m_cRD = 0;
	ZeroMemory(m_plRD, 10*4);

	m_wRise = 0;
	m_wEqual = 0;
	m_wFall = 0;

	m_wRN = 0;
	m_wDN = 0;
	m_wRD = 0;
}

void CIndex::Read(CBuffer& buffer)
{
	m_wRise = buffer.ReadShort();
	m_wEqual = buffer.ReadShort();
	m_wFall = buffer.ReadShort();

	m_wRN = buffer.ReadShort();
	m_wDN = buffer.ReadShort();
	m_wRD = buffer.ReadShort();

	buffer.Read(&m_cRD, 1);
	for (char c=0; c<m_cRD; c++)
		buffer.Read(m_plRD+c, 4);
}

void CIndex::Write(CBuffer& buffer)
{
	buffer.WriteShort(m_wRise);
	buffer.WriteShort(m_wEqual);
	buffer.WriteShort(m_wFall);

	buffer.WriteShort(m_wRN);
	buffer.WriteShort(m_wDN);
	buffer.WriteShort(m_wRD);

	buffer.Write(&m_cRD, 1);
	for (char c=0; c<m_cRD; c++)
		buffer.Write(m_plRD+c, 4);
}
#if 0
void CGoods::HisMinFromOld()
{
	if (IsHK())
		return;

	CString str;
	if (m_cStation==1)
		str.Format("data\\sznse\\HisMin\\%06d.hmn", m_dwNameCode);
	else if (m_cStation==2)
		str.Format("data\\pk\\HisMin\\%06d.hmn", m_dwNameCode);
	else
		str.Format("data\\shase\\HisMin\\%06d.hmn", m_dwNameCode);

	DWORD dwDate, dwClose, dwHigh, dwLow;
	CBaseMinOld pOld[241];

	WORD wVolMulti;
	if (IsIndex() || IsSHZQ())
		wVolMulti = 1;
	else
		wVolMulti = 100;

	CGoodsCode gcGoodsID = GetGoodsCode();

	CHisMinDF* pHMDF = NULL;
	CHisMinArray aHisMin;

	TRY
	{
		CFile file;
		if (file.Open(theApp.m_strSharePath+str, CFile::modeRead|CFile::shareDenyWrite))
		{
			long lSizePerDay = sizeof(CBaseMinOld)*241l+16;
			long lLength = (long)file.GetLength();
		
			if (lLength%lSizePerDay==0)
			{
				short sDay = short(lLength/lSizePerDay);
				for (short s=0; s<sDay; s++)
				{
					file.Read(&dwDate, 4);
					file.Read(&dwClose, 4);
					file.Read(&dwHigh, 4);
					file.Read(&dwLow, 4);

					DWORD dwYYMMDD0000 = dwDate%1000000*10000;

					file.Read(pOld, sizeof(CBaseMinOld)*241l);

					CHisMinDF* pDF = theApp.GetHisMinDF(dwDate);
					if (pDF==NULL)
						continue;

					if (pHMDF!=pDF)
					{
						if (pHMDF)
							pHMDF->m_dfHisMin.SaveData(gcGoodsID, aHisMin, 0, FALSE);
						
						pHMDF = pDF;
						pHMDF->m_dfHisMin.LoadData(gcGoodsID, aHisMin);
					}

					CHisMin hm;
					INT64 hVolume, hVolumeSum, hAmountSum, hVolumeSumLast;

					for (short i=0; i<240; i++)
					{
						hm.m_dwTime = dwYYMMDD0000+Pos2Min(i, FALSE);
						hm.m_dwPrice = pOld[i+1].m_dwPrice;

						hVolumeSum = pOld[i+1].m_dwVolume;
						if (hVolumeSum==NULL)
							continue;

						if (i==0)
							hVolume = hVolumeSum;
						else
							hVolume = hVolumeSum-hVolumeSumLast;

						hVolumeSumLast = hVolumeSum;
						hAmountSum = pOld[i+1].m_dwAmount;

//						hVolume *= wVolMulti;
//						hVolumeSum *= wVolMulti;
						hAmountSum *= 10000;

						if (hVolumeSum>0)
						{
							hm.m_dwAve = DWORD(hAmountSum/hVolumeSum);
							if (hm.m_dwAve<dwClose/10 || hm.m_dwAve>dwClose*10)
								hm.m_dwAve = hm.m_dwPrice;
						}
						else if (i==0)
							hm.m_dwAve = dwClose;
						hm.m_xVolume = hVolume;
			
						aHisMin.Change(hm);
					}

					hm.m_dwTime = 9999+dwYYMMDD0000;
					hm.m_dwPrice = dwClose;
					hm.m_dwAve = dwHigh;
					hm.m_xVolume.SetRawData(dwLow);
					aHisMin.Change(hm);
				}
			}
			file.Close();
		}
	}
	CATCH_ALL(e)
	{
	}
	END_CATCH_ALL

	if (pHMDF)
		pHMDF->m_dfHisMin.SaveData(gcGoodsID, aHisMin, 0, FALSE);
}


void CGoods::HisMinFromMin1()
{
	CGoodsCode gcGoodsID = GetGoodsCode();

	CMyArray<CDay, CDay&> aMin1;

	if (theApp.m_dfMin1.LoadData(gcGoodsID, aMin1)!=0)
		return;

	CHisMinDF* pHMDF = NULL;
	CHisMinArray aHisMin;
	
	DWORD dwDate, dwDateLast = 0;
	DWORD dwAveLast = 0;

	int nMin1 = int(aMin1.GetSize());
	for (int i=0; i<nMin1; i++)
	{
		CDay& min1 = aMin1[i];
		
		dwDate = 20000000+min1.m_dwTime/10000;	// YYMMDDhhmm->20YYMMDD

		if (dwDateLast!=dwDate)
		{
			dwDateLast = dwDate;
			dwAveLast = 0;

			CHisMinDF* pDF = theApp.GetHisMinDF(dwDate);
			if (pDF==NULL)
				continue;

			if (pHMDF!=pDF)
			{
				if (pHMDF)
					pHMDF->m_dfHisMin.SaveData(gcGoodsID, aHisMin, 0, FALSE);
				
				pHMDF = pDF;
				pHMDF->m_dfHisMin.LoadData(gcGoodsID, aHisMin);
			}
		}

		if (pHMDF==NULL)
			continue;

		CHisMin hm;
		hm.m_dwTime = min1.m_dwTime;
		int nFind = aHisMin.Find(hm);
		if (nFind>=0)
			aHisMin[nFind].m_xZJJL = min1.m_pxAmtOfBuy[2]+min1.m_pxAmtOfBuy[1]-min1.m_pxAmtOfSell[2]-min1.m_pxAmtOfSell[1];
/*
		hm.m_dwPrice = min1.m_dwClose;

		hVolume = min1.m_xVolume;
		hAmount = min1.m_xAmount;
		if (hVolume>0)
		{
			hm.m_dwAve = DWORD(hAmount/hVolume);
			if (hm.m_dwAve<hm.m_dwPrice/10 || hm.m_dwAve>hm.m_dwPrice*10)
				hm.m_dwAve = hm.m_dwPrice;
		}
		else if (dwAveLast==0)
			hm.m_dwAve = hm.m_dwPrice;
		else
			hm.m_dwAve = dwAveLast;

		hm.m_xVolume = hVolume;
		hm.m_xZJJL = min1.m_pxAmtOfBuy[2]+min1.m_pxAmtOfBuy[1]-min1.m_pxAmtOfSell[2]-min1.m_pxAmtOfSell[1];

		aHisMin.Change(hm);
*/
	}

	if (pHMDF)
		pHMDF->m_dfHisMin.SaveData(gcGoodsID, aHisMin, 0, FALSE);
}
#endif

void CGoods::CheckMainTrack()
{
}

//////////////////////////////////////////////////////////////////////////
void CGoods::SetBufferBase(PBYTE pcBase)
{
	m_pcBufferBase = (char*)pcBase;

 	m_pcBargainParam = m_pcBufferBase;
 	ReadBargainParam();
 	m_pBargainBuffer = (CBargain*)(m_pcBargainParam+32);
 	m_wBargainBuffer = (GOODS_PAGESIZE-32)/sizeof(CBargain);
 
	m_pcMinuteParam = m_pcBufferBase+GOODS_PAGESIZE;
	ReadMinuteParam();
	m_pMinuteBuffer = (CMinute*)(m_pcMinuteParam+32);
	m_wMinuteBuffer = (GOODS_PAGESIZE-32)/sizeof(CMinute);
}

void CGoods::LoadDayFromSpecialFile( void* pDF )
{

	CDataDay* pData = new CDataDay;
	if (pData)
	{
		pData->m_pGoods = this;
		pData->m_wDataType = DAY_DATA;
		pData->m_wPeriod = 1;

		pData->LoadDayFromSpecialFile(pDF);
		pData->SaveData();
	}
	delete pData;
	
}


//Steven New Begin
///20120327 sjp 解决新股停牌没横线问题
void CGoods::AddEmptyMinute()	//CHisCompress::ExpandMinute没收到aMinute，但CData需要AddEmptyMinute
{
	if (m_pDataMinute0 && m_pDataMinute0->m_cLoadOK==1)
		((CDataMinute*)m_pDataMinute0)->AddEmptyMinute();
	if (m_pDataMinute1 && m_pDataMinute1->m_cLoadOK==1)
		((CDataMinute*)m_pDataMinute1)->AddEmptyMinute();
	if (m_pDataMin30 && m_pDataMin30->m_cLoadOK==1)
		((CDataDay*)m_pDataMin30)->AddEmptyMinute();
	if (m_pDataMin5 && m_pDataMin5->m_cLoadOK==1)
		((CDataDay*)m_pDataMin5)->AddEmptyMinute();
	if (m_pDataMin1 && m_pDataMin1->m_cLoadOK==1)
		((CDataDay*)m_pDataMin1)->AddEmptyMinute();
}
///20120327 sjp 解决新股停牌没横线问题

BOOL CGoods::IsQH()
{
	return IsGZQH() || IsSPQH() || m_cSecurityType==OPTIONS;
}

BOOL CGoods::IsSPQH()
{
	return m_cStation==7 || m_cStation==8;
}

BOOL CGoods::IsQH(DWORD dwGoodsID)
{
	return IsGZQH(dwGoodsID) || IsSPQH(dwGoodsID);
}

BOOL CGoods::IsQH(char cStation)
{
	return (cStation==4 || cStation==7 || cStation==8); // 期货
}

BOOL CGoods::IsSPQH(DWORD dwGoodsID)
{
	char cStation = (char)(dwGoodsID/1000000);
	return cStation==7 || cStation==8;
}

int CGoods::ExpandDynamicValue6(CBitStream& stream)
{
	CDynamicValue dynaDecode;
	CDynamicValue* pDyna = &dynaDecode;

	CDynamicValue* pOldDyna = m_aDynamicValue+NUMOF_DYNAS-1;
	///???sjp20100420	if (pOldDyna->m_dwTime == 0)
	///???sjp20100420		pOldDyna = NULL;

	int nRet = CDynaCompress::ExpandDynaData6(stream, this, pDyna, pOldDyna);
	if (nRet==0)
	{
		pDyna->m_dwCountValue = ++m_dwCountDynamic;

		LoadDataFromDynamicValue(*pDyna);

		MoveMemory(m_aDynamicValue, m_aDynamicValue+1, (NUMOF_DYNAS-1)*sizeof(CDynamicValue));
		CopyMemory(m_aDynamicValue+NUMOF_DYNAS-1, pDyna, sizeof(CDynamicValue));
	}

	return nRet;
}

//////////////////////////////////////////////////////////////////////////
// 停牌信息 [3/20/2013 frantqian]
void CGoods::CheckSuspendInfo()
{
	if (m_SuspendInfo.m_dwInitDay != CMDSApp::g_dwSysYMD)
	{
		m_SuspendInfo.clear();
		m_SuspendInfo.m_dwInitDay = CMDSApp::g_dwSysYMD;
	}
}

void CGoods::CalcWeek52Info()
{
	DWORD PRICE_DIVIDE = GetPriceDiv2();
	//比对当天价格
	if (0 != m_dwHigh &&  m_dwWeekHigh52 < m_dwHigh*PRICE_DIVIDE)
		m_dwWeekHigh52 = m_dwHigh*PRICE_DIVIDE;

	if (0 != m_dwLow && m_dwWeekLow52 > m_dwLow*PRICE_DIVIDE)
		m_dwWeekLow52 = m_dwLow*PRICE_DIVIDE;

	// 每日只刷一次 [2/26/2013 frantqian]
	if (m_dwCalcTime == theApp.m_dwDateValue)
	{
		return;
	}
	m_dwCalcTime = theApp.m_dwDateValue;

	CArrayFDayMobile m_aFDay;
	WORD m_wDataType = DAY_DATA;
	WORD m_wPeriod = WEEK_PERIOD;
	LoadDay(m_aFDay, m_wDataType, m_wPeriod, 0);

	CFDayMobile* pFDay = m_aFDay.m_pData;
	int nDay = m_aFDay.GetSize();
	int nCalcNum = nDay>WEEKNUMPERYEAR?WEEKNUMPERYEAR:nDay;

	if (nDay>0 && pFDay!=NULL)
	{
		m_dwWeekHigh52 = (DWORD)((pFDay[nDay-1].m_fHigh*1000.0+0.5)*PRICE_DIVIDE);
		m_dwWeekLow52 = (DWORD)((pFDay[nDay-1].m_fLow*1000.0+0.5)*PRICE_DIVIDE);

		for (int i = nDay; i > nDay - nCalcNum; i--)
		{
			if (m_dwWeekHigh52 < (DWORD)(pFDay[i-1].m_fHigh*1000.0+0.5)*PRICE_DIVIDE)
				m_dwWeekHigh52 = (DWORD)(pFDay[i-1].m_fHigh*1000.0+0.5)*PRICE_DIVIDE;

			if (m_dwWeekLow52 >  (DWORD)(pFDay[i-1].m_fLow*1000.0+0.5)*PRICE_DIVIDE)
				m_dwWeekLow52 = (DWORD)(pFDay[i-1].m_fLow*1000.0+0.5)*PRICE_DIVIDE;
		}
	}
}

int CGoods::LoadDay_op( CArrayFDayMobile& aFDay, WORD wDataType, WORD wPeriod, char isNeedCQ /*= 0*/,DWORD dwKNum, std::map<DWORD, DWORD>* pMapHSL )
{
	int nDay = 0;
	int nKNum = 0;
	CFDayMobile* pFDay = NULL;
	CArray<DWORD,DWORD &> aHisHSL;

	CDataDay* pData = (CDataDay*)TryLoadData(wDataType, wPeriod);
	if (pData)
	{
		pData->AcquireLock();

		if (pData->m_cLoadOK == 1 && pData->m_dwDay > 0)
		{
			nDay = pData->m_dwDay;
			nKNum = nDay>dwKNum?dwKNum:nDay;
			pFDay = new(std::nothrow) CFDayMobile[nKNum];
			if (pFDay)
			{
				aHisHSL.SetSize(nKNum);
				int nOffset = nDay - nKNum;
				for (int i=0; i<nKNum; i++)
				{
					pFDay[i] = Day2Mobile(pData->m_pDay[i+nOffset]);
					aHisHSL[i] = pData->m_aHisHSL[i+nOffset];
				}
			}
		}

		pData->ReleaseLock();

		pData->FreeData();
	}

	if (pFDay)
	{
		if(isNeedCQ == 0)// 1为下除权处理 [11/12/2012 frantqian]
		{
			// 日线下除权处理 [11/9/2012 frantqian]
			CSortArray<CFHSP> aFHSP;
			theApp.m_fhsp.Get(GetID(), aFHSP);
			int nFHSP = int(aFHSP.GetSize());
			if (nFHSP>0)
			{
				double fCQA = 1;
				double fCQB = 0;
				DWORD dwSaveDate = 0;
				DWORD dwLastDate = 0;

				int nF = nFHSP-1;
				int nCQ = -1;

				for (int i=nKNum-1; i>=0; i--)
				{					
					DWORD dwDate;
					if (wDataType==DAY_DATA)
						dwDate = pFDay[i].m_dwTime;
					else
						dwDate = 20000000+pFDay[i].m_dwTime/10000;
					if (dwLastDate==0)
						dwLastDate = dwDate;

					if (dwSaveDate!=dwDate)
					{
						dwSaveDate = dwDate;

						if (nCQ>=0 && i>0 && pFDay[i].m_fClose>0)
						{
							double fA, fB;

							aFHSP[nCQ].DoPriceCQ(2, fA, fB);

							fCQB += fCQA*fB;
							fCQA *= fA;

							nCQ = -1;
						}
						else 
						{
							for (; nF>=0; nF--)
							{
								CFHSP& fhsp = aFHSP[nF];

								if (fhsp.m_dwDate>dwLastDate)
									continue;

								if (fhsp.m_dwDate<dwDate)
									break;
								else
								{
									if (fhsp.m_dwDate==dwDate)
										nCQ = nF;
									else
									{
										double fA, fB;
										fhsp.DoPriceCQ(2, fA, fB);

										fCQB += fCQA*fB;
										fCQA *= fA;
									}
								}
							}
						}
					}

					pFDay[i].m_fOpen  = pFDay[i].m_fOpen *fCQA+fCQB;
					pFDay[i].m_fHigh  = pFDay[i].m_fHigh *fCQA+fCQB;
					pFDay[i].m_fLow   = pFDay[i].m_fLow  *fCQA+fCQB;
					pFDay[i].m_fClose = pFDay[i].m_fClose*fCQA+fCQB;

					//pFDay[i].m_fVolume /= fCQA;  //关掉对量的除权Liu yuguang 2016/Aug/24

					for (int j=0; j<3; j++)
					{
						pFDay[i].m_fVolOfBuy[j] /= fCQA;
						pFDay[i].m_fVolOfSell[j] /= fCQA;
					}

					//fd.m_fNeiPan /= fCQA;
				}	
			}

			aFHSP.RemoveAll();
		}
		else if (isNeedCQ == 1)
		{
			// 日线上除权处理 [11/9/2012 frantqian]
			CSortArray<CFHSP> aXFHSP;
			theApp.m_fhsp.Get(GetID(), aXFHSP);
			int nXFHSP = int(aXFHSP.GetSize());
			if (nXFHSP>0)
			{
				double fCQA = 1;
				double fCQB = 0;
				DWORD dwSaveDate = 0;
				DWORD dwLastDate = 0;

				int nF = 0;
				int nCQ = -1;

				for (int i=0; i <= nKNum-1; i++)
				{					
					DWORD dwDate;
					if (wDataType==DAY_DATA)
						dwDate = pFDay[i].m_dwTime;
					else
						dwDate = 20000000+pFDay[i].m_dwTime/10000;

					if (dwLastDate==0)
						dwLastDate = dwDate;

					if (dwSaveDate!=dwDate)
					{
						dwSaveDate = dwDate;

						if (nCQ>=0 && i>0 && pFDay[i].m_fClose>0)
						{
							double fA, fB;

							aXFHSP[nCQ].DoPriceCQ(1, fA, fB);

							fCQB += fCQA*fB;
							fCQA *= fA;

							nCQ = -1;
						}
						else 
						{
							for (; nF<nXFHSP; nF++)
							{
								CFHSP& fhsp = aXFHSP[nF];

								if (fhsp.m_dwDate<dwLastDate)
									continue;

								if (fhsp.m_dwDate>dwDate)
									break;
								else
								{
									if (fhsp.m_dwDate==dwDate)
										nCQ = nF;
									else
									{
										double fA, fB;
										fhsp.DoPriceCQ(1, fA, fB);

										fCQB += fCQA*fB;
										fCQA *= fA;
									}
								}
							}
						}
					}

					pFDay[i].m_fOpen  = pFDay[i].m_fOpen *fCQA+fCQB;
					pFDay[i].m_fHigh  = pFDay[i].m_fHigh *fCQA+fCQB;
					pFDay[i].m_fLow   = pFDay[i].m_fLow  *fCQA+fCQB;
					pFDay[i].m_fClose = pFDay[i].m_fClose*fCQA+fCQB;

					//pFDay[i].m_fVolume /= fCQA; //关掉对量的除权Liu yuguang 2016/Aug/24

					for (int j=0; j<3; j++)
					{
						pFDay[i].m_fVolOfBuy[j] /= fCQA;
						pFDay[i].m_fVolOfSell[j] /= fCQA;
					}

					//fd.m_fNeiPan /= fCQA;
				}	
			}

			aXFHSP.RemoveAll();

		}

		// 周线，月线除权处理,这部分还要算 [11/9/2012 frantqian]
		CFDayMobile fd, fdLast;
		DWORD dwHSL = 0;
		for (int i=0; i<nKNum; i++)
		{
			if (i==0)
			{
				fd = pFDay[i];
				dwHSL = aHisHSL[i];//(*pMapHSL)[fd.m_dwTime];
			}
			else if (InSamePeriod(fdLast.m_dwTime, pFDay[i].m_dwTime, wDataType, wPeriod)==FALSE)
			{
				if (fd.m_dwTime>0 && fd.m_fOpen>0 && fd.m_fHigh>0 && fd.m_fLow>0 && fd.m_fClose>0)
				{
					if( pMapHSL != NULL )
						(*pMapHSL)[fd.m_dwTime] = dwHSL;
					
					aFDay.Add(fd);
				}

				fd = pFDay[i];
				dwHSL = aHisHSL[i];//(*pMapHSL)[fd.m_dwTime];
			}
			else
			{
				fd.m_dwTime = pFDay[i].m_dwTime; 
				fd.m_fClose = pFDay[i].m_fClose; 
				if (fd.m_fHigh<pFDay[i].m_fHigh)
					fd.m_fHigh = pFDay[i].m_fHigh; 
				if (fd.m_fLow>pFDay[i].m_fLow)
					fd.m_fLow = pFDay[i].m_fLow; 
				fd.m_fVolume += pFDay[i].m_fVolume;	
				fd.m_fAmount += pFDay[i].m_fAmount;// 计算周，月的成交金额 [5/10/2013 frantqian]
				dwHSL += aHisHSL[i];//(*pMapHSL)[fd.m_dwTime];//计算周，月的换手率[2016/Aug/19 liu yuguang]

				fd.m_dwNumOfBuy += pFDay[i].m_dwNumOfBuy;
				fd.m_dwNumOfSell += pFDay[i].m_dwNumOfSell;
				for (int j = 0; j < 3; j++)
				{
					fd.m_fAmtOfBuy[j] += pFDay[i].m_fAmtOfBuy[j];
					fd.m_fAmtOfSell[j] += pFDay[i].m_fAmtOfSell[j];
					fd.m_fVolOfBuy[j] += pFDay[i].m_fVolOfBuy[j];
					fd.m_fVolOfSell[j] += pFDay[i].m_fVolOfSell[j];
				}
			}

			fdLast = pFDay[i];
		}

		if (fd.m_dwTime>0 && fd.m_fOpen>0 && fd.m_fHigh>0 && fd.m_fLow>0 && fd.m_fClose>0)
		{
			// 60分钟K线最后一根时间，切换后显示错误问题处理 [8/1/2013 frantqian]
			if (wDataType == MIN_DATA)
			{
				WORD wPos = this->Min2Pos(WORD(fd.m_dwTime%10000), FALSE);
				fd.m_dwTime = fd.m_dwTime/10000*10000+this->Pos2Min(wPos/wPeriod*wPeriod+wPeriod-1, FALSE);
			}

			if( pMapHSL != NULL ){
				(*pMapHSL)[fd.m_dwTime] = dwHSL;
			}
			aFDay.Add(fd);
		}

		delete [] pFDay;
		pFDay = 0;
	}

	return aFDay.GetSize();
}

DWORD CGoods::GetMinuteTime(BOOL bIncludeVirtual)
{
	return GetDateValue()%1000000*10000+Time2Min(GetTimeValue(), bIncludeVirtual);
}


XInt32 CGoods::GetHisLTG(CGoodsHisGB& hisGB, DWORD dwDate, XInt32 defValue)
{
	XInt32 xLTG = defValue;
	CGoodsHisGB ghgb;
	DWORD dwFindDate; //2011-10-18 wyj 找到的一个股本日期

	int nGB = (int)hisGB.m_aHisGB.GetSize();

	if (nGB>0)
	{
		if (dwDate >= hisGB.m_aHisGB[0].m_dwDate)
		{
			int i = nGB -1;
			for(;i >= 0;i--)
			{
				dwFindDate  = hisGB.m_aHisGB[i].m_dwDate;
				if (dwFindDate <=dwDate) //2012-3-1 wyj 等于判断当日的流通股
				{
					xLTG = hisGB.m_aHisGB[i].m_xLTG;
					break;
				}
			}
		}
	}
	return xLTG;
}

//////////////////////////////////////////////////////////////////////////
CString CGoods::GetCode()
{
    CString str;

    if (IsIF())
        str.Format("IF%04d", m_dwNameCode);
    else if (IsTF())
        str.Format("TF%04d", m_dwNameCode%10000);
	else if (IsIH())
		str.Format("IH%04d", m_dwNameCode%10000);
	else if (IsIC())
		str.Format("IC%04d", m_dwNameCode%10000);
    else if (m_cStation==3)
        str.Format("H%04d", m_dwNameCode);
    else if (m_cStation==5)
    {
        DWORD dwID = GetID();

        long l = dwID / 1000;

        if ( l == 5500 || l == 5510 )
        {
            if ( m_pcCode[0] )
            {
                char pcCode[LEN_STOCKCODE];
                memcpy( pcCode, m_pcCode, LEN_STOCKCODE );

                char* p = strrchr( pcCode, '\n' );
                if ( p ) *p = '\0';

                return pcCode;
            }
            else
            {
                str.Format("HK%04d", m_dwNameCode);
            }
        }
        else
        {
            str.Format("HK%04d", m_dwNameCode);
        }
    }
    else if (m_cStation==2)
        str.Format("BK%04d", m_dwNameCode);
    else
        str.Format("%06d", m_dwNameCode);

    return str;
}

//////////////////////////////////////////////////////////////////////
int CGoods::GetGroup()
{

}

//////////////////////////////////////////////////////////////////////
CStaticOptions::CStaticOptions()
{
	m_cOptionType = ' ';
	m_cCallOrPut = ' ';

	m_dwExercisePrice = 0;

	m_dwExerciseDate = 0;
	m_dwDeliveryDate = 0;
	m_dwExpireDate = 0;
	m_cUpdateVersion = 0;

	m_hMarginUnit = 0;
	m_wMarginRatioParam1 = 0;
	m_wMarginRatioParam2 = 0;

	m_hLmtOrdMinFloor = 0;
	m_hLmtOrdMaxFloor = 0;
	m_hMktOrdMinFloor = 0;
	m_hMktOrdMaxFloor = 0;

	memset(m_pcStatusFlag, ' ', 8);
}

void CStaticOptions::Read(CBuffer& buf, WORD wVersion)
{
	m_gcBaseID.m_dwGoodsID = buf.ReadInt();
	if (g_IsNullID(m_gcBaseID.m_dwGoodsID))
		g_ReadCodeName(buf, m_gcBaseID.m_pcCode, LEN_STOCKCODE);
	else
		m_gcBaseID.m_pcCode[0] = 0;

	m_cOptionType = buf.ReadChar();
	m_cCallOrPut = buf.ReadChar();

	m_dwExercisePrice = buf.ReadInt();

	m_dwExerciseDate = buf.ReadInt();
	m_dwDeliveryDate = buf.ReadInt();
	m_dwExpireDate = buf.ReadInt();
	m_cUpdateVersion = buf.ReadChar();

	m_hMarginUnit = buf.ReadInt64();
	m_wMarginRatioParam1 = buf.ReadShort();
	m_wMarginRatioParam2 = buf.ReadShort();

	m_hLmtOrdMinFloor = buf.ReadInt64();
	m_hLmtOrdMaxFloor = buf.ReadInt64();
	m_hMktOrdMinFloor = buf.ReadInt64();
	m_hMktOrdMaxFloor = buf.ReadInt64();

	buf.Read(m_pcStatusFlag, 8);
}

void CStaticOptions::Write(CBuffer& buf, WORD wVersion)
{
	buf.WriteInt(m_gcBaseID.m_dwGoodsID);
	if (g_IsNullID(m_gcBaseID.m_dwGoodsID))
		g_WriteCodeName(buf, m_gcBaseID.m_pcCode, LEN_STOCKCODE);

	buf.WriteChar(m_cOptionType);
	buf.WriteChar(m_cCallOrPut);

	buf.WriteInt(m_dwExercisePrice);

	buf.WriteInt(m_dwExerciseDate);
	buf.WriteInt(m_dwDeliveryDate);
	buf.WriteInt(m_dwExpireDate);
	buf.WriteChar(m_cUpdateVersion);

	buf.WriteInt64(m_hMarginUnit);
	buf.WriteShort(m_wMarginRatioParam1);
	buf.WriteShort(m_wMarginRatioParam2);

	buf.WriteInt64(m_hLmtOrdMinFloor);
	buf.WriteInt64(m_hLmtOrdMaxFloor);
	buf.WriteInt64(m_hMktOrdMinFloor);
	buf.WriteInt64(m_hMktOrdMaxFloor);

	buf.Write(m_pcStatusFlag, 8);
}

CZyhgHandle::CZyhgHandle()
{
    // 参考质押回购时间计算.docx/"债券质押式逆回购及代码"图表
	// 注意深市在程序中以1开头
	
	CZyhg zyhgs[18] = {
		{1131810, 1, 0},
		{204001, 1, 0},
		{1131811, 2, 0},
		{204002, 2, 0},
		{1131800, 3, 0},
		{204003, 3, 0},
		{1131809, 4, 0},
		{204004, 4, 0},
		{1131801, 7, 0},
		{204007, 7, 0},
		{1131802, 14, 0},
		{204014, 14, 0},
		{1131803, 28, 0},
		{204028, 28, 0},
		{1131805, 91, 0},
		{204091, 91, 0},
		{1131806, 182, 0},
		{204182, 182, 0}
	};

	std::vector<CZyhg> __zyhglist(zyhgs, zyhgs+18);
	_zyhglist = __zyhglist;
    fillNhsyl();
}

void CZyhgHandle::fillNhsyl()
{
	// 对于质押式回购, "现价" 即为 "预期年化收益率"
    std::vector<CZyhg>::iterator iter;
    for (iter = _zyhglist.begin(); iter != _zyhglist.end(); iter++) {
        CGoods *pGoods = theApp.GetGoods(iter->id);
		if (pGoods)
			iter->nhsyl = pGoods->m_dwPrice;
    }
}

std::vector<CZyhg> CZyhgHandle::GetFullList()
{
	return _zyhglist;
}

std::vector<CZyhg> CZyhgHandle::GetBestList(bool bLt1w)
{
	std::vector<CZyhg> outputList;
	std::vector<CZyhg> tempList;
	if (bLt1w)
	{
		outputList.push_back(_zyhglist[0]);
		outputList.push_back(_zyhglist[2]);
		std::vector<CZyhg>::iterator iter;
		for (iter = _zyhglist.begin()+4; iter != _zyhglist.end(); iter++)
		{
			if (iter->id / 100 == 11318)
				tempList.push_back(*iter);
		}
		std::sort(tempList.begin(), tempList.end(), compNhsyl);
		outputList.push_back(tempList[0]);
		outputList.push_back(tempList[1]);
	}
	else
	{
		tempList.assign(_zyhglist.begin(), _zyhglist.begin()+4);
		std::sort(tempList.begin(), tempList.end(), compNhsyl);
		outputList.push_back(tempList[0]);
		outputList.push_back(tempList[1]);
		tempList.clear();
		tempList.assign(_zyhglist.begin()+4, _zyhglist.end());
		std::sort(tempList.begin(), tempList.end(), compNhsyl);
		outputList.push_back(tempList[0]);
		outputList.push_back(tempList[1]);
	}
	return outputList;
}

bool CZyhgHandle::compNhsyl(const CZyhg &s1, const CZyhg &s2)
{
	return s1.nhsyl > s2.nhsyl;
}
