
/************************************
  REVISION LOG ENTRY
    Revision By: 刘成柱
	  Revised on 2009-12-4 16:20:53
	    Comments: 跨平台的日期时间类
		 ************************************/
#include <time.h>
#include <assert.h>
#include "SysGlobal.h"
#include "AppGlobal.h"
#include "MyTime.h"

#define ATLASSERT assert

/////////////////////////////////////////////////////////////////////////////
// CMyTimeSpan
/////////////////////////////////////////////////////////////////////////////

 CMyTimeSpan::CMyTimeSpan()  :
	m_timeSpan(0)
{
}

 CMyTimeSpan::CMyTimeSpan( __time64_t time )  :
	m_timeSpan( time )
{
}

 CMyTimeSpan::CMyTimeSpan(int lDays, int nHours, int nMins, int nSecs) 
{
 	m_timeSpan = nSecs + 60* (nMins + 60* (nHours + (24) * lDays));
}

 LONGLONG CMyTimeSpan::GetDays() 
{
	return( m_timeSpan/(24*3600) );
}

 LONGLONG CMyTimeSpan::GetTotalHours() 
{
	return( m_timeSpan/3600 );
}

 int CMyTimeSpan::GetHours() 
{
	return( int( GetTotalHours()-(GetDays()*24) ) );
}

 LONGLONG CMyTimeSpan::GetTotalMinutes() 
{
	return( m_timeSpan/60 );
}

 int CMyTimeSpan::GetMinutes() 
{
	return( int( GetTotalMinutes()-(GetTotalHours()*60) ) );
}

 LONGLONG CMyTimeSpan::GetTotalSeconds() 
{
	return( m_timeSpan );
}

 int CMyTimeSpan::GetSeconds() 
{
	return( int( GetTotalSeconds()-(GetTotalMinutes()*60) ) );
}

 __time64_t CMyTimeSpan::GetTimeSpan() 
{
	return( m_timeSpan );
}

 CMyTimeSpan CMyTimeSpan::operator+( CMyTimeSpan span ) 
{
	return( CMyTimeSpan( m_timeSpan+span.m_timeSpan ) );
}

 CMyTimeSpan CMyTimeSpan::operator-( CMyTimeSpan span ) 
{
	return( CMyTimeSpan( m_timeSpan-span.m_timeSpan ) );
}

 CMyTimeSpan& CMyTimeSpan::operator+=( CMyTimeSpan span ) 
{
	m_timeSpan += span.m_timeSpan;
	return( *this );
}

 CMyTimeSpan& CMyTimeSpan::operator-=( CMyTimeSpan span ) 
{
	m_timeSpan -= span.m_timeSpan;
	return( *this );
}

 bool CMyTimeSpan::operator==( CMyTimeSpan span ) 
{
	return( m_timeSpan == span.m_timeSpan );
}

 bool CMyTimeSpan::operator!=( CMyTimeSpan span ) 
{
	return( m_timeSpan != span.m_timeSpan );
}

 bool CMyTimeSpan::operator<( CMyTimeSpan span ) 
{
	return( m_timeSpan < span.m_timeSpan );
}

 bool CMyTimeSpan::operator>( CMyTimeSpan span ) 
{
	return( m_timeSpan > span.m_timeSpan );
}

 bool CMyTimeSpan::operator<=( CMyTimeSpan span ) 
{
	return( m_timeSpan <= span.m_timeSpan );
}

 bool CMyTimeSpan::operator>=( CMyTimeSpan span ) 
{
	return( m_timeSpan >= span.m_timeSpan );
}


/////////////////////////////////////////////////////////////////////////////
// CTime
/////////////////////////////////////////////////////////////////////////////

 CMyTime CMyTime::GetCurrentTime() 
{
#ifdef WIN32
	return( CMyTime( ::_time64( 0 ) ) );
#else
	time_t curtime;
	(void) time(&curtime);
	return( CMyTime(curtime) );
#endif
}

 CMyTime::CMyTime()  :
	m_time(0)
{
}

 CMyTime::CMyTime( __time64_t time )  :
	m_time( time )
{
}

 CMyTime::CMyTime(int nYear, int nMonth, int nDay, int nHour, int nMin, int nSec,
	int nDST)
{
	struct tm atm;
	atm.tm_sec = nSec;
	atm.tm_min = nMin;
	atm.tm_hour = nHour;
	ATLASSERT(nDay >= 1 && nDay <= 31);
	atm.tm_mday = nDay;
	ATLASSERT(nMonth >= 1 && nMonth <= 12);
	atm.tm_mon = nMonth - 1;        // tm_mon is 0 based
	ATLASSERT(nYear >= 1900);
	atm.tm_year = nYear - 1900;     // tm_year is 1900 based
	atm.tm_isdst = nDST;
	m_time = kernel_mktime(&atm);
	ATLASSERT(m_time != -1);       // indicates an illegal input time
	
}





 CMyTime& CMyTime::operator=( __time64_t time ) 
{
	m_time = time;

	return( *this );
}

 CMyTime& CMyTime::operator+=( CMyTimeSpan span ) 
{
	m_time += span.GetTimeSpan();

	return( *this );
}

 CMyTime& CMyTime::operator-=( CMyTimeSpan span ) 
{
	m_time -= span.GetTimeSpan();

	return( *this );
}

 CMyTimeSpan CMyTime::operator-( CMyTime time ) 
{
	return( CMyTimeSpan( m_time-time.m_time ) );
}

 CMyTime CMyTime::operator-( CMyTimeSpan span ) 
{
	return( CMyTime( m_time-span.GetTimeSpan() ) );
}

 CMyTime CMyTime::operator+( CMyTimeSpan span ) 
{
	return( CMyTime( m_time+span.GetTimeSpan() ) );
}

 bool CMyTime::operator==( CMyTime time ) 
{
	return( m_time == time.m_time );
}

 bool CMyTime::operator!=( CMyTime time ) 
{
	return( m_time != time.m_time );
}

 bool CMyTime::operator<( CMyTime time ) 
{
	return( m_time < time.m_time );
}

 bool CMyTime::operator>( CMyTime time ) 
{
	return( m_time > time.m_time );
}

 bool CMyTime::operator<=( CMyTime time ) 
{
	return( m_time <= time.m_time );
}

 bool CMyTime::operator>=( CMyTime time ) 
{
	return( m_time >= time.m_time );
}



 struct tm* CMyTime::GetLocalTm(struct tm* ptm) 
{
	if (ptm != NULL)
	{
#ifdef _WIN32
		struct tm* ptmTemp = _localtime64(&m_time);
#else
		struct tm* ptmTemp = localtime_r(&m_time, &m_tms);
#endif
		if (ptmTemp == NULL)
			return NULL;    // indicates the m_time was not initialized!

		*ptm = *ptmTemp;
		return ptm;
	}
	else
#ifdef _WIN32
		return _localtime64(&m_time);
#else
		return localtime_r(&m_time, &m_tms);
#endif
}



 __time64_t CMyTime::GetTime() 
{
	return( m_time );
}

 int CMyTime::GetYear()
{ 
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? (ptm->tm_year) + 1900 : 0 ; 
}

 int CMyTime::GetMonth()
{ 
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? ptm->tm_mon + 1 : 0;
}

 int CMyTime::GetDay()
{
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? ptm->tm_mday : 0 ; 
}

 int CMyTime::GetHour()
{
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? ptm->tm_hour : -1 ; 
}

 int CMyTime::GetMinute()
{ 
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? ptm->tm_min : -1 ; 
}

 int CMyTime::GetSecond()
{ 
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? ptm->tm_sec : -1 ;
}

 int CMyTime::GetDayOfWeek()
{ 
	struct tm * ptm;
	ptm = GetLocalTm();
	return ptm ? ptm->tm_wday + 1 : 0 ;
}



