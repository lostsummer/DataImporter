// QMatrixCompress.cpp: implementation of the CQMatrixCompress class.
//
//////////////////////////////////////////////////////////////////////
#include "QMatrixCompress.h"
#include "MDS.h"

//#include "Global.h"

static CBitCode BC_QMATRIX_TIMEDIFF[] = 
{
	{0x06,	3,	'C',	0,		0},			// 110				= 0s
	{0x00,	1,	'C',	0,		3},			// 0				= 3s
	{0x02,	2,	'D',	4,		0},			// 10	+4Bit		= 4Bit
	{0x07,	3,	'O',	18,		0},			// 111	+18Bit		= 18Bit Org
};
static int BCNUM_QMATRIX_TIMEDIFF = sizeof(BC_QMATRIX_TIMEDIFF)/sizeof(CBitCode);

const DWORD MAX_QMATRIX_TIMEDIFF = 15;

static CBitCode BC_QMATRIX_TIMEINDEX[] = 
{
	{0x00,	1,	'C',	0,		0},			// 0				= 0
	{0x02,	2,	'C',	0,		1},			// 10				= 1
	{0x03,	2,	'O',	7,		0},			// 11	+7Bit		= 7Bit Org
};
static int BCNUM_QMATRIX_TIMEINDEX = sizeof(BC_QMATRIX_TIMEINDEX)/sizeof(CBitCode);

const DWORD MAX_QMATRIX_TIMEINDEX = 99;

static CBitCode BC_QMATRIX_NUMQ[] = 
{
	{0x00,	1,	'D',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'D',	6,		16},		// 10	+6Bit		= 6Bit+16
	{0x06,	3,	'D',	8,		80},		// 110	+8Bit		= 8Bit+64+16
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_QMATRIX_NUMQ = sizeof(BC_QMATRIX_NUMQ)/sizeof(CBitCode);

static CBitCode BC_QMATRIX_PRICEDIFF[] = 
{
	{0x00,	1,	'D',	2,		1},			// 0	+2bit		= 2bit+1
	{0x02,	2,	'D',	4,		5},			// 10	+4Bit		= 4Bit+4+1
	{0x06,	3,	'D',	8,		21},		// 110  +8Bit		= 8Bit+16+4+1
	{0x07,	3,	'O',	32,		0},			// 111	+32Bit		= 32Bit Org
};
static int BCNUM_QMATRIX_PRICEDIFF = sizeof(BC_QMATRIX_PRICEDIFF)/sizeof(CBitCode);

static CBitCode BC_QMATRIX_NUMORDERS[] = 
{
	{0x00,	1,	'D',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'D',	6,		16},		// 10	+6Bit		= 6Bit+16
	{0x06,	3,	'D',	8,		80},		// 110	+8Bit		= 8Bit+64+16
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_QMATRIX_NUMORDERS = sizeof(BC_QMATRIX_NUMORDERS)/sizeof(CBitCode);

static CBitCode BC_QMATRIX_ORDERVOL[] = 
{
	{0x00,	1,	'H',	4,		0},			// 0	+4Bit		= (4Bit)*100
	{0x02,	2,	'H',	8,		16},		// 10	+8Bit		= (8Bit+16)*100
	{0x0C,	4,	'H',	14,		272},		// 1100	+14Bit		= (14Bit+256+16)*100
	{0x0D,	4,	'D',	12,		0},			// 1101+12Bit		= 12Bit
	{0x0E,	4,	'D',	16,		4096},		// 1110+16Bit		= 16Bit+4096
	{0x1E,	5,	'D',	20,		69632},		// 11110+24Bit		= 20Bit+65536+4096
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_QMATRIX_ORDERVOL = sizeof(BC_QMATRIX_ORDERVOL)/sizeof(CBitCode);



static CBitCode BC_QMATRIX_PRICE[] = 
{
	{0x02,	2,	'D',	8,		0},			//10	+8Bit		= 8Bit
	{0x00,	1,	'D',   16,		256},		//0		+16Bit		= 16Bit+256
	{0x06,	3,	'D',   24,		65792},		//110	+24Bit		= 24Bit+65536+256
	{0x07,	3,	'O',   32,		0},			//111	+32Bit		= 32Bit Org
};
static int BCNUM_QMATRIX_PRICE = sizeof(BC_QMATRIX_PRICE)/sizeof(CBitCode);

//////////////////////////////////////////////////////////////////////

//static CBitCode BC_STATCOUNT_COUNT[] = 
//{
//	{0x02,	2,	'D',	10,		0},			// 10	+10Bit		= 10Bit
//	{0x00,	1,	'D',	12,		1024},		// 0	+12Bit		= 12Bit+1024
//	{0x03,	2,	'O',	16,		0},			// 11	+16Bit		= 16Bit Org
//};
//static int BCNUM_STATCOUNT_COUNT = sizeof(BC_STATCOUNT_COUNT)/sizeof(CBitCode);

static CBitCode BC_STATCOUNT_STATNUM[] = 
{
	{0x06,	3,	'D',	4,		0},			// 110	+4Bit		= 4Bit
	{0x00,	1,	'D',	8,		16},		// 0	+8Bit		= 8Bit+16
	{0x02,	2,	'D',	12,		272},		// 10	+12Bit		= 12Bit+256+16
	{0x0E,	4,	'D',	16,		4368},		// 1110	+16Bit		= 16Bit+4096+256+16
	{0x0F,	4,	'O',	24,		0},			// 1111	+24Bit		= 24Bit Org
};
static int BCNUM_STATCOUNT_STATNUM = sizeof(BC_STATCOUNT_STATNUM)/sizeof(CBitCode);

static CBitCode BC_STATCOUNT_STATVOL[] = 
{
	{0x0E,	4,	'C',	0,		0},			// 1110				= 0
	{0x02,	2,	'D',	12,		0},			// 10	+12Bit		= 12Bit
	{0x00,	1,	'D',	16,		4096},		// 0	+16Bit		= 16Bit+4096
	{0x06,	3,	'D',	20,		69632},		// 110	+20Bit		= 20Bit+65536+4096
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
static int BCNUM_STATCOUNT_STATVOL = sizeof(BC_STATCOUNT_STATVOL)/sizeof(CBitCode);

static CBitCode BC_STATCOUNT_MAXVOL[] = 
{
	{0x06,	3,	'D',	12,		0},			// 110	+12Bit		= 12Bit
	{0x00,	1,	'D',	16,		4096},		// 0	+16Bit		= 16Bit+4096
	{0x02,	2,	'D',	20,		69632},		// 10	+20Bit		= 20Bit+65536+4096
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_STATCOUNT_MAXVOL = sizeof(BC_STATCOUNT_MAXVOL)/sizeof(CBitCode);

//static CBitCode BC_STATCOUNT_COUNTDIFF[] = 
//{
//	{0x00,	1,	'C',	0,		1},			// 0				= 1
//	{0x02,	2,	'C',	0,		2},			// 10				= 2
//	{0x06,	3,	'D',	4,		0},			// 110	+4Bit		= 4Bit
//	{0x07,	3,	'O',	16,		0},			// 111	+16Bit		= 16Bit Org
//};
//static int BCNUM_STATCOUNT_COUNTDIFF = sizeof(BC_STATCOUNT_COUNTDIFF)/sizeof(CBitCode);

static CBitCode BC_STATCOUNT_STATNUMDIFF[] = 
{
	{0x00,	1,	'C',	0,		0},			// 0				= 0
	{0x02,	2,	'D',	4,		1},			// 10	+4Bit		= 4Bit+1
	{0x06,	3,	'D',	8,		17},		// 110	+8Bit		= 8Bit+16+1
	{0x0E,	4,	'D',	16,		273},		// 1110	+16Bit		= 16Bit+256+16+1
	{0x0F,	4,	'O',	24,		0},			// 1111	+24Bit		= 24Bit Org
};
static int BCNUM_STATCOUNT_STATNUMDIFF = sizeof(BC_STATCOUNT_STATNUMDIFF)/sizeof(CBitCode);

static CBitCode BC_STATCOUNT_STATVOLDIFF[] = 
{
	{0x00,	2,	'C',	0,		0},			// 00				= 0
	{0x01,	2,	'H',	6,		0},			// 01	+6Bit		= (6Bit)*100
	{0x02,	2,	'H',	12,		64},		// 10	+12Bit		= (12Bit+64)*100
	{0x06,	3,	'D',	16,		0},			// 110	+16Bit		= 16Bit
	{0x0E,	4,	'D',	20,		65536},		// 1110	+20Bit		= 20Bit+65536
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
static int BCNUM_STATCOUNT_STATVOLDIFF = sizeof(BC_STATCOUNT_STATVOLDIFF)/sizeof(CBitCode);

static CBitCode BC_STATCOUNT_MAXVOLDIFF[] = 
{
	{0x00,	1,	'C',	0,		0},			// 0				= 0
	{0x02,	2,	'H',	6,		0},			// 10	+6Bit		= (6Bit)*100
	{0x06,	3,	'D',	16,		0},			// 110	+16Bit		= 16Bit
	{0x0E,	4,	'D',	20,		65536},		// 1110	+20Bit		= 20Bit+65536
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
static int BCNUM_STATCOUNT_MAXVOLDIFF = sizeof(BC_STATCOUNT_MAXVOLDIFF)/sizeof(CBitCode);

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CQMatrixCompress::CQMatrixCompress()
{
	m_dwLastTimeBuy = 0;
	m_dwLastTimeSell = 0;

	m_dwMinPriceBuy = 0;
	m_dwMaxPriceSell = 0;
}

CQMatrixCompress::~CQMatrixCompress()
{

}

int CQMatrixCompress::CompressHisQ(CHisOrderQueue& hisQ, BOOL bBuySell1, DWORD dwLastTime, CBitStream& stream)
{
	// hisQ.m_dwTime
	CompressQTime(hisQ.m_dwTime, dwLastTime, stream);

	// hisQ.m_dwNumOfOrders
	stream.EncodeData(hisQ.m_dwNumOfOrders, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);

	// NumQOrder, Q orders
	int nNumQOrder = hisQ.m_adwOrderVol.size();
	stream.EncodeData(nNumQOrder, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);

	for(int i = 0; i < nNumQOrder; i++)
		stream.EncodeData(hisQ.m_adwOrderVol[i], BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);

	// buysell1
	if(bBuySell1)
	{
		stream.WriteDWORD(1, 1);

		CompressBuySell1(hisQ, stream);
	}
	else
		stream.WriteDWORD(0, 1);

	return 0;
}

int CQMatrixCompress::CompressBuySell1(CHisOrderQueue& hisQ, CBitStream& stream)
{
	// hisQ.m_dwOriginalVol, m_dwPartTradeVol, m_dwPartNewVol
	stream.EncodeData(hisQ.m_dwOriginalVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
	stream.EncodeData(hisQ.m_dwPartTradeVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
	stream.EncodeData(hisQ.m_dwPartNewVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);

	// hisQ.m_nNumOfRcntTrade, m_adwRcntTrade
	stream.WriteDWORD(hisQ.m_nNumOfRcntTrade, 4);	// <= 10
	for(int i = 0; i < hisQ.m_nNumOfRcntTrade; i++)
		stream.EncodeData(hisQ.m_adwRcntTrade[i], BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);

	// Order Status
	int nNumOrderStatus = hisQ.m_aOrderStatus.size();
	stream.EncodeData(nNumOrderStatus, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);

	for(int i = 0; i < nNumOrderStatus; i++)
	{
		COrderStatus& os = hisQ.m_aOrderStatus[i];

		stream.EncodeData(os.m_dwOrderVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
		stream.WriteDWORD(os.m_status, 3);
	}

	return 0;
}

int CQMatrixCompress::CompressQueueMatrix(CQueueMatrix& qMatrix, BOOL bActiveOnly, CBitStream& stream)
{
	DWORD dwMTimeBuy = qMatrix.m_dwDynamicTimeBuy;
	DWORD dwMTimeSell = qMatrix.m_dwDynamicTimeSell;

///???///	ASSERT(m_dwLastTimeBuy <= dwMTimeBuy && m_dwLastTimeSell <= dwMTimeSell);
	if(m_dwLastTimeBuy > dwMTimeBuy || m_dwLastTimeSell > dwMTimeSell)
		return ERR_COMPRESS_PARAM;

	if(m_dwLastTimeBuy == dwMTimeBuy && m_dwLastTimeSell == dwMTimeSell)
		return ERR_COMPRESS_NODATA;

	try
	{
		DWORD PRICE_DIVIDE = qMatrix.m_pGoods->GetPriceDiv2();
		if (PRICE_DIVIDE==1)
			stream.WriteDWORD(0, 1);
		else
			stream.WriteDWORD(1, 1);

		// bActiveOnly
		if(bActiveOnly)
			stream.WriteDWORD(1, 1);
		else
			stream.WriteDWORD(0, 1);

		for(int nLoop = 0; nLoop < 2; nLoop++)
		{
			DWORD dwLastTime = 0;
			DWORD dwMTime = 0;
			CHisOrderQueueSArray* psaHisOrder = NULL;

			PDWORD pdwMinMaxPrice = NULL;

			BYTE cBuySell = 0;

			if(nLoop == 0)
			{
				dwLastTime = m_dwLastTimeBuy;
				dwMTime = dwMTimeBuy;
				psaHisOrder = &qMatrix.m_saHisOrderBuyArray;

				pdwMinMaxPrice = &m_dwMinPriceBuy;

				cBuySell = ORDER_BID;
			}
			else
			{
				dwLastTime = m_dwLastTimeSell;
				dwMTime = dwMTimeSell;
				psaHisOrder = &qMatrix.m_saHisOrderSellArray;

				pdwMinMaxPrice = &m_dwMaxPriceSell;

				cBuySell = ORDER_OFFER;
			}

			CHisOrderQueueSArray& saHisOrder = *psaHisOrder;

			DWORD& dwMinMaxPrice = *pdwMinMaxPrice;

			if(dwLastTime < dwMTime)
			{
				// bHasQM
				stream.WriteDWORD(1, 1);

				// dwMTime
				DWORD dwLastT = dwLastTime/100;
				DWORD dwLastI = dwLastTime%100;
				stream.WriteDWORD(dwLastT, 18);
				stream.EncodeData(dwLastI, BC_QMATRIX_TIMEINDEX, BCNUM_QMATRIX_TIMEINDEX);

				CompressQTime(dwMTime, dwLastTime, stream);

				// Q array
				int nNumQ = saHisOrder.GetSize();
				if(nNumQ > theApp.m_nMaxNumOfHisQSend)
					nNumQ = theApp.m_nMaxNumOfHisQSend;

				int nNumQSend = nNumQ;
				if(bActiveOnly)
				{
					int i = 0;
					for(; i < nNumQ; i++)
					{
						CHisOrderQueue& hisQ = saHisOrder[i];

						if (hisQ.m_cActive==0)
							break;
					}

					nNumQSend = i;
				}

				// Q array - nNumQSend
				stream.EncodeData(nNumQSend, BC_QMATRIX_NUMQ, BCNUM_QMATRIX_NUMQ);

				DWORD dwLastQPrice = 0;
				for(int i = 0; i < nNumQSend; i++)
				{
					CHisOrderQueue& hisQ = saHisOrder[i];

					stream.WriteDWORD(hisQ.m_cActive, 1);

					// Q price
					DWORD dwPriceDiff = 0;

					DWORD dwQPrice = hisQ.m_dwPrice/PRICE_DIVIDE;

					if(dwLastQPrice == 0)
						dwPriceDiff = dwQPrice;
					else
					{
						if(hisQ.m_cBuySell == ORDER_BID)
							dwPriceDiff = dwLastQPrice - dwQPrice;
						else
							dwPriceDiff = dwQPrice - dwLastQPrice;
					}
					stream.EncodeData(dwPriceDiff, BC_QMATRIX_PRICEDIFF, BCNUM_QMATRIX_PRICEDIFF);

					dwLastQPrice = dwQPrice;

					BOOL bClientHasQ = FALSE;

					if(cBuySell == ORDER_BID)
					{
						if(dwMinMaxPrice > 0 && dwMinMaxPrice <= hisQ.m_dwPrice)
							bClientHasQ = TRUE;
					}
					else
					{
						if(dwMinMaxPrice >= hisQ.m_dwPrice)
							bClientHasQ = TRUE;
					}

					// Q data
					if(hisQ.m_dwTime > dwLastTime || !bClientHasQ)
					{
						// bHasQData
						stream.WriteDWORD(1, 1);
						CompressHisQ(hisQ, (i == 0), dwLastTime, stream);
					}
					else
					{
						stream.WriteDWORD(0, 1);

						if(i == 0)
							CompressBuySell1(hisQ, stream);
					}
				}

				if(nNumQSend > 0)
				{
					dwMinMaxPrice = saHisOrder[nNumQSend-1].m_dwPrice;
				}
				else
				{
					dwMinMaxPrice = 0;
				}

				stream.WriteDWORD(qMatrix.GetCheckSum(cBuySell, bActiveOnly), 8);
			}
			else
				stream.WriteDWORD(0, 1);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	m_dwLastTimeBuy = dwMTimeBuy;
	m_dwLastTimeSell = dwMTimeSell;

	return 0;
}

int CQMatrixCompress::ExpandHisQ(CHisOrderQueue& hisQ, DWORD dwLastTime, CBitStream& stream)
{
	// hisQ.m_dwTime
	hisQ.m_dwTime = ExpandQTime(dwLastTime, stream);

	// hisQ.m_dwNumOfOrders
	stream.DecodeData(hisQ.m_dwNumOfOrders, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);

	// NumQOrder, Q orders
	DWORD dwTemp = 0;
	stream.DecodeData(dwTemp, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);
	int nNumQOrder = (int)dwTemp;

	hisQ.m_adwOrderVol.resize(nNumQOrder);

	for(int i = 0; i < nNumQOrder; i++)
		stream.DecodeData(hisQ.m_adwOrderVol[i], BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);

	// buysell1
	BOOL bBuySell1 = stream.ReadDWORD(1);
	if(bBuySell1)
		ExpandBuySell1(hisQ, stream);

	return 0;
}

int CQMatrixCompress::ExpandBuySell1(CHisOrderQueue& hisQ, CBitStream& stream)
{
	// hisQ.m_dwOriginalVol, m_dwPartTradeVol, m_dwPartNewVol
	stream.DecodeData(hisQ.m_dwOriginalVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
	stream.DecodeData(hisQ.m_dwPartTradeVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
	stream.DecodeData(hisQ.m_dwPartNewVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);

	// hisQ.m_nNumOfRcntTrade, m_adwRcntTrade
	hisQ.m_nNumOfRcntTrade = (int)stream.ReadDWORD(4);
	for(int i = 0; i < hisQ.m_nNumOfRcntTrade; i++)
		stream.DecodeData(hisQ.m_adwRcntTrade[i], BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);

	// Order Status
	DWORD dwTemp = 0;
	stream.DecodeData(dwTemp, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);
	int nNumOrderStatus = (int)dwTemp;
	hisQ.m_aOrderStatus.resize(nNumOrderStatus);

	for(int i = 0; i < nNumOrderStatus; i++)
	{
		COrderStatus& os = hisQ.m_aOrderStatus[i];

		stream.DecodeData(os.m_dwOrderVol, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
		os.m_status = (ORDER_CMP)stream.ReadDWORD(3);
	}

	return 0;
}

int CQMatrixCompress::ClearHisQBuySell1(CHisOrderQueue& hisQ)
{
	hisQ.m_dwOriginalVol = 0;
	hisQ.m_dwPartTradeVol = 0;
	hisQ.m_dwPartNewVol = 0;

	hisQ.m_nNumOfRcntTrade = 0;

	hisQ.m_aOrderStatus.clear();

	return 0;
}

int CQMatrixCompress::ExpandQueueMatrix(CQueueMatrix& qMatrix, CBitStream& stream)
{
	try
	{
		DWORD PRICE_DIVIDE;
		char cDiv = (char)stream.ReadDWORD(1);
		if (cDiv==0)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;

		// bActiveOnly
		BOOL bActiveOnly = stream.ReadDWORD(1);

		for(int nLoop = 0; nLoop < 2; nLoop++)
		{
			// bHasQM
			BOOL bHasQM = stream.ReadDWORD(1);
			if(!bHasQM)
				continue;

			DWORD dwLastTime = 0;
			DWORD* pdwMTime = NULL;
			CHisOrderQueueSArray* psaHisOrder = NULL;
			BYTE cBuySell = 0;

			if(nLoop == 0)
			{
				dwLastTime = qMatrix.m_dwDynamicTimeBuy;
				pdwMTime = &qMatrix.m_dwDynamicTimeBuy;
				psaHisOrder = &qMatrix.m_saHisOrderBuyArray;

				cBuySell = ORDER_BID;
			}
			else
			{
				dwLastTime = qMatrix.m_dwDynamicTimeSell;
				pdwMTime = &qMatrix.m_dwDynamicTimeSell;
				psaHisOrder = &qMatrix.m_saHisOrderSellArray;

				cBuySell = ORDER_OFFER;
			}

			DWORD& dwMTime = *pdwMTime;
			CHisOrderQueueSArray& saHisOrder = *psaHisOrder;

			// dwMTime
			DWORD dwLastTimeExpand = stream.ReadDWORD(18);
			DWORD dwLastIndex = 0;
			stream.DecodeData(dwLastIndex, BC_QMATRIX_TIMEINDEX, BCNUM_QMATRIX_TIMEINDEX);

			dwLastTimeExpand = dwLastTimeExpand*100 + dwLastIndex;

//			if(dwLastTimeExpand != dwLastTime)
			if (dwLastTimeExpand!=0 && dwLastTimeExpand!=dwLastTime)
				return ERR_COMPRESS_SRCDATA;

			dwMTime = ExpandQTime(dwLastTimeExpand, stream);

			// Q array
			DWORD dwTemp = 0;
			stream.DecodeData(dwTemp, BC_QMATRIX_NUMQ, BCNUM_QMATRIX_NUMQ);
			int nNumQSend = (int)dwTemp;

			DWORD dwLastQPrice = 0;
			for(int i = 0; i < nNumQSend; i++)
			{
				char cActive = (char)stream.ReadDWORD(1);

				// Q price
				DWORD dwPriceDiff = 0;
				stream.DecodeData(dwPriceDiff, BC_QMATRIX_PRICEDIFF, BCNUM_QMATRIX_PRICEDIFF);

				DWORD dwQPrice = 0;
				if(dwLastQPrice == 0)
					dwQPrice = dwPriceDiff;
				else
				{
					if(cBuySell == ORDER_BID)
						dwQPrice = dwLastQPrice - dwPriceDiff;
					else
						dwQPrice = dwLastQPrice + dwPriceDiff;
				}

				dwLastQPrice = dwQPrice;

				DWORD dwHisQPrice = dwQPrice * PRICE_DIVIDE;

				// find Q
				int nQMSize = saHisOrder.GetSize();
				BOOL bFoundQ = FALSE;
				int nQMPos = i;
				for(; nQMPos < nQMSize; nQMPos++)
				{
					CHisOrderQueue& hQ = saHisOrder[nQMPos];

					if(hQ.m_dwPrice == dwHisQPrice)
					{
						bFoundQ = TRUE;
						break;
					}

					if(cBuySell == ORDER_BID && hQ.m_dwPrice < dwHisQPrice
							|| cBuySell == ORDER_OFFER && hQ.m_dwPrice > dwHisQPrice)
						break;
				}

				if(nQMPos > i)
					saHisOrder.RemoveAt(i, nQMPos-i);

				if(!bFoundQ)
				{
					CHisOrderQueue newQ;

					newQ.m_cBuySell = cBuySell;
					newQ.m_dwPrice = dwHisQPrice;
//					newQ.m_pQueueMatrix = &qMatrix;

					saHisOrder.InsertAt(i, newQ);
				}

				CHisOrderQueue& hisQ = saHisOrder[i];
				hisQ.m_cActive = cActive;

				BOOL bHasQData = stream.ReadDWORD(1);
				if(bHasQData)
//					ExpandHisQ(hisQ, dwLastTime, stream);
					ExpandHisQ(hisQ, dwLastTimeExpand, stream);
				else
				{
///???///					ASSERT(bFoundQ);
					if(!bFoundQ)
						return ERR_COMPRESS_SRCDATA;

					if(i == 0)
						ExpandBuySell1(hisQ, stream);
				}

				if(i != 0)
					ClearHisQBuySell1(hisQ);
			}

			int nQMSize = saHisOrder.GetSize();
			if(nQMSize > nNumQSend)
				saHisOrder.RemoveAt(nNumQSend, nQMSize - nNumQSend);

			BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);
			if(cStreamChkSum != qMatrix.GetCheckSum(cBuySell, bActiveOnly))
				return ERR_COMPRESS_CHECK;
		}

		qMatrix.m_dwCount++;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

void CQMatrixCompress::CompressQTime(DWORD dwTime, DWORD dwLastTime, CBitStream& stream)
{
	DWORD dwT = dwTime/100;
	DWORD dwI = dwTime%100;	

	DWORD dwLT = dwLastTime/100;

	DWORD dwSecondsDiff = 0;
	if(dwT >= dwLT)
	{
		dwSecondsDiff = GetSecondsDiff(dwLT, dwT);
		if(dwSecondsDiff > MAX_QMATRIX_TIMEDIFF)
			dwSecondsDiff = dwT;
	}
	else
		dwSecondsDiff = dwT;

	stream.EncodeData(dwSecondsDiff, BC_QMATRIX_TIMEDIFF, BCNUM_QMATRIX_TIMEDIFF);

	stream.EncodeData(dwI, BC_QMATRIX_TIMEINDEX, BCNUM_QMATRIX_TIMEINDEX);
}

DWORD CQMatrixCompress::ExpandQTime(DWORD dwLastTime, CBitStream& stream)
{
	DWORD dwTime = 0;

	DWORD dwSecondsDiff = 0;
	stream.DecodeData(dwSecondsDiff, BC_QMATRIX_TIMEDIFF, BCNUM_QMATRIX_TIMEDIFF);

	if(dwSecondsDiff > MAX_QMATRIX_TIMEDIFF)
		dwTime = dwSecondsDiff;
	else
		dwTime = AddSeconds(dwLastTime/100, dwSecondsDiff);

	DWORD dwTimeIndex = 0;
	stream.DecodeData(dwTimeIndex, BC_QMATRIX_TIMEINDEX, BCNUM_QMATRIX_TIMEINDEX);
	
	return dwTime*100 + dwTimeIndex;
}

//////////////////////////////////////////////////////////////////////

int CQMatrixCompress::CompressStatCounts(CStatCounts& sc, CBitStream& stream)
{
	try
	{
		for(int i = 0; i < 25; i++)
		{
			stream.EncodeData(sc.m_pdwNumOfBuy[i], BC_STATCOUNT_STATNUM, BCNUM_STATCOUNT_STATNUM);
			stream.EncodeXInt32(sc.m_pxVolOfBuy[i], BC_STATCOUNT_STATVOL, BCNUM_STATCOUNT_STATVOL);
			stream.EncodeData(sc.m_pdwNumOfSell[i], BC_STATCOUNT_STATNUM, BCNUM_STATCOUNT_STATNUM);
			stream.EncodeXInt32(sc.m_pxVolOfSell[i], BC_STATCOUNT_STATVOL, BCNUM_STATCOUNT_STATVOL);
		}

		stream.EncodeData(sc.m_dwMaxVolOfBuy, BC_STATCOUNT_MAXVOL, BCNUM_STATCOUNT_MAXVOL);
		stream.EncodeData(sc.m_dwMaxVolOfSell, BC_STATCOUNT_MAXVOL, BCNUM_STATCOUNT_MAXVOL);

		stream.WriteDWORD(sc.GetCheckSum(), 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CQMatrixCompress::ExpandStatCounts(CStatCounts& sc, CBitStream& stream)
{
	try
	{
		for(int i = 0; i < 25; i++)
		{
			stream.DecodeData(sc.m_pdwNumOfBuy[i], BC_STATCOUNT_STATNUM, BCNUM_STATCOUNT_STATNUM);
			stream.DecodeXInt32(sc.m_pxVolOfBuy[i], BC_STATCOUNT_STATVOL, BCNUM_STATCOUNT_STATVOL);
			stream.DecodeData(sc.m_pdwNumOfSell[i], BC_STATCOUNT_STATNUM, BCNUM_STATCOUNT_STATNUM);
			stream.DecodeXInt32(sc.m_pxVolOfSell[i], BC_STATCOUNT_STATVOL, BCNUM_STATCOUNT_STATVOL);
		}

		stream.DecodeData(sc.m_dwMaxVolOfBuy, BC_STATCOUNT_MAXVOL, BCNUM_STATCOUNT_MAXVOL);
		stream.DecodeData(sc.m_dwMaxVolOfSell, BC_STATCOUNT_MAXVOL, BCNUM_STATCOUNT_MAXVOL);

		BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);
		if (cStreamChkSum!=sc.GetCheckSum())
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CQMatrixCompress::CompressStatCountsDiff(CStatCounts& scNew, CStatCounts& scOld, CBitStream& stream)
{
	try
	{
		for(int i = 0; i < 25; i++)
		{
			stream.EncodeData(scNew.m_pdwNumOfBuy[i], BC_STATCOUNT_STATNUMDIFF, BCNUM_STATCOUNT_STATNUMDIFF, scOld.m_pdwNumOfBuy+i);
			stream.EncodeXInt32(scNew.m_pxVolOfBuy[i], BC_STATCOUNT_STATVOLDIFF, BCNUM_STATCOUNT_STATVOLDIFF, scOld.m_pxVolOfBuy+i);
			stream.EncodeData(scNew.m_pdwNumOfSell[i], BC_STATCOUNT_STATNUMDIFF, BCNUM_STATCOUNT_STATNUMDIFF, scOld.m_pdwNumOfSell+i);
			stream.EncodeXInt32(scNew.m_pxVolOfSell[i], BC_STATCOUNT_STATVOLDIFF, BCNUM_STATCOUNT_STATVOLDIFF, scOld.m_pxVolOfSell+i);
		}

		stream.EncodeData(scNew.m_dwMaxVolOfBuy, BC_STATCOUNT_MAXVOLDIFF, BCNUM_STATCOUNT_MAXVOLDIFF, &scOld.m_dwMaxVolOfBuy);
		stream.EncodeData(scNew.m_dwMaxVolOfSell, BC_STATCOUNT_MAXVOLDIFF, BCNUM_STATCOUNT_MAXVOLDIFF, &scOld.m_dwMaxVolOfSell);

		stream.WriteDWORD(scNew.GetCheckSum(), 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CQMatrixCompress::ExpandStatCountsDiff(CStatCounts& sc, CBitStream& stream)
{
	try
	{
		CStatCounts scNew;

		for(int i = 0; i < 25; i++)
		{
			stream.DecodeData(scNew.m_pdwNumOfBuy[i], BC_STATCOUNT_STATNUMDIFF, BCNUM_STATCOUNT_STATNUMDIFF, sc.m_pdwNumOfBuy+i);
			stream.DecodeXInt32(scNew.m_pxVolOfBuy[i], BC_STATCOUNT_STATVOLDIFF, BCNUM_STATCOUNT_STATVOLDIFF, sc.m_pxVolOfBuy+i);
			stream.DecodeData(scNew.m_pdwNumOfSell[i], BC_STATCOUNT_STATNUMDIFF, BCNUM_STATCOUNT_STATNUMDIFF, sc.m_pdwNumOfSell+i);
			stream.DecodeXInt32(scNew.m_pxVolOfSell[i], BC_STATCOUNT_STATVOLDIFF, BCNUM_STATCOUNT_STATVOLDIFF, sc.m_pxVolOfSell+i);
		}

		stream.DecodeData(scNew.m_dwMaxVolOfBuy, BC_STATCOUNT_MAXVOLDIFF, BCNUM_STATCOUNT_MAXVOLDIFF, &sc.m_dwMaxVolOfBuy);
		stream.DecodeData(scNew.m_dwMaxVolOfSell, BC_STATCOUNT_MAXVOLDIFF, BCNUM_STATCOUNT_MAXVOLDIFF, &sc.m_dwMaxVolOfSell);

		BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);
		if (cStreamChkSum != scNew.GetCheckSum())
			return ERR_COMPRESS_CHECK;

		CopyMemory(&sc, &scNew, sizeof(CStatCounts));
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}


int CQMatrixCompress::ExpandMatrix(CGoods* pGoods, CBitStream& stream, WORD wVersion)
{
	try
	{
		DWORD PRICE_DIVIDE;
		char cDiv = (char)stream.ReadDWORD(1);
		if (cDiv==0)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		
//		RWLock_scope_wrlock  wrlock(pGoods->m_matrix.m_MatrixLock);

		pGoods->m_matrix.m_cHisOrderTime = (char)stream.ReadDWORD(1);

		DWORD dwTimeOld = ExpandQTime(0, stream);
		DWORD dwTime = ExpandQTime(dwTimeOld, stream);
		
		int nRet = ExpandHisOrder(pGoods->m_matrix.m_saHisOrderBuyArray, stream, dwTimeOld, dwTime, PRICE_DIVIDE, TRUE, wVersion);
		if (nRet==0)
		{
			pGoods->m_matrix.m_dwDynamicTimeBuy = dwTime;

			nRet = ExpandHisOrder(pGoods->m_matrix.m_saHisOrderSellArray, stream, dwTimeOld, dwTime, PRICE_DIVIDE, FALSE, wVersion);
			if (nRet==0)
			{
				pGoods->m_matrix.m_dwDynamicTimeSell = dwTime;
			}
		}

		pGoods->m_matrix.m_dwCount++;
		return nRet;
	}
	catch(...)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}


int CQMatrixCompress::ExpandHisOrder(CHisOrderQueueSArray& aHisOrder, CBitStream& stream, DWORD dwTimeOld, DWORD dwTime, DWORD PRICE_DIVIDE, BOOL bBuy, WORD wVersion)
{
	try
	{
		int nSize = (int)aHisOrder.GetSize();
		for (int i=0; i<nSize; i++)
		{
			aHisOrder[i].m_bWantDelete = TRUE;
		}

		DWORD dwTemp = 0;
		stream.DecodeData(dwTemp, BC_QMATRIX_NUMQ, BCNUM_QMATRIX_NUMQ);
		int nNum = dwTemp;

		CHisOrderQueue hoq;
		hoq.m_bWantDelete = FALSE;
		hoq.m_dwTime = dwTime;
		if (bBuy)
			hoq.m_cBuySell = ORDER_BID;
		else
			hoq.m_cBuySell = ORDER_OFFER;

		DWORD dwPriceLast = 0;
		int nPos = 0;
		char cActive = 0;

		for (int i=0; i<nNum; i++)
		{
			if (i==0)
			{
				stream.DecodeData(dwTemp, BC_QMATRIX_PRICE, BCNUM_QMATRIX_PRICE);
				hoq.m_dwPrice = dwTemp*PRICE_DIVIDE;
			}
			else
			{
				stream.DecodeData(dwTemp, BC_QMATRIX_PRICEDIFF, BCNUM_QMATRIX_PRICEDIFF);
				if (bBuy)
					hoq.m_dwPrice = dwPriceLast - dwTemp*PRICE_DIVIDE;
				else
					hoq.m_dwPrice = dwPriceLast + dwTemp*PRICE_DIVIDE;
			}

			dwPriceLast = hoq.m_dwPrice;

			cActive = (char)stream.ReadDWORD(1);;

			BOOL bUpdate = stream.ReadDWORD(1);
			if (bUpdate)
			{
				nPos = aHisOrder.Find(hoq);
				if (nPos>=0)
				{
					CHisOrderQueue& hoqOld = aHisOrder[nPos];
					hoqOld.m_bWantDelete = FALSE;
					
					ExpandHisOrder(hoqOld, stream, dwTimeOld, dwTime, wVersion);

					hoqOld.m_cActive = cActive;
					if (hoqOld.m_cActive)
						ExpandBuySell1(hoqOld, stream);
				}
				else
				{
					ExpandHisOrder(hoq, stream, dwTimeOld, dwTime, wVersion);

					hoq.m_cActive = cActive;
					if (hoq.m_cActive)
						ExpandBuySell1(hoq, stream);

					aHisOrder.Add(hoq);
				}
			}
			else
			{
				nPos = aHisOrder.Find(hoq);
				//				ASSERT(nPos>=0);
				if (nPos>=0)
				{
					CHisOrderQueue& hoqOld = aHisOrder[nPos];
					hoqOld.m_bWantDelete = FALSE;
					hoqOld.m_cActive = cActive;
				}
				else
				{
					int iii = 0;
				}
			}
		}

		nSize = (int)aHisOrder.GetSize();
		for (int i=nSize-1; i>=0; i--)
		{
			if (aHisOrder[i].m_bWantDelete)
				aHisOrder.RemoveAt(i);
		}
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	return 0;
}

int CQMatrixCompress::ExpandHisOrder(CHisOrderQueue& ho, CBitStream& stream, DWORD dwTimeOld, DWORD dwTime, WORD wVersion)
{
	ho.m_dwNumOfOrders = 0;

	BOOL bTime = stream.ReadDWORD(1);
	if (bTime)
	{
		ho.m_dwTime = ExpandQTime(dwTimeOld, stream);

		if (wVersion>=2)
			stream.DecodeData(ho.m_dwNumOfOrders, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);
	}
	else
	{
		ho.m_dwTime = dwTime;

		if (wVersion>=3)
			stream.DecodeData(ho.m_dwNumOfOrders, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);
	}

	DWORD dwTemp = 0;
	stream.DecodeData(dwTemp, BC_QMATRIX_NUMORDERS, BCNUM_QMATRIX_NUMORDERS);
	int nNum = dwTemp;

	if (ho.m_dwNumOfOrders==0)
		ho.m_dwNumOfOrders = dwTemp;

	int countOld, countNew;
	ho.m_adwOrderVol.clear();
	countOld = ho.m_adwOrderVol.size();
	countNew = ho.m_adwOrderVol.capacity();
	ho.m_adwOrderVol.reserve(nNum); //. SetSize(nNum);
	
	for (int i = 0; i < nNum; i++)
	{
		stream.DecodeData(dwTemp, BC_QMATRIX_ORDERVOL, BCNUM_QMATRIX_ORDERVOL);
//		ho.m_adwOrderVol[i] = dwTemp;
		ho.m_adwOrderVol.push_back(dwTemp);
	}
/*
	if( nNum > 50 ){
		LOG_ERR("ExpandHisOrder %d/%d %d/%d", countOld, countNew, ho.m_adwOrderVol.size(), ho.m_adwOrderVol.capacity());
	}
*/
	return nNum;
}

