#ifndef SOCKDATA_H
#define SOCKDATA_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)
#include <arpa/inet.h>

//#include <loki/SmallObj.h>
#include <vector> 

#include "DataHead.h"
#include "Buffer.h"
#include "XInt32.h"
#include "Goods.h"
#include "Ind.h"
#include "Util.h" 
#include "MyTime.h"
#include "Compress.h"
#include "SysAlarm.h"
#include "Data.h"

#define LEN_PHONENUMBER 11
#define LEN_DATE 30


// CSockData //////////////////////////////////////////////////////////////////
class CMySocket;
//class CMDSClientContext;


enum {
	INFO_LEVEL1 = 1, 
	INFO_LEVEL2 = 2, 
	INFO_LEVEL3 = 4,//pad 
	INFO_LEVELALL =7 
};

class CSockData //: public  Loki::SmallObject<Loki::ClassLevelLockable>
{
public:
	CSockData();
	virtual ~CSockData();

	virtual void Recv(CDataHead& head, CBuffer& buf);
	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);

	virtual void Send(CBuffer& buf);
	//By Steven 2012/11/25 15:21:47 Begin
	virtual void ThrowError(){ throw -1;};
//
//	virtual void SetBuddyPool(CBuddyPagePool* pBuddyPool)
//	{
//		m_pBuddyPool = pBuddyPool;
//	}
	//By Steven 2012 2012/11/25 15:21:47 End
protected:
	virtual void SendHead(CBuffer& buf, WORD wDataType);
	virtual void SendHeadMobile(CBuffer& buf, WORD wDataType);
	virtual void Abandon();
	void ExchagePhoneNum(CString& strPhoneNum);
	//By Steven 2012/11/25 15:22:36 Begin
	void* AllocBuffer(DWORD dwNum)
	{
		void* p = NULL;

//		if (m_pBuddyPool)
//			p = m_pBuddyPool->AllocBlock(dwNum);
//		else
			p = new char[dwNum];
		
		if (p)
			ZeroMemory(p, dwNum);
		return p;
	}
//
	void FreeBuffer(void* p)
	{
//		if (m_pBuddyPool)
//			m_pBuddyPool->FreeBlock(p);
//		else
		if( p )
			delete (char *)p;
	}
	//By Steven 2012 2012/11/25 15:22:36 End

public:
	CDataHead m_head;
	CDataHeadMobile m_headMobile;

	enum { WantNone = 0, WantSend, WantRecv, WantAll, WantDelete };
	WORD m_wStatus;
	WORD m_wVersion;
	WORD m_wPriority;

	DWORD m_dwLastSendTime;

	CMySocket* m_pSocket;

	//CMDSClientContext* m_pContext;

	DWORD m_time_start;/*发包时间,用于等待超时处理*/

	int m_nConnectID;

	// 请求超时检查
	virtual void OnTime(void);

};


// CCheckData ////////////////////////////////////////////////////////////////////

class CCheckData : public CSockData
{
public:
	virtual void Send(CBuffer& buf);
};

//////////////////////////////////////////////////////////////////////

/*class CL2LB_DS_LoginData : public CSockData
{
public:
	CL2LB_DS_LoginData();

	virtual void Send(CBuffer& buf);

public:
	DWORD m_dwIPAddress;
	short m_sPort;

private:
	DWORD m_lTime;
};*/

//////////////////////////////////////////////////////////////////////

class CL2DM_LoginData : public CSockData
{
public:
	CL2DM_LoginData();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	CString m_strName;
	CString m_strPassword;
};

//////////////////////////////////////////////////////////////////////

class CL2DM_ValueData : public CSockData
{
public:
	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);
};


///20120319 sjp
//CCompressSockData/////////////////////////////////////////////////////////

class CCompressSockData : public CSockData
{
public:
	CCompressSockData();
	virtual ~CCompressSockData();

//	virtual void SetBuddyPool(CBuddyPagePool* pBuddyPool);

	virtual BOOL RecvCompressData(CBuffer& buf);
	virtual BOOL RecvCompressDataStock(CBuffer& buf);
	virtual BOOL SendCompressData(CBuffer& buf);

protected:
	void Compress2Buf(CBuffer& buf, WORD wDataType);
	void CompressAck2Buf(CBuffer& buf);

protected:
	CBuffer m_bufComp;
	
	CCompress m_compSend;

	CCompress m_compRecv;
	int m_nCompRecvInBuffer;	//m_compRecv里没这个，避免频繁分配内存
};
///20120319 sjp



//**********************SockData5***********************************//
//////////////////////////////////////////////////////////////////////

class CRecv_GoodsData : public CCompressSockData
{
public:
	CRecv_GoodsData()
	{
		m_bufGoods.m_bSingleRead = true;
///		m_bufGoods.Initialize(8192, true, 0, &g_BuddyPoolDay);

//		m_pChannel = NULL;
	}

//	void SetBuddyPool(CBuddyPagePool* pBuddyPool)
//	{
//		CCompressSockData::SetBuddyPool(pBuddyPool);
//
//		m_bufGoods.Initialize(_SendBuffer_Size_, true, 0, m_pBuddyPool);
//	}

protected:
	void Read2GoodsBuf();

public:
//	CChannel* m_pChannel;
	//sam copy from pc client begin
	BOOL m_bCode;			// FALSE:用GoodsID唯一标识CGoods TRUE:增加Code
	//sam copy from pc client end


protected:
	CBuffer m_bufGoods;
};



//////////////////////////////////////////////////////////////////////

class CL2DM_Value5Data : public CRecv_GoodsData
{
public:
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
};


//////////////////////////////////////////////////////////////////////

struct CGoodsMinute;

class CL2DM_Minute5Data : public CRecv_GoodsData
{
public:
	CL2DM_Minute5Data();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

protected:
	void MakeGM();

protected:
	CArray<CGoodsMinute, CGoodsMinute&> m_aGM;
	CMinuteSortArray m_aMinuteTemp;
};



//////////////////////////////////////////////////////////////////////
struct CGoodsBargain;
class CL2DM_Bargain5Data : public CRecv_GoodsData
{
public:
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

protected:
	void MakeGB();

protected:
	CArray<CGoodsBargain, CGoodsBargain&> m_aGB;
};


class CL2DM_Value6Data : public CL2DM_Value5Data
{
public:
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
private:
	BOOL DoRecv(CBuffer& buf, CGoods* pGoods);
};


class CL2DM_Minute6Data : public CL2DM_Minute5Data
{
public:
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
};

class CL2DM_Bargain6Data : public CL2DM_Bargain5Data
{
public:
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
};


class CMDS_ClientLoginData : public CSockData
{
public:
    CMDS_ClientLoginData();
    virtual ~CMDS_ClientLoginData();

    virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
    virtual void Send(CBuffer& buf);

public:
    //登录类型 0 匿名登录 1手机号+匿名号注册登录 2手机号登录
    short m_slogin_type;        //登录类型
    short m_sIsFirstLogin;      //是否首次登录(0:首次,1：非首次)
    
    DWORD m_dwInSession;        //会话标识
    CString m_strUserName;      //用户名(硬件码)
    CString m_strPhoneNum;      //手机号
    CString m_strPassword;      //密码
    CString m_strInvitationCode;//邀请码
    int m_vendor;               //供应商
    CString m_machine_type;     //机器类型
    int m_arch;                 //平台类型
    int m_big_version;          //版本号(大)
    int m_mid_version;          //版本号(中)
    int m_small_version;        //版本号(小)
    int m_nProductID;           //产品ID
    CString m_strCapitalNum;    //资金账号
    CString m_strCustomNum;     //客户代码
	CString m_strUUID;			//设备标识			
	CString m_strIdfa;			//IDFA
	CString m_strProvince;      //省份
	CString m_strInputNickName;
	CString m_strGuid;          //加密手机号，去重
    CString m_strDoubleInfo;
    CString m_strLastLogin;
    CString m_strCurPid;
    CString m_strLastPid;
    CString m_strAvatarUrl;
    
    
    //out 
    DWORD m_dwOutSession;       //返回的会话标识
    short m_sError;             //错误代码
    short m_sOpType;            //操作提示代码
    CString m_strError;         //错误信息
    CString m_strOp;            //操作提示文本(电话号码)
    CString m_strURL_DS;        //行情服务器地址
    CString m_strURL_DS_EX;
    
    short m_userType;           //用户类型(收费 0 免费1 匿名2)
    short m_isTrial;            //服务状态(已过期 0 未过期 1 临近过期 2)
    short m_sUserLevel;
    short m_sOldUserPhone;
    short m_sOldUserFree;
    
    int   m_logindays;          //登录天数
    DWORD m_dwRefGoodUT;        //自选股更新时间
    
    int newest_big_ver;         //最新版本号(大)
    int newest_mid_ver;         //最新版本号(中)
    int newest_small_ver;       //最新版本号(小)
    int m_nStartDate;
	int m_nEndDate;
    int m_nLoginTimes;
    int m_nPointCount;
    int m_nUserExp;
    int m_nNextUserExp;
    int m_nContinLoginDays;
    int m_nUserRight;
    
    
    INT64 m_hBitmapNewApp;
    INT64 m_hBindId;
    
    
    
    CString m_sEmoneyQQ;
    CString m_sWeiboID;
    CString m_sWeiboName;
    CString m_sMacAddress;
};

/*class CFM_ClientLoginData : public CSockData
{   
public:
    CFM_ClientLoginData();
    virtual ~CFM_ClientLoginData();
    
    virtual void Recv(CDataHead& head, CBuffer& buf);
    virtual void Send(CBuffer& buf);
    virtual void OnTime();
    
public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
    short m_slogin_type;            //登录类型
    short m_sIsFirstLogin;          //是否首次登录(0:首次,1：非首次)
    
    CString m_strUserName;          //硬件码或IME
    CString m_strPhoneNum;          //手机号
    CString m_strPassword;          //密码
    CString m_strInvitationCode;    //邀请码
    DWORD m_dwSession;              //会话标识
    int m_vendor;                   //供应商
    CString m_machine_type;         //机器类型
    int m_arch;                     //平台类型
    int m_big_version;              //版本号(大)
    int m_mid_version;              //版本号(中)
    int m_small_version;            //版本号(小)
    int m_nProductID;               //产品ID
	CString m_strUUID;              //设备唯一标识
	CString m_strIdfa;				//IDFA
	CString m_strInputNickName;		//昵称
    //CString m_strCapitalNum;        //资金账号
    //CString m_strCustomNum;         //客户代码
    CString m_strProvince;            //省份
    CString m_strGuid;                //加密手机号
    CString m_strDoubleInfo;
    CString m_strLastLogin;
    CString m_strCurPid;
    CString m_strLastPid;
    CString m_strAvatarUrl;
    
    short m_sOpType;                //操作提示代码
    CString m_strOp;                //操作提示文本(电话号码)
    CString m_strURL_DS;            //行情服务器地址
    CString m_strURL_DS_EX;            //行情服务器地址
    
    short m_userType;               //用户类型(收费 0 免费1 匿名2)
    short m_isTrial;                //服务状态(已过期 0 未过期 1 临近过期 2)
    short m_sUserLevel;
    short m_sOldUserPhone;
    short m_sOldUserFree;
    int   m_logindays;              //登录天数
	int   m_nUserRight;              //用户权限位
	int   m_nStartDate;
	int   m_nEndDate;
    int m_nLoginTimes;
    int m_nPointCount;
    int m_nUserExp;
    int m_nNextUserExp;
    int m_nContinLoginDays;

    DWORD m_dwRefGoodUT;            //自选股更新时间
    INT64 m_hBitmapNewApp;
    INT64 m_hBindId;

    CDataHeadMobile m_headMobileLogin;
};*/



//////////////////////////////////////////////////////////////////////

struct CGoodsMinute
{
	CGoodsMinute()
	{
		ZeroMemory(this, sizeof(CGoodsMinute));
	}

	DWORD m_dwGoodsID;
	WORD m_wMinute;
	BYTE m_cCheckSum;
	DWORD m_dwCheckSum;
	//sam copy from pc client begin
	char m_pcCode[LEN_STOCKCODE+1];
	//sam copy from pc client end
};

class CL2DM_MinuteData : public CSockData
{
public:
	CL2DM_MinuteData();
	virtual ~CL2DM_MinuteData();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	void MakeGM();

	CArray<CGoodsMinute, CGoodsMinute&> m_aGM;
	CSortArrayMinute m_aMinuteTemp;
};

//////////////////////////////////////////////////////////////////////

struct CGoodsBargain
{
	CGoodsBargain()
	{
		ZeroMemory(this, sizeof(CGoodsBargain));
	}

	DWORD m_dwGoodsID;
	DWORD m_dwBargain;
	BYTE m_cCheckSum;
	DWORD m_dwCheckSum;
	//sam copy from pc client begin
	char m_pcCode[LEN_STOCKCODE+1];
	//sam copy from pc client end
};

class CL2DM_BargainData : public CSockData
{
public:
	CL2DM_BargainData();
	virtual ~CL2DM_BargainData();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	void MakeGB();

	CArray<CGoodsBargain, CGoodsBargain&> m_aGB;
};

//////////////////////////////////////////////////////////////////////////
class CRecvCompressData : public CSockData
{
public:
	CRecvCompressData();
	virtual ~CRecvCompressData();

	virtual BOOL RecvCompressData(CBuffer& buf);

protected:
	CBuffer m_bufComp;
	CCompress m_comp;
	int m_nCompInBuffer;	//m_comp里没这个，避免频繁分配内存
};

//////////////////////////////////////////////////////////////////////////
class CL2DM_SysAlarmData : public CRecvCompressData
{
public:
	CL2DM_SysAlarmData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

	void SetIdx(std::string &strName,short s);
	void FilterIdx();

private:
	CSysAlarmArray m_aAlarm;
	WORD m_wSFIdx1;
	WORD m_wSFIdx2;
	WORD m_wVolIdx;
};

//////////////////////////////////////////////////////////////////////
class CL2DM_IndGoodsData : public CRecvCompressData
{
public:
	CL2DM_IndGoodsData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	CIndSelGoodsArray m_aIndGoods;
};

//////////////////////////////////////////////////////////////////////////
class CL2DM_CPXData : public CRecvCompressData
{
public:
	CL2DM_CPXData(){}

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
	void CalcCPXStat();
	void AddASAlert(CGoods *pGoods,int nDay,int nMin30,int nMin60);
};

//个股日线验证包
class CL2DM_GoodsCheckData : public CSockData
{
public:
	CL2DM_GoodsCheckData();
	~CL2DM_GoodsCheckData();
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	FILE* m_file;
	DWORD m_dwLastSendTime;
	BOOL m_bReceived;
};

//下载日线校验包
class CL2DM_DayCheckData : public CSockData
{
public:
	CL2DM_DayCheckData();
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
	BOOL SaveDayCheck(CGoods *pGoods);
	DWORD m_dwLastPos;

public:
	DWORD m_dwGoodsID;
	CDayCheckArray m_aDayCheck;
};

//日线同步包
class CL2DM_DayData : public CSockData
{
public:
	CL2DM_DayData();
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);
	BOOL CheckDayCheck(CGoods *pGoods, BOOL bSecondCheck);
	int SaveToDataFile(CGoods *pGoods);
	BOOL CheckDataFile();
	
	// 检查本地的分钟线数据，只能检查根数和高低一样的可能问题K线 [4/27/2013 frantqian]
	void LocalMinCheck(CGoods *pGoods);

	// 测试代码，用来测试校验问题,直接从文件读取checkgoods_ds.txt [4/25/2013 frantqian]
	BOOL  LoadCheckGoodsFile();
	BOOL SaveDayCheck( CGoods *pGoods );
	BOOL bStartTest;

public:
	DWORD m_dwGoodsID;
	DWORD m_dwLastPos;
	CSortArray<WORD> m_aErrDay;
	CSortArray<CDay> m_aDayData;
	CDayCheckArray m_aDayCheck;
	char m_cCheckDataFile;//检查本地日线dayfile,check_dayfile，如果小于600M，90M,则是错误数据，不做下载，需手工初始化拷贝数据
};

/*
class CMDS_QuoteData : public CSockData
{
public:
	CMDS_QuoteData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	DWORD m_dwGoodsID;

	DWORD m_dwCountStatic;
	DWORD m_dwCountDynamic;

	BOOL m_bL2Encode;
	BOOL m_bNeedMMP;
};

//////////////////////////////////////////////////////////////////////

class CMDS_HisData : public CSockData
{
public:
	CMDS_HisData();
	virtual ~CMDS_HisData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

protected:
	void QueryStockNews(CBuffer& buf);
 
public:
	CGoods* m_pGoods;
// in 
	int		m_nUserRight;// userright=bitmap [10/17/2012 frantqian]
	int		m_nFuncPoint; //功能点权限值
	UINT64 m_hBitmapNewApp;
	UINT64 m_hBitmapFuncPoint;
	DWORD m_dwGoodsID;

	char m_cWantClose;
	char m_cFresh;

	XInt32 m_xRecvVolume;
	XInt32 m_xRecvAmount;

	char m_cOpenFund;	// 1=开放式基金，只传收盘价
	char m_cDataType;	// 0=日线，1=周线，2=月线，100=5分钟线，101=5分钟线，102=5分钟线，103=5分钟线
	char m_cIndType;	// 2=VMA 3=MACD 4=KDJ 5=RSI 6=WR 7=VR
	WORD m_wWant;		// 需要最后几根

	DWORD m_dwRecvTimeFirst;
	DWORD m_dwRecvTimeLast;
	BYTE m_cRecvCheckSum;

	DWORD m_dwRecvTimeFirstInd;
	DWORD m_dwRecvTimeLastInd;
// in

	WORD m_wDataType;
	WORD m_wPeriod;
	char m_cCQType;	// 除权方式 0:下除权，1：上除权，2：不除权 [11/9/2012 frantqian]

	short m_sLoadDayCount;
	CInd_MA m_indMA;
	CInd_VMA m_indVMA;
	CInd* m_pInd;

	int m_nFilterNum;

private:
	CArrayFDayMobile m_aFDay;
};

//////////////////////////////////////////////////////////////////////
class CDataMinute;
//////////////////////////////////////////////////////////////////////////
class CMDS_DAPAN :public CSockData
{
public:
	CMDS_DAPAN();
	~CMDS_DAPAN();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	// 首页上面的N只股票ID,用于返回当前价格和涨跌幅 [11/20/2013 frantqian]
	std::vector<DWORD> m_vecGoodsID;
	char m_cLinesPerPage;
	short m_sOffset;

	// in 
	DWORD m_dwGoodsID;

	char m_cStation;// 首页上部的N只股票状态 [11/25/2013 frantqian]
	char m_cWantClose;
	char m_cFresh;

	DWORD m_dwRecvHigh;
	DWORD m_dwRecvLow;
	XInt32 m_xRecvVolume;
	XInt32 m_xRecvAmount;
	//XInt32 m_xRecvAmtDiff;

	WORD m_wRecvNum;

	DWORD m_dwRecvDate;

	// in
private:
	CDataMinute* m_pData;
};


class CMDS_CurData : public CSockData
{
public:
	CMDS_CurData();
	~CMDS_CurData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);
	
public:
// in 
	int		m_nUserRight;// userright=bitmap [10/17/2012 frantqian]
	DWORD m_dwGoodsID;
	UINT64  m_hBitmapNewApp;
	char m_cWantClose;
	char m_cFresh;

	DWORD m_dwRecvHigh;
	DWORD m_dwRecvLow;
	XInt32 m_xRecvVolume;
	XInt32 m_xRecvAmount;
	XInt32 m_xRecvAmtDiff;

	WORD m_wRecvNum;

	DWORD m_dwRecvDate;

// in

private:
	CDataMinute* m_pData;
};



class CMDS_CurData5 : public CSockData
{
public:
	CMDS_CurData5();
	~CMDS_CurData5();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);
	
public:
// in 
	int		m_nUserRight;// userright=bitmap [10/17/2012 frantqian]
	DWORD m_dwGoodsID;
	UINT64  m_hBitmapNewApp;
	char m_cWantClose;
	char m_cFresh;

	DWORD m_dwRecvHigh;
	DWORD m_dwRecvLow;
	XInt32 m_xRecvVolume;
	XInt32 m_xRecvAmount;
	XInt32 m_xRecvAmtDiff;

	WORD m_wRecvNum;

	DWORD m_dwRecvDate;

// in

private:
	void LoadData();
	void DeleteData();
	BOOL CheckLoad(CDataHisMin& data, int interval);
	int CompressHisMin(CDataHisMin& data, int interval);
	void WriteMinInDay(CGoods* pGoods, CBuffer& buf);
	CDataHisMin* m_pData;
	char m_cFirst;
	int m_nTotal;

	int m_nWantFirst;
	int m_nWantLast;
	int m_nSendFirst;
	int m_nSendLast;

	char m_cLoadOK;

	BOOL m_bCheckData;	
};


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 关于m_cGroup和 m_cType的相关解释[8/29/2012 frantqian]
//m_cGroup
//enum gridgroupid
//{
//_group_shszA = 0,   //沪深Ａ股
//_group_shA = 1,     //上证Ａ股
//_group_shB = 2,     //上证Ｂ股
//_group_szA = 3,     //深证Ａ股
//_group_szB = 4,     //深证Ｂ股
//_group_shJ = 5,     //上证基金
//_group_szJ = 6,     //深证基金
//_group_shZ = 7,     //上证债券
//_group_szZ = 8,     //深证债券
//_group_zxb = 9,     //中小板
//_group_qz = 10,      //权证
//_group_zs = 11,      //指数
//_group_zxg= 12,     //自选股
//_group_zjll=13,    //最近浏览
//_group_gnbk=14,    //概念板块
//_group_hybk=15,    //行业板块
//_group_dqbk=16,    //地区板块
//_group_kfjj=17,    //开放基金
//_group_Allbk=18,   //板块融合 
//_group_gzqh=19,    //股指期货
//_group_HK=20,      //香港H股
//_group_H =21,
//_group_impzs=22,   // 重点指数
//_group_sb=23,		//三板
//_group_qqzs = 30,//全球指数,
//_group_htszl = 31,//沪退市整理,
//_group_stszl = 32,//深退市整理
//_group_shqh = 33,//上海期货
//_group_dlsp = 34,//大连期货
//_group_zzsp = 35,//郑州期货
//_group_zggn = 36,//中国概念
//_group_wpqh = 37,//外盘期货
//_group_wh = 38,//外汇
//_group_spqhhj = 39,//商品期货集合
//_group_swzs = 40,//申万指数板块
//_group_gzqh = 41,//国债期货板块
//_group_cyb = -14   //创业板
//}

// m_cType:
// 1: GROUP_GNBK 概念板块, GROUP_HYBK 行业板块, GROUP_DQBK 地区板块, GROUP_AllBK 所有板块(概念+行业+地区 +热点)
// 2: GROUP_KFJJ 开发基金
// 3: GROUP_GZQH 股指期货
// 4：GROUP_ZYZS 重点指数
//////////////////////////////////////////////////////////////////////////
class CMDS_GridData : public CSockData
{
public:
	CMDS_GridData();
	virtual ~CMDS_GridData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

	//开始压缩goodsid vector
	void EncodeGoodsIDVec(BYTE maketgroup, CBitStream& stream,std::vector<DWORD>& vecGoods);


	//根据m_cSortID获取sortid && cSort
	void GetSortInfo(char& sortid, short& sSortID,char& cSort);
	//根据股票指针pGoods及当前版本来限制旧版本刷新港股,byzhaolin hk
	bool IsHkNotRefresh(CGoods* pGoods,WORD wVersion);
public:
// in 
	// in 
	// 板块带个股信息 [1/6/2014 frantqian]
	bool	m_bIsNeedHotGoods;	//是否需要热门个股
	bool	m_bNeedName;		//是否需要传中文名字
	int 	m_cNeedHotGoodsNum;	//客户端发送,需要热门个股的板块数量 m_cLinesPerPage > m_cNeedHotGoodsNum
	short	m_sSortID;//保存sSortID,send函数使用
	short   m_sSortID_Hot;

	DWORD m_dwValueTime;
	char m_cGroup;
	CString m_strGroup;
	char m_cType;
	char m_cSortID;
	char m_cSortID_Hot;
	char m_cLinesPerPage;
	short m_sOffset;

	CSortArray<DWORD> m_aRecvGoodsID;
// in
	char m_cStation;
	char m_cNoChange;
	
	short m_sTotal;
	CPtrArray m_aGoods;
	DWORD m_LTGoodsID;	// 龙头股ID [1/29/2013 frantqian]
	std::vector<DWORD> m_vecGoods; 
	char  m_cNeedGoodIDVec;
	int		m_nUserRight;// userright=bitmap
	char m_cFreshHk;
	UINT64 m_hBitmapNewApp;
};

//////////////////////////////////////////////////////////////////////

struct CFindGoods
{
	DWORD m_dwGoodsID;
	CString m_strName;
	CString	m_strGoodsScore;
};

class CMDS_FindGoodsData : public CSockData
{
public:
	CMDS_FindGoodsData();
	virtual ~CMDS_FindGoodsData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	char m_cStation;
	CString m_strInput;
	char m_cFilter;

	typedef std::multimap<int, CFindGoods> Container;
	Container m_ResultList;
};

class CDataBargain;
class CMDS_BarData : public CSockData
{
public:
	CMDS_BarData();
	~CMDS_BarData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
// in 
	DWORD m_dwGoodsID;

	char m_cWantClose;

	int m_nRecvNum;

	short m_sWant;		// 需要最后几个

// in

	CDataBargain* m_pData;
};

class CMDS_JBMData : public CSockData
{
public:
	CMDS_JBMData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
// in 
	DWORD m_dwGoodsID;
};

class CMDS_DPTJData : public CSockData
{
public:
	CMDS_DPTJData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
// in 
	DWORD m_dwGoodsID;
};


//add by liuchengzhu 20091215
//明日提示
class CDS_TipData : public CSockData
{
public:
	CDS_TipData();
	~CDS_TipData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	// USERRIGHT=BITMAP [10/17/2012 frantqian]
	int  m_nUserRight;
	UINT64  m_hBitmapNewApp;
	int m_lGoodsID;	
	short m_sDataType;

// in
	WORD m_wDataType;
	WORD m_wPeriod;

private:
	CArrayFDayMobile m_aFDay;
};
//end add by liuchengzhu 20091215

//add by liuchengzhu 20091126
//心跳
class CFM_CheckData : public CSockData
{
public:
	CFM_CheckData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	int m_lDate;
	int m_lSecond;
};
//end add by liuchengzhu 20091126

//add by liuchengzhu 20091127
//DS系统登录FM
class CFM_LoginData : public CSockData
{
public:
	CFM_LoginData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	CString m_strName;
	CString m_strPassword;
};
//end add by liuchengzhu 20091127

//add by liuchengzhu 20091130
//用户登录
//用户登录请求 包号21 
class CDS_LoginDataEx : public CSockData
{
public:
	CDS_LoginDataEx();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	int m_nCheckNum;
	CString m_strUserName;
	CString m_strPassword;
	int m_vendor;
	CString m_machine_type;
	int m_arch;
	int m_big_version;
	int m_mid_version;
	int m_small_version;
	DWORD m_dwSession;
	short m_sError;
	CString m_strError;
	CString m_strURL;
	int m_nLoginDays;
	short m_isTrial;
	
	CTime m_server_time;
	CTime m_referal_goods_time;
};

class CFM_UserLoginDataEx : public CSockData
{
public:
	CFM_UserLoginDataEx();

	virtual void Recv(CDataHead& head, CBuffer& buf);
	virtual void Send(CBuffer&);
	virtual void OnTime();

public:
	CString m_strUserName;
	CString m_strPassword;
	DWORD m_dwSession;
	int m_vendor;
	CString m_machine_type;
	int m_arch;
	int m_big_version;
	int m_mid_version;
	int m_small_version;
};

//end add by liuchengzhu 20091130

//add by liuchengzhu 20091208
class CFM_UserLogoutData : public CSockData
{
public:
	CFM_UserLogoutData();

	virtual void Send(CBuffer&);

public:
	CString		m_strUserName;
	DWORD		m_UserSession;
	int		m_dtBegin;
	int		m_dtEnd;
};
//end add by liuchengzhu 20091208

//add by liuchengzhu 20091202
//修改用户密码
class CDS_ChangePWD : public CSockData
{
public:
	CDS_ChangePWD();
	virtual void Recv(CDataHeadMobile& headNew,CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	CString m_strUserName;
	CString m_strPwdOld;
	CString m_strPwdNew;
	short m_sError;
	CString m_strError;
	short m_sDoubleFlag;
};
class CFM_ChangePWD : public CSockData
{
public:
	CFM_ChangePWD();
	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;
	CString m_strUserName;
	CString m_strPwdOld;
	CString m_strPwdNew;
	short m_sDoubleFlag;
};
//////////////////////////////////////////////////////////////////////////
// 微博，QQ号绑定益盟实名手机账号 [9/29/2012 frantqian]
class CDS_BindPhone_WeiboAndQQ : public CSockData
{
public:
	CDS_BindPhone_WeiboAndQQ();
	virtual void Recv(CDataHeadMobile& headNew,CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	int		m_nBindType;			//绑定类型 1：微博号  2：qq号
	CString m_strUserName;		//IMEI
	CString m_strPhoneNumber;	//手机号
	CString m_strPassWord;		//手机账号密码
	CString	m_EmonyQQ;			//QQ号
	CString m_weibo;			//WEIBO号
	short     m_nErrorCode;
	CString m_strRetMsg;
};
class CFM_BindPhone_WeiboAndQQ : public CSockData
{
public:
	CFM_BindPhone_WeiboAndQQ();
	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	int		m_nBindType;			//绑定类型 1：微博号  2：qq号
	CString m_strUserName;		//IMEI
	CString m_strPhoneNumber;	//手机号
	CString m_strPassWord;		//手机账号密码
	CString	m_EmonyQQ;			//QQ号
	CString m_weibo;			//WEIBO号
};




//////////////////////////////////////////////////////////////////////////

//end add by liuchengzhu 20091202

//自选股
struct stDWReferalGoods
{
	char _Alarm;
	DWORD _dwGood;
	stDWReferalGoods()
	{
		_Alarm = 0;
		_dwGood = 0;
	}
};
//查询用户自选股信息
class CDS_DownLoadReferalGoods : public CSockData
{
public:
	CDS_DownLoadReferalGoods();

	virtual void Recv(CDataHeadMobile& headNew, CBuffer& buf);
	virtual void Send(CBuffer& buf);

	short	m_sLoginType;
	CString m_strPhoneNumber;
	std::vector<stDWReferalGoods> m_vecGoods;
	CTime m_last_upd_time;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	short m_sDoubleFlag;
};


class CFM_DownLoadReferalGoods : public CSockData
{
public:
	CFM_DownLoadReferalGoods();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	short	m_sLoginType;
	CString m_strPhoneNumber;
	CTime m_last_upd_time;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	short m_sDoubleFlag;
	std::vector<stDWReferalGoods> m_vecGoods;
};


enum RERFERAL_GOOD_OP_TYPE { 
	REFERAL_OP_OVERWRITE=0,
	REFERAL_OP_APPEND,
	REFERAL_OP_DELETE,
	REFERAL_OP_INSERT
};

//查询用户自选股信息
class CDS_Platform_DownLoadReferalGoods : public CSockData
{
public:
	CDS_Platform_DownLoadReferalGoods();

	virtual void Recv(CDataHeadMobile& headNew, CBuffer& buf);
	virtual void Send(CBuffer& buf);

	short	m_sLoginType;
	CString m_strPhoneNumber;
	std::vector<stDWReferalGoods> m_vecGoods;
	CTime m_last_upd_time;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	CString m_strPassword;
	CString m_lastPid;
	//short m_sDoubleFlag;
};


class CFM_Platform_DownLoadReferalGoods : public CSockData
{
public:
	CFM_Platform_DownLoadReferalGoods();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	short	m_sLoginType;
	CString m_strPhoneNumber;
	CTime m_last_upd_time;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	//short m_sDoubleFlag;
	CString m_strPassword;
	CString m_lastPid;
	std::vector<stDWReferalGoods> m_vecGoods;
};

struct GoodParam
{
	int goodid;
	int errcode;
	CString errmsg;
	GoodParam():goodid(0),errcode(0) {};
	GoodParam(int _goodid,int _errcode,CString _errmsg):goodid(_goodid),errcode(_errcode),errmsg(_errmsg) {};
};

typedef std::vector<int> Goods;
typedef std::vector<GoodParam> ParamsResult;

//更新用户自选股信息
class CDS_UpdateReferalGoods : public CSockData
{
public:
	CDS_UpdateReferalGoods();
	virtual void Recv(CDataHeadMobile& headNew, CBuffer& buf);
	virtual void Send(CBuffer& buf);

	short   m_sLoginType;
	CString m_strPhoneNumber;
	RERFERAL_GOOD_OP_TYPE op_type;
	Goods m_pgoods;
	Goods m_pinvalid_goods;
	ParamsResult m_pgoods_result;
	CTime m_last_upd_time;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	short m_sDoubleFlag;
};

class CFM_UpdateReferalGoods : public CSockData
{
public:
	CFM_UpdateReferalGoods();
	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);
public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	short	m_sLoginType;
	CString m_strPhoneNumber;
	CTime  m_last_upd_time;
	RERFERAL_GOOD_OP_TYPE op_type;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	short m_sDoubleFlag;
	Goods m_pgoods;
	Goods m_pinvalid_goods;
	ParamsResult m_pgoods_result;
};

class CDS_Platform_UpdateReferalGoods : public CSockData
{
public:
	CDS_Platform_UpdateReferalGoods();
	virtual void Recv(CDataHeadMobile& headNew, CBuffer& buf);
	virtual void Send(CBuffer& buf);

	short   m_sLoginType;
	CString m_strPhoneNumber;
	RERFERAL_GOOD_OP_TYPE op_type;
	Goods m_pgoods;
	Goods m_pinvalid_goods;
	ParamsResult m_pgoods_result;
	CTime m_last_upd_time;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	//short m_sDoubleFlag;
	CString m_strPassword;
	CString m_lastPid;
};

class CFM_Platform_UpdateReferalGoods : public CSockData
{
public:
	CFM_Platform_UpdateReferalGoods();
	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);
public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	short	m_sLoginType;
	CString m_strPhoneNumber;
	CTime  m_last_upd_time;
	RERFERAL_GOOD_OP_TYPE op_type;
	int m_nReturn;
	CString m_strReturn;
	CString m_strUid;
	//short m_sDoubleFlag;
	Goods m_pgoods;
	Goods m_pinvalid_goods;
	ParamsResult m_pgoods_result;
	CString m_strPassword;
	CString m_lastPid;
};

class CDS_EndDateData : public CSockData
{
public:
	CDS_EndDateData();

	virtual void Recv(CDataHeadMobile& headNew, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	int m_nAskType;
	CString m_strUserName;
	//1303,新增 登陆类型 [10/31/2012 frantqian] 
	short	m_sLoginType;
	//1303,新增 PRODUCT ID [10/31/2012 frantqian]
	short	m_sProductID;
	//1305,双平台标记(1:双平台,0：其它版本) 20131016
	short	m_sDoubleFlag;
	int m_nReturn;
	CString m_strReturn;
};


class CFM_UserEndDateData : public CSockData
{
public:
	CFM_UserEndDateData();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	int m_nAskType;
	CString m_strUserName;
	//1303,新增 登陆类型 [10/31/2012 frantqian] 
	short	m_sLoginType;
	//1303,新增 PRODUCT ID [10/31/2012 frantqian]
	short	m_sProductID;
	//1305,双平台标记(1:双平台,0：其它版本) 20131016
	short	m_sDoubleFlag;
};

class CDS_CardData : public CSockData
{
public:
	CDS_CardData();

	virtual void Recv(CDataHeadMobile& headNew, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	CString m_strUserName;
	CString m_strCard;
	CString m_strCardPWD;

	int m_nClientVer;
	CString m_strOhterProduct;
	CString m_strOhterPwd;

	int m_nReturn;
	CString m_strReturn;
};


class CFM_UserCardData : public CSockData
{
public:
	CFM_UserCardData();

	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	CString m_strUserName;
	CString m_strCard;
	int m_nCardPWD;

	int m_nClientVer;
	CString m_strOhterProduct;
	CString m_strOhterPwd;
};
//end add by liuchengzhu 20091203

//////////////////////////////////////////////////////////////////////

class CFM_UserReferalData : public CSockData
{
public:
	CFM_UserReferalData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	CString m_strOldPhoneNumber;
	CString m_strNewPhoneNumber;
};

//////////////////////////////////////////////////////////////////////
struct ReferalPhone
{
	CString m_strNewPhoneNumber;
	CString m_err_msg;
	int m_err_code;
	ReferalPhone() { m_err_code = 0;}
};

typedef CArray<ReferalPhone,ReferalPhone &> AryReferalPhones;
//////////////////////////////////////////////////////////////////////

class CFM_UserReferalSetData : public CSockData
{
public:
	CFM_UserReferalSetData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	CString m_strOldPhoneNumber;
	int m_nReturn;
	CString m_strReturn;
	int m_nSuccess;
	int m_nFail;
	int m_nUnProcessed;
	AryReferalPhones m_phones;
};

//////////////////////////////////////////////////////////////////////
class CFM_UserExchangeLoyaltyPointsData : public CSockData
{
public:
	CFM_UserExchangeLoyaltyPointsData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	CString m_strPhoneNumber;
	int m_months;
};



struct LOYALTY_POINTS
{
	char actionType;
	char associatedCpNumber[LEN_PHONENUMBER + 1];
	int pointsAwarded;
	unsigned int date;
	unsigned int time;
	unsigned int fraction;
	LOYALTY_POINTS()
	{
		ZeroMemory(this, sizeof(LOYALTY_POINTS));
	}

	void htonData()
	{
		pointsAwarded  = htonl(pointsAwarded);
		date = htonl(date);
		time = htonl(time);
		fraction = htonl(fraction);
	}

	void ntohData()
	{
		pointsAwarded  = ntohl(pointsAwarded);
		date = ntohl(date);
		time = ntohl(time);
		fraction = ntohl(fraction);
	}

	LOYALTY_POINTS(const LOYALTY_POINTS & other)
	{
		actionType = other.actionType;
		memcpy(associatedCpNumber,other.associatedCpNumber,LEN_PHONENUMBER + 1);
		pointsAwarded = other.pointsAwarded;
		date = other.date;
		time = other.time;
		fraction = other.fraction;
	}

	LOYALTY_POINTS & operator = (const LOYALTY_POINTS & other)
	{
		if (this != &other)
		{
			actionType = other.actionType;
			memcpy(associatedCpNumber,other.associatedCpNumber,LEN_PHONENUMBER + 1);
			pointsAwarded = other.pointsAwarded;
			date = other.date;
			time = other.time;
			fraction = other.fraction;
		}
		return *this;
	}
};

const int LOYALTY_POINTS_PACK_LEN = 4 + 4 + LEN_PHONENUMBER + 1 + 3 * 4;

typedef std::vector<LOYALTY_POINTS> vecLoyaltyPoints;

//////////////////////////////////////////////////////////////////////
class CFM_UserQueryLoyaltyPointsData : public CSockData
{
public:
	CFM_UserQueryLoyaltyPointsData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	vecLoyaltyPoints m_vecPoints;
	CString m_strPhoneNumber;
	int m_nTotalPoints;
};

//////////////////////////////////////////////////////////////////////
struct ActionType
{
	int action_type;
	CString action_name;
};

typedef std::vector<ActionType> vecActionTypes;
//////////////////////////////////////////////////////////////////////
class CFM_UserQueryActionTypeData : public CSockData
{
public:
	CFM_UserQueryActionTypeData();

	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	vecActionTypes m_vecActions;
};

//////////////////////////////////////////////////////////////////////
//memo
class CMDS_AskData : public CSockData
{
public:
	CMDS_AskData();

	virtual void Recv(CDataHeadMobile& headMobile, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	int m_nServerType;
	int m_nAskType;
	int m_nAskSubType;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_QueryScreenLogoData : public CSockData
{
public:
	CMDS_QueryScreenLogoData();

	virtual void Recv(CDataHeadMobile& headMobile, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	int m_Date;
	short m_PicType;
	short m_UserType;
	short m_ScreenWidth;
	short m_ScreenHeiht;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_QryTraders : public CSockData
{
public:
	CMDS_QryTraders();
	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	CString m_strTraderPY;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_QryTraderAreas : public CSockData
{
public:
	CMDS_QryTraderAreas();
	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	CString m_strTrader;
	CString m_strAreaPY;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_QryTraderAreaBranches : public CSockData
{
public:
	CMDS_QryTraderAreaBranches();
	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	CString m_strTrader;
	CString m_strArea;
	CString m_strBranchPY;
};

//////////////////////////////////////////////////////////////////////////
class TraderWapInfo : public YSingleton<TraderWapInfo>
{
	struct TraderWap
	{
		CString m_trader;
		CString m_wap_addr;
		TraderWap(CString strTrader,CString strWapAddr)
			:m_trader(strTrader), m_wap_addr(strWapAddr) 
		{}
	};

	typedef std::vector<TraderWap> vecTraderWap;
	typedef vecTraderWap::const_iterator CIterTraderWap;

public:
	TraderWapInfo() 
	{ 
	}

	virtual ~TraderWapInfo()
	{
	}

	int Encode(CBuffer&, short sType);

	bool init();

private:
	vecTraderWap m_vecTraderWap1;
	vecTraderWap m_vecTraderWap2;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_QryTraderWapAddr : public CSockData
{
public:
	CMDS_QryTraderWapAddr();
	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);
private:
	short m_sType;
};

//////////////////////////////////////////////////////////////////////
class CMDS_MainInfoData : public CSockData
{
public:
	CMDS_MainInfoData();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

protected:
	int GetInfoType();

protected:
	int m_lTime;
	int m_nInfoType;
};

//////////////////////////////////////////////////////////////////////
class CMDS_InfoTitleDataEx : public CSockData
{
public:
	CMDS_InfoTitleDataEx();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	short QueryStockNew(CBuffer& buf);
	short QueryOldF10(CBuffer&);
	short QueryNewF10(CBuffer&);

public:
	short m_sInfoType;
	int	  m_nSubInfoType;
	char m_cType;
	int m_lGoodsID;
	char m_cLinesPerPage;
};
//////////////////////////////////////////////////////////////////////
class CMDS_AllInfoTitleDataEx : public CSockData
{
	struct NewsItem
	{
		short sType;
		DWORD dwGoodID;
		INT64 NewsID;
		std::string strTitle;
		std::string strDateTime;
		std::string strAbstract;
		NewsItem()
		{
			sType = 0;
			dwGoodID = 0;
			NewsID = 0;
		}
	};

	struct keycmp_greater
	{
		bool operator () (const std::string a, const std::string b)
		{
			return a > b;
		}
	};

	typedef std::multimap<std::string, NewsItem, keycmp_greater> container;
public:
	CMDS_AllInfoTitleDataEx();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	short m_sType;
	short m_sWant;

	container m_NewsList;
};
//////////////////////////////////////////////////////////////////////
class CMDS_InfoScrollTitleDataEx : public CSockData
{
public:
	CMDS_InfoScrollTitleDataEx();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	void GetSubInfoType();

private:
	int m_lTime;
	short m_sInfoType;
	int m_nSubInfoType;
	char m_cType;
	int m_lGoodsID;
	char m_cLinesPerPage;
	int dwsessionid;
	short m_isTrial;
	int m_nLoginDays;
};

//////////////////////////////////////////////////////////////////////
class CMDS_InfoTextDataEx : public CSockData
{
public:
	CMDS_InfoTextDataEx();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	void GetSubInfoType();

public:
	short m_sInfoType;
	int m_nSubInfoType;
	int m_lGoodsID;
	long long m_lOffset;
	int m_lLength;
	int m_nOpType;	//0-无操作,1-注册,2-推荐,3-电话
	char m_cWnatTitle;
};

//////////////////////////////////////////////////////////////////////
class CMDS_ReferalData : public CSockData
{
public:
	CMDS_ReferalData();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	CString m_strOldPhoneNumber;
	CString m_strNewPhoneNumber;
	int m_nReturn;
	CString m_strReturn;
};


//////////////////////////////////////////////////////////////////////
class CMDS_ReferalSetData : public CSockData
{
public:
	CMDS_ReferalSetData();
	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	CString m_strOldPhoneNumber;
	int m_nReturn;
	CString m_strReturn;
	int m_nSuccess;
	int m_nFail;
	int m_nUnProcessed;
	AryReferalPhones m_phones;
};

//////////////////////////////////////////////////////////////////////
class CMDS_ExchangeLoyaltyPointsData : public CSockData
{
public:
	CMDS_ExchangeLoyaltyPointsData();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	CString m_strPhoneNumber;
	int m_nReturn;
	int m_months;
	CString m_strReturn;
};

//////////////////////////////////////////////////////////////////////
class CMDS_QueryActionTypesData : public CSockData
{
public:
	CMDS_QueryActionTypesData();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);
};

//////////////////////////////////////////////////////////////////////
class CMDS_QueryLoyaltyPointsData : public CSockData
{
public:
	CMDS_QueryLoyaltyPointsData();

	virtual void Recv(CDataHeadMobile&, CBuffer&);
	virtual void Send(CBuffer&);

public:
	CString m_strPhoneNumber;
	vecLoyaltyPoints m_vecPoints;
	int m_nReturn;
	CString m_strReturn;
	int m_nTotalPoints;
};

//////////////////////////////////////////////////////////////////////////

class CMDS_L2_QuoteData : public CSockData
{
public:
	CMDS_L2_QuoteData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	DWORD m_dwGoodsID;
	DWORD m_dwCountDynamic;
	UINT64 m_hBitmapNewApp;
};

class CMDS_L2_OrderCountsData : public CSockData
{
public:
	CMDS_L2_OrderCountsData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	DWORD m_dwGoodsID;
	DWORD m_dwCountDynamic;
	BYTE m_cType; //0 逐单(额), 1 逐单(量), 2 逐笔
};

//////////////////////////////////////////////////////////////////////

class CMDS_GridDataL2: public CSockData
{
public:
	CMDS_GridDataL2();
	virtual ~CMDS_GridDataL2();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	// in 
	DWORD m_dwValueTime;
	char m_cGroup;
	CString m_strGroup;
	char m_cType;
	char m_cSortID;
	char m_cLinesPerPage;
	short m_sOffset;

	CSortArray<DWORD> m_aRecvGoodsID;
	// in
	char m_cStation;
	char m_cNoChange;

	short m_sTotal;
	CPtrArray m_aGoods;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_GroupAssociate : public CSockData
{
public:
	CMDS_GroupAssociate();
	~CMDS_GroupAssociate();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	DWORD m_dwGoodsID;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_Update : public CSockData
{
public:
	CMDS_Update();
	~CMDS_Update();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	short m_sCompany;
	short m_sPlatform;

	short m_sMaxV;
	short m_sMidV;
	short m_sMinV;
	CString m_strURL;
	CString m_strMemo;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_HisBK : public CSockData
{
public:
	CMDS_HisBK();
	~CMDS_HisBK();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	DWORD m_dwDate;
	CHisBK m_hb;
};

//////////////////////////////////////////////////////////////////////////
struct AlarmInfo
{
	char _cOp;
	DWORD _dwGood;
	AlarmInfo()
	{
		_cOp = 0;
		_dwGood = 0;
	}
};
class CMDS_SetAlarm : public CSockData
{
public:
	CMDS_SetAlarm();
	~CMDS_SetAlarm();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	CString m_strPhoneNO;
	std::vector<AlarmInfo> m_vecGoods;

	short m_sRet;
	std::string m_strInfo;

	std::string m_strIMS;
	std::string m_strClientProvince;// antai_province [9/5/2012 frantqian]
};
//////////////////////////////////////////////////////////////////////////
class CFM_SetAlarm : public CSockData
{
public:
	CFM_SetAlarm();
	~CFM_SetAlarm();

	virtual void Recv(CDataHead& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	std::string m_strPhoneNO;
	std::string m_strIMS;// 114使用 [12/27/2012 frantqian]
	std::vector<AlarmInfo> m_vecGoods;

	short m_sRet;
	std::string m_strInfo;
};

//////////////////////////////////////////////////////////////////////

class CMDS_DiagnosisData : public CSockData
{
public:
	CMDS_DiagnosisData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	DWORD m_dwGoodsID;
	int m_nDiagnosisReqType;
	int m_nReserved;
	std::string m_strParam;
	int m_nRet;
	std::string m_strResponsedData;
};

class CFM_DiagnosisData : public CSockData
{
public:
	CFM_DiagnosisData();

	virtual void Recv(CDataHead& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	DWORD m_dwGoodsID;
	int m_nDiagnosisReqType;
	int m_nReserved;
	std::string m_strParam;
	int m_nRet;
	std::string m_strResponsedData;
};

class CDS_SetCancelPushInfo : public CSockData
{
public:
	CDS_SetCancelPushInfo();
	virtual void Recv(CDataHeadMobile& headNew,CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	CString m_strToken; //令牌
	int m_nUserType;		//类型 1：实名  2：匿名
	CString m_strUserName;  //用户名
	int m_nOpFunID;		//操作功能ID 
	int m_nProductID;	//产品
	CString m_strOpFunID;
	
	int m_nReturn;
	CString m_strReturn;
};

class CFM_SetCancelPushInfo : public CSockData
{
public:
	CFM_SetCancelPushInfo();
	virtual void Recv(CDataHead&, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	CString m_strToken; //令牌
	int m_nUserType;		//类型 1：实名  2：匿名
	CString m_strUserName;  //用户名
	int m_nOpFunID;		//操作功能ID 
	int m_nProductID;	//产品
	CString m_strOpFunID;
};

//////////////////////////////////////////////////////////////////////////
class CMDS_Suggestion : public CSockData
{
public:
	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	std::string m_strIMSI;
	CString m_strPhone;
	std::string m_strSuggestion;

	int m_nErrNO;
	std::string m_strErr;
};

class CFM_Suggestion : public CSockData
{
public:
	virtual void Recv(CDataHead& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	std::string m_strIMSI;
	std::string m_strPhone;
	std::string m_strSuggestion;

	int m_nErrNO;
	std::string m_strErr;
};


class CFM_SetAlarmNew : public CSockData
{
public:
	CFM_SetAlarmNew();
	~CFM_SetAlarmNew();

	virtual void Recv(CDataHead& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

public:
	bool m_bClientIsHttp;			//客户端登陆协议类型
	std::string m_strPhoneNO;
	std::string m_strProvinceClient;
	std::vector<AlarmInfo> m_vecGoods;

	short m_sRet;
	std::string m_strInfo;
};


//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// 关于m_cGroup和 m_cType的相关解释[8/29/2012 frantqian]
//m_cGroup
//enum gridgroupid
//{
//_group_shszA = 0,   //沪深Ａ股
//_group_shA = 1,     //上证Ａ股
//_group_shB = 2,     //上证Ｂ股
//_group_szA = 3,     //深证Ａ股
//_group_szB = 4,     //深证Ｂ股
//_group_shJ = 5,     //上证基金
//_group_szJ = 6,     //深证基金
//_group_shZ = 7,     //上证债券
//_group_szZ = 8,     //深证债券
//_group_zxb = 9,     //中小板
//_group_qz = 10,      //权证
//_group_zs = 11,      //指数
//_group_zxg= 12,     //自选股
//_group_zjll=13,    //最近浏览
//_group_gnbk=14,    //概念板块
//_group_hybk=15,    //行业板块
//_group_dqbk=16,    //地区板块
//_group_kfjj=17,    //开放基金
//_group_Allbk=18,   //板块融合 ,所有板块
//_group_gzqh=19,    //股指期货
//_group_HK=20,      //香港H股
//_group_H =21,
//_group_impzs=22,   // 重点指数
//_group_sb=23,		//三板
//_group_qqzs = 30,//全球指数,
//_group_htszl = 31,//沪退市整理,
//_group_stszl = 32,//深退市整理
//_group_shqh = 33,//上海期货
//_group_dlsp = 34,//大连期货
//_group_zzsp = 35,//郑州期货
//_group_zggn = 36,//中国概念
//_group_wpqh = 37,//外盘期货
//_group_wh = 38,//外汇
//_group_spqhhj = 39,//商品期货集合
//_group_swzs = 40,//申万指数板块
//_group_gzqh = 41,//国债期货板块
//_group_cyb = -14   //创业板
//} 

// m_cType:
// 1: GROUP_GNBK 概念板块, GROUP_HYBK 行业板块, GROUP_DQBK 地区板块, GROUP_AllBK 所有板块(概念+行业+地区 +热点)
// 2: GROUP_KFJJ 开发基金
// 3: GROUP_GZQH 股指期货
// 4：GROUP_ZYZS 重点指数
//////////////////////////////////////////////////////////////////////////

// 排名版面需要内容： [1/2/2014 frantqian]
// 取沪深A、中小板、创业板涨幅前10 [1/2/2014 frantqian]
// 沪深A、中小板、创业板、香港主板上涨家数 [1/2/2014 frantqian]
//资金流入、流出第一
//返回客户端需要显示的大盘板块名字、groupID（动态板块列表）


// 综合版面需要内容： [1/2/2014 frantqian]
//沪深A、中小板，创业板涨幅前10
//热门行业前6的名字及龙头股名字，价格，涨幅
class CMDS_ZongheData: public CSockData
{   
	bool m_bNeedHotGoodsInfo;   //是否领涨个股信息
	//bool m_bNeedHOTBKInfo; //是否需要传板块及龙头信息
	bool m_bNeedName;// 是否需要传中文名字，客户端参数
	bool m_bNeedSZJS; //是否需要传上涨家数信息
	bool m_bNeedDynamicList; //是否需要传动态板块列表
	bool m_bNeedZJJLGoods; //是否需要资金流入流出第一个股

public:
	CMDS_ZongheData();
	virtual ~CMDS_ZongheData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);
	void CalcOtherGroup(char cGroup,CString strGroup,int& iBKRiseCount);
public:
	// in 
	DWORD m_dwValueTime;
	char m_cGroup;
	CString m_strGroup;
	char m_cType;
	char m_cSortID;

	char m_cLinesPerPage;//返回个数

	CSortArray<DWORD> m_aRecvGoodsID;
	// in
	char m_cStation;
	char m_cNoChange;

	short m_sTotal;
	CPtrArray m_aHOTBKID; 
	std::vector<DWORD> m_vecHotBKGoodsID;


	CPtrArray m_aZJJLFirst; 
	CPtrArray m_aZJJLLast; 

	CPtrArray m_aGoods[10];
	int m_iGnBk;
	int m_iHyBk;
	int m_iDqBk;
	int m_iLzBk;
	int m_iSzbBk;
};

//////////////////////////////////////////////////////////////////////////
//个股、板块资金动向
class CMDS_ZJDXL2 : public CSockData
{
public:
	CMDS_ZJDXL2();
	virtual ~CMDS_ZJDXL2();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	bool m_bNeedName;
	// in 
	DWORD m_dwValueTime;
	char m_cGroup;
	CString m_strGroup;
	char m_cType;
	char m_cSortID;
	char m_cLinesPerPage;

	// in
	char m_cStation;
	char m_cNoChange;

	short m_sTotal;
	CPtrArray m_aGoods;//正向列表
	int m_nNeedGoodsCount;//正向列表需要返回股票个数
	CPtrArray m_aGoodsBackWard;//反向列表
	int m_nNeedReserveGoodsCount; //反向列表需要返回股票个数
};

/////////////////////////////////////////////////

//  交易界面信息显示，提供给技术部的交易sdk使用 [5/26/2016 qianyifan]
//////////////////////////////////////////////////////////////////////////
class CMDS_TradeInfo : public CSockData
{
public:
	CMDS_TradeInfo();
	virtual ~CMDS_TradeInfo();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:

	CGoodsCode  m_GoodsCode;	
	
	//证券代码：int	
	//证券名称：vchar
	//昨收：int
	//开盘：int
	//现价：int
	//最高：
	//最低：
	//成交量：
	//成交金额：
	//五档价格：
	//五档成交量：
	//交易手单位：
	//涨停：
	//跌停：
	//成功笔数：

};

class CMDS_ZYHGFullData : public CSockData
{
public:
    CMDS_ZYHGFullData():_hgHandle(){}
    virtual ~CMDS_ZYHGFullData(){}
    virtual void Recv(CDataHeadMobile &head, CBuffer &buf);
    virtual void Send(CBuffer &buff);

private:
    CZyhgHandle _hgHandle;
};

class CMDS_ZYHGBestData : public CSockData
{
public:
    CMDS_ZYHGBestData():_hgHandle(){ _lt10w = true; }
    virtual ~CMDS_ZYHGBestData(){}
    virtual void Recv(CDataHeadMobile &head, CBuffer &buf);
    virtual void Send(CBuffer &buff);

private:
    CZyhgHandle _hgHandle;
    bool _lt10w;

};


/////////////////////////////////////////////////

//资金，由原来的龙虎和资金流变组成[2016/Jun/28 liuyuguang]
//////////////////////////////////////////////////////////////////////////
class CMDS_ZijinData : public CSockData
{
public:
	CMDS_ZijinData();
	virtual ~CMDS_ZijinData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

private:
	int m_dwGoodsID;	
	DWORD m_dwCountDynamic;	
	BYTE m_cType; //0 逐单(额), 1 逐单(量), 2 逐笔

	int		m_nUserRight;// userright=bitmap [10/17/2012 frantqian]
	UINT64 m_hBitmapNewApp;
};

/////////////////////////////////////////////////
/////////////////////////////////////////////////
// 快速选股 [10/9/2014 %zhaolin%]
//快速选股条件项
struct CXGCond
{
	CXGCond()
	{
		ZeroMemory(this, sizeof(CXGCond));
	}

	BOOL CheckCond(CGoods* pGoods);
	//BOOL CheckCondEx(CGoods* pGoods,XInt32 *pVal = NULL);
	short m_sFieldID; 
	BYTE m_bCondType; 
	int m_nMinVal;
	int m_nMaxVal;
	//union
	//{
	//	struct
	//	{
	//		int m_nMinVal;
	//		int m_nMaxVal;
	//	};
	//	struct
	//	{
	//		XInt32 m_xMinVal;
	//		XInt32 m_xMaxVal;
	//	};
	//};
};

class CMDS_QuickSelGoods : public CSockData
{
public:
	CMDS_QuickSelGoods();
	virtual ~CMDS_QuickSelGoods();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);

protected:
	void SetGroupXG();

protected:
	CArray<CXGCond, CXGCond&> m_aXGCond;	//筛选条件
	CPtrArray aXGGoods;
	short m_nCondSize;
	char m_cMarket;
	UINT64  m_lPlatformUserRight;
	char m_cException;
};
////////////////////////////////////////////

// 平台版仓位显示 begin/////////////////////////////////////////////
class CMDS_PlatformCwxData : public CSockData
{
public:
	CMDS_PlatformCwxData();
	virtual ~CMDS_PlatformCwxData();

	virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
	virtual void Send(CBuffer& buf);
private:
	void SendGoods(CBuffer& buf, char m_cStation, short m_sTotal,CPtrArray& m_aGoods, DWORD dwSessionID, UINT64 m_hBitmapNewApp);

protected:
	UINT64  m_lPlatformUserRight;
	DWORD 	m_dwGoodsID;
	short   m_sVer;
	CInd_New_CWX* m_pInd;
	CArrayFDayMobile m_aFDay;
	static CPtrArray m_aGoods;
};
// 平台版仓位显示 end////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 关于m_cGroup和 m_cType的相关解释[8/29/2012 frantqian]
//m_cGroup
//enum gridgroupid
//{
//_group_shszA = 0,   //沪深Ａ股
//_group_shA = 1,     //上证Ａ股
//_group_shB = 2,     //上证Ｂ股
//_group_szA = 3,     //深证Ａ股
//_group_szB = 4,     //深证Ｂ股
//_group_shJ = 5,     //上证基金
//_group_szJ = 6,     //深证基金
//_group_shZ = 7,     //上证债券
//_group_szZ = 8,     //深证债券
//_group_zxb = 9,     //中小板
//_group_qz = 10,      //权证
//_group_zs = 11,      //指数
//_group_zxg= 12,     //自选股
//_group_zjll=13,    //最近浏览
//_group_gnbk=14,    //概念板块
//_group_hybk=15,    //行业板块
//_group_dqbk=16,    //地区板块
//_group_kfjj=17,    //开放基金
//_group_Allbk=18,   //板块融合 
//_group_gzqh=19,    //股指期货
//_group_HK=20,      //香港H股
//_group_H =21,
//_group_impzs=22,   // 重点指数
//_group_sb=23,		//三板
//_group_qqzs = 30,//全球指数,
//_group_htszl = 31,//沪退市整理,
//_group_stszl = 32,//深退市整理
//_group_shqh = 33,//上海期货
//_group_dlsp = 34,//大连期货
//_group_zzsp = 35,//郑州期货
//_group_zggn = 36,//中国概念
//_group_wpqh = 37,//外盘期货
//_group_wh = 38,//外汇
//_group_spqhhj = 39,//商品期货集合
//_group_swzs = 40,//申万指数板块
//_group_gzqh = 41,//国债期货板块
//_group_cyb = -14   //创业板
//}

// m_cType:
// 1: GROUP_GNBK 概念板块, GROUP_HYBK 行业板块, GROUP_DQBK 地区板块, GROUP_AllBK 所有板块(概念+行业+地区 +热点)
// 2: GROUP_KFJJ 开发基金
// 3: GROUP_GZQH 股指期货
// 4：GROUP_ZYZS 重点指数
//////////////////////////////////////////////////////////////////////////
 
#define GDF_GoodsPCCODE		0x00000001LL	//PCCODE			dword
#define	GDF_GoodsName		0x00000002LL	//股票名称			string
#define	GDF_High			0x00000004LL	//最高				DWORD m_dwHigh;			// 最高
#define	GDF_Low				0x00000008LL	//最低				DWORD m_dwLow;			// 最低
#define	GDF_Open			0x00000010LL	//开				DWORD m_dwOpen;			// 开盘
#define	GDF_Close			0x00000020LL	//收				DWORD m_dwClose;
#define	GDF_Price			0x00000040LL	//当前价			DWORD m_dwPrice;		// 成交
#define	GDF_Volume			0x00000080LL	//成交量			XInt32 m_xVolume;		// 成交量	
#define	GDF_Amount			0x00000100LL	//总额				XInt32 m_xAmount;		// 成交额
#define	GDF_Hsl				0x00000200LL	//换手率			xint32 getvalue(hsl)
#define	GDF_Hsl5			0x00000800LL	//5日换手率			xint32 getvalue(hsl5)

#define	GDF_ImVolume		0x00001000LL	//指数的成交量		XInt32  m_xVolume;
#define	GDF_NowINC			0x00002000LL	//现增仓			dword	GetValue(CUR_OI)
#define	GDF_DayINC			0x00008000LL	//日增仓			dword	GetValue(TODAY_OI)
#define	GDF_CcVolume		0x00010000LL	//持仓量			dword	GetValue(OPENINTEREST)
#define	GDF_BasicDiff		0x00020000LL	//基差				dword	GetValue(JICHA)
#define	GDF_BasicDiffPer	0x00040000LL	//基差率			dword	GetValue(JICHA_PER)
#define	GDF_PrevAccount		0x00080000LL	//前结算			dword	GetValue(PRE_SETTLEMENT)
#define	GDF_AcconutPrice	0x00100000LL	//结算价			dword	GetValue(SETTLEMENT)
#define	GDF_ImNum			0x00200000LL	//样本数量			DWORD m_dwI_Num;		// 样本个数
#define	GDF_ImaverPrice		0x00400000LL	//样本均价			DWORD m_dwI_Ave;		// 样本均价(3)
#define	GDF_ImaverCapital	0x00800000LL	//平均股本			DWORD m_dwI_AveGB;		// 样本平均股本(2) 亿股
#define	GDF_ImAmount		0x01000000LL	//总市值			DWORD m_dwI_SumSZ;		// 总市值(2) 万亿股
#define	GDF_ImPer			0x02000000LL	//占比				DWORD m_dwI_Percent;	// 占比%(2)
#define	GDF_ImPE			0x04000000LL	//静态市盈率		DWORD m_dwI_SYL;		// 静态市盈率
#define	GDF_PE				0x08000000LL	//市盈率			xint32 getvalue(SYL)
#define	GDF_Mgsy			0x10000000LL	//每股收益			xint32 getvalue(sy)
#define	GDF_Ltsz			0x20000000LL	//流通市值			xint32 getvalue(LTSZ)				
#define	GDF_Pingji			0x40000000LL	//评级				string
#define	GDF_MainFundb		0x80000000LL	//主力资金净买		xint32		GetValue(BIGAMT)
#define	GDF_MainFundb5		0x100000000LL	//5日净买			xint32		GetValue(BIGAMT5)
#define	GDF_MainFundsb		0x200000000LL	//主力强买(连续买	xint32		GetValue(ZLQM)
#define	GDF_MainFund10INC	0x400000000LL	//10日增仓			xint32		GetValue(ZLZC10)
#define	GDF_MainFund20INC	0x800000000LL	//20日增仓			xint32		GetValue(ZLZC20)
#define	GDF_MainFundDDBL	0x1000000000LL	//大单比率			xint32		GetValue(DDBL)

//可以通过其他值计算
#define	GDF_Liangbi			0x2000000000LL	//量比 此是目前是服务器传的 可以计算依赖成交量 股票代码 开盘时间
#define	GDF_Zhenfu			0x4000000000LL	//涨幅  此是目前是服务器传的 可以计算依赖最高价，最低价，收盘。
#define	GDF_Zd				0x8000000000LL	//涨跌 依赖最新价，昨收盘价
#define	GDF_Zdf				0x10000000000LL	//涨跌幅
#define	GDF_Zdf5			0x20000000000LL	//5日涨跌幅  依赖最新价，昨收盘价 五日收盘价
//未列出：
#define	GDF_Zhangsu			0x40000000000LL		//涨速 依赖最新价，5分钟价
#define	GDF_PBuy			0x80000000000LL		//dwPBuy 委买价，依赖dwPSell
#define	GDF_PSell			0x100000000000LL	//dwPSell 委卖价（委卖均价）
#define	GDF_Weibi			0x200000000000LL	//xWeibi 委比
#define	GDF_VSell			0x400000000000LL	//xVSell 委卖量
#define	GDF_VBuy			0x800000000000LL	//xVBuy 委买量，依赖xVSell
#define	GDF_Neipan			0x1000000000000LL	//xNeipan 内盘，依赖xVBuy
#define	GDF_Price5			0x2000000000000LL	//5分钟价格
#define	GDF_Zhangsu5		0x4000000000000LL	//5分钟涨速影响

#define	GDF_IsOpen			0x8000000000000LL	//免费标记	stream.WriteBOOL(theApp.m_OpenStockMgr.IsOpen(dwGoodsID));//1位免费开放，0位不开放，收费
#define	GDF_IsLT			0x10000000000000LL	//是否龙头标记
#define	GDF_NewsID			0x20000000000000LL	//是否龙头标记
#define	GDF_SuspendType		0x40000000000000LL	//是否停牌	stream.WriteBOOL(pGoods->m_SuspendInfo.m_SuspendType);

#define	GDF_CLOSE5			0x80000000000000LL	//close5
#define	GDF_CPXDay			0x100000000000000LL	//操盘线 日BS 这个的可配操盘线的字段
#define	GDF_RZRQ			0x200000000000000LL	//融资融券标志，可选字段
#define	GDF_TTM_SYL			0x400000000000000LL	//ttm市盈率，可选字段
#define	GDF_TTM_SXL			0x800000000000000LL	//ttm市销率，可选字段
#define	GDF_ZSZ				0x1000000000000000LL	//总市值，可选字段



//#define GDF_BK_HOTGoods		0x80000000000000LL	//板块热门股票

//stream.WriteDWORD(sGroup, 16);
//DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
//int nFind = m_aRecvGoodsID.Find(dwGoodsID);
//stream.EncodeData(pGoods->IsGZQH() ? pGoods->m_dwPreSettlement:pGoods->m_dwClose, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
 class CMDS_DynaGridData : public CSockData
 	{ 
 	public:
 		CMDS_DynaGridData();
 		virtual ~CMDS_DynaGridData();
 
 		virtual void Recv(CDataHeadMobile& head, CBuffer& buf);
 		virtual void Send(CBuffer& buf);

		// 填写大列表数据 [2/24/2014 frantqian]
		BOOL	FillGoodIDVec(CBitStream& stream);
		// 填写板块的热门个股信息 [2/24/2014 frantqian]
		BOOL	FillBKHotGoods(CBitStream& stream, CGoods* pGoods, int pos);//pos表示是当前列表第几个股票需要热门股信息，pos < m_cNeedHotGoodsNum
		// 填写基础字段[2/24/2014 frantqian]
		BOOL	FillGoodsBaseFields(CBitStream& stream, CGoods* pGoods);
		// 根据客户端发送的i64DynaGoodsField来填写客户端需要的字段 [2/24/2014 frantqian]
		void	FillGoodsDynaFields(CBitStream& stream,INT64 i64DynaGoodsField, CGoods* pGoods);
 		//开始压缩goodsid vector
 		void EncodeGoodsIDVec(BYTE marketgroup, CBitStream& stream,std::vector<DWORD>& vecGoods);
 	public:
 		// in 
 		// 客户端需要的字段标志 [2/21/2014 frantqian]
		INT64	m_i64DynaGoodsField;
		BOOL	m_bOpenDependent;
 		// 板块带个股信息 [1/6/2014 frantqian]
 		bool	m_bIsNeedHotGoods;	//是否需要热门个股
 		bool	m_bNeedName;		//是否需要传中文名字
 		int 	m_cNeedHotGoodsNum;	//客户端发送,需要热门个股的板块数量 m_cLinesPerPage > m_cNeedHotGoodsNum
 		short	m_sSortID;//保存sSortID,send函数使用
 
 		DWORD m_dwValueTime;
 		char m_cGroup;
 		CString m_strGroup;
 		char m_cType;
 		char m_cSortID;
 		short m_cLinesPerPage;
 		short m_sOffset;
 
 		CSortArray<DWORD> m_aRecvGoodsID;
 		// in
 		char m_cStation;
 		char m_cNoChange;
 
 		short m_sTotal;
 		CPtrArray m_aGoods;
 		DWORD m_LTGoodsID;	// 龙头股ID [1/29/2013 frantqian]
 		std::vector<DWORD> m_vecGoods; 
 		char  m_cNeedGoodIDVec;
		char m_cFreshHk;
		UINT64 m_hBitmapNewApp;
 	};

*/
struct CGoodsMatrix
{
	CGoodsMatrix()
	{
		ZeroMemory(this, sizeof(CGoodsMatrix));
	}

	DWORD m_dwGoodsID;
	DWORD m_dwTime;

	CGoods* m_pGoods;
};

inline BOOL operator > (CGoodsMatrix& first, CGoodsMatrix& second)
{
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

inline BOOL operator == (CGoodsMatrix& first, CGoodsMatrix& second)
{
	return first.m_dwGoodsID==second.m_dwGoodsID;
}

inline BOOL operator < (CGoodsMatrix& first, CGoodsMatrix& second)
{
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

class CL2DM_MatrixData : public CRecv_GoodsData
{
public:	
	virtual void Recv(CDataHead&, CBuffer&);
	virtual void Send(CBuffer&);

private:
	void MakeMatrix();
	BOOL DoRecv(CGoods* pGoods, CBuffer& buf);

	CArray<CGoodsMatrix, CGoodsMatrix&> m_aGoodsMatrix;
};

/*
class CHttpGetHandler
{
public:
	CHttpGetHandler();
	virtual ~CHttpGetHandler();
	virtual void Handle(CBuffer& buf, std::map<std::string, std::string> &parameters, std::map < std::string,std::string > &headers)=0;
};

class CHttpGetDefualtHandler:public CHttpGetHandler
{
public:
	virtual void Handle(CBuffer& buf, std::map<std::string, std::string> &parameters, std::map < std::string,std::string > &headers);
};

class CHttpGetEchoHandler:public CHttpGetHandler
{
public:
	virtual void Handle(CBuffer& buf, std::map<std::string, std::string> &parameters, std::map < std::string,std::string > &headers);
};

class CHttpGetStatisticsHandler:public CHttpGetHandler
{
public:
	virtual void Handle(CBuffer& buf, std::map<std::string, std::string> &parameters, std::map < std::string,std::string > &headers);
};

//////////////////////////////////////////////////////////////////////////
*/
#endif


