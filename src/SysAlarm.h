#ifndef _SYSALARM_H_
#define _SYSALARM_H_


#include "Array.h"
#include "GlobalData.h"
#include "DataFile.h"

/////////////////////////////////////////////////////////////////////////////

#pragma pack(push, 1)

struct CSysAlarm
{
	DWORD	m_dwTime;
	DWORD	m_dwGoodsID;
	WORD	m_wType;		//预警类型
	DWORD	m_dwPrice;		//预警时的价格

	CSysAlarm()
	{
		ZeroMemory(this, sizeof(CSysAlarm));
	}

	CSysAlarm(DWORD dwTime, DWORD dwGoodsID, WORD wType, DWORD dwPrice)
	{
		m_dwTime = dwTime;
		m_dwGoodsID = dwGoodsID;
		m_wType = wType;
		m_dwPrice = dwPrice;
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CSysAlarm));
	}

	DWORD GetCheckSum()
	{
		return DWORD(m_dwTime+m_dwGoodsID+m_wType+m_dwPrice);
	}
};

#pragma pack(pop)

typedef CArray<CSysAlarm, CSysAlarm&> CSysAlarmArray;

/////////////////////////////////////////////////////////////////////////////

class CSysAlarmAll
{
public:
	CSysAlarmAll()
	{
		m_dwClearCount = 0;
		m_strNameFile = "";
	}

	void Init(CString strPath);
	void Exit();

	void AddAlarm(CSysAlarmArray& aAlarm);
	void ClearAlarm();

	void GetName(CArray<CString, CString&>& aName);

public:
	DWORD m_dwClearCount;
	CGlobalData<CSysAlarm> m_alarmTemp;

	CGlobalData<CSysAlarm> m_alarm;
	CDataFile<CSysAlarm> m_dfAlarm;

	Mutex m_MutexName;
	CArray<CString, CString&> m_aName;
	CString m_strNameFile;
};

//////////////////////////////////////////////////////////////////////////
#pragma pack(push, 1)
struct CIndSelGoods
{
	DWORD	m_dwTime;
	DWORD	m_dwGoodsID;
	WORD	m_wType;		//预警类型
	DWORD	m_dwPrice;		//预警时的价格
	DWORD	m_dwHigh[4];	//30,60,120,240日最高价
	CIndSelGoods()
	{
		ZeroMemory(this, sizeof(CIndSelGoods));
	}

	CIndSelGoods(DWORD dwTime, DWORD dwGoodsID, WORD wType, DWORD dwPrice)
	{
		m_dwTime = dwTime;
		m_dwGoodsID = dwGoodsID;
		m_wType = wType;
		m_dwPrice = dwPrice;

		for (UINT i = 0;i < 4;i++)
			m_dwHigh[i] = 0;
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CIndSelGoods));
	}

	DWORD GetCheckSum()
	{
		DWORD dwSum = (m_dwTime+m_dwGoodsID+m_wType+m_dwPrice);
		for (UINT i = 0;i < 4;i++)
			dwSum += m_dwHigh[i];
		return dwSum;
	}
};

#define XGTYPE_ZLBTB 100	//主力被套榜类型
struct CHitchGoods
{
	DWORD	m_dwTime;		//介入日期
	DWORD	m_dwGoodsID;
	WORD	m_wType;		//==XGTYPE_ZLBTB
	DWORD	m_dwPrice;		//主力成本
	DWORD	m_dwStatus;		//==1
	DWORD	m_dwBTDays;		//被套天数 不超过60日
	DWORD	m_dwBTZDF;		//被套涨跌幅(静态)
	DWORD	m_dwBTDate;		//被套日期
	CHitchGoods()
	{
		ZeroMemory(this, sizeof(CHitchGoods));
	}
	CHitchGoods(const CIndSelGoods &r)
	{
		CopyMemory(this,&r,sizeof(*this));
	}
	const CHitchGoods& operator=(const CIndSelGoods&r)
	{
		CopyMemory(this,&r,sizeof(*this));
		return *this;
	}
};

inline BOOL operator > (CHitchGoods& first, CHitchGoods& second)
{
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

inline BOOL operator == (CHitchGoods& first, CHitchGoods& second)
{
	return first.m_dwGoodsID==second.m_dwGoodsID;
}

inline BOOL operator < (CHitchGoods& first, CHitchGoods& second)
{
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

//////////////////////////////////////////////////////////////////////////
#define XGTYPE_OVERSELL		6		//超跌股票池
struct COverSellGoods
{
	DWORD	m_dwTime;		//入池日期
	DWORD	m_dwGoodsID;
	WORD	m_wType;		//==XGTYPE_CDG - (XGTYPE_CDG + 2)
	DWORD	m_dwPrice;		//入选日次日开盘价
	DWORD	m_dwStatus;		//==0 入池  入榜: 1 英雄 2 观察
	DWORD	m_dwDays;		//入榜期限
	DWORD	m_dwPrice2;		//入榜时的价格
	DWORD	m_dwDate;		//入榜日期
	COverSellGoods()
	{
		ZeroMemory(this, sizeof(COverSellGoods));
	}
	COverSellGoods(const CIndSelGoods &r)
	{
		CopyMemory(this,&r,sizeof(*this));
	}
	const COverSellGoods& operator=(const CIndSelGoods&r)
	{
		CopyMemory(this,&r,sizeof(*this));
		return *this;
	}
};

inline BOOL operator > (COverSellGoods& first, COverSellGoods& second)
{
	if (first.m_wType != second.m_wType)
		return (first.m_wType > second.m_wType);
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

inline BOOL operator == (COverSellGoods& first, COverSellGoods& second)
{
	return (first.m_wType == second.m_wType && first.m_dwGoodsID==second.m_dwGoodsID);
}

inline BOOL operator < (COverSellGoods& first, COverSellGoods& second)
{
	if (first.m_wType != second.m_wType)
		return (first.m_wType < second.m_wType);
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

#pragma pack(pop)

typedef CArray<CIndSelGoods, CIndSelGoods&> CIndSelGoodsArray;
typedef CSortArray<CHitchGoods> CHitchGoodsArray;
typedef CSortArray<COverSellGoods> COverSellGoodsArray;

/////////////////////////////////////////////////////////////////////////////
//指标选股池数据
class CIndSelGoodsAll
{
public:
	CIndSelGoodsAll()
	{
		m_dwClearCount = 0;
		m_aHitchGoods.SetSize(0,500);
		m_aHitchGoods.SetSize(0,500);
		m_aHitchGroup.SetSize(0,500);
		m_aOverSellGoods.SetSize(0,500);
	}

	void Init(CString strPath);
	void SeparatePool();
	void Exit();

	void AddSelGoods(CIndSelGoodsArray& aIndGoods);
	void ClearAll();
public:
	DWORD m_dwClearCount;

public:
	//选股池名
	Mutex m_MutexName;
	CArray<CString, CString&> m_aName;
	CString m_strNameFile;

	//超级策略池
	CGlobalData<CIndSelGoods> m_indSelGoods;
	CDataFile<CIndSelGoods> m_dfIndGoods;

	//主力被套榜
	CHitchGoodsArray	m_aHitchGoods;
	CSortArray<WORD>	m_aHitchGroup;
	int FindHitchGoods(DWORD dwGoodsID);
	void AddHitchGoods(int nSize, CIndSelGoods *pIndGoods);

	//超跌股票池
	COverSellGoodsArray m_aOverSellGoods;
	void AddOverSellGoods(int nSize, CIndSelGoods *pIndGoods);
	void GetOverSellGoodsId( CString &strGroup, CDWordArray &aGoodsId );
	int FindOverSellGoods( DWORD dwGoodsID);
	int FindOverSellGoods2( DWORD dwGoodsID);
};

#endif

