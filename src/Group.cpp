// Group.cpp: implementation of the CGroup class.
//
//////////////////////////////////////////////////////////////////////

#include "MDS.h"
#include "Group.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGroup::CGroup()
{
	//CALL_TRACE("CGroup::CGroup");

	m_strClass = "";
	m_strGroup = "";

	m_pGoods = NULL;

	Initial();
}

CGroup::~CGroup()
{
	//CALL_TRACE("CGroup::~CGroup");
	m_aGoodsID.RemoveAll();
	m_aGoods.RemoveAll();

}

void CGroup::Initial()
{
	//CALL_TRACE("CGroup::Initial");

	m_rCritical.Lock();

	m_lDate = 0;
	m_lTime = 0;

	m_fSumLTG = 0;

	memset(m_plValue, 0, G_FIELDNUM*4);
	memset(m_plSHValue, 0, G_FIELDNUM*4);
	memset(m_plSZValue, 0, G_FIELDNUM*4);

	m_rCritical.UnLock();
}

void CGroup::SetGroup(char cStation)
{
	//CALL_TRACE("CGroup::SetGroup");
	m_rCritical.Lock();

	m_aGoodsID.RemoveAll();
	m_aGoods.RemoveAll();

	FILE* fp = fopen(m_strGroupFile.GetData(),"r");
	//LogInfo("SetGroup => m_strGroupFile = %s",m_strGroupFile.GetData());
	if (fp!=NULL)
	{
		struct stat st;
		stat(m_strGroupFile.GetData(), &st); 
		//LogInfo("SetGroup => st = %d",st.st_size);
		if (st.st_size%4==0)
		{
			DWORD dw;

			while(!feof(fp))
			{
				fread(&dw,sizeof(char),4,fp);
				// 修改申万外服LOADGROUP无数据问题 [1/16/2013 frantqian]
				//CGoods* pGoods = theApp.GetGoods(dw);
				CGoods* pGoods = theApp.CreateGoods(dw);
				if (pGoods)
				{
					if (cStation<0 || pGoods->m_cStation==cStation)
					{
						m_aGoodsID.Add(dw);
						m_aGoods.Add(pGoods);
						if (m_nGroup != -1)
						{
							std::string tmp  = m_strClass.GetData();
							if (m_nGroup == 15)
							{
								int pos = tmp.find('2');
								if (pos != (int)std::string::npos)
								{
									tmp.erase(pos);
								}
							}

							pGoods->AddBKInfo(m_nGroup, tmp, m_strGroup.GetData());
						}
					}
				}
			}
		}

		fclose(fp);
	}

	m_rCritical.UnLock();
}

void CGroup::ClearGoodsBK()
{
	int size = m_aGoods.GetSize();
	for (int i = 0; i < size; i++)
	{
		CGoods* pGoods = (CGoods*)m_aGoods[i];
		pGoods->ClearBKInfo();
	}
}

void CGroup::Calc()
{

}

void CGroup::Calc(int* plValue, char cStation)
{

}

char CGroup::Compare(CGroup* pGroup, short sFieldID, char cSort, char cStation)
{
	//CALL_TRACE("CGroup::Compare");
	if (sFieldID==-1)
		cSort = 1;

	if (cSort==0)
		return 0;
	else
	{
		if (sFieldID>=0 && sFieldID<G_FIELDNUM)
		{
			int lThis = GetValue(sFieldID, cStation);
			int lGroup = pGroup->GetValue(sFieldID, cStation);

			if (lThis>lGroup)
				return cSort;
			else if (lThis<lGroup)
				return -cSort;
			else
				return Compare(pGroup, -1, 1, cStation);
		}
		else
		{
			if (m_strGroup>pGroup->m_strGroup)
				return cSort;
			else if (m_strGroup<pGroup->m_strGroup)
				return -cSort;
			else
				return 0;
		}
	}
}

int CGroup::GetValue(short sID, char cStation)
{
	//CALL_TRACE("CGroup::GetValue");
	int lThis;
	if (cStation==0)
		lThis = m_plSHValue[sID];
	else if (cStation==1)
		lThis = m_plSZValue[sID];
	else
		lThis = m_plValue[sID];

	return lThis;
}


