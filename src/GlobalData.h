#ifndef _GLOBALDATA_H_
#define _GLOBALDATA_H_

#include "Array.h"
#include "RWLock.h"
#include "Buffer.h"
//#include "MD5Coder.h"

template <class TYPE> class CGlobalData  
{
public:
	CGlobalData();
	virtual ~CGlobalData();

	void Init();
	void Alloc(DWORD);
	void Add(TYPE&);
	void Read(CBuffer&);
	void Write(CBuffer&);
	void CreateMD5();

	TYPE* GetBuffer();

public:
	RWLock m_wr;
	DWORD m_dwSize;
	char m_pcHash[16];		// MD5

protected:
	TYPE* m_pBuffer;
	DWORD m_dwMaxSize;

	BOOL m_bBuddy;
};

template <class TYPE> CGlobalData<TYPE>::CGlobalData()
{
	m_pBuffer = NULL;
	m_dwMaxSize = 0;
	m_dwSize = 0;

	ZeroMemory(m_pcHash, 16);

	m_bBuddy = FALSE;
}

template <class TYPE> CGlobalData<TYPE>::~CGlobalData()
{
	if (m_pBuffer)
	{
		delete [] m_pBuffer;
		m_pBuffer = NULL;
	}
}

template <class TYPE> void CGlobalData<TYPE>::Init()
{
	m_wr.AcquireWriteLock();

	m_dwSize = 0;
	
	m_wr.ReleaseWriteLock();
}

template <class TYPE> void CGlobalData<TYPE>::Alloc(DWORD dwNewSize)
{
	if (dwNewSize>m_dwMaxSize)
	{
		TYPE* pSave = m_pBuffer;

		DWORD dwBuffer = (dwNewSize*sizeof(TYPE)+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE;
		m_pBuffer = (TYPE*)new(std::nothrow) BYTE[dwBuffer];
		ASSERT(m_pBuffer);
		if (m_pBuffer)
		{
			m_dwMaxSize = dwBuffer/sizeof(TYPE);

			if (pSave && m_dwSize>0)
				CopyMemory(m_pBuffer, pSave, m_dwSize*sizeof(TYPE));
		}

		if (pSave)
		{
			delete [] pSave;
			pSave = NULL;
		}
	}
}

template <class TYPE> void CGlobalData<TYPE>::Add(TYPE& type)
{
	Alloc(m_dwSize+1);
	if (m_pBuffer)
		m_pBuffer[m_dwSize++] = type;
}

template <class TYPE> void CGlobalData<TYPE>::Read(CBuffer& buf)
{
	m_wr.AcquireWriteLock();

	m_dwSize = 0;

	try
	{
		DWORD dwNewSize;
		buf.Read(&dwNewSize, 4);
		Alloc(dwNewSize);

		if (m_pBuffer && m_dwMaxSize>=dwNewSize)
		{
			m_dwSize = dwNewSize;
			buf.Read(m_pBuffer,m_dwSize*sizeof(TYPE));
		}
	}
	catch (int)
	{
	}

	m_wr.ReleaseWriteLock();
}

template <class TYPE> void CGlobalData<TYPE>::Write(CBuffer& buf)
{
	m_wr.AcquireReadLock();

	try
	{
		if (m_pBuffer && m_dwSize>0)
		{
			buf.Write(&m_dwSize, 4);
			buf.Write(m_pBuffer, m_dwSize*sizeof(TYPE));
		}
		else
		{
			DWORD dw0 = 0;
			buf.Write(&dw0, 4);
		}
	}
	catch (int)
	{
	}

	m_wr.ReleaseReadLock();
}

template <class TYPE> TYPE* CGlobalData<TYPE>::GetBuffer()
{
	return m_pBuffer;
}

template <class TYPE> void CGlobalData<TYPE>::CreateMD5()
{
	ZeroMemory(m_pcHash, 16);
	if (m_pBuffer && m_dwSize>0)
		;//CMD5Coder::EncodeMD5Raw((BYTE*)m_pBuffer, m_dwSize*sizeof(TYPE), m_pcHash);
}

/////////////////////////////////////////////////////////////////////////////

#pragma pack(push, 1)

struct CAlarm
{
	DWORD	m_dwTime;
	DWORD	m_dwGoodsID;
	BYTE	m_bType;
	char	m_cValue;
	XInt32	m_xValue;

	CAlarm()
	{
		ZeroMemory(this, sizeof(CAlarm));
	}

	CAlarm(DWORD dwTime, DWORD dwGoodsID, BYTE bType, XInt32 xValue, char cValue = 0)
	{
		m_dwTime = dwTime;
		m_dwGoodsID = dwGoodsID;
		m_bType = bType;
		m_cValue = cValue;
		m_xValue = xValue;
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CAlarm));
	}

	DWORD GetCheckSum()
	{
		return DWORD(m_dwTime+m_dwGoodsID+m_bType+m_cValue+m_xValue);
	}
};

#pragma pack(pop)

typedef CArray<CAlarm, CAlarm&> CAlarmArray;
typedef CGlobalData<CAlarm> CAlarmData;

//////////////////////////////////////////////////////////////////////
//大块数据，如升级包，收盘包，道破天际

class CLargeData : public CGlobalData<char>
{
public:
	CLargeData()
	{
		m_dwDate = 20100101;
		m_dwTime = 0;
	}

	DWORD m_dwVersion;		// 文件版本
	DWORD m_dwDate;			// 文件日期 YYYYMMDD
	DWORD m_dwTime;			// 文件时间 hhmmss
};

//////////////////////////////////////////////////////////////////////

class CMinData : public CLargeData
{
public:
	CMinData()
	{
		m_wHHMM = 0;
	}

	WORD m_wHHMM;
};

#endif
