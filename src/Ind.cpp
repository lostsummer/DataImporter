#include <math.h>

#include "Ind.h"
#include "AppGlobal.h"
#include "MyLogAdapter.h"
#include "Goods.h"
#include "CalcCPX.h"
#include "MDS.h"

#define EPS (1e-5)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void Calc_MA(double* pfMA, CFDayMobile* pDay, int nDay, short sParam)
{
	if (pDay == NULL || nDay <= 0) return;

	for (int i=0; i<nDay; i++)
	{
		if (i<sParam-1)
			pfMA[i]=pDay[i].m_fClose;
		else
		{
			pfMA[i] = 0;
			for (int j=i-sParam+1; j<=i; j++)
				pfMA[i] += pDay[j].m_fClose;
			pfMA[i] /= sParam;
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Calc_EMA(double* pfEMA, double* pfValue, int nValue, short sParam)
{
	LogTrace("%s", __FUNCTION__);
	if (pfEMA && pfValue && nValue>0 && sParam>0)
	{
		pfEMA[0] = pfValue[0];
		for (int i=1; i<nValue; i++)
			pfEMA[i] = (pfEMA[i-1]*(sParam-1)+pfValue[i]*2)/(sParam+1);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Calc_LLV(double* pfLLV, CFDayMobile* pDay, int nDay, short sParam)
{
	LogTrace("%s", __FUNCTION__);
	if (pfLLV && pDay && nDay>0 && sParam>0)
	{
		int nFirst = int(sParam-1);

		for (int i=0; i<nDay; i++)
		{
			if (i == 0)
				pfLLV[i] = pDay[i].m_fLow;
			else if (i < nFirst)
			{
				if (pfLLV[i-1] > pDay[i].m_fLow)
					pfLLV[i] = pDay[i].m_fLow;
				else
					pfLLV[i] = pfLLV[i-1];
			}
			else
			{
				int nFront = i-sParam+1;
				pfLLV[i] = pDay[nFront].m_fLow;

				for (int j=nFront+1; j<=i; j++)
				{
					if (pfLLV[i]>pDay[j].m_fLow)
						pfLLV[i] = pDay[j].m_fLow;
				}
			}
		}
	}
}

void Calc_LLV(int index, CFDayMobile* pDay, int nDay, short sParam)
{
	LogTrace("%s", __FUNCTION__);
	if (pDay && nDay>0 && sParam>0)
	{
		int nFirst = int(sParam-1);

		for (int i=0; i<nDay; i++)
		{
			if (i == 0)
				pDay[i].m_pfInd[index] = pDay[i].m_fLow;
			else if (i < nFirst)
			{
				if (pDay[i-1].m_pfInd[index] > pDay[i].m_fLow)
					pDay[i].m_pfInd[index] = pDay[i].m_fLow;
				else
					pDay[i].m_pfInd[index] = pDay[i-1].m_pfInd[index];
			}
			else
			{
				int nFront = i-sParam+1;
				pDay[i].m_pfInd[index] = pDay[nFront].m_fLow;

				for (int j=nFront+1; j<=i; j++)
				{
					if (pDay[i].m_pfInd[index] > pDay[j].m_fLow)
						pDay[i].m_pfInd[index] = pDay[j].m_fLow;
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Calc_HHV(double* pfHHV, CFDayMobile* pDay, int nDay, short sParam)
{
	LogTrace("%s", __FUNCTION__);
	if (pfHHV && pDay && nDay>0 && sParam>0)
	{
		int nFirst = int(sParam-1);

		for (int i=0; i<nDay; i++)
		{
			if (i == 0)
				pfHHV[i] = pDay[i].m_fHigh;
			else if(i < nFirst)
			{
				if (pfHHV[i-1] < pDay[i].m_fHigh)
					pfHHV[i] = pDay[i].m_fHigh;
				else
					pfHHV[i] = pfHHV[i-1];
			}
			else
			{
				int nFront = i-sParam+1;
				pfHHV[i] = pDay[nFront].m_fHigh;

				for (int j=nFront+1; j<=i; j++)
				{
					if (pfHHV[i]<pDay[j].m_fHigh)
						pfHHV[i] = pDay[j].m_fHigh;
				}
			}
		}
	}
}

void Calc_HHV(int index, CFDayMobile* pDay, int nDay, short sParam)
{
	LogTrace("%s", __FUNCTION__);
	if (pDay && nDay>0 && sParam>0)
	{
		int nFirst = int(sParam-1);

		for (int i=0; i<nDay; i++)
		{
			if (i == 0)
				pDay[i].m_pfInd[index] = pDay[i].m_fHigh;
			else if (i < nFirst)
			{
				if (pDay[i-1].m_pfInd[index] < pDay[i].m_fHigh)
					pDay[i].m_pfInd[index] = pDay[i].m_fHigh;
				else
					pDay[i].m_pfInd[index] = pDay[i-1].m_pfInd[index];
			}
			else
			{
				int nFront = i-sParam+1;
				pDay[i].m_pfInd[index] = pDay[nFront].m_fHigh;

				for (int j=nFront+1; j<=i; j++)
				{
					if (pDay[i].m_pfInd[index] < pDay[j].m_fHigh)
						pDay[i].m_pfInd[index] = pDay[j].m_fHigh;
				}
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////
void Calc_SMA(double* pfSMA, double* pfValue, int nValue, short sParam1, short sParam2, int nStart)
{
	LogTrace("%s", __FUNCTION__);
	if (pfSMA && pfValue && nValue>0 && sParam1>0 && sParam2>0 && nStart>=0)
	{
		if (nStart >=0 && nStart < nValue)
			pfSMA[nStart] = pfValue[nStart];
		for (int i=nStart+1; i<nValue; i++)
			pfSMA[i] = (pfSMA[i-1]*(sParam1-sParam2)+pfValue[i]*sParam2)/sParam1;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void Calc_SUM(double* pfSum, double* pfValue, int nValue, short sParam, int nStart, int& nFirst)
{
	LogTrace("%s", __FUNCTION__);
	if (pfSum && pfValue && nValue>0 && sParam>0 && nStart>=0)
	{
		nFirst = int(nStart+sParam-1);

		for (int i=nStart; i<nValue; i++)
		{
			pfSum[i] = 0;
			if (i == nStart)
				pfSum[i] = pfValue[i];
			else if(i < nFirst)
				for (int j=nStart; j<=i; j++)
					pfSum[i] += pfValue[j];
			else
				for (int j=i-sParam+1; j<=i; j++)
					pfSum[i] += pfValue[j];
		}
	}
}

//////////////////////////////////////////////////////////////////////

CInd::CInd()
{
	m_cParamSize = 0;
	ZeroMemory(m_psParam, 6*sizeof(short));

	m_cExpSize = 0;
	ZeroMemory(m_pnFirst, 6*sizeof(int));
}

CInd::CInd(CGoods* pGoods)
{
	m_cParamSize = 0;
	ZeroMemory(m_psParam, 6*sizeof(short));

	m_cExpSize = 0;
	ZeroMemory(m_pnFirst, 6*sizeof(int));

	m_pGoods = pGoods;
}

CInd::~CInd()
{
	m_pGoods = NULL;
}

char CInd::GetGroup()
{
	if (m_pGoods)
		return m_pGoods->m_cGroup;
	else
		return 10;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CInd_MA::CInd_MA() : CInd()
{
	m_psParam[0] = 5;
	m_psParam[1] = 10;
	m_psParam[2] = 20;
	m_psParam[3] = 30;
	m_psParam[4] = 60;
	m_psParam[5] = 120;
}

void CInd_MA::Calc(CFDayMobile* pFDay, int nNum)
{
	LogTrace("%s", __FUNCTION__);

	//ASSERT(m_cParamSize<=6);
	m_cExpSize = m_cParamSize;

	if (pFDay == NULL || nNum <= 0) return;

	for (int c=0; c<m_cParamSize; c++)
	{
		short sParam = m_psParam[c];
		if (sParam<1)
			sParam = 1;

		m_pnFirst[c] = int(sParam-1);  //设置前置K线根数

		for (int i=0; i<nNum; i++)
		{
			if (i<m_pnFirst[c])
				pFDay[i].m_pfMA[c] = pFDay[i].m_fClose;
			else if (sParam<2)
				pFDay[i].m_pfMA[c] = pFDay[i].m_fClose;
			else
			{
				pFDay[i].m_pfMA[c] = 0;
				for (int j=i-sParam+1; j<=i; j++)
					pFDay[i].m_pfMA[c] += pFDay[j].m_fClose;
				pFDay[i].m_pfMA[c] /= sParam;
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
CInd_VMA::CInd_VMA() : CInd()
{
	m_psParam[0] = 5;
	m_psParam[1] = 10;
	m_psParam[2] = 20;
	m_psParam[3] = 30;
	m_psParam[4] = 60;
	m_psParam[5] = 120;
}

void CInd_VMA::Calc(CFDayMobile* pFDay, int nNum)
{
	LogTrace("%s", __FUNCTION__);

	//ASSERT(m_cParamSize<=5);
	m_cExpSize = m_cParamSize+1;

	if (pFDay == NULL || nNum <= 0) return;

	for (int c=0; c<m_cParamSize+1; c++)
	{
		if (c==0)
		{
			m_pnFirst[c] = 0;
			for (int i=0; i<nNum; i++)
			{
				pFDay[i].m_pfVMA[c] = pFDay[i].m_pfInd[c] = pFDay[i].m_fVolume;
			}
		}
		else
		{
			short sParam = m_psParam[c-1];
			if (sParam<1)
				sParam = 1;

			m_pnFirst[c] = int(sParam-1);

			for (int i=0; i<nNum; i++)
			{
				if (i<m_pnFirst[c])
					pFDay[i].m_pfVMA[c] = pFDay[i].m_pfInd[c] = pFDay[i].m_fVolume;
				else if (sParam<m_cParamSize)
					pFDay[i].m_pfVMA[c] = pFDay[i].m_pfInd[c] = pFDay[i].m_fVolume;
				else
				{
					pFDay[i].m_pfVMA[c] = pFDay[i].m_pfInd[c] = 0;
					for (int j=i-sParam+1; j<=i; j++)
					{
						pFDay[i].m_pfInd[c] += pFDay[j].m_fVolume;
						pFDay[i].m_pfVMA[c] = pFDay[i].m_pfInd[c];
					}
					pFDay[i].m_pfInd[c] /= sParam;
					pFDay[i].m_pfVMA[c] = pFDay[i].m_pfInd[c];
				}
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CInd_MACD::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==3);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	double* pfClose = new(std::nothrow) double[nNum];	ZeroMemory(pfClose, nNum*sizeof(double));
	double* pfEMA_Long = new(std::nothrow) double[nNum];ZeroMemory(pfEMA_Long, nNum*sizeof(double));
	double* pfEMA_Short = new(std::nothrow) double[nNum];ZeroMemory(pfEMA_Short, nNum*sizeof(double));

	if (pfClose && pfEMA_Long && pfEMA_Short)
	{
		for (int i=0; i<nNum; i++)
			pfClose[i] = pFDay[i].m_fClose;

		Calc_EMA(pfEMA_Long, pfClose, nNum, m_psParam[0]);
		Calc_EMA(pfEMA_Short, pfClose, nNum, m_psParam[1]);

		double* pfDif = pfClose;	// pfClose没用了
		for (int i=0; i<nNum; i++)
			pfClose[i] = pfEMA_Short[i]-pfEMA_Long[i];

		double* pfDea = pfEMA_Long;	// pfEMA_Long没用了
		Calc_EMA(pfDea, pfDif, nNum, m_psParam[2]);

		for (int i=0; i<nNum; i++)
		{
			pFDay[i].m_pfInd[0] = pfDif[i];
			pFDay[i].m_pfInd[1] = pfDea[i];
		}
	}

	if (pfClose)
		delete [] pfClose;
	if (pfEMA_Long)
		delete [] pfEMA_Long;
	if (pfEMA_Short)
		delete [] pfEMA_Short;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//RSV = (CLOSE-LLV(LOW,N))/(HHV(HIGH,N)-LLV(LOW,N))*100;
//K : SMA(RSV,M1,1);
//D : SMA(K,M2,1);
//J : 3*K-2*D;

void CInd_KDJ::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==3);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	double* pfLLV = new(std::nothrow) double[nNum];ZeroMemory(pfLLV, nNum*sizeof(double));
	double* pfHHV = new(std::nothrow) double[nNum];ZeroMemory(pfHHV, nNum*sizeof(double));
	double* pfRSV = new(std::nothrow) double[nNum];ZeroMemory(pfRSV, nNum*sizeof(double));

	if (pfLLV && pfHHV && pfRSV)
	{
		int nN = m_psParam[0];
		int nM1 = m_psParam[1];
		int nM2 = m_psParam[2];

		Calc_LLV(pfLLV, pFDay, nNum, nN);
		Calc_HHV(pfHHV, pFDay, nNum, nN);

		int nFirst;
		if (nN-1>0)
			nFirst = nN-1;
		else
			nFirst = 0;
		for (int i=0; i<nNum; i++)
		{
			if (pfHHV[i]!=pfLLV[i])
				pfRSV[i] = (pFDay[i].m_fClose-pfLLV[i])/(pfHHV[i]-pfLLV[i])*100;
			else if (i==0)
				pfRSV[i] = 0;
			else
				pfRSV[i] = pfRSV[i-1];
		}

		m_pnFirst[0] = 0;
		double* pfK = pfLLV;
		Calc_SMA(pfK, pfRSV, nNum, nM1, 1, 0);


		m_pnFirst[1] = 0;
		double* pfD = pfHHV;
		Calc_SMA(pfD, pfK, nNum, nM2, 1, 0);

		for (int i=0; i<nNum; i++)
		{
			pFDay[i].m_pfInd[0] = pfK[i];
			pFDay[i].m_pfInd[1] = pfD[i];
		}
	}

	if (pfLLV)
		delete [] pfLLV;
	if (pfHHV)
		delete [] pfHHV;
	if (pfRSV)
		delete [] pfRSV;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CInd_RSI::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==2);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	double* pfMax = new(std::nothrow) double[nNum];ZeroMemory(pfMax, nNum*sizeof(double));
	double* pfAbs = new(std::nothrow) double[nNum];ZeroMemory(pfAbs, nNum*sizeof(double));
	double* pfMaxSMA = new(std::nothrow) double[nNum];ZeroMemory(pfMaxSMA, nNum*sizeof(double));
	double* pfAbsSMA = new(std::nothrow) double[nNum];ZeroMemory(pfAbsSMA, nNum*sizeof(double));

	if (pfMax && pfAbs && pfMaxSMA && pfAbsSMA)
	{
		for (int c=0; c<m_cParamSize; c++)
		{
			int nN = m_psParam[c];

			for (int i=1; i<nNum; i++)
			{
				if (pFDay[i].m_fClose >= pFDay[i-1].m_fClose)
				{
					pfMax[i] = pFDay[i].m_fClose - pFDay[i-1].m_fClose;
					pfAbs[i] = pFDay[i].m_fClose - pFDay[i-1].m_fClose;
				}
				else
				{
					pfMax[i] = 0;
					pfAbs[i] = pFDay[i-1].m_fClose - pFDay[i].m_fClose;
				}
			}

			Calc_SMA(pfMaxSMA, pfMax, nNum, nN, 1, 1);
			Calc_SMA(pfAbsSMA, pfAbs, nNum, nN, 1, 1);

			m_pnFirst[c] = 1;
			for (int i=1; i<nNum; i++)
			{
				if (pfAbsSMA[i]!=0)
					pFDay[i].m_pfInd[c] = pfMaxSMA[i]/pfAbsSMA[i]*100;
				else if (i==1)
					pFDay[i].m_pfInd[c] = 0;
				else
					pFDay[i].m_pfInd[c] = pFDay[i-1].m_pfInd[c];
			}
		}
	}

	if (pfMax)
		delete [] pfMax;
	if (pfAbs)
		delete [] pfAbs;
	if (pfMaxSMA)
		delete [] pfMaxSMA;
	if (pfAbsSMA)
		delete [] pfAbsSMA;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CInd_WR::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==1);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN<1)
		nN = 1;

	Calc_LLV(1, pFDay, nNum, nN);
	Calc_HHV(2, pFDay, nNum, nN);

	m_pnFirst[0] = 0;

	for (int i=m_pnFirst[0]; i<nNum; i++)
	{
		if (pFDay[i].m_pfInd[2]!=pFDay[i].m_pfInd[1])
			pFDay[i].m_pfInd[0] = (pFDay[i].m_pfInd[2]-pFDay[i].m_fClose)/(pFDay[i].m_pfInd[2]-pFDay[i].m_pfInd[1])*100;
		else if (i==m_pnFirst[0])
			pFDay[i].m_pfInd[0] = 0;
		else
			pFDay[i].m_pfInd[0] = pFDay[i-1].m_pfInd[0];
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void CInd_VR::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==1);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	double* pfLV = new(std::nothrow) double[nNum];ZeroMemory(pfLV, nNum*sizeof(double));
	double* pfSV = new(std::nothrow) double[nNum];ZeroMemory(pfSV, nNum*sizeof(double));
	double* pfLV_SUM = new(std::nothrow) double[nNum];ZeroMemory(pfLV_SUM, nNum*sizeof(double));
	double* pfSV_SUM = new(std::nothrow) double[nNum];ZeroMemory(pfSV_SUM, nNum*sizeof(double));

	if (pfLV && pfSV && pfLV_SUM && pfSV_SUM)
	{
		int nN = m_psParam[0];
		if (nN<1)
			nN = 1;

		for (int i=1; i<nNum; i++)
		{
			if (pFDay[i].m_fClose > pFDay[i-1].m_fClose)
			{
				pfLV[i] = pFDay[i].m_fVolume;
				pfSV[i] = 0;
			}
			else
			{
				pfLV[i] = 0;
				pfSV[i] = pFDay[i].m_fVolume;
			}
		}

		int nFirst;
		Calc_SUM(pfLV_SUM, pfLV, nNum, nN, 1, nFirst);
		Calc_SUM(pfSV_SUM, pfSV, nNum, nN, 1, nFirst);

		m_pnFirst[0] = 1;

		for (int i=m_pnFirst[0]; i<nNum; i++)
		{
			if (pfSV_SUM[i]!=0)
				pFDay[i].m_pfInd[0] = pfLV_SUM[i]/pfSV_SUM[i]*100;
			else if (i==nN-1)
				pFDay[i].m_pfInd[0] = 0;
			else
				pFDay[i].m_pfInd[0] = pFDay[i-1].m_pfInd[0];
		}
	}

	if (pfLV)
		delete [] pfLV;
	if (pfSV)
		delete [] pfSV;
	if (pfLV_SUM)
		delete [] pfLV_SUM;
	if (pfSV_SUM)
		delete [] pfSV_SUM;
}

//////////////////////////////////////////////////////////////////////////

void CIndL2_ZJLB::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==1);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nN1 = m_psParam[0];
	if (nN1 < 0)
	{
		m_cExpSize = 1;
		nN1 = 0;
	}
	else if (nN1 > 500)
	{
		nN1 = 500;
	}

	for (int i = 0; i < nNum ; i++)
	{
		pFDay[i].m_pfInd[0] = (pFDay[i].m_fAmtOfBuy[1] + pFDay[i].m_fAmtOfBuy[2] 
		- pFDay[i].m_fAmtOfSell[1] - pFDay[i].m_fAmtOfSell[2]) / 10000.0;

		LogErr("CIndL2_ZJLB %d %f", pFDay[i].m_dwTime, pFDay[i].m_pfInd[0]);
	}

	short sParam = nN1;
	if (sParam<1)
		sParam = 1;

	m_pnFirst[1] = int(sParam-1);
	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[1])
		{
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[0];
		}
		else if (sParam < 2)
		{
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[0];
		}
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-sParam+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[0];
		}
	}
}

void CIndL2_ZJBY::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==0);
	m_cExpSize = 4;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;

	double* pfCJ_EMA = new(std::nothrow) double[nNum];ZeroMemory(pfCJ_EMA, nNum*sizeof(double));
	double* pfDH_EMA = new(std::nothrow) double[nNum];ZeroMemory(pfDH_EMA, nNum*sizeof(double));
	double* pfZH_EMA = new(std::nothrow) double[nNum];ZeroMemory(pfZH_EMA, nNum*sizeof(double));
	double* pfSH_EMA = new(std::nothrow) double[nNum];ZeroMemory(pfSH_EMA, nNum*sizeof(double));
	if (pfCJ_EMA && pfDH_EMA && pfZH_EMA &&& pfSH_EMA)
	{
		for (int i = 0; i < nNum; i++)
		{
			pFDay[i].m_pfInd[0] = pFDay[i].m_fVolOfBuy[2] - pFDay[i].m_fVolOfSell[2];
			pFDay[i].m_pfInd[1] = pFDay[i].m_fVolOfBuy[1] - pFDay[i].m_fVolOfSell[1];
			pFDay[i].m_pfInd[2] = pFDay[i].m_fVolOfBuy[0] - pFDay[i].m_fVolOfSell[0];
			pFDay[i].m_pfInd[3] = pFDay[i].m_fVolOfSell[2] - pFDay[i].m_fVolOfBuy[2] 
				+ pFDay[i].m_fVolOfSell[1] - pFDay[i].m_fVolOfBuy[1] 
				+ pFDay[i].m_fVolOfSell[0] - pFDay[i].m_fVolOfBuy[0];
		}

		for (int i = 0, nParam = 0, j = 0; i < nNum; i++,j++)
		{
			//if (pfSH[i] > -EPS && pfSH[i] < EPS)
			//	continue;
			nParam++;

			nParam = nParam > 60 ? 60 : nParam;
			if (nParam == 1)
			{
				pfCJ_EMA[i] = (pfCJ_EMA[j]*(nParam-1)+pFDay[i].m_pfInd[0]*2)/(nParam+1);
				pFDay[i].m_pfInd[0] = pfCJ_EMA[i]*nParam/double(xLTG.GetValue())*10000000;

				pfDH_EMA[i] = (pfDH_EMA[j]*(nParam-1)+pFDay[i].m_pfInd[1]*2)/(nParam+1);
				pFDay[i].m_pfInd[1] = pfDH_EMA[i]*nParam/double(xLTG.GetValue())*10000000;

				pfZH_EMA[i] = (pfZH_EMA[j]*(nParam-1)+pFDay[i].m_pfInd[2]*2)/(nParam+1);
				pFDay[i].m_pfInd[2] = pfZH_EMA[i]*nParam/double(xLTG.GetValue())*10000000;

				pfSH_EMA[i] = (pfSH_EMA[j]*(nParam-1)+pFDay[i].m_pfInd[3]*2)/(nParam+1);
				pFDay[i].m_pfInd[3] = pfSH_EMA[i]*nParam/double(xLTG.GetValue())*10000000;
				continue;
			}
			else
			{
				pfCJ_EMA[i] = (pfCJ_EMA[j-1]*(nParam-1)+pFDay[i].m_pfInd[0]*2)/(nParam+1);
				pFDay[i].m_pfInd[0] = pfCJ_EMA[i]*nParam/double(xLTG.GetValue())*10000000;

				pfDH_EMA[i] = (pfDH_EMA[j-1]*(nParam-1)+pFDay[i].m_pfInd[1]*2)/(nParam+1);
				pFDay[i].m_pfInd[1] = pfDH_EMA[i]*nParam/double(xLTG.GetValue())*10000000;

				pfZH_EMA[i] = (pfZH_EMA[j-1]*(nParam-1)+pFDay[i].m_pfInd[2]*2)/(nParam+1);
				pFDay[i].m_pfInd[2] = pfZH_EMA[i]*nParam/double(xLTG.GetValue())*10000000;

				pfSH_EMA[i] = (pfSH_EMA[j-1]*(nParam-1)+pFDay[i].m_pfInd[3]*2)/(nParam+1);
				pFDay[i].m_pfInd[3] = pfSH_EMA[i]*nParam/double(xLTG.GetValue())*10000000;
			}
		}
	}

	if (pfCJ_EMA)
		delete [] pfCJ_EMA;
	if (pfDH_EMA)
		delete [] pfDH_EMA;
	if (pfZH_EMA)
		delete [] pfZH_EMA;
	if (pfSH_EMA)
		delete [] pfSH_EMA;
}

void CIndL2_DDBL::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==2);
	m_cExpSize = 4;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;

	int nN1 = m_psParam[0];
	if (nN1 < 1)
		nN1 = 1;
	else if (nN1 > 100)
		nN1 = 100;

	int nN2 = m_psParam[1];
	if (nN2 < 1)
		nN2 = 1;
	else if (nN2 > 100)
		nN2 = 100;

	for (int i = 0; i < nNum; i++)
		pFDay[i].m_pfInd[0] = (pFDay[i].m_fVolOfBuy[2] + pFDay[i].m_fVolOfBuy[1]
		- pFDay[i].m_fVolOfSell[2] - pFDay[i].m_fVolOfSell[1]) / double(xLTG)*10000000;

	pFDay[0].m_pfInd[1] = pFDay[0].m_pfInd[0];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[1] = (pFDay[i-1].m_pfInd[1]*(nN1-1)+pFDay[i].m_pfInd[0]*2)/(nN1+1);

	pFDay[0].m_pfInd[2] = pFDay[0].m_pfInd[0];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[2] = (pFDay[i-1].m_pfInd[2]*(nN2-1)+pFDay[i].m_pfInd[0]*2)/(nN2+1);
}

void CIndL2_CMJS::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==0);
	m_cExpSize = 2;
	
	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;

	int nN1 = 10;
	for (int i = 0; i < nNum; i++)
		pFDay[i].m_pfInd[0] = 100000000.0 * (double(pFDay[i].m_dwNumOfSell) - double(pFDay[i].m_dwNumOfBuy)) / double(xLTG.GetValue());

	pFDay[0].m_pfInd[1] = pFDay[0].m_pfInd[0];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[1] = (pFDay[i-1].m_pfInd[1]*(nN1-1)+pFDay[i].m_pfInd[0]*2)/(nN1+1);
}

void CIndL2_CJZJ::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==0);
	m_cExpSize = 3;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;
	
	for (int i = 0; i < nNum; i++)
	{
		pFDay[i].m_pfInd[0] = (pFDay[i].m_fVolOfBuy[2] - pFDay[i].m_fVolOfSell[2])/double(xLTG.GetValue())*10000000;
	}

	for (int i = 0, nSum = 0, nAvg = 0, j = 0; i < nNum; i++,j++)
	{
		//if (pfSH[i] > -EPS && pfSH[i] < EPS)
		//	continue;
		nSum++;
		nAvg++;

		nSum = nSum > 60 ? 60 : nSum;
		nAvg = nAvg > 10 ? 10 : nAvg;
		if (nSum == 1 && nAvg == 1)
		{
			pFDay[i].m_pfInd[3] = (pFDay[j].m_pfInd[3]*(nSum-1)+pFDay[i].m_pfInd[0]*2)/(nSum+1);
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*nSum;

			pFDay[i].m_pfInd[2] = (pFDay[j].m_pfInd[2]*(nAvg-1)+pFDay[i].m_pfInd[3]*nSum*2)/(nAvg+1);
			continue;
		}
		else
		{
			pFDay[i].m_pfInd[3] = (pFDay[j-1].m_pfInd[3]*(nSum-1)+pFDay[i].m_pfInd[0]*2)/(nSum+1);
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*nSum;

			pFDay[i].m_pfInd[2] = (pFDay[j-1].m_pfInd[2]*(nAvg-1)+pFDay[i].m_pfInd[3]*nSum*2)/(nAvg+1);
		}
	}
}


void CIndL2_SHZJ::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==0);
	m_cExpSize = 3;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;
	
	for (int i = 0; i < nNum; i++)
	{
		pFDay[i].m_pfInd[0] = (pFDay[i].m_fVolOfSell[2] - pFDay[i].m_fVolOfBuy[2] 
				+ pFDay[i].m_fVolOfSell[1] - pFDay[i].m_fVolOfBuy[1] 
				+ pFDay[i].m_fVolOfSell[0] - pFDay[i].m_fVolOfBuy[0])/double(xLTG.GetValue())*10000000;
	}

	for (int i = 0, nSum = 0, nAvg = 0, j = 0; i < nNum; i++,j++)
	{
		//if (pfSH[i] > -EPS && pfSH[i] < EPS)
		//	continue;
		nSum++;
		nAvg++;

		nSum = nSum > 60 ? 60 : nSum;
		nAvg = nAvg > 10 ? 10 : nAvg;
		if (nSum == 1 && nAvg == 1)
		{
			pFDay[i].m_pfInd[3] = (pFDay[j].m_pfInd[3]*(nSum-1)+pFDay[i].m_pfInd[0]*2)/(nSum+1);
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*nSum;

			pFDay[i].m_pfInd[2] = (pFDay[j].m_pfInd[2]*(nAvg-1)+pFDay[i].m_pfInd[3]*nSum*2)/(nAvg+1);
			continue;
		}
		else
		{
			pFDay[i].m_pfInd[3] = (pFDay[j-1].m_pfInd[3]*(nSum-1)+pFDay[i].m_pfInd[0]*2)/(nSum+1);
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*nSum;

			pFDay[i].m_pfInd[2] = (pFDay[j-1].m_pfInd[2]*(nAvg-1)+pFDay[i].m_pfInd[3]*nSum*2)/(nAvg+1);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
CInd_DMI::CInd_DMI() : CInd()
{
	m_psParam[0] = 14;
	m_psParam[1] = 6;
}

void CInd_DMI::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 2);
	m_cExpSize = 4;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 2) nN = 2;
	if (nN > 100) nN = 100;

	int nM = m_psParam[1];
	if (nM < 1) nM = 1;
	if (nM > 100) nM = 100;

	//if (nNum < nN) return;

	m_pnFirst[0] = m_pnFirst[1] = 1;
	m_pnFirst[2] = nM;
	m_pnFirst[3] = m_pnFirst[2] + nM;

	for (int i = 0; i < nNum; i++)
	{
		if (i < nN)	
		{
			pFDay[i].m_pfInd[4] = 0.0;
			for (int j=1; j<=i; j++)
			{
				double fABS1 = fabs(pFDay[j].m_fHigh-pFDay[j-1].m_fClose);
				double fABS2 = fabs(pFDay[j].m_fLow-pFDay[j-1].m_fClose);
				double fMax = Max(Max(pFDay[j].m_fHigh - pFDay[j].m_fLow, fABS1), fABS2);;
				pFDay[i].m_pfInd[4] += fMax;
			}
		}
		else
		{
			pFDay[i].m_pfInd[4] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
			{
				double fABS1 = fabs(pFDay[j].m_fHigh-pFDay[j-1].m_fClose);
				double fABS2 = fabs(pFDay[j].m_fLow-pFDay[j-1].m_fClose);
				double fMax = Max(Max(pFDay[j].m_fHigh - pFDay[j].m_fLow, fABS1), fABS2);;
				pFDay[i].m_pfInd[4] += fMax;
			}
		}
	}

	pFDay[0].m_pfInd[2] = 0.0;
	pFDay[0].m_pfInd[3] = 0.0;
	for (int i=1; i<nNum; i++)
	{
		double fHD = pFDay[i].m_fHigh - pFDay[i-1].m_fHigh;
		double fLD = pFDay[i-1].m_fLow - pFDay[i].m_fLow;
		pFDay[i].m_pfInd[2] = (fHD > 0 && fHD > fLD) ? fHD : 0;
		pFDay[i].m_pfInd[3] = (fLD > 0 && fLD > fHD) ? fLD : 0;
	}

	for (int i = 0; i < nNum; i++)
	{
		if (i < nN)
		{
			pFDay[i].m_pfInd[0] = 0.0;
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=1; j<=i; j++)
			{
				pFDay[i].m_pfInd[0] += pFDay[j].m_pfInd[2];
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[3];
			}

			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[0] * 100 / pFDay[i].m_pfInd[4];
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[1]* 100 / pFDay[i].m_pfInd[4];
		}
		else
		{
			pFDay[i].m_pfInd[0] = 0.0;
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
			{
				pFDay[i].m_pfInd[0] += pFDay[j].m_pfInd[2];
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[3];
			}

			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[0] * 100 / pFDay[i].m_pfInd[4];
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[1]* 100 / pFDay[i].m_pfInd[4];
		}
	}

	for (int i=0; i<nNum; i++)	
		pFDay[i].m_pfInd[3] = fabs(pFDay[i].m_pfInd[1] - pFDay[i].m_pfInd[0])/(pFDay[i].m_pfInd[1] + pFDay[i].m_pfInd[0]) * 100;

	for (int i=0; i<nNum; i++)
	{
		if (i<nM)
			pFDay[i].m_pfInd[2] = pFDay[i].m_pfInd[3];
		else
		{
			pFDay[i].m_pfInd[2] = 0;
			for (int j=i-nM+1; j<=i; j++)
				pFDay[i].m_pfInd[2] += pFDay[j].m_pfInd[3];
			pFDay[i].m_pfInd[2] /= nM;
		}
	}

	for (int i = 0; i < nNum; i++)
	{
		if (i < nM)
			pFDay[i].m_pfInd[3] = pFDay[i].m_pfInd[2];
		else
			pFDay[i].m_pfInd[3] = (pFDay[i].m_pfInd[2] + pFDay[i-nM].m_pfInd[2]) / 2;
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_DMA::CInd_DMA() : CInd()
{
	m_psParam[0] = 10;
	m_psParam[1] = 50;
	m_psParam[2] = 10;
}

void CInd_DMA::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 3);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nShort = m_psParam[0];
	if (nShort < 2) nShort = 2;
	if (nShort > 300) nShort = 300;

	int nLong = m_psParam[1];
	if (nLong < 10) nLong = 10;
	if (nLong > 300) nLong = 300;

	int nM = m_psParam[2];
	if (nM < 1) nM = 1;
	if (nM > 300) nM = 300;

	m_pnFirst[0] = nLong-1;
	m_pnFirst[1] = nLong + nM -2;

	if (nNum < nLong) return;

	for (int i=0; i<nNum; i++)
	{
		if (i < nShort-1)
			pFDay[i].m_pfInd[0] = 0.0;//pFDay[i].m_fClose;
		else
		{
			pFDay[i].m_pfInd[0] = 0.0;
			for (int j=i-nShort+1; j<=i; j++)
			{
				pFDay[i].m_pfInd[0] += pFDay[j].m_fClose;
			}
			pFDay[i].m_pfInd[0] /= nShort;
		}
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < nLong-1)
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_fClose;
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nLong+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j].m_fClose;
			pFDay[i].m_pfInd[1] /= nLong; 
		}
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[0] = 0.0;
		else
			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[0] - pFDay[i].m_pfInd[1];
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[1])
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[0].m_pfInd[0];
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nM+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j<0?0:j].m_pfInd[0];
			pFDay[i].m_pfInd[1] /= nM;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_TRIX::CInd_TRIX() : CInd()
{
	m_psParam[0] = 12;
	m_psParam[1] = 20;
}
//三重指数平均线
void CInd_TRIX::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 2);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 1) return;

	int nN = m_psParam[0];
	if (nN < 3) nN = 3;
	if (nN > 100) nN = 100;

	int nM = m_psParam[1];
	if (nM < 1) nM = 1;
	if (nM > 100) nM = 100;

	m_pnFirst[0] = 1;
	m_pnFirst[1] = nM;

	pFDay[0].m_pfInd[1] = pFDay[0].m_fClose;
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[1] = (pFDay[i-1].m_pfInd[1] *(nN-1)+pFDay[i].m_fClose*2)/(nN+1);

	pFDay[0].m_pfInd[0] = pFDay[0].m_pfInd[1];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[0] = (pFDay[i-1].m_pfInd[0] *(nN-1)+pFDay[i].m_pfInd[1]*2)/(nN+1);

	pFDay[0].m_pfInd[1] = pFDay[0].m_pfInd[0];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[1] = (pFDay[i-1].m_pfInd[1] *(nN-1)+pFDay[i].m_pfInd[0]*2)/(nN+1);


	pFDay[0].m_pfInd[0] = 0.0;//pFDay[0].m_pfInd[1];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[0] = (pFDay[i].m_pfInd[1] - pFDay[i-1].m_pfInd[1])/pFDay[i-1].m_pfInd[1]*100;

	for (int i=0; i<nNum; i++)
	{
		if (i < nM)
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_pfInd[0];
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nM+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[0];
			pFDay[i].m_pfInd[1] /= nM; 
		}
	}

}

//////////////////////////////////////////////////////////////////////////
CInd_BRAR::CInd_BRAR() : CInd()
{
	m_psParam[0] = 26;
}
//情绪指标
void CInd_BRAR::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 1);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 2) nN = 2;
	if (nN > 300) nN = 300;

	//if (nNum < nN) return;

	//m_pnFirst[0] = nN-1;
	m_pnFirst[0] = 0;
	m_pnFirst[1] = m_pnFirst[0] + 1;

	for (int i=0; i<nNum; i++)
	{
		if (i < nN-1)
		{
			pFDay[i].m_pfInd[0] = 0.0;
			pFDay[i].m_pfInd[2] = 0.0;//pFDay[i].m_fHigh - pFDay[i].m_fOpen;
			pFDay[i].m_pfInd[3] = 0.0;//pFDay[i].m_fOpen - pFDay[i].m_fLow;
			for (int j=0; j<=i; j++)
			{
				pFDay[i].m_pfInd[2] += (pFDay[j].m_fHigh - pFDay[j].m_fOpen);
				pFDay[i].m_pfInd[3] += (pFDay[j].m_fOpen - pFDay[j].m_fLow);
			}
		
			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[2] / pFDay[i].m_pfInd[3] * 100;
		}
		else
		{
			pFDay[i].m_pfInd[0] = 0.0;
			pFDay[i].m_pfInd[2] = 0.0;
			pFDay[i].m_pfInd[3] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
			{
				pFDay[i].m_pfInd[2] += (pFDay[j].m_fHigh - pFDay[j].m_fOpen);
				pFDay[i].m_pfInd[3] += (pFDay[j].m_fOpen - pFDay[j].m_fLow);
			}

			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[2] / pFDay[i].m_pfInd[3] * 100;
		}
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < nN)
		{
			pFDay[i].m_pfInd[1] = 0.0;
			pFDay[i].m_pfInd[2] = 0.0;//Max(0, pFDay[i].m_fHigh - pFDay[i-1].m_fClose);
			pFDay[i].m_pfInd[3] = 0.0;//Max(0, pFDay[i-1].m_fClose - pFDay[i].m_fLow);
			for (int j=1; j<=i; j++)
			{
				pFDay[i].m_pfInd[2] += Max(0.0, pFDay[j].m_fHigh - pFDay[j-1].m_fClose);
				pFDay[i].m_pfInd[3] += Max(0.0, pFDay[j-1].m_fClose - pFDay[j].m_fLow);
			}
			if(i == 0)
				pFDay[i].m_pfInd[1] = 0;
			else
				pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[2] / pFDay[i].m_pfInd[3] * 100;
		}
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			pFDay[i].m_pfInd[2] = 0.0;
			pFDay[i].m_pfInd[3] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
			{
				pFDay[i].m_pfInd[2] += Max(0.0, pFDay[j].m_fHigh - pFDay[j-1].m_fClose);
				pFDay[i].m_pfInd[3] += Max(0.0, pFDay[j-1].m_fClose - pFDay[j].m_fLow);
			}

			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[2] / pFDay[i].m_pfInd[3] * 100;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_CR::CInd_CR() : CInd()
{
	m_psParam[0] = 26;
	m_psParam[1] = 5;
	m_psParam[2] = 10;
	m_psParam[3] = 20;
}
//带状能量线
void CInd_CR::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 4);
	m_cExpSize = 4;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 5) nN = 5;
	if (nN > 300) nN = 300;

	int nM1 = m_psParam[1];
	if (nM1 < 1) nM1 = 1;
	if (nM1 > 100) nM1 = 100;

	int nM2 = m_psParam[2];
	if (nM2 < 1) nM2 = 1;
	if (nM2 > 100) nM2 = 100;

	int nM3 = m_psParam[3];
	if (nM3 < 1) nM3 = 1;
	if (nM3 > 100) nM3 = 100;

	//if (nNum <= nN) return;

	int nRefIndex1 = int(nM1/2.5+1);//3
	int nRefIndex2 = int(nM2/2.5+1);//5
	int nRefIndex3 = int(nM3/2.5+1);//9

	//m_pnFirst[0] = nN;//26
	m_pnFirst[0] = 1;//26
	m_pnFirst[1] = m_pnFirst[0] + nM1 + nRefIndex1 - 1;
	m_pnFirst[2] = m_pnFirst[1] + nM2 - nRefIndex1;
	m_pnFirst[3] = m_pnFirst[2] + nM2 + nRefIndex2 - 1;

	for (int i=0; i<nNum; i++)
		pFDay[i].m_pfInd[4] = (pFDay[i].m_fHigh + pFDay[i].m_fLow + pFDay[i].m_fClose) / 3;

	for (int i=0; i<nNum; i++)
	{
		if (i < nN)
		{
			pFDay[i].m_pfInd[0] = 0.0;
			pFDay[i].m_pfInd[2] = 0.0;
			pFDay[i].m_pfInd[3] = 0.0;
			for (int j=1; j<=i; j++)
			{
				pFDay[i].m_pfInd[2] += Max(0, pFDay[j].m_fHigh - pFDay[j-1].m_pfInd[4]);
				pFDay[i].m_pfInd[3] += Max(0, pFDay[j-1].m_pfInd[4] - pFDay[j].m_fLow);
			}
			if(i == 0)
				pFDay[i].m_pfInd[0] = 0;
			else
				pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[2] / pFDay[i].m_pfInd[3] * 100;
		}
		else
		{
			pFDay[i].m_pfInd[0] = 0.0;
			pFDay[i].m_pfInd[2] = 0.0;
			pFDay[i].m_pfInd[3] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
			{
				pFDay[i].m_pfInd[2] += Max(0, pFDay[j].m_fHigh - pFDay[j-1].m_pfInd[4]);
				pFDay[i].m_pfInd[3] += Max(0, pFDay[j-1].m_pfInd[4] - pFDay[j].m_fLow);
			}

			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[2] / pFDay[i].m_pfInd[3] * 100;
		}
	}

	int nSplit = m_pnFirst[1] - nRefIndex1;
	if (nSplit < nM1) nSplit = nM1;
	for (int i=0; i<nNum; i++)
	{
		if (i < nSplit)
			pFDay[i].m_pfInd[4] = 0.0;
		else
		{
			pFDay[i].m_pfInd[4] = 0.0;
			for (int j=i-nM1+1; j<=i; j++)
				pFDay[i].m_pfInd[4] += pFDay[j].m_pfInd[0];
			pFDay[i].m_pfInd[4] /= nM1; 
		}
	}

	nSplit = m_pnFirst[1];
	if (nSplit < nRefIndex1) nSplit = nRefIndex1;
	for (int i=0; i<nNum; i++)
	{
		if (i < nSplit)
			pFDay[i].m_pfInd[1] = 0.0;
		else
			pFDay[i].m_pfInd[1] = pFDay[i-nRefIndex1].m_pfInd[4];
	}

	nSplit = m_pnFirst[2] - nRefIndex2;
	if (nSplit < nM2) nSplit = nM2;
	for (int i=0; i<nNum; i++)
	{
		if (i < nSplit)
			pFDay[i].m_pfInd[4] = 0.0;
		else
		{
			pFDay[i].m_pfInd[4] = 0.0;
			for (int j=i-nM2+1; j<=i; j++)
				pFDay[i].m_pfInd[4] += pFDay[j].m_pfInd[0];
			pFDay[i].m_pfInd[4] /= nM2; 
		}
	}

	nSplit = m_pnFirst[2];
	if (nSplit < nRefIndex2) nSplit = nRefIndex2;
	for (int i=0; i<nNum; i++)
	{
		if (i < nSplit)
			pFDay[i].m_pfInd[2] = 0.0;
		else
			pFDay[i].m_pfInd[2] = pFDay[i-nRefIndex2].m_pfInd[4];
	}

	nSplit = m_pnFirst[2] + nRefIndex2;
	if (nSplit < nM3) nSplit = nM3;
	for (int i=0; i<nNum; i++)
	{
		if (i < nSplit)
			pFDay[i].m_pfInd[4] = 0.0;
		else
		{
			pFDay[i].m_pfInd[4] = 0.0;
			for (int j=i-nM3+1; j<=i; j++)
				pFDay[i].m_pfInd[4] += pFDay[j].m_pfInd[0];
			pFDay[i].m_pfInd[4] /= nM3; 
		}
	}

	nSplit = m_pnFirst[3];
	if (nSplit < nRefIndex3) nSplit = nRefIndex3;
	for (int i=0; i<nNum; i++)
	{
		if (i < nSplit)
			pFDay[i].m_pfInd[3] = 0.0;
		else
			pFDay[i].m_pfInd[3] = pFDay[i-nRefIndex3].m_pfInd[4];
	}
}

//////////////////////////////////////////////////////////////////////////
//累计能量线
void CInd_OBV::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 0);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	m_pnFirst[0] = 1;

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[1] = 0.0;
		if (i < 1)
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_fVolume;
		else if (pFDay[i].m_fClose > pFDay[i-1].m_fClose)
			pFDay[i].m_pfInd[1] = pFDay[i].m_fVolume;
		else if (pFDay[i].m_fClose < pFDay[i-1].m_fClose)
			pFDay[i].m_pfInd[1] = -pFDay[i].m_fVolume;
		else
			pFDay[i].m_pfInd[1] = 0.0;
	}

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[0] = 0.0;
		if (i < 1)
			pFDay[i].m_pfInd[0] = 0.0;//pFDay[i].m_pfInd[1];
		else
			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[1] + pFDay[i-1].m_pfInd[0];
	}
}

//////////////////////////////////////////////////////////////////////////
//振动升降指标
void CInd_ASI::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 0);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	m_pnFirst[0] = 1;

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[1] = 0.0;
		if (i > 0)
		{
			double fLC = pFDay[i-1].m_fClose;
			double fAA = fabs(pFDay[i].m_fHigh - fLC);
			double fBB = fabs(pFDay[i].m_fLow - fLC);
			double fCC = fabs(pFDay[i].m_fHigh - pFDay[i-1].m_fLow);
			double fDD = fabs(fLC - pFDay[i-1].m_fOpen);
			double fR = 0.0;
			if (fAA>fBB && fAA>fCC)
				fR = fAA + fBB/2 + fDD/4;
			else if (fBB>fCC && fBB>fAA)
				fR = fBB + fAA/2 + fDD/4;
			else
				fR = fCC + fDD/4;
			double fX = pFDay[i].m_fClose - fLC + (pFDay[i].m_fClose - pFDay[i].m_fOpen) / 2 + fLC - pFDay[i-1].m_fOpen;

			if(fR == 0 || Max(fAA,fBB) == 0)	
				pFDay[i].m_pfInd[1] = 0;
			else
				pFDay[i].m_pfInd[1] = 16 * fX / fR * Max(fAA, fBB);
		}
	}

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[0] = 0.0;
		if (i < 1)
			pFDay[i].m_pfInd[0] = 0.0;//pFDay[i].m_pfInd[1];
		else
			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[1] + pFDay[i-1].m_pfInd[0];
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_EMV::CInd_EMV() : CInd()
{
	m_psParam[0] = 14;
	m_psParam[1] = 9;
}
//简易波动指标
void CInd_EMV::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 2);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 1) nN = 1;
	if (nN > 100) nN = 100;

	int nM = m_psParam[1];
	if (nM < 1) nM = 1;
	if (nM > 100) nM = 100;

	m_pnFirst[0] = nN;
	m_pnFirst[1] = m_pnFirst[0] + nM - 1;

	if (nNum < nN) return;

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[3] = 0.0;
		if (i < 1) continue;

		double fA = (pFDay[i].m_fHigh+pFDay[i].m_fLow) / 2;
		double fB = (pFDay[i-1].m_fHigh+pFDay[i-1].m_fLow) / 2;
		double fMID = fA - fB;
		double fBRO = pFDay[i].m_fVolume / (pFDay[i].m_fHigh - pFDay[i].m_fLow);

		pFDay[i].m_pfInd[3] = fMID / fBRO;
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_pfInd[3];
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[3];
			pFDay[i].m_pfInd[1] /= nN;
		}
	}

	pFDay[0].m_pfInd[2] = pFDay[0].m_fVolume;
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[2] = (pFDay[i-1].m_pfInd[2]*(250-1)+pFDay[i].m_fVolume*2)/(250+1);

	for (int i=0; i<nNum; i++)
		pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[1] * pFDay[i].m_pfInd[2];

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[1])
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_pfInd[0];
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nM+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[0];
			pFDay[i].m_pfInd[1] /= nM;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_CCI::CInd_CCI() : CInd()
{
	m_psParam[0] = 14;
}
//商品路径指标
void CInd_CCI::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 1);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 2) nN = 2;
	if (nN > 100) nN = 100;

	m_pnFirst[0] = nN-1;

	if (nNum < nN) return;

	for (int i=0; i<nNum; i++)
		pFDay[i].m_pfInd[3] = (pFDay[i].m_fHigh + pFDay[i].m_fLow + pFDay[i].m_fClose) / 3;

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[2] = 0.0;//pFDay[i].m_pfInd[3];
		else
		{
			for (int j=i-nN+1; j<=i; j++)
				pFDay[i].m_pfInd[2] += pFDay[j].m_pfInd[3];
			pFDay[i].m_pfInd[2] /= nN;
		}
	}


	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_pfInd[0];
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += fabs(pFDay[j].m_pfInd[3] - pFDay[i].m_pfInd[2]);
			pFDay[i].m_pfInd[1] /= nN;
		}
	}

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[0] = 0.0;
		if (i < m_pnFirst[0]) continue;
		pFDay[i].m_pfInd[0] = (pFDay[i].m_pfInd[3] - pFDay[i].m_pfInd[2]) / (0.015 * pFDay[i].m_pfInd[1]);
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_ROC::CInd_ROC() : CInd()
{
	m_psParam[0] = 12;
	m_psParam[1] = 6;
}
//变动率指标
void CInd_ROC::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 2);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 1) nN = 1;
	if (nN > 100) nN = 100;

	int nM = m_psParam[1];
	if (nM < 1) nM = 1;
	if (nM > 50) nM = 50;

	m_pnFirst[0] = nN;
	m_pnFirst[1] = m_pnFirst[0] + nM - 1;

	if (nNum < nN) return;

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[0] = 0.0;
		else
			pFDay[i].m_pfInd[0] = (pFDay[i].m_fClose - pFDay[i-nN].m_fClose) / pFDay[i-nN].m_fClose * 100;
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[1])
			pFDay[i].m_pfInd[1] = 0.0;//pFDay[i].m_pfInd[0];
		else
		{
			pFDay[i].m_pfInd[1] = 0.0;
			for (int j=i-nM+1; j<=i; j++)
				pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[0];
			pFDay[i].m_pfInd[1] /= nM; 
		}
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_MTM::CInd_MTM() : CInd()
{
	m_psParam[0] = 6;
	m_psParam[1] = 6;
}
//动量线
void CInd_MTM::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 2);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 1) nN = 1;
	if (nN > 100) nN = 100;

	int nM = m_psParam[1];
	if (nM < 1) nM = 1;
	if (nM > 100) nM = 100;

	m_pnFirst[0] = nN;
	m_pnFirst[1] = m_pnFirst[0] + nM - 1;

	if (nNum < nN) return;

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[0] = 0.0;
		else
			pFDay[i].m_pfInd[0] = pFDay[i].m_fClose - pFDay[i-nN].m_fClose;
	}

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[1] = 0.0;
		if (i < m_pnFirst[1]) continue;

		pFDay[i].m_pfInd[1] = 0.0;
		for (int j=i-nM+1; j<=i; j++)
			pFDay[i].m_pfInd[1] += pFDay[j].m_pfInd[0];
		pFDay[i].m_pfInd[1] /= nM;
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_PSY::CInd_PSY() : CInd()
{
	m_psParam[0] = 12;
}
//心理线
void CInd_PSY::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 1);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 1) nN = 1;
	if (nN > 100) nN = 100;

	m_pnFirst[0] = 1;

	//if (nNum < nN) return;

	for (int i=0; i<nNum; i++)
	{
		if (i < nN)
		{
			pFDay[i].m_pfInd[0] = 0.0;
			for (int j=1; j<=i; j++)
			{
				if (pFDay[j].m_fClose > pFDay[j-1].m_fClose)
					pFDay[i].m_pfInd[0] += 1;
			}

			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[0] / nN * 100;
		}
		else
		{
			pFDay[i].m_pfInd[0] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
			{
				if (pFDay[j].m_fClose > pFDay[j-1].m_fClose)
					pFDay[i].m_pfInd[0] += 1;
			}

			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[0] / nN * 100;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_BOLL::CInd_BOLL() : CInd()
{
	m_psParam[0] = 26;
	m_psParam[1] = 20;
}
//布林线
void CInd_BOLL::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 2);
	m_cExpSize = 3;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 5) nN = 5;
	if (nN > 300) nN = 300;

	int nP = m_psParam[1];
	if (nP < 1) nP = 1;
	if (nP > 100) nP = 100;

	m_pnFirst[0] = m_pnFirst[1] = m_pnFirst[2] = nN - 1;
	if (nNum < nN) return;

	for (int i = 0; i < nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[0] = 0.0;
		else
		{
			pFDay[i].m_pfInd[0] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
				pFDay[i].m_pfInd[0] += pFDay[j].m_fClose;
			pFDay[i].m_pfInd[0] /= nN;
		}
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[3] = 0.0;
		else
		{
			pFDay[i].m_pfInd[3] = 0.0;
			for (int j=i-nN+1; j<=i; j++)
				pFDay[i].m_pfInd[3] += pow(pFDay[j].m_fClose - pFDay[i].m_pfInd[0], 2);
			pFDay[i].m_pfInd[3] /= nN;
		}
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < m_pnFirst[0])
			pFDay[i].m_pfInd[3] = 0.0;
		else
			pFDay[i].m_pfInd[3] = sqrt(pFDay[i].m_pfInd[3] * nN / (nN - 1));
	}

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[0] + nP / 10 * pFDay[i].m_pfInd[3];
		pFDay[i].m_pfInd[2] = pFDay[i].m_pfInd[0] - nP / 10 * pFDay[i].m_pfInd[3];
	}
}

//////////////////////////////////////////////////////////////////////////
//分水岭
void CInd_FSL::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 0);
	m_cExpSize = 2;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;

	m_pnFirst[0] = 0;
	m_pnFirst[1] = 0;

	//{SWL
	pFDay[0].m_pfInd[2] = pFDay[0].m_fClose;
	pFDay[0].m_pfInd[3] = pFDay[0].m_fClose;
	for (int i=1; i<nNum; i++)
	{
		pFDay[i].m_pfInd[2] = (pFDay[i-1].m_pfInd[2] * (5 - 1) + pFDay[i].m_fClose*2) / (5 + 1);
		pFDay[i].m_pfInd[3] = (pFDay[i-1].m_pfInd[3] * (10 - 1) + pFDay[i].m_fClose*2) / (10 + 1);
	}

	for (int i=0; i<nNum; i++)
		pFDay[i].m_pfInd[0] = (pFDay[i].m_pfInd[2] * 7 + pFDay[i].m_pfInd[3] * 3) / 10;
	//}

	//{SWS
	pFDay[0].m_pfInd[2] = pFDay[0].m_fClose;
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[2] = (pFDay[i-1].m_pfInd[2] * (12 - 1) + pFDay[i].m_fClose*2) / (12 + 1);

	for (int i=0; i<nNum; i++)
	{
		if (i < 5-1)
			pFDay[i].m_pfInd[3] = 0.0;
		else
		{
			pFDay[i].m_pfInd[3] = 0.0;
			for (int j=i-5+1; j<=i; j++)
				pFDay[i].m_pfInd[3] += pFDay[j].m_fVolume;
		}

		pFDay[i].m_pfInd[3] = Max(1, 100 * pFDay[i].m_pfInd[3] / (3 * xLTG.GetValue()));
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < 1)
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[0];
		else
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*pFDay[i].m_pfInd[2]+(1-pFDay[i].m_pfInd[3])*pFDay[i-1].m_pfInd[1];
	}
	//}
}

//////////////////////////////////////////////////////////////////////////
CInd_SAR::CInd_SAR() : CInd()
{
	m_psParam[0] = 10;
	m_psParam[1] = 2;
	m_psParam[2] = 20;
}

void CInd_SAR::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 3);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];//def 10
	if (nN < 1) nN = 1;
	if (nN > 100) nN = 100;

	int nStep = m_psParam[1];//def %2(0.02)
	if (nStep < 1) nStep = 1;
	if (nStep > 100) nStep = 100;

	int nMaxXP = m_psParam[2];//def 20%(0.2)
	if (nMaxXP < 5) nMaxXP = 5;
	if (nMaxXP > 100) nMaxXP = 100;

	m_pnFirst[0] = m_pnFirst[1] = nN;
	if (nNum <= nN) return;

	double fStep = double(nStep)/100.0;
	double fMax = double(nMaxXP)/100.0;

	BOOL bSARUp = TRUE;
	BOOL bLastUp = TRUE;
	double fLastMid = 0;
	int nUpDays = 0;

	// pFDay[i].m_pfInd[0] SAR(N, STEP, MAXP)
	// pFDay[i].m_pfInd[2] SARTURN(N, STEP, MAXP) 上升第一天1，下降第一天-1，其余0
	// pFDay[i].m_pfInd[1] SARSIGN(N, STEP, MAXP) 上升1，下降-1，上升下降第一天0
	for(int n = 0; n <= nN; n++)
	{
		double fMid = (pFDay[n].m_fHigh + pFDay[n].m_fLow)/2;
		if(n > 0)
		{
			if(fMid >= fLastMid)
			{
				nUpDays++;
				bLastUp = TRUE;
			}
			else
			{
				nUpDays--;
				bLastUp = FALSE;
			}
		}

		fLastMid = fMid;
	}

	if(nUpDays > 0)
		bSARUp = TRUE;
	else if(nUpDays < 0)
		bSARUp = FALSE;
	else
		bSARUp = bLastUp;

	double fAF = fStep;
	double fMaxHigh = 0;
	double fMinLow = 1e+10;
	for(int n = 0; n < nN; n++)
	{
		if(pFDay[n].m_fHigh > fMaxHigh)
			fMaxHigh = pFDay[n].m_fHigh;
		if(pFDay[n].m_fLow > 0 && pFDay[n].m_fLow < fMinLow)
			fMinLow = pFDay[n].m_fLow;
	}

	if(bSARUp)
	{
		pFDay[m_pnFirst[0]].m_pfInd[0] = fMinLow;
		pFDay[m_pnFirst[0]].m_pfInd[2] = 1;
		pFDay[m_pnFirst[0]].m_pfInd[1] = 0;
	}
	else
	{
		pFDay[m_pnFirst[0]].m_pfInd[0] = fMaxHigh;
		pFDay[m_pnFirst[0]].m_pfInd[2] = -1;
		pFDay[m_pnFirst[0]].m_pfInd[1] = 0;
	}

	for(int n = nN+1; n < nNum; n++)
	{
		fMaxHigh = 0;
		fMinLow = 1e+10;
		for(int m = n-nN; m < n; m++)
		{
			if(pFDay[m].m_fHigh > fMaxHigh)
				fMaxHigh = pFDay[m].m_fHigh;
			if(pFDay[m].m_fLow > 0 && pFDay[m].m_fLow < fMinLow)
				fMinLow = pFDay[m].m_fLow;
		}

		if(bSARUp)
		{
			if(pFDay[n-1].m_pfInd[2] == 0 && pFDay[n-1].m_pfInd[0] >= pFDay[n-1].m_fLow)	// turn
			{
				bSARUp = FALSE;

				pFDay[n].m_pfInd[0] = fMaxHigh;
				pFDay[n].m_pfInd[2] = -1;

				fAF = fStep;
			}
			else
			{
				if(pFDay[n].m_fHigh > fMaxHigh)
				{
					if(fAF + fStep <= fMax)
						fAF += fStep;
				}

				pFDay[n].m_pfInd[0] = pFDay[n-1].m_pfInd[0] + fAF*(fMaxHigh - pFDay[n-1].m_pfInd[0]);
				pFDay[n].m_pfInd[1] = 1;
			}
		}
		else	//SARDown
		{
			if(pFDay[n-1].m_pfInd[2] == 0 && pFDay[n-1].m_pfInd[0] <= pFDay[n-1].m_fHigh)	//turn
			{
				bSARUp = TRUE;

				pFDay[n].m_pfInd[0] = fMinLow;
				pFDay[n].m_pfInd[2] = 1;

				fAF = fStep;
			}
			else
			{
				if(pFDay[n].m_fLow < fMinLow)
				{
					if(fAF + fStep <= fMax)
						fAF += fStep;
				}

				pFDay[n].m_pfInd[0] = pFDay[n-1].m_pfInd[0] + fAF*(fMinLow - pFDay[n-1].m_pfInd[0]);
				pFDay[n].m_pfInd[1] = -1;
			}
		}
	}
}


//////////////////////////////////////////////////////////////////////////
void CInd_MCST::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 0);
	m_cExpSize = 1;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[2] = pFDay[i].m_fAmount/(100*pFDay[i].m_fVolume);
		pFDay[i].m_pfInd[3] = pFDay[i].m_fVolume/(xLTG.GetValue()/100);
	}

	for (int i=0; i<nNum; i++)
	{
		if (i < 1)
			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[2];
		else
			pFDay[i].m_pfInd[0] = pFDay[i].m_pfInd[3] * pFDay[i].m_pfInd[2] + (1 - pFDay[i].m_pfInd[3]) * pFDay[i-1].m_pfInd[0];
	}
}

//////////////////////////////////////////////////////////////////////////
//按部就班指标
CInd_ABJB::CInd_ABJB(CGoods* pGoods) : CInd(pGoods)
{
	m_psParam[0] = 5;
}

int CInd_ABJB::CompareFloat(double f)
{
	if ((f >= -0.00001) && (f <=0.00001))
		return 0;
	else if (f > 0.00001)
	{
		return 1;
	}else
		return -1;
}

double CInd_ABJB::GetLTG()
{
	if (m_pGoods)
		return m_pGoods->TransVol2Hand(m_pGoods->m_xLTG.GetValue()/100);
	else
		return 0;
}

void CInd_ABJB::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 1);
	m_cExpSize = 5;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 1) nN = 1;
	if (nN > 100) nN = 100;

	double fExpression[4] = {19*nN, 15*nN, 6*nN, 1*nN};

	m_pnFirst[0] = 0;
	m_pnFirst[1] = 0;
	m_pnFirst[2] = 0;
	m_pnFirst[3] = 0;
	
	int nFirst = 0, nLast = nNum-1, n;

	CDayCostPV	pPV[100];
	short		sPV = 0;
	double		fXS = 0.98;
	short		sMaxSteps = 100;

	double fSumVolume = GetLTG()*200;
	short sRecvFirst = 0;

	double fLastMax = 0, fLastMin = 0;
	double fMax = 0, fMin = 0;
	short sLastFirst = sRecvFirst;
	short sFirst, s;
	int lSpan = 0;

	for(n = nFirst; n <= nLast; n++)
	{
		if (fSumVolume > 0)
		{
			double fSum = 0;
			for (sFirst = n; sFirst >= sRecvFirst; sFirst--)
			{
				fSum += pFDay[sFirst].m_fVolume;
				if (fSum >= fSumVolume)
					break;
			}
		}
		else
		{
			sFirst = n-500;
		}

		if (sFirst < sRecvFirst)
			sFirst = sRecvFirst;

		BOOL bReCalc = FALSE;
		if(CompareFloat(pFDay[n].m_fHigh-fLastMax) == 1 || CompareFloat(pFDay[n].m_fLow-fLastMin) == -1)
			bReCalc = TRUE;
		else
		{
			for(s = sLastFirst; s < sFirst; s++)
			{
				if(CompareFloat(pFDay[s].m_fHigh-fLastMax) >=0 || CompareFloat(pFDay[s].m_fLow-fLastMin) <=0)
				{
					bReCalc = TRUE;
					break;
				}
			}
		}

		if(bReCalc)
		{
			fMax = 0;
			fMin = 1e+10;
			for (s = sFirst; s <= n; s++)
			{
				if (pFDay[s].m_fHigh > fMax)
					fMax = pFDay[s].m_fHigh;
				if (pFDay[s].m_fLow > 0 && pFDay[s].m_fLow < fMin)
					fMin = pFDay[s].m_fLow;
			}

			int lMax = static_cast<int>(fMax*1000);
			int lMin = static_cast<int>(fMin*1000);

			if(lMax < lMin)
			{
				int l = lMin;
				lMin = lMax;
				lMax = l;
			}

			lSpan = (lMax-lMin+sMaxSteps-2)/(sMaxSteps-1);
			if(lSpan == 0)
				lSpan = 1;
			if (GetGroup() != 2)
				lSpan = (lSpan+9)/10*10;

			sPV = short((lMax-lMin)/lSpan+1);
			if (sPV>sMaxSteps)
				sPV = sMaxSteps;

			for (s = 0; s < sPV; s++)
			{
				pPV[s].Initial();
				pPV[s].m_fPrice = fMin+lSpan*s/1000.0;
			}

			for (s = sFirst; s <= n; s++)
			{
				double fHigh = pFDay[s].m_fHigh;
				double fLow = pFDay[s].m_fLow;
				double pfExp[100];
				double fSumExp = 0;
				short i;
				for (i=0; i<sPV; i++)
				{
					pfExp[i] = 0;
					if (CompareFloat(pPV[i].m_fPrice-fHigh) <=0 && CompareFloat(pPV[i].m_fPrice-fLow) >=0)
					{
						pfExp[i] = 1;
						fSumExp += 1;
					}
				}

				if(fSumExp == 0)
				{
					i = int(((fHigh+fLow)/2-fMin)*1000/lSpan+0.5);
					if(i >= 0 && i < sPV)
						pfExp[i] = 1;
					fSumExp = 1;
				}

				double fLastClose;
				if (s > sRecvFirst)
					fLastClose = pFDay[s-1].m_fClose;
				else
					fLastClose = pFDay[s].m_fOpen;

				double fV = pFDay[s].m_fVolume;
				for (i = 0; i < sPV; i++)
				{
					if (CompareFloat(pPV[i].m_fPrice-fLastClose)>=0)
					{
						pPV[i].m_fRedVolume = pPV[i].m_fRedVolume*fXS + fV*pfExp[i]/fSumExp;
						pPV[i].m_fGreenVolume = pPV[i].m_fGreenVolume*fXS;
					}
					else
					{
						pPV[i].m_fRedVolume = pPV[i].m_fRedVolume*fXS;
						pPV[i].m_fGreenVolume = pPV[i].m_fGreenVolume*fXS + fV*pfExp[i]/fSumExp;
					}
				}	
			}
		}
		else
		{
			fMax = fLastMax;
			fMin = fLastMin;
			double fSignal;
			for(s = sLastFirst; s <= n; s++)
			{
				fSignal = pow(fXS, (n-s-1));

				if(s == sFirst)
				{
					for (short i=0; i<sPV; i++)
					{
						pPV[i].m_fRedVolume = pPV[i].m_fRedVolume*fXS;
						pPV[i].m_fGreenVolume = pPV[i].m_fGreenVolume*fXS;
					}

					s = n;
					fSignal = -1;
				}

				double fHigh = pFDay[s].m_fHigh;
				double fLow = pFDay[s].m_fLow;

				double pfExp[100];
				double fSumExp = 0;

				short i;
				for (i=0; i<sPV; i++)
				{
					pfExp[i] = 0;
					if (CompareFloat(pPV[i].m_fPrice-fHigh) <=0 && CompareFloat(pPV[i].m_fPrice-fLow) >=0)
					{
						pfExp[i] = 1;
						fSumExp += 1;
					}
				}

				if(fSumExp == 0)
				{
					i = int(((fHigh+fLow)/2-fMin)*1000/lSpan+0.5);
					if(i >= 0 && i < sPV)
						pfExp[i] = 1;
					fSumExp = 1;
				}

				double fLastClose;
				if (s > sRecvFirst)
					fLastClose = pFDay[s-1].m_fClose;
				else
					fLastClose = pFDay[s].m_fOpen;
				double fV = pFDay[s].m_fVolume;
				for (i = 0; i < sPV; i++)
				{
					if (CompareFloat(pPV[i].m_fPrice-fLastClose) >=0)
						pPV[i].m_fRedVolume -= fV*pfExp[i]/fSumExp*fSignal;
					else
						pPV[i].m_fGreenVolume -= fV*pfExp[i]/fSumExp*fSignal;
				}	
			}
		}

		double fSum = 0;
		for(s = 0; s < sPV; s++)
			fSum += pPV[s].m_fRedVolume + pPV[s].m_fGreenVolume;

		double fSumSave = fSum;
		for(int i = 0; i < m_cExpSize; i++)
		{
			if (i < 1)
				pFDay[n].m_pfInd[i] = pFDay[n].m_fClose;
			else
			{
				double fCostVol = 0;
				fSum = fSumSave * (fExpression[i-1]/100);
				for(s = 0; s < sPV; s++)
				{
					fCostVol += pPV[s].m_fRedVolume + pPV[s].m_fGreenVolume;
					if(fCostVol >= fSum)
						break;
				}

				pFDay[n].m_pfInd[i] = pPV[s].m_fPrice;
			}
		}
				
		fLastMax = fMax;
		fLastMin = fMin;
		sLastFirst = sFirst;
	}
}

//////////////////////////////////////////////////////////////////////////
CInd_LTSH::CInd_LTSH(CGoods* pGoods) : CInd(pGoods)
{
	m_psParam[0] = 8;
}

void CInd_LTSH::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 1);
	m_cExpSize = 1;

	if (pFDay == NULL || nNum <= 0) return;

	int nN = m_psParam[0];
	if (nN < 0) nN = 0;
	if (nN > 100) nN = 100;

	Calc_LLV(1, pFDay, nNum, nN*5);
	Calc_HHV(2, pFDay, nNum, nN*5);
	for (int i = 0; i < nNum; i++)
		pFDay[i].m_pfInd[0] = 100 - 100 * (pFDay[i].m_pfInd[2] - pFDay[i].m_fClose) / (pFDay[i].m_pfInd[2] - pFDay[i].m_pfInd[1]);
}

//////////////////////////////////////////////////////////////////////////
CInd_QSDD::CInd_QSDD(CGoods* pGoods) : CInd(pGoods)
{

}
void CInd_QSDD::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize == 1);
	m_cExpSize = 3;

	if (pFDay == NULL || nNum <= 0) return;
	
	m_pnFirst[0] = 0;
	m_pnFirst[1] = 0;
	m_pnFirst[2] = 18;

	Calc_LLV(4, pFDay, nNum, 14);
	Calc_HHV(5, pFDay, nNum, 14);

	//短期线
	for (int i=0; i<nNum; i++)
	{
		double temp = (pFDay[i].m_pfInd[5] - pFDay[i].m_pfInd[4]);
		if (temp < 0.00001)
		{
			temp = 0.01;
		}
		pFDay[i].m_pfInd[0] = -100 * (pFDay[i].m_pfInd[5] - pFDay[i].m_fClose) / (pFDay[i].m_pfInd[5] - pFDay[i].m_pfInd[4]);
	}

	Calc_LLV(2, pFDay, nNum, 34);
	Calc_HHV(3, pFDay, nNum, 34);
	for (int i=0; i<nNum; i++)
	{
		double temp = (pFDay[i].m_pfInd[3] - pFDay[i].m_pfInd[2]);
		if (temp < 0.00001)
		{
			temp = 0.01;
		}
		pFDay[i].m_pfInd[5] = -100 * (pFDay[i].m_pfInd[3] - pFDay[i].m_fClose) / temp;
	}
	
	//中期线
	pFDay[0].m_pfInd[1] = pFDay[0].m_pfInd[5];
	for (int i=1; i<nNum; i++)
		pFDay[i].m_pfInd[1] = (pFDay[i-1].m_pfInd[1]*(4-1)+pFDay[i].m_pfInd[5]*2)/(4+1);

	//长期线
	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[2] = 0.0;
		if (i < 18) continue;

		pFDay[i].m_pfInd[2] = 0.0;
		for (int j=i-19+1; j<=i; j++)
			pFDay[i].m_pfInd[2] += pFDay[j].m_pfInd[5];
		pFDay[i].m_pfInd[2] = pFDay[i].m_pfInd[2] / 19;
	}

	for (int i=0; i<nNum; i++)
	{
		pFDay[i].m_pfInd[0] += 100;
		pFDay[i].m_pfInd[1] += 100;
		if (i >= m_pnFirst[2])
			pFDay[i].m_pfInd[2] += 100;
	}

	//趋势顶底指标
	//见顶:(ref(中期线,1)>85 and  ref(短期线,1)>85 and ref(长期线,1)>65) and cross(长期线,短期线),color777777,PRECIS0;
	//顶部区域:(中期线<ref(中期线,1) and ref(中期线,1)>80) and (ref(短期线,1)>95 or ref(短期线,2)>95 ) and 长期线>60 and 短期线<83.5 
	//and 短期线<中期线 and 短期线<长期线+4,colorAAAAAA,PRECIS0;
	//顶部:=filter(顶部区域,4);

	//
	//底部区域:(长期线<12 and 中期线<8 and (短期线<7.2 or ref(短期线,1)<5) and (中期线>ref(中期线,1) or 短期线>ref(短期线,1)))
	//or (长期线<8 and 中期线<7 and 短期线<15 and 短期线>ref(短期线,1)) or (长期线<10 and 中期线<7 and 短期线<1),color4080AA ,PRECIS0;
	//

	//
	//低位金叉:长期线<15 and ref(长期线,1)<15 and 中期线<18 and 短期线>ref(短期线,1) and cross(短期线,长期线) and 短期线>中期线  
	//and (ref(短期线,1)<5 or ref(短期线,2)<5 )  and (中期线>=长期线 or ref( 短期线,1)<1 ),COLOR800080,PRECIS0;
	//
}


//////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////
// 大户资金算法，先放着，申万版本暂时不调用 [3/19/2013 frantqian]
void CIndL2_DHZJ::Calc( CFDayMobile* pFDay, int nNum )
{
	//ASSERT(m_cParamSize==0);
	m_cExpSize = 3;

	if (m_pGoods == NULL || pFDay == NULL || nNum <= 0) return;
	XInt32 xLTG = m_pGoods->m_xLTG;
	if (xLTG.GetValue() <= 0) return;

	for (int i = 0; i < nNum; i++)
	{
		pFDay[i].m_pfInd[0] = (pFDay[i].m_fVolOfBuy[1] - pFDay[i].m_fVolOfSell[1])/double(xLTG.GetValue())*10000000;
	}

	for (int i = 0, nSum = 0, nAvg = 0, j = 0; i < nNum; i++,j++)
	{
		//if (pfSH[i] > -EPS && pfSH[i] < EPS)
		//	continue;
		nSum++;
		nAvg++;

		nSum = nSum > 60 ? 60 : nSum;
		nAvg = nAvg > 10 ? 10 : nAvg;
		if (nSum == 1 && nAvg == 1)
		{
			pFDay[i].m_pfInd[3] = (pFDay[j].m_pfInd[3]*(nSum-1)+pFDay[i].m_pfInd[0]*2)/(nSum+1);
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*nSum;

			pFDay[i].m_pfInd[2] = (pFDay[j].m_pfInd[2]*(nAvg-1)+pFDay[i].m_pfInd[3]*nSum*2)/(nAvg+1);
			continue;
		}
		else
		{
			pFDay[i].m_pfInd[3] = (pFDay[j-1].m_pfInd[3]*(nSum-1)+pFDay[i].m_pfInd[0]*2)/(nSum+1);
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[3]*nSum;

			pFDay[i].m_pfInd[2] = (pFDay[j-1].m_pfInd[2]*(nAvg-1)+pFDay[i].m_pfInd[3]*nSum*2)/(nAvg+1);
		}
	}
}
//////////////////////////////////////////////////////////////////////////
// 仓位
//1、CWCPX_XH
//a:ref(CPX,1)
// 2、CW指标
//cpxw:="CWCPX_XH#week";
//cpxd:=CPX;
//a1:=IF((cpxd=2 or cpxd=1),100,IF(cpxw=2 and (cpxd=-1 or cpxd=0 or cpxd=-2),50,0));
//ma90:=ma(c,90);
//IF(c/ma90<0.99,a1*0.5,a1)
void CInd_CWX::Calc(CFDayMobile* pFDay, int nNum)
{
	LogTrace("%s", __FUNCTION__);

	bool isClose = CMDSApp::g_dwSysHMS>=150200 ? true:false;
	CArrayFDayMobile aFDayX;
	m_pGoods->LoadDay(aFDayX, DAY_DATA, WEEK_PERIOD, 0);
	CFDayMobile* pFDayW = aFDayX.m_pData;
	int nNumW = aFDayX.GetSize();
	if(nNumW>0 && pFDayW!=NULL)
	{
		CCalcCPX cpx;
		cpx.CalcCPX(300, m_pGoods->GetZDTBL(), DAY_DATA, WEEK_PERIOD, pFDayW, nNumW);
	}

	if (pFDay == NULL || nNum <= 0 || pFDayW == NULL || nNumW <= 0) return;

	m_cExpSize = 2;
	int nN = m_psParam[0];
	short sParam = 90;
	char cpx,preWeekCpx,a1,cwx;
	double ma90;
	int n=-1;

	for (int i=0; i<nNum; i++)
	{
		if (i<sParam-1)
			ma90 = pFDay[i].m_fClose;
		else
		{
			ma90 = 0;
			for (int j=i-sParam+1; j<=i; j++)
				ma90 += pFDay[j].m_fClose;
			ma90 /= sParam;
		}

		cpx = pFDay[i].m_cCPX;

		for(int m=nNumW-1; m>=0; m--)
		{
			if (pFDay[i].m_dwTime > pFDayW[m].m_dwTime && pFDay[i].m_dwTime <= pFDayW[m+1].m_dwTime)
			{
				preWeekCpx = pFDayW[m].m_cCPX;
				break;
			}
		}

		if (cpx == 1 || cpx == 2)
			a1 = 100;
		else if ((cpx == -1 || cpx == 0 || cpx == -2) && preWeekCpx == 2)
			a1 = 50;
		else 
			a1 = 0;

		//IF(c/ma90<0.99,a1*0.5,a1)
		if(pFDay[i].m_fClose/ma90<0.99)
			cwx = a1 * 0.5;
		else
			cwx = a1;

		pFDay[i].m_pfInd[0] = cwx;//仓位  m_pfInd[1]仓位对应文字描述下标

		switch(cwx)
		{
		case 0: 
			pFDay[i].m_pfInd[2] = 0;
			break;
		case 25:
			pFDay[i].m_pfInd[2] = 1;
			break;
		case 50:
			pFDay[i].m_pfInd[2] = 2;
			break;
		case 100:
			pFDay[i].m_pfInd[2] = 3;
			break;
		default:
			pFDay[i].m_pfInd[2] = 0;
		}

		//当天数据,收盘前按照昨天的仓位,收盘后按照计算出来的
		if (i == nNum -1 && isClose==false)
		{
			pFDay[i].m_pfInd[2] = pFDay[i-1].m_pfInd[2];
			pFDay[i].m_pfInd[0] = pFDay[i-1].m_pfInd[0];
		}

		//计算对应文字下标(up对应本身,down+4)
		for(int j=i; j>=0; j--)
		{
			if (pFDay[i].m_pfInd[2] != pFDay[j].m_pfInd[2])
			{
				n = j;
				break;
			}
		}

		if (n != -1 && i != 0 && pFDay[i].m_pfInd[2] < pFDay[n].m_pfInd[2])
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[2] + 4;
		else
			pFDay[i].m_pfInd[1] = pFDay[i].m_pfInd[2];
	}
}


//////////////////////////////////////////////////////////////////////////

CInd_EMA::CInd_EMA()
{
	m_psParam[0] = 5;
	m_psParam[1] = 10;
	m_psParam[2] = 20;
	m_psParam[3] = 30;
	m_psParam[4] = 60;
	m_psParam[5] = 120;
}


// EMA（N）=2/（N+1）*（C-昨日EMA）+昨日EMA；
//m_psParam:均线值
void CInd_EMA::Calc(CFDayMobile* pFDay, int nNum)
{
	LogTrace("%s", __FUNCTION__);

	//ASSERT(m_cParamSize<=6);
	m_cExpSize = m_cParamSize;

	if (pFDay == NULL || nNum <= 0 ) return;

	for (int c=0; c<m_cParamSize; c++)
	{
		short sParam = m_psParam[c];
		if (sParam<1)
			sParam = 1;

		m_pnFirst[c] = int(sParam-1);  //设置前置K线根数

		for (int i=0; i<nNum; i++)
		{
			if (i<m_pnFirst[c])
				pFDay[i].m_pfInd[c] = pFDay[i].m_fClose;
			else if (sParam<2)
				pFDay[i].m_pfInd[c] = pFDay[i].m_fClose;
			else
			{
				pFDay[i].m_pfInd[c] = (pFDay[i-1].m_pfInd[c]*(sParam-1)+pFDay[i].m_fClose*2)/(sParam+1);

			}
		}
	}

}




CInd_SLOWKD::CInd_SLOWKD()
{	m_psParam[0] = 5;
m_psParam[1] = 10;
m_psParam[2] = 20;
m_psParam[3] = 30;
m_psParam[4] = 60;
m_psParam[5] = 120;

}

//RSV:= (CLOSE-LLV(LOW,N))/(HHV(HIGH,N)-LLV(LOW,N))*100;
//FASTK:=SMA(RSV,M1,1);
//K:SMA(FASTK,M2,1);
//D:SMA(K,M3,1)

void CInd_SLOWKD::Calc(CFDayMobile* pFDay, int nNum)
{

	//ASSERT(m_cParamSize==3);
	m_cExpSize = 2;

	if (pFDay == NULL || nNum <= 0) return;

	double* pfLLV = new(std::nothrow) double[nNum];ZeroMemory(pfLLV, nNum*sizeof(double));
	double* pfHHV = new(std::nothrow) double[nNum];ZeroMemory(pfHHV, nNum*sizeof(double));
	double* pfRSV = new(std::nothrow) double[nNum];ZeroMemory(pfRSV, nNum*sizeof(double));
	double* pfFASTK = new(std::nothrow) double[nNum];ZeroMemory(pfFASTK, nNum*sizeof(double));

	if (pfLLV && pfHHV && pfRSV)
	{
		int nN = m_psParam[0];
		int nM1 = m_psParam[1];
		int nM2 = m_psParam[2];
		int nM3 = m_psParam[3];


		Calc_LLV(pfLLV, pFDay, nNum, nN);
		Calc_HHV(pfHHV, pFDay, nNum, nN);

		int nFirst;
		if (nN-1>0)
			nFirst = nN-1;
		else
			nFirst = 0;
		for (int i=0; i<nNum; i++)
		{
			if (pfHHV[i]!=pfLLV[i])
				pfRSV[i] = (pFDay[i].m_fClose-pfLLV[i])/(pfHHV[i]-pfLLV[i])*100;
			else if (i==0)
				pfRSV[i] = 0;
			else
				pfRSV[i] = pfRSV[i-1];
		}

		double* pfFASTK = pfLLV;
		Calc_SMA(pfFASTK, pfRSV, nNum, nM1, 1, 0);


		m_pnFirst[0] = 0;
		double* pfK = pfLLV;
		Calc_SMA(pfK, pfFASTK, nNum, nM2, 1, 0);


		m_pnFirst[1] = 0;
		double* pfD = pfHHV;
		Calc_SMA(pfD, pfK, nNum, nM3, 1, 0);

		for (int i=0; i<nNum; i++)
		{
			pFDay[i].m_pfInd[0] = pfK[i];
			pFDay[i].m_pfInd[1] = pfD[i];
		}
	}

	if (pfLLV)
		delete [] pfLLV;
	if (pfHHV)
		delete [] pfHHV;
	if (pfRSV)
		delete [] pfRSV;
	if (pfFASTK)
		delete [] pfFASTK;

}



CInd_ENE::CInd_ENE()
{
	m_psParam[0] = 10;
	m_psParam[1] = 11;
	m_psParam[2] = 9;

}
//UPPER:(1+M1/100)*MA(CLOSE,N);
//LOWER:(1-M2/100)*MA(CLOSE,N);
//ENE:(UPPER+LOWER)/2;
//M1代表中轨基准线的上浮百分比，一般设为11（即11%）；
//M2代表中轨基准线的下跌百分比，一般设为9（即9%）。
//m_psParam[0]:N
//m_psParam[1]:M1
//m_psParam[2]:M2
void CInd_ENE::Calc(CFDayMobile* pFDay, int nNum)
{
	//ASSERT(m_cParamSize==3);
	m_cExpSize = 3;

	if (pFDay == NULL || nNum <= 0 || m_cExpSize !=3) return;

	//范围保护
	if (m_psParam[0] < 1 || m_psParam[0] >300)
		m_psParam[0] = 10;

	if (m_psParam[1] < 1 || m_psParam[1] >300)
		m_psParam[1] = 11;

	if (m_psParam[2] < 1 || m_psParam[2] >300)
		m_psParam[2] = 9;


	double* pfMA_Close = new(std::nothrow) double[nNum];ZeroMemory(pfMA_Close, nNum*sizeof(double));


	if (pfMA_Close)
	{
		Calc_MA(pfMA_Close, pFDay, nNum, m_psParam[0]);


		for (int i=0; i<nNum; i++)
		{
			pFDay[i].m_pfInd[0] = pfMA_Close[i];
			pFDay[i].m_pfInd[1] = (1+(double)m_psParam[1]/100)*pfMA_Close[i];
			pFDay[i].m_pfInd[2] = (1-(double)m_psParam[2]/100)*pfMA_Close[i];
		}
	}

	if (pfMA_Close)
		delete [] pfMA_Close;
}



CInd_BIAS::CInd_BIAS()
{
	m_psParam[0] = 6;
	m_psParam[1] = 12;
	m_psParam[2] = 30;
}


//BIAS1 : (CLOSE-MA(CLOSE,L1))/MA(CLOSE,L1)*100;
//BIAS2 : (CLOSE-MA(CLOSE,L2))/MA(CLOSE,L2)*100;
//BIAS3 : (CLOSE-MA(CLOSE,L3))/MA(CLOSE,L3)*100;
//m_psParam[0]: L1 (1, 300, 6); 
//m_psParam[1]:	L2 (1, 300, 12); 		
//m_psParam[2]:	L3 (1, 300, 30); 

void CInd_BIAS::Calc(CFDayMobile* pFDay, int nNum)
{

	//ASSERT(m_cParamSize==3);
	m_cExpSize = 3;

	if (pFDay == NULL || nNum <= 0 || m_cExpSize !=3) return;

	//参数范围保护
	if (m_psParam[0] < 1 || m_psParam[0] >300)
		m_psParam[0] = 6;

	if (m_psParam[1] < 1 || m_psParam[1] >300)
		m_psParam[1] = 12;

	if (m_psParam[2] < 1 || m_psParam[2] >300)
		m_psParam[2] = 30;


	double* pfMA_Close1 = new(std::nothrow) double[nNum];ZeroMemory(pfMA_Close1, nNum*sizeof(double));
	double* pfMA_Close2 = new(std::nothrow) double[nNum];ZeroMemory(pfMA_Close2, nNum*sizeof(double));
	double* pfMA_Close3 = new(std::nothrow) double[nNum];ZeroMemory(pfMA_Close3, nNum*sizeof(double));


	if (NULL != pfMA_Close1 && NULL != pfMA_Close2 && NULL != pfMA_Close3)
	{
		Calc_MA(pfMA_Close1, pFDay, nNum, m_psParam[0]);
		Calc_MA(pfMA_Close2, pFDay, nNum, m_psParam[0]);
		Calc_MA(pfMA_Close3, pFDay, nNum, m_psParam[0]);


		for (int i=0; i<nNum; i++)
		{
			if (0 == pfMA_Close1[i])
				pfMA_Close1[i] = 1;
			if (0 ==pfMA_Close2[i])
				pfMA_Close2[i] = 1;
			if (0 == pfMA_Close3[i])
				pfMA_Close3[i] = 1;

			pFDay[i].m_pfInd[0] = (pFDay[i].m_fClose-pfMA_Close1[i])/pfMA_Close1[i]*100;
			pFDay[i].m_pfInd[1] = (pFDay[i].m_fClose-pfMA_Close2[i])/pfMA_Close1[i]*100;
			pFDay[i].m_pfInd[2] = (pFDay[i].m_fClose-pfMA_Close3[i])/pfMA_Close1[i]*100;
		}
	}

	if (pfMA_Close1)
		delete [] pfMA_Close1;

	if (pfMA_Close2)
		delete [] pfMA_Close2;

	if (pfMA_Close3)
		delete [] pfMA_Close3;

}
