#include "MDS.h"
#include "HisCompress.h"
#include "AppGlobal.h"
#include "Data.h"
#include "MyTime.h"

CBitCode BC_MIN_NUMBER[] = 
{
	{0x00,	1,	'C',	0,		0},					//0				= 0
	{0x02,	2,	'D',	4,		1},					//10	+4Bit	= 4Bit+1
	{0x06,	3,	'D',	8,		17},				//110	+8Bit	= 8Bit+16+1
	{0x07,	3,	'O',	16,		0},					//111	+16Bit	= 16Bit
};
int BCNUM_MIN_NUMBER = sizeof(BC_MIN_NUMBER)/sizeof(CBitCode);

CBitCode BC_MIN_TIME[] = 
{
	{0x00,	1,	'C',	0,		1},					//0				= 1时间单位
	{0x02,	2,	'D',	5,		2},					//10	+5Bit	= 5Bit+2时间单位
	{0x06,	3,	'D',	8,		34},				//110	+8Bit	= 8Bit+32+2时间单位
	{0x07,	3,	'O',	16,		0},					//111	+16Bit	= 16Bit时间单位
};
int BCNUM_MIN_TIME = sizeof(BC_MIN_TIME)/sizeof(CBitCode);

CBitCode BC_MINDAY_PRICE[] = 
{
	{0x02,	2,	'D',	8,		0},			//10	+8Bit		= 8Bit
	{0x00,	1,	'D',   16,		256},		//0		+16Bit		= 16Bit+256
	{0x06,	3,	'D',   24,		65792},		//110	+24Bit		= 24Bit+65536+256
	{0x07,	3,	'O',   32,		0},			//111	+32Bit		= 32Bit Org
};
int BCNUM_MINDAY_PRICE = sizeof(BC_MINDAY_PRICE)/sizeof(CBitCode);

CBitCode BC_MINDAY_PRICE_DIFFS[] = 
{
	{0x00,	1,	'I',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'I',	6,		8},			// 10	+6Bit		= 6Bit+8
	{0x06,	3,	'I',	8,		40},		// 110	+8Bit		= 8Bit+32+8
	{0x0E,	4,	'I',	16,		168},		// 1110	+16Bit		= 16Bit+128+32+8
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
int BCNUM_MINDAY_PRICE_DIFFS = sizeof(BC_MINDAY_PRICE_DIFFS)/sizeof(CBitCode);

CBitCode BC_MINDAY_PRICE_DIFF[] = 
{
	{0x00,	1,	'D',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'D',	6,		16},		// 10	+6Bit		= 6Bit+16
	{0x06,	3,	'D',	8,		80},		// 110	+8Bit		= 8Bit+64+16
	{0x0E,	4,	'D',	16,		336},		// 1110+16Bit		= 16Bit+256+64+16
	{0x0F,	4,	'O',	32,		0},			// 1111+32Bit		= 32Bit Org
};
int BCNUM_MINDAY_PRICE_DIFF = sizeof(BC_MINDAY_PRICE_DIFF)/sizeof(CBitCode);

CBitCode BC_MIN_VOL[] = 
{
	{0x00,	1,	'D',	12,		0},			// 0	+12Bit		= 12Bit
	{0x02,	2,	'D',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x06,	3,	'D',	24,		69632},		// 110	+24Bit		= 24Bit+65536+4096
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
int BCNUM_MIN_VOL = sizeof(BC_MIN_VOL)/sizeof(CBitCode);

static CBitCode BC_MIN_OI[] = 
{
	{0x00,	1,	'I',	12,		0},			// 0	+12Bit		= 12Bit
	{0x02,	2,	'I',	16,		2048},		// 10	+16Bit		= 16Bit+2048
	{0x06,	3,	'I',	24,		34816},		// 110	+24Bit		= 24Bit+32768+2048
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_MIN_OI = sizeof(BC_MIN_OI)/sizeof(CBitCode);

CBitCode BC_MINDAY_AMNT[] = 
{
	{0x00,	2,	'I',	16,		0},			// 00	+16Bit		= 16Bit
	{0x01,	2,	'I',	24,		32768},		// 01	+24Bit		= 24Bit+32768
	{0x02,	2,	'I',	28,		8421376},	// 10	+28Bit		= 28Bit+8388608+32768
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_MINDAY_AMNT = sizeof(BC_MINDAY_AMNT)/sizeof(CBitCode);

CBitCode BC_MINDAY_AMNT_DIFF[] = 
{ 
	{0x00,	2,	'I',	4,	0},		// 00	+4Bit		= 4Bit
	{0x01,	2,	'I',	8,	8},		// 01	+8Bit		= 8Bit+8
	{0x04,	3,	'I',	12, 136},	// 100	+12Bit		= 12Bit+128+8
	{0x05,	3,	'I',	16, 2184},	// 101	+16Bit		= 16Bit+2048+128+8
	{0x06,	3,	'I',	24, 34952},	// 110	+24Bit		= 24Bit+32768+2048+128+8
	{0x07,	3,	'O',	32,	0},		// 111	+32Bit		= 32Bit Org
};
int BCNUM_MINDAY_AMNT_DIFF = sizeof(BC_MINDAY_AMNT_DIFF)/sizeof(CBitCode);

CBitCode BC_MIN_TRADENUM_OLD[] = 
{
	{0x00,	1,	'D',	8,		0},			// 0	+8Bit		= 8Bit
	{0x02,	2,	'D',	12,		256},		// 10	+12Bit		= 12Bit+256
	{0x06,	3,	'D',	16,		4352},		// 110	+16Bit		= 16Bit+4096+256
	{0x07,	3,	'O',	24,		0},			// 111	+24Bit		= 24Bit Org
};
int BCNUM_MIN_TRADENUM_OLD = sizeof(BC_MIN_TRADENUM_OLD)/sizeof(CBitCode);

CBitCode BC_MIN_TRADENUM[] = 
{
	{0x00,	1,	'D',	8,		0},			// 0	+8Bit		= 8Bit
	{0x02,	2,	'D',	12,		256},		// 10	+12Bit		= 12Bit+256
	{0x06,	3,	'D',	16,		4352},		// 110	+16Bit		= 16Bit+4096+256
	{0x0E,	4,	'D',	24,		69888},		// 1110	+24Bit		= 24Bit+65536+4096+256
	{0x0F,	4,	'O',	32,		0},			// 1111	+32Bit		= 32Bit Org
};
int BCNUM_MIN_TRADENUM = sizeof(BC_MIN_TRADENUM)/sizeof(CBitCode);

CBitCode BC_MINDAY_STRONG[] = 
{
	{0x00,	2,	'I',	8,		0},			// 00	+8Bit		= 8Bit
	{0x01,	2,	'I',	11,		128},		// 01	+11Bit		= 11Bit+128
	{0x02,	2,	'I',	13,		1152},		// 10	+13Bit		= 13Bit+1024+128
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_MINDAY_STRONG = sizeof(BC_MINDAY_STRONG)/sizeof(CBitCode);

CBitCode BC_MINDAY_STRONG_DIFF[] = 
{
	{0x00,	1,	'I',	3,		0},			// 0	+3Bit		= 3Bit
	{0x02,	2,	'I',	6,		4},			// 10	+6Bit		= 6Bit+4
	{0x06,	3,	'I',	10,		36},		// 110	+10Bit		= 10Bit+32+4
	{0x0E,	4,	'I',	14,		548},		// 1110	+14Bit		= 14Bit+512+32+4
	{0x0F,	4,	'O',	32,		0},			// 1111	+32Bit		= 32Bit Org
};
int BCNUM_MINDAY_STRONG_DIFF = sizeof(BC_MINDAY_STRONG_DIFF)/sizeof(CBitCode);

CBitCode BC_MINDAY_RISEFALL[] = 
{
	{0x00,	1,	'D',	10,		0},			// 0	+10Bit		= 10Bit
	{0x02,	2,	'D',	12,		1024},		// 10	+12Bit		= 12Bit+1024
	{0x06,	3,	'O',	16,		0},			// 110	+16Bit		= 16Bit Org
};
int BCNUM_MINDAY_RISEFALL = sizeof(BC_MINDAY_RISEFALL)/sizeof(CBitCode);

CBitCode BC_DAY_NUMBER[] = 
{
	{0x00,	1,	'C',	0,		1},					//0				= 1
	{0x02,	2,	'D',	7,		2},					//10	+7Bit	= 7Bit+2
	{0x06,	3,	'D',	12,		130},				//110	+12Bit	= 12Bit+128+2
	{0x0E,	4,	'D',	16,		4226},				//1110	+16Bit	= 16Bit+4096+128+2
	{0x0F,	4,	'O',	32,		0},					//1111	+32Bit	= 32Bit Org
};
int BCNUM_DAY_NUMBER = sizeof(BC_DAY_NUMBER)/sizeof(CBitCode);

CBitCode BC_DAY_TIME[] = 
{
	{0x00,	1,	'C',	0,		1},					//0				= 1天
	{0x02,	2,	'C',	0,		3},					//10			= 3天
	{0x0C,	4,	'C',	0,		2},					//1100			= 2天
	{0x0D,	4,	'D',	4,		4},					//1101	+4Bit	= 4Bit+4天
	{0x0E,	4,	'D',	8,		20},				//1110	+8Bit	= 8Bit+20天
	{0x0F,	4,	'O',	16,		0},					//1111	+16Bit	= 16Bit天
};
int BCNUM_DAY_TIME = sizeof(BC_DAY_TIME)/sizeof(CBitCode);

CBitCode BC_DAY_TIME_MIND[] = 
{
	{0x00,	1,	'C',	0,		0},					//0				= 0天
	{0x02,	2,	'C',	0,		1},					//10			= 1天
	{0x06,	3,	'C',	0,		3},					//110			= 3天
	{0x1C,	5,	'D',	4,		0},					//11100	+4Bit	= 4Bit天
	{0x1D,	5,	'D',	8,		16},				//11101	+8Bit	= 8Bit+16天
	{0x1E,	5,	'O',	16,		0},					//11110	+16Bit	= 16Bit天
};
int BCNUM_DAY_TIME_MIND = sizeof(BC_DAY_TIME_MIND)/sizeof(CBitCode);

CBitCode BC_DAY_TIME_MINM[] = 
{
	{0x00,	1,	'D',	5,		0},					//0		+5Bit	= 5Bit时间单位
	{0x02,	2,	'D',	8,		32},				//10	+8Bit	= 8Bit+32时间单位
	{0x03,	2,	'O',	12,		0},					//11	+12Bit	= 12Bit时间单位
};
int BCNUM_DAY_TIME_MINM = sizeof(BC_DAY_TIME_MINM)/sizeof(CBitCode);

CBitCode BC_DAY_VOL[] = 
{
	{0x0F,	4,	'C',	0,		0},			// 1111				= 0
	{0x0E,	4,	'D',	12,		0},			// 1110	+12Bit		= 12Bit
	{0x02,	2,	'D',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x00,	1,	'D',	24,		69632},		// 0	+24Bit		= 24Bit+65536+4096
	{0x06,	3,	'O',	32,		0},			// 110  +32Bit		= 32Bit Org
};
int BCNUM_DAY_VOL = sizeof(BC_DAY_VOL)/sizeof(CBitCode);

CBitCode BC_DAY_TRADENUM[] = 
{
	{0x06,	3,	'D',	8,		0},			// 110	+8Bit		= 8Bit
	{0x02,	2,	'D',	12,		256},		// 10	+12Bit		= 12Bit+256
	{0x00,	1,	'D',	16,		4352},		// 0	+16Bit		= 16Bit+4096+256
	{0x0E,	4,	'D',	24,		69888},		// 1110	+24Bit		= 24Bit+65536+4096+256
	{0x0F,	4,	'O',	32,		0},			// 1111	+32Bit		= 32Bit Org
};
int BCNUM_DAY_TRADENUM = sizeof(BC_DAY_TRADENUM)/sizeof(CBitCode);

// MACD - DIF, DEA
CBitCode BC_EXP_MACD[] = 
{
	{0x00,	2,	'I',	12,		0},			// 00	+12Bit		= 12Bit
	{0x01,	2,	'I',	16,		2048},		// 01	+16Bit		= 16Bit+2048
	{0x02,	2,	'I',	20,		34816},		// 10	+20Bit		= 20Bit+32768+2048
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_EXP_MACD = sizeof(BC_EXP_MACD)/sizeof(CBitCode);

// KDJ - K, D; RSI; WR
CBitCode BC_EXP_KDJ[] = 
{
	{0x00,	1,	'D',	17,		0},			// 0	+17Bit		= 17Bit
	{0x01,	1,	'O',	32,		0},			// 1	+32Bit		= 32Bit Org
};
int BCNUM_EXP_KDJ = sizeof(BC_EXP_KDJ)/sizeof(CBitCode);

CBitCode BC_EXP_KDJ_DIFF[] = 
{
	{0x00,	1,	'I',	13,		0},			// 0	+13Bit		= 13Bit
	{0x02,	2,	'I',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_EXP_KDJ_DIFF = sizeof(BC_EXP_KDJ_DIFF)/sizeof(CBitCode);

// VR 
CBitCode BC_EXP_VR[] = 
{
	{0x00,	1,	'D',	20,		0},			// 0	+20Bit		= 20Bit
	{0x01,	1,	'O',	32,		0},			// 1	+32Bit		= 32Bit Org
};
int BCNUM_EXP_VR = sizeof(BC_EXP_VR)/sizeof(CBitCode);

CBitCode BC_EXP_VR_DIFF[] = 
{
	{0x00,	1,	'I',	14,		0},			// 0	+14Bit		= 14Bit
	{0x02,	2,	'I',	17,		8192},		// 10	+17Bit		= 17Bit+8192
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_EXP_VR_DIFF = sizeof(BC_EXP_VR_DIFF)/sizeof(CBitCode);

//////////////////////////////////////////////////////////////////////

static CBitCode BC_TRADE_NUMBER[] = 
{
	{0x00,	1,	'C',	0,		1},					//0				= 1
	{0x02,	2,	'D',	7,		2},					//10	+7Bit	= 7Bit+2
	{0x06,	3,	'D',	12,		130},				//110	+12Bit	= 12Bit+128+2
	{0x0E,	4,	'D',	16,		4226},				//1110	+16Bit	= 16Bit+4096+128+2
	{0x0F,	4,	'O',	32,		0},					//1111	+32Bit	= 32Bit Org
};
static int BCNUM_TRADE_NUMBER = sizeof(BC_TRADE_NUMBER)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_TIMEDIFF[] = 
{
	{0x00,	1,	'C',	0,		3},			// 0				= 3s
	{0x06,	3,	'C',	0,		6},			// 110				= 6s
	{0x02,	2,	'D',	4,		0},			// 10	+4Bit		= 4Bit
	{0x0E,	4,	'D',	8,		16},		// 1110	+8Bit		= 8Bit+16
	{0x0F,	4,	'O',	18,		0},			// 1111	+18Bit		= 18Bit Org
};
static int BCNUM_BARGAIN_TIMEDIFF = sizeof(BC_BARGAIN_TIMEDIFF)/sizeof(CBitCode);

static CBitCode BC_TRADE_PRICE[] = 
{
	{0x02,	2,	'D',	8,		0},			//10	+8Bit		= 8Bit
	{0x00,	1,	'D',   16,		256},		//0		+16Bit		= 16Bit+256
	{0x06,	3,	'D',   24,		65792},		//110	+24Bit		= 24Bit+65536+256
	{0x07,	3,	'O',   32,		0},			//111	+32Bit		= 32Bit Org
};
static int BCNUM_TRADE_PRICE = sizeof(BC_TRADE_PRICE)/sizeof(CBitCode);

static CBitCode BC_TRADE_PRICE_DIFFS[] = 
{
	{0x00,	1,	'C',	0,		0},			// 0				= 0
	{0x02,	2,	'I',	2,		0},			// 10	+2Bit		= 2Bit
	{0x06,	3,	'I',	4,		2},			// 110	+4Bit		= 4Bit+2
	{0x0E,	4,	'I',	8,		10},		// 1110	+8Bit		= 8Bit+8+2
	{0x1E,	5,	'I',	16,		138},		// 11110+16Bit		= 16Bit+128+8+2
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_TRADE_PRICE_DIFFS = sizeof(BC_TRADE_PRICE_DIFFS)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_VOL[] = 
{
	{0x00,	1,	'H',	4,		0},			// 0	+4Bit		= (4Bit)*100
	{0x02,	2,	'H',	8,		16},		// 10	+8Bit		= (8Bit+16)*100
	{0x0C,	4,	'H',	12,		272},		// 1100	+12Bit		= (12Bit+256+16)*100
	{0x0D,	4,	'D',	12,		0},			// 1101+12Bit		= 12Bit
	{0x0E,	4,	'D',	16,		4096},		// 1110+16Bit		= 16Bit+4096
	{0x1E,	5,	'D',	24,		69632},		// 11110+24Bit		= 24Bit+65536+4096
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_BARGAIN_VOL = sizeof(BC_BARGAIN_VOL)/sizeof(CBitCode);

static CBitCode BC_TRADE_TRADENUM[] = 
{
	{0x06,	3,	'D',	8,		0},			// 110	+8Bit		= 8Bit
	{0x02,	2,	'D',	12,		256},		// 10	+12Bit		= 12Bit+256
	{0x00,	1,	'D',	16,		4352},		// 0	+16Bit		= 16Bit+4096+256
	{0x0E,	4,	'D',	20,		69888},		// 1110	+20Bit		= 20Bit+65536+4096+256
	{0x1E,	5,	'D',	24,		1118464},	// 11110+24Bit		= 24Bit+1048576+65536+4096+256
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_TRADE_TRADENUM = sizeof(BC_TRADE_TRADENUM)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_TRADENUM_DIFF[] = 
{
	{0x1E,	5,	'C',	0,		0},			// 11110			= 0
	{0x00,	2,	'C',	0,		1},			// 00				= 1
	{0x01,	2,	'C',	0,		2},			// 01				= 2
	{0x02,	2,	'D',	4,		3},			// 10	+4Bit		= 4Bit+3
	{0x06,	3,	'D',	6,		19},		// 110	+6Bit		= 6Bit+16+3
	{0x0E,	4,	'D',	8,		83},		// 1110	+8Bit		= 8Bit+64+16+3
	{0x3E,	6,	'D',	14,		339},		// 111110+8Bit		= 14Bit+256+64+16+3
	{0x3F,	6,	'O',	32,		0},			// 111111+32Bit		= 32Bit Org
};
static int BCNUM_BARGAIN_TRADENUM_DIFF = sizeof(BC_BARGAIN_TRADENUM_DIFF)/sizeof(CBitCode);


CBitCode BC_AMT[] = 
{
	{0x00,	2,	'I',	8,		0},			// 00	+8Bit		= 8Bit
	{0x01,	2,	'I',	11,		128},		// 01	+11Bit		= 11Bit+128
	{0x02,	2,	'I',	13,		1152},		// 10	+13Bit		= 13Bit+1024+128
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
int BCNUM_AMT = sizeof(BC_AMT)/sizeof(CBitCode);

CBitCode BC_AMT_DIFF[] = 
{
	{0x00,	1,	'I',	3,		0},			// 0	+3Bit		= 3Bit
	{0x02,	2,	'I',	6,		4},			// 10	+6Bit		= 6Bit+4
	{0x06,	3,	'I',	10,		36},		// 110	+10Bit		= 10Bit+32+4
	{0x0E,	4,	'I',	14,		548},		// 1110	+14Bit		= 14Bit+512+32+4
	{0x0F,	4,	'O',	32,		0},			// 1111	+32Bit		= 32Bit Org
};
int BCNUM_AMT_DIFF = sizeof(BC_AMT_DIFF)/sizeof(CBitCode);
//////////////////////////////////////////////////////////////////////

extern CBitCode BC_DYNA_AMNT[];
extern int BCNUM_DYNA_AMNT;
extern CBitCode BC_DYNA_AMNT_DIFF[];
extern int BCNUM_DYNA_AMNT_DIFF;


int CHisCompress::CompressDay(CBitStream& stream, CGoods* pGoods, CSortArrayDayMobile& aDay, WORD wDataType, WORD wPeriod, char cOpenFund, char cIndType, char MaExp, char VMAExp, char IndExp,WORD wVersion)
{
	if (pGoods==NULL)
		return ERR_COMPRESS_PARAM;

	try
	{
		CDayMobile* pDay = aDay.m_pData;
		int nDay = aDay.GetSize();

		if (pDay==NULL || nDay==0)
		{
			stream.EncodeData(0, BC_DAY_NUMBER, BCNUM_DAY_NUMBER);
			return 0;
		}

		WORD wDayNum = (WORD)nDay;
		stream.EncodeData(wDayNum, BC_DAY_NUMBER, BCNUM_DAY_NUMBER);

		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);

		DWORD dwLastDay = 0;
		WORD  wLastMinute = MINUTE_BEGIN_TIME;
		DWORD dwLastClose = 0;
		XInt32 xLastAmount = 0;
		DWORD pdwLastMA[6] = {0};
		XInt32 pxLastVMA[6] = {0};
		XInt32 pxLastInd[6] = {0};
		DWORD dwLastHsl = 0;
		XInt32 xLastZJJL = 0;

		DWORD dbgOpen, dbgHigh, dbgLow, dbgClose;

		WORD wMinuteSpan = 1;
		if (wDataType==DAY_DATA)
			wMinuteSpan = wPeriod;

		CDayMobile* pCurDay = pDay;
		for (WORD w=0; w<wDayNum; w++)
		{
			if (wDataType==DAY_DATA)		// yyyymmdd  //...day
			{
				DWORD dwDate = pCurDay->m_dwTime;
				DWORD dwCurDay = GetDay(dwDate);

				stream.EncodeData(dwCurDay, BC_DAY_TIME, BCNUM_DAY_TIME, &dwLastDay);

				dwLastDay = dwCurDay;
			}
			else		// ymmddhhmm
			{
				DWORD dwTimeDay = 2000*10000 + pCurDay->m_dwTime/10000;		// yyyymmdd
				WORD wTimeMin = WORD(pCurDay->m_dwTime%10000);					// hhmm

				DWORD dwCurDay = GetDay(dwTimeDay);

				stream.EncodeData(dwCurDay, BC_DAY_TIME_MIND, BCNUM_DAY_TIME_MIND, &dwLastDay);

				if (dwCurDay!=dwLastDay)
				{
					dwLastDay = dwCurDay;
					wLastMinute = MINUTE_BEGIN_TIME;
				}
				
				WORD wMinutes = GetMinutesDiff(wLastMinute, wTimeMin);

				if (wMinutes==wMinuteSpan)
					stream.WriteDWORD(0, 1);
				else
				{
					stream.WriteDWORD(1, 1);
					stream.EncodeData(wMinutes, BC_DAY_TIME_MINM, BCNUM_DAY_TIME_MINM);
				}

				wLastMinute = wTimeMin;
			}
	
			if (pCurDay->m_cStatus!=1)
			{
				stream.WriteBOOL(0);

				// m_dwClose
				DWORD dwClose = pCurDay->m_dwClose/PRICE_DIVIDE;
				if (w==0)
					stream.EncodeData(dwClose, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
				else
					stream.EncodeData(dwClose, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastClose);
				dwLastClose = dwClose;

				dbgClose = dwClose;

				if (pCurDay->m_cCPX>0)//1,2
					stream.WriteBOOL(TRUE);		// hong
				else//-1,-2
					stream.WriteBOOL(FALSE);	// lv

				if (pCurDay->m_cCPX==1 || pCurDay->m_cCPX==-1)
					stream.WriteBOOL(TRUE);		// BS
				else
					stream.WriteBOOL(FALSE);

				//21219 金额 [4/3/2013 frantqian]
				//21217 (20130425包号降级,仓位,金额同一个版本,都升级成17即可)
				if (wVersion >= 9)
				{
					if (w==0)
						stream.EncodeXInt32(pCurDay->m_xAmount, BC_DYNA_AMNT, BCNUM_DYNA_AMNT);
					else
						stream.EncodeXInt32(pCurDay->m_xAmount, BC_DYNA_AMNT_DIFF, BCNUM_DYNA_AMNT_DIFF, &xLastAmount);
					xLastAmount = pCurDay->m_xAmount;
				}

				if (wVersion >=13)
				{
					stream.EncodeData(pCurDay->m_dwHSL, BC_MIN_TRADENUM, BCNUM_MIN_TRADENUM, &dwLastHsl);
					dwLastHsl = pCurDay->m_dwHSL;
					stream.EncodeXInt32(pCurDay->m_xZJJL, BC_AMT_DIFF, BCNUM_AMT_DIFF, &xLastZJJL);
					xLastZJJL = pCurDay->m_xZJJL;
				}

				if (cOpenFund==0)
				{
					// m_xVolume
					if (pGoods->CheckVolume(pCurDay->m_xVolume))
					{
						stream.WriteBOOL(TRUE);

						// m_dwHigh, m_dwLow, m_dwOpen
						stream.EncodeData(pCurDay->m_dwHigh/PRICE_DIVIDE, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwClose);
						DWORD dwLow = pCurDay->m_dwLow/PRICE_DIVIDE;
						stream.EncodeData(dwLow, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwClose, TRUE);
						stream.EncodeData(pCurDay->m_dwOpen/PRICE_DIVIDE, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwLow);

						dbgOpen = pCurDay->m_dwOpen/PRICE_DIVIDE;
						dbgHigh = pCurDay->m_dwHigh/PRICE_DIVIDE;
						dbgLow = dwLow;
						
					}
					else
						stream.WriteBOOL(FALSE);

					for (int c=0; c<MaExp; c++)
					{
						DWORD dwMA;
						////21219 MA除以10发给客户端,精度丢掉了一位,导致分钟线走势很不平滑,现修改成按照原始值发送,包号升级21219
						//if(wVersion >= 10)
						//	dwMA = pCurDay->m_pdwMA[c]+0.5;
						//else
							dwMA = (pCurDay->m_pdwMA[c]+PRICE_DIVIDE/2)/PRICE_DIVIDE;

						if (w==0)
							stream.EncodeData(dwMA, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwClose);
						else
							stream.EncodeData(dwMA, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, pdwLastMA+c);
						
						pdwLastMA[c] = dwMA;
					}

					for (int c=0; c<VMAExp; c++)
					{
						if (w==0)
							stream.EncodeXInt32(pCurDay->m_pxVMA[c], BC_DAY_VOL, BCNUM_DAY_VOL);
						else
							stream.EncodeXInt32(pCurDay->m_pxVMA[c], BC_DAY_VOL, BCNUM_DAY_VOL, pxLastVMA+c);
						pxLastVMA[c] = pCurDay->m_pxVMA[c];
					}

				}
			}
			else
				stream.WriteBOOL(1);

			if (cOpenFund==0)
			{
				for (int c=0; c<IndExp; c++)
				{
					if (w==0)
					{
						if (cIndType == 2)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_DAY_VOL, BCNUM_DAY_VOL);
						else if (cIndType == 3)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_EXP_MACD, BCNUM_EXP_MACD);
						else if (cIndType == 7)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_EXP_VR, BCNUM_EXP_VR);
						else if (cIndType == 8 || cIndType == 9 || cIndType == 10 || cIndType == 11 || cIndType == 12 || cIndType == 32 || cIndType == 33)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_AMT, BCNUM_AMT);
						else if(wVersion >= 9 && pGoods->GetID() == 300 && cIndType == 127)
							stream.WriteDWORD(pCurDay->m_pxInd[c].GetRawData(),8);
						else
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_EXP_KDJ, BCNUM_EXP_KDJ);
					}
					else
					{
						if (cIndType == 2)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_DAY_VOL, BCNUM_DAY_VOL, pxLastInd+c);
						else if (cIndType == 3)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_EXP_MACD, BCNUM_EXP_MACD, pxLastInd+c);
						else if (cIndType == 7)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_EXP_VR_DIFF, BCNUM_EXP_VR_DIFF, pxLastInd+c);
						else if (cIndType == 8 || cIndType == 9 || cIndType == 10 || cIndType == 11 || cIndType == 12 || cIndType == 32 || cIndType == 33)
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_AMT_DIFF, BCNUM_AMT_DIFF, pxLastInd+c);
						else if(wVersion >= 9 && pGoods->GetID() == 300 && cIndType == 127)
							stream.WriteDWORD(pCurDay->m_pxInd[c].GetRawData(),8);
						else
							stream.EncodeXInt32(pCurDay->m_pxInd[c], BC_EXP_KDJ_DIFF, BCNUM_EXP_KDJ_DIFF, pxLastInd+c);
					}
					pxLastInd[c] = pCurDay->m_pxInd[c];
				}
			}

			pCurDay++;
		}

	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return 0;
}

void CHisCompress::CompressDayNum(CBitStream& stream, DWORD dwTotal)
{
	stream.EncodeData(dwTotal, BC_DAY_NUMBER, BCNUM_DAY_NUMBER);
}

void CHisCompress::CompressDate(CBitStream& stream, DWORD& dwDate)
{
	DWORD dwCurDay = GetDay(dwDate);
	stream.EncodeData(dwCurDay, BC_DAY_TIME, BCNUM_DAY_TIME);
}

DWORD CHisCompress::ExpandDate(CBitStream& stream)
{
	DWORD dwCurDay = 0;
	stream.DecodeData(dwCurDay, BC_DAY_TIME, BCNUM_DAY_TIME);

	struct tm tmBase;
	time_t ttBase, ttDate;

	tmBase.tm_year = DAY_BEGIN_YEAR - 1900;
	tmBase.tm_mon = 0;
	tmBase.tm_mday = 1;
	tmBase.tm_hour = 0;
	tmBase.tm_min = 0;
	tmBase.tm_sec = 0;
	tmBase.tm_isdst = 0;

	ttBase = kernel_mktime(&tmBase);
	ttDate = ttBase+dwCurDay*86400;

	// localtime多线程不安全，改成_r函数 [2/5/2013 frantqian]
	//struct tm * pDate = localtime(&ttDate);
	struct tm tDate;
	localtime_r(&ttDate,&tDate);

	return  (tDate.tm_year+1900)*10000 + (tDate.tm_mon+1)*100 + tDate.tm_mday;
}

DWORD CHisCompress::GetDay(DWORD& dwDate)
{
	DWORD dwBase = DAY_BEGIN_YEAR*10000+101;	// 19800101
	if (dwDate<dwBase)
		dwDate = dwBase;

	struct tm tmBase, tmDate;
	time_t ttBase, ttDate;

	tmBase.tm_year = DAY_BEGIN_YEAR - 1900;
	tmBase.tm_mon = 0;
	tmBase.tm_mday = 1;
	tmBase.tm_hour = 0;
	tmBase.tm_min = 0;
	tmBase.tm_sec = 0;
	tmBase.tm_isdst = 0;

	tmDate.tm_year = dwDate/10000 - 1900;
	tmDate.tm_mon = dwDate%10000/100 - 1;
	tmDate.tm_mday = dwDate%100;
	tmDate.tm_hour = 0;
	tmDate.tm_min = 0;
	tmDate.tm_sec = 0;
	tmDate.tm_isdst = 0;

	ttBase = kernel_mktime(&tmBase);
	ttDate = kernel_mktime(&tmDate);

	return (ttDate-ttBase)/86400;
}

int CHisCompress::ExpandMinute(CBitStream& stream, CGoods* pGoods, CSortArrayMinute& aMinute)
{
try
	{
		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
//		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		BOOL bClear = stream.ReadDWORD(1);
		if (bClear)
		{
			DWORD dwDate = stream.ReadDWORD(32);

		}

		DWORD dwMinuteNum;
		stream.DecodeData(dwMinuteNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);
		WORD wMinuteNum = (WORD)dwMinuteNum;
		//if (wMinuteNum>1)
		//	int iii = 0;

		BOOL bIsGZQH = pGoods->IsGZQH();

		BOOL bIsIndex, bIndex, bIsLevel2;
		if (bIsGZQH==FALSE)
		{
			bIsIndex = stream.ReadDWORD(1);
			bIndex = stream.ReadDWORD(1);
			bIsLevel2 = stream.ReadDWORD(1);
		}
		else
		{
			bIsIndex = FALSE;
			bIndex = FALSE;
			bIsLevel2 = FALSE;
		}

		BYTE cCheckSum = 0;

		DWORD dwCountMinute = pGoods->m_dwCountMinute;
		if (wMinuteNum>0)
		{
			aMinute.RemoveAt(0, aMinute.GetSize());

			dwCountMinute++;

			WORD wLastTime = MINUTE_BEGIN_TIME;
			DWORD dwLastClose = 0;
			XInt32 xOI, xLastOI;

			DWORD dwLastSellPrice = 0;
			DWORD dwLastBuyPrice = 0;

			int lLastStrong = 0;

			CMinute min;
			for (WORD w=0; w<wMinuteNum; w++)
			{
				min.Clear();

				// m_wTime,  hhmm
				DWORD dwTemp = 0;
				stream.DecodeData(dwTemp, BC_MIN_TIME, BCNUM_MIN_TIME);
				WORD wMinutes = (WORD)dwTemp;
				min.m_wTime = AddMinutes(wLastTime, wMinutes);

				wLastTime = min.m_wTime;

				// m_dwOpen, m_dwHigh, m_dwLow, m_dwClose, m_dwAve
				DWORD dwOpen = 0;
				DWORD dwHigh = 0;
				DWORD dwLow = 0;
				DWORD dwClose = 0;

				if (w==0)
					stream.DecodeData(dwOpen, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
				else
					stream.DecodeData(dwOpen, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastClose);

				stream.DecodeData(dwHigh, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwOpen);
				stream.DecodeData(dwLow, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwOpen, TRUE);
				stream.DecodeData(dwClose, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwLow);
				
				dwLastClose = dwClose;

				min.m_dwOpen = dwOpen * PRICE_DIVIDE;
				min.m_dwHigh = dwHigh * PRICE_DIVIDE;
				min.m_dwLow = dwLow * PRICE_DIVIDE;
				min.m_dwClose = dwClose * PRICE_DIVIDE;

				stream.DecodeData(min.m_dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwClose);

				// m_xVolume, m_xAmount, m_dwTradeNum
				stream.DecodeXInt32(min.m_xVolume, BC_MIN_VOL, BCNUM_MIN_VOL);

				if (bIsGZQH || bIsIndex)
					stream.DecodeXInt32(min.m_xAmount, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);
				else
				{
					XInt32 xBase = (INT64)min.m_xVolume * min.m_dwAve * PRICE_DIVIDE;
					stream.DecodeXInt32(min.m_xAmount, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT, &xBase);
				}

				if (bIsGZQH)
				{
					if (w==0)
						stream.DecodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI);
					else
						stream.DecodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI, &xLastOI);
					min.m_dwTradeNum = xOI.GetRawData();
					xLastOI = xOI;
				}
				else
				{
					stream.DecodeData(min.m_dwTradeNum, BC_MIN_TRADENUM, BCNUM_MIN_TRADENUM);

					// m_dwSellPrice, m_dwBuyPrice, m_xSellVol, m_xBuyVol
					if (w==0)
					{
						stream.DecodeData(min.m_dwSellPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwHigh);
						stream.DecodeData(min.m_dwBuyPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwLow);
					}
					else
					{
						stream.DecodeData(min.m_dwSellPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastSellPrice);
						stream.DecodeData(min.m_dwBuyPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastBuyPrice);
					}

					dwLastSellPrice = min.m_dwSellPrice;
					dwLastBuyPrice = min.m_dwBuyPrice;

					stream.DecodeXInt32(min.m_xSellVol, BC_DAY_VOL, BCNUM_DAY_VOL);
					stream.DecodeXInt32(min.m_xBuyVol, BC_DAY_VOL, BCNUM_DAY_VOL);

					if(bIndex)
					{
						//if (pGoods->m_dwNameCode==300)
						//	int iii = 0;

						// m_nStrong
						if (w==0)
							stream.DecodeData(dwTemp, BC_MINDAY_STRONG, BCNUM_MINDAY_STRONG);
						else
							stream.DecodeData(dwTemp, BC_MINDAY_STRONG_DIFF, BCNUM_MINDAY_STRONG_DIFF, (PDWORD)&lLastStrong);

						min.m_nStrong = dwTemp;

						lLastStrong = min.m_nStrong;

						// m_wRise, m_wFall
						stream.DecodeData(dwTemp, BC_MINDAY_RISEFALL, BCNUM_MINDAY_RISEFALL);
						min.m_wRise = (WORD)dwTemp;

						stream.DecodeData(dwTemp, BC_MINDAY_RISEFALL, BCNUM_MINDAY_RISEFALL);
						min.m_wFall = (WORD)dwTemp;
					}

					if (bIsLevel2)
					{
						ExpandOrderCounts(stream, min.m_ocOrder, min, TRUE);
						ExpandOrderCounts(stream, min.m_ocTrade, min, FALSE);

						for (int j = 0; j < 2; j++)
						{
							stream.DecodeXInt32(min.m_pxNewOrder[j], BC_MIN_VOL, BCNUM_MIN_VOL);
							stream.DecodeXInt32(min.m_pxDelOrder[j], BC_MIN_VOL, BCNUM_MIN_VOL);
						}
					}
				}
				
				min.m_dwCount = dwCountMinute;

				cCheckSum += min.GetCheckSum(bIsGZQH, bIndex, bIsLevel2, PRICE_DIVIDE);
/*
				BYTE cCheck = min.GetCheckSum(bIndex, bIsLevel2);
				BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);
				if (cStreamChkSum!=cCheck)
					int iii = 0;
*/
				aMinute.Add(min);
			}

			pGoods->AddMinutes(aMinute);
			pGoods->m_dwCountMinute = dwCountMinute;
		}

		BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);

		if (cStreamChkSum!=cCheckSum)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return 0;
}

void CHisCompress::ExpandOrderCounts(CBitStream& stream, COrderCounts& ocNew, CMinute& min, BOOL bOrder)
{
		DWORD dwNum = 0;
	XInt32 xVolBuy = 0;
	XInt32 xVolSell = 0;
	XInt32 xAmtBuy = 0;
	XInt32 xAmtSell = 0;

	for (int i=0; i<4; i++)
	{
		stream.DecodeData(ocNew.m_pdwNumOfBuy[i], BC_MIN_TRADENUM, BCNUM_MIN_TRADENUM);

		if (i>0 || bOrder==TRUE)
			stream.DecodeData(ocNew.m_pdwNumOfSell[i], BC_MIN_TRADENUM, BCNUM_MIN_TRADENUM);

		if (bOrder==FALSE)
		{
			dwNum += ocNew.m_pdwNumOfBuy[i];
			if (i>0)
				dwNum += ocNew.m_pdwNumOfSell[i];
		}

		if (i>0 || bOrder==FALSE)
		{
			stream.DecodeXInt32(ocNew.m_pxVolOfBuy[i], BC_MIN_VOL, BCNUM_MIN_VOL);
			stream.DecodeXInt32(ocNew.m_pxAmtOfBuy[i], BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

			xVolBuy += ocNew.m_pxVolOfBuy[i];
			xAmtBuy += ocNew.m_pxAmtOfBuy[i];
		}

		if (i>0)
		{
			stream.DecodeXInt32(ocNew.m_pxVolOfSell[i], BC_MIN_VOL, BCNUM_MIN_VOL);
			stream.DecodeXInt32(ocNew.m_pxAmtOfSell[i], BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

			xVolSell += ocNew.m_pxVolOfSell[i];
			xAmtSell += ocNew.m_pxAmtOfSell[i];
		}
	}

	if (bOrder==TRUE)
	{
		ocNew.m_pxVolOfBuy[0] = min.m_xVolume-xVolBuy;
		ocNew.m_pxVolOfSell[0] = min.m_xVolume-xVolSell;
		ocNew.m_pxAmtOfBuy[0] = min.m_xAmount-xAmtBuy;
		ocNew.m_pxAmtOfSell[0] = min.m_xAmount-xAmtSell;
	}
	else
	{
		if (min.m_dwTradeNum>=dwNum)
			ocNew.m_pdwNumOfSell[0] = min.m_dwTradeNum-dwNum;
		ocNew.m_pxVolOfSell[0] = min.m_xVolume-(xVolSell+xVolBuy);
		ocNew.m_pxAmtOfSell[0] = min.m_xAmount-(xAmtSell+xAmtBuy);
	}
}

int CHisCompress::CompressMinute(CBitStream& stream, BOOL bIndex, CArrayMinute& aMinute, WORD& wPosStart, DWORD dwDiv)
{
	WORD wMinuteNum = (WORD)aMinute.GetSize();
	
	if (wPosStart>wMinuteNum)
		wPosStart = 0;

	int nRet = 0;

	wMinuteNum -= wPosStart;
/*
	if (bIndex)
	{
		if (wMinuteNum>40)
		{
			wMinuteNum = 40;
			nRet = 1;
		}
	}
	else
	{
		if (wMinuteNum>80)
		{
			wMinuteNum = 80;
			nRet = 1;
		}
	}
*/
	try
	{	
		stream.EncodeData(wMinuteNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);

		stream.WriteBOOL(bIndex);

		WORD wLastTime = MINUTE_BEGIN_TIME;
		DWORD dwLastClose = 0;
		DWORD dwLastAve = 0;
		int lLastStrong = 0;

		for (WORD w=0; w<wMinuteNum; w++)
		{
			CMinute& min = aMinute[w+wPosStart];

			// m_wTime,  hhmm
			WORD wMinutes = GetMinutesDiff(wLastTime, min.m_wTime);
			stream.EncodeData(wMinutes, BC_MIN_TIME, BCNUM_MIN_TIME);
			wLastTime = min.m_wTime;

			// m_dwOpen, m_dwHigh, m_dwLow, m_dwClose, m_dwAve
			DWORD dwClose = g_DivPrice(min.m_dwClose, dwDiv);
			DWORD dwAve = g_DivPrice(min.m_dwAve, dwDiv);

			if (w==0)
			{
				stream.EncodeData(dwClose, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
				stream.EncodeData(dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwClose);
			}
			else
			{
				stream.EncodeData(dwClose, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastClose);
				stream.EncodeData(dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastAve);
			}

			dwLastClose = dwClose;
			dwLastAve = dwAve;

			stream.EncodeXInt32(min.m_xVolume, BC_MIN_VOL, BCNUM_MIN_VOL);
          
			if(bIndex)
			{
				// m_lStrong
				if (w==0)
					stream.EncodeData(min.m_nStrong, BC_MINDAY_STRONG, BCNUM_MINDAY_STRONG);
				else
					stream.EncodeData(min.m_nStrong, BC_MINDAY_STRONG_DIFF, BCNUM_MINDAY_STRONG_DIFF, (PDWORD)&lLastStrong);

				lLastStrong = min.m_nStrong;
			}
		}

		wPosStart += wMinuteNum;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return nRet;
}
//////////////////////////////////////////////////////////////////////////
//分时博弈& 资金净流
int CHisCompress::CompressMinuteVolLevel2(CBitStream& stream, const XInt32& xLTG, CArrayMinute& aMinute, WORD& wPosStart, const XInt32& xRecvAmtDiff)
{
	WORD wMinuteNum = (WORD)aMinute.GetSize();
	
	if (wPosStart>wMinuteNum)
		wPosStart = 0;

	int nRet = 0;

	wMinuteNum -= wPosStart;
/*
	if (bIndex)
	{
		if (wMinuteNum>40)
		{
			wMinuteNum = 40;
			nRet = 1;
		}
	}
	else
	{
		if (wMinuteNum>80)
		{
			wMinuteNum = 80;
			nRet = 1;
		}
	}
*/
	try
	{	
		if (xLTG.GetValue() <= 0)
		{
			stream.EncodeData(0, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);
			return nRet;
		}

		stream.EncodeData(wMinuteNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);

		WORD wLastTime = MINUTE_BEGIN_TIME;

		XInt32 xNow = 0, xLast = 0;
		//资金净流
		double fLastSumAmtDiff = 0;
		double fSumAmtDiff = 0;// = xRecvAmtDiff; 

		//资金博弈
		double fLastSumCJ = 0, fLastSumDH = 0, fLastSumZH = 0, fLastSumSH = 0;
		double fSumCJ = 0, fSumDH = 0, fSumZH = 0, fSumSH = 0;
		WORD wAmtNum = (WORD)aMinute.GetSize();
		for (WORD i = 0; i < wAmtNum; i++)
		{
			if (i == wPosStart)
			{
				break;
			}

			CMinute& min = aMinute[i];
			
			//资金净流 
			fSumAmtDiff += double(min.m_ocOrder.m_pxAmtOfBuy[3] + min.m_ocOrder.m_pxAmtOfBuy[2]
				- min.m_ocOrder.m_pxAmtOfSell[3] - min.m_ocOrder.m_pxAmtOfSell[2]) / 10000000.0;

			//超级资金
			fSumCJ += double((min.m_ocOrder.m_pxVolOfBuy[3]-min.m_ocOrder.m_pxVolOfSell[3])*1000000)/double(xLTG.GetValue()/100);

			//大户资金
			fSumDH += double((min.m_ocOrder.m_pxVolOfBuy[2]-min.m_ocOrder.m_pxVolOfSell[2])*1000000)/double(xLTG.GetValue()/100);

			//中户资金
			fSumZH += double((min.m_ocOrder.m_pxVolOfBuy[1]-min.m_ocOrder.m_pxVolOfSell[1])*1000000)/double(xLTG.GetValue()/100);

			//散户资金
			fSumSH += double((min.m_ocOrder.m_pxVolOfBuy[0]-min.m_ocOrder.m_pxVolOfSell[0])*1000000)/double(xLTG.GetValue()/100);
		}

		for (WORD w=0; w<wMinuteNum; w++)
		{
			CMinute& min = aMinute[w+wPosStart];

			// m_wTime,  hhmm
			WORD wMinutes = GetMinutesDiff(wLastTime, min.m_wTime);
			stream.EncodeData(wMinutes, BC_MIN_TIME, BCNUM_MIN_TIME);
			wLastTime = min.m_wTime;

			//资金净流
			fSumAmtDiff += double(min.m_ocOrder.m_pxAmtOfBuy[3] + min.m_ocOrder.m_pxAmtOfBuy[2]
				- min.m_ocOrder.m_pxAmtOfSell[3] - min.m_ocOrder.m_pxAmtOfSell[2]) / 10000000.0;

			//超级资金
			fSumCJ += double((min.m_ocOrder.m_pxVolOfBuy[3]-min.m_ocOrder.m_pxVolOfSell[3])*1000000)/double(xLTG.GetValue()/100);

			//大户资金
			fSumDH += double((min.m_ocOrder.m_pxVolOfBuy[2]-min.m_ocOrder.m_pxVolOfSell[2])*1000000)/double(xLTG.GetValue()/100);

			//中户资金
			fSumZH += double((min.m_ocOrder.m_pxVolOfBuy[1]-min.m_ocOrder.m_pxVolOfSell[1])*1000000)/double(xLTG.GetValue()/100);

			//散户资金
			fSumSH += double((min.m_ocOrder.m_pxVolOfBuy[0]-min.m_ocOrder.m_pxVolOfSell[0])*1000000)/double(xLTG.GetValue()/100);

			if (w==0)
			{
				xNow = INT64(fSumAmtDiff > 0 ? (fSumAmtDiff+0.5) : (fSumAmtDiff-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

				xNow = INT64(fSumCJ > 0 ? (fSumCJ+0.5) : (fSumCJ-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

				xNow = INT64(fSumDH > 0 ? (fSumDH+0.5) : (fSumDH-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

				xNow = INT64(fSumZH > 0 ? (fSumZH+0.5) : (fSumZH-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

				xNow = INT64(fSumSH > 0 ? (fSumSH+0.5) : (fSumSH-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);
			}
			else
			{
				xNow = INT64(fSumAmtDiff > 0 ? (fSumAmtDiff+0.5) : (fSumAmtDiff-0.5));
				xLast = INT64(fLastSumAmtDiff > 0 ? (fLastSumAmtDiff+0.5) : (fLastSumAmtDiff-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT_DIFF, BCNUM_MINDAY_AMNT_DIFF, &xLast);

				xNow = INT64(fSumCJ > 0 ? (fSumCJ+0.5) : (fSumCJ-0.5));
				xLast = INT64(fLastSumCJ > 0 ? (fLastSumCJ+0.5) : (fLastSumCJ-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT_DIFF, BCNUM_MINDAY_AMNT_DIFF, &xLast);

				xNow = INT64(fSumDH > 0 ? (fSumDH+0.5) : (fSumDH-0.5));
				xLast = INT64(fLastSumDH > 0 ? (fLastSumDH+0.5) : (fLastSumDH-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT_DIFF, BCNUM_MINDAY_AMNT_DIFF, &xLast);

				xNow = INT64(fSumZH > 0 ? (fSumZH+0.5) : (fSumZH-0.5));
				xLast = INT64(fLastSumZH > 0 ? (fLastSumZH+0.5) : (fLastSumZH-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT_DIFF, BCNUM_MINDAY_AMNT_DIFF, &xLast);

				xNow = INT64(fSumSH > 0 ? (fSumSH+0.5) : (fSumSH-0.5));
				xLast = INT64(fLastSumSH > 0 ? (fLastSumSH+0.5) : (fLastSumSH-0.5));
				stream.EncodeXInt32(xNow, BC_MINDAY_AMNT_DIFF, BCNUM_MINDAY_AMNT_DIFF, &xLast);
			}

			fLastSumAmtDiff = fSumAmtDiff;
			fLastSumCJ = fSumCJ;
			fLastSumDH = fSumDH;
			fLastSumZH = fSumZH;
			fLastSumSH = fSumSH;
		}

		wPosStart += wMinuteNum;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return nRet;
}
//////////////////////////////////////////////////////////////////////

int CHisCompress::ExpandBargain(CBitStream& stream, CGoods* pGoods)
{
	if (pGoods==NULL)
		return -1;

	BYTE cCheckSum = 0;

	try
	{
		DWORD dwNum = 0;
		stream.DecodeData(dwNum, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);

		if (dwNum>0)
		{
			DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();

			CBargain bar;		
			DWORD dwCurPrice;
			DWORD dwLastPrice;

			for (DWORD dw=0; dw<dwNum; dw++)
			{
				if (dw==0)
				{
					bar.m_dwTime = stream.ReadDWORD(18);

					stream.DecodeData(dwCurPrice, BC_TRADE_PRICE, BCNUM_TRADE_PRICE);
					stream.DecodeData(bar.m_dwTradeNum, BC_TRADE_TRADENUM, BCNUM_TRADE_TRADENUM);
				}
				else
				{
					DWORD dwSecondsDiff = 0;
					stream.DecodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);
					bar.m_dwTime = AddSeconds(bar.m_dwTime, dwSecondsDiff);

					stream.DecodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);

					DWORD dwLastTradeNum = bar.m_dwTradeNum;
					stream.DecodeData(bar.m_dwTradeNum, BC_BARGAIN_TRADENUM_DIFF, BCNUM_BARGAIN_TRADENUM_DIFF, &dwLastTradeNum);
				}

				stream.DecodeXInt32(bar.m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
				bar.m_cBS = (char)stream.ReadDWORD(2) - 1;

				bar.m_dwPrice = dwCurPrice * PRICE_DIVIDE;

				pGoods->AddBargain(bar);
				cCheckSum += bar.GetCheckSum(PRICE_DIVIDE);

				dwLastPrice = dwCurPrice;
			}

			BYTE cCheck = (BYTE)stream.ReadDWORD(8);
			if (cCheck!=cCheckSum)
				return ERR_COMPRESS_CHECK;
		}
	}
	catch (int)
	{
		return -2;
	}

	return 0;
}

int CHisCompress::CompressBargain(CBitStream& stream, CDataBargain* pData, DWORD dwRecvNum, WORD wWant)
{
	if (pData==NULL)
		return -1;

	try
	{
		stream.EncodeData(pData->m_dwSize, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);

		if (dwRecvNum>pData->m_dwSize)
		{
			stream.WriteBOOL(TRUE);
			dwRecvNum = 0;
		}
		else
			stream.WriteBOOL(FALSE);
		int nFirst = dwRecvNum;
		int nNum = pData->m_dwSize-nFirst;
		if (nNum>(int)wWant)
		{
			nNum = (int)wWant;
			nFirst = pData->m_dwSize-nNum;
		}

		DWORD PRICE_DIVIDE = pData->m_pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);

		CBargain* pCurBar = pData->m_pBuffer+nFirst;

		DWORD dwLastTradeNum = 0;
		if (nFirst>0)
			dwLastTradeNum = pData->m_pBuffer[nFirst-1].m_dwTradeNum;

		DWORD dwLastTime = 0;
		DWORD dwLastPrice = 0;

		stream.EncodeData(nNum, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);

		for (int i=0; i<nNum; i++)
		{
			DWORD dwCurPrice = pCurBar->m_dwPrice/PRICE_DIVIDE;

			if (i==0)
			{
				stream.WriteDWORD(pCurBar->m_dwTime, 18);
				stream.EncodeData(dwCurPrice, BC_TRADE_PRICE, BCNUM_TRADE_PRICE);
			}
			else
			{
				DWORD dwSecondsDiff = GetSecondsDiff(dwLastTime, pCurBar->m_dwTime);
				stream.EncodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);

				stream.EncodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);
			}

			DWORD dwTradeNum = pCurBar->m_dwTradeNum-dwLastTradeNum;
			stream.EncodeData(dwTradeNum, BC_BARGAIN_TRADENUM_DIFF, BCNUM_BARGAIN_TRADENUM_DIFF);

			stream.EncodeXInt32(pCurBar->m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
			stream.WriteDWORD(pCurBar->m_cBS+1, 2);

			dwLastTime = pCurBar->m_dwTime;
			dwLastPrice = dwCurPrice;
			dwLastTradeNum = pCurBar->m_dwTradeNum;

			pCurBar++;
		}
	}
	catch(int)
	{
		return -2;
	}

	return 0;
}

int CHisCompress::CompressMinute_simple( CBitStream& stream, BOOL bIndex, CArrayMinute& aMinute, WORD& wPosStart, DWORD PRICE_DIVIDE )
{
	WORD wMinuteNum = (WORD)aMinute.GetSize();
	
	if (wPosStart>wMinuteNum)
		wPosStart = 0;

	int nRet = 0;

	wMinuteNum -= wPosStart;

	try
	{	
		stream.EncodeData(wMinuteNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);

		stream.WriteBOOL(bIndex);

		WORD wLastTime = MINUTE_BEGIN_TIME;
		DWORD dwLastClose = 0;
		DWORD dwLastAve = 0;
		int lLastStrong = 0;

		for (WORD w=0; w<wMinuteNum; w++)
		{
			CMinute& min = aMinute[w+wPosStart];

			// m_wTime,  hhmm
			WORD wMinutes = GetMinutesDiff(wLastTime, min.m_wTime);
			stream.EncodeData(wMinutes, BC_MIN_TIME, BCNUM_MIN_TIME);
			wLastTime = min.m_wTime;

			// m_dwOpen, m_dwHigh, m_dwLow, m_dwClose, m_dwAve
			DWORD dwClose = min.m_dwClose/PRICE_DIVIDE;
			DWORD dwAve = min.m_dwAve/PRICE_DIVIDE;

			if (w==0)
			{
				stream.EncodeData(dwClose, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
				//stream.EncodeData(dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwClose);
			}
			else
			{
				stream.EncodeData(dwClose, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastClose);
				//stream.EncodeData(dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastAve);
			}

			dwLastClose = dwClose;
			dwLastAve = dwAve;
		}

		wPosStart += wMinuteNum;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return nRet;
}

int CHisCompress::ExpandMinute(CBitStream& stream, CGoods* pGoods, CMinuteSortArray& aMinute, short sVersion)
{
	try
	{
		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
//		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		BOOL bClear = stream.ReadDWORD(1);
		if (bClear)
		{
			DWORD dwDate = stream.ReadDWORD(32);
			pGoods->ClearMinute(TRUE, dwDate);
		}

		DWORD dwMinuteNum;
		stream.DecodeData(dwMinuteNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);
		WORD wMinuteNum = (WORD)dwMinuteNum;
		if (wMinuteNum>1)
			int iii = 0;

		BOOL bIsGZQH = pGoods->IsQH();

		BOOL bIsIndex, bIndex, bIsLevel2;
		if (bIsGZQH==FALSE)
		{
			bIsIndex = stream.ReadDWORD(1);
			bIndex = stream.ReadDWORD(1);
			bIsLevel2 = stream.ReadDWORD(1);
		}
		else
		{
			bIsIndex = FALSE;
			bIndex = FALSE;
			bIsLevel2 = FALSE;
		}

		BYTE cCheckSum = 0;

		DWORD dwCountMinute = pGoods->m_dwCountMinute;
		if (wMinuteNum>0)
		{
			aMinute.RemoveAt(0, aMinute.GetSize());

			dwCountMinute++;

			WORD wLastTime = MINUTE_BEGIN_TIME;
			DWORD dwLastClose = 0;
			XInt32 xOI, xLastOI;

			DWORD dwLastSellPrice = 0;
			DWORD dwLastBuyPrice = 0;

			long lLastStrong = 0;

			CMinute min;
			for (WORD w=0; w<wMinuteNum; w++)
			{
				min.Clear();

				// m_wTime,  hhmm
				DWORD dwTemp = 0;
				stream.DecodeData(dwTemp, BC_MIN_TIME, BCNUM_MIN_TIME);
				WORD wMinutes = (WORD)dwTemp;
				min.m_wTime = AddMinutes(wLastTime, wMinutes);

				wLastTime = min.m_wTime;

				// m_dwOpen, m_dwHigh, m_dwLow, m_dwClose, m_dwAve
				DWORD dwOpen = 0;
				DWORD dwHigh = 0;
				DWORD dwLow = 0;
				DWORD dwClose = 0;

				if (w==0)
					stream.DecodeData(dwOpen, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
				else
					stream.DecodeData(dwOpen, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastClose);

				stream.DecodeData(dwHigh, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwOpen);
				stream.DecodeData(dwLow, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwOpen, TRUE);
				stream.DecodeData(dwClose, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwLow);
				
				dwLastClose = dwClose;

				min.m_dwOpen = dwOpen * PRICE_DIVIDE;
				min.m_dwHigh = dwHigh * PRICE_DIVIDE;
				min.m_dwLow = dwLow * PRICE_DIVIDE;
				min.m_dwClose = dwClose * PRICE_DIVIDE;

				stream.DecodeData(min.m_dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwClose);

				// m_xVolume, m_xAmount, m_dwTradeNum
				stream.DecodeXInt32(min.m_xVolume, BC_MIN_VOL, BCNUM_MIN_VOL);

				if (bIsGZQH || bIsIndex)
					stream.DecodeXInt32(min.m_xAmount, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);
				else
				{
					XInt32 xBase = (INT64)min.m_xVolume * min.m_dwAve * PRICE_DIVIDE;
					stream.DecodeXInt32(min.m_xAmount, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT, &xBase);
				}

				if (bIsGZQH)
				{
					if (w==0)
						stream.DecodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI);
					else
						stream.DecodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI, &xLastOI);
					min.m_dwTradeNum = xOI.GetRawData();
					xLastOI = xOI;
				}
				else
				{
					stream.DecodeData(min.m_dwTradeNum, BC_MIN_TRADENUM, BCNUM_MIN_TRADENUM);

					// m_dwSellPrice, m_dwBuyPrice, m_xSellVol, m_xBuyVol
					if (w==0)
					{
						stream.DecodeData(min.m_dwSellPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwHigh);
						stream.DecodeData(min.m_dwBuyPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwLow);
					}
					else
					{
						stream.DecodeData(min.m_dwSellPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastSellPrice);
						stream.DecodeData(min.m_dwBuyPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastBuyPrice);
					}

					dwLastSellPrice = min.m_dwSellPrice;
					dwLastBuyPrice = min.m_dwBuyPrice;

					stream.DecodeXInt32(min.m_xSellVol, BC_DAY_VOL, BCNUM_DAY_VOL);
					stream.DecodeXInt32(min.m_xBuyVol, BC_DAY_VOL, BCNUM_DAY_VOL);

					if (sVersion>=1)
					{
						stream.DecodeXInt32(min.m_xSellVol5, BC_DAY_VOL, BCNUM_DAY_VOL);
						stream.DecodeXInt32(min.m_xBuyVol5, BC_DAY_VOL, BCNUM_DAY_VOL);
					}

					if(bIndex)
					{
						// m_nStrong
						if (w==0)
							stream.DecodeData(dwTemp, BC_MINDAY_STRONG, BCNUM_MINDAY_STRONG);
						else
							stream.DecodeData(dwTemp, BC_MINDAY_STRONG_DIFF, BCNUM_MINDAY_STRONG_DIFF, (PDWORD)&lLastStrong);

						min.m_nStrong = (long)dwTemp;

						lLastStrong = min.m_nStrong;

						// m_wRise, m_wFall
						stream.DecodeData(dwTemp, BC_MINDAY_RISEFALL, BCNUM_MINDAY_RISEFALL);
						min.m_wRise = (WORD)dwTemp;

						stream.DecodeData(dwTemp, BC_MINDAY_RISEFALL, BCNUM_MINDAY_RISEFALL);
						min.m_wFall = (WORD)dwTemp;
					}

					if (bIsLevel2)
					{
						ExpandOrderCounts(stream, min.m_ocOrder, min, TRUE);
						ExpandOrderCounts(stream, min.m_ocTrade, min, FALSE);

						for (int j = 0; j < 2; j++)
						{
							stream.DecodeXInt32(min.m_pxNewOrder[j], BC_MIN_VOL, BCNUM_MIN_VOL);
							stream.DecodeXInt32(min.m_pxDelOrder[j], BC_MIN_VOL, BCNUM_MIN_VOL);
						}
					}
				}
				
				min.m_dwCount = dwCountMinute;

				cCheckSum += min.GetCheckSum(bIsGZQH, bIndex, bIsLevel2, PRICE_DIVIDE);
/*
				BYTE cCheck = min.GetCheckSum(bIndex, bIsLevel2);
				BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);
				if (cStreamChkSum!=cCheck)
					int iii = 0;
*/
				aMinute.Add(min);
			}

			pGoods->AddMinutes(aMinute);
			pGoods->m_dwCountMinute = dwCountMinute;
		}
///20120327 sjp 解决新股停牌没横线问题
		else
			pGoods->AddEmptyMinute(); 
///20120327 sjp 解决新股停牌没横线问题

		BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);

		if (cStreamChkSum!=cCheckSum)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return 0;
}

int CHisCompress::ExpandMinute6(CBitStream& stream, CGoods* pGoods, CMinuteSortArray& aMinute, short sVersion)
{
	try
	{
		char cDiv = (char)stream.ReadDWORD(3);
		DWORD dwDiv = g_CheckDiv(cDiv);
		if (dwDiv==0)
			return ERR_COMPRESS_DIV;

		BOOL bClear = stream.ReadDWORD(1);
		if (bClear)
		{
			DWORD dwDate = stream.ReadDWORD(32);
			pGoods->ClearMinute(TRUE, dwDate);
		}

		DWORD dwTimeDay = pGoods->m_dwDateMinute%1000000;

		DWORD dwMinuteNum;
		stream.DecodeData(dwMinuteNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);
		WORD wMinuteNum = (WORD)dwMinuteNum;

		BOOL bIsSimple = stream.ReadDWORD(1);
		BOOL bIsQH = stream.ReadDWORD(1);

		BOOL bIndex, bIsLevel2;
		if (bIsSimple==FALSE)
		{
			bIndex = stream.ReadDWORD(1);
			bIsLevel2 = stream.ReadDWORD(1);
		}
		else
		{
			bIndex = FALSE;
			bIsLevel2 = FALSE;
		}

		BYTE cCheckSum = 0;

		DWORD dwCountMinute = pGoods->m_dwCountMinute;
		if (wMinuteNum>0)
		{
			aMinute.RemoveAt(0, aMinute.GetSize());

			dwCountMinute++;

			CTime tmBase(MINUTE_BEGIN_YEAR, 1, 1, 0, 0, 0);
			DWORD dwLastDay = 0;
			WORD  wLastMinute = MINUTE_BEGIN_TIME;
			DWORD dwLastClose = 0, dwLastAve = 0;
			XInt32 xOI, xLastOI;

			DWORD dwLastSellPrice = 0;
			DWORD dwLastBuyPrice = 0;

			long lLastStrong = 0;

			BOOL bRecvDay = FALSE;

			DWORD dbgOpen, dbgHigh, dbgLow, dbgClose;

			CMinute min;
			for (WORD w=0; w<wMinuteNum; w++)
			{
				min.Clear();

				if (w==0)
					bRecvDay = stream.ReadDWORD(1);

				if (bRecvDay)
				{
					DWORD dwCurDay = 0;
					stream.DecodeData(dwCurDay, BC_DAY_TIME_MIND, BCNUM_DAY_TIME_MIND, &dwLastDay);

					CTime tm = tmBase + CMyTimeSpan(dwCurDay, 0, 0, 0);
					dwTimeDay = (tm.GetYear()-2000)*10000 + tm.GetMonth()*100 + tm.GetDay();	//YYMMDD

					if (dwCurDay!=dwLastDay)
					{
						dwLastDay = dwCurDay;
						wLastMinute = MINUTE_BEGIN_TIME;
					}
				}

				DWORD dwTemp = 0;
				stream.DecodeData(dwTemp, BC_MIN_TIME, BCNUM_MIN_TIME);
				WORD wMinutes = (WORD)dwTemp;

				WORD wTimeMin = AddMinutes(wLastMinute, wMinutes);

				min.m_wTime = dwTimeDay*10000 + wTimeMin;
				wLastMinute = wTimeMin;

				// m_dwOpen, m_dwHigh, m_dwLow, m_dwClose, m_dwAve
				DWORD dwOpen = 0, dwHigh = 0, dwLow = 0, dwClose = 0;

				if (w==0)
					stream.DecodeData(dwOpen, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
				else
					stream.DecodeData(dwOpen, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastClose);

				stream.DecodeData(dwHigh, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwOpen);
				stream.DecodeData(dwLow, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwOpen, TRUE);
				stream.DecodeData(dwClose, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwLow);
				
				min.m_dwOpen = dwOpen * dwDiv;
				min.m_dwHigh = dwHigh * dwDiv;
				min.m_dwLow = dwLow * dwDiv;
				min.m_dwClose = dwClose * dwDiv;

				dbgOpen = min.m_dwOpen;
				dbgHigh = min.m_dwHigh;
				dbgLow = min.m_dwLow;
				dbgClose = min.m_dwClose;
				
					

				dwLastClose = dwClose;

				if (w==0)
					stream.DecodeData(min.m_dwAve, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &(min.m_dwClose));
				else
					stream.DecodeData(min.m_dwAve, BC_MINDAY_PRICE_DIFF, BCNUM_MINDAY_PRICE_DIFF, &dwLastAve);

				dwLastAve = min.m_dwAve;

				// m_xVolume, m_xAmount, m_dwTradeNum
				stream.DecodeXInt32(min.m_xVolume, BC_MIN_VOL, BCNUM_MIN_VOL);
				stream.DecodeXInt32(min.m_xAmount, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);

				if (bIsQH)
				{
					if (w==0)
						stream.DecodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI);
					else
						stream.DecodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI, &xLastOI);
					min.m_dwTradeNum = xOI.GetRawData();
					xLastOI = xOI;
				}
				else
					stream.DecodeData(min.m_dwTradeNum, BC_MIN_TRADENUM, BCNUM_MIN_TRADENUM);

				if (bIsSimple==FALSE)
				{
					// m_dwSellPrice, m_dwBuyPrice, m_xSellVol, m_xBuyVol
					if (w==0)
					{
						stream.DecodeData(min.m_dwSellPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwHigh);
						stream.DecodeData(min.m_dwBuyPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &min.m_dwLow);
					}
					else
					{
						stream.DecodeData(min.m_dwSellPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastSellPrice);
						stream.DecodeData(min.m_dwBuyPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastBuyPrice);
					}

					dwLastSellPrice = min.m_dwSellPrice;
					dwLastBuyPrice = min.m_dwBuyPrice;

					stream.DecodeXInt32(min.m_xSellVol, BC_DAY_VOL, BCNUM_DAY_VOL);
					stream.DecodeXInt32(min.m_xBuyVol, BC_DAY_VOL, BCNUM_DAY_VOL);

					stream.DecodeXInt32(min.m_xSellVol5, BC_DAY_VOL, BCNUM_DAY_VOL);
					stream.DecodeXInt32(min.m_xBuyVol5, BC_DAY_VOL, BCNUM_DAY_VOL);

					if(bIndex)
					{
						// m_nStrong
						if (w==0)
							stream.DecodeData(dwTemp, BC_MINDAY_STRONG, BCNUM_MINDAY_STRONG);
						else
							stream.DecodeData(dwTemp, BC_MINDAY_STRONG_DIFF, BCNUM_MINDAY_STRONG_DIFF, (PDWORD)&lLastStrong);

						min.m_nStrong = (long)dwTemp;

						lLastStrong = min.m_nStrong;

						// m_wRise, m_wFall
						stream.DecodeData(dwTemp, BC_MINDAY_RISEFALL, BCNUM_MINDAY_RISEFALL);
						min.m_wRise = (WORD)dwTemp;

						stream.DecodeData(dwTemp, BC_MINDAY_RISEFALL, BCNUM_MINDAY_RISEFALL);
						min.m_wFall = (WORD)dwTemp;
					}

					if (bIsLevel2)
					{
						ExpandOrderCounts(stream, min.m_ocOrder, min, TRUE);
						ExpandOrderCounts(stream, min.m_ocTrade, min, FALSE);

						for (int j = 0; j < 2; j++)
						{
							stream.DecodeXInt32(min.m_pxNewOrder[j], BC_MIN_VOL, BCNUM_MIN_VOL);
							stream.DecodeXInt32(min.m_pxDelOrder[j], BC_MIN_VOL, BCNUM_MIN_VOL);
						}
					}
				}
				
				min.m_dwCount = dwCountMinute;

				cCheckSum += min.GetCheckSum(bIsSimple, bIndex, bIsLevel2, dwDiv);
/*
				BYTE cCheck = min.GetCheckSum(bIndex, bIsLevel2);
				BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);
				if (cStreamChkSum!=cCheck)
					int iii = 0;
*/

				aMinute.Add(min);
                //theApp.m_pRedisImporter->WriteMinute(pGoods, min);
			}

			pGoods->AddMinutes(aMinute);
			pGoods->m_dwCountMinute = dwCountMinute;
		}
///20120327 sjp 解决新股停牌没横线问题
		else
			pGoods->AddEmptyMinute(); 
///20120327 sjp 解决新股停牌没横线问题

		BYTE cStreamChkSum = (BYTE)stream.ReadDWORD(8);

		if (cStreamChkSum!=cCheckSum)
			return ERR_COMPRESS_CHECK;
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}

	
	return 0;
}

int CHisCompress::CompressHisMin6(CBitStream& stream, const XInt32& xLTG, CDataHisMin* pData, BOOL bLevel2)
{
	LogErr("CompressHisMin6");
	WORD wNum = (WORD)pData->m_aHisMin.GetSize();
	
	try
	{	
		char cDiv = pData->m_pGoods->m_cPriceDiv;
		DWORD dwDiv = pData->m_pGoods->m_dwPriceDiv;

		stream.WriteDWORD(cDiv, 3);

		BOOL bIsQH = pData->m_pGoods->IsQH();
		stream.WriteBOOL(bIsQH);

		DWORD dwClose = g_DivPrice(pData->m_dwClose, dwDiv);
		DWORD dwHigh  = g_DivPrice(pData->m_dwHigh, dwDiv);
		DWORD dwLow   = g_DivPrice(pData->m_dwLow, dwDiv);

		stream.EncodeData(dwClose, BC_MINDAY_PRICE, BCNUM_MINDAY_PRICE);
		stream.EncodeData(dwHigh, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwClose);
		stream.EncodeData(dwLow, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwClose);

		stream.EncodeData(wNum, BC_MIN_NUMBER, BCNUM_MIN_NUMBER);

		CTime tmBase(DAY_BEGIN_YEAR, 1, 1, 0, 0, 0);
		DWORD dwLastDay = 0;
		WORD  wLastMinute = MINUTE_BEGIN_TIME;
		DWORD dwLastPrice = dwClose;
		DWORD dwLastAve = pData->m_dwClose;
		XInt32 xOI, xLastOI, xZJJL;
		DWORD dwLastTimeDay = 0;
		BOOL  bHasLTG = xLTG<=0 ? FALSE:TRUE;

		
		BYTE cCheckSum = 0;
		for (WORD w=0; w<wNum; w++)
		{
			CHisMin hm = pData->m_aHisMin[w];	//不用引用，因为要修价格...

			DWORD dwTimeDay = hm.m_dwTime/10000;		// YYYMMDD
			WORD wTimeMin = WORD(hm.m_dwTime%10000);	// hhmm

			BOOL bSendDay = FALSE;
			if( dwLastTimeDay != dwTimeDay ){
				bSendDay = TRUE;
			}
			stream.WriteBOOL(bSendDay);
			if (bSendDay)
			{
				CTime tm(2000+dwTimeDay/10000, dwTimeDay%10000/100, dwTimeDay%100, 0, 0, 0);
				DWORD dwCurDay = (DWORD)((tm-tmBase).GetDays());

				stream.EncodeData(dwCurDay, BC_DAY_TIME_MIND, BCNUM_DAY_TIME_MIND, &dwLastDay);

				if (dwCurDay!=dwLastDay)
				{
					dwLastDay = dwCurDay;
					wLastMinute = MINUTE_BEGIN_TIME;
				}
				dwLastTimeDay = dwTimeDay;
			}

			// m_wTime,  hhmm
			WORD wMinutes = GetMinutesDiff(wLastMinute, wTimeMin);
			stream.EncodeData(wMinutes, BC_MIN_TIME, BCNUM_MIN_TIME);
			wLastMinute = wTimeMin;

			hm.m_dwPrice = g_DivPrice(hm.m_dwPrice, dwDiv);
			stream.EncodeData(hm.m_dwPrice, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastPrice);
			dwLastPrice = hm.m_dwPrice;

			stream.EncodeData(hm.m_dwAve, BC_MINDAY_PRICE_DIFFS, BCNUM_MINDAY_PRICE_DIFFS, &dwLastAve);
			dwLastAve = hm.m_dwAve;

			stream.EncodeXInt32(hm.m_xVolume, BC_MIN_VOL, BCNUM_MIN_VOL);

			if (bIsQH==FALSE && bLevel2==FALSE)
				hm.m_xZJJL = 0;
			
			if( !bHasLTG ){
				hm.m_xZJJL = 0;
			}

			if (hm.m_xZJJL.m_nBase==0)
				stream.WriteBOOL(FALSE);
			else
			{
				stream.WriteBOOL(TRUE);
				if (bIsQH)
				{
					xOI = hm.m_xZJJL;
					if (w==0)
						stream.EncodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI);
					else
						stream.EncodeXInt32(xOI, BC_MIN_OI, BCNUM_MIN_OI, &xLastOI);
					xLastOI = xOI;
				}
				else
				{
					xZJJL = hm.m_xZJJL;

					if (xZJJL.m_nBase<0)
					{
						stream.WriteBOOL(FALSE);
						xZJJL.m_nBase = -xZJJL.m_nBase;
					}
					else
						stream.WriteBOOL(TRUE);		
					stream.EncodeXInt32(xZJJL, BC_MINDAY_AMNT, BCNUM_MINDAY_AMNT);
				}
			}
			LogErr("m_HisMin[%d] dwTime:%d price:%d, ave:%d, vol-base:%d, vol-mul:%d, zjjl-base:%d, zjjl-mul:%d, zjjl:%lld", 
				w, hm.m_dwTime, hm.m_dwPrice, hm.m_dwAve, hm.m_xVolume.m_nBase, hm.m_xVolume.m_cMul, 
				hm.m_xZJJL.m_nBase, hm.m_xZJJL.m_cMul,hm.m_xZJJL.GetValue());

			cCheckSum += hm.GetCheckSum6();
		}

		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return ERR_COMPRESS_BITBUF;
	}
	
	return 0;
}

