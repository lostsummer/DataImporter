#ifndef _CALCGROUP_H_
#define _CALCGROUP_H_


#include "XInt32.h"
#include "Buffer.h"
#include "Mutex.h"

#define G_SIZE		29
#define G_SIZE_OLD	28
#define G_X_SIZE	6

#define	G_SZ		0		//市值			(亿元*100)
#define	G_LTSZ		1		//流通市值		(亿元*100)
#define	G_VOLUME	2		//成交量		(亿股*100)
#define	G_AMOUNT	3		//成交金额		(亿元*100)
#define	G_GB		4		//平均股本
#define	G_LTG		5		//平均流通股
//6

#define	G_SYL		6		//平均市盈率
#define	G_SY		7		//平均每股收益
#define	G_GJJ		8		//平均每股公积金
#define	G_JZC		9		//平均每股净资产

#define	G_ZDF		10		//涨跌幅
#define	G_ZDF5		11		//5日涨跌幅
#define	G_HSL		12		//换手率
#define	G_HSL5		13		//5日换手率
#define	G_STRONG	14		//对大盘的贡献(点数)

#define	G_GPJS		15		//股票数量
#define	G_SZJS		16		//上涨家数
#define	G_XDJS		17		//下跌家数
#define	G_QSG		18		//当日强势股
#define	G_QSG5		19		//5日强势股
#define	G_RSG		20		//当日弱势股
#define	G_RSG5		21		//5日弱势股

#define	G_BIGAMT	22		//机构净买
#define	G_BIGAMT5	23		//5日净买

#define	G_AVGPRICE	24		//平均股价 //新增项目 暂后处理
#define	G_AVGSJL	25		//平均市净率 总市值求和/股东权益求和
#define G_SHSTRONG	26      //对沪指的贡献程度
#define G_SZSTRONG  27      //对深指的贡献程度
//28
#define	G_CPX		28		//操盘线日线	//20110620

//扩展数据字段
#define G_SIZE_EXT   4
#define G_RSG_ZSJ	 1001	//强势股昨收
#define G_RSG_DQJ	 1002	//强势股现价
#define G_RSG_ZSF	 1003	//弱势股昨收
#define G_RSG_DQF	 1004	//弱势股现价

//指数统计字段
class CCalcGroupStat
{
public:
	CCalcGroupStat()
	{
		m_dwGroupID = 0;
		
		ZeroMemory(m_pxValue, G_X_SIZE*4);

		m_dwCount = 0;
	}

public:
	Mutex m_Mutex;

	DWORD m_dwGroupID;

	XInt32 m_pxValue[G_X_SIZE];
/*
	DWORD m_dwAvgPrice;	//算术平均股价
    DWORD m_dwWgtPrice;	//加权平均股价
	DWORD m_dwWgtSYL;	//加权平均市盈率
	DWORD m_dwWgtSJL;	//加权平均市净率
	XInt32 m_xZSZ;		//总市值 以10万为单位
	XInt32 m_xLTSZ;		//流通市值 以10万为单位
*/
	DWORD m_dwCount;
};

//////////////////////////////////////////////////////////////////////

class CCalcGroup  
{
public:
	CCalcGroup();
	virtual ~CCalcGroup();

	void Initial();

	char Compare(CCalcGroup*, short, char);

	INT64 GetValue(short sID);
	INT64 GetValueForCompare(short sID);

	void Read(CBuffer& buf, WORD wVersion);
	void Write(CBuffer& buf, WORD wVersion);

	BOOL IsBad();

	void SaveValue2Goods();

public:
	Mutex m_Mutex;

	DWORD m_dwGroupID;
	CString m_strGroup;

	XInt32 m_pxValue[G_SIZE];

	DWORD m_dwCount;
};

#endif
