/** *************************************************************************
* Application Log Liberary
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file ApplicationLogger.cpp
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 18 19:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#include <algorithm>
#include <stdarg.h>

#include "AppLogger.h"
#include "SysGlobal.h"
#include "Util.h"
#include "Mutex.h"

#ifndef WIN32
	#define GetCurrentThreadId() pthread_self()
	#define GetCurrentProcessId() getpid()
#endif

#ifdef _MSC_VER
#pragma warning(disable:4267)
#endif
/**
* @brief log type string
*/
const char* GetTypeStr(LogType LogType)
{
	switch (LogType)
	{
	case LOG_APP: return "APP";
	case LOG_ERROR: return "ERR";
	case LOG_INFO: return "INF";
	case LOG_TRACE: return "TRC";
	default: return "UNK";
	}
}

/**
* @brief readjust buffer size of log data
* @return return a LogData that contain old buffer and this contain new buffer
* @return return 0 if memory allocate failed and this remain unchanged
*/
LogData* LogData::Resize(size_t NewSize)
{
	LogData* ret = 0;
	PBYTE NewBuffer = 0;

	try
	{
		NewBuffer = new(std::nothrow) BYTE[NewSize];
		memset(NewBuffer, 0, NewSize);
		ret = new LogData(m_Buffer, m_BuferSize);

		m_Buffer = NewBuffer;
		m_BuferSize = NewSize;
	} 
	catch (...) 
	{
		if (NewBuffer)
		{
			delete[] NewBuffer;
		}
	}

	return ret;
}

/**
* @brief constructor
*/
LogData::LogData(PBYTE Buffer, size_t BufferSize)
	: m_Buffer(Buffer), m_BuferSize(BufferSize), m_Length(0), m_MsgSize(0), m_MsgBuf(0)
{
}

/**
* @brief destructor
*/
LogData::~LogData()
{
	if (m_Buffer)
	{
		delete[] m_Buffer;
		m_Buffer = 0;
	}
}

//////////////////////////////////////////////////////////////////////////
/**
* @brief Create Next File when file size same as the setting. or other need to when condition done. 
*/
void LogToFile::CreateNextFile()
{
	m_FileNameTime = time(0);
    struct tm tm_s;
	struct tm* pt = localtime_r(&m_FileNameTime, &tm_s);

	char FileName[512+1] = {0};

	snprintf(FileName, 512, "%s/%02d%02d%02d%02d%02d%02d-%x-%s",
 		m_Info.Dir.c_str(),
 		(pt->tm_year+1900) & 0xf,
 		pt->tm_mon+1,
 		pt->tm_mday,
 		pt->tm_hour,
 		pt->tm_min,
 		pt->tm_sec,
 		GetCurrentProcessId(),		
 		m_Info.FileNameBase.c_str());

	std::string fileName(FileName);

	LogManager::GetMe().GetLogConfig()->OnNewLogFile(m_Info.FileNameBase, fileName, m_File.IsValid());

	m_File.Close();
	m_File.SetFileName(fileName.c_str());
	m_File.OpenAlways();
}

/**
* @brief initialize : create log path / log files
*/
bool LogToFile::Initialize()
{
	if (!SimpleFile::IsDirectoryExists(m_Info.Dir.c_str()))
	{
		std::string cmd("mkdir -p ");
		cmd.append(m_Info.Dir.c_str());
		if (system(cmd.c_str()) != 0)
			return false;
	}

	CreateNextFile();

	return m_File.IsValid();
}

/**
* @brief terminate : close log file
*/
void LogToFile::Terminate()
{
	m_File.Close();
}

/**
* @brief log to file
*/
void LogToFile::Log(LogData* logData)
{
	m_FileChangeLocker.Lock();

	if (m_Info.SeperateByDate)
	{
		time_t tmp = time(0);
        struct tm tm_s_now, tm_s_file;
		struct tm* ptNow = localtime_r(&tmp, &tm_s_now);
		struct tm* ptFile = localtime_r(&m_FileNameTime, &tm_s_file);
		
		if (ptNow->tm_year != ptFile->tm_year 
			|| ptNow->tm_mon != ptFile->tm_mon 
			|| ptNow->tm_mday != ptFile->tm_mday)
			CreateNextFile();
	}

	if (m_Info.MaxFileSize)
	{
		if (m_File.GetFileSize() >  m_Info.MaxFileSize)
			CreateNextFile();
	}

	m_FileChangeLocker.UnLock();

	if (m_File.IsValid())
		m_File.Write(logData->GetBuffer(), logData->GetLength());
}

//////////////////////////////////////////////////////////////////////////
void LogConfig::EnableLog(LogType, DWORD LogTargetBitMask, int TargetID)
{
}

void LogConfig::DisableLog(LogType, DWORD LogTargetBitMask)
{
}

bool LogConfig::Initialize()
{
	ZeroMemory(m_LogEnableFlag, sizeof(m_LogEnableFlag));
	size_t FileCount = GetLogFileCount();
	if (FileCount > 32)
		return false;

	for (int i = 0; i < LOG_TYPE_MAX; i++)
	{
		for (size_t j = 1; j <= FileCount; j++)
		{
			if (IsLogEnabled((LogType)i, LOG_TO_FILE, j))
			{
				m_LogEnableFlag[i].Target[LOG_TO_FILE] |= (1 << (j -1));
			}
		}

		m_LogEnableFlag[i].Target[LOG_TO_SERVER] = IsLogEnabled((LogType)i, LOG_TO_SERVER, 1);
		m_LogEnableFlag[i].Target[LOG_TO_DEBUGGER] = IsLogEnabled((LogType)i, LOG_TO_DEBUGGER, 1);
	}

	return true;
}

void LogConfig::Terminate()
{

}

LogEnableFlag& LogConfig::GetLogEnableFlag(LogType logType)
{
	return m_LogEnableFlag[logType];
}


/////////////////////////////////////////////////////////////////////////
/**
* @brief constructor
*/
LogManager::LogManager() : m_LogConfig(0)
{

}

/**
* @brief destructor
*/
LogManager::~LogManager()
{
	LogDataList m_TmpData;
	LogDataList m_TmpFreeData;

	m_Locker.Lock();
	m_TmpData.splice(m_TmpData.begin(), m_Data);
	m_TmpFreeData.splice(m_TmpFreeData.begin(), m_FreeData);
	m_Locker.UnLock();

	for(LogDataList::iterator it = m_TmpData.begin(); it != m_TmpData.end(); it++)
	{
		DoLog(*it);
		delete *it;
	}

	for(LogDataList::iterator it = m_TmpFreeData.begin(); it != m_TmpFreeData.end(); it++)
		delete *it;

	for (size_t j = 0; j < m_FileLoggers.size(); j++)
		delete m_FileLoggers[j];

	m_LogConfig = 0;

	m_init = false;
}

void LogManager::init(LogConfig* config)
{
	ZeroMemory((void*)m_LogID, sizeof(m_LogID));

	bool InitOk = false;

	m_LogConfig = config;

	do
	{
		if (!m_LogConfig)
			break;

		size_t FileCount = m_LogConfig->GetLogFileCount();
		for (size_t j = 1; j <= FileCount; j++)
		{
			LogToFile* FileLogger = new LogToFile;
			m_LogConfig->GetLogFileInfo(j, FileLogger->GetLogInfo());
			m_FileLoggers.push_back(FileLogger);
			if (!m_FileLoggers.back()->Initialize())
				break;
		}

		bool HasLogSvr = m_LogConfig->GetLogSocketServerInfo(m_SocketLogger.GetLogInfo());
		if (HasLogSvr && !m_SocketLogger.Initialize())
			break;

		InitOk = true;
	}while (false);

	if (!InitOk)
		return;

	m_init = m_LogConfig->Initialize();
}

bool LogDataLessThan (const LogData* a, const LogData* b)
{
	return a->GetSize() < b->GetSize();
};

/**
* @brief prepare log data, add log header, log tail.
*/
LogData* LogManager::PrepareLogData(LogType logType, size_t MsgBufSize, const char* format, va_list argp)
{
	m_logIDLocker.Lock();
	m_LogID[logType] += 1;
	m_logIDLocker.UnLock();

	size_t LogID = m_LogID[logType];

	time_t tmp = time(NULL);
    struct tm tm_s;
	struct tm* pt = localtime_r(&tmp, &tm_s);

	char HeadBuf[64]={0};


#ifdef _DEBUG
		size_t HeadSize = snprintf(HeadBuf, 64, "%s:%x[%02d/%02d:%02d:%02d %x@%x] [",
			GetTypeStr(logType), LogID, pt->tm_mday,pt->tm_hour, pt->tm_min, pt->tm_sec, GetCurrentThreadId(), GetCurrentProcessId());
#else
		size_t HeadSize = snprintf(HeadBuf, 64, "%x[%02d/%02d:%02d:%02d %x@%x] [",
			LogID, pt->tm_mday, pt->tm_hour, pt->tm_min, pt->tm_sec, (int)GetCurrentThreadId(), (int)GetCurrentProcessId());
#endif


	const static char TailBuf[] = "]\xD\xA";
	const static size_t TailSize = 3;

	const int retrymax = 7;
	int retry = 0;

	bool Error = false;
	size_t PrintBufSize;

	assert(LogData::DEFAULT_BUFSIZE > TailSize + HeadSize + 1);


	LogData* logData = GetData(((TailSize + HeadSize + MsgBufSize + LogData::DEFAULT_BUFSIZE + 1) 
			/ LogData::DEFAULT_BUFSIZE + 1) * LogData::DEFAULT_BUFSIZE);

	if (!logData)
		return false;

	do
	{
		size_t BufSize = logData->GetSize() - TailSize - HeadSize - MsgBufSize - 1;
		try
		{
			// assume error
			PrintBufSize = 0;
			PrintBufSize = vsnprintf(logData->GetBuffer() + HeadSize, BufSize, format, argp);
		} 
		catch (...) 
		{
			;
		}

		if (PrintBufSize < 0)
		{
			size_t NewSize = logData->GetSize() * 2;
			// free old one
			FreeData(logData, true);
			logData = GetData(NewSize);
			if (logData == 0)
				Error = true;
		}
		else if (PrintBufSize > BufSize || PrintBufSize == 0)
		{
			// maybe length of string to written is 0 or a unexpected result
			Error = true;
		} 
		else 
		{
			// succeeded
			break;
		}
	} while (!Error && ++retry <= retrymax);


	if (logData)
	{
		if (!Error)
		{
			PBYTE Buffer = (PBYTE)logData->GetBuffer();

			// fill head info
			CopyMemory(Buffer, HeadBuf, HeadSize);
			Buffer += HeadSize;

			// print data has been filled previously
			Buffer += PrintBufSize;

			// msg data will be filled later
			logData->SetMsgBuf(Buffer);
			logData->SetMsgSize(MsgBufSize);

			Buffer += MsgBufSize;

			// fill tail data
			CopyMemory(Buffer, TailBuf, TailSize + 1);
			Buffer += TailSize;

			// finish
			logData->SetLength(Buffer - (PBYTE)logData->GetBuffer());
			logData->SetType(logType);

		} 
		else
		{
			FreeData(logData, true);
			logData = 0;
		}
	}

	return logData;
}


bool LogManager::Log(LogType logType, const char* format, va_list argp)
{
	if (!m_init) return false;
	
	if (!IsLogEnabled(logType))
		return true;

	LogData* logData = PrepareLogData(logType, 0, format, argp);
	if (logData == 0)
	{
		// 引起崩溃，关闭 [6/2/2016 qianyifan]
		//logData = PrepareLogData(logType, 0, "Error", 0);
		//if (logData == 0)
			return false;
	}

	return DoLog(logData);
}

LogData* LogManager::GetData(size_t DataSize)
{
	LogData* logData = 0;

	Mutex_scope_lock locker(m_Locker);

	// find the first data that has buffer great than DataSize
	// note, stl upper_bound using !(CompareLogData) to determine which  great than which
	LogData key(0, DataSize);
	LogDataList::iterator it = lower_bound(m_FreeData.begin(), m_FreeData.end(), &key, LogDataLessThan);
	if (it == m_FreeData.end())
	{
		PBYTE bf = 0;
		try
		{
			bf = new(std::nothrow) BYTE[DataSize];
			memset(bf, 0, DataSize);
			logData = new LogData(bf, DataSize);
		} 
		catch (...) 
		{
			delete[] bf;
			delete logData;
			bf = 0;
			logData = 0;
		}

		if (logData)
			m_Data.push_front(logData);

	} 
	else 
	{
		m_Data.splice(m_Data.begin(), m_FreeData, it);
		logData = m_Data.front();
	}

	if (logData)
		logData->SetContainer(m_Data.begin());

	return logData;
}


void LogManager::FreeData(LogData* logData, bool bRemoveFromContainer)
{
	Mutex_scope_lock locker(m_Locker);

	// should we cache this size of data ?
	if (logData->GetSize() > 1024 * 1024)
	{
		if (bRemoveFromContainer)
			m_Data.erase(logData->GetContainer());

		delete logData;
	} 
	else 
	{
		LogDataList::iterator it = lower_bound(m_FreeData.begin(), m_FreeData.end(), logData, LogDataLessThan);
		if (bRemoveFromContainer)
			m_FreeData.splice(it, m_Data, logData->GetContainer());
		else
			m_FreeData.insert(it, logData);
	}
}

bool LogManager::DoLog(LogData* logData)
{
	if (logData && IsLogEnabled(logData->GetType()))
	{
		if (m_LogConfig)
		{
			LogEnableFlag& flags = m_LogConfig->GetLogEnableFlag(logData->GetType());

			if (flags.Target[LOG_TO_DEBUGGER])
				m_DebuggerLogger.Log(logData);

			if (flags.Target[LOG_TO_SERVER])
				m_SocketLogger.Log(logData);

			DWORD FileMask = flags.Target[LOG_TO_FILE];
			int FileID = 0;
			while (FileMask)
			{
				if (FileMask & 1)
					m_FileLoggers[FileID]->Log(logData);
				FileMask >>= 1;
				FileID++;
			}
		}
	}

	FreeData(logData, true);

	return true;
}

//////////////////////////////////////////////////////////////////////////

/**
* @brief at first you should inherit LogConfig class to set your setting of log, then call this function.
*/
void LogInitialize(LogConfig* cfg)
{
	LogManager::GetMe().init(cfg);
}

/**
* @brief when application exit you should call this function to free memory.
*/
void LogTerminate()
{
	LogManager::DelMe();
}

size_t LogApp(const char* format_str, ...)
{
	va_list argp;

	va_start (argp, format_str);

	size_t const result = LogManager::GetMe().Log(LOG_APP, format_str, argp) ? 0 : -1;

	va_end (argp);

	return result;
};

size_t LogDbg(const char* format_str, ...)
{
#ifdef _DEBUG
	va_list argp;

	va_start (argp, format_str);

	size_t const result = LogManager::GetMe().Log(LOG_TRACE, format_str, argp) ? 0 : -1;

	va_end (argp);

	return result;
#else
	return 0;
#endif
};

size_t LogInfo(const char* format_str, ...)
{
	va_list argp;

	va_start (argp, format_str);

	size_t const result = LogManager::GetMe().Log(LOG_INFO, format_str, argp) ? 0 : -1;

	va_end (argp);

	return result;
};

size_t LogTrace(const char* format_str, ...)
{
	return 0;
	va_list argp;

	va_start (argp, format_str);

	size_t const result = LogManager::GetMe().Log(LOG_TRACE, format_str, argp) ? 0 : -1;

	va_end (argp);

	return result;
};

size_t LogErr(const char* format_str, ...)
{
	va_list argp;

	va_start (argp, format_str);

	size_t const result = LogManager::GetMe().Log(LOG_ERROR, format_str, argp) ? 0 : -1;

	va_end (argp);

	return result;
};


