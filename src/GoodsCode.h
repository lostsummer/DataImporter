#pragma once

#include "Buffer.h"


const int MAX_CODE_LEN = 128;
const int LEN_STOCKCODE = 24;			///WI

const int NULL_ID = 999999;

const DWORD OLD_ID_MULTI = 1000000;
const DWORD NEW_ID_MULTI = 100000000;

inline DWORD g_GetGoodsID(BYTE cStation, DWORD dwNameCode)
{
	if (dwNameCode<OLD_ID_MULTI)
		return cStation*OLD_ID_MULTI+dwNameCode;			// 老的6位代码的编码
	else
		return (cStation+1)*NEW_ID_MULTI+dwNameCode;		// 新的8位代码的编码，为了兼容 -> cStation+1
}

inline BYTE g_ID2Station(DWORD dwGoodsID)
{
	if (dwGoodsID<NEW_ID_MULTI)		// 老的6位代码的编码
		return BYTE(dwGoodsID/OLD_ID_MULTI);
	else
		return BYTE(dwGoodsID/NEW_ID_MULTI-1);
}

inline DWORD g_ID2NameCode(DWORD dwGoodsID)
{
	if (dwGoodsID<NEW_ID_MULTI)		// 老的6位代码的编码
		return dwGoodsID%OLD_ID_MULTI;
	else
		return dwGoodsID%NEW_ID_MULTI;
}

inline DWORD g_AdjustID(DWORD dwGoodsID)	// 统一成8位代码，用以比较
{
	if (dwGoodsID<NEW_ID_MULTI)		// 老的6位代码的编码
		return (dwGoodsID/OLD_ID_MULTI+1)*NEW_ID_MULTI + dwGoodsID%OLD_ID_MULTI;
	else
		return dwGoodsID;
}

inline DWORD g_GetNewID(BYTE cStation, DWORD dwNameCode)
{
	return (cStation+1)*NEW_ID_MULTI+dwNameCode;		// 新的8位代码的编码，为了兼容 -> cStation+1
}

inline BOOL g_IsNullID(DWORD dwGoodsID)
{
	return (g_ID2NameCode(dwGoodsID)==NULL_ID);
}

inline void g_WriteCodeName(CBuffer& buf, const char* pcName, int nMaxLen)
{
	int nLen = 0;
	nLen =nMaxLen;
	for (int i=0; i<nMaxLen; i++)
	{
		if (pcName[i]==0){
			nLen = i;
			break;
		}
	}

	buf.WriteChar(nLen);
	buf.Write(pcName, nLen);
}

inline void g_ReadCodeName(CBuffer& buf, char* pcName, int nMaxLen)
{
	ZeroMemory(pcName, nMaxLen);

	UINT nLen = buf.ReadChar();
	if (nLen>(UINT)nMaxLen)
		nLen = nMaxLen;

	buf.Read(pcName, nLen);
}

inline void g_String2Code(char* pcCode, const CString strCode, int nLen)
{
	char pcBuf[MAX_CODE_LEN+1];
	
	if (nLen>MAX_CODE_LEN)
		nLen = MAX_CODE_LEN;

	strncpy(pcBuf, strCode, nLen);
	CopyMemory(pcCode, pcBuf, nLen);
}

inline CString g_Code2String(const char* pcCode, int nLen)
{
	char pcBuf[MAX_CODE_LEN+1];

	if (nLen>MAX_CODE_LEN)
		nLen = MAX_CODE_LEN;

	CopyMemory(pcBuf, pcCode, nLen);
	pcBuf[nLen] = '\0';

	return pcBuf;
}

//////////////////////////////////////////////////////////////////////

inline int g_CompareGoods(DWORD dwFirstID, const CString strFirstCode, DWORD dwSecondID, const CString strSecondCode)
{
	if (dwFirstID > dwSecondID)
		return 1;
	else if (dwFirstID < dwSecondID)
		return -1;
	else if (g_IsNullID(dwFirstID))
		return strFirstCode.Compare(strSecondCode);
	else
		return 0;
}

inline int g_CompareGoods(DWORD dwFirstID, const char* pcFirstCode, DWORD dwSecondID, const char* pcSecondCode)
{
	if (dwFirstID > dwSecondID)
		return 1;
	else if (dwFirstID < dwSecondID)
		return -1;
	else if (g_IsNullID(dwFirstID))
		return memcmp(pcFirstCode, pcSecondCode, LEN_STOCKCODE);
	else
		return 0;
}

//////////////////////////////////////////////////////////////////////

struct CGoodsCode
{
	CGoodsCode()
	{
		ZeroMemory(this, sizeof(CGoodsCode));
	}

	CGoodsCode(DWORD dwGoodsID, const char* pcCode = NULL)
	{
		m_dwGoodsID = dwGoodsID;
		if (pcCode)
		{
			CopyMemory(m_pcCode, pcCode, LEN_STOCKCODE);
			m_pcCode[LEN_STOCKCODE] = '\0';
		}
		else
			m_pcCode[0] = '\0';
	}

	CGoodsCode(DWORD dwGoodsID, const CString strCode)
	{
		m_dwGoodsID = dwGoodsID;
		strncpy(m_pcCode, strCode, LEN_STOCKCODE);
	}

	const CGoodsCode& operator=(const CGoodsCode& src)
	{
		m_dwGoodsID = src.m_dwGoodsID;
		CopyMemory(m_pcCode, src.m_pcCode, LEN_STOCKCODE+1);

		return *this;
	}

	CString Output()
	{
		CString str;
		str.Format("%d", m_dwGoodsID);
		
		if (g_IsNullID(m_dwGoodsID))
		{
			str += "+";
			str += m_pcCode;
		}

		return str;
	}

	void Recv(CBuffer& buf, BOOL bRecvCode)
	{
		m_dwGoodsID = buf.ReadInt();
		if (bRecvCode && g_IsNullID(m_dwGoodsID))
			g_ReadCodeName(buf, m_pcCode, LEN_STOCKCODE);
		else
			m_pcCode[0] = '\0';
	}

	void Send(CBuffer& buf, BOOL bSendCode)
	{
		buf.WriteInt(m_dwGoodsID);
		if (bSendCode && g_IsNullID(m_dwGoodsID))
			g_WriteCodeName(buf, m_pcCode, LEN_STOCKCODE);
	}

	DWORD m_dwGoodsID;
	char m_pcCode[LEN_STOCKCODE+1];
};

inline BOOL operator > (const CGoodsCode& first, const CGoodsCode& second)
{
	return g_CompareGoods(first.m_dwGoodsID, first.m_pcCode, second.m_dwGoodsID, second.m_pcCode) > 0;
}

inline bool operator == (const CGoodsCode& first, const CGoodsCode& second)
{
	return g_CompareGoods(first.m_dwGoodsID, first.m_pcCode, second.m_dwGoodsID, second.m_pcCode) == 0;
}

inline bool operator != (const CGoodsCode& first, const CGoodsCode& second)
{
	return g_CompareGoods(first.m_dwGoodsID, first.m_pcCode, second.m_dwGoodsID, second.m_pcCode) != 0;
}

inline bool operator < (const CGoodsCode& first, const CGoodsCode& second)
{
	return g_CompareGoods(first.m_dwGoodsID, first.m_pcCode, second.m_dwGoodsID, second.m_pcCode) < 0;
}


