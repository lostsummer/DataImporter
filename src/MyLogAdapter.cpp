/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file MyLogAdapter.cpp
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 18 19:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#include "MyLogAdapter.h"

/**
* @brief constructor
*/
MyLogAdapter::MyLogAdapter()
{
}

/**
* @brief destructor
*/
MyLogAdapter::~MyLogAdapter()
{
}

/**
* @brief when create a new file call this, you can set your log file name here.
* @param FileNameBase
* @param FileName
* @param Succeeded
*/
void MyLogAdapter::OnNewLogFile(std::string& FileNameBase, std::string& FileName, bool Succeeded)
{
	time_t tmp = time(NULL);
    struct tm tm_s;
	struct tm* pt = localtime_r(&tmp, &tm_s);

	char file[512+1] = {0};
	snprintf(file, 512, "%s/%04d%02d%02d_%02d%02d%02d_%s",
		m_LogPath.c_str(),
		(pt->tm_year+1900),
		pt->tm_mon+1,
		pt->tm_mday,
		pt->tm_hour,
		pt->tm_min,
		pt->tm_sec,
		FileNameBase.c_str());

	FileName = file;
}

/**
* @brief Get Log File Info. you can set file path, size, base name etc. here
* @param FileID
* @param FileInfo
*/
void MyLogAdapter::GetLogFileInfo(size_t FileID, LogFileInfo& FileInfo)
{
	FileInfo.Dir = m_LogPath;
	FileInfo.MaxFileSize = 10 * 1024 * 1024;
	FileInfo.SeperateByDate = true;
	FileInfo.MaxFileNumb = 0;

	switch (FileID)
	{
	case 1: FileInfo.FileNameBase = "DS_app.log"; break;
	case 2: FileInfo.FileNameBase = "DS_error.log"; break;
	case 3: FileInfo.FileNameBase = "DS_info.log"; break;
	case 4: FileInfo.FileNameBase = "DS_trace.log"; break;
	default: assert(false);
	}
}

/**
* @brief check which attr of log is enabled. you can set which log print to secreen , which log to file.
* @param logType
* @param logTarget
* @param TargetID
*/
bool MyLogAdapter::IsLogEnabled(LogType logType, LogTarget logTarget, int targetID)
{
	if (logTarget == LOG_TO_DEBUGGER)
	{
		switch (logType)
		{
		case LOG_APP: return true;
		case LOG_ERROR:return true;
		case LOG_INFO:return false;
		case LOG_TRACE:return false;
		default:return false;
		}
	}
	else if (logTarget == LOG_TO_SERVER)
		return false;
	else if (logTarget == LOG_TO_FILE)
	{
		switch (logType)
		{
		case LOG_APP: return targetID == 1;
		case LOG_ERROR:return targetID == 2;
		case LOG_INFO:return targetID == 3;
		case LOG_TRACE:
#ifdef _DEBUG
			return targetID == 4;
#else
			return false;
#endif
		default:return false;
		}
		return true;
	}

	return false;
}


