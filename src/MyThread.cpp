#include <sys/stat.h>
#include <math.h>
#include <signal.h>
// #if __GNUC__ == 4 && __GNUC_MINOR__ >= 7
// #include <ext/atomicity.h>
// #else
// #include <bits/atomicity.h>
// #endif

#include "MyThread.h"
#include "ThreadMgr.h"
#include "FHSP.h"
#include "MDS.h"
#include "MainTrack.h"
#include "Data.h"
#include "GoodsLua.h"

//////////////////////////////////////////////////////////////////////
extern DWORD g_dwFreeDataDelay;
extern BOOL g_bSyncKL;
extern BOOL g_bSyncKM;

CMyThread::CMyThread(bool joinbale)
{
	m_nThreadID = 0;
	sem_init(&m_semExit, 0, 0);
	m_delaySec = 0;
	m_delayNSec = 0;
	
	m_runnable = this;
	m_bJoinable = joinbale;
	m_bAlive = false;

	ThreadMgr::GetMe().AddThread(this);
}

CMyThread::CMyThread(CRunnable* runnable, bool joinbale)
{
	m_nThreadID = 0;
	sem_init(&m_semExit, 0, 0);
	m_delaySec = 0;
	m_delayNSec = 0;

	m_runnable = runnable;
	m_bJoinable = joinbale;
	m_bAlive = false;

	ThreadMgr::GetMe().AddThread(this);
}

CMyThread::~CMyThread()
{
	sem_destroy(&m_semExit);

	ThreadMgr::GetMe().RemoveThread(this);
}

bool CMyThread::Start(double delaySec)
{
	m_mutex.Lock();
	if (!m_bAlive)
	{
		m_delaySec = int(delaySec);												//秒
		m_delayNSec = int((delaySec - m_delaySec + 0.0000000005)*pow(10.0, 9.0));  //纳秒

		int ret = ::pthread_create(&m_nThreadID, NULL, s_ThreadProcFunc, this);
		if (ret != 0)
		{
			LOG_ERR("Start %s fail!! error NO. %d", Name(), ret);
			theApp.m_dwErrThreadCount++;
			m_mutex.UnLock();
			return false;
		}
	}
	else
	{
		m_mutex.UnLock();
	}

	return true;
}

void CMyThread::End()
{
	if (0 != m_nThreadID && m_bJoinable)
	{
		sem_post(&m_semExit);
		::pthread_join(m_nThreadID, NULL);
		m_nThreadID = 0;

		m_mutex.Lock();
		while(m_bAlive)
		{
			m_cond.Wait(m_mutex);
		}
		m_mutex.UnLock();
	}
}

void* CMyThread::s_ThreadProcFunc(void* pParam)
{
	CMyThread* pThread = static_cast<CMyThread*>(pParam);

	pThread->m_bAlive = true;
	pThread->m_cond.Broadcast();
	pThread->m_mutex.UnLock();

	sigset_t sig_mask;
	sigfillset(&sig_mask);
	pthread_sigmask(SIG_SETMASK, &sig_mask, NULL);

	pThread->Run();

	pThread->m_mutex.Lock();
	pThread->m_bAlive = false;
	pThread->m_cond.Broadcast();
	pThread->m_mutex.UnLock();
	if (!pThread->m_bJoinable)
	{
		SAFE_DELETE(pThread);
	}

	::pthread_exit(NULL);
}

void CMyThread::Run()
{
	m_runnable->Run();
}

const bool CMyThread::isAlive() const
{
	return m_bAlive;
}

const bool CMyThread::isJoinable() const
{
	return m_bJoinable;
}

pthread_t CMyThread::getMyThreadId() 
{
	return m_nThreadID;
}

pthread_t CMyThread::getCurrentThreadId()
{
	return ::pthread_self();
}

void CMyThread::atomicAdd(volatile int *val, int x)
{
	__gnu_cxx::__atomic_add((volatile _Atomic_word*)val, x);
}

void CMyThread::atomicInc(volatile int *val)
{
	__gnu_cxx::__atomic_add((volatile _Atomic_word*)val, 1);
}

void CMyThread::atomicDec(volatile int *val)
{
	__gnu_cxx::__atomic_add((volatile _Atomic_word*)val, -1);
}

void CMyThread::sleep(const int sec)
{
	::sleep(sec);
}

void CMyThread::msleep(int millis)
{
	::usleep(1000 * millis);
}

void CMyThread::usleep(const int usec)
{
	::usleep(usec);
}

void CMyThread::SetJoinable( bool joinable )
{
	m_bJoinable = joinable;
}
//////////////////////////////////////////////////////////////////////
void CMyTimerThread::Run()
{
	LogApp("%s start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);

	/////////////////////////////////////////////////
	//  [9/16/2014 %zhaolin%]
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);
	while(sem_timedwait(&m_semExit, &ts) != 0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
		{
			break;
		}

		try
		{
			theApp.OnTimer();
		}
		catch(...)
		{
			LOG_ERR("test theApp.OnTimer()执行异常 theApp.sTimeThreadInfo:%s",theApp.sTimeThreadInfo.c_str());
		}

		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}



CMyInitDayThread::CMyInitDayThread(bool bJoinable) : CMyThread(bJoinable), m_cType(0)
{
	LOG_APP("%s", Name());
}

CMyInitDayThread::~CMyInitDayThread()
{
	LOG_APP("%s", Name());
}

void CMyInitDayThread::Run()
{
	LogApp("%s start", Name());
	if (m_cType<1 || m_cType>3)	//20110629
		return;
	
	if (m_cType==1)
		LogInfo("SH&SZ开盘开始");
	else if (m_cType==2)
		LogInfo("HK开盘开始");
	else
		LogInfo("开盘开始");

	theApp.m_thCurBK.End();
	theApp.m_thSaveGoods.End();
	theApp.m_thDelData.End();
	theApp.m_thDM.End();
	
	if (m_cType&0x01)
	{
		theApp.m_dwTimeSH_bar = 0;
		theApp.m_dwTimeSZ_bar = 0;
		theApp.m_dwTimeSH_minute = 0;
		theApp.m_dwTimeSZ_minute = 0;
		
		if (theApp.m_fhsp.Read()==false)
		{
			LOG_ERR("Data/fhspL2.dat read exception!");
			//return;
		}
		if(theApp.m_jbm.Read()==false)
		{
			LOG_ERR("Data/jbmL2.dat read exception!");
			//return;
		}
		if (theApp.m_dptj.Read()==false)
		{
			LOG_ERR("Data/jjtj.dat read exception!");
			//return;
		}

		g_mt.Init();

		theApp.ClearDataFile(FALSE);
	}

	if (m_cType&0x02)
		theApp.ClearDataFile(TRUE);

	theApp.m_wrGoods.AcquireReadLock();

	int nGoods = theApp.m_aGoods.GetSize();
	for (int i=0; i<nGoods; i++)
	{					
		CGoods* pGoods = (CGoods*)theApp.m_aGoods[i];
		if (pGoods)	
		{
			if (pGoods->m_cStation==6)
				continue;

			if (pGoods->m_cStation==5)	//HK
			{
				if ((m_cType&0x02)==0)
					continue;
			}
			else
			{
				if ((m_cType&0x01)==0)
					continue;
			}


			RWLock_scope_wrlock  rwlock(pGoods->m_rwGood);
			pGoods->m_rwGoodcount[10] += 1;
			pGoods->InitDay();
			pGoods->ClearDay();
            pGoods->MakePY();

			pGoods->m_rwGoodcount[10] -= 1;

			pGoods->m_rwMinute.AcquireWriteLock();
			pGoods->ClearMinute(TRUE, 0);
			pGoods->m_rwMinute.ReleaseWriteLock();

			pGoods->m_rwBargain.AcquireWriteLock();
			pGoods->ClearBargain(TRUE, 0);
			pGoods->m_rwBargain.ReleaseWriteLock();

			/*
			似乎暂时用不到
			pGoods->m_rwHisMin.AcquireWriteLock();
			pGoods->ClearHisMin();
			pGoods->m_rwHisMin.ReleaseWriteLock();
			*/
		}
	}

	theApp.m_wrGoods.ReleaseReadLock();

	if (m_cType&0x01)
	{
		theApp.LoadDynamicGroupTxt();
		theApp.UpdateGroupData();//检查GROUP/DATA文件夹是否有更新
		theApp.UpdateDynamicGroupTxt();

		theApp.DeleteGroup();

		theApp.LoadGroup(14, "概念板块");
		theApp.LoadGroup(15, "行业板块2");
		theApp.LoadGroup(16, "地区板块");
		theApp.LoadGroup(25, "三级概念");
		theApp.LoadGroup(-1, "益盟板块");
		theApp.LoadGroup(46, "三级行业");

		theApp.ClearHotGroup();
		theApp.LoadHotGroup();// 热点板块 [11/6/2012 frantqian]


		theApp.m_wrCurBK.AcquireWriteLock();
		theApp.m_nCountCurBK = 1;
		theApp.m_CurBK.m_dwDate = theApp.m_dwDateValue;
		ZeroMemory(theApp.m_CurBK.m_Top, sizeof(CZDF_ZJJL)*3);
		ZeroMemory(theApp.m_CurBK.m_Bottom, sizeof(CZDF_ZJJL)*3);

		theApp.m_wrCurBK.ReleaseWriteLock();
	}

	if (!theApp.m_thDM.Start(0.000002))
		LOG_ERR("After Init Date, Start DM Thread fail!!");

	if (!theApp.m_thDelData.Start(1))
		LOG_ERR("After Init Date, Start Save Goods Thread fail!!");

	if (!theApp.m_thSaveGoods.Start(3))
		LOG_ERR("After Init Date, Start Save Goods Thread fail!!");

	if (!theApp.m_thCurBK.Start(5))
		LOG_ERR("After Init Date, Start Cur BK Thread fail!!");

	if (m_cType&0x02)
	{
		theApp.m_dwInitDateHK = theApp.m_dwDateValue;
		theApp.SetProfileInt("System", "InitDateHK", theApp.m_dwInitDateHK);
	}

	if (m_cType&0x01)
	{
		theApp.FlushAllDB();
		theApp.m_dwInitDate = theApp.m_dwDateValue;
		theApp.SetProfileInt("System", "InitDate", theApp.m_dwInitDate);
	}

	if (m_cType==1)
		LogInfo("SH&SZ开盘结束：%d",theApp.m_dwInitDate);
	else if (m_cType==2)
		LogInfo("HK开盘结束:%d", theApp.m_dwInitDateHK);
	else
		LogInfo("开盘结束");

    CString sTemp;
    sTemp.Format("%sData/Min1_%d.dat", theApp.m_strSharePath.GetData(), CMDSApp::g_dwSysYMD);
    theApp.m_dfTodayMin1.Initial(sTemp);

	theApp.m_bInitingDate = false;
	theApp.m_bSaveingDate = false;

	LogApp("%s end", Name());
}

//////////////////////////////////////////////////////////////////////
CMySaveDayThread::CMySaveDayThread(bool bJoinable) : CMyThread(bJoinable), m_bHK(false)
{
	LOG_APP("%s", Name());
}

CMySaveDayThread::~CMySaveDayThread()
{
	LOG_APP("%s", Name());
}

void CMySaveDayThread::Run()
{
	LogApp("%s start", Name());

	DWORD dwDateValue = 0;
	
	if (m_bHK){
		LogInfo("HK收盘开始:%d",theApp.m_dwDateValueHK);
		dwDateValue = theApp.m_dwDateValueHK;
	}
	else
	{
		dwDateValue = theApp.m_dwDateValue;
		
		theApp.m_thCurBK.End();
		LogInfo("SA&SZ收盘开始:%d",theApp.m_dwDateValue);
	}

	CHisMinDF* pHMDF = theApp.GetHisMinDF(dwDateValue);

	theApp.m_wrGoods.AcquireReadLock();
	int nGoods = theApp.m_aGoods.GetSize();
	if (nGoods)
	{
		for (int i=0; i<nGoods; i++)
		{					
			CGoods* pGoods = (CGoods*)theApp.m_aGoods[i];
			if (pGoods) 
			{
				if (pGoods->m_cStation==6)
					continue;

				if ((m_bHK==TRUE && pGoods->m_cStation!=5) || (m_bHK==FALSE && pGoods->m_cStation==5))
					continue;

				//pGoods->m_rwGood.AcquireReadLock();
				// 更换锁 [3/5/2015 frant.qian]
				RWLock_scope_rdlock  rdlock(pGoods->m_rwGood);	
				pGoods->m_rwGoodcount[11] += 1;
				pGoods->SaveToday();
				// 检查收盘是否成功，并计数 [4/22/2013 frantqian]
				if (pGoods->WantSaveToday(TRUE))
				{
					//LogApp("goods save: %d", pGoods->GetID());
					theApp.m_nSaveDayCount++;
					theApp.m_mapSaveGoodsID.insert(std::make_pair(pGoods->GetID(),pGoods->GetID()));
				}
				else
				{
					theApp.m_nNoSaveDayCount++;
					//LogApp("no check goods save: %d", pGoods->GetID());
				}
				
				pGoods->m_rwGoodcount[11] -= 1;
				//pGoods->m_rwGood.ReleaseReadLock();

				if (pHMDF)
					pGoods->SaveHisMin(pHMDF, dwDateValue);

			}
			
		}
	}

	theApp.m_wrGoods.ReleaseReadLock();

	if (m_bHK)
	{
		theApp.m_dwSaveDateHK = CMDSApp::g_dwSysYMD;
		theApp.m_bSaveingDateHK = false;
		theApp.m_bInitingDate = false;

		theApp.SetProfileInt("System", "SaveDateHK", theApp.m_dwSaveDateHK);
		LogInfo("HK收盘结束:%d,count = %d, nosavecount = %d",theApp.m_dwSaveDateHK, theApp.m_nSaveDayCount, theApp.m_nNoSaveDayCount);
	}
	else
	{
		theApp.m_wrCurBK.AcquireReadLock();
		DWORD dwCurBKDate = theApp.m_CurBK.m_dwDate;
		theApp.m_wrCurBK.ReleaseReadLock();
		if (dwCurBKDate > 0)
		{
			theApp.m_aHisBK.Change(theApp.m_CurBK);
			theApp.m_dfHisBK.SaveData(1, theApp.m_aHisBK, dwCurBKDate, FALSE);
		}

		theApp.WriteGoods();

		g_CpxStat.Write();

		theApp.m_dwSaveDate = CMDSApp::g_dwSysYMD;
		theApp.m_bSaveingDate = false;
		theApp.m_bInitingDate = false;

		theApp.m_thCurBK.Start(5);

		theApp.SetProfileInt("System", "SaveDate", theApp.m_dwSaveDate);

		LogInfo("SA&SZ收盘结束:%d, count = %d",theApp.m_dwSaveDate, theApp.m_nSaveDayCount);

	}
	
	LogApp("%s end", Name());
}

//////////////////////////////////////////////////////////////////////
void CMySaveGoodsThread::Run()
{
	LogApp("%s start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	//ts.tv_sec += m_delaySec;
	//ts.tv_nsec += m_delayNSec;

	/////////////////////////////////////////////////
	//  [9/16/2014 %zhaolin%]
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);
	////////////////////////////////////////////
	
	while(sem_timedwait(&m_semExit, &ts)!=0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
			break;

		if (theApp.m_bInitingDate) continue;

		if (theApp.m_bSaveingDate || theApp.m_bSaveingDateHK) continue;

		if (theApp.m_bGoodsChanged)
		{
			theApp.m_bGoodsChanged = false;
			theApp.WriteGoods();
		}

		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}
//////////////////////////////////////////////////////////////////////////
void CMySyncRedisThread::Run()
{
	LogApp("%s start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);

	theApp.FlushSub(m_SyncType);
	
	while(sem_timedwait(&m_semExit, &ts)!=0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
			break;

		if (theApp.m_bInitingDate) continue;

		if (theApp.m_bSaveingDate || theApp.m_bSaveingDateHK) continue;

		theApp.SyncRedis(m_SyncType);
		
		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}
//////////////////////////////////////////////////////////////////////////
void CMyDeleteDataThread::Run()
{
	LogApp("%s start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	//ts.tv_sec += m_delaySec;
	//ts.tv_nsec += m_delayNSec;

	/////////////////////////////////////////////////
	//  [9/16/2014 %zhaolin%]
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);
	////////////////////////////////////////////

	CPtrArray aGoods;
	WORD wGoodsNum = 0;

	while(sem_timedwait(&m_semExit, &ts)!=0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
			break;

		if (wGoodsNum!=theApp.m_wGoods)
		{
			theApp.m_wrGoods.AcquireReadLock();
			aGoods.Copy(theApp.m_aGoods);
			wGoodsNum = theApp.m_wGoods;
			theApp.m_wrGoods.ReleaseReadLock();
		}

		int nSize = (int)aGoods.GetSize();
		for (int i=0; i<nSize; i++)
		{
			CGoods* pGoods = (CGoods*)aGoods[i];
			pGoods->FreeData(this);
		}

		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}

//////////////////////////////////////////////////////////////////////////
void CMyLoadDataThread::Run()
{
	LogApp("%s start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	//ts.tv_sec += m_delaySec;
	//ts.tv_nsec += m_delayNSec;

	/////////////////////////////////////////////////
	//  [9/16/2014 %zhaolin%]
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);
	////////////////////////////////////////////

	CPtrArray aGoods;
	//WORD wGoodsNum = 0;

	while(sem_timedwait(&m_semExit, &ts)!=0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
			break;

		if (g_LoadData.DoLoadData(*this) != 0)
			break;

		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}


//////////////////////////////////////////////////////////////////////////
void CMyCurBKThread::Run()
{
	LogApp("%s Start", Name());

	int lCount = 0;

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	//ts.tv_sec += m_delaySec;
	//ts.tv_nsec += m_delayNSec;

	/////////////////////////////////////////////////
	//  [9/16/2014 %zhaolin%]
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);
	////////////////////////////////////////////

	while(sem_timedwait(&m_semExit, &ts) != 0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
			break;

		if (lCount != theApp.m_nCountCurBK)
		{
			theApp.CalcCurBK();
			lCount = theApp.m_nCountCurBK;
		}
	
		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}

//////////////////////////////////////////////////////////////////////////


void CLuaThread::Run()
{
	LogApp("%s Start", Name());

	struct timespec ts;
	clock_gettime(CLOCK_REALTIME, &ts);
	unsigned long ulAddSec = 0;
	unsigned long ulNSec = m_delayNSec + ts.tv_nsec ;
	ulAddSec = ( ulNSec )/(1000*1000*1000);
	ts.tv_sec += ( m_delaySec + ulAddSec );
	ts.tv_nsec = ulNSec%(1000*1000*1000);

	while(sem_timedwait(&m_semExit, &ts) != 0)
	{
		if(errno != ETIMEDOUT && errno != EINTR && errno != EINVAL)
			break;

	    //todo
        //
		clock_gettime(CLOCK_REALTIME, &ts);
		ulNSec = m_delayNSec + ts.tv_nsec ;
		ulAddSec = ( ulNSec )/(1000*1000*1000);
		ts.tv_sec += ( m_delaySec + ulAddSec );
		ts.tv_nsec = ulNSec%(1000*1000*1000);
	}

	LogApp("%s end", Name());
}
