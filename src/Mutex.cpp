/** *************************************************************************
* FSClient
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file Mutex.cpp
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#include "Mutex.h"

bool Mutex::attr_initalized = false;
pthread_mutexattr_t Mutex::attr;
/**
* @brief Initializes a mutex class, with InitializeCriticalSection / pthread_mutex_init
*/
Mutex::Mutex()
{
	if(!attr_initalized)
	{
		::pthread_mutexattr_init(&attr);
		::pthread_mutexattr_settype(&attr, recursive);
		attr_initalized = true;
	}

	::pthread_mutex_init(&mutex, &attr);
}

/**
* @brief Deletes the associated critical section / mutex
*/
Mutex::~Mutex() 
{ 
	::pthread_mutex_destroy(&mutex); 
}


