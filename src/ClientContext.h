#ifndef CLIENTCONTEXT_H
#define CLIENTCONTEXT_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "SysGlobal.h"
#include "MySocket.h"

class CClientContext : public CMySocket
{
public:
	CClientContext();
	virtual ~CClientContext();

	virtual void AfterConnect();
	virtual void Close();

	virtual int DoSend(CSockThreadData* pThreadData);

	virtual int HandleClientEvent(unsigned int unEvents, CSockThreadData* pThreadData = NULL);

public:
	int m_nConnectID;
	struct epoll_event	m_epollEvent;

	BOOL m_bWriteReady;
	char m_pcAddress[16];
};

#endif


