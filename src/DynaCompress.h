#ifndef DYNACOMPRESS_H
#define DYNACOMPRESS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "BitStream.h"
#include "Goods.h"

const int DYNA_BEGIN_DATE = 20100101;


struct CExpConfig;
class CDynaCompress  
{
public:
	static int CompressDynamic(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP, BOOL bNew);
	static int CompressDynamicH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna);
	static int CompressDynamicQH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, WORD wVersion, BOOL bL2Encode, BOOL bNeedMMP);

	static int ExpandDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna);
	static int ExpandDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna);
	static int ExpandDynaDataQH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna);

	//Steven New Begin
	static int ExpandDynaData6(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna);
///20120319 sjp
	static int ExpandStatic5(CBitStream& stream, CGoods* pGoods);
	static int ExpandStaticQH5(CBitStream& stream, CGoods* pGoods);
	static int ExpandXGStatic5(CBitStream& stream, CGoods* pGoods);

	static int ExpandExpValue(CBitStream& stream, CGoods* pGoods, CSortArray<CExpConfig>& aExpConfig);
///20120319 sjp
	static int ExpandXGStaticData(CBitStream& stream, CGoods* pGoods);
	static int ExpandStatic6(CBitStream& stream, CGoods* pGoods);
	static int ExpandStaticQH6(CBitStream& stream, CGoods* pGoods);
	//Steven New End
	static int CompressStatic(CBitStream& stream, CGoods* pGoods, WORD wVersion);
	static int ExpandStatic(CBitStream& stream, CGoods* pGoods);

	static int CompressStaticQH(CBitStream& stream, CGoods* pGoods, WORD wVersion);
	static int ExpandStaticQH(CBitStream& stream, CGoods* pGoods);

	static int CompressQuoteStatic(CBitStream& stream, CGoods* pGoods, WORD wVersion);
	
	static int CompressQuote(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP);

	static int CompressHLVA(CBitStream& stream, CGoods* pGoods, DWORD PRICE_DIVIDE, DWORD dwRecvHigh, DWORD dwRecvLow, XInt32 xRecvVolume, XInt32 xRecvAmount);
	static int CompressVA(CBitStream& stream, CGoods* pGoods, XInt32 xRecvVolume, XInt32 xRecvAmount);
	static int CompressVA(CBitStream& stream, XInt32 xVolume, XInt32 xAmount, XInt32 xRecvVolume, XInt32 xRecvAmount);

	//sam copy from pc client begin
	static DWORD ExpandDate(CBitStream& stream, DWORD dwDateBase = DYNA_BEGIN_DATE);
	//sam copy from pc client end

private:
	static int CompressDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP, BOOL bNew);	
	static int CompressDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, BOOL bL2Encode, BOOL bNeedMMP, BOOL bNew);

	static int CompressDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna);	
	static int CompressDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna);

	static int ExpandDynaData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, BOOL bL2Encode);
	static int ExpandDynaDataH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna);
	static int ExpandDynaDataQH(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna);
	//Steven New Begin
	static int ExpandDynaData6(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna);
	//Steven New End

	static int EncodeAllMMPPrice(CBitStream& stream, int NUM_OF_MMP, int nNumBuy, int nNumSell, PDWORD adwMMPPrice, DWORD dwPrice);
	static int DecodeAllMMPPrice(CBitStream& stream, int NUM_OF_MMP, int nNumBuy, int nNumSell, PDWORD adwMMPPrice, DWORD dwPrice);

	static void CompressOrderCounts(CBitStream& stream, COrderCounts& ocNew, BOOL bNew, BOOL bOrder);
	static void CompressOrderCounts(CBitStream& stream, COrderCounts& ocNew, COrderCounts& ocOld, BOOL bNew, BOOL bOrder);

	static void ExpandOrderCounts(CBitStream& stream, COrderCounts& ocNew, CDynamicValue* pDyna, BOOL bOrder);
	static void ExpandOrderCounts(CBitStream& stream, COrderCounts& ocNew, COrderCounts& ocOld, CDynamicValue* pDyna, BOOL bOrder);

	static void CompressMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bIsQH, BOOL bL2Encode);
	static void CompressMMP(CBitStream& stream, CDynamicValue* pDyna, BOOL bIsQH, BOOL bL2Encode);

	static void ExpandMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bIsQH, BOOL bL2Encode);
	static void ExpandMMP(CBitStream& stream, CDynamicValue* pDyna, BOOL bIsQH, BOOL bL2Encode);

	static void CompressVirtualMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna);
	static void CompressVirtualMMP(CBitStream& stream, CDynamicValue* pDyna);

	static void ExpandVirtualMMP(CBitStream& stream, CDynamicValue* pDyna, CDynamicValue* pOldDyna);
	static void ExpandVirtualMMP(CBitStream& stream, CDynamicValue* pDyna);

	static int CompressQuoteData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, CDynamicValue* pOldDyna, BOOL bL2Encode, BOOL bNeedMMP);
	static int CompressQuoteData(CBitStream& stream, CGoods* pGoods, CDynamicValue* pDyna, BOOL bL2Encode, BOOL bNeedMMP);
};

#endif


