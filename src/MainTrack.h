#ifndef MAINTRACK_H
#define MAINTRACK_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "RWLock.h"
#include "SortArray.h"

class CMainTrack
{
public:
	CMainTrack()
	{
		m_dwGoodsID = 0;
		m_nValue = 0;
		m_cGroupHY = 0;
	}

	CMainTrack(DWORD dwGoodsID, DWORD dwValue, char cGroupHY)
	{
		m_dwGoodsID = dwGoodsID;
		m_nValue = dwValue;
		m_cGroupHY = cGroupHY;
	}

	CMainTrack(const CMainTrack& rth)
	{
		m_dwGoodsID = rth.m_dwGoodsID;
		m_nValue = rth.m_nValue;
		m_cGroupHY = rth.m_cGroupHY;
	}

	CMainTrack& operator=(const CMainTrack& rth)
	{
		if (this != &rth)
		{
			m_dwGoodsID = rth.m_dwGoodsID;
			m_nValue = rth.m_nValue;
			m_cGroupHY = rth.m_cGroupHY;
		}

		return *this;
	}

public:
	DWORD m_dwGoodsID;
	int m_nValue;
	char m_cGroupHY;
};

inline BOOL operator > (const CMainTrack& first, const CMainTrack& second)
{
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

inline BOOL operator == (const CMainTrack& first, const CMainTrack& second)
{
	return first.m_dwGoodsID==second.m_dwGoodsID;
}

inline BOOL operator < (const CMainTrack& first, const CMainTrack& second)
{
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

//////////////////////////////////////////////////////////////////////

class CMainTrackAll
{
public:
	CMainTrackAll();
	~CMainTrackAll();

	void Init();
	void Compress();

	void AddZC(DWORD dwGoodsID, int nValue, char cGroupHY);
	void ChangeZC(DWORD dwGoodsID, int nValue, char cGroupHY);
	void DeleteZC(DWORD dwGoodsID);
	void AddQM(DWORD dwGoodsID, int nValue, char cGroupHY);
	void ChangeQM(DWORD dwGoodsID, int nValue, char cGroupHY);
	void DeleteQM(DWORD dwGoodsID);

public:
	RWLock m_wr;
	CSortArray<CMainTrack> m_aZC;
	CSortArray<CMainTrack> m_aQM;

	char* m_pcBuffer;
	DWORD m_dwBuffer;
	DWORD m_dwTotal;

	DWORD m_dwCountFresh;
	DWORD m_dwCountCompress;
};

extern CMainTrackAll g_mt;

#endif

