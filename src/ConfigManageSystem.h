/********************************************************
 * 版权所有 (C)2004－2008
 * 
 * 文件名称：ConfigManageSystem.h
 * 文件标识：
 * 模块名称：
 * 内容摘要：
 * 其它说明：
 * 当前版本：
 * 作    者：刘成柱
 * 完成日期：2006-8-7 18:20:19
 * 
 * 修改记录：
 *   修改日期：
 *   版 本 号：
 *   修 改 人：
 *   修改内容：
 *********************************************************/
#ifndef CONFIGMGRSYS_H
#define CONFIGMGRSYS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <string>
#include <vector>

const int MAXCOUNT=3000;

/**类名称 : ConfigDataItem
***基类名称: 
***类说明 : 配置项类
*/
class ConfigDataItem
{
//{ modify by panlishen 2010-01-05
public:
	ConfigDataItem(const std::string& group, const std::string& item, const std::string& value) 
		: m_group(group), m_item(item), m_value(value)
	{}

	std::string m_group;			//组名
	std::string m_item;			//项名
	std::string m_value;			//项值
//} modify by panlishen 2010-01-05
};

/**类名称 : ConfigData
***基类名称: 
***类说明 : 配置数据集合类
*/
class ConfigData
{
public:
	std::vector<ConfigDataItem> m_listitem;	//配置项列表

public:
	//初始化函数
	ConfigData();
};

/**类名称 : ConfilgLoader
***基类名称: 
***类说明 : 配置数据加载类
*/
class ConfigLoader
{
public:
	ConfigData* m_pData;					//配置集合指针
	std::string m_FileName;						//配置文件名

public:
	ConfigLoader(){m_pData=NULL;}
	/**函数名称:SetFileName
	***功能说明:设置配置文件名称
	* @param : char *filename
	* @return : void 
	*/
	void SetFileName(const std::string& filename);

	/**函数名称:SetpData
	***功能说明:设置配置集合指针
	* @param : ConfigData *pData
	* @return : void 
	*/
	void SetpData(ConfigData *pData);

	/**函数名称:Load
	***功能说明:从配置文件中加载配置到配置集合
	* @return : void 
	*/
	void Load();

	/**函数名称:Save
	***功能说明:配置集合写入配置文件中
	* @return : void 
	*/
	void Save();
private:
	//{ add by panlishen 2010-01-04
	//去掉所有空格
	void Trim(std::string& str);

	//去掉左边空格
	void TrimLeft(std::string& str);
	//} add by panlishen 2010-01-04

	/**函数名称:f_LoadData
	***功能说明:从配置文件中加载配置到配置集合
	* @return : void 
	*/
	void f_LoadData();	

	/**函数名称:f_SaveData
	***功能说明:从配置文件中加载配置到配置集合
	* @return : void 
	*/
	void f_SaveData();	
};

/**类名称 : ConfigManager
***基类名称: 
***类说明 : 配置管理类
*/
class ConfigManager
{
private:
	ConfigData* m_pData;	//配置集合指针				
public:

	/**函数名称:ConfigManager
	***功能说明:初始化函数
	* @return : 
	*/
	ConfigManager();

	/**函数名称:SetpData
	***功能说明:设置配置集合指针
	* @param : ConfigData *pData
	* @return : void 
	*/
	void SetpData(ConfigData *pData);

	/**函数名称:SetConfig
	***功能说明:设置配置项的值
	* @param : char *group
	* @param : char *item
	* @param : char *value
	* @return : void 
	*/
	void SetConfig(const char *group, const char *item, const char *value);

	/**函数名称:GetConfig
	***功能说明:获取配置项的值
	* @param : char *group
	* @param : char *item
	* @param : char *value
	* @return : void 
	*/
	void GetConfig(const char *group, const char *item, std::string& value);
};

#endif



