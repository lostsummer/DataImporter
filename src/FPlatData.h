#pragma once
#include "Buffer.h"
#include "XInt32.h"

#define PX_COL_TOTAL		108 //XInt32 字段总数
#define CS_COL_TOTAL		10	//CString字段总数

// 股票的字段 -1表示废弃
#define S_SecuType			0	//证券类型  1001 A股  100 2B股
#define S_IsFinacial		1	//是否金融类  1 为金融类   0 为非金融类
// 删除 20130712	
#define S_TradeDate			-1 //交易日期 (年月日)
#define S_CZHD_XJL			2	//筹资活动现金流 20140226
#define S_EndDate			3	//报告日期
#define S_AG_XS				4	//限售AG
// 删除 20130712
#define S_SJL				-1	//市净率
#define S_TZHD_XJL			5	//投资活动现金流 20140226
#define S_MGSY				6	//每股收益
#define S_MGJZC				7	//每股净资产
#define S_MGJYXJL			8	//每股经营现金流
#define S_ZGB				9	//总股本
#define S_LTGB				10	//流通股本
#define S_YXLTGB			11	//有效流通股本
#define S_HJCG				12	//户均持股
#define S_HJCGTB			13	//户均持股同比
#define S_GDHSB				14	//股东户数比
#define S_ZYSRTB			15	//主营收入同比
#define S_ZYLRTB			16	//主营利润同比
#define S_JLRTB				17	//净利润同比
#define S_XSMLL				18	//销售毛利率
#define S_JLXSYL			19	//净利息收益率
#define S_JZCSYL			20	//净资产收益率
#define S_GDQYB				21	//股东权益比
#define S_ZCFZL				22	//资产负债率
#define S_GuXiL				23	//股息率
#define S_JGCGJS			24	//机构持股家数
#define S_JJCGJS			25	//基金持股家数
#define S_JGCGZB			26	//机构持股占比
#define S_JJ_ZYXLTB			27	//基金持股占有效流通股比
#define S_QF_ZYXLTB			28	//QFII持股占有效流通股比
#define S_BX_ZYXLTB			29	//保险持股占有效流通股比
#define S_QS_ZYXLTB			30	//券商持股占有效流通股比
#define S_XT_ZYXLTB			31	//信托持股占有效流通股比
#define S_ZYSR				32	//主营收入
#define S_ZYLR				33	//主营利润
#define S_JYHDXJL			34	//经营活动现金流
#define S_YFZK				35	//预付账款
#define S_YSZK				36	//应收账款
#define S_XJYHKX			37	//现金及存放中央银行款项
#define S_FFDKDK			38	//发放贷款及垫款
#define S_TYCFKX			39	//同业及其他金融机构存放款项
#define S_XSKX				40	//吸收存款
#define S_AG				41	//A股
#define S_BG				42	//B股
#define S_HG				43	//H股
#define S_MGGJJ				44	//每股公积金
#define S_MGWFPLR			45	//每股未分配利润
#define S_SSRQ				46	//上市日期
#define S_GDHS_HB			47	//股东户数环比		4个小数位
#define S_GDZHS_XD			48	//股东总户数相对值	4个小数位
#define S_AG_HS_XD			49	//A股户数相对值		4个小数位
#define S_ZX_HJCG			50	//最新户均持股		4个小数位
#define S_ZX_HJLTA			51	//最新户均流通A股	4个小数位
#define S_ZX_HJYXLT			52	//最新户均有效流通股 4个小数位
#define S_AG_HS_HB			53	//A股户数环比		4个小数位
#define S_AG_HS_TB			54	//A股户数同比		4个小数位
#define S_HJCG_HB			55	//户均持股环比		4个小数位
#define S_HJ_LTA_HB			56	//户均流通A股环比	4个小数位
#define S_HJ_YXLT_HB		57	//户均有效流通股环比 4个小数位
//FP_Quotation_Features表
#define S_52Z_ZF			58	//52周涨幅			4个小数位
#define S_FH_BGQ			59	//分红报告期		年月日
#define S_PXBL_HKD			60	//派息比例(港币)	4个小数位
#define S_PXBL_USD			61	//派息比例(美元)	4个小数位
#define S_PXBL_RMB			62	//派息比例(人民币)	4个小数位
#define S_ZZ_BL				63	//转增比例			4个小数位
#define S_SG_BL				64	//送股比例			4个小数位
#define S_FAJD_BM			65	//方案进度编码
#define S_YJYG_BGQ			66	//业绩预告报告期	年月日
#define S_CW_BGQ			67	//财务报告期		年月日
//FP_Quotation_Finance表
#define S_YYLR				68	//营业利润			4个小数位  元
#define S_QTLR				69	//其他利润			4个小数位  元
#define S_YYWSZ				70	//营业外收支		4个小数位  元
#define S_BTSR				71	//补贴收入			4个小数位  元
#define S_TZSY				72	//投资收益			4个小数位  元
#define S_SYTZ				73	//损益调整			4个小数位  元
#define S_LRZE				74	//利润总额			4个小数位  元
#define S_JLR				75	//净利润			4个小数位  元
#define S_ZZC				76	//总资产			4个小数位  元
#define S_LDZC				77	//流动资产			4个小数位  元
#define S_WXZC				78	//无形资产			4个小数位  元
#define S_GDZC				79	//固定资产			4个小数位  元
#define S_CQTZ				80	//长期投资			4个小数位  元
#define S_YFu_ZK			81	//应付账款			4个小数位  元
#define S_YS_ZK				82	//预收账款			4个小数位  元
#define S_LD_FZ				83	//流动负债			4个小数位  元
#define S_CQ_FZ				84	//长期负债			4个小数位  元
#define S_JZC				85	//净资产			4个小数位  元
#define S_GDQY				86	//股东权益			4个小数位  元
#define S_ZB_GJJ			87	//资本公积金		4个小数位  元
#define S_F_WFPLR			88	//非未分配利润		4个小数位  元
#define S_NH_MGSY			89	//每股收益(年化)	4个小数位  元
#define S_JLRL				90	//净利润率			4个小数位  %
#define S_SXL				91	//市销率			4个小数位  倍
#define S_ZYSR_HB			92	//主营收入环比		4个小数位  %
#define S_ZYLR_HB			93	//主营利润环比		4个小数位  %
#define S_JLR_HB			94	//净利润环比		4个小数位  %
#define S_Y3_FHZZ			95	//三年复合增长		4个小数位  %
//FP_Quotation_Shares表
#define S_GD_HS				96	//股东户数			户
#define S_JJ_CG				97	//基金持股			4个小数位  万股
#define S_JJ_CG_SZ			98	//基金持股市值		4个小数位  亿元
#define S_ZLTGB_JG			99	//机构持股占流通股比	4个小数位  %
#define S_ZLTGB_JJ			100 //基金持股占流通股比	4个小数位  %
#define S_ZLTGB_QF2			101 //QFII持股占流通股比	4个小数位  %
#define S_ZLTGB_BX			102 //保险持股占流通股比	4个小数位  %
#define S_ZLTGB_QS			103 //券商持股占流通股比	4个小数位  %
#define S_ZLTGB_XT			104 //信托持股占流通股比	4个小数位  %
#define S_RY_MGPXE			105	//近一年每股派息额	20140302
#define S_TTM_JLR			106 //TTM归属母公司净利润   4个小数位  元
#define S_TTM_ZYSR			107 //TTM主营收入

//港股的字段
#define H_SecuType			0	//证券类型   1009 港股
#define H_EndDate			1	//报告日期
#define H_SYL				2	//市盈率
#define H_SJL				3	//市净率
#define H_MGSY				4	//每股收益
#define H_MGJZC				5	//每股净资产
#define H_ZGB				6	//总股本
#define H_ZYSRTB			7	//主营收入同比
#define H_ZYLRTB			8	//主营利润同比
#define H_JLRTB				9	//净利润同比
#define H_GDQYB				10	//股东权益比
#define H_JZCSYL			11	//净资产收益率
#define H_ZCFZL				12	//资产负债率
#define H_ZZC				13	//总资产
#define H_GDZC				14	//固定资产
#define H_LDZC				15	//流动资产
#define H_ZFZ				16	//总负债
#define H_LDFZ				17	//流动负债
#define H_CQFZ				18	//长期负债
#define H_GDQY				19	//股东权益
#define H_ZYSR				20	//主营收入
#define H_ZYLR				21	//主营利润
#define H_ZLR				22	//总利润
#define H_JLR				23	//净利润
#define H_SSRQ				24	//上市日期

//基金
#define F_CLRQ				0	//成立日期
#define F_FundType			1	//基金类型
#define F_TZFG				2	//投资风格
#define F_DWJZ				3	//单位净值
#define F_LJJZ				4	//累计净值
#define F_DWJZ_ZZL			5	//单位净值增长率
#define F_LJJZ_ZZL			6	//累计净值增长率
#define F_JJFE				7	//基金份额
#define F_WFDWSY			8	//每万份单位收益
#define F_7NHSYL			9	//7日年化收益率(%)
#define F_WFSYZZL			10	//万份收益增长率(%) 
#define F_7NHSYZZL			11	//7日年化收益增长率(%) 
#define F_EndDate			12	//报告日期
#define F_QY_TZZB			13	//权益类投资占比
#define F_GP_TZZB			14	//股票投资占比
#define F_CT_TZZB			15	//存托凭证占比
#define F_JJ_TZZB			16	//基金投资占比
#define F_GD_TZZB			17	//固定收益类投资占比
#define F_ZQ_TZZB			18	//债券投资占比
#define F_ZC_TZZB			19	//资产支持证券占比
#define F_JR_TZZB			20	//金融衍生品投资占比
#define F_FS_TZZB			21	//买入返售金融资产占比
#define F_CK_TZZB			22	//银行存款和结算备付金占比
#define F_HB_TZZB			23	//货币市场工具占比
#define F_QT_TZZB			24	//其他资产占比
#define F_CBBGRQ			25	//财报报告日期
#define F_JLR_TB			26	//净利润同比(%)
#define F_ZLR_TB			27	//总利润同比(%)
#define F_ZZC_TB			28	//总资产同比(%)
#define F_ZFZ_TB			29	//总负债同比(%)
#define F_CXQSR				30	//存续起始日
#define F_CXZZQ				31	//存续终止日
#define F_JDQR				32	//据到期日(天)  为负值时显示已到期
#define F_CXQX				33	//存续期限
#define F_TGFL				34	//托管费率(%)
#define F_GLFL				35	//管理费率(%)
#define F_JJSC				36	//基金市场
#define F_JJFE_ZX			37	//最新基金份额		份 4个小数位  

//债券
#define B_ZQQX				0	//债券期限(月)
#define B_FXPL				1	//付息频率
#define B_FX_FXL			2	//发行量
#define B_FX_FXJG			3	//发行价格
#define B_FX_DWMZ			4	//单位面值
//2013-06-18 李晓娟删除
//#define B_GZ_FXL			2	//国债发行量
//#define B_GZ_FXJG			3	//国债发行价格
//#define B_GZ_DWMZ			4	//国债单位面值
//#define B_KZZ_FXL			5	//可转债发行量
//#define B_KZZ_FXJG			6	//可转债发行价格
//#define B_KZZ_DWMZ			7	//可转债单位面值
//#define B_QYZ_FXL			8	//企业债发行量
//#define B_QYZ_FXJG			9	//企业债发行价格
//#define B_QYZ_DWMZ			10	//企业债单位面值
#define B_ZGJG				11	//转股价格
#define B_PMLL				12	//票面利率（%）
#define B_FXRQ				13	//发行日期
#define B_SSRQ				14	//上市日期
#define B_QXR				15	//起息日
#define B_DQR				16	//到期日
#define B_DFR				17	//兑付日
#define B_ZGJZR				18	//转股截止日
#define B_ZQSYJYR			19	//债券剩余交易日

//CString
#define CS_Name				0	//基金名称 或 债券名称
#define CS_Abbr				1	//股票简称 / 基金简称 / 债券简称
#define CS_ZXFH				2	//最新分红
//		股票
#define CS_S_BKname			3	//所属行业
#define CS_S_Concept		4	//所属概念
#define CS_S_FAJD			5	//方案进度
#define CS_S_YJYG			6	//业绩预告
#define CS_S_YJDZ_FD		7	//业绩大增幅度
#define CS_S_YJZZ_FD		8	//业绩增长幅度
#define CS_S_YJWD_FD		9	//业绩稳定幅度
//      基金
#define CS_F_JLR			3	//基金经理
#define CS_F_GLR			4	//基金管理人
#define CS_F_TGR			5	//基金托管人
#define CS_F_YJBJJZ			6	//业绩比较基准
//      债券
#define CS_B_ZLMC			3	//债券种类名称
#define CS_B_LLLX			4	//利率类型
#define CS_B_JXFS			5	//计利方式
#define CS_B_FXFS			6	//付息方式
#define CS_B_FXR			7	//发行人
#define CS_B_NDJXJZ			8	//年度计息基准

// 数据类型
#define FPD_Type_Stock		'S'	//股票  A股或B股
#define FPD_Type_HKStock	'H'	//港股
#define FPD_Type_Fund		'F'	//基金
#define FPD_Type_Bond		'B'	//债券


class CFPlatData
{
public:
	char    m_cType;					// 类型：'S'---股票 'H'---港股 'F'---基金 'B'---债券
	long	m_lGoodsID;					// 股票代码 / 基金代码 / 债券代码
	long	m_dwUpdateDate;				// 记录修改日期
	long	m_dwUpdateTime;				// 记录修改时间
	CString m_csValue[CS_COL_TOTAL];	// 字符串字段
	XInt32	m_pxValue[PX_COL_TOTAL];	// 数值字段

public:
	CFPlatData(void);
	~CFPlatData(void);
	BOOL Read(CBuffer& buffer, long cs_max, long px_max);
	void Write(CBuffer& buffer);
	const CFPlatData& operator=(const CFPlatData&);
};

BOOL operator > (CFPlatData&, CFPlatData&);
BOOL operator == (CFPlatData&, CFPlatData&);
BOOL operator < (CFPlatData&, CFPlatData&);

