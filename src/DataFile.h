#ifndef DATAFILE_H
#define DATAFILE_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <stdio.h>
#include <unistd.h>

#include "SysGlobal.h"
#include "Mutex.h"
#include "SimpleFile.h"
#include "SortArray.h"
#include "Goods.h"
#include "GlobalData.h"

#define DATAFILE_DAY		1
#define DATAFILE_MIN1		2
#define DATAFILE_MIN5		3
#define DATAFILE_MIN30		4

#define DATAFILE_TRADE		10
#define DATAFILE_BARGAIN	11
#define DATAFILE_PARTORDER	12

const char DATAFILE_HEADER[] = "EM_DataFile";
const char DATAFILE2_HEADER[] = "EM_DataFile2";

const WORD DF_BLOCK_SIZE = 8192;		// bytes in one block
const DWORD DF2_BLOCK_SIZE = 65536;		// bytes in one block
const WORD DF_BLOCKS_GROWBY = 64;		// min file blocks appended if a size increase is necessary

const WORD DF_MAX_GOODSNUM = 21840;		// max number of goods in the file

struct CDataFileGoods
{
	DWORD m_dwGoodsID;
	DWORD m_dwNumOfData;		// 这个股票有几个数据
	DWORD m_dwFirstBlockID;		// 第一块的ID
	DWORD m_dwDataBlockID;		// 下次开始写数据的ID
	DWORD m_dwLastBlockID;		// 最后一块的ID
	DWORD m_dwLastDataIndex;	// 最后一个数据的关键值，一般是时间或TradeIndex
//	DWORD m_pdwReserved[6];
	char m_pcCode[LEN_STOCKCODE];

	CDataFileGoods()
	{
		ZeroMemory(this, sizeof(CDataFileGoods));
	}
};	// 48

const DWORD SIZEOF_DATAFILE_GOODS = sizeof(CDataFileGoods);

struct CDataFileInfo
{
	char	m_pcHeader[32];					// "EM_day format 2\0"
	DWORD	m_dwVersion;					// file version
	DWORD	m_dwTotalBlocks;				// total blocks in this file
	DWORD	m_dwUsedBlocks;					// used blocks
	DWORD	m_dwNumOfGoods;					// num of goods
	char	m_pcReserved[208];

	CDataFileInfo()
	{
		ZeroMemory(this, sizeof(CDataFileInfo));
	}
};	// 256
const DWORD SIZEOF_DATAFILE_INFO = sizeof(CDataFileInfo);

struct CDataFileHead
{
	CDataFileInfo	m_dfi;
	CDataFileGoods	m_pDFG[DF_MAX_GOODSNUM];

	CDataFileHead()
	{
		ZeroMemory(this, sizeof(CDataFileHead));
	}
};	// 128*8192 ; MAX_GOODSNUM = (128*8192-256)/48=21840

const DWORD SIZEOF_DATAFILE_HEAD = sizeof(CDataFileHead);
// number of blocks for the file head
const DWORD DATAFILE_HEAD_BLOCKS = (SIZEOF_DATAFILE_HEAD+DF_BLOCK_SIZE-1)/DF_BLOCK_SIZE;	// 64
const DWORD DATAFILE2_HEAD_BLOCKS = (SIZEOF_DATAFILE_HEAD+DF2_BLOCK_SIZE-1)/DF2_BLOCK_SIZE;	// 64

// length of file head
const DWORD DATAFILE_HEAD_LEN = DATAFILE_HEAD_BLOCKS*DF_BLOCK_SIZE;
const DWORD DATAFILE2_HEAD_LEN = DATAFILE2_HEAD_BLOCKS*DF2_BLOCK_SIZE;


struct CGoodsIndex
{
	CGoodsCode m_gcGoodsID;
	DWORD m_wIndex;

	CGoodsIndex()
	{
		m_wIndex = 0;
	}
};

inline BOOL operator > (CGoodsIndex& first, CGoodsIndex& second)
{
	return first.m_gcGoodsID > second.m_gcGoodsID;
}

inline BOOL operator == (CGoodsIndex& first, CGoodsIndex& second)
{
	return first.m_gcGoodsID == second.m_gcGoodsID;
}

inline BOOL operator < (CGoodsIndex& first, CGoodsIndex& second)
{
	return first.m_gcGoodsID < second.m_gcGoodsID;
}

typedef CSortArray<CGoodsIndex> CGoodsIndexArray;

extern size_t LogErr(const char* format_str, ...);

//////////////////////////////////////////////////////////////////////////

template <class TYPE> class CDataFile
{
public:
	CDataFile();
	virtual ~CDataFile();

	void Initial(CString strFile);
	void Clear();

	int LoadData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData);
	int SaveData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData, DWORD dwLastDataIndex, BOOL bAppend);

	int LoadData(const CGoodsCode& gcGoodsID, TYPE*& pData, DWORD& dwData);
	int SaveData(const CGoodsCode& gcGoodsID, TYPE* pData, DWORD dwData, DWORD dwLastDataIndex, BOOL bAppend);

	int LoadData(const CGoodsCode& gcGoodsID, CGlobalData<TYPE>& data);

	int ClearData(const CGoodsCode& gcGoodsID);

	// 需要从外部解析文件，private改成public [11/7/2012 frantqian]
public:
	int ReadHead();
	DWORD AddNewBlock(UINT64& hFileLength);
	int GetNumOfData(const CGoodsCode& gcGoodsID, WORD& wIndex);
	int LoadData(WORD wIndex, TYPE* pData, DWORD dwData);

protected:
	
	UINT64 GetBlockOffset(DWORD dwBlockID);
	FastMutex m_mutex;

	WORD m_wSizeofData;		// 每个数据的大小
	DWORD m_dwDataPerBlock;	// 每个块最多有几块数据

	
	CGoodsIndexArray m_aGoodsIndex;
public:
	CDataFileHead m_dfh;
public:
	UINT64 GetFileLength();

private:
	FILE* m_pFile;
	CString m_strFile;
};

template <class TYPE> CDataFile<TYPE>::CDataFile()
{
	m_wSizeofData = sizeof(TYPE);
	m_dwDataPerBlock = (DF_BLOCK_SIZE-4)/m_wSizeofData;

	m_pFile = NULL;
}

template <class TYPE> CDataFile<TYPE>::~CDataFile()
{
	try
	{
		if (m_pFile!=NULL)
			fclose(m_pFile);
	}
	catch(...)
	{
	}

	m_pFile = NULL;

	m_aGoodsIndex.RemoveAll();
}

template <class TYPE> void CDataFile<TYPE>::Initial(CString strFile)
{
	m_strFile = strFile;
	
	Mutex_scope_lock scope_lock(m_mutex);

	ReadHead();
}

template <class TYPE> UINT64 CDataFile<TYPE>::GetBlockOffset(DWORD dwBlockID)
{
	UINT64 hOffset = dwBlockID;
	hOffset = hOffset * DF_BLOCK_SIZE;
	return 	hOffset;
}


template <class TYPE> int CDataFile<TYPE>::ReadHead()
{
	try
	{
		if (m_pFile!=NULL)
			fclose(m_pFile);
	}
	catch(...)
	{
	}

	m_pFile = NULL;

	m_aGoodsIndex.RemoveAll();
	ZeroMemory(&m_dfh, SIZEOF_DATAFILE_HEAD);

	BOOL bReadOK = FALSE;

	try
	{
		m_pFile = fopen(m_strFile, "r+");
		if (m_pFile!=NULL)
		{
			UINT64 dwFileLength = GetFileLength();

			if (dwFileLength>=SIZEOF_DATAFILE_HEAD)
			{
				fread(&m_dfh, SIZEOF_DATAFILE_HEAD, 1, m_pFile);

				bReadOK = TRUE;
			}
			else
				fclose(m_pFile);
		}

		if (bReadOK==FALSE)
		{
			m_pFile = fopen(m_strFile, "w+");
			if (m_pFile!=NULL)
			{
				strcpy(m_dfh.m_dfi.m_pcHeader, DATAFILE_HEADER);
				m_dfh.m_dfi.m_dwVersion = 1;
				m_dfh.m_dfi.m_dwTotalBlocks = DATAFILE_HEAD_BLOCKS;
				m_dfh.m_dfi.m_dwUsedBlocks = DATAFILE_HEAD_BLOCKS;

				fwrite(&m_dfh, SIZEOF_DATAFILE_HEAD, 1, m_pFile);
				fclose(m_pFile);
				m_pFile=NULL;

				bReadOK = TRUE;

				m_pFile = fopen(m_strFile, "r+");
			}
		}
	}
	catch(...)
	{
	}

	if (bReadOK)
	{
		CGoodsIndex gi;
		for (DWORD dw=0; dw<m_dfh.m_dfi.m_dwNumOfGoods; dw++)
		{
			gi.m_gcGoodsID = CGoodsCode(m_dfh.m_pDFG[dw].m_dwGoodsID, m_dfh.m_pDFG[dw].m_pcCode);
			gi.m_wIndex = (WORD)dw;

			m_aGoodsIndex.Add(gi);
		}

		return 0;
	}

	return -1;
}

template <class TYPE> void CDataFile<TYPE>::Clear()
{
	Mutex_scope_lock scope_lock(m_mutex);

	if (m_pFile==NULL)
		ReadHead();

	m_aGoodsIndex.RemoveAll();
	ZeroMemory(&m_dfh, SIZEOF_DATAFILE_HEAD);

	strcpy(m_dfh.m_dfi.m_pcHeader, DATAFILE_HEADER);
	m_dfh.m_dfi.m_dwVersion = 1;
	m_dfh.m_dfi.m_dwTotalBlocks = DATAFILE_HEAD_BLOCKS;
	m_dfh.m_dfi.m_dwUsedBlocks = DATAFILE_HEAD_BLOCKS;

	try
	{
		if (m_pFile!=NULL)
		{
			ftruncate(fileno(m_pFile), 0);
			fseek(m_pFile, 0, SEEK_SET);
			fwrite(&m_dfh, SIZEOF_DATAFILE_HEAD, 1, m_pFile);
		}
	}
	catch(...)
	{
	}
}

template <class TYPE> int CDataFile<TYPE>::GetNumOfData(const CGoodsCode& gcGoodsID, WORD& wIndex)
{
	wIndex = 0;

	if (m_pFile==NULL)
		ReadHead();

	CGoodsIndex gi;	
	gi.m_gcGoodsID = gcGoodsID;

	int nPos = m_aGoodsIndex.Find(gi);

	if (nPos<0)	// goods not found
	{
		return -1;
	}

	wIndex = m_aGoodsIndex[nPos].m_wIndex;

	if (m_pFile!=NULL)
		return 0;
	else
		return -1;
}

template <class TYPE> int CDataFile<TYPE>::LoadData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData)
{
	aData.RemoveAt(0, aData.GetSize());
	DWORD dwData = 0;

	BOOL bReadOK = FALSE;

	do 
	{
		Mutex_scope_lock scope_lock(m_mutex);
		WORD wIndex;
		if (GetNumOfData(gcGoodsID, wIndex)==0)
		{
			dwData = m_dfh.m_pDFG[wIndex].m_dwNumOfData;
			aData.SetSize(dwData);

			if (LoadData(wIndex, aData.m_pData, dwData)==0)
				bReadOK = TRUE;
		}
	} while (0);
	
	if (bReadOK==FALSE)
	{
		aData.RemoveAll();
	
		return -1;
	}
	else
		return 0;
}

template <class TYPE> int CDataFile<TYPE>::LoadData(const CGoodsCode& gcGoodsID, TYPE*& pData, DWORD& dwData)
{
	if (pData)
		delete [] pData;
	pData = NULL;
	dwData = 0;

	BOOL bReadOK = FALSE;

	do 
	{
		Mutex_scope_lock scope_lock(m_mutex);
		WORD wIndex;
		if (GetNumOfData(gcGoodsID, wIndex)==0)
		{
			dwData = m_dfh.m_pDFG[wIndex].m_dwNumOfData;
			pData = (TYPE*)new(std::nothrow) BYTE[(dwData*m_wSizeofData+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE];
			if (pData)
				memset(pData, 0, (dwData*m_wSizeofData+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE);

			if (LoadData(wIndex, pData, dwData)==0)
				bReadOK = TRUE;
		}
	} while (0);
	
	if (bReadOK==FALSE)
	{
		if (pData)
			delete [] pData;
		pData = NULL;
		dwData = 0;
	
		return -1;
	}
	else
		return 0;
}

template <class TYPE> int CDataFile<TYPE>::LoadData(WORD wIndex, TYPE* pData, DWORD dwData)
{
	if (dwData==0)
		return 0;
	if (pData==NULL)
		return -1;

	BOOL bReadOK = FALSE;

	if (m_pFile!=NULL)
	{
		try
		{
			UINT64 dwFileLength = GetFileLength();

			DWORD dwDataToRead = dwData;
			TYPE* pDataToRead = pData;

			DWORD dwBlockID = m_dfh.m_pDFG[wIndex].m_dwFirstBlockID;
			while (dwDataToRead>0)
			{
				if (dwBlockID==0)
					break;

				DWORD dwRead = dwDataToRead;
				if (dwRead>m_dwDataPerBlock)
					dwRead = m_dwDataPerBlock;

				DWORD dwReadLength = dwRead*m_wSizeofData;

				UINT64 dwOffset = GetBlockOffset(dwBlockID);//((UINT64)dwBlockID)*DF_BLOCK_SIZE;
				if (dwFileLength < dwOffset+DF_BLOCK_SIZE)
					break;

				//get next block id to prepare next to read next block
				DWORD dwNextBlockID;

				fseek(m_pFile, dwOffset, SEEK_SET);
				fread(&dwNextBlockID, 4, 1, m_pFile);

				if (dwNextBlockID>m_dfh.m_pDFG[wIndex].m_dwLastBlockID)
					break;

				//read data from this block
				fread(pDataToRead, 1, dwReadLength, m_pFile);

				dwDataToRead -= dwRead;
				pDataToRead += dwRead;

				dwBlockID = dwNextBlockID;
			}

			if (dwDataToRead==0)
			{
				bReadOK = TRUE;
			}
		}
		catch(...)
		{
		}		
	}

	if (bReadOK==FALSE)
		return -1;
	else
		return 0;
}


template <class TYPE> int CDataFile<TYPE>::LoadData(const CGoodsCode& gcGoodsID, CGlobalData<TYPE>& data)
{
	data.m_dwSize = 0;

	BOOL bReadOK = FALSE;

	Mutex_scope_lock scope_lock(m_mutex);

	WORD wIndex;
	if (GetNumOfData(gcGoodsID, wIndex)==0)
	{
		DWORD dwData = m_dfh.m_pDFG[wIndex].m_dwNumOfData;
		data.Alloc(dwData);

		if (LoadData(wIndex, data.GetBuffer(), dwData)==0)
		{
			data.m_dwSize = dwData;
			bReadOK = TRUE;
		}
	}

	if (bReadOK==FALSE)
		return -1;
	else
		return 0;
}

template <class TYPE> int CDataFile<TYPE>::SaveData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData, DWORD dwLastDataIndex, BOOL bAppend)
{
	return SaveData(gcGoodsID, aData.GetData(), DWORD(aData.GetSize()), dwLastDataIndex, bAppend);
}

template <class TYPE> int CDataFile<TYPE>::SaveData(const CGoodsCode& gcGoodsID, TYPE* pData, DWORD dwData, DWORD dwLastDataIndex, BOOL bAppend)
{
//	if (dwData==0 || pData==NULL)
//		return 0;

	Mutex_scope_lock scope_lock(m_mutex);

	if (m_pFile==NULL)
		ReadHead();

	WORD wIndex = 0;
	BOOL bNewGoods = FALSE;

	CGoodsIndex gi;
	gi.m_gcGoodsID = gcGoodsID;

	int nPos = m_aGoodsIndex.Find(gi);
	if (nPos<0)	// goods not found
	{
		bNewGoods = TRUE;

		wIndex = (WORD)m_aGoodsIndex.GetSize();
		if (wIndex>=DF_MAX_GOODSNUM)
		{
			return -1;
		}
	}
	else	// goods exists
	{
		wIndex = m_aGoodsIndex[nPos].m_wIndex;
	}

	BOOL bWriteOK = FALSE;

	if (m_pFile!=NULL)
	{
		try
		{
			UINT64 dwFileLength = GetFileLength();

			BOOL bFileInfoChanged = FALSE;

			DWORD dwDataToWrite = dwData;
			TYPE* pDataToWrite = pData;

			DWORD dwBlockID = 0;
			DWORD dwDataExist = 0;

			DWORD dwNextBlockID = 0;

			if (m_dfh.m_pDFG[wIndex].m_dwNumOfData==0 || bAppend==FALSE)
			{
				dwBlockID = m_dfh.m_pDFG[wIndex].m_dwFirstBlockID;
				if (dwBlockID==0)
				{
					dwBlockID = AddNewBlock(dwFileLength);
					dwNextBlockID = 0;

					bFileInfoChanged = TRUE;

					m_dfh.m_pDFG[wIndex].m_dwFirstBlockID = dwBlockID;
					m_dfh.m_pDFG[wIndex].m_dwLastBlockID = dwBlockID;
				}
				else
				{
					UINT64 dwOffset = GetBlockOffset(dwBlockID);//((UINT64)dwBlockID)*DF_BLOCK_SIZE;
					//ASSERT(dwFileLength >= dwOffset+DF_BLOCK_SIZE);

					fseek(m_pFile, dwOffset, SEEK_SET);
					fread(&dwNextBlockID, 4, 1, m_pFile);
				}

				dwDataExist = 0;
			}
			else
			{
				dwBlockID = m_dfh.m_pDFG[wIndex].m_dwDataBlockID;
				
				if (dwBlockID==m_dfh.m_pDFG[wIndex].m_dwLastBlockID)
					dwNextBlockID = 0;
				else
				{
					UINT64 dwOffset = GetBlockOffset(dwBlockID);//((UINT64)dwBlockID)*DF_BLOCK_SIZE;
					//ASSERT(dwFileLength >= dwOffset+DF_BLOCK_SIZE);

					fseek(m_pFile, dwOffset, SEEK_SET);
					fread(&dwNextBlockID, 4, 1, m_pFile);
				}

				dwDataExist = m_dfh.m_pDFG[wIndex].m_dwNumOfData%m_dwDataPerBlock;
			}

			while (dwDataToWrite>0)
			{
				//ASSERT(dwBlockID);
				if (dwBlockID==0){
					::LogErr("SaveData dwBlockID==0");
					break;
				}

				UINT64 dwOffset = GetBlockOffset(dwBlockID);
				//ASSERT(hFileLength>=hOffset+DF2_BLOCK_SIZE);
				if (dwFileLength < dwOffset+DF_BLOCK_SIZE){
					UINT64 offset = GetBlockOffset(dwBlockID);
					::LogErr("SaveData hFileLength:%d < hOffset:%d+DF_BLOCK_SIZE, dwBlockID:%d %d", 
						dwFileLength, dwOffset, dwBlockID, offset);
					::LogErr("file:%s", m_strFile.GetData());
					break;
				}
				
				BOOL bNewBlock = FALSE;

				if (dwNextBlockID==0)
				{
					dwNextBlockID = AddNewBlock(dwFileLength);
					bNewBlock = TRUE;

					bFileInfoChanged = TRUE;

					m_dfh.m_pDFG[wIndex].m_dwLastBlockID = dwNextBlockID;

					fseek(m_pFile, dwOffset, SEEK_SET);
					fwrite(&dwNextBlockID, 4, 1, m_pFile);
				}
				else
				{
					fseek(m_pFile, dwOffset, SEEK_SET);

					DWORD dwID;
					fread(&dwID, 4, 1, m_pFile);

					//ASSERT(dwID==dwNextBlockID);
				}

				m_dfh.m_pDFG[wIndex].m_dwDataBlockID = dwBlockID;

				DWORD dwWrite = dwDataToWrite;
				DWORD dwMaxWrite = m_dwDataPerBlock-dwDataExist;
				if (dwWrite>dwMaxWrite)
					dwWrite = dwMaxWrite;

				DWORD dwWriteLength = dwWrite*m_wSizeofData;

				if (dwDataExist>0)
					fseek(m_pFile, dwDataExist*m_wSizeofData, SEEK_CUR);
				fwrite(pDataToWrite, 1, dwWriteLength, m_pFile);

				dwDataToWrite -= dwWrite;
				pDataToWrite += dwWrite;

				dwBlockID = dwNextBlockID;
				dwDataExist = 0;
				
				//ASSERT(dwBlockID<=m_dfh.m_pDFG[wIndex].m_dwLastBlockID);

				if (bNewBlock==FALSE && dwBlockID<m_dfh.m_pDFG[wIndex].m_dwLastBlockID)
				{
					UINT64 dwOffset = GetBlockOffset(dwBlockID);//((UINT64)dwBlockID)*DF_BLOCK_SIZE;
					//ASSERT(dwFileLength >= dwOffset+DF_BLOCK_SIZE);

					fseek(m_pFile, dwOffset, SEEK_SET);
					fread(&dwNextBlockID, 4, 1, m_pFile);
				}
				else
					dwNextBlockID = 0;

				if (dwDataToWrite==0 && dwWrite==dwMaxWrite)
					m_dfh.m_pDFG[wIndex].m_dwDataBlockID = dwBlockID;
			}

			if (dwDataToWrite==0)
			{
				if (bNewGoods)  // add m_dwNumOfGoods and sort index
				{
					m_dfh.m_dfi.m_dwNumOfGoods++;
					bFileInfoChanged = TRUE;

					gi.m_gcGoodsID = gcGoodsID;
					gi.m_wIndex = wIndex;

					m_aGoodsIndex.Add(gi);
				}

				if (bFileInfoChanged)
				{
					fseek(m_pFile, 0, SEEK_SET);
					fwrite(&(m_dfh.m_dfi), SIZEOF_DATAFILE_INFO, 1, m_pFile);
				}

				m_dfh.m_pDFG[wIndex].m_dwGoodsID = gcGoodsID.m_dwGoodsID;
				CopyMemory(m_dfh.m_pDFG[wIndex].m_pcCode, gcGoodsID.m_pcCode, LEN_STOCKCODE);
				if (bAppend)
					m_dfh.m_pDFG[wIndex].m_dwNumOfData += dwData;
				else
					m_dfh.m_pDFG[wIndex].m_dwNumOfData = dwData;
				m_dfh.m_pDFG[wIndex].m_dwLastDataIndex = dwLastDataIndex;

				DWORD dwIndexOffset = SIZEOF_DATAFILE_INFO+wIndex*SIZEOF_DATAFILE_GOODS;

				fseek(m_pFile, dwIndexOffset, SEEK_SET);
				fwrite(m_dfh.m_pDFG+wIndex, SIZEOF_DATAFILE_GOODS, 1, m_pFile);

				bWriteOK = TRUE;
			}
		}
		catch(...)
		{
		}
	}

	if (bWriteOK)
		return 0;

	return -1;
}

template <class TYPE> int CDataFile<TYPE>::ClearData(const CGoodsCode& gcGoodsID)
{
	Mutex_scope_lock scope_lock(m_mutex);

	if (m_pFile==NULL)
		ReadHead();

	WORD wIndex = 0;
	BOOL bNewGoods = FALSE;

	CGoodsIndex gi;
	gi.m_gcGoodsID = gcGoodsID;

	int nPos = m_aGoodsIndex.Find(gi);
	if (nPos<0)	// goods not found
	{
		bNewGoods = TRUE;

		wIndex = (WORD)m_aGoodsIndex.GetSize();
		if (wIndex>=DF_MAX_GOODSNUM)
		{
			return -1;
		}
	}
	else	// goods exists
	{
		wIndex = m_aGoodsIndex[nPos].m_wIndex;
	}

	BOOL bWriteOK = FALSE;

	if (m_pFile!=NULL)
	{
		try
		{
			if (bNewGoods)  // add m_dwNumOfGoods and sort index
			{
				m_dfh.m_dfi.m_dwNumOfGoods++;

				fseek(m_pFile, 0, SEEK_SET);
				fwrite(&(m_dfh.m_dfi), SIZEOF_DATAFILE_INFO, 1, m_pFile);

				gi.m_gcGoodsID = gcGoodsID;
				gi.m_wIndex = wIndex;

				m_aGoodsIndex.Add(gi);
			}

			//ASSERT(m_dfh.m_pDFG[wIndex].m_dwGoodsID==dwGoodsID || m_dfh.m_pDFG[wIndex].m_dwGoodsID==0);

			CGoodsCode gcFile = CGoodsCode(m_dfh.m_pDFG[wIndex].m_dwGoodsID, m_dfh.m_pDFG[wIndex].m_pcCode);
			if (m_dfh.m_pDFG[wIndex].m_dwNumOfData!=0 || gcFile!=gcGoodsID)
			{
				m_dfh.m_pDFG[wIndex].m_dwNumOfData = 0;
				m_dfh.m_pDFG[wIndex].m_dwLastDataIndex = 0;
				
				m_dfh.m_pDFG[wIndex].m_dwGoodsID = gcGoodsID.m_dwGoodsID;
				CopyMemory(m_dfh.m_pDFG[wIndex].m_pcCode, gcGoodsID.m_pcCode, LEN_STOCKCODE);

				DWORD dwIndexOffset = SIZEOF_DATAFILE_INFO+wIndex*SIZEOF_DATAFILE_GOODS;

				fseek(m_pFile, dwIndexOffset, SEEK_SET);
				fwrite(m_dfh.m_pDFG+wIndex, SIZEOF_DATAFILE_GOODS, 1, m_pFile);
			}

			bWriteOK = TRUE;
		}
		catch(...)
		{
		}
	}

	if (bWriteOK)
		return 0;

	return -1;
}

template <class TYPE> DWORD CDataFile<TYPE>::AddNewBlock(UINT64& hFileLength)
{
	if (m_dfh.m_dfi.m_dwUsedBlocks>=m_dfh.m_dfi.m_dwTotalBlocks)	// increase data blocks
	{
		DWORD totalBlocks = m_dfh.m_dfi.m_dwTotalBlocks+DF_BLOCKS_GROWBY;
		hFileLength = GetBlockOffset(totalBlocks);
		ftruncate(fileno(m_pFile), hFileLength);

		m_dfh.m_dfi.m_dwTotalBlocks = totalBlocks;
	}

	DWORD dwBlockID = m_dfh.m_dfi.m_dwUsedBlocks++;

	return dwBlockID;
}

template <class TYPE> UINT64 CDataFile<TYPE>::GetFileLength()
{
	if (m_pFile == NULL)
		return 0;
	return SimpleFile::GetFileSize(m_strFile.GetData());
}

//////////////////////////////////////////////////////////////////////////
inline UINT64 UInt32x32To64(DWORD a, DWORD b){
	UINT64 t1 = (UINT64)a;
	UINT64 t2 = (UINT64)b;
	t1 = t1*t2;
	return t1;
}

template <class TYPE> class CDataFile2
{
public:
	CDataFile2();
	virtual ~CDataFile2();

	void Initial(CString strFile);
	void Clear();

	int LoadData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData);
	int SaveData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData, DWORD dwLastDataIndex, BOOL bAppend);

	int LoadData(const CGoodsCode& gcGoodsID, TYPE*& pData, DWORD& dwData);
	int SaveData(const CGoodsCode& gcGoodsID, TYPE* pData, DWORD dwData, DWORD dwLastDataIndex, BOOL bAppend);

	//int LoadData(DWORD dwGoodsID, CGlobalData<TYPE>& data);

	int ClearData(const CGoodsCode& gcGoodsID);

private:
	UINT64 GetBlockOffset(DWORD dwBlockID);
	int ReadHead();
	DWORD AddNewBlock(UINT64& hFileLength);
	int GetNumOfData(const CGoodsCode& gcGoodsID, WORD& wIndex);
	int LoadData(WORD wIndex, TYPE* pData, DWORD dwData);

public:
	FastMutex m_mutex;

	WORD m_wSizeofData;		// 每个数据的大小
	DWORD m_dwDataPerBlock;	// 每个块最多有几块数据

	CDataFileHead m_dfh;
	CGoodsIndexArray m_aGoodsIndex;
public:
	UINT64 GetFileLength();

private:
	FILE* m_pFile;
	CString m_strFile;
};

template <class TYPE> CDataFile2<TYPE>::CDataFile2()
{
	m_wSizeofData = sizeof(TYPE);
	m_dwDataPerBlock = (DF2_BLOCK_SIZE-4)/m_wSizeofData;

	m_pFile = NULL;
}

template <class TYPE> CDataFile2<TYPE>::~CDataFile2()
{
	try
	{
		if (m_pFile!=NULL)
			fclose(m_pFile);
	}
	catch(...)
	{
	}

	m_pFile = NULL;

	m_aGoodsIndex.RemoveAll();
}

template <class TYPE> void CDataFile2<TYPE>::Initial(CString strFile)
{
	m_strFile = strFile;

	Mutex_scope_lock scope_lock(m_mutex);

	ReadHead();
}

template <class TYPE> UINT64 CDataFile2<TYPE>::GetBlockOffset(DWORD dwBlockID)
{
	UINT64 hOffset = UInt32x32To64(dwBlockID, DF2_BLOCK_SIZE);
	return 	hOffset;
}


template <class TYPE> int CDataFile2<TYPE>::ReadHead()
{
	try
	{
		if (m_pFile!=NULL)
			fclose(m_pFile);
	}
	catch(...)
	{
	}

	m_pFile = NULL;

	m_aGoodsIndex.RemoveAll();
	ZeroMemory(&m_dfh, SIZEOF_DATAFILE_HEAD);

	BOOL bReadOK = FALSE;

	try
	{
		m_pFile = fopen(m_strFile, "r+");
		if (m_pFile!=NULL)
		{
			UINT64 hFileLength = GetFileLength();

			if (hFileLength>=SIZEOF_DATAFILE_HEAD)
			{
				fread(&m_dfh, SIZEOF_DATAFILE_HEAD, 1, m_pFile);

				bReadOK = TRUE;
			}
			else
				fclose(m_pFile);
		}

		if (bReadOK==FALSE)
		{
			m_pFile = fopen(m_strFile, "w+");
			if (m_pFile!=NULL)
			{
				strcpy(m_dfh.m_dfi.m_pcHeader, DATAFILE2_HEADER);
				m_dfh.m_dfi.m_dwVersion = 1;
				m_dfh.m_dfi.m_dwTotalBlocks = DATAFILE2_HEAD_BLOCKS;
				m_dfh.m_dfi.m_dwUsedBlocks = DATAFILE2_HEAD_BLOCKS;

				fwrite(&m_dfh, SIZEOF_DATAFILE_HEAD, 1, m_pFile);
				fclose(m_pFile);
				m_pFile=NULL;

				bReadOK = TRUE;

				m_pFile = fopen(m_strFile, "r+");
			}
		}
	}
	catch(...)
	{
	}

	if (bReadOK)
	{
		CGoodsIndex gi;
		for (DWORD dw=0; dw<m_dfh.m_dfi.m_dwNumOfGoods; dw++)
		{
			gi.m_gcGoodsID = CGoodsCode(m_dfh.m_pDFG[dw].m_dwGoodsID, m_dfh.m_pDFG[dw].m_pcCode);
			gi.m_wIndex = dw;

			m_aGoodsIndex.Add(gi);
		}

		return 0;
	}

	return -1;
}

template <class TYPE> void CDataFile2<TYPE>::Clear()
{
	Mutex_scope_lock scope_lock(m_mutex);

	if (m_pFile == NULL)
		ReadHead();

	m_aGoodsIndex.RemoveAll();
	ZeroMemory(&m_dfh, SIZEOF_DATAFILE_HEAD);

	strcpy(m_dfh.m_dfi.m_pcHeader, DATAFILE2_HEADER);
	m_dfh.m_dfi.m_dwVersion = 1;
	m_dfh.m_dfi.m_dwTotalBlocks = DATAFILE2_HEAD_BLOCKS;
	m_dfh.m_dfi.m_dwUsedBlocks = DATAFILE2_HEAD_BLOCKS;

	try
	{
		if (m_pFile!=NULL)
		{
			ftruncate(fileno(m_pFile), 0);
			fseeko(m_pFile, 0, SEEK_SET);
			fwrite(&m_dfh, SIZEOF_DATAFILE_HEAD, 1, m_pFile);
		}
	}
	catch(...)
	{
	}
}

template <class TYPE> int CDataFile2<TYPE>::GetNumOfData(const CGoodsCode& gcGoodsID, WORD& wIndex)
{
	wIndex = 0;

	if (m_pFile==NULL)
		ReadHead();

	CGoodsIndex gi;	
	gi.m_gcGoodsID = gcGoodsID;

	int nPos = m_aGoodsIndex.Find(gi);

	if (nPos<0)	// goods not found
	{
		return -1;
	}

	wIndex = m_aGoodsIndex[nPos].m_wIndex;

	if (m_pFile!=NULL)
		return 0;
	else
		return -1;
}

template <class TYPE> int CDataFile2<TYPE>::LoadData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData)
{
	aData.RemoveAt(0, aData.GetSize());
	DWORD dwData = 0;

	BOOL bReadOK = FALSE;

	do 
	{
		Mutex_scope_lock scope_lock(m_mutex);
		WORD wIndex;
		if (GetNumOfData(gcGoodsID, wIndex)==0)
		{
			dwData = m_dfh.m_pDFG[wIndex].m_dwNumOfData;
			aData.SetSize(dwData);

			if (LoadData(wIndex, aData.GetData(), dwData)==0)
				bReadOK = TRUE;
		}
	} while (0);
	
	if (bReadOK==FALSE)
	{
		aData.RemoveAll();

		return -1;
	}
	else
		return 0;
}

template <class TYPE> int CDataFile2<TYPE>::LoadData(const CGoodsCode& gcGoodsID, TYPE*& pData, DWORD& dwData)
{
	if (pData)
		delete [] pData;
	pData = NULL;
	dwData = 0;

	BOOL bReadOK = FALSE;

	do 
	{
		Mutex_scope_lock scope_lock(m_mutex);

		WORD wIndex;
		if (GetNumOfData(gcGoodsID, wIndex)==0)
		{
			dwData = m_dfh.m_pDFG[wIndex].m_dwNumOfData;
			pData = (TYPE*)new(std::nothrow) BYTE[(dwData*m_wSizeofData+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE];
			if (pData)
				memset(pData, 0, (dwData*m_wSizeofData+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE);

			if (LoadData(wIndex, pData, dwData)==0)
				bReadOK = TRUE;
		}
	}while(0);

	if (bReadOK==FALSE)
	{
		if (pData)
			delete [] pData;
		pData = NULL;
		dwData = 0;

		return -1;
	}
	else
		return 0;
}

template <class TYPE> int CDataFile2<TYPE>::LoadData(WORD wIndex, TYPE* pData, DWORD dwData)
{
	if (dwData==0)
		return 0;
	if (pData==NULL)
		return -1;

	BOOL bReadOK = FALSE;

	if (m_pFile!=NULL)
	{
		try
		{
			UINT64 hFileLength = GetFileLength();

			//DWORD dwFirstRead = 0;
			//DWORD dwFirstBlock = 0;

			DWORD dwDataToRead = dwData;
			TYPE* pDataToRead = pData;

			DWORD dwBlockID = m_dfh.m_pDFG[wIndex].m_dwFirstBlockID;
			while (dwDataToRead>0)
			{
				if (dwBlockID==0)
					break;

				DWORD dwRead = dwDataToRead;
				if (dwRead>m_dwDataPerBlock)
					dwRead = m_dwDataPerBlock;

				DWORD dwReadLength = dwRead*m_wSizeofData;

				UINT64 hOffset =  GetBlockOffset(dwBlockID);
				if (hFileLength < hOffset+DF2_BLOCK_SIZE)
					break;

				DWORD dwNextBlockID;

				fseeko(m_pFile, hOffset, SEEK_SET);
				fread(&dwNextBlockID, 4, 1, m_pFile);

				if (dwNextBlockID>m_dfh.m_pDFG[wIndex].m_dwLastBlockID)
					break;

				//if (dwNextBlockID==0)
				//	int iii = 0;

				fread(pDataToRead, 1, dwReadLength, m_pFile);

				dwDataToRead -= dwRead;
				pDataToRead += dwRead;

				dwBlockID = dwNextBlockID;
			}

			if (dwDataToRead==0)
			{
				bReadOK = TRUE;
			}
		}
		catch(...)
		{
		}
	}

	if (bReadOK==FALSE)
		return -1;
	else
		return 0;
}

template <class TYPE> int CDataFile2<TYPE>::SaveData(const CGoodsCode& gcGoodsID, CArray<TYPE, TYPE&>& aData, DWORD dwLastDataIndex, BOOL bAppend)
{
	return SaveData(gcGoodsID, aData.GetData(), DWORD(aData.GetSize()), dwLastDataIndex, bAppend);
}


template <class TYPE> int CDataFile2<TYPE>::SaveData(const CGoodsCode& gcGoodsID, TYPE* pData, DWORD dwData, DWORD dwLastDataIndex, BOOL bAppend)
{
	//	if (dwData==0 || pData==NULL)
	//		return 0;

	Mutex_scope_lock scope_lock(m_mutex);

	if (m_pFile==NULL)
		ReadHead();

	DWORD wIndex = 0;
	BOOL bNewGoods = FALSE;

	CGoodsIndex gi;
	gi.m_gcGoodsID = gcGoodsID;


	int nPos = m_aGoodsIndex.Find(gi);
	if (nPos<0)	// goods not found
	{
		bNewGoods = TRUE;

		wIndex = (DWORD)m_aGoodsIndex.GetSize();
		if (wIndex>=DF_MAX_GOODSNUM)
		{
			return -1;
		}
	}
	else	// goods exists
	{
		wIndex = m_aGoodsIndex[nPos].m_wIndex;
	}

	BOOL bWriteOK = FALSE;

	if (m_pFile!=NULL)
	{
		try
		{
			UINT64 hFileLength = GetFileLength();

			BOOL bFileInfoChanged = FALSE;

			DWORD dwDataToWrite = dwData;
			TYPE* pDataToWrite = pData;

			DWORD dwBlockID = 0;
			DWORD dwDataExist = 0;

			DWORD dwNextBlockID = 0;

			char code[64];
			memset(code, 0, 64);
			memcpy(code, m_dfh.m_pDFG[wIndex].m_pcCode, LEN_STOCKCODE);

			if (m_dfh.m_pDFG[wIndex].m_dwNumOfData==0 || bAppend==FALSE)
			{
				dwBlockID = m_dfh.m_pDFG[wIndex].m_dwFirstBlockID;
				if (dwBlockID==0)
				{
					dwBlockID = AddNewBlock(hFileLength);
					dwNextBlockID = 0;

					bFileInfoChanged = TRUE;

					m_dfh.m_pDFG[wIndex].m_dwFirstBlockID = dwBlockID;
					m_dfh.m_pDFG[wIndex].m_dwLastBlockID = dwBlockID;
				}
				else
				{
					UINT64 hOffset = GetBlockOffset(dwBlockID);
					//ASSERT(hFileLength >= hOffset+DF2_BLOCK_SIZE);

					fseeko(m_pFile, hOffset, SEEK_SET);
					fread(&dwNextBlockID, 4, 1, m_pFile);
				}

				dwDataExist = 0;
			}
			else
			{
				dwBlockID = m_dfh.m_pDFG[wIndex].m_dwDataBlockID;

				if (dwBlockID==m_dfh.m_pDFG[wIndex].m_dwLastBlockID)
					dwNextBlockID = 0;
				else
				{
					UINT64 hOffset = GetBlockOffset(dwBlockID);
					//ASSERT(hFileLength >= hOffset+DF2_BLOCK_SIZE);

					fseeko(m_pFile, hOffset, SEEK_SET);
					fread(&dwNextBlockID, 4, 1, m_pFile);
				}

				dwDataExist = m_dfh.m_pDFG[wIndex].m_dwNumOfData%m_dwDataPerBlock;
			}

			while (dwDataToWrite>0)
			{
				//ASSERT(dwBlockID);
				if (dwBlockID==0){
					break;
				}

				UINT64 hOffset = GetBlockOffset(dwBlockID);
				//ASSERT(hFileLength>=hOffset+DF2_BLOCK_SIZE);
				if (hFileLength < hOffset+DF2_BLOCK_SIZE){
					break;
				}

				BOOL bNewBlock = FALSE;

				if (dwNextBlockID==0)
				{
					dwNextBlockID = AddNewBlock(hFileLength);
					bNewBlock = TRUE;

					bFileInfoChanged = TRUE;

					m_dfh.m_pDFG[wIndex].m_dwLastBlockID = dwNextBlockID;

					fseeko(m_pFile, hOffset, SEEK_SET);
					fwrite(&dwNextBlockID, 4, 1, m_pFile);
				}
				else
				{
					fseeko(m_pFile, hOffset, SEEK_SET);

					DWORD dwID;
					fread(&dwID, 4, 1, m_pFile);
				}

				m_dfh.m_pDFG[wIndex].m_dwDataBlockID = dwBlockID;

				DWORD dwWrite = dwDataToWrite;
				DWORD dwMaxWrite = m_dwDataPerBlock-dwDataExist;
				if (dwWrite>dwMaxWrite)
					dwWrite = dwMaxWrite;

				DWORD dwWriteLength = dwWrite*m_wSizeofData;

				if (dwDataExist>0)
					fseeko(m_pFile, UInt32x32To64(dwDataExist, m_wSizeofData), SEEK_CUR);

				fwrite(pDataToWrite, 1, dwWriteLength, m_pFile);

				dwDataToWrite -= dwWrite;
				pDataToWrite += dwWrite;

				dwBlockID = dwNextBlockID;
				dwDataExist = 0;

				//ASSERT(dwBlockID<=m_dfh.m_pDFG[wIndex].m_dwLastBlockID);

				if (bNewBlock==FALSE && dwBlockID<m_dfh.m_pDFG[wIndex].m_dwLastBlockID)
				{
					UINT64 hOffset = GetBlockOffset(dwBlockID);
					//ASSERT(hFileLength >= hOffset+DF2_BLOCK_SIZE);

					fseeko(m_pFile, hOffset, SEEK_SET);
					fread(&dwNextBlockID, 4, 1, m_pFile);
				}
				else
					dwNextBlockID = 0;

				if (dwDataToWrite==0 && dwWrite==dwMaxWrite){
					m_dfh.m_pDFG[wIndex].m_dwDataBlockID = dwBlockID;//question?
				}
			}

			if (dwDataToWrite==0)
			{
				if (bNewGoods)  // add m_dwNumOfGoods and sort index
				{
					m_dfh.m_dfi.m_dwNumOfGoods++;
					bFileInfoChanged = TRUE;

					gi.m_gcGoodsID = gcGoodsID;
					gi.m_wIndex = wIndex;

					m_aGoodsIndex.Add(gi);
				}

				if (bFileInfoChanged)
				{
					fseeko(m_pFile, 0, SEEK_SET);
					fwrite(&(m_dfh.m_dfi), SIZEOF_DATAFILE_INFO, 1, m_pFile);
				}

				m_dfh.m_pDFG[wIndex].m_dwGoodsID = gcGoodsID.m_dwGoodsID;
				CopyMemory(m_dfh.m_pDFG[wIndex].m_pcCode, gcGoodsID.m_pcCode, LEN_STOCKCODE);

				if (bAppend)
					m_dfh.m_pDFG[wIndex].m_dwNumOfData += dwData;
				else
					m_dfh.m_pDFG[wIndex].m_dwNumOfData = dwData;
				m_dfh.m_pDFG[wIndex].m_dwLastDataIndex = dwLastDataIndex;

				DWORD dwIndexOffset = SIZEOF_DATAFILE_INFO+wIndex*SIZEOF_DATAFILE_GOODS;

				fseeko(m_pFile, dwIndexOffset, SEEK_SET);
				fwrite(m_dfh.m_pDFG+wIndex, SIZEOF_DATAFILE_GOODS, 1, m_pFile);

				bWriteOK = TRUE;
			}
		}
		catch(...)
		{
		}
	}

	if (bWriteOK){
		return 0;
	}

	return -1;
}

template <class TYPE> int CDataFile2<TYPE>::ClearData(const CGoodsCode& gcGoodsID)
{
	Mutex_scope_lock scope_lock(m_mutex);

	if (m_pFile==NULL)
		ReadHead();

	WORD wIndex = 0;
	BOOL bNewGoods = FALSE;

	CGoodsIndex gi;
	gi.m_gcGoodsID = gcGoodsID;

	int nPos = m_aGoodsIndex.Find(gi);
	if (nPos<0)	// goods not found
	{
		bNewGoods = TRUE;

		wIndex = (WORD)m_aGoodsIndex.GetSize();
		if (wIndex>=DF_MAX_GOODSNUM)
		{
			return -1;
		}
	}
	else	// goods exists
	{
		wIndex = m_aGoodsIndex[nPos].m_wIndex;
	}

	BOOL bWriteOK = FALSE;

	if (m_pFile != NULL)
	{
		try
		{
			if (bNewGoods)  // add m_dwNumOfGoods and sort index
			{
				m_dfh.m_dfi.m_dwNumOfGoods++;

				fseeko(m_pFile, 0, SEEK_SET);
				fwrite(&(m_dfh.m_dfi), SIZEOF_DATAFILE_INFO, 1, m_pFile);

				gi.m_gcGoodsID = gcGoodsID;
				gi.m_wIndex = wIndex;

				m_aGoodsIndex.Add(gi);
			}

			//ASSERT(m_dfh.m_pDFG[wIndex].m_dwGoodsID==dwGoodsID || m_dfh.m_pDFG[wIndex].m_dwGoodsID==0);

			CGoodsCode gcFile(m_dfh.m_pDFG[wIndex].m_dwGoodsID, m_dfh.m_pDFG[wIndex].m_pcCode);

			if (m_dfh.m_pDFG[wIndex].m_dwNumOfData!=0 || gcFile!=gcGoodsID)
			{
				m_dfh.m_pDFG[wIndex].m_dwNumOfData = 0;
				m_dfh.m_pDFG[wIndex].m_dwLastDataIndex = 0;

				m_dfh.m_pDFG[wIndex].m_dwGoodsID = gcGoodsID.m_dwGoodsID;
				CopyMemory(m_dfh.m_pDFG[wIndex].m_pcCode, gcGoodsID.m_pcCode, LEN_STOCKCODE);

				DWORD dwIndexOffset = SIZEOF_DATAFILE_INFO+wIndex*SIZEOF_DATAFILE_GOODS;

				fseeko(m_pFile, dwIndexOffset, SEEK_SET);
				fwrite(m_dfh.m_pDFG+wIndex, SIZEOF_DATAFILE_GOODS, 1, m_pFile);
			}

			bWriteOK = TRUE;
		}
		catch(...)
		{
		}
	}

	if (bWriteOK)
		return 0;

	return -1;
}

template <class TYPE> DWORD CDataFile2<TYPE>::AddNewBlock(UINT64& hFileLength)
{
	if (m_dfh.m_dfi.m_dwUsedBlocks>=m_dfh.m_dfi.m_dwTotalBlocks)	// increase data blocks
	{
		DWORD blocks = m_dfh.m_dfi.m_dwTotalBlocks+DF_BLOCKS_GROWBY;
		hFileLength = GetBlockOffset(blocks);
		ftruncate(fileno(m_pFile), hFileLength);

		m_dfh.m_dfi.m_dwTotalBlocks =blocks;
	}

	DWORD dwBlockID = m_dfh.m_dfi.m_dwUsedBlocks++;
	return dwBlockID;
}

template <class TYPE> UINT64 CDataFile2<TYPE>::GetFileLength()
{
	if (m_pFile == NULL)
		return 0;
	return SimpleFile::GetFileSize(m_strFile.GetData());
}

//////////////////////////////////////////////////////////////////////////
typedef CDataFile<CDay> CDataFile_Day;
typedef CDataFile<CHisMin> CDataFile_HisMin;

typedef CDataFile2<CTrade> CDataFile_Trade;
typedef CDataFile2<CBargain> CDataFile_Bargain;
typedef CDataFile2<CPartOrder> CDataFile_PartOrder;
typedef CDataFile2<COrder> CDataFile_Order;
typedef CDataFile2<CMinute> CDataFile_Minute;

typedef CDataFile<CHisBK> CDataFile_HisBK;

typedef CDataFile<CAlarm> CDataFile_Alarm;

typedef CDataFile<CDayCheck> CDataFile_DayCheck;


#endif


