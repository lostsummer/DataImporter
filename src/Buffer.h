#ifndef _BUFFER_H_
#define _BUFFER_H_

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)
#include <string>

#include "SysGlobal.h"
#include "String.h"

#include "XInt32.h"

#define		DATA_LACK		-1
#define		DATA_ERROR		-2



class CBuffer  
{
// Attributes
protected:
	PBYTE		m_pBase;
	UINT		m_nDataSize;
	UINT		m_nSize;

	UINT		m_nInitSize;

	UINT		m_nMoreBytes;

	bool		m_bSustainSize;

	static DWORD		m_dwPageSize;

public:
	bool		m_bNoAlloc;		//不分配内存
	bool		m_bSingleRead;
	UINT		m_nReadPos;

	bool		m_bNetworkOrder;	// true：网络序；false：intel主机序

// Methods
protected:
	UINT DeAllocateBuffer(UINT nRequestedSize);

public:
	CBuffer(bool bNetOder = false);
	virtual ~CBuffer();

	void ClearBuffer();
	void Initialize(UINT nInitsize, bool bSustain, UINT nMoreBytes = 0);

	UINT GetMemSize();

	UINT Delete(UINT nSize);
	UINT Add(UINT nSize);
	UINT Read(void* pData, UINT nSize);
	UINT Write(const void* pData, UINT nSize);
	UINT Insert(const void* pData, UINT nSize);
	UINT DeleteEnd(UINT nSize);

	UINT SkipData(int nSize);

	UINT ReadString(CString&);
	UINT WriteString(CString&);

	UINT ReadString(std::string&);
	UINT WriteString(const std::string&);

	PBYTE GetBuffer(UINT nPos=0);
	UINT GetBufferLen();

	void FileWrite(const CString& strFileName);
	void FileRead(const CString& strFileName);

	const CBuffer& operator+(CBuffer& buff);
	UINT ReAllocateBuffer(UINT nRequestedSize);

	void SetBuffer(PBYTE, UINT);

	CString ReadUnicodeString();
	void WriteUnicodeString(const CString&);
	void WriteUnicodeStringEx(const std::string&, const char* charsetFrom);

	void ReadUnicodeString(std::string&);
	void WriteUnicodeString(const std::string&);

	void WriteName(char*);

	int ReadInt();
	void WriteInt(int);

	XInt32 ReadXInt();
	void WriteXInt(XInt32);

	short ReadShort();
	void WriteShort(short);

	WORD ReadWORD();
	void WriteWORD(WORD);

	char ReadChar();
	void WriteChar(char);

	unsigned char ReadUChar();
	void WriteUChar(unsigned char);
	
	//add by liuchengzhu 20091207
	void WriteInt64(INT64);
	INT64 ReadInt64();
	//end add by liuchengzhu 20091207
};

#endif		// BUFFER_H



