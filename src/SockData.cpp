#include <sys/stat.h>
#include <fcntl.h>

#include <algorithm>
#include <sstream>

#include "SockData.h"
#include "MyDefine.h"
#include "MySocket.h"
#include "DynaCompress.h"
#include "HisCompress.h"
#include "TradeCompress.h"
#include "Data.h"
#include "Grid.h"
#include "CalcCPX.h"
#include "QMatrixCompress.h" 
#include "MDS.h"
#include "DESCoder.h"
#include "Sockets.h"
#include "GoodsLua.h"
#include <fstream>
#include <sys/syscall.h>

using namespace std;

#define LEN_PHONE 11
#define LEN_PHONE_PASSWD 6
#define LEN_MACHINE_TYPE 50
#define LEN_USER 20

#define MAX_GOODS_OF_HK_LIST 20

// CSockData //////////////////////////////////////////////////////////////////
extern CBitCode BC_DAY_NUMBER[];
extern int BCNUM_DAY_NUMBER;
extern CBitCode BC_MINDAY_PRICE[];
extern int BCNUM_MINDAY_PRICE;
extern CBitCode BC_MIN_NUMBER[];
extern int BCNUM_MIN_NUMBER;
extern CBitCode BC_DYNA_PRICE_DIFF[];
extern int BCNUM_DYNA_PRICE_DIFF;
extern CBitCode BC_DYNA_PRICE_DIFFS[];
extern int BCNUM_DYNA_PRICE_DIFFS;
extern CBitCode BC_DYNA_AMNT[];
extern int BCNUM_DYNA_AMNT;
extern CBitCode BC_DYNA_VOL[];
extern int BCNUM_DYNA_VOL;
extern CBitCode BC_DYNA_VOL_DIFF[];
extern int BCNUM_DYNA_VOL_DIFF;
extern CBitCode BC_DYNA_QH_OI_DIFF[];
extern int BCNUM_DYNA_QH_OI_DIFF;
extern CBitCode BC_DYNA_CPX[];
extern int BCNUM_DYNA_CPX;

//////////////////////////////////Steven Begin//////////////////////////////////////////////
extern INT64 g_hCompIn;
extern INT64 g_hCompOut;
extern int g_nMinCompressLen;
extern int g_nMinCompressDel;
extern int g_bypass;
extern int g_IsOpenByPass;

void l_ReadCodeName(CBuffer& buf, char* pcName, int nMaxLen)
{
	ZeroMemory(pcName, nMaxLen);

	UINT nLen = buf.ReadChar();
	if (nLen>(UINT)nMaxLen)
		nLen = nMaxLen;

	buf.Read(pcName, nLen);

}
//////////////////////////////////////////////////////////////////////


CSockData::CSockData()
{
	m_wStatus = CSockData::WantNone;
	m_wVersion = 0;
	m_wPriority = 0;
	m_dwLastSendTime = 0;
	m_pSocket = NULL;
	m_time_start=0;
	m_nConnectID = 0;

	m_head.Initial();
	m_headMobile.Initial();
}

CSockData::~CSockData()
{
}

void CSockData::Recv(CDataHead& head, CBuffer& buf)
{
	m_head = head;
}

void CSockData::Recv(CDataHeadMobile& head, CBuffer& buf)
{
	m_headMobile = head;
}

void CSockData::Send(CBuffer& buf)
{

}

void CSockData::SendHead(CBuffer& buf, WORD wDataType)
{
	CDataHead head = m_head;
	head.m_wDataType = wDataType;
	head.m_dwDataLength = buf.GetBufferLen()-_DataHeadLength_;
	
	head.Send((char*)buf.GetBuffer(), buf.m_bNetworkOrder);
}

void CSockData::SendHeadMobile(CBuffer& buf, WORD wDataType)
{
	CDataHeadMobile head = m_headMobile;
	head.m_wDataType = wDataType;
	head.m_dwDataLength = buf.GetBufferLen()-_DataHeadMobileLength_;
	
	head.Send((char*)buf.GetBuffer(), buf.m_bNetworkOrder);
}

void CSockData::ExchagePhoneNum(CString& strPhoneNum)
{
	if (m_headMobile.m_wHeadID == 0x1122)
	{
		if (m_headMobile.dwSessionID == 0)
			m_headMobile.dwSessionID = 0x123456C0;

		CDESCoder DESCoder;
		CString strKey;
		strKey.Format("%d", m_headMobile.dwSessionID);
		LogDbg("加密前 data=%s key=%s", strPhoneNum.GetData(), strKey.GetData());
		strPhoneNum = DESCoder.Str2Des(strPhoneNum, strKey);
		LogDbg("加密后 data=%s", strPhoneNum.GetData());
	}
}

void CSockData::Abandon()
{
	LogErr("CSockData::Abandon");
	if (m_pSocket)
		m_pSocket->m_wStatus = CMySocket::WantDelete;
}

// 请求超时检查

void CSockData::OnTime(void)
{
}

// CCheckData //////////////////////////////////////////////////////////////////

void CCheckData::Send(CBuffer& buf)
{
	DWORD dwSysTime = CMDSApp::g_dwSysSecond;
	if (m_pSocket && dwSysTime > m_pSocket->m_dwActiveTime+10)
	{
		buf.Add(_DataHeadLength_);

		SendHead(buf, _DATA_Check_*100+1);
	}
}

//////////////////////////////////////////////////////////////////////////
/*
CL2LB_DS_LoginData::CL2LB_DS_LoginData()
{
	LogTrace("%s", __FUNCTION__);
	m_dwIPAddress = 0;
	m_sPort = 80;
	m_lTime = 0;
}

void CL2LB_DS_LoginData::Send(CBuffer& buf)
{
	LogTrace("%s", __FUNCTION__);
	if (m_lTime/5 != CMDSApp::g_dwSysHMS/5)
	{
		m_lTime = CMDSApp::g_dwSysHMS;

		buf.Add(_DataHeadMobileLength_);

		buf.WriteInt(m_dwIPAddress);
		buf.WriteShort(m_sPort);

		buf.WriteInt(theApp.m_mds.GetActiveCons());
		buf.WriteInt(theApp.m_mds.GetIdleCons());
		buf.WriteInt(theApp.m_mds.GetConnectCount());
		buf.WriteInt(CMDSClientContext::m_dwRecvPack);
		buf.WriteInt(CMDSClientContext::m_dwSendPack);
		buf.WriteInt(theApp.m_dwStartHMS);
		buf.WriteInt(theApp.m_pSH ? theApp.m_pSH->m_dwPrice : 0);
		buf.WriteInt(theApp.m_wMaxFDS);

		buf.WriteInt(theApp.m_dwInitDate);		//开盘日期
		buf.WriteInt(theApp.m_bInitingDate?1:0);	//
		buf.WriteInt(theApp.m_dwSaveDate);		//收盘日期
		buf.WriteInt(theApp.m_bSaveingDate?1:0);	//

		buf.WriteInt(0);
		buf.WriteInt(0);
		buf.WriteInt(0);
		buf.WriteInt(0);
		buf.WriteInt(0);
		buf.WriteInt(0);
		buf.WriteInt(0);

		SendHeadMobile(buf, _DATA_LB_Login_);
	}
}

*/
//////////////////////////////////////////////////////////////////////

CL2DM_LoginData::CL2DM_LoginData()
{
	m_strName = "";
	m_strPassword = "";
}

void CL2DM_LoginData::Recv(CDataHead& head, CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	BYTE bLoginAck;
	CString strLoginAck;

	LOG_ERR("CL2DM_LoginData::Recv");

	bLoginAck = buf.ReadChar();
	buf.ReadString(strLoginAck);

	CTradeSession pTradeSession[4];
	for (UINT i=0; i<4; i++)
	{
		pTradeSession[i].m_cNum = buf.ReadChar();
		for (int c=0; c<pTradeSession[i].m_cNum; c++)
		{
			pTradeSession[i].m_pwTimeBegin[c] = buf.ReadShort();
			pTradeSession[i].m_pwTimeEnd[c] = buf.ReadShort();
		}
	}

	pSocket->m_cCheckSumDW = 0;
	if (buf.GetBufferLen()>0)
	{
		pSocket->m_cCheckSumDW = buf.ReadChar();
		if (pSocket->m_cCheckSumDW!=1)
			pSocket->m_cCheckSumDW = 0;
	}

	pSocket->m_sVersion = 0;
	if (buf.GetBufferLen()>=2)
	{
		pSocket->m_sVersion = buf.ReadShort();
	}
	ASSERT(pSocket->m_sVersion >= 11);


	if (bLoginAck==0)
	{
		if (m_pSocket)
		{
			CopyMemory(theApp.m_pTradeSession, pTradeSession, 4*sizeof(CTradeSession));

			CCheckData* pCheckData = new CCheckData;
			if (pCheckData)
			{
				pCheckData->m_wStatus = CSockData::WantAll;
				m_pSocket->AddData(pCheckData);
			}

//			CL2DM_ValueData* pValueData = new CL2DM_ValueData;

			CL2DM_Value6Data* pValueData = new CL2DM_Value6Data;
			if (pValueData)
			{
				pValueData->m_wStatus = CSockData::WantSend;
				pValueData->m_bCode = (pSocket->m_sVersion>=10);
				m_pSocket->AddData(pValueData);
			}

/*			pc client want this message
			CL2DM_Static5Data* pStaticData = new CL2DM_Static5Data;
			if (pStaticData)
			{
				pStaticData->m_pChannel = pChannel;
				pStaticData->m_wStatus = CSockData::WantSend;
				pStaticData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pStaticData);
			}
*/	

//			CL2DM_MinuteData* pMinuteData = new CL2DM_MinuteData;

			CL2DM_Minute6Data* pMinuteData = new CL2DM_Minute6Data;
			if (pMinuteData)
			{
				pMinuteData->m_wStatus = CSockData::WantSend;
				pMinuteData->m_bCode = (pSocket->m_sVersion>=10);
				m_pSocket->AddData(pMinuteData);
			}

			CL2DM_CPXData* pCPXData = new CL2DM_CPXData;
			if (pCPXData)
			{
				pCPXData->m_wStatus = CSockData::WantSend;

				m_pSocket->AddData(pCPXData);
			}
/*			pc client want this message
			CL2DM_Pool1Data* pPool1Data = new CL2DM_Pool1Data;
			if (pPool1Data)
			{
				pPool1Data->m_pChannel = pChannel;
				pPool1Data->m_wStatus = CSockData::WantSend;
				pPool1Data->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pPool1Data);
			}
*/
			CL2DM_MatrixData* pMatrixData = new CL2DM_MatrixData;
			if (pMatrixData)
			{
				pMatrixData->m_wStatus = CSockData::WantSend;
				m_pSocket->AddData(pMatrixData);
			}

/*			pc client also want this message
			CL2DM_Alarm5Data* pAlarmData = new CL2DM_Alarm5Data;
			if (pAlarmData)
			{
				pAlarmData->m_pChannel = pChannel;
				pAlarmData->m_wStatus = CSockData::WantSend;
				pAlarmData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pAlarmData);
			}
*/
	


			CL2DM_SysAlarmData* pSysAlarmData = new CL2DM_SysAlarmData;
			if (pSysAlarmData)
			{
				pSysAlarmData->m_wStatus = CSockData::WantSend;

				m_pSocket->AddData(pSysAlarmData);
			}

/*			pc client also want this message
			CL2DM_IndGoodsData* pIndGoodsData = new CL2DM_IndGoodsData;
			if (pIndGoodsData)
			{
				pIndGoodsData->m_pChannel = pChannel;
				pIndGoodsData->m_wStatus = CSockData::WantSend;
				pIndGoodsData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pIndGoodsData);
			}

			CL2DM_ExpValueData* pExpValueData = new CL2DM_ExpValueData;
			if (pExpValueData)
			{
				pExpValueData->m_pChannel = pChannel;
				pExpValueData->m_wStatus = CSockData::WantSend;
				pExpValueData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pExpValueData);
			}

*/
			
//			CL2DM_BargainData* pBargainData = new CL2DM_BargainData;

			CL2DM_Bargain6Data* pBargainData = new CL2DM_Bargain6Data;
			if (pBargainData)
			{
				pBargainData->m_wStatus = CSockData::WantSend;
				pBargainData->m_bCode = (pSocket->m_sVersion>=10);
				m_pSocket->AddData(pBargainData);
			}

/*
			CL2DM_Group5Data* pGroupData = new CL2DM_Group5Data;
			if (pGroupData)
			{
				pGroupData->m_pChannel = pChannel;
				pGroupData->m_wStatus = CSockData::WantSend;
				pGroupData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pGroupData);
			}

			CL2DM_GroupStat5Data* pGroupStatData = new CL2DM_GroupStat5Data;
			if (pGroupStatData)
			{
				pGroupStatData->m_pChannel = pChannel;
				pGroupStatData->m_wStatus = CSockData::WantSend;
				pGroupStatData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pGroupStatData);
			}

			CL2DM_Trade5Data* pTradeData = new CL2DM_Trade5Data;
			if (pTradeData)
			{
				pTradeData->m_pChannel = pChannel;
				pTradeData->m_wStatus = CSockData::WantSend;
				pTradeData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pTradeData);
			}

			CL2DM_PartOrder5Data* pPartOrderData = new CL2DM_PartOrder5Data;
			if (pPartOrderData)
			{
				pPartOrderData->m_pChannel = pChannel;
				pPartOrderData->m_wStatus = CSockData::WantSend;
				pPartOrderData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pPartOrderData);
			}

			CL2DM_StatCount5Data* pStatCountData = new CL2DM_StatCount5Data;
			if (pStatCountData)
			{
				pStatCountData->m_pChannel = pChannel;
				pStatCountData->m_wStatus = CSockData::WantSend;
				pStatCountData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pStatCountData);
			}

			CL2DM_RxtPoolData *pRxtPoolData = new CL2DM_RxtPoolData;
			if (pRxtPoolData)
			{
				pRxtPoolData->m_pChannel = pChannel;
				pRxtPoolData->m_wStatus = CSockData::WantSend;
				pRxtPoolData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pRxtPoolData);
			}

			CL2DM_RxtPoolTipsData *pRxtPoolTipData = new CL2DM_RxtPoolTipsData;
			if (pRxtPoolTipData)
			{
				pRxtPoolTipData->m_pChannel = pChannel;
				pRxtPoolTipData->m_wStatus = CSockData::WantSend;
				pRxtPoolTipData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pRxtPoolTipData);
			}

			CL2DM_HmldData *pHmldData = new CL2DM_HmldData;
			if (pHmldData)
			{
				pHmldData->m_pChannel = pChannel;
				pHmldData->m_wStatus = CSockData::WantSend;
				pHmldData->SetBuddyPool(&g_BuddyPoolDay);

				//m_pSocket->AddData(pHmldData);
			}

*/

			if (theApp.m_bEnableCheckDay)
			{
				CL2DM_GoodsCheckData* pCheckData = new CL2DM_GoodsCheckData;
				if (pCheckData)
				{
					pCheckData->m_wStatus = CSockData::WantSend;
					m_pSocket->AddData(pCheckData);
				}

				CL2DM_DayCheckData* pDayCheckData = new CL2DM_DayCheckData;
				if (pDayCheckData)
				{
					pDayCheckData->m_wStatus = CSockData::WantSend;
					m_pSocket->AddData(pDayCheckData);
				}

				CL2DM_DayData* pDayData = new CL2DM_DayData;
				if (pDayData)
				{
					pDayData->m_wStatus = CSockData::WantSend;
					m_pSocket->AddData(pDayData);
				}
			}
		}
	}

	m_wStatus = CSockData::WantDelete;

	//ASSERT(buf.GetBufferLen()==0);
}

void CL2DM_LoginData::Send(CBuffer& buf)
{
	buf.Add(_DataHeadLength_);

	buf.WriteString(m_strName);
	buf.WriteString(m_strPassword);

	SendHead(buf, _DATA_L2DM_Login_);

	m_wStatus = CSockData::WantRecv;
	LOG_ERR("CL2DM_LoginData::%s %d", __func__, __LINE__);
}

//////////////////////////////////////////////////////////////////////

void CL2DM_ValueData::Recv(CDataHead& head, CBuffer& buf)
{
	CSockData::Recv(head, buf);

	DWORD dwDate = buf.ReadInt();
	DWORD dwDateHK = buf.ReadInt();

	char cInitType = 0;

	if (dwDate>0 && theApp.m_dwDateValue!=dwDate)
		cInitType |= 0x01;

	if ((cInitType==0 || dwDate==dwDateHK) && dwDateHK>0 && theApp.m_dwDateValueHK!=dwDateHK)
		cInitType |= 0x02;

	if (cInitType)
	{
		if (cInitType&0x01)
			theApp.InitDate(dwDate, cInitType);
		else
			theApp.InitDate(dwDateHK, cInitType);

		Abandon();
		return;
	}

	if (dwDateHK>0 && theApp.m_dwDateValueHK!=dwDateHK)
	{
		Abandon();
		return;
	}

	char pcName[LEN_STOCKNAME];
	BOOL bGoodsNameChange = FALSE;

	//WORD wGoodsOld = theApp.m_wGoods;

	WORD wNum = buf.ReadShort();

	if (wNum>0)
		theApp.m_bGoodsChanged = true;

	//char cStationSave = -1;

	for (WORD w=0; w<wNum; w++)
	{
		DWORD dwGoodsID = buf.ReadInt();

		if (g_IsGoodsID(dwGoodsID)==FALSE)
		{
			Abandon();
			return;
		}

		CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
		if (pGoods)
		{
			//pGoods->m_rwGood.AcquireWriteLock();
			// 更换锁 [3/5/2015 frant.qian]
			RWLock_scope_wrlock  rwlock(pGoods->m_rwGood);	
			pGoods->m_rwGoodcount[12] += 1;

			try
			{
				BYTE bCount = buf.ReadChar();

				if (bCount & 0x01)
				{
					pGoods->m_cLevel = buf.ReadChar();

					char cStatus = buf.ReadChar();

					ZeroMemory(pcName, LEN_STOCKNAME);
					int nLen = buf.ReadChar();
					buf.Read(pcName, nLen);

					if (cStatus & 0x01)
					{
						nLen = buf.ReadChar();
						ZeroMemory(pGoods->m_pcCode, LEN_STOCKCODE);
						buf.Read(pGoods->m_pcCode, nLen);
					}

					if (cStatus & 0x02)
						pGoods->m_sTimeZone = buf.ReadShort();

					if (cStatus & 0x04)
					{
						pGoods->m_tradeSession.m_cNum = buf.ReadChar();
						if (pGoods->m_tradeSession.m_cNum>0 && pGoods->m_tradeSession.m_cNum<=MAX_TRADING_SESSION)
						{
							for (int c=0; c<pGoods->m_tradeSession.m_cNum; c++)
							{
								pGoods->m_tradeSession.m_pwTimeBegin[c] = buf.ReadShort();
								pGoods->m_tradeSession.m_pwTimeEnd[c] = buf.ReadShort();
							}
						}
						else
							pGoods->m_tradeSession.m_cNum = 0;
					}

///					pGoods->m_cDecimalPosition = buf.ReadChar();
					
					if (cStatus & 0x08)
					{
						DWORD dwDate = buf.ReadInt();
						if (dwDate>0 && pGoods->m_dwDate!=dwDate)
						{
							if (pGoods->m_dwDate>0 && pGoods->m_dwDateSave!=pGoods->m_dwDate)
							{
								pGoods->SaveToday();

								CHisMinDF* pHMDF = theApp.GetHisMinDF(pGoods->m_dwDate);
								if (pHMDF)
									pGoods->SaveHisMin(pHMDF, pGoods->m_dwDate);

								pGoods->m_dwDateSave = pGoods->m_dwDate;
							}

							pGoods->InitDay();
							pGoods->m_dwDate = dwDate;

							if (pGoods->m_dwDateBargain!=dwDate || pGoods->m_dwDateMinute!=dwDate)
							{
								//pGoods->m_rwGood.ReleaseWriteLock();

								if (pGoods->m_dwDateBargain!=dwDate)
								{
									RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);
									pGoods->ClearBargain(TRUE, dwDate);
								}

								if (pGoods->m_dwDateMinute!=dwDate)
								{
									RWLock_scope_wrlock scope_wrlock(pGoods->m_rwMinute);
									pGoods->ClearMinute(TRUE, dwDate);
								}

								//pGoods->m_rwGood.AcquireWriteLock();
							}
						}
					}

					if (memcmp(pGoods->m_pcName, pcName, LEN_STOCKNAME))
					{
						CopyMemory(pGoods->m_pcName, pcName, LEN_STOCKNAME);
						bGoodsNameChange = TRUE;
					}

					buf.ReadString(pGoods->m_strEName);

					pGoods->m_dwClose = buf.ReadInt();
					pGoods->m_dwA_H = buf.ReadInt();

					pGoods->Goods2Group();
					pGoods->MakePY();
					
					CBitStream stream(&buf, TRUE);
					if (pGoods->IsGZQH())
					{
						int nRet = CDynaCompress::ExpandStaticQH(stream, pGoods);
						if (nRet<0)
						{
							LOG_ERR("%06d  SysTime=%d  ExpandStaticQH=%d", pGoods->m_dwNameCode, CMDSApp::g_dwSysHMS, nRet);

							Abandon();
							//pGoods->m_rwGood.ReleaseWriteLock();
							return;
						}
					}
					else
					{
						int nRet = CDynaCompress::ExpandStatic(stream, pGoods);
						if (nRet<0)
						{
							LOG_ERR("%d  SysTime=%d ExpandStatic=%d",	pGoods->GetID(), CMDSApp::g_dwSysHMS, nRet);
							Abandon();
							//pGoods->m_rwGood.ReleaseWriteLock();
							return;
						}
					}

					pGoods->m_dwCountStatic++;
				}

				if (bCount & 0x10)
				{
					char pcPreName[4];
					buf.Read(pcPreName, 4);
					if (memcmp(pGoods->m_pcPreName, pcPreName, 4))
					{
						CopyMemory(pGoods->m_pcPreName, pcPreName, 4);
						pGoods->m_strPreName = pGoods->GetPreName();
					}

					pGoods->m_cTradeCode = buf.ReadChar();

					if (bCount & 0x20)
					{
						pGoods->m_dwI_Num = buf.ReadInt();
						if (pGoods->m_dwI_Num>0)
						{
							pGoods->m_dwI_Close = buf.ReadInt();
							pGoods->m_dwI_Ave = buf.ReadInt();
							pGoods->m_dwI_Amount = buf.ReadInt();
							pGoods->m_dwI_AveGB = buf.ReadInt();
							pGoods->m_dwI_SumSZ = buf.ReadInt();
							pGoods->m_dwI_Percent = buf.ReadInt();
							pGoods->m_dwI_SYL = buf.ReadInt();
							pGoods->m_dwI_JBBS = buf.ReadInt();
						}
					}

					pGoods->m_dwCountStatus++;
				}

				if (bCount & 0x06)		// 02, 04
				{
					// dynaCompress
					CBitStream stream(&buf, TRUE);

					int nRet = pGoods->ExpandDynamicValue(stream);
					if (nRet<0)
					{
						LOG_ERR("%06d  SysTime=%d  ExpandDynamicValue=%d", pGoods->m_dwNameCode, CMDSApp::g_dwSysHMS, nRet);
						Abandon();
						//pGoods->m_rwGood.ReleaseWriteLock();
						return;
					}

					char cIndex = buf.ReadChar();
					if (cIndex)
					{
						char cRD = buf.ReadChar();
						if (pGoods->m_pIndex == NULL)
							pGoods->m_pIndex = new CIndex;

						if (pGoods->m_pIndex)
						{
							pGoods->m_pIndex->m_cRD = cRD;

							buf.Read(pGoods->m_pIndex->m_plRD, cRD*4);

							pGoods->m_pIndex->m_wRise = buf.ReadShort();
							pGoods->m_pIndex->m_wEqual = buf.ReadShort();
							pGoods->m_pIndex->m_wFall = buf.ReadShort();

							pGoods->m_pIndex->m_wRN = buf.ReadShort();
							pGoods->m_pIndex->m_wDN = buf.ReadShort();
							pGoods->m_pIndex->m_wRD = buf.ReadShort();
						}
						else
							buf.Delete(cRD*4+12);
					}

					pGoods->m_nBKStrong = buf.ReadInt();
					pGoods->m_dwSysAlert = buf.ReadInt();

					if (dwGoodsID==1 || dwGoodsID==1399001) //20110625
					{
						//if (theApp.m_dwTimeValue != pGoods->m_dwTime)
							theApp.m_dwTimeValue = pGoods->m_dwTime;
					}

					if (dwGoodsID==5500001)
						theApp.m_dwTimeValueHK = pGoods->m_dwTime;

					//////////////////////////////////////////////////////////////////////////
					if (dwGoodsID==1 || dwGoodsID == 1399001 || dwGoodsID == 5500001)
						theApp.m_dwTime = pGoods->m_dwTime;

					if (dwGoodsID == 1) 
						theApp.m_dwTimeSH = theApp.m_dwInitDate % 10000 * 1000000 + pGoods->m_dwTime;

					if (dwGoodsID == 1399001)
						theApp.m_dwTimeSZ = theApp.m_dwInitDate % 10000 * 1000000 + pGoods->m_dwTime;

					pGoods->AddDay();
				}

				if (bCount & 0x08)		// Matrix
				{
					CBitStream stream(&buf, TRUE);
//					RWLock_scope_wrlock  wrlock(pGoods->m_matrix.m_MatrixLock);
					int nRet = CQMatrixCompress::ExpandQueueMatrix(pGoods->m_matrix, stream);
					if (nRet!=0)
					{
						LOG_ERR("%06d  m_dwDynamicTimeBuy=%d m_dwDynamicTimeSell=%d  ExpandQueueMatrix=%d",
							pGoods->m_dwNameCode, pGoods->m_matrix.m_dwDynamicTimeBuy, pGoods->m_matrix.m_dwDynamicTimeSell, nRet);
						Abandon();
						//pGoods->m_rwGood.ReleaseWriteLock();
						return;
					}
				}
			}
			catch (int n)
			{
				Abandon();
				LOG_ERR("CL2DM_ValueData::Recv fail");
				//pGoods->m_rwGood.ReleaseWriteLock();
				return;
			}


			pGoods->m_rwGoodcount[12] -= 1;
			//pGoods->m_rwGood.ReleaseWriteLock();

			pGoods->CheckMainTrack();
		}
	}
	if (CMDSApp::g_dwSysSecond%200==0)
	{
		LOG_ERR("test CL2DM_ValueData::Recv pid:%d",syscall(SYS_gettid));
	}
	//ASSERT(buf.GetBufferLen()==0);
}

void CL2DM_ValueData::Send(CBuffer& buf)
{
	buf.Add(_DataHeadLength_);

	SendHead(buf, _DATA_L2DM_Value_);

	m_wStatus = CSockData::WantRecv;
}

//////////////////////////////////////////////////////////////////////

CL2DM_MinuteData::CL2DM_MinuteData()
{
	m_aMinuteTemp.SetSize(0, 60);
}

CL2DM_MinuteData::~CL2DM_MinuteData()
{
}

void CL2DM_MinuteData::MakeGM()
{
	m_aGM.RemoveAt(0, m_aGM.GetSize());

	CSortArrayMinute m_aMinute;
	CPtrArray aGoods;

	theApp.m_wrGoods.AcquireReadLock();
	aGoods.Copy(theApp.m_aGoods);
	theApp.m_wrGoods.ReleaseReadLock();

	WORD wGoods = aGoods.GetSize();
	for (WORD w=0; w<wGoods; w++)
	{
		CGoods* pGoods = (CGoods*)aGoods[w];

		CGoodsMinute gm;
		gm.m_dwGoodsID = pGoods->GetID();

		BOOL bIsGZQH = pGoods->IsGZQH();
		BOOL bIndex = (pGoods->m_pIndex != NULL);
		BOOL bIsLevel2 = (pGoods->m_cLevel == 2);

		RWLock_scope_rdlock scope_rdlock(pGoods->m_rwMinute);

		gm.m_wMinute = pGoods->m_wMinuteInDisk;
		gm.m_cCheckSum = pGoods->m_cMinuteCheckSumInDisk;
		gm.m_dwCheckSum = pGoods->m_dwMinuteCheckSumInDisk;

		WORD wMinute = pGoods->m_wMinute;
		if (pGoods->m_pMinuteBuffer && wMinute>1)
		{
			wMinute--;

			for (WORD i=0; i<wMinute; i++)
			{
				gm.m_cCheckSum += pGoods->m_pMinuteBuffer[i].GetCheckSum(bIsGZQH, bIndex, bIsLevel2, 1);
				gm.m_dwCheckSum += pGoods->m_pMinuteBuffer[i].GetCheckSumDW(bIsGZQH, bIndex, bIsLevel2, 1);
			}

			gm.m_wMinute += wMinute;
		}
	
		m_aGM.Add(gm);
	}
}

void CL2DM_MinuteData::Recv(CDataHead& head, CBuffer& buf)
{
	CSockData::Recv(head, buf);

	WORD wGoodsNum = buf.ReadShort();
	WORD i=0;
	for (i=0; i<wGoodsNum; i++)
	{
		DWORD dwGoodsID = buf.ReadInt();
		if (g_IsGoodsID(dwGoodsID)==FALSE)
		{
			Abandon();
			return;
		}
        
		CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
		if (pGoods==NULL)
			break;

		RWLock_scope_wrlock scope_wrlock(pGoods->m_rwMinute);

		try
		{
			CBitStream stream(&buf, TRUE);

			int nRet = CHisCompress::ExpandMinute(stream, pGoods, m_aMinuteTemp);
			if (nRet<0)
			{
				LOG_ERR("%d %06d  %d ExpandMinute=%d", i, dwGoodsID, pGoods->m_dwTime, nRet);
				Abandon();
				return;
			}
		}
		catch(int n)
		{
			Abandon();
			return;
		}
	}

	//if (i==wGoodsNum)
		//ASSERT(buf.GetBufferLen()==0);
}

void CL2DM_MinuteData::Send(CBuffer& buf)
{
	buf.Add(_DataHeadLength_);

	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	MakeGM();

	DWORD dwDate = theApp.m_dwDateValue;
	buf.WriteInt(dwDate);

	WORD wSize = WORD(m_aGM.GetSize());
	buf.WriteShort(wSize);
	for (WORD w=0; w<wSize; w++)
	{
		CGoodsMinute& gm = m_aGM[w];

		buf.WriteInt(gm.m_dwGoodsID);
		buf.WriteShort(gm.m_wMinute);
		if (pSocket->m_cCheckSumDW==1)
			buf.WriteInt(gm.m_dwCheckSum);
		else
			buf.WriteChar(gm.m_cCheckSum);
	}

	SendHead(buf, _DATA_L2DM_Minute_);
	m_wStatus = CSockData::WantRecv;
}

//////////////////////////////////////////////////////////////////////

CL2DM_BargainData::CL2DM_BargainData()
{
}

CL2DM_BargainData::~CL2DM_BargainData()
{
}

void CL2DM_BargainData::MakeGB()
{
	m_aGB.RemoveAt(0, m_aGB.GetSize());

	CPtrArray aGoods;
	CGoodsBargain gb;

	theApp.m_wrGoods.AcquireReadLock();
	aGoods.Copy(theApp.m_aGoods);
	theApp.m_wrGoods.ReleaseReadLock();

	WORD wGoods = aGoods.GetSize();
	for (WORD w=0; w<wGoods; w++)
	{
		CGoods* pGoods = (CGoods*)aGoods[w];
	
		//pGoods->m_rwBargain.AcquireWriteLock();

		gb.m_dwGoodsID = pGoods->GetID();
		gb.m_dwBargain = pGoods->m_dwBargainTotal;
		gb.m_cCheckSum = pGoods->m_cBargainCheckSum;
		gb.m_dwCheckSum = pGoods->m_dwBargainCheckSum;

		//pGoods->m_rwBargain.ReleaseWriteLock();

		m_aGB.Add(gb);
	}
}

void CL2DM_BargainData::Recv(CDataHead& head, CBuffer& buf)
{
	WORD wGoodsNum = buf.ReadShort();
	WORD i;
	for (i=0; i<wGoodsNum; i++)
	{
		DWORD dwGoodsID = buf.ReadInt();
		if (g_IsGoodsID(dwGoodsID)==FALSE)
		{
			Abandon();
			return;
		}

		CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
		if (pGoods==NULL)
			break;

		RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);

		try
		{
			CBitStream stream(&buf, TRUE);

			char cClear = (char)stream.ReadDWORD(1);
			if (cClear)
			{
				DWORD dwDate = stream.ReadDWORD(32);
				pGoods->ClearBargain(TRUE, dwDate);
			}

			DWORD dwNum = CTradeCompress::ExpandNum(stream);
			if (dwNum>0)
			{
				int nRet = CTradeCompress::ExpandBargain(stream, pGoods, dwNum);
				if (nRet<0)
				{
					LOG_ERR("%06d  %d ExpandBargain()=%d %d", dwGoodsID, CMDSApp::g_dwSysHMS, nRet, dwNum);

					Abandon();
					return;
				}
			}
		}
		catch (int)
		{
			Abandon();
			return;
		}
	}

	//if (i==wGoodsNum)
		//ASSERT(buf.GetBufferLen()==0);
}

void CL2DM_BargainData::Send(CBuffer& buf)
{
	buf.Add(_DataHeadLength_);

	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	MakeGB();

	DWORD dwDate = theApp.m_dwDateValue;
	buf.WriteInt(dwDate);

	WORD wSize = WORD(m_aGB.GetSize());
	buf.WriteShort(wSize);
	for (WORD w=0; w<wSize; w++)
	{
		CGoodsBargain& gb = m_aGB[w];

		buf.WriteInt(gb.m_dwGoodsID);
		buf.WriteInt(gb.m_dwBargain);
		if (pSocket->m_cCheckSumDW==1)
			buf.WriteInt(gb.m_dwCheckSum);
		else
			buf.WriteChar(gb.m_cCheckSum);
	}

	SendHead(buf, _DATA_L2DM_Bargain_);
	m_wStatus = CSockData::WantRecv;
}

CRecvCompressData::CRecvCompressData()
{
	m_bufComp.Initialize(_SendBuffer_Size_, true, 0);
	m_nCompInBuffer = 0;
}

CRecvCompressData::~CRecvCompressData()
{
	if (m_comp.m_pcIn)
		delete [] m_comp.m_pcIn;
}

BOOL CRecvCompressData::RecvCompressData(CBuffer& buf)
{
	try
	{
		m_comp.m_lIn = buf.ReadInt();
		if (m_comp.m_lIn==0)
			return TRUE;

		if (m_comp.m_pcIn==NULL || m_comp.m_lIn>m_nCompInBuffer)
		{
			if (m_comp.m_pcIn)
				delete [] m_comp.m_pcIn;

			m_nCompInBuffer = (m_comp.m_lIn+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE;
			m_comp.m_pcIn = (char*)new(nothrow) BYTE[m_nCompInBuffer];
			if (m_comp.m_pcIn==NULL)
				m_nCompInBuffer = 0;
		}

		if (m_comp.m_pcIn==NULL)
			return FALSE;

		buf.Read(m_comp.m_pcIn, m_comp.m_lIn);
		if (m_comp.Decode())
		{
			m_bufComp.ClearBuffer();
			m_bufComp.Write(m_comp.m_pcOut, m_comp.m_lOut);

			return TRUE;
		}
	}
	catch(...)
	{
	}

	return FALSE;
}


CL2DM_SysAlarmData::CL2DM_SysAlarmData()
{
	m_wVolIdx = m_wSFIdx1 = m_wSFIdx2 = -1;	

	m_aAlarm.SetSize(0, 60);
}

void CL2DM_SysAlarmData::SetIdx( std::string &strName, short s)
{
	string::size_type pos = strName.find_last_not_of(' ');
	if(pos != string::npos)
	{
		strName.erase(pos + 1);
		pos = strName.find_first_not_of(' ');
		if(pos != string::npos) strName.erase(0, pos);
	}
	else 
		strName.erase(strName.begin(), strName.end());

	if (strName == "快速选股_资金向上")
		m_wSFIdx1 = s;
	else if (strName == "快速选股_资金向下")
		m_wSFIdx2 = s;
	else if (strName == "快速选股_突然放量")
		m_wVolIdx = s;
}

void CL2DM_SysAlarmData::FilterIdx()
{
	short wNum = m_aAlarm.GetSize();
	for (short i = wNum-1;i >=0 ;i--)
	{
		CSysAlarm &alarm = m_aAlarm[i];
		if(alarm.m_wType == m_wSFIdx1 || alarm.m_wType == m_wSFIdx2
			|| alarm.m_wType == m_wVolIdx)
		{
			CGoods *pGoods = theApp.GetGoods(alarm.m_dwGoodsID);
			if (pGoods)
			{
				if (alarm.m_wType == m_wSFIdx1)
				{	
					pGoods->m_dwXGStatus |= 0x01;
					//LogInfo("%s 快速选股_资金向上 %d", pGoods->GetName().GetData(), pGoods->m_dwXGStatus);
				}
				else if (alarm.m_wType == m_wSFIdx2)
				{	
					pGoods->m_dwXGStatus &= ~0x01;
					//LogInfo("%s 快速选股_资金向下 %d", pGoods->GetName().GetData(), pGoods->m_dwXGStatus);
				}
				else if (alarm.m_wType == m_wVolIdx)
				{	
					pGoods->m_dwXGStatus |= 0x02;
					//LogInfo("%s 快速选股_突然放量 %d", pGoods->GetName().GetData(), pGoods->m_dwXGStatus);
				}
				else{}
			}
			m_aAlarm.RemoveAt(i);
		}
	}
}

void CL2DM_SysAlarmData::Recv(CDataHead& head, CBuffer& buf)
{

	CSockData::Recv(head, buf);

	char cInit = buf.ReadChar();
	if (cInit==2) //Title
	{
		CArray<CString, CString&> aName;
		std::string strName;
		CString stmp;
		short sSize = buf.ReadShort();
		for (short s=0; s<sSize; s++)
		{
			buf.ReadString(strName);
			stmp = strName.c_str();
			aName.Add(stmp);
			SetIdx(strName,s);
			//LogInfo("%s %d", strName.c_str(), s);
		}

		theApp.m_SysAlarm.m_MutexName.Lock();
		theApp.m_SysAlarm.m_aName.Copy(aName);
		theApp.m_SysAlarm.m_MutexName.UnLock();

		return;
	}

	if (cInit)
	{
		theApp.m_SysAlarm.ClearAlarm();
	}

	try
	{
		if (RecvCompressData(buf))
		{
			WORD wNum = m_bufComp.ReadShort();
			if (wNum>0 && wNum<=1000)
			{
				m_aAlarm.SetSize(wNum);
				CSysAlarm* pAlarm = m_aAlarm.GetData();
				if (pAlarm)
					m_bufComp.Read(pAlarm, sizeof(CSysAlarm)*wNum);

				FilterIdx();

				theApp.m_SysAlarm.AddAlarm(m_aAlarm);
			}
		}
	}
	catch(...)
	{
	}

	//ASSERT(buf.GetBufferLen()==0);
}

void CL2DM_SysAlarmData::Send(CBuffer& buf)
{
	buf.Add(_DataHeadLength_);

	buf.WriteInt(theApp.m_dwDateValue);

	theApp.m_SysAlarm.m_alarm.m_wr.AcquireReadLock();

	CSysAlarm* pAlarm = theApp.m_SysAlarm.m_alarm.GetBuffer();
	if (pAlarm)
	{
		DWORD dwCheckSum = 0;

		for (DWORD dw=0; dw<theApp.m_SysAlarm.m_alarm.m_dwSize; dw++)
		{
			dwCheckSum += pAlarm[dw].GetCheckSum();
		}

		buf.WriteInt(theApp.m_SysAlarm.m_alarm.m_dwSize);
		buf.WriteInt(dwCheckSum);
	}
	else
	{
		buf.WriteInt(0);
		buf.WriteInt(0);
	}

	theApp.m_SysAlarm.m_alarm.m_wr.ReleaseReadLock();

	SendHead(buf, _DATA_L2DM_SysAlarm_);

	m_wStatus = CSockData::WantRecv;
}


void CL2DM_CPXData::Recv(CDataHead& head, CBuffer& buf)
{

	CSockData::Recv(head, buf);

	try
	{
		theApp.m_cHorsePower = buf.ReadChar();
		theApp.m_cHorsePower1 = buf.ReadChar();
		theApp.m_cHorsePowerMA = buf.ReadChar();

		WORD wNum = buf.ReadShort();

		if (wNum>0 && RecvCompressData(buf))
		{
			for (WORD w=0; w<wNum; w++)
			{
				DWORD dwGoodsID = m_bufComp.ReadInt();

				int nDay = m_bufComp.ReadInt();
				int nMin60 = m_bufComp.ReadInt();
				DWORD dwPrice = m_bufComp.ReadInt();

				int nMonth = m_bufComp.ReadInt();
				int nWeek = m_bufComp.ReadInt();

				int nMin1 = m_bufComp.ReadInt();
				int nMin5 = m_bufComp.ReadInt();
				int nMin15 = m_bufComp.ReadInt();
				int nMin30 = m_bufComp.ReadInt();

				if (g_IsGoodsID(dwGoodsID)==FALSE)
				{
					Abandon();
					return;
				}


				CGoods* pGoods = theApp.GetGoods(dwGoodsID);
				if (pGoods)
				{
					AddASAlert(pGoods,nDay,nMin30,nMin60);

					//pGoods->m_rwGood.AcquireWriteLock();
					// 更换锁 [3/5/2015 frant.qian]
					RWLock_scope_wrlock  rwlock(pGoods->m_rwGood);
					pGoods->m_rwGoodcount[13] += 1;

					if (pGoods->m_nCPX_Day!=nDay)
					{
						pGoods->m_nCPX_Day = nDay;
						pGoods->m_dwCountStatic++;
					}

					pGoods->m_nCPX_Min60 = nMin60;
					pGoods->m_dwCPX_Close = dwPrice;

					pGoods->m_nCPX_Month = nMonth;
					pGoods->m_nCPX_Week = nWeek;

					pGoods->m_nCPX_Min1 = nMin1;
					pGoods->m_nCPX_Min5 = nMin5;
					pGoods->m_nCPX_Min15 = nMin15;
					pGoods->m_nCPX_Min30 = nMin30;

					pGoods->m_dwCountCPX++;
					pGoods->m_rwGoodcount[13] -= 1;
					//pGoods->m_rwGood.ReleaseWriteLock();
				}

				if (dwGoodsID/1000000==2)	//板块20110620
				{
					DWORD dwGroupID = dwGoodsID%1000000;
					CCalcGroup* pCalcGroup = theApp.GetCalcGroup(dwGroupID);
					if (pCalcGroup)
					{
						pCalcGroup->m_Mutex.Lock();

						pCalcGroup->m_pxValue[G_CPX] = nDay;
						pCalcGroup->m_dwCount++;

						pCalcGroup->m_Mutex.UnLock();
					}
				}
			}//end for

			CalcCPXStat();
		}
	}
	catch(...)
	{
	}

	//ASSERT(buf.GetBufferLen()==0);
}

void CL2DM_CPXData::Send(CBuffer& buf)
{
	buf.Add(_DataHeadLength_);

	SendHead(buf, _DATA_L2DM_CPX_);

	m_wStatus = CSockData::WantRecv;
}

void CL2DM_CPXData::AddASAlert( CGoods *pGoods ,int nDay,int nMin30,int nMin60)
{
	if (g_Alerts.m_dwValueDate == 0)
	{
		g_Alerts.m_wrAlarm.AcquireWriteLock();
		g_Alerts.m_dwValueDate = theApp.m_dwDateValue;
		g_Alerts.m_aiItem.SetSize(0,5000);
		g_Alerts.m_wrAlarm.ReleaseWriteLock();
	}

	if (theApp.m_dwDateValue != CMDSApp::g_dwSysYMD)
		return;
	if (CMDSApp::g_dwSysHMS >= 150000)
		return;

	// 	if ((pGoods->m_dwGroup & (GROUP_ALL_A | GROUP_SH_B | GROUP_SZ_B)) ==0)
	// 		return;

	g_Alerts.m_wrAlarm.AcquireWriteLock();
	if (pGoods->m_nCPX_Day != nDay)
	{
		if (nDay==-1 || nDay==1)
		{
			CASAlertItem item;
			item.m_dwGoodID = pGoods->GetID();
			item.m_dwTime = CMDSApp::g_dwSysHMS;
			item.m_sAlertID = 1;
			item.m_cState = ((nDay==1)?'B':'S');
			//item.m_dwDate = g_Alerts.m_dwValueDate;		
			item.m_fPrice = pGoods->m_dwPrice;
			g_Alerts.m_aiItem.Add(item);
		}
	}

	if (pGoods->m_nCPX_Min30 != nMin30)
	{
		if (nMin30==-1 || nMin30==1)
		{
			CASAlertItem item;
			item.m_dwGoodID = pGoods->GetID();
			item.m_dwTime = CMDSApp::g_dwSysHMS;
			item.m_sAlertID = 2;
			item.m_cState = ((nMin30==1)?'B':'S');
			//item.m_dwDate = g_Alerts.m_dwValueDate;		
			item.m_fPrice = pGoods->m_dwPrice;
			g_Alerts.m_aiItem.Add(item);
		}
	}

	if (pGoods->m_nCPX_Min60 != nMin60)
	{
		if (nMin60==-1 || nMin60==1)
		{
			CASAlertItem item;
			item.m_dwGoodID = pGoods->GetID();
			item.m_dwTime = CMDSApp::g_dwSysHMS;
			item.m_sAlertID = 3;
			item.m_cState = ((nMin60==1)?'B':'S');
			//item.m_dwDate = g_Alerts.m_dwValueDate;		
			item.m_fPrice = pGoods->m_dwPrice;
			g_Alerts.m_aiItem.Add(item);
		}
	}
	g_Alerts.m_wrAlarm.ReleaseWriteLock();
}

void CL2DM_CPXData::CalcCPXStat()
{
	g_CpxStat.m_wrCpxStat.AcquireWriteLock();

	theApp.m_wrGoods.AcquireReadLock();
	WORD wGoods = (WORD)theApp.m_aGoods.GetSize();	

	ZeroMemory(g_CpxStat.m_aCPXStat,sizeof(CCPXStat)*3);

	for (WORD w=0; w<wGoods; w++)
	{
		CGoods* pGoods = (CGoods*)theApp.m_aGoods[w];
		if (pGoods==NULL)
			continue;

		int nIdx = -1;
		if (pGoods->m_hMaskGroup & GROUP_SH_A)
			nIdx = 0;
		else if (pGoods->m_hMaskGroup & (GROUP_SZ_A|GROUP_SZ_ZXB|GROUP_SZ_CYB))
			nIdx = 1;
		else if (pGoods->m_hMaskGroup & GROUP_HYBK)
			nIdx = 2;
		if (nIdx < 0)
			continue;

		CCPXStat &cpxstat = g_CpxStat.m_aCPXStat[nIdx];
		if (pGoods->m_nCPX_Day==-1)
			cpxstat.m_wSell++;
		else if (pGoods->m_nCPX_Day==1)
			cpxstat.m_wBuy++;
		else if (pGoods->m_nCPX_Day<-1)
			cpxstat.m_wOverSell++;
		else if (pGoods->m_nCPX_Day>1)
			cpxstat.m_wOverBuy++;
	}

	theApp.m_wrGoods.ReleaseReadLock();

	g_CpxStat.m_wrCpxStat.ReleaseWriteLock();
}

//////////////////////////////////////////////////////////////////////
//日线验证包代码 收盘后执行，必须在收盘后
CL2DM_GoodsCheckData::CL2DM_GoodsCheckData()
{
	m_dwLastSendTime = 0;
	m_bReceived = FALSE;
	m_file = NULL;
}

CL2DM_GoodsCheckData::~CL2DM_GoodsCheckData()
{
	if (m_file != NULL)
		fclose(m_file);
	m_file = NULL;
}

#define _CHECK_TIME_ 1830 //18:00 DM收盘后开始 
void CL2DM_GoodsCheckData::Send(CBuffer& buf)
{
	if (theApp.m_dwInitDate == CMDSApp::g_dwSysYMD && (CMDSApp::g_dwSysHMS/100 < _CHECK_TIME_)) //交易日必须收盘后处理
		return;

	if (theApp.m_dwCheckDate == theApp.m_dwInitDate)
		return;

	if (theApp.m_dwSaveDate == theApp.m_dwCheckDayDate)	//反向控制，如果当日同步过，就不在处理
		return;

	if (theApp.m_dwSaveDateHK != theApp.m_dwSaveDate) 
		return;

	if (theApp.m_dwSaveDateHK != theApp.m_dwInitDate) //yxf 20110917 港股收盘较晚
		return;

	if (m_dwLastSendTime != 0)
	{
		if (CMDSApp::g_dwSysSecond - m_dwLastSendTime < 60*5)//如果请求无数据则延迟5分钟在请求
			return;
	}

	if (m_file == NULL)
	{
		CString str;
		str.Format("%sData/CheckGoods_ds.txt", theApp.m_strSharePath.GetData());
		unlink(str.GetData());
		remove(str.GetData());
		m_file = fopen(str.GetData(), "w+");
	}

	m_dwLastSendTime = 0;
	m_bReceived = FALSE;
	buf.Add(_DataHeadLength_);

	buf.WriteInt(theApp.m_dwSaveDate);
	buf.WriteChar(1);//ds=1，dt=0 暂保留

	SendHead(buf, _DATA_L2DM_GoodCheckData_);

	m_wStatus = CSockData::WantRecv;
}

void CL2DM_GoodsCheckData::Recv(CDataHead& head, CBuffer& buf)
{
	CSockData::Recv(head, buf);

	try
	{
		CString str;
		std::map<DWORD,DWORD>::iterator goodsItor;
		WORD wNum = buf.ReadShort();

		if (wNum == 0)
		{
			if (m_bReceived) //接收成功完成
			{
				theApp.m_dwCheckDate = theApp.m_dwSaveDate;
				m_dwLastSendTime = 0;
			}
			else
			{
				theApp.m_dwCheckDate = 0;
				m_dwLastSendTime = CMDSApp::g_dwSysSecond;
			}
			m_wStatus = CSockData::WantSend;

			fclose(m_file);
			m_file = NULL;
			return;
		}

		LogInfo("====> CL2DM_GoodsCheckData::Recv %d",wNum);
		m_bReceived = TRUE;
		for (WORD w=0; w<wNum; w++)
		{
			DWORD dwGoodsID  = buf.ReadInt();
			DWORD dwCheckSum = buf.ReadInt();
			DWORD dwCheckCnt = buf.ReadInt();
			if (g_IsGoodsID(dwGoodsID)==FALSE)
				continue;

			//LogInfo("GoodsCheck: %d , %x , %d ",dwGoodsID, dwCheckSum, dwCheckCnt);
			CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
			if (pGoods)
			{
				pGoods->m_dwCheckSum = dwCheckSum;
				pGoods->m_dwCheckCnt = dwCheckCnt;

				if (m_file == NULL)
					continue;

				BOOL bWantSave = ((dwCheckSum & 0x0100) > 0);//必须等于WantSaveToday(TRUE)
				dwCheckSum = 0;

				CDay day;
				if (bWantSave)
				{
					// 增加一个计数器，用于记录当日DM收盘个股数量 [4/22/2013 frantqian]
					theApp.m_nDMCheckCount++;
					goodsItor = theApp.m_mapSaveGoodsID.find(pGoods->GetID());
					if (goodsItor == theApp.m_mapSaveGoodsID.end())
					{
						theApp.m_mapNotSaveGoodsID.insert(make_pair(pGoods->GetID(),pGoods->GetID()));
					}

					pGoods->GetToday(day);
					//dwCheckSum = day.GetCheckSum(TRUE,TRUE);//注意，这个m_dwCheckSum不准确，是调整后的值，不是原始文件中的值，所以有可能显示Not Match,但事实是匹配的
					dwCheckSum = day.GetCRC();
				}

				if ((dwCheckSum & 0xFF) == (pGoods->m_dwCheckSum & 0xFF))//当日数据校验与DM相等
				{
					str.Format("%07d\t%08X\t%d\r\n", dwGoodsID,pGoods->m_dwCheckSum,dwCheckCnt);
					fwrite(str.GetData(), 1, str.GetLength(), m_file);
				}
				else
				{
					str.Format("%07d\t%08X\t%d\t%02X MayBe Not Match!\r\n", dwGoodsID,pGoods->m_dwCheckSum,dwCheckCnt,dwCheckSum);//不确切
					fwrite(str.GetData(), 1, str.GetLength(), m_file);
				}
			}
		}
	}
	catch(...)
	{
	}

	//ASSERT(buf.GetBufferLen()==0);
}

//////////////////////////////////////////////////////////////////////
//日线校验包
CL2DM_DayCheckData::CL2DM_DayCheckData()
{
	m_dwLastPos = 0;
	m_dwGoodsID = 0;
}

void CL2DM_DayCheckData::Send(CBuffer& buf)
{
	if (theApp.m_dwSaveDate != theApp.m_dwCheckDate)
		return;

	if (theApp.m_dwSaveDate == theApp.m_dwCheckSaveDate)
		return;

	WORD nGoodsNum = WORD(theApp.m_aGoods.GetSize());

	if (m_dwLastPos == 0)
		LogInfo("日线校验文件开始！");

	for (;m_dwLastPos < nGoodsNum;m_dwLastPos++)
	{
		CGoods *pGoods = (CGoods*)theApp.m_aGoods[m_dwLastPos];
		if (pGoods==NULL)
			continue;
		DWORD dwGoodsID = pGoods->GetID();

		pGoods->m_dwCheckOK = FALSE;

		if ((pGoods->m_dwCheckSum == 0) && (pGoods->m_dwCheckCnt == 0))
			continue;

		if (SaveDayCheck(pGoods))
		{
			pGoods->m_dwCheckOK = TRUE;
			continue;
		}

		if (m_dwGoodsID != dwGoodsID)
		{
			buf.Add(_DataHeadLength_);
			m_dwGoodsID = dwGoodsID; //dwGoodsID 高字节空余，可做类型扩展用
			buf.WriteInt(m_dwGoodsID);
			m_aDayCheck.SetSize(0,1000);
			SendHead(buf, _DATA_L2DM_DayCheckData_);
			LogDbg("===>DS Update Goods CheckData: %d!",m_dwGoodsID);

			m_wStatus = CSockData::WantRecv;
			break;
		}

		LogInfo("===>%d 日线校验文件，二次校验失败!",dwGoodsID);
	}

	if (m_dwLastPos == nGoodsNum)
	{
		LogInfo("日线校验文件结束 total = %d！", nGoodsNum);
		m_dwLastPos = 0;
		theApp.m_dwCheckSaveDate = theApp.m_dwSaveDate;
	}
}

void CL2DM_DayCheckData::Recv(CDataHead& head, CBuffer& buf)
{
	CSockData::Recv(head, buf);
	CDayCheck dayCheck;

	DWORD dwGoodsID = buf.ReadInt();
	WORD wNum = buf.ReadShort();
	char cEnd = 0;

	if (m_dwGoodsID == dwGoodsID)
	{
		for(WORD i = 0;i < wNum;i++)
		{
			dayCheck.m_wDay = buf.ReadShort();
			dayCheck.m_bCheckSum = buf.ReadChar();
			m_aDayCheck.Add(dayCheck);
		}

		cEnd = buf.ReadChar();
		if (cEnd)
		{
			theApp.m_dfDayCheck.SaveData(dwGoodsID, m_aDayCheck, theApp.m_dwSaveDate, FALSE);
			m_wStatus = CSockData::WantSend;
		}
	}
	else
		m_wStatus = CSockData::WantSend;
}

BOOL CL2DM_DayCheckData::SaveDayCheck( CGoods *pGoods )
{
	CDayCheckArray aDayCheck;
	CDayCheck dayCheck;

	theApp.m_dfDayCheck.LoadData(pGoods->GetID(), aDayCheck);

	//bWantSave 必须等于WantSaveToday(TRUE)
	if (pGoods->m_cStation==6)	//WI
	{
		if (pGoods->m_dwCheckSum & 0x0100)
		{
			dayCheck.m_wDay = WORD(pGoods->m_dwCheckCnt >> 16);
			dayCheck.m_bCheckSum = (pGoods->m_dwCheckSum & 0xFF);
			aDayCheck.Change(dayCheck);
		}
	}
	else if (pGoods->m_dwCheckSum & 0x0100)
	{
		dayCheck.m_wDay = dayCheck.DateToWord(theApp.m_dwSaveDate);
		dayCheck.m_bCheckSum = (pGoods->m_dwCheckSum & 0xFF);
		aDayCheck.Change(dayCheck);
	}

	WORD wCheck = 0;
	if (aDayCheck.GetSize() > 0)
		wCheck = g_GetCRC((BYTE *)aDayCheck.GetData(),sizeof(CDayCheck) * aDayCheck.GetSize());

	if ((wCheck == (WORD)(pGoods->m_dwCheckSum >> 16)) && (aDayCheck.GetSize() == pGoods->m_dwCheckCnt))
	{
		if (pGoods->m_dwCheckSum & 0x0100)
		{
			if (aDayCheck.GetSize() <= 0)
				return FALSE;
			theApp.m_dfDayCheck.SaveData(pGoods->GetID(), aDayCheck, theApp.m_dwSaveDate, FALSE);
		}
		return TRUE;
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////////
//日线下载包
CL2DM_DayData::CL2DM_DayData()
{
	m_dwLastPos = 0;
	m_dwGoodsID = 0;
	m_cCheckDataFile = 0;
	bStartTest = false;
}

void CL2DM_DayData::Send(CBuffer& buf)
{ 
 	if (theApp.m_dwSaveDate != theApp.m_dwCheckDate)
 		return;
 
 	if (theApp.m_dwSaveDate != theApp.m_dwCheckSaveDate)
 		return;
 
 	if (theApp.m_dwSaveDate == theApp.m_dwCheckDayDate)
 		return;

	//////////////////////////////////////////////////////////////////////////
	// 测试代码 [4/25/2013 frantqian]
// 	if (bStartTest == false)
// 		return;
// 
//  	if (m_dwGoodsID == 0)
//  		LoadCheckGoodsFile();
	//////////////////////////////////////////////////////////////////////////

	if (m_cCheckDataFile == 0)
	{
		m_cCheckDataFile = (CheckDataFile()?2:1);
		if (m_cCheckDataFile != 2)
		{
			LogInfo("数据文件大小错误，需要同步数据文件才能执行校验！");
			return;
		}
	}

	if (m_cCheckDataFile != 2)
		return;

	WORD nGoodsNum = WORD(theApp.m_aGoods.GetSize());

	if(m_dwLastPos==0)
		LogInfo("日线同步开始！");	

	for (;m_dwLastPos < nGoodsNum;m_dwLastPos++)
	{
		CGoods *pGoods = (CGoods*)theApp.m_aGoods[m_dwLastPos];
		if (pGoods==NULL)
			continue;

		DWORD dwGoodsID = pGoods->GetID();
		if ((pGoods->m_dwCheckSum & 0x0100) == 0)
			continue;

		// 测试代码 [4/25/2013 frantqian]
		//SaveDayCheck(pGoods);

		// 测试时需要关闭 [4/25/2013 frantqian]
  		if (pGoods->m_dwCheckOK==FALSE)
  		{
  			LogInfo("===>个股: %d,%08X 校验数据失败，取消同步！",dwGoodsID,pGoods->m_dwCheckSum);			
  			continue;
  		}

		// 分钟K本机初步检查 [4/27/2013 frantqian]
		LocalMinCheck(pGoods);
		BOOL bSecondCheck = (m_dwGoodsID==dwGoodsID);
		if (CheckDayCheck(pGoods,bSecondCheck))
			continue;

		if (bSecondCheck == FALSE)
		{
			buf.Add(_DataHeadLength_);
			m_dwGoodsID = dwGoodsID;
			buf.WriteInt(m_dwGoodsID);//dwGoodsID 高字节空余，可做类型扩展用
			WORD wNum = m_aErrDay.GetSize();
			buf.WriteShort(wNum);
			for (int i = 0;i < m_aErrDay.GetSize();i++)
				buf.WriteShort(m_aErrDay[i]);
			SendHead(buf, _DATA_L2DM_DayData_);
			m_wStatus = CSockData::WantRecv;

			LogInfo("===>个股: %d,同步 %d 项数据",dwGoodsID,m_aErrDay.GetSize());
			break;
		}

		LogInfo("===>个股：%d 日线同步二次校验失败!",dwGoodsID);
	}

	if (m_dwLastPos == nGoodsNum)
	{
		m_dwLastPos = 0;
		m_cCheckDataFile = 0;
		theApp.m_dwCheckDayDate = theApp.m_dwSaveDate;
		theApp.SetProfileInt("System", "CheckDayDate", theApp.m_dwCheckDayDate);
		LogInfo("日线同步结束！");
		std::string strCheckInfo = "";
		theApp.CreateCheckAlertInfo(strCheckInfo);
		LogInfo("校验情况:%s",strCheckInfo.c_str());
	}
}

void CL2DM_DayData::Recv(CDataHead& head, CBuffer& buf)
{ 
	CSockData::Recv(head, buf);

	DWORD dwGoodsID = buf.ReadInt();
	WORD wNum = buf.ReadShort();
	char cEnd = 0;
	CDay day;

	if (m_dwGoodsID == dwGoodsID)
	{
		for(WORD i = 0;i < wNum;i++)
		{
			buf.Read(&day,sizeof(CDay));
			m_aDayData.Change(day);
		}
		cEnd = buf.ReadChar();
		if (cEnd)
			m_wStatus = CSockData::WantSend;
	}
	else
		m_wStatus = CSockData::WantSend;

	LogInfo("===>个股: %d,下载 %d 项数据",dwGoodsID,wNum);
}

//日线文件校验过程
BOOL CL2DM_DayData::CheckDayCheck( CGoods *pGoods, BOOL bSecondCheck )
{
	//数据写回
	if (bSecondCheck && m_aDayData.GetSize()>0)
	{
		if (SaveToDataFile(pGoods) == 0)
		{
			LOG_INFO("同步数据存储成功:GoodsID=%d, SaveSize=%d; ", pGoods->GetID(), m_aDayData.GetSize());
		}
		else
		{
			LOG_INFO("同步数据存储失败:GoodsID=%d, SaveSize=%d; ", pGoods->GetID(), m_aDayData.GetSize());
		}
	}

	m_aErrDay.SetSize(0,100);		//需要同步的数据
	m_aDayData.SetSize(0,1000);		//日线数据
	m_aDayCheck.SetSize(0,1000);	//日线校验数据

	DWORD dwGoodsID = pGoods->GetID();

	//读取校验
	CDayCheckArray aDayCheck;
	theApp.m_dfDayCheck.LoadData(dwGoodsID, aDayCheck);

	if (aDayCheck.GetSize()>0)
	{
		for (int i = 0;i < aDayCheck.GetSize();i++)
			m_aDayCheck.Change(aDayCheck[i]);
		aDayCheck.SetSize(0);
	}
	else
	{
		return TRUE;
	}

	// 日线文件选取走统一接口 [5/7/2013 frantqian]
	//读取日线
	CDataFile_Day* pDF = theApp.GetDataFile(pGoods->m_cStation, DAY_DATA, DAY_PERIOD);

	//CDataDay* pData = (CDataDay*)pGoods->LoadData2(DAY_DATA, 1);//不能用这个，LoadData2做过精度调整，不是原始数据
	CArray<CDay, CDay&> aDayLoad;	
	pDF->LoadData(dwGoodsID, aDayLoad);//20110720不考虑是否加载成功，以校验数据为准
	for(int i = 0;i < aDayLoad.GetSize();i++)
		m_aDayData.Change(aDayLoad[i]);
	aDayLoad.SetSize(0);

	std::map<DWORD,DWORD>::iterator dateItor;
	//校验比对
	BOOL bHadDel = FALSE;
	for (int i = 0;i < m_aDayCheck.GetSize();)
	{
		//尾部缺少
		if (i >= m_aDayData.GetSize())
		{
			LogInfo("===>个股：%d [%d]日期：%d 尾部缺失，将增加！",dwGoodsID,i,CDayCheck::WordToDate(m_aDayCheck[i].m_wDay));				

			CDay day;
			day.m_dwTime = CDayCheck::WordToDate(m_aDayCheck[i].m_wDay);
			m_aDayData.Change(day);
			m_aErrDay.Add(m_aDayCheck[i].m_wDay);
			i++;
			continue;
		}

		//日期无效
		if (m_aDayData[i].m_dwTime < 19900101 || m_aDayData[i].m_dwTime > CMDSApp::g_dwSysYMD)
		{
			LogInfo("===>个股：%d [%d]日期：%d 无效，将删除！",dwGoodsID,i,m_aDayData[i].m_dwTime);				

			m_aDayData.RemoveAt(i);
			bHadDel = TRUE;
			continue;
		}

		WORD wCurDay = 0;
		wCurDay = CDayCheck::DateToWord(m_aDayData[i].m_dwTime);

		if (m_aDayCheck[i].m_wDay == wCurDay)
		{
			BYTE bCheck = m_aDayData[i].GetCRC();
			if (bCheck != m_aDayCheck[i].m_bCheckSum)
			{
				LogInfo("===>个股：%d [%d]日期：%d 校验 %02X != %02X,将更新！",dwGoodsID,i,m_aDayData[i].m_dwTime,
					m_aDayCheck[i].m_bCheckSum,bCheck);				

				m_aErrDay.Add(wCurDay);
			} 
			i++;
		}
		else if (m_aDayCheck[i].m_wDay > wCurDay)
		{
			LogInfo("===>个股：%d [%d]日期：%d 小于校验日期 %d，将删除！",dwGoodsID,i,m_aDayData[i].m_dwTime,
				CDayCheck::WordToDate(m_aDayCheck[i].m_wDay));

			m_aDayData.RemoveAt(i);
			bHadDel = TRUE;

			dateItor = theApp.m_mapDelDateCount.find(m_aDayData[i].m_dwTime);
			if (dateItor != theApp.m_mapDelDateCount.end())
			{
				dateItor->second++;
			}
			else
			{
				theApp.m_mapDelDateCount.insert(std::make_pair(m_aDayData[i].m_dwTime, 1));
			}

		}
		else //if (m_aDayCheck[i].m_wDay < wCurDay)
		{
			LogInfo("===>个股：%d [%d]日期：%d 大于校验日期 %d，将增加！",dwGoodsID,i,m_aDayData[i].m_dwTime,
				CDayCheck::WordToDate(m_aDayCheck[i].m_wDay));

			CDay day;
			day.m_dwTime = CDayCheck::WordToDate(m_aDayCheck[i].m_wDay);
			m_aDayData.Change(day);
			m_aErrDay.Add(m_aDayCheck[i].m_wDay); 
			i++;
		}

		//尾部多出
		if(i == m_aDayCheck.GetSize())
		{
			int nCount = m_aDayData.GetSize() - i;
			if (nCount > 0)
			{
				LogInfo("===>个股：%d 日期：%d 起 尾部超出 %d 个数据，将删除！",dwGoodsID,m_aDayData[i].m_dwTime,nCount);
				m_aDayData.RemoveAt(i,nCount);
				bHadDel = TRUE;
			}
			break;
		}
	}

	//有删除但无同步，直接保存，否则同步后保存
	if (m_aErrDay.GetSize()==0 && bHadDel)
		SaveToDataFile(pGoods);

	//对本地分钟线文件进行检查，只能检查根数
	return (m_aErrDay.GetSize()==0);
}

int CL2DM_DayData::SaveToDataFile(CGoods *pGoods)
{
	CDataFile_Day* pDF = theApp.GetDataFile(pGoods->m_cStation, DAY_DATA, DAY_PERIOD);

	if (pDF)
		return pDF->SaveData(pGoods->GetID(), m_aDayData.GetData(), m_aDayData.GetSize(), theApp.m_dwDateValue, FALSE);
	else
		return -1;
}

BOOL CL2DM_DayData::CheckDataFile()
{
	UINT64 lFileSize = theApp.m_dfDayCheck.GetFileLength();
	if (lFileSize < 92*1024*1024)
		return FALSE;

	lFileSize = theApp.m_dfDay.GetFileLength();
	if (lFileSize < 672*1024*1024)
		return FALSE;

	lFileSize = theApp.m_dfDay_HK.GetFileLength();
	if (lFileSize < 302*1024*1024)
		return FALSE;

	return TRUE;
}

BOOL CL2DM_DayData::LoadCheckGoodsFile()
{
	CString strPath = theApp.m_strSharePath+"Data/CheckGoods_ds.txt";
	int  DatTime = 0;
	char pcDatName[256];

	CString	strClass;//类型名字

	std::string strLine;
	std::ifstream infile(strPath.GetData());
	if ( !infile )
		return -1;


	DWORD dwGoodsID  = 0;
	DWORD dwCheckSum = 0;
	DWORD dwCheckCnt = 0;

	while ( getline(infile, strLine, '\n') )
	{
		memset(pcDatName,0, 32);
		sscanf(strLine.c_str(), "%d %x %d", &dwGoodsID, &dwCheckSum, &dwCheckCnt);

		BOOL bWantSave = ((dwCheckSum & 0x0100) > 0);

		if (bWantSave == TRUE)
		{
			CGoods* pGoods = theApp.CreateGoods(dwGoodsID);

			if (pGoods != NULL)
			{
				pGoods->m_dwCheckSum = dwCheckSum;
				pGoods->m_dwCheckCnt = dwCheckCnt;
			}

		}
	}

	infile.close();
}

BOOL CL2DM_DayData::SaveDayCheck( CGoods *pGoods )
{
	CDayCheckArray aDayCheck;
	CDayCheck dayCheck;

	theApp.m_dfDayCheck.LoadData(pGoods->GetID(), aDayCheck);

	//bWantSave 必须等于WantSaveToday(TRUE)
	if (pGoods->m_cStation==6)	//WI
	{
		if (pGoods->m_dwCheckSum & 0x0100)
		{
			dayCheck.m_wDay = WORD(pGoods->m_dwCheckCnt >> 16);
			dayCheck.m_bCheckSum = (pGoods->m_dwCheckSum & 0xFF);
			aDayCheck.Change(dayCheck);
		}
	}
	else if (pGoods->m_dwCheckSum & 0x0100)
	{
		dayCheck.m_wDay = dayCheck.DateToWord(theApp.m_dwSaveDate);
		dayCheck.m_bCheckSum = (pGoods->m_dwCheckSum & 0xFF);
		aDayCheck.Change(dayCheck);
	}

	WORD wCheck = 0;
	if (aDayCheck.GetSize() > 0)
		wCheck = g_GetCRC((BYTE *)aDayCheck.GetData(),sizeof(CDayCheck) * aDayCheck.GetSize());

	if ((wCheck == (WORD)(pGoods->m_dwCheckSum >> 16)) && (aDayCheck.GetSize() == pGoods->m_dwCheckCnt))
	{
		if (pGoods->m_dwCheckSum & 0x0100)
		{
			if (aDayCheck.GetSize() <= 0)
				return FALSE;
			theApp.m_dfDayCheck.SaveData(pGoods->GetID(), aDayCheck, theApp.m_dwSaveDate, FALSE);
		}
		return TRUE;
	}
	return FALSE;
}

#define MIN30KNUM_PER_DAY_A		8
#define MIN30KNUM_PER_DAY_HK	11
void CL2DM_DayData::LocalMinCheck( CGoods *pGoods )
{
	//只检查A股
	if ((pGoods->m_hMaskGroup&GROUP_ALL_A) == 0)
		return;

	int nKNum = MIN30KNUM_PER_DAY_A;

	CArray<CDay, CDay&> aDayLoad;	
	CDataFile_Day* pDF = NULL;
	int i = 0;
	int nKCount = 0;
	pDF = theApp.GetDataFile(pGoods->GetID()/1000000,MIN_DATA,MIN_30);

	if(pDF == NULL)
		return;

	pDF->LoadData(pGoods->GetID(), aDayLoad);//20110720不考虑是否加载成功，以校验数据为准

	nKCount = aDayLoad.GetSize();
	if ( nKCount < nKNum)
		return;

	DWORD dwTimeDay = 0;
	//DWORD  wTimeMin = 0;

	DWORD dwHigh = 0;
	DWORD dwLow = 0;

	if ( nKCount < nKNum)
	{
		theApp.m_dwMinKErrCount ++;
		return;
	}

	dwTimeDay = 2000*10000 + aDayLoad[nKCount-nKNum].m_dwTime/10000;		// yyyymmdd
	//wTimeMin = aDayLoad[nKCount-nKNum].m_dwTime%10000;					// hhmm
	if (dwTimeDay != theApp.g_dwSysYMD)
	{
		theApp.m_dwMinKErrCount ++;
	}

	for (i = 0; i < nKNum; i++)
	{
		dwHigh = aDayLoad[nKCount-i-1].m_dwHigh;
		dwLow = aDayLoad[nKCount-i-1].m_dwLow;

		if (dwHigh == dwLow )
		{
			theApp.m_dwMinKErrValue ++;
			break;
		}
	}	

}


CCompressSockData::CCompressSockData()
{
	m_bufComp.m_bSingleRead = true;
///	m_bufComp.Initialize(_SendBuffer_Size_, true, 0, &g_BuddyPoolDay);

	m_nCompRecvInBuffer = 0;
}

CCompressSockData::~CCompressSockData()
{
	// 此处会当机，修改看看效果 [12/20/2012 frantqian]
// 	if (m_compRecv.m_pcIn)
// 		FreeBuffer(m_compRecv.m_pcIn);
	if (m_compRecv.m_pcIn)
		delete [] m_compRecv.m_pcIn;
}

//void CCompressSockData::SetBuddyPool(CBuddyPagePool* pBuddyPool)
//{
//	CSockData::SetBuddyPool(pBuddyPool);
//
//	m_compSend.m_pBuddyPool = pBuddyPool;
//	m_compRecv.m_pBuddyPool = pBuddyPool;
//
//	m_bufComp.Initialize(_SendBuffer_Size_, true, 0, m_pBuddyPool);
//}

BOOL CCompressSockData::RecvCompressData(CBuffer& buf)
{
	try
	{
		m_bufComp.ClearBuffer();

		m_compRecv.m_lIn = buf.ReadInt();
		if (m_compRecv.m_lIn==0)
			return TRUE;

		if (m_compRecv.m_pcIn==NULL || m_compRecv.m_lIn>m_nCompRecvInBuffer)
		{
			if (m_compRecv.m_pcIn)
				FreeBuffer(m_compRecv.m_pcIn);

			m_nCompRecvInBuffer = ((m_compRecv.m_lIn+PAGE_SIZE)/PAGE_SIZE)*PAGE_SIZE;
			m_compRecv.m_pcIn = (char*)AllocBuffer(m_nCompRecvInBuffer);
			if (m_compRecv.m_pcIn==NULL)
				m_nCompRecvInBuffer = 0;
		}

		if (m_compRecv.m_pcIn==NULL)
			return FALSE;

		buf.Read(m_compRecv.m_pcIn, m_compRecv.m_lIn);
		if (m_compRecv.Decode())
		{
			m_bufComp.Write(m_compRecv.m_pcOut, m_compRecv.m_lOut);

			g_hCompIn += m_compRecv.m_lIn;
			g_hCompOut += m_compRecv.m_lOut;

			return TRUE;
		}
	}
	catch(...)
	{
	}

	return FALSE;
}

BOOL CCompressSockData::RecvCompressDataStock(CBuffer& buf)
{
	try
	{
		m_bufComp.ClearBuffer();

		int nLen = buf.ReadInt();
		if (nLen==0)
			return TRUE;

		if (nLen<0)
		{
			m_compRecv.m_lIn = -nLen;

			if (m_compRecv.m_pcIn==NULL || m_compRecv.m_lIn>m_nCompRecvInBuffer)
			{
				if (m_compRecv.m_pcIn)
					FreeBuffer(m_compRecv.m_pcIn);

				m_nCompRecvInBuffer = (m_compRecv.m_lIn+PAGE_SIZE)/PAGE_SIZE*PAGE_SIZE;
				m_compRecv.m_pcIn = (char*)AllocBuffer(m_nCompRecvInBuffer);
				if (m_compRecv.m_pcIn==NULL)
					m_nCompRecvInBuffer = 0;
			}

			if (m_compRecv.m_pcIn==NULL)
				return FALSE;

			buf.Read(m_compRecv.m_pcIn, m_compRecv.m_lIn);
			if (m_compRecv.Decode())
			{
				m_bufComp.Write(m_compRecv.m_pcOut, m_compRecv.m_lOut);

				g_hCompIn += m_compRecv.m_lIn;
				g_hCompOut += m_compRecv.m_lOut;

				return TRUE;
			}
		}
		else
		{
			m_bufComp.Write(buf.GetBuffer(buf.m_nReadPos), nLen);
			buf.SkipData(nLen);

			return TRUE;
		}
	}
	catch(...)
	{
	}

	return FALSE;
}

BOOL CCompressSockData::SendCompressData(CBuffer& buf)
{
	BOOL bRet = FALSE;

	UINT nLen = m_bufComp.GetBufferLen();
	if (nLen > 0)
	{
		try
		{
			m_compSend.m_pcIn = (char*)m_bufComp.GetBuffer();
			m_compSend.m_lIn = (long)nLen;

			if (m_compSend.Encode() && m_compSend.m_lOut>0 && m_compSend.m_pcOut)
			{
				buf.WriteInt(m_compSend.m_lOut);
				buf.Write(m_compSend.m_pcOut, m_compSend.m_lOut);
				bRet = TRUE;
			}
		}
		catch(...)
		{
			bRet = FALSE;
		}
	}

	return bRet;
}

void CCompressSockData::Compress2Buf(CBuffer& buf, WORD wDataType)
{
	buf.Add(_DataHeadLength_);

	if (SendCompressData(buf))
	{
		SendHead(buf, wDataType);

		m_wStatus = CSockData::WantRecv;
	}
	else
	{
		buf.ClearBuffer();
	}				
}

void CCompressSockData::CompressAck2Buf(CBuffer& buf)
{
	int nLen = m_bufComp.GetBufferLen();
	if (nLen==0)
		return;

	if (nLen > g_nMinCompressLen) //<=256不压缩
	{
		try
		{
			m_compSend.m_pcIn = (char*)m_bufComp.GetBuffer();
			m_compSend.m_lIn = (long)nLen;

			if (m_compSend.Encode() && 
				m_compSend.m_lOut>0 &&
//				m_compSend.m_lOut>m_compSend.m_lIn+m_compSend.m_lIn/10 && // 压缩率大于10%
				m_compSend.m_lOut<m_compSend.m_lIn-g_nMinCompressDel && 
				m_compSend.m_pcOut)
			{
				buf.Write(&m_head, _DataHeadLength_);
			
				buf.WriteInt(-m_compSend.m_lOut);
				buf.Write(m_compSend.m_pcOut, m_compSend.m_lOut);

				SendHead(buf, m_head.m_wDataType+1);

				return;
			}
		}
		catch(...)
		{
		}
	}

	try
	{
		buf.ClearBuffer();

		buf.Write(&m_head, _DataHeadLength_);
			
		buf.WriteInt(nLen);
		buf.Write(m_bufComp.GetBuffer(), nLen);

		SendHead(buf, m_head.m_wDataType+1);
	}
	catch (...)
	{
		buf.ClearBuffer();
	}
}


void CRecv_GoodsData::Read2GoodsBuf()
{
	int nLen = m_bufComp.ReadInt();

	m_bufGoods.ClearBuffer();
	m_bufGoods.Write(m_bufComp.GetBuffer(m_bufComp.m_nReadPos), nLen);
	m_bufComp.SkipData(nLen);
}


void CL2DM_Value5Data::Recv(CDataHead& head, CBuffer& buf)
{
	//	if (m_pChannel==NULL)
	//		return;

	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;

	try
	{
		if (RecvCompressData(buf)==FALSE)
		{
			LOG_ERR("==>CL2DM_Value5Data::Recv");
			ThrowError();
			return;
		}

		DWORD dwDate = m_bufComp.ReadInt();
		DWORD dwDateHK = m_bufComp.ReadInt();		
		DWORD dwDateSPQH = 0;
		if (pSocket->m_sVersion>=5)
			dwDateSPQH = m_bufComp.ReadInt();

		if ((dwDate>0 && g_IsDateGood(dwDate)==FALSE) ||
			(dwDateHK>0 && g_IsDateGood(dwDateHK)==FALSE) || 
			(dwDateSPQH>0 && g_IsDateGood(dwDateSPQH)==FALSE))
		{
			LOG_ERR("==>CL2DM_Value5Data::Recv"); ThrowError();
			return;
		}

		BOOL bWantInitDate = FALSE;

		if (dwDate>0 && theApp.m_dwDateValue!=dwDate)
		{
			bWantInitDate = TRUE;
			theApp.InitDate(dwDate, 1);
		}
		if (dwDateHK>0 && theApp.m_dwDateValueHK!=dwDateHK)
		{
			bWantInitDate = TRUE;
			theApp.InitDate(dwDateHK, 2);
		}
		if (dwDateSPQH>0 && theApp.m_dwDateValueSPQH!=dwDateSPQH)
		{
			bWantInitDate = TRUE;
			theApp.InitDate(dwDateSPQH, 3);
		}

		if (bWantInitDate)
		{
			LOG_ERR("==>CL2DM_Value5Data::Recv");
			ThrowError();
			return;
		}

		char pcName[LEN_STOCKNAME];
		BOOL bGoodsNameChange = FALSE;

		WORD wGoodsOld = theApp.m_wGoods;

		WORD wNum = m_bufComp.ReadShort();

		if (wNum>0)
			theApp.m_bGoodsChanged = true;

		char cStationSave = -1;

		for (WORD w=0; w<wNum; w++)
		{
			DWORD dwGoodsID = m_bufComp.ReadInt();

			if (g_IsGoodsID(dwGoodsID)==FALSE)
			{
				LOG_ERR("==>CL2DM_Value5Data::Recv");
				ThrowError();
				return;
			}

			CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
			if (pGoods)
			{
				char cStation = char(dwGoodsID/1000000);
				//				if (cStationSave!=cStation && cStation>=0 && cStation<STATION_NUM)
				//				{
				//					theApp.m_pStation[cStation].m_dwDateValue =  CMDSApp::g_dwSysYMD;
				//					theApp.m_pStation[cStation].m_dwSecondValue = g_dwSysSecond;
				//
				//					cStationSave = cStation;
				//				}
				try
				{
					Read2GoodsBuf();

					BYTE bCount = m_bufGoods.ReadChar();

					if (bCount & 0x01)
					{
						pGoods->m_cLevel = m_bufGoods.ReadChar();

						char cStatus = m_bufGoods.ReadChar();

						ZeroMemory(pcName, LEN_STOCKNAME);
						int nLen = m_bufGoods.ReadChar();
						m_bufGoods.Read(pcName, nLen);

						if (cStatus & 0x01)
						{
							nLen = m_bufGoods.ReadChar();
							ZeroMemory(pGoods->m_pcCode, LEN_STOCKCODE+1);	///20110826 sjp
							m_bufGoods.Read(pGoods->m_pcCode, nLen);
							pGoods->m_pcCode[LEN_STOCKCODE] = '\0';
						}

						if (cStatus & 0x02)
							pGoods->m_sTimeZone = m_bufGoods.ReadShort();

						if (cStatus & 0x04)
						{
							pGoods->m_tradeSession.m_cNum = m_bufGoods.ReadChar();
							if (pGoods->m_tradeSession.m_cNum>0 && pGoods->m_tradeSession.m_cNum<=MAX_TRADING_SESSION)
							{
								for (char c=0; c<pGoods->m_tradeSession.m_cNum; c++)
								{
									pGoods->m_tradeSession.m_pwTimeBegin[c] = m_bufGoods.ReadShort();
									pGoods->m_tradeSession.m_pwTimeEnd[c] = m_bufGoods.ReadShort();
								}
							}
							else
								pGoods->m_tradeSession.m_cNum = 0;
						}

						if (cStatus & 0x08)
						{
							DWORD dwDate = m_bufGoods.ReadInt();
							if (dwDate>0 && pGoods->m_dwDate!=dwDate)
							{
								if ( pGoods->m_dwDate>0 && pGoods->m_dwDateSave!=pGoods->m_dwDate)
								{
									pGoods->SaveToday();

									CHisMinDF* pHMDF = theApp.GetHisMinDF(pGoods->m_dwDate);
									if (pHMDF)
										pGoods->SaveHisMin(pHMDF, pGoods->m_dwDate);

									pGoods->m_dwDateSave = pGoods->m_dwDate;
								}

								pGoods->InitDay();
								pGoods->m_dwDate = dwDate;

								if (pGoods->m_dwDateBargain!=dwDate || pGoods->m_dwDateMinute!=dwDate)
								{
									if (pGoods->m_dwDateBargain!=dwDate)
									{
										RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);
										pGoods->ClearBargain(TRUE, dwDate);
									}

									if (pGoods->m_dwDateMinute!=dwDate)
									{
										RWLock_scope_rdlock scope_rdlock(pGoods->m_rwMinute);
										pGoods->ClearMinute(TRUE, dwDate);
									}
								}
							}
						}

						if (cStatus & 0x10)
						{
							pGoods->m_cTradingStatus = m_bufGoods.ReadChar();
							pGoods->m_cSecurityProperties = m_bufGoods.ReadChar();
						}

						if (cStatus & 0x20)
							pGoods->m_cDecimalPosition = m_bufGoods.ReadChar();

						if (memcmp(pGoods->m_pcName, pcName, LEN_STOCKNAME))
						{
							CopyMemory(pGoods->m_pcName, pcName, LEN_STOCKNAME);
							bGoodsNameChange = TRUE;
						}

						m_bufGoods.ReadString(pGoods->m_strEName);

						pGoods->m_dwClose = m_bufGoods.ReadInt();
						pGoods->m_dwA_H = m_bufGoods.ReadInt();

						pGoods->Goods2Group();
						pGoods->MakePY();

						CBitStream stream(&m_bufGoods, TRUE);
						if (pGoods->IsQH())
						{
							int nRet = CDynaCompress::ExpandStaticQH5(stream, pGoods);
							if (nRet<0)
							{
								//								if (g_bError2Log)
								//								{
								//									CString str;
								//									str.Format("%06d  SysTime=%d  ExpandStaticQH=%d",
								//										pGoods->m_dwNameCode, CMDSApp::g_tSysTime, nRet);
								//									theApp.m_log.Add(str);
								//								}
								LOG_ERR("%06d  SysTime=%d  ExpandStaticQH=%d",
									pGoods->m_dwNameCode, CMDSApp::g_tSysTime, nRet);
								ThrowError();
								return;
							}
						}
						else
						{
							int nRet = CDynaCompress::ExpandStatic5(stream, pGoods);
							if (nRet<0)
							{
								//								if (g_bError2Log)
								//								{
								//									CString str;
								//									str.Format("%06d  SysTime=%d  ExpandStatic=%d",
								//										pGoods->m_dwNameCode, CMDSApp::g_tSysTime, nRet);
								//									theApp.m_log.Add(str);
								//								}
								LOG_ERR("%06d  SysTime=%d  ExpandStaticQH=%d",
									pGoods->m_dwNameCode, CMDSApp::g_tSysTime, nRet);
								ThrowError();
								return;
							}
						}

						pGoods->m_dwCountStatic++;
					}

					if (bCount & 0x10)
					{
						char pcPreName[4];
						m_bufGoods.Read(pcPreName, 4);
						if (memcmp(pGoods->m_pcPreName, pcPreName, 4))
						{
							CopyMemory(pGoods->m_pcPreName, pcPreName, 4);
							pGoods->m_strPreName = pGoods->GetPreName();
						}

						pGoods->m_cTradeCode = m_bufGoods.ReadChar();

						if (bCount & 0x20)
						{
							pGoods->m_dwI_Num = m_bufGoods.ReadInt();
							if (pGoods->m_dwI_Num>0)
							{
								pGoods->m_dwI_Close = m_bufGoods.ReadInt();
								pGoods->m_dwI_Ave = m_bufGoods.ReadInt();
								pGoods->m_dwI_Amount = m_bufGoods.ReadInt();
								pGoods->m_dwI_AveGB = m_bufGoods.ReadInt();
								pGoods->m_dwI_SumSZ = m_bufGoods.ReadInt();
								pGoods->m_dwI_Percent = m_bufGoods.ReadInt();
								pGoods->m_dwI_SYL = m_bufGoods.ReadInt();
								pGoods->m_dwI_JBBS = m_bufGoods.ReadInt();
							}
						}

						pGoods->m_dwCountStatus++;
					}

					if (bCount & 0x06)		// 02, 04
					{
						// dynaCompress

						CBitStream stream(&m_bufGoods, TRUE);

						int nRet = pGoods->ExpandDynamicValue(stream);
						if (nRet<0)
						{
							//							if (g_bError2Log)
							//							{
							//								CString str;
							//								str.Format("%d  SysTime=%d  ExpandDynamicValue=%d",
							//									dwGoodsID, CMDSApp::g_tSysTime, nRet);
							//								theApp.m_log.Add(str);
							//							}
							LOG_ERR("%d  SysTime=%d  ExpandDynamicValue=%d",
								dwGoodsID, CMDSApp::g_tSysTime, nRet);
							ThrowError();
							return;
						}

						char cIndex = m_bufGoods.ReadChar();
						if (cIndex)
						{
							char cRD = m_bufGoods.ReadChar();

							if (pGoods->m_pIndex==NULL)
								pGoods->m_pIndex = new CIndex;

							if (pGoods->m_pIndex)
							{
								pGoods->m_pIndex->m_cRD = cRD;

								m_bufGoods.Read(pGoods->m_pIndex->m_plRD, cRD*4);

								pGoods->m_pIndex->m_wRise = m_bufGoods.ReadShort();
								pGoods->m_pIndex->m_wEqual = m_bufGoods.ReadShort();
								pGoods->m_pIndex->m_wFall = m_bufGoods.ReadShort();
								pGoods->m_pIndex->m_wRN = m_bufGoods.ReadShort();
								pGoods->m_pIndex->m_wDN = m_bufGoods.ReadShort();
								pGoods->m_pIndex->m_wRD = m_bufGoods.ReadShort();

							}
							else
								m_bufGoods.SkipData(cRD*4+12);
						}

						pGoods->m_nBKStrong = m_bufGoods.ReadInt();
						pGoods->m_dwSysAlert = ((m_bufGoods.ReadInt() & 0xFFFFFF)|(pGoods->m_dwSysAlert & 0xFF000000));//20120420

						if (dwGoodsID==1 || dwGoodsID==1399001) //20110625
						{
							if (theApp.m_dwTimeValue<pGoods->m_dwTime)
								theApp.m_dwTimeValue = pGoods->m_dwTime;
							//需要替代方案
							//							theApp.m_pdwStationTime[cStation] =  CMDSApp::g_dwSysYMD % 10000 * 1000000 + pGoods->m_dwTime;//CMDSApp::g_tSysTime;
						}
						if (dwGoodsID==5500001)
							theApp.m_dwTimeValueHK = pGoods->m_dwTime;

						//if (cStation == 0 || cStation == 1)
						//	theApp.m_pdwStationTime[cStation] =  CMDSApp::g_dwSysYMD % 10000 * 1000000 + CMDSApp::g_tSysTime;

						pGoods->AddDay();
					}

					if (bCount & 0x08)		// Matrix
					{
						CBitStream stream(&m_bufGoods, TRUE);
//						RWLock_scope_wrlock  wrlock(pGoods->m_matrix.m_MatrixLock);
						int nRet = CQMatrixCompress::ExpandQueueMatrix(pGoods->m_matrix, stream);
						if (nRet!=0)
						{
							//							if (g_bError2Log)
							//							{
							//								CString str;
							//								str.Format("%06d  m_dwDynamicTimeBuy=%d m_dwDynamicTimeSell=%d  ExpandQueueMatrix=%d",
							//									pGoods->m_dwNameCode, pGoods->m_matrix.m_dwDynamicTimeBuy, pGoods->m_matrix.m_dwDynamicTimeSell, nRet);
							//								theApp.m_log.Add(str);
							//							}

							LOG_ERR("%06d  m_dwDynamicTimeBuy=%d m_dwDynamicTimeSell=%d  ExpandQueueMatrix=%d",
								pGoods->m_dwNameCode, pGoods->m_matrix.m_dwDynamicTimeBuy, pGoods->m_matrix.m_dwDynamicTimeSell, nRet);
							ThrowError();
							return;
						}
					}
				}
				catch(...)
				{
					LOG_ERR("==>CL2DM_Value5Data::");
					ThrowError();
					return;
				}

				pGoods->CheckMainTrack();

				//				theApp.m_SortSH.Sort(pGoods);
				//				theApp.m_SortSZ.Sort(pGoods);
			}
		}

		//		theApp.FlushZDF40GoodsID();
		// 	CString str;
		// 	str.Format("+++===>Valuedata %d\n",wNum);
		// 	OutputDebugString(str);

		WORD wGoods = theApp.m_wGoods;
		//		if (bGoodsNameChange || wGoodsOld!=wGoods)
		//			theApp.CreateGoodsInfo();

		ASSERT(m_bufComp.GetBufferLen()==0);
	}
	catch(...)
	{
	}
}

void CL2DM_Value5Data::Send(CBuffer& buf)
{
	//	if (m_pChannel==NULL || theApp.IsInitingDate())
	//		return;

	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	buf.Add(_DataHeadLength_);
	/*
	m_wVersion = 1;

	if (pSocket)
	{
	if (pSocket->m_sVersion>=5)
	m_wVersion = 3;
	else if (pSocket->m_sVersion>=3)
	m_wVersion = 2;
	}

	SendHead(buf, _DATA_L2DM_Value5_ + (m_wVersion - 1) * 2);
	*/
	SendHead(buf, _DATA_L2DM_Value5_);

	m_wStatus = CSockData::WantRecv;
}


CL2DM_Minute5Data::CL2DM_Minute5Data()
{
	m_aMinuteTemp.SetSize(0, 60);
}

void CL2DM_Minute5Data::MakeGM()
{
	m_aGM.RemoveAt(0, m_aGM.GetSize());

	//	if (m_pChannel==NULL)
	//		return;
	LOG_ERR("MakeGM");

	CPtrArray aGoods;

	theApp.m_wrGoods.AcquireReadLock();
	aGoods.Copy(theApp.m_aGoods);
	theApp.m_wrGoods.ReleaseReadLock();

	WORD wGoods = (WORD)aGoods.GetSize();
	for (WORD w=0; w<wGoods; w++)
	{
		CGoods* pGoods = (CGoods*)aGoods[w];
		CGoodsMinute gm;

		gm.m_dwGoodsID = pGoods->GetID();
		//sam copy from pc client begin
		CopyMemory(gm.m_pcCode, pGoods->m_pcCode, LEN_STOCKCODE);
		gm.m_pcCode[LEN_STOCKCODE] = '\0';
		//sam copy from pc client end

		BOOL bIsGZQH = pGoods->IsQH();
		BOOL bIndex = (pGoods->m_pIndex != NULL);
		BOOL bIsLevel2 = (pGoods->m_cLevel == 2);

		RWLock_scope_wrlock scope_wrlock(pGoods->m_rwMinute);

		gm.m_wMinute = pGoods->m_wMinuteInDisk;
		gm.m_dwCheckSum = pGoods->m_dwMinuteCheckSumInDisk;

		WORD wMinute = pGoods->m_wMinute;
		if (pGoods->m_pMinuteBuffer && wMinute>1)
		{
			wMinute--;

			for (WORD i=0; i<wMinute; i++)
			{
				gm.m_dwCheckSum += pGoods->m_pMinuteBuffer[i].GetCheckSumDW(bIsGZQH, bIndex, bIsLevel2, 1);
			}

			gm.m_wMinute += wMinute;
		}


		m_aGM.Add(gm);
	}
}

void CL2DM_Minute5Data::Recv(CDataHead& head, CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;

	//	if (m_pChannel==NULL)
	//		return;

	try
	{
		if (RecvCompressData(buf)==FALSE)
		{
			LOG_ERR("===>CL2DM_Minute5Data");
			ThrowError();
			return;
		}

		char cStationSave = -1;

		WORD wGoodsNum = m_bufComp.ReadShort();
		WORD i=0;
		for (i=0; i<wGoodsNum; i++)
		{
			DWORD dwGoodsID = m_bufComp.ReadInt();

			if (g_IsGoodsID(dwGoodsID)==FALSE)
			{
				LOG_ERR("===>CL2DM_Minute5Data");
				ThrowError();
				return;
			}

			CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
			if (pGoods==NULL)
				break;

			char cStation = char(dwGoodsID/1000000);
			//			if (cStationSave!=cStation && cStation>=0 && cStation<STATION_NUM)
			//			{
			//				theApp.m_pStation[cStation].m_dwDateValue =  CMDSApp::g_dwSysYMD;
			//				theApp.m_pStation[cStation].m_dwSecondValue = g_dwSysSecond;
			//
			//				cStationSave = cStation;
			//			}


			RWLock_scope_rdlock scope_rdlock(pGoods->m_rwMinute);

			try
			{
				Read2GoodsBuf();

				CBitStream stream(&m_bufGoods, TRUE);

				int nRet = CHisCompress::ExpandMinute(stream, pGoods, m_aMinuteTemp, pSocket->m_sVersion);
				if (nRet<0)
				{
					//					if (g_bError2Log)
					//					{
					//						CString str;
					//						str.Format("%06d  %d ExpandMinute=%d", dwGoodsID, pGoods->m_dwTime, nRet);
					//						theApp.m_log.Add(str, TRUE);
					//					}
					LOG_ERR("%06d  %d ExpandMinute=%d", dwGoodsID, pGoods->m_dwTime, nRet);
					ThrowError();
					return;
				}
			}
			catch(...)
			{
				LOG_ERR("Exception CL2DM_Minute5Data");
				ThrowError();
				return;
			}
		}

		if (i==wGoodsNum)
			ASSERT(m_bufComp.GetBufferLen()==0);
	}
	catch(...)
	{
	}
}

void CL2DM_Minute5Data::Send(CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;

	//	if (m_pChannel==NULL || theApp.IsInitingDate())
	//		return;

	MakeGM();

	m_bufComp.ClearBuffer();

	m_bufComp.WriteInt(theApp.m_dwDateValue);

	WORD wSize = WORD(m_aGM.GetSize());
	m_bufComp.WriteShort(wSize);

	for (WORD w=0; w<wSize; w++)
	{
		CGoodsMinute& gm = m_aGM[w];

		m_bufComp.WriteInt(gm.m_dwGoodsID);
		m_bufComp.WriteShort(gm.m_wMinute);

		m_bufComp.WriteInt(gm.m_dwCheckSum);
	}

	Compress2Buf(buf, _DATA_L2DM_Minute5_);
}



void CL2DM_Bargain5Data::MakeGB()
{
	m_aGB.RemoveAt(0, m_aGB.GetSize());

	//	if (m_pChannel==NULL)
	//		return;

	CPtrArray aGoods;
	CGoodsBargain gb;

	theApp.m_wrGoods.AcquireReadLock();
	aGoods.Copy(theApp.m_aGoods);
	theApp.m_wrGoods.ReleaseReadLock();

	int nSize = (int)aGoods.GetSize();
	for (int i=0; i<nSize; i++)
	{
		CGoods* pGoods = (CGoods*)aGoods[i];

		RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);

		gb.m_dwGoodsID = pGoods->GetID();
		gb.m_dwBargain = pGoods->m_dwBargainTotal;
		gb.m_dwCheckSum = pGoods->m_dwBargainCheckSum;
		//sam copy from pc client begin
		CopyMemory(gb.m_pcCode, pGoods->m_pcCode, LEN_STOCKCODE);
		gb.m_pcCode[LEN_STOCKCODE] = '\0';
		//sam copy from pc client end

		m_aGB.Add(gb);
	}
}

void CL2DM_Bargain5Data::Recv(CDataHead& head, CBuffer& buf)
{
	//	if (m_pChannel==NULL)
	//		return;

	try
	{
		if (RecvCompressData(buf)==FALSE)
		{
			LOG_ERR("===>CL2DM_Bargain5Data");
			ThrowError();
			return;
		}

		WORD wGoodsNum = m_bufComp.ReadShort();
		WORD i=0; 
		for (i=0; i<wGoodsNum; i++)
		{
			DWORD dwGoodsID = m_bufComp.ReadInt();

			if (g_IsGoodsID(dwGoodsID)==FALSE)
			{
				LOG_ERR("===>CL2DM_Bargain5Data");
				ThrowError();
				return;
			}

			CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
			if (pGoods==NULL)
				break;

			RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);

			try
			{
				Read2GoodsBuf();

				CBitStream stream(&m_bufGoods, TRUE);

				char cClear = (char)stream.ReadDWORD(1);
				if (cClear)
				{
					DWORD dwDate = stream.ReadDWORD(32);
					pGoods->ClearBargain(TRUE, dwDate);
				}

				DWORD dwNum = CTradeCompress::ExpandNum(stream);
				if (dwNum>0)
				{
					int nRet = CTradeCompress::ExpandBargain(stream, pGoods, dwNum);
					if (nRet<0)
					{
						//						if (g_bError2Log)
						//						{
						//							CString str;
						//							str.Format("1.%06d  %d ExpandBargain()=%d %d", dwGoodsID, CMDSApp::g_tSysTime, nRet, dwNum);
						//							theApp.m_log.Add(str, TRUE);
						//						}

						LOG_ERR("1.%06d  %d ExpandBargain()=%d %d", dwGoodsID, CMDSApp::g_tSysTime, nRet, dwNum);
						ThrowError();
						return;
					}
				}
			}
			catch (...)
			{
				LOG_ERR("Exception");
				ThrowError();
				return;
			}
		}

		if (i==wGoodsNum)
			ASSERT(m_bufComp.GetBufferLen()==0);
	}
	catch(...)
	{
	}
}

void CL2DM_Bargain5Data::Send(CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	//	if (m_pChannel==NULL || theApp.IsInitingDate())
	//		return;

	MakeGB();

	m_bufComp.ClearBuffer();

	m_bufComp.WriteInt(theApp.m_dwDateValue);

	WORD wSize = WORD(m_aGB.GetSize());
	m_bufComp.WriteShort(wSize);

	for (WORD w=0; w<wSize; w++)
	{
		CGoodsBargain& gb = m_aGB[w];

		m_bufComp.WriteInt(gb.m_dwGoodsID);
		m_bufComp.WriteInt(gb.m_dwBargain);
		m_bufComp.WriteInt(gb.m_dwCheckSum);
	}

	Compress2Buf(buf, _DATA_L2DM_Bargain5_);
}

 
void CL2DM_Value6Data::Recv(CDataHead& head, CBuffer& buf)
{
	//	if (m_pChannel==NULL)
	//		return;
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;

	//////////////////////////////////////////////////////////////////////////
	// test code 用于测试VALUE错误，可以直接根据MDS.INI文件来读取相应的BUFF文件，进行还原分析 [2/4/2013 frantqian]
// 	bool isdebugbuff = false;
// 	if (isdebugbuff == true)
// 	{
// 		buf.ClearBuffer();
// 		CString errfilename = theApp.GetProfileString("valueerr", "errfile", "");
// 		//g_readRecvErr(buf,errfilename);
// 	}

	//////////////////////////////////////////////////////////////////////////
	try
	{
		if (RecvCompressData(buf)==FALSE)
		{
			LOG_ERR("CL2DM_Value6Data");
			ThrowError();
			return;
		}



		DWORD dwDate = m_bufComp.ReadInt();
		DWORD dwDateHK = m_bufComp.ReadInt();		
		DWORD dwDateSPQH = m_bufComp.ReadInt();

		if ((dwDate>0 && g_IsDateGood(dwDate)==FALSE) ||
			(dwDateHK>0 && g_IsDateGood(dwDateHK)==FALSE) || 
			(dwDateSPQH>0 && g_IsDateGood(dwDateSPQH)==FALSE))
		{
			LOG_ERR("CL2DM_Value6Data::Recv");
			ThrowError();
			return;
		}

		BOOL bWantInitDate = FALSE;

		if (dwDate>0 && theApp.m_dwDateValue!=dwDate)
		{
			bWantInitDate = TRUE;
			theApp.InitDate(dwDate, 1);
		}
		if (dwDateHK>0 && theApp.m_dwDateValueHK!=dwDateHK)
		{
			bWantInitDate = TRUE;
			theApp.InitDate(dwDateHK, 2);
		}
		if (dwDateSPQH>0 && theApp.m_dwDateValueSPQH!=dwDateSPQH)
		{
			//bWantInitDate = TRUE;
			//theApp.InitDate(dwDateSPQH, 3);
		}

		if (bWantInitDate)
		{
			LOG_ERR("CL2DM_Value6Data::Recv,dwDate = %d, dwDateHK = %d, dwDateSPQH = %d, m_dwDateValue = %d, m_dwDateValueHK = %d, m_dwDateValueSPQH = %d",
				dwDate, dwDateHK, dwDateSPQH, theApp.m_dwDateValue, theApp.m_dwDateValueHK, theApp.m_dwDateValueSPQH);
			//ThrowError();
		}

		BOOL bGoodsNameChange = FALSE;

		WORD wGoodsOld = theApp.m_wGoods;

		WORD wNum = m_bufComp.ReadShort();

		if (wNum>0)
			theApp.m_bGoodsChanged = true;

		char cStationSave = -1;
		CGoodsCode gcGoodsID;

		for (WORD w=0; w<wNum; w++)
		{
			gcGoodsID.Recv(m_bufComp, m_bCode);

			if (g_IsGoodsID(gcGoodsID.m_dwGoodsID)==FALSE)
			{
				ThrowError();
				return;
			}

			Read2GoodsBuf();

			CGoods* pGoods = theApp.CreateGoods(gcGoodsID);
			if (pGoods)
			{
				char cStation = g_ID2Station(gcGoodsID.m_dwGoodsID);
				/*
				if (cStationSave!=cStation && cStation>=0 && cStation<STATION_NUM)
				{
					theApp.m_pStation[cStation].m_dwDateValue = g_dwSysDate;
					theApp.m_pStation[cStation].m_dwSecondValue = g_dwSysSecond;

					cStationSave = cStation;
				}
				*/
				try{

					//pGoods->m_rwGood.AcquireWriteLock();
					// 更换锁 [3/5/2015 frant.qian]
					RWLock_scope_wrlock  rwlock(pGoods->m_rwGood);	
					pGoods->m_rwGoodcount[12] += 1;
				
					DoRecv(m_bufGoods,pGoods);				
                    //theApp.m_pRedisImporter->WriteQuote(pGoods);
				}
				catch(...){
					//pGoods->m_rwGood.ReleaseWriteLock();
					if( cStation == 5 ){
						LOG_ERR("%d %s CL2DM_Value6Data::Recv error, %d", gcGoodsID.m_dwGoodsID, (LPCTSTR)pGoods->GetName(), pGoods->m_cStation);
					}
					else{
						pGoods->m_rwGoodcount[12] -= 1;
						ThrowError();
					}
				}
			}
		}

		//		theApp.FlushZDF40GoodsID();
		// 	CString str;
		// 	str.Format("+++===>Valuedata %d\n",wNum);
		// 	OutputDebugString(str);

		WORD wGoods = theApp.m_wGoods;
		//		if (bGoodsNameChange || wGoodsOld!=wGoods)
		//			theApp.CreateGoodsInfo();

		ASSERT(m_bufComp.GetBufferLen()==0);
	}
	catch(...)
	{
		LOG_ERR("CL2DM_Value6Data::%s %d", __func__, __LINE__);
	}
}

BOOL CL2DM_Value6Data::DoRecv(CBuffer& buf, CGoods* pGoods)
{
	DWORD dwGoodsID = pGoods->GetID();

	char pcName[LEN_STOCKNAME];
	char pcCode[LEN_STOCKCODE+1];

	try
	{
		BOOL bGoods2Group = FALSE;

		BYTE bCount = buf.ReadChar();

		if (bCount & 0x01)
		{
			pGoods->m_cLevel = buf.ReadChar();
			DWORD dwPreClose = pGoods->m_dwClose = buf.ReadInt();

			WORD wStatus = buf.ReadShort();

			CopyMemory(pcName, pGoods->m_pcName, LEN_STOCKNAME);
			l_ReadCodeName(buf, pGoods->m_pcName, LEN_STOCKNAME);

			CopyMemory(pcCode, pGoods->m_pcCode, LEN_STOCKCODE);
			
			if (wStatus & 0x01)
				g_ReadCodeName(buf, pGoods->m_pcCode, LEN_STOCKCODE);
			else
				pGoods->m_pcCode[0] = 0;

			if (wStatus & 0x02)
				pGoods->m_sTimeZone = buf.ReadShort();
			else
				pGoods->m_sTimeZone = 480;

			if (wStatus & 0x04)
			{
				pGoods->m_tradeSession.Read(buf);
			}
			else{
				pGoods->m_tradeSession.m_cNum = 0;
			}

			if (wStatus & 0x08)
			{
				DWORD dwDate = buf.ReadInt();
				{
					pGoods->m_dwDate = dwDate;
					CHisMinDF* pHMDF = theApp.GetHisMinDF(pGoods->m_dwDate);
					if (pHMDF)
						pGoods->SaveHisMin(pHMDF, pGoods->m_dwDate);
				}
				if (dwDate>0 && pGoods->m_dwDate!=dwDate)
				{
					//								if (theApp.m_nChannelNo==theApp.m_nCurrentChannel && pGoods->m_dwDate>0 && pGoods->m_dwDateSave!=pGoods->m_dwDate)
					if (pGoods->m_dwDate>0 && pGoods->m_dwDateSave!=pGoods->m_dwDate)
					{
						pGoods->SaveToday();
						//需要替代方案
						CHisMinDF* pHMDF = theApp.GetHisMinDF(pGoods->m_dwDate);
						if (pHMDF)
							pGoods->SaveHisMin(pHMDF, pGoods->m_dwDate);

						pGoods->m_dwDateSave = pGoods->m_dwDate;
					}

					pGoods->InitDay();
					pGoods->m_dwDate = dwDate;
					pGoods->m_dwClose = dwPreClose;
					pGoods->ClearDay();

					if (pGoods->m_dwDateBargain!=dwDate || pGoods->m_dwDateMinute!=dwDate)
					{
						if (pGoods->m_dwDateBargain!=dwDate)
						{
							RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);
							pGoods->ClearBargain(TRUE, dwDate);
						}

						if (pGoods->m_dwDateMinute!=dwDate)
						{
							RWLock_scope_rdlock scope_rdlock(pGoods->m_rwMinute);
							pGoods->ClearMinute(TRUE, dwDate);
						}
					}
				}
			}

			if (wStatus & 0x10)
			{
				pGoods->m_cTradingStatus = buf.ReadChar();
				pGoods->m_cSecurityProperties = buf.ReadChar();
			}
			else
			{
				pGoods->m_cTradingStatus = 0;
				pGoods->m_cSecurityProperties = 'N';
			}

			if (wStatus & 0x20)
			{
				pGoods->m_cDotShow = buf.ReadChar();
				pGoods->m_cDotData = buf.ReadChar();
				pGoods->m_wVolInHand = buf.ReadShort();
			}
			else
			{
				pGoods->m_cDotShow = -1;
				pGoods->m_cDotData = -1;
				pGoods->m_wVolInHand = 0;
			}

			if (wStatus & 0x40)
			{
				buf.ReadString(pGoods->m_strEName);
			}

			if (wStatus & 0x80)
			{
				pGoods->m_dwA_H = buf.ReadInt();
			}

			if (wStatus & 0x100)
			{
				pGoods->m_cSecurityType = buf.ReadChar();

				if (m_bCode&& pGoods->m_cSecurityType==OPTIONS)
				{
					CGoodsCode gcBaseID = pGoods->m_Options.m_gcBaseID;

					pGoods->m_Options.Read(buf);
					/*
					if (pGoods->m_Options.m_gcBaseID!=gcBaseID)
					{
						//pGoods->m_pGoodsBase = NULL;
					}
					*/
				}
			}
			else
				pGoods->m_cSecurityType = -2;

			if (wStatus & 0x200)
				pGoods->m_dwLastDate = buf.ReadInt();
			else
				pGoods->m_dwLastDate = 0;

			if (wStatus & 0x400)
				pGoods->m_dwVolDiv = buf.ReadInt();
			else
				pGoods->m_dwVolDiv = 0;

			//CGoodsCode gcBaseID = pGoods->m_gcBaseID;

			if (wStatus & 0x800){
				//sam copy from pc client, but not sure it is ok or not
				//
				CGoodsCode code;
				code.Recv(buf,true);

				/*
				pGoods->m_cStation = char(code.m_dwGoodsID/1000000);
				pGoods->m_dwNameCode = code.m_dwGoodsID%1000000;

				memcpy(pGoods->m_pcCode, code.m_pcCode, LEN_STOCKCODE);
				*/
			}
			/*
			else
				pGoods->m_gcBaseID = 0;
			*/

			if (memcmp(pGoods->m_pcName, pcName, LEN_STOCKNAME))
			{
				//pGoods->m_dwDateGoodsInfo = g_dwSysDate;
				//pGoods->m_dwTimeGoodsInfo = g_dwSysTime;

				//pChannel->m_bWantCreateGoodsInfo = TRUE;
			}
			else if (memcmp(pGoods->m_pcCode, pcCode, LEN_STOCKCODE))
			{
				//pGoods->m_dwDateGoodsInfo = g_dwSysDate;
				//pGoods->m_dwTimeGoodsInfo = g_dwSysTime;
			}

			/*
			if (memcmp(pGoods->m_pcName, pcName, LEN_STOCKNAME))
				bGoodsNameChange = TRUE;
			*/

			bGoods2Group = TRUE;
			
			CBitStream stream(&buf, TRUE);

			BOOL bType = (BOOL)stream.ReadDWORD(1);

			if (bType)
			{
				int nRet = CDynaCompress::ExpandStaticQH6(stream, pGoods);
				if (nRet<0)
				{
					//								if (g_bError2Log)
					//								{
					//									CString str;
					//									str.Format("%06d  SysTime=%d  ExpandStaticQH6=%d",
					//										pGoods->m_dwNameCode, g_tSysTime, nRet);
					//									theApp.m_log.Add(str);
					//								}
					LOG_ERR("%08d  SysTime=%d  ExpandStaticQH6=%d",pGoods->GetID(), CMDSApp::g_dwSysHMS, nRet);
					//m_bufComp.m_nReadPos = _nReadPos;
					//w--;
					//std::string errfilename = g_getname(pGoods->GetID(),w);
					//g_writeRecvErr(buf, errfilename);
					//goto L_GOODSREADSTARTPOS;
					ThrowError();
				}
			}
			else
			{
				int nRet = CDynaCompress::ExpandStatic6(stream, pGoods);
				if (nRet<0)
				{
					//								if (g_bError2Log)
					//								{
					//									CString str;
					//									str.Format("%06d  SysTime=%d  ExpandStatic6=%d",
					//										pGoods->m_dwNameCode, g_tSysTime, nRet);
					//									theApp.m_log.Add(str);
					//								}
					LOG_ERR("%08d  SysTime=%d  ExpandStatic6=%d",pGoods->GetID(), CMDSApp::g_dwSysHMS, nRet);
					//m_bufComp.m_nReadPos = _nReadPos;
					//std::string errfilename = g_getname(pGoods->GetID(),w);
					//g_writeRecvErr(buf, errfilename);
					//goto L_GOODSREADSTARTPOS;
					ThrowError();
				}
			}

			pGoods->m_dwCountStatic++;
		}

		if (bCount & 0x10)
		{
			char pcPreName[4];
			buf.Read(pcPreName, 4);
			if (memcmp(pGoods->m_pcPreName, pcPreName, 4))
			{
				CopyMemory(pGoods->m_pcPreName, pcPreName, 4);
				pGoods->m_strPreName = pGoods->GetPreName();
			}

			pGoods->m_cTradeCode = buf.ReadChar();

			if (bCount & 0x20)
			{
				pGoods->m_dwI_Num = buf.ReadInt();
				if (pGoods->m_dwI_Num>0)
				{
					pGoods->m_dwI_Close = buf.ReadInt();
					pGoods->m_dwI_Ave = buf.ReadInt();
					pGoods->m_dwI_Amount = buf.ReadInt();
					pGoods->m_dwI_AveGB = buf.ReadInt();
					pGoods->m_dwI_SumSZ = buf.ReadInt();
					pGoods->m_dwI_Percent = buf.ReadInt();
					pGoods->m_dwI_SYL = buf.ReadInt();
					pGoods->m_dwI_JBBS = buf.ReadInt();
				}
			}

			if (bCount & 0x80)
			{
				buf.Read(pGoods->m_pcTradeCode, 4);
			}
			else
			{
				memset(pGoods->m_pcTradeCode, ' ', 4);
			}

			pGoods->m_dwCountStatus++;
			
			bGoods2Group = TRUE;
		}

		if (bGoods2Group)
			pGoods->Goods2Group();

        if (pGoods->m_aPY.GetSize() == 0)
            pGoods->MakePY();

		if (bCount & 0x02)		// 02
		{
			// dynaCompress
			CBitStream stream(&buf, TRUE);
			int nRet = pGoods->ExpandDynamicValue6(stream);
			if (nRet<0)
			{
				LOG_ERR("%08d  SysTime=%d  ExpandDynamicValue6=%d",pGoods->GetID(), CMDSApp::g_dwSysHMS, nRet);
				ThrowError();
				//pGoods->m_wr.ReleaseWriteLock();
				//return;
			}
			
			if (bCount & 0x04)
			{
				char cRD = buf.ReadChar();

				if (pGoods->m_pIndex==NULL)
					pGoods->m_pIndex = new CIndex;

				if (pGoods->m_pIndex)
				{
					pGoods->m_pIndex->m_cRD = cRD;

					buf.Read(pGoods->m_pIndex->m_plRD, cRD*4);

					pGoods->m_pIndex->m_wRise = buf.ReadShort();
					pGoods->m_pIndex->m_wEqual = buf.ReadShort();
					pGoods->m_pIndex->m_wFall = buf.ReadShort();
					pGoods->m_pIndex->m_wRN = buf.ReadShort();
					pGoods->m_pIndex->m_wDN = buf.ReadShort();
					pGoods->m_pIndex->m_wRD = buf.ReadShort();

				}
				else
					buf.SkipData(cRD*4+12);
			}

			if (bCount & 0x40)
			{
				pGoods->m_nBKStrong = buf.ReadInt();
				pGoods->m_dwSysAlert = ((buf.ReadInt() & 0xFFFFFF)|(pGoods->m_dwSysAlert & 0xFF000000));//20120420
			}

			if (bCount & 0x08)
			{
				pGoods->m_dwAuctionPrice = buf.ReadInt();
				pGoods->m_hAuctionQty = buf.ReadInt64();
			}
			else
			{
				pGoods->m_dwAuctionPrice = 0;
				pGoods->m_hAuctionQty = 0;
			}

			if (dwGoodsID==1 || dwGoodsID==1399001) //20110625
			{
				// 解决港股时间延迟问题 [3/21/2013 frantqian]
				if (theApp.m_dwTimeValue<pGoods->m_dwTime)
					theApp.m_dwTimeValue = pGoods->m_dwTime;
			}
			else if (dwGoodsID==5500001)
			{
				if (theApp.m_dwTimeValueHK<pGoods->m_dwTime)
					theApp.m_dwTimeValueHK = pGoods->m_dwTime;
			}
			// 外盘期货的更新时间会对国内期货市场有影响，改成只对国内期货市场更新时间 [4/3/2013 frantqian]
			//else if (pGoods->IsSPQH())
			else if (pGoods->m_cStation == 7)
			{
				if (theApp.m_dwTimeValueSPQH<pGoods->m_dwTime)
					theApp.m_dwTimeValueSPQH = pGoods->m_dwTime;

			}

			if (dwGoodsID == 1) 
				theApp.m_dwTimeSH = theApp.m_dwInitDate % 10000 * 1000000 + pGoods->m_dwTime;

			if (dwGoodsID == 1399001)
				theApp.m_dwTimeSZ = theApp.m_dwInitDate % 10000 * 1000000 + pGoods->m_dwTime;
			//if (cStation == 0 || cStation == 1)
			//	theApp.m_pdwStationTime[cStation] = g_dwSysDate % 10000 * 1000000 + g_tSysTime;

			pGoods->AddDay();
		}

		if( bCount & 0x06 ){
			//////////////////////////////////////////////////////////////////////////
			if (dwGoodsID==1 || dwGoodsID == 1399001 || dwGoodsID == 5500001){
				theApp.m_dwTime = pGoods->m_dwTime;
			}
		}

	}
	catch(...)
	{
		LOG_ERR("Exception,%d",pGoods->GetID());
		theApp.m_thDM.m_bWantRestart = TRUE;
		ThrowError();
	}

	pGoods->CheckMainTrack();
	
	//_nLastReadPos = _nReadPos;
	//				theApp.m_SortSH.Sort(pGoods);
	//				theApp.m_SortSZ.Sort(pGoods);
}



void CL2DM_Value6Data::Send(CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	buf.Add(_DataHeadLength_);

	if (m_bCode)
	{
		if (pSocket->m_sVersion>=11)
			SendHead(buf, _DATA_L2DM_Value6_+8);
		else
			SendHead(buf, _DATA_L2DM_Value6_+4);
	}
	else
		SendHead(buf, _DATA_L2DM_Value6_);

	m_wStatus = CSockData::WantRecv;
}


void CL2DM_Minute6Data::Recv(CDataHead& head, CBuffer& buf)
{

	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;


	try
	{		
		if (RecvCompressData(buf)==FALSE)
		{
			LOG_ERR("CL2DM_Minute6Data");
			ThrowError();
			return;
		}

		char cStationSave = -1;

		WORD wGoodsNum = m_bufComp.ReadShort();

		CGoodsCode gcGoodsID;
		WORD i=0;
		for (i=0; i<wGoodsNum; i++)
		{
			gcGoodsID.Recv(m_bufComp, m_bCode);

			if (g_IsGoodsID(gcGoodsID.m_dwGoodsID)==FALSE)
			{
				ThrowError();
				return;
			}


			CGoods* pGoods = theApp.CreateGoods(gcGoodsID);

			char cStation = g_ID2Station(gcGoodsID.m_dwGoodsID);
			RWLock_scope_wrlock scope_wrlock(pGoods->m_rwMinute);
			try
			{
				Read2GoodsBuf();

				CBitStream stream(&m_bufGoods, TRUE);

                int nBfmc = pGoods->m_wMinute;
				int nRet = CHisCompress::ExpandMinute6(stream, pGoods, m_aMinuteTemp, pSocket->m_sVersion);
				if (nRet<0)
				{
					LOG_ERR("%06d  %d ExpandMinute6=%d", gcGoodsID.m_dwGoodsID, pGoods->m_dwTime, nRet);
					if( cStation == 5 ){
						LOG_ERR("%d %s ExpandMinute6 error, %d", gcGoodsID.m_dwGoodsID, (LPCTSTR)pGoods->GetName(), pGoods->m_cStation);
					}
					else{ 
						ThrowError();
						this->m_pSocket->Close();//实现重连 By Steven
						return;
					}
				}

			}
			catch(...)
			{
				LOG_ERR("Exception");
				ThrowError();
				return;
			}

		}

		if (i==wGoodsNum)
			ASSERT(m_bufComp.GetBufferLen()==0);


	}
	catch(...)
	{
		LOG_ERR("CL2DM_Minute6Data::%s %d %d", __func__, __LINE__, m_wStatus);
		m_wStatus = CSockData::WantDelete;
	}	
}

void CL2DM_Minute6Data::Send(CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;
 

	MakeGM();

	m_bufComp.ClearBuffer();

	m_bufComp.WriteInt(theApp.m_dwDateValue);

	WORD wSize = WORD(m_aGM.GetSize());
	m_bufComp.WriteShort(wSize);

	for (WORD w=0; w<wSize; w++)
	{
		CGoodsMinute& gm = m_aGM[w];

		if (m_bCode==FALSE && g_IsNullID(gm.m_dwGoodsID))
			continue;

		m_bufComp.WriteInt(gm.m_dwGoodsID);
		if (m_bCode && g_IsNullID(gm.m_dwGoodsID))
			g_WriteCodeName(m_bufComp, gm.m_pcCode, LEN_STOCKCODE);

		m_bufComp.WriteShort(gm.m_wMinute);		
		m_bufComp.WriteInt(gm.m_dwCheckSum);
	}

	if (m_bCode)
	{
		if (pSocket->m_sVersion>=11)
			Compress2Buf(buf, _DATA_L2DM_Minute6_+6);
		else
			Compress2Buf(buf, _DATA_L2DM_Minute6_+2);
	}
	else
		Compress2Buf(buf, _DATA_L2DM_Minute6_);
}


void CL2DM_Bargain6Data::Recv(CDataHead& head, CBuffer& buf)
{
	//	if (m_pChannel==NULL)
	//		return;

	try
	{
		if (RecvCompressData(buf)==FALSE)
		{
			LOG_ERR("CL2DM_Bargain6Data::Recv");
			ThrowError();
			return;
		}

		WORD wGoodsNum = m_bufComp.ReadShort();
		CGoodsCode gcGoodsID;
		WORD i=0;
		for (i=0; i<wGoodsNum; i++)
		{
			gcGoodsID.Recv(m_bufComp, m_bCode);

			if (g_IsGoodsID(gcGoodsID.m_dwGoodsID)==FALSE)
			{
				ThrowError();
				return;
			}


			CGoods* pGoods = theApp.CreateGoods(gcGoodsID);

			if (pGoods==NULL)
				break;

			RWLock_scope_wrlock scope_wrlock(pGoods->m_rwBargain);
			try
			{
				Read2GoodsBuf();

				CBitStream stream(&m_bufGoods, TRUE);

				char cClear = (char)stream.ReadDWORD(1);
				if (cClear)
				{
					DWORD dwDate = stream.ReadDWORD(32);
					pGoods->ClearBargain(TRUE, dwDate);
				}

				DWORD dwNum = CTradeCompress::ExpandNum(stream);
				if (dwNum>0)
				{
					int nRet = CTradeCompress::ExpandBargain6(stream, pGoods, dwNum);
					if (nRet<0)
					{
						LOG_ERR("1.%06d  %d ExpandBargain6()=%d %d", gcGoodsID.m_dwGoodsID, CMDSApp::g_dwSysHMS, nRet, dwNum);
						ThrowError();
					}
				}
			}
			catch (...)
			{
				LOG_ERR("Exception");
				char cStation = g_ID2Station(gcGoodsID.m_dwGoodsID);
				if( cStation == 5 ){
						LOG_ERR("%d %s CL2DM_Bargain6Data::Recv error, %d", gcGoodsID.m_dwGoodsID, (LPCTSTR)pGoods->GetName(), pGoods->m_cStation);
				}
				else{
					ThrowError();
				}
			}

		}

		if (i==wGoodsNum)
			ASSERT(m_bufComp.GetBufferLen()==0);
	}
	catch(...)
	{
		m_wStatus = CSockData::WantDelete;
		LOG_ERR("CL2DM_Bargain6Data::%s %d", __func__, __LINE__);
	}
}

void CL2DM_Bargain6Data::Send(CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;

	MakeGB();

	m_bufComp.ClearBuffer();

	m_bufComp.WriteInt(theApp.m_dwDateValue);

	WORD wSize = WORD(m_aGB.GetSize());
	if( wSize > 1 ){
		wSize = 1;
	}
	m_bufComp.WriteShort(wSize);

	for (WORD w=0; w<wSize; w++)
	{
		CGoodsBargain& gb = m_aGB[w];

		gb.m_dwGoodsID = 1;
		gb.m_pcCode[0] = 0;
		gb.m_dwBargain = 0;
		gb.m_dwCheckSum = 0;

		m_bufComp.WriteInt(gb.m_dwGoodsID);
		if (m_bCode && g_IsNullID(gb.m_dwGoodsID))
			g_WriteCodeName(m_bufComp, gb.m_pcCode, LEN_STOCKCODE);

		m_bufComp.WriteInt(gb.m_dwBargain);
		m_bufComp.WriteInt(gb.m_dwCheckSum);
	}


	if (m_bCode)
	{
		if (pSocket->m_sVersion>=11){
			Compress2Buf(buf, _DATA_L2DM_Bargain6_+6);
		}
		else
			Compress2Buf(buf, _DATA_L2DM_Bargain6_+2);
	}
	else
		Compress2Buf(buf, _DATA_L2DM_Bargain6_);	
}


void CL2DM_MatrixData::MakeMatrix()
{
	m_aGoodsMatrix.RemoveAll();
/*
	CMyPtrArray aGoods;
	CGoodsMatrix gm;
	
	theApp.m_wrGoods.BeginRead();
	aGoods.Copy(theApp.m_aGoods);
	theApp.m_wrGoods.EndRead();
	
	WORD wGoods = aGoods.GetSize();
	for (WORD w=0; w<wGoods; w++)
	{
		CGoods* pGoods = (CGoods*)aGoods[w];
		if (pGoods->m_cStation>=2 || pGoods->m_cLevel!=2)
			continue;
		
		pGoods->m_wr.BeginRead();
		
		gm.m_dwGoodsID = pGoods->GetID();
		gm.m_dwTime = pGoods->m_matrix.m_dwDynamicTimeBuy;

		pGoods->m_wr.EndRead();

		m_aGoodsMatrix.Add(gm);
	}
*/
}

void CL2DM_MatrixData::Recv(CDataHead& head, CBuffer& buf)
{
	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket==NULL)
		return;

	BOOL bError = FALSE;

	try
	{
		if (RecvCompressData(buf)==FALSE)
		{
			ThrowError();
			return;
		}

		WORD wNum = m_bufComp.ReadShort();
		for (WORD w=0; w<wNum; w++)
		{
			DWORD dwGoodsID = m_bufComp.ReadInt();

			if (g_IsGoodsID(dwGoodsID)==FALSE)
			{
				ThrowError();
				return;
			}

			Read2GoodsBuf();


			CGoods* pGoods = theApp.CreateGoods(dwGoodsID);
			if (pGoods==NULL)
				break;

			if( DoRecv(pGoods,m_bufGoods) == FALSE ){
				LOG_ERR("error");
				if( pGoods->m_cStation== 5 ){
						LOG_ERR("%d %s CL2DM_MatrixData::Recv error, %d", pGoods->GetID(), (LPCTSTR)pGoods->GetName(), pGoods->m_cStation);
				}
			}
		}
	}
	catch(...)
	{
		bError = TRUE;
	}

	if (bError)
	{
		ThrowError();
		return;
	}
	else
		ASSERT(buf.GetBufferLen()==0);
}

BOOL CL2DM_MatrixData::DoRecv(CGoods* pGoods, CBuffer& buf)
{
	if (pGoods==NULL)
		return TRUE;
	CBitStream stream(&buf, TRUE);

	RWLock_scope_wrlock  wrlock(pGoods->m_rwGood);
	
	try
	{
		int nRet = CQMatrixCompress::ExpandMatrix(pGoods, stream, m_wVersion);
		if (nRet<0)
		{
			LOG_ERR("%d  ExpandMatrix=%d", pGoods->GetID(), nRet);
			return FALSE;
		}
	}
	catch(...)
	{
		return FALSE;
	}

	return TRUE;

}


void CL2DM_MatrixData::Send(CBuffer& buf)
{

	MakeMatrix();

	m_bufComp.ClearBuffer();

	m_bufComp.WriteInt(theApp.m_dwDateValue);

	WORD wSize = WORD(m_aGoodsMatrix.GetSize());
	m_bufComp.WriteShort(wSize);

	for (WORD w=0; w<wSize; w++)
	{
		CGoodsMatrix& Matrix = m_aGoodsMatrix[w];

		m_bufComp.WriteInt(Matrix.m_dwGoodsID);
		m_bufComp.WriteInt(Matrix.m_dwTime);
	}

	m_wVersion = 1;

	CL2DMSocket* pSocket = (CL2DMSocket*)m_pSocket;
	if (pSocket)
	{
		if (pSocket->m_sVersion>=7)
			m_wVersion = 3;
		else if (pSocket->m_sVersion>=6)
			m_wVersion = 2;
	}

	Compress2Buf(buf, _DATA_L2DM_Matrix_ + (m_wVersion - 1) * 2);
}

