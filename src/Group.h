// Group.h: interface for the CGroup class.
//
//////////////////////////////////////////////////////////////////////
#ifndef GROUP_H
#define GROUP_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

//add by liuchengzhu 20100112
#ifndef WIN32 
#include "SysGlobal.h"
#include "String.h"
#include "Array.h"
#include "PtrArray.h"
#endif
#include "Goods.h"
//end add by liuchengzhu 20100112

#include "Mutex.h"
#include "CalcGroup.h"

#define G_FIELDNUM	30
////总
//#define	G_SZ		0		//市值			(亿元*100)
//#define	G_SZ_B		1		//市值比
//#define	G_LTSZ		2		//流通市值		(亿元*100)
//#define	G_LTSZ_B	3		//流通市值比
//#define	G_VOLUMN	4		//成交量		(亿股*100)
//#define	G_AMOUNT	5		//成交金额		(亿元*100)
////平均
//#define	G_SY		6		//收益
//#define	G_GB		7		//股本
//#define	G_ZDF		8		//涨跌幅
//#define	G_HSL		9		//换手率
//#define	G_5HSL		10		//5日换手率
//#define	G_SYL		11		//市盈率
//#define	G_GPJS		12		//股票数量
//#define	G_SZJS		13		//上涨家数
//#define	G_XDJS		14		//下跌家数
//#define	G_QSG		15		//当日强势股
//#define	G_5QSG		16		//5日强势股
//#define	G_RSG		17		//当日弱势股
//#define	G_5RSG		18		//5日弱势股
//#define	G_5ZDF		19		//5日涨跌幅
//#define	G_STRONG	20		//对大盘的贡献(点数)


class CGroup  
{
public:
	CGroup();
	virtual ~CGroup();

	void Initial();
	void SetGroup(char);

	char Compare(CGroup*, short, char, char);

	void Calc();

	int GetValue(short, char);

	void ClearGoodsBK();

private:
	void Calc(int*, char);

public:
	Mutex	m_rCritical;

	int    m_nGroup;
	CString m_strClass;
	CString m_strGroup;
	CString m_strGroupFile;
	int m_plValue[G_FIELDNUM];
	int m_plSHValue[G_FIELDNUM];
	int m_plSZValue[G_FIELDNUM];

	CArray<DWORD,DWORD &> m_aGoodsID;
	CPtrArray m_aGoods;

	int m_lDate;
	int m_lTime;

	double m_fSumLTG;

	CGoods* m_pGoods;
};

#endif

