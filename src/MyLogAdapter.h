/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file MyLogAdapter.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 18 19:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef MYLOGADDAPTER_H
#define MYLOGADDAPTER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "AppLogger.h"

/**
* @brief class SimpleFile define 
*/
class MyLogAdapter : public LogConfig
{
private:
	std::string m_LogPath;

public:
	/**
	* @brief constructor
	*/
	MyLogAdapter(void);

	/**
	* @brief destructor
	*/
	virtual ~MyLogAdapter(void);

	/**
	* @brief set log path
	* @param path
	*/
	void SetLogPath(const std::string& path)
	{
		m_LogPath = path;
	}

	/**
	* @brief Get log path
	*/
	std::string GetLogPath() const
	{
		return m_LogPath;
	}

	/**
	* @brief Get Log File Count
	* @return file count
	*/
	virtual size_t GetLogFileCount()
	{
		return 4;
	}

	/**
	* @brief when create a new file call this, you can set your log file name here.
	* @param FileNameBase
	* @param FileName
	* @param Succeeded
	*/
	virtual void OnNewLogFile(std::string& FileNameBase, std::string& FileName, bool Succeeded);

	/**
	* @brief Get Log File Info. you can set file path, size, base name etc. here
	* @param FileID
	* @param FileInfo
	*/
	virtual void GetLogFileInfo(size_t FileID, LogFileInfo& FileInfo);

	/**
	* @brief Get Log Socket Server Info
	*/
	virtual bool GetLogSocketServerInfo(LogSocketInfo& ServerSocketInfo)
	{
		return false;
	}

	/**
	* @brief check which attr of log is enabled.
	* @param logType
	* @param logTarget
	* @param TargetID
	*/
	virtual bool IsLogEnabled(LogType logType, LogTarget logTarget, int TargetID);
};

#endif


