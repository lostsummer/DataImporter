/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file DESCoder.h
* @brief  
*
* $Author: panlishen$
* $Date: Sum Oct 9 17:35:45 UTC+0800 2011$
* $Revision$
*/
/* *********************************************************************** */

#ifndef _DESCODER_H_
#define _DESCODER_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "SysGlobal.h"
#include "String.h"


class CDESCoder
{
public:
	CDESCoder();
	~CDESCoder();

	CString Str2Des(const CString& strSource, const CString& strKey);
	CString Des2Str(const CString& strSource, const CString& strKey);

private:
	void LeftBitMove(int k[56], int offset);
	void KeyInitialize(int KeyData[64], int KeyArray[16][48]);
	void ReadDataToBirnaryIntArray(BYTE byteData[8], int bitData[64]);
	void GetEncryptResultOfByteArray(int data[64], BYTE value[8]);
	void LoopF(int M[64], int times, int flag, int keyarray[16][48]);
	void Encrypt(int encryptdata[64], int KeyArray[16][48], BYTE Result[8], int flags);
	void UnitDes(BYTE Data[8], BYTE Key[8], BYTE Result[8], int flags);

	void Hex2Byte(const CString& strHex, BYTE*& byteData, int nBufLen);
	void Byte2Hex(BYTE*& byteData, int nBufLen, CString& strHex);
};



#endif 


