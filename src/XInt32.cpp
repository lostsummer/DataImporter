// XInt32.cpp: implementation of the XInt32 class.
//
//////////////////////////////////////////////////////////////////////

#include "XInt32.h"

INT32 XInt32::m_nMaxBase = 0x0FFFFFFF;	// 28bit
INT32 XInt32::m_nMinBase = 0xF0000000;
BYTE  XInt32::m_cMaxMul = 7;

XInt32::XInt32()
{
	m_nBase = 0;
	m_cMul = 0;
}

XInt32::XInt32(const INT64 n)
{
	*this = n;
}

INT64 XInt32::GetValue() const
{
	INT64 n = m_nBase;

	for(BYTE c = 0; c < m_cMul; c++)
		n *= 16;

	return n;
}

XInt32 XInt32::operator=(const XInt32 x)
{
	m_nBase = x.m_nBase;
	m_cMul = x.m_cMul;

	return *this;
}

XInt32 XInt32::operator=(const INT64 n)
{
	INT64 nBase = n;
	m_cMul = 0;

	INT32 nMod = 0;

	while(nBase > m_nMaxBase || nBase < m_nMinBase)
	{
		nMod = (INT32)(nBase % 16);
		nBase /= 16;

		if(nMod >= 8)
			nBase++;
		else if(nMod <= -8)
			nBase--;

		m_cMul++;
		if(m_cMul >= m_cMaxMul)
			break;
	}

	m_nBase = (INT32)nBase;

	return *this;
}

XInt32 XInt32::operator+=(const INT64 n)
{
//	*this = *this+n;
	*this = GetValue() + n;
	return *this;
}

INT32 XInt32::GetRawData()
{
	return *(INT32*)this;
}

void XInt32::SetRawData(INT32 n)
{
	*(INT32*)this = n;
}

BOOL XInt32::operator==(const XInt32 x)
{
	INT64 n64 = *this;
	INT64 nX = x;

	return (n64 == nX);
}


