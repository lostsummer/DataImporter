// L2DSApp.cpp: implementation of the CGoodsJBM class.
//
//////////////////////////////////////////////////////////////////////
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>

#include <signal.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <dirent.h>

#include <execinfo.h>

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

#include "MDS.h"
#include "Data.h"
#include "ThreadMgr.h"
#include "MyThread.h"
#include "Goods.h"
#include "Sockets.h"

#include <sstream>
#include <stdlib.h>

const int DATA_FILE_MAPPING_SIZE_1 = 128 * 1024 * 1024;
const int DATA_FILE_MAPPING_SIZE_2 = 256 * 1024 * 1024;
const int CON_FILE_MAPPING_SIZE = 1 * 1024 * 1024;

CMDSApp theApp;

volatile time_t CMDSApp::g_tsL2DMTime = 0;
volatile DWORD CMDSApp::g_tL2DMTime = 0;
volatile time_t CMDSApp::g_tSysTime = 0;
volatile DWORD CMDSApp::g_dwSysYMD = 0;
volatile DWORD CMDSApp::g_dwSysHMS = 0;
volatile DWORD CMDSApp::g_dwSysSecond = 0;
volatile DWORD CMDSApp::g_dwSysWeek = 0;
volatile long long CMDSApp::g_llSysRecvByte = 0;        
volatile long long CMDSApp::g_llSysSendByte = 0;        
volatile DWORD CMDSApp::g_dwSendPack = 0;               
volatile DWORD CMDSApp::g_dwRecvPack = 0;
volatile int CMDSApp::g_iSysGnBkRiseTotal=0; //概念版块上涨个数
volatile int CMDSApp::g_iSysHyBkRiseTotal=0; 
volatile int CMDSApp::g_iSysDqBkRiseTotal=0; 
volatile int CMDSApp::g_iSysLzBkRiseTotal=0; 
volatile int CMDSApp::g_iSysSzbBkRiseTotal=0;
volatile int CMDSApp::g_currCwValue = 0;
volatile int CMDSApp::g_currCwIndex = 0;
volatile char CMDSApp::m_cHorsePowerLast = -1;
volatile DWORD CMDSApp::g_lastSysYMD = 0;

DWORD g_dwFreeDataDelay = 15;
BOOL g_bFreeDayData = FALSE;
BOOL g_bFreeMin1Data = TRUE;
BOOL g_bFreeMin5Data = TRUE;
BOOL g_bFreeMin30Data = FALSE;
BOOL g_bFreeMinuteData = FALSE;
BOOL g_bFreeBargainData = TRUE;
BOOL g_bFreeHisMinData = FALSE;
BOOL g_bSyncCQ = FALSE;
BOOL g_bSyncKL = FALSE;
BOOL g_bSyncKM = FALSE;

DWORD g_dwSaveMin1 = 10;//2400;
DWORD g_dwSaveMin5 = 25;//1200;
DWORD g_dwSaveMin30 = 300;//1200;
DWORD g_dwDayGrowBy = 500;
DWORD g_dwSaveHisMin = 7;
DWORD g_dwMin30Redis = 120;
DWORD g_dwMin60Redis = 120;
DWORD g_dwDayRedis = 120;
DWORD g_dwWeekRedis = 120;
DWORD g_dwMonthRedis = 120;

DWORD g_dwStartDateGZIC = 20150416;
DWORD g_dwStartDateGZIH = 20150416;
DWORD g_dwStartDateGZQH = 20100416;
DWORD g_dwStartDateTF = 20130906;
DWORD g_dwStartDateT = 20150320;
DWORD g_dwStartDateOptions = 20131226;

BOOL g_bWantSaveStation = 0;

BOOL g_bReCreate = 0;

int g_nLoadThread = 1;

//Steven New Begin
//20120319 sjp                                                                                                                                                  
INT64 g_hCompIn = 0;                                                                                                                                             
INT64 g_hCompOut = 0;                                                                                                                                            
//20120319 sjp    
int g_nMinCompressLen = 256;
int g_nMinCompressDel = 128;
//Steven New End


volatile DWORD CMDSApp::m_dwConIdle = 0;

time_t g_transdb_status = 0;
volatile bool CMDSApp::m_brunning = true;


//////////////////////////////////////////////////////////////////////
void CMDSApp::ChangeTime()
{
    time_t tm_t = time(NULL);
    g_tSysTime = tm_t;
    struct tm tm_s;
    struct tm* p = localtime_r(&tm_t, &tm_s);
    g_dwSysYMD = (p->tm_year+1900)*10000+(p->tm_mon+1)*100+p->tm_mday;
    g_dwSysHMS = p->tm_hour*10000+p->tm_min*100+p->tm_sec;
    g_dwSysSecond = p->tm_hour*3600+p->tm_min*60+p->tm_sec;
    g_dwSysWeek = p->tm_wday;
}
//////////////////////////////////////////////////////////////////////

CMDSApp::CMDSApp()
{
    m_wPortApp = 8080;
    m_wMaxFDS = 1000;
    m_wThreadNum = 8;

    m_dwDateValue = 0;
    m_dwTime = 0;
    m_dwTimeValue = 0;
    m_bGoodsChanged = false;

    m_wGoods = 0;
    m_pSH = NULL;
    m_pSZ = NULL;
    m_p300 = NULL;
    m_p016 = NULL;
    m_p905 = NULL;

    m_bInitingDate = false;
    m_bSaveingDate = false;

    m_pFileMapping1 = MAP_FAILED;
    m_pFileMapping2 = MAP_FAILED;

    m_thInitDay = NULL;
    m_thSaveDay = NULL;
    m_thSaveDayHK = NULL;

    m_pRedisImporterDB = NULL;
    m_pRedisImporterCQ = NULL;
    m_pRedisImporterKM = NULL;
    m_pRedisImporterKL = NULL;

    m_dwInitDate = 0;
    m_dwInitDateHK = 0;
    m_dwSaveDate = 0;
    m_dwSaveDateHK = 0;

    m_bInitingDate = false;
    m_bSaveingDate = false;
    m_bSaveingDateHK = false;

    m_dwCheckDayDate = m_dwCheckSaveDate = m_dwCheckDate = 0;
    m_bEnableCheckDay = FALSE;

    m_dwDPTJDate = 0;

    m_brunning = true;

    tzset();
    g_tSysTime = m_dwSysStartTime = time(NULL);

    m_nMaxNumOfHisQSend = 32;

    m_dwTimeSH = 0;
    m_dwTimeSZ = 0;

    m_dwTimeSH_bar = 0;
    m_dwTimeSZ_bar = 0;

    m_dwTimeSH_minute = 0;
    m_dwTimeSZ_minute = 0;

    m_cHorsePower = -1;
}

CMDSApp::~CMDSApp()
{
    DeleteGoods();
	if (m_pRedisImporterDB)
		delete m_pRedisImporterDB;
	if (m_pRedisImporterCQ)
		delete m_pRedisImporterCQ;
	if (m_pRedisImporterKM)
		delete m_pRedisImporterKM;
	if (m_pRedisImporterKL)
		delete m_pRedisImporterKL;
}

void CMDSApp::PrintInfo()
{
    LogInfo("--------------------------------------------------------------------");

    LogInfo("GoodsNum=%d", m_wGoods);
    if (m_pSH)
    {
        DWORD dwPrice = m_pSH->m_dwPrice;
        XInt32 xVolume = m_pSH->m_xVolume;
        XInt32 xAmount = m_pSH->m_xAmount;
        LogInfo("SH P=%d V=%s手 A=%s元", dwPrice, (LPCTSTR)g_Value2String(xVolume), (LPCTSTR)g_Value2String(xAmount/1000));
    }

}

BOOL CMDSApp::InitInstance()
{
    ChangeTime();
    m_nDMCheckCount = 0;
    m_nSaveDayCount = 0;
    m_nNoSaveDayCount = 0;
    m_dwMinKErrCount = 0;
    m_dwMinKErrValue = 0;
    m_dwErrThreadCount = 0;
    m_mapNotSaveGoodsID.clear();
    m_mapDelDateCount.clear();
    m_mapSaveGoodsID.clear();
    m_dwStartHMS = g_dwSysHMS;
    m_dwStartYMD = g_dwSysYMD;
    dwUpdateTime_TimeThread = g_dwSysHMS; 
    char workDir[256] = {0};
    if (getcwd(workDir, 256) == NULL)
    {
        printf("Get Work Directory fail!\n");
        return FALSE;
    }

    m_ModulePath.Format("%s", workDir);

    std::string logPath(workDir);
    logPath.append("/log");
    m_LogAdapter.SetLogPath(logPath);
    LogInitialize(&m_LogAdapter); // after this you can use logger

    LogApp("程序编译信息 %s %s", SubVersion, BuildTime);


#ifndef _DEBUG
    if (m_LockFile.AlreadyExist()) return FALSE;
#endif

    LogApp("Begin %s", __FUNCTION__);
    
    //配置管理
    if (!Load_Config()) return FALSE;

    ListAll();

    m_pTradeSession[0].m_cNum = 2;
    m_pTradeSession[0].m_pwTimeBegin[0] = 930;
    m_pTradeSession[0].m_pwTimeEnd[0] = 1130;
    m_pTradeSession[0].m_pwTimeBegin[1] = 1300;
    m_pTradeSession[0].m_pwTimeEnd[1] = 1500;

    m_pTradeSession[1].m_cNum = 3;              // 集合竞价
    m_pTradeSession[1].m_pwTimeBegin[0] = 915;
    m_pTradeSession[1].m_pwTimeEnd[0] = 925;
    m_pTradeSession[1].m_pwTimeBegin[1] = 930;
    m_pTradeSession[1].m_pwTimeEnd[1] = 1130;
    m_pTradeSession[1].m_pwTimeBegin[2] = 1300;
    m_pTradeSession[1].m_pwTimeEnd[2] = 1500;

    m_pTradeSession[2].m_cNum = 2;
    m_pTradeSession[2].m_pwTimeBegin[0] = 915;
    m_pTradeSession[2].m_pwTimeEnd[0] = 1130;
    m_pTradeSession[2].m_pwTimeBegin[1] = 1300;
    m_pTradeSession[2].m_pwTimeEnd[1] = 1515;

    m_pTradeSession[3].m_cNum = 2;
    m_pTradeSession[3].m_pwTimeBegin[0] = 930;
    m_pTradeSession[3].m_pwTimeEnd[0] = 1200;
    if (g_dwSysYMD>=20120305)
        m_pTradeSession[3].m_pwTimeBegin[1] = 1300;
    else
        m_pTradeSession[3].m_pwTimeBegin[1] = 1330;
    m_pTradeSession[3].m_pwTimeEnd[1] = 1600;


    m_strSharePath = GetProfileString("System", "SharePath", "/home/stock/");
    m_dwConIdle = GetProfileInt("idle", "Interval", 10);
    m_nFilterNumYear = GetProfileInt("System", "FilterNumMon", 3);
    m_nFilterNumHfYear = GetProfileInt("System", "FilterNumMon", 3);
    m_nFilterNumQuarter = GetProfileInt("System", "FilterNumMon", 3);
    m_nFilterNumMon = GetProfileInt("System", "FilterNumMon", 6);
    m_nFilterNumWeek = GetProfileInt("System", "FilterNumWeek", 10);
    m_nFilterNumDay = GetProfileInt("System", "FilterNumDay", 20);
    m_nFilterNumMin1 = GetProfileInt("System", "FilterNumMin1", 20);
    m_nFilterNumMin5 = GetProfileInt("System", "FilterNumMin5", 20);
    m_nFilterNumMin15 = GetProfileInt("System", "FilterNumMin15", 20);
    m_nFilterNumMin30 = GetProfileInt("System", "FilterNumMin30", 20);
    m_nFilterNumMin60 = GetProfileInt("System", "FilterNumMin60", 20);
    g_dwFreeDataDelay = GetProfileInt("System", "FreeDataDelay", 15);
    g_bFreeDayData = GetProfileInt("System", "FreeDayData", 0);
    g_bFreeMin1Data = GetProfileInt("System", "FreeMin1Data", 1);
    g_bFreeMin5Data = GetProfileInt("System", "FreeMin5Data", 1);
    g_bFreeMin30Data = GetProfileInt("System", "FreeMin30Data", 0);
    g_bFreeMinuteData = GetProfileInt("System", "FreeMinuteData", 0);
    g_bFreeBargainData = GetProfileInt("System", "FreeBargainData", 1);
    g_bSyncCQ = GetProfileInt("Redis", "SyncCQ", 0);
    g_bSyncKL = GetProfileInt("Redis", "SyncKL", 0);
    g_bSyncKM = GetProfileInt("Redis", "SyncKM", 0);
    g_dwMin30Redis = GetProfileInt("Redis", "NumMin30", g_dwMin30Redis);
    g_dwMin60Redis = GetProfileInt("Redis", "NumMin60", g_dwMin60Redis);
    g_dwDayRedis = GetProfileInt("Redis", "NumDay", g_dwDayRedis);
    g_dwWeekRedis = GetProfileInt("Redis", "NumWeek", g_dwWeekRedis);
    g_dwMonthRedis = GetProfileInt("Redis", "NumMonth", g_dwMonthRedis);
    
    LoadHZ();
    LoadDYZ();
    //分红配送
    if (m_fhsp.Read()==false)
    {
        LOG_ERR("Data/fhspL2.dat read exception!");
        //return FALSE;
    }

    //基本面
    if (m_jbm.Read()==false)
    {
        LOG_ERR("Data/jbmL2.dat read exception!");
        //return FALSE;
    }

    //历史流通股本
    m_hisGB.Read();

    //道破天机
    if(m_dptj.Read()==false)
    {
        LOG_ERR("Data/jjtj.dat read exception!");
        //return FALSE;
    }

    if(g_CpxStat.Read()==false)
    {
        LOG_ERR("Data/CpxStat.dat read exception!");
        //return FALSE;
    }

    if(!ReadGoods2())
    {
        LOG_ERR("ReadGoods2 faild!!");
    }


    if (!ReadGoods1())
    {
        LOG_ERR("ReadGoods1 faild!!");
        return FALSE;
    }

    LoadDynamicGroupTxt();
    UpdateGroupData();//检查GROUP/DATA文件夹是否有更新
    UpdateDynamicGroupTxt();
    
    //K线和成交明细数据
    m_dfDay.Initial(m_strSharePath+"Data/Day.dat");
    m_dfMin1.Initial(m_strSharePath+"Data/Min1.dat");
    m_dfMin5.Initial(m_strSharePath+"Data/Min5.dat");
    m_dfMin30.Initial(m_strSharePath+"Data/Min30.dat");

    m_dfDay_HK.Initial(m_strSharePath+"Data/Day_HK.dat");
    m_dfMin1_HK.Initial(m_strSharePath+"Data/Min1_HK.dat");
    m_dfMin5_HK.Initial(m_strSharePath+"Data/Min5_HK.dat");
    m_dfMin30_HK.Initial(m_strSharePath+"Data/Min30_HK.dat");

    m_dfDay_SPQH.Initial(m_strSharePath+"Data/Day_SPQH.dat");
    m_dfMin1_SPQH.Initial(m_strSharePath+"Data/Min1_SPQH.dat");
    m_dfMin5_SPQH.Initial(m_strSharePath+"Data/Min5_SPQH.dat");
    m_dfMin30_SPQH.Initial(m_strSharePath+"Data/Min30_SPQH.dat");

    m_dfDay_WI.Initial(m_strSharePath+"Data/Day_WI.dat");
    m_dfMin1_WI.Initial(m_strSharePath+"Data/Min1_WI.dat");
    m_dfMin5_WI.Initial(m_strSharePath+"Data/Min5_WI.dat");
    m_dfMin30_WI.Initial(m_strSharePath+"Data/Min30_WI.dat");

    CString sTemp;
    sTemp.Format("%sData/Min1_%d.dat", m_strSharePath.GetData(), g_dwSysYMD);
    m_dfTodayMin1.Initial(sTemp);

    for(int i=0; i<10; i++ ){
        char szIndex[64];
        sprintf(szIndex, "Data/HisMin.dat_%d", i);
        m_pdfHisMin[i].Initial(m_strSharePath + szIndex);
    }

    m_dfBargain.Initial(m_strSharePath+"Data/Bargain.dat");
    m_dfHisBK.Initial(m_strSharePath+"Data/HisBK.dat");
    m_dfHisBK.LoadData(1, m_aHisBK);

    CString strIndex;
    for (int i=0; i<10; i++)
    {
        strIndex.Format("_%d", i);
        m_pdfTrade[i].Initial(m_strSharePath+"Data/Trade.dat"+strIndex);
        m_pdfBargain[i].Initial(m_strSharePath+"Data/Bargain.dat"+strIndex);
        m_pdfPartOrder[i].Initial(m_strSharePath+"Data/PartOrder.dat"+strIndex);
        m_pdfMinute[i].Initial(m_strSharePath+"Data/Minute.dat"+strIndex);
    }

    m_dfBargainHK.Initial(m_strSharePath+"Data/BargainHK.dat");
    m_dfMinuteHK.Initial(m_strSharePath+"Data/MinuteHK.dat");

    m_dfBargainSPQH.Initial(m_strSharePath+"Data/BargainSPQH.dat3");
    m_dfMinuteSPQH.Initial(m_strSharePath+"Data/MinuteSPQH.dat3");

    m_dfBargainWI.Initial(m_strSharePath+"Data/BargainWI.dat");
    m_dfMinuteWI.Initial(m_strSharePath+"Data/MinuteWI.dat");

    m_Alarm.Init(theApp.m_strSharePath);
    m_SysAlarm.Init(theApp.m_strSharePath);
    m_IndGoods.Init(theApp.m_strSharePath);

    m_dwCheckDayDate = GetProfileInt("System", "CheckDayDate", 0);
    m_bEnableCheckDay = GetProfileInt("System", "EnableCheckDay", 0);
    m_dfDayCheck.Initial(m_strSharePath+"Data/DayCheck.dat");

    LoadGroup(14, "概念板块");
    LoadGroup(15, "行业板块2");
    LoadGroup(16, "地区板块");
    LoadGroup(25, "三级概念");
    LoadGroup(-1, "益盟板块");
    LoadGroup(46, "三级行业");

    ClearHotGroup();
    LoadHotGroup();// 热点板块 [11/6/2012 frantqian]

    CalcCurBK();

  
    m_thDM.m_pSocket->m_strAddress1 = m_thDM.m_pSocket->m_strCurAddr = GetProfileString("DM", "Address1");
    m_thDM.m_pSocket->m_wPort1 = m_thDM.m_pSocket->m_wCurPort = GetProfileInt("DM", "Port1");
    LogInfo("DM Address1[%s] Port1[%d]",m_thDM.m_pSocket->m_strAddress1.GetData(),m_thDM.m_pSocket->m_wPort1);
    m_thDM.m_pSocket->m_strAddress2 = GetProfileString("DM", "Address2");
    m_thDM.m_pSocket->m_wPort2 = GetProfileInt("DM", "Port2");
    LogInfo("DM Address2[%s] Port2[%d]",m_thDM.m_pSocket->m_strAddress2.GetData(),m_thDM.m_pSocket->m_wPort2);
    if (!m_thDM.Start(0.000002))//启动与L2DM之间的通讯处理
    {
        LOG_ERR("start DM thread fail!!");
        return FALSE;
    }


    if (!m_thSaveGoods.Start(3))
    {
        LOG_ERR("start save goods thread fail!!");
        return FALSE;
    }



    if (!m_thCurBK.Start(5))
    {
        LOG_ERR("start CurBK thread fail!!");
        return FALSE;
    }

    if (!m_OpenStockMgr.Start(30))
    {
        LOG_ERR("start open stock mgr fail!!");
        return FALSE;
    }

    if (!m_thLoadData.Start(2))
    {
        LOG_ERR("start Load Data Thread fail!!");
        return FALSE;
    }

    ThreadMgr::GetMe().Status();

    if (!m_thTimer.Start())
    {
        LOG_ERR("start timer thread fail!!");
        return FALSE;
    }

    m_pRedisImporterDB = new CRedisImporter();

	if (g_bSyncCQ)
	{
		m_pRedisImporterCQ = new CRedisImporter();
		m_thSyncCQ = new CMySyncRedisThread(SYNC_CQ);
		if (!m_thSyncCQ->Start(3))
		{
			LOG_ERR("start sync_cq thread fail!!");
			return FALSE;
		}
	} 
	if (g_bSyncKM)
	{
		m_pRedisImporterKM = new CRedisImporter();
		m_thSyncKM = new CMySyncRedisThread(SYNC_KM);
		if (!m_thSyncKM->Start(3))
		{
			LOG_ERR("start sync_km thread fail!!");
			return FALSE;
		}
	} 
	if (g_bSyncKL)
	{
		m_pRedisImporterKL = new CRedisImporter();
		m_thSyncKL = new CMySyncRedisThread(SYNC_KL);
		if (!m_thSyncKL->Start(3))
		{
			LOG_ERR("start sync_kl thread fail!!");
			return FALSE;
		}
	} 

    LogApp("End %s", __FUNCTION__);
    return TRUE;
}

void CMDSApp::ExitInstance()
{
    LogApp("Begin %s", __FUNCTION__);

	if(m_thSyncCQ)
		m_thSyncCQ->End();
	if(m_thSyncKM)
		m_thSyncKM->End();
	if(m_thSyncCQ)
		m_thSyncKL->End();

    m_thTimer.End();

    m_thDelData.End();

    m_thLoadData.End();

    m_OpenStockMgr.End();

    m_thCurBK.End();

    m_thSaveGoods.End();

    m_thDM.End();

    if (m_bGoodsChanged)
        WriteGoods();

    int nRet = msync(m_pFileMapping1, DATA_FILE_MAPPING_SIZE_1, MS_SYNC);
    munmap(m_pFileMapping1, DATA_FILE_MAPPING_SIZE_1);
    m_pFileMapping1 = MAP_FAILED;

    nRet = msync(m_pFileMapping2, DATA_FILE_MAPPING_SIZE_2, MS_SYNC);
    munmap(m_pFileMapping2, DATA_FILE_MAPPING_SIZE_2);
    m_pFileMapping2 = MAP_FAILED;

    g_CpxStat.Write(TRUE);

    g_LoadData.DeleteAll();

    m_Alarm.Exit();
    m_SysAlarm.Exit();
    m_IndGoods.Exit();

    DeleteGroup();

    //保存配置
    Save_Config();

    DeleteGoods();

    ThreadMgr::GetMe().Status();

    LogApp("End %s", __FUNCTION__);

    LogTerminate(); // after call this function, can't use log
}

void CMDSApp::Run()
{
    //{ hook signals.
    signal(SIGINT, _OnSignal);
    signal(SIGTERM, _OnSignal);
    signal(SIGABRT, _OnSignal);
    signal(SIGHUP, _OnSignal);
    signal(SIGPIPE, _OnSignal);
#ifdef _DEBUG
    signal(SIGSEGV, _OnSignal);
#endif
    //}

    while (m_brunning)
    {
        CheckExit();

        ChangeTime();

        CheckThread();
        sleep(1);
    }

    //{ unhook signals. 
    signal(SIGINT, 0);
    signal(SIGTERM, 0);
    signal(SIGABRT, 0);
    signal(SIGHUP, 0);
    signal(SIGPIPE, 0);
#ifdef _DEBUG
    signal(SIGSEGV, 0);
#endif
    //}
}


// Signal Handler
void CMDSApp::_OnSignal(int s)
{
    switch (s)
    {
#ifdef _DEBUG
    case SIGSEGV:
        _DumpStack();
        m_brunning = false;
        break;
#endif
    case SIGHUP: {
        LogInfo("开始××××收到信号重新加载基本面、板块数据、重新加载Redis数据×××××");
        if(theApp.m_fhsp.Read()==false)
        {
            LOG_ERR("Data/fhspL2.dat read exception!");
            //return;
        }
        if(theApp.m_jbm.Read()==false)
        {
            LOG_ERR("Data/jbmL2.dat read exception!");
            //return;
        }

        theApp.m_hisGB.Read();
        
        if(theApp.m_dptj.Read()==false)
        {
            LOG_ERR("Data/jjtj.dat read exception!");
            //return;
        }
        theApp.DeleteGroup();
        theApp.LoadGroup(14, "概念板块");
        theApp.LoadGroup(15, "行业板块2");
        theApp.LoadGroup(16, "地区板块");
        theApp.LoadGroup(25, "三级概念");
        theApp.LoadGroup(-1, "益盟板块");
        theApp.LoadGroup(46, "三级行业");
        
        theApp.ClearHotGroup();
        theApp.LoadHotGroup();// 热点板块 [11/6/2012 frantqian]

        theApp.LoadMemo();

		if (theApp.m_pRedisImporterDB)
		{
			delete theApp.m_pRedisImporterDB;
			theApp.m_pRedisImporterDB = new CRedisImporter();
		}

		if (theApp.m_thSyncCQ)
			theApp.m_thSyncCQ->End();
		if (theApp.m_thSyncKM)
			theApp.m_thSyncKM->End();
		if (theApp.m_thSyncKL)
			theApp.m_thSyncKL->End();

		sleep(10);


		if (theApp.m_thSyncCQ && theApp.m_pRedisImporterCQ)
		{
			theApp.FlushSub(SYNC_CQ);
			delete theApp.m_pRedisImporterCQ;
			theApp.m_pRedisImporterCQ = new CRedisImporter();
			theApp.m_thSyncCQ->Start(3);
		}

		if (theApp.m_thSyncKM && theApp.m_pRedisImporterKM)
		{
			theApp.FlushSub(SYNC_KM);
			delete theApp.m_pRedisImporterKM;
			theApp.m_pRedisImporterKM = new CRedisImporter();
			theApp.m_thSyncKM->Start(3);
		}

		if (theApp.m_thSyncKL && theApp.m_pRedisImporterKL)
		{
			theApp.FlushSub(SYNC_KL);
			delete theApp.m_pRedisImporterKL;
			theApp.m_pRedisImporterKL = new CRedisImporter();
			theApp.m_thSyncKL->Start(3);
		}

		theApp.m_pRedisImporterDB->WriteSpec();

        LogInfo("结束××××收到信号重新加载基本面、板块数据×××××");
        break;
	}
    case SIGPIPE:
    case SIGINT:
    case SIGTERM:
    case SIGABRT:
        LogInfo("××××收到信号%d, 程序退出 ×××××", s);
        m_brunning = false;
        break;
    }

    signal(s, _OnSignal);
}

void CMDSApp::_DumpStack()
{
    void *array[10];
    size_t size;
    char **strings;
    size_t i;

    size = backtrace(array, 10);
    strings = backtrace_symbols(array, size);

    printf("Obtained %zd stack frames.\n", size);

    for (i = 0; i < size; i++)
        printf("%s\n", strings[i]);

    free (strings);
}

void CMDSApp::_Dump2GDB(int s)
{
    char buf[1024];
    char cmd[1024];
    FILE *fh;

    snprintf(buf, sizeof(buf), "/proc/%d/cmdline", getpid());
    if(!(fh = fopen(buf, "r")))
        exit(0);
    if(!fgets(buf, sizeof(buf), fh))
        exit(0);
    fclose(fh);
    if(buf[strlen(buf) - 1] == '\n')
        buf[strlen(buf) - 1] = '\0';
    snprintf(cmd, sizeof(cmd), "gdb %s %d", buf, getpid());
    printf("%s", cmd);
    system(cmd);

    exit(0);
}

int CMDSApp::_kbhit() 
{
    static const int STDIN = 0;
    static bool initialized = false;    
    if (! initialized) 
    {
        termios term;
        tcgetattr(STDIN, &term);
        term.c_lflag &= ~ICANON;
        tcsetattr(STDIN, TCSANOW, &term);
        setbuf(stdin, NULL);
        initialized = true;
    }    

    int bytesWaiting;
    ioctl(STDIN, FIONREAD, &bytesWaiting);

    return bytesWaiting;
}

void CMDSApp::CheckExit()
{
    if (_kbhit())
    {
        int c = getchar();
        if(c == 'q' || c == 'Q')
        {
            m_brunning = false;
        }
        else if(c == 'p' || c == 'P')
        {
            PrintInfo();
        }
        else if (c == 'S')
            SaveDay(FALSE, FALSE);
        else if (c == 's')
            SaveDay(TRUE, FALSE);
        else if (c == 'I')
            InitDate(g_dwSysYMD, 0x01);
        else if (c == 'i')
            InitDate(g_dwSysYMD, 0x02);
        else if (c == 'l' || c == 'L')
        {
            Load_Config(true);
        }
        else if (c == 'r' || c == 'R')
        {
            theApp.DeleteGroup();
            theApp.LoadGroup(14, "概念板块");
            theApp.LoadGroup(15, "行业板块2");
            theApp.LoadGroup(16, "地区板块");
            theApp.LoadGroup(25, "三级概念");
            theApp.LoadGroup(-1, "益盟板块");
            theApp.LoadGroup(46, "三级行业");

            theApp.ClearHotGroup();
            theApp.LoadHotGroup();// 热点板块 [11/6/2012 frantqian]
        }

#ifdef _DEBUG
        else if (c == 't' || c == 'T')
        {
            theApp.SaveGoodsTxt();
        }
#endif
    }
}

void CMDSApp::OnTimer()
{   
    dwUpdateTime_TimeThread = g_dwSysHMS;
    sTimeThreadInfo = "save s&a";
#ifdef _DEBUG
    const int nAfterStartTime=60;
#else
    const int nAfterStartTime=600;
#endif

    if (!m_bSaveingDate && m_dwSaveDate!=g_dwSysYMD &&
        g_dwSysHMS>=170000 && (DWORD)g_tSysTime>m_dwSysStartTime+nAfterStartTime)
    {
        SaveDay(FALSE, TRUE);
    }
    else
    {
        // 查找不收盘原因 [4/22/2013 frantqian]
        /*if (g_dwSysHMS>=171500 && m_dwSaveDate!=g_dwSysYMD)
        {
            LogErr("沪深A不收盘：bSave = %d, SaveDate = %d, SysYMD = %d, SysTime = %d, SysStartTime+AfterStartTime = %d",
                m_bSaveingDate, m_dwSaveDate, g_dwSysYMD, g_tSysTime, m_dwSysStartTime+nAfterStartTime);
        }*/
    }

    sTimeThreadInfo = "save HK";
    if (!m_bSaveingDateHK && m_dwSaveDateHK!=g_dwSysYMD &&
        g_dwSysHMS>=173000 && (DWORD)g_tSysTime>m_dwSysStartTime+nAfterStartTime)
    {
        SaveDay(TRUE, TRUE);
    }
    else
    {
        /*if (g_dwSysHMS>=174500 && m_dwSaveDateHK!=g_dwSysYMD)
        {
            LogErr("港股不收盘：bSave = %d, SaveDate = %d, SysYMD = %d, SysTime = %d, SysStartTime+AfterStartTime = %d",
                m_bSaveingDateHK, m_dwSaveDateHK, g_dwSysYMD, g_tSysTime, m_dwSysStartTime+nAfterStartTime);
        }*/
    }

#ifdef _DEBUG
	#define S_TIME_OUT 120
#else
	#define S_TIME_OUT 120
#endif

    _mgr.Check(S_TIME_OUT);

    sTimeThreadInfo = "ResetCountVal";
    /////////////////////////////////////////////////
    // 根据m_dwStartYMD日期判断是否从cwb.txt里取数据,若当前交易日则取dm下发数据否则从cwb.txt取值.  [9/19/2014 %zhaolin%]
    if (m_dwStartYMD != g_dwSysYMD)
    {
        m_dwStartYMD =g_dwSysYMD; 
    }
    ////////////////////////////////////////////



#ifdef _DEBUG
    if (g_dwSysSecond%300==0)
    {
        CGoods::TraceCache();
    }
#endif

    sTimeThreadInfo = "CheckDataFile";
}


bool CMDSApp::ReadGoods1()
{
    LogTrace("%s", __FUNCTION__);

    int fd = -1;
    m_pFileMapping1 = MAP_FAILED;

    std::string mapPath(m_strSharePath);
    CString strGoodsFM = theApp.GetProfileString("System", "NameOfFM1", "GoodsFM1.dat");
    mapPath.append(strGoodsFM.GetData());
    fd = open(mapPath.c_str(), O_CREAT | O_RDWR , 0666);
    if (fd == -1)  
    {
        LOG_ERR("open %s failed, errno = %d ", mapPath.c_str(), errno);
        return false;
    }

    struct stat st;
    fstat(fd,&st);
    lseek(fd,st.st_size-1,SEEK_SET);
    write(fd,"",1);        //必须的，如果不设置，当写入数据的时候会遇到文件结束符，产生SIGBUS信号

    int nSizeOfFM = theApp.GetProfileInt("System", "SizeOfFM1", DATA_FILE_MAPPING_SIZE_1);
    m_pFileMapping1 = mmap(NULL, nSizeOfFM, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (m_pFileMapping1 == MAP_FAILED)
    {
        LOG_ERR("ReadGoods1 mmap failed, errno = %d ", errno);
        close(fd);
        fd = -1;
        return false;
    }

    if ( st.st_size == 0)
    {
        LOG_ERR("ReadGoods1 mmap failed, size与配置文件不一致！fdsize: %d", st.st_size);
        close(fd);
        fd = -1;
        return false;
    }

    //////////////////////////////////////////////////////////////////////////
    // 增加ReadGoodsStatus，ReadGoods1前后加标记，发现为1立即重置缓存,0:初始化，1:进入readgoods，2:readgoods完成 [2/25/2014 frantqian]
    int nReadGoodsStatus= GetProfileInt("System", "ReadGoodsStatus", 0);
    if (nReadGoodsStatus == 1)
    {
        m_dwDateValue = 0;
        m_dwTimeValue = 0;
        m_dwDateValueHK = 0;
        m_dwTimeValueHK = 0;
        WriteGoods();
        int size = 0;
        CopyMemory(m_pFileMapping2, &size, 4);
        theApp.SetProfileInt("System", "ReadGoodsStatus", 0);
        return false;
    }
    SetProfileInt("System", "ReadGoodsStatus", 1);

    //////////////////////////////////////////////////////////////////////////

    DWORD dwBuffer;
    CopyMemory(&dwBuffer, m_pFileMapping1, sizeof(int));
    if (dwBuffer>0)
    {
        PBYTE pcFileMapping = (PBYTE)m_pFileMapping1;

        CBuffer buf;
        buf.m_bSingleRead = true;
        buf.SetBuffer(pcFileMapping+sizeof(int), dwBuffer);

        try
        {
            WORD wVersion = buf.ReadShort();
            CTradeSession pTradeSession[4];
            for (int i=0; i<4; i++)
            {
                pTradeSession[i].m_cNum = buf.ReadChar();
                for (UINT c=0; c<pTradeSession[i].m_cNum; c++)
                {
                    pTradeSession[i].m_pwTimeBegin[c] = buf.ReadShort();
                    pTradeSession[i].m_pwTimeEnd[c] = buf.ReadShort();
                }
            }

            CopyMemory(theApp.m_pTradeSession, pTradeSession, 4*sizeof(CTradeSession));
            
            m_dwDateValue = buf.ReadInt();
            m_dwTimeValue = buf.ReadInt();

            m_dwDateValueHK = buf.ReadInt();
            m_dwTimeValueHK = buf.ReadInt();

            DWORD dwGoods = buf.ReadInt();

            // 增加缓存出错处理 [11/27/2013 frantqian]
            if (dwGoods > 100000)
            {
                LOG_ERR("MAPPING ERROR,SIZE:%d,RESET!", dwGoods);
                // 一旦出错全部清空 [11/27/2013 frantqian]
                m_dwDateValue = 0;
                m_dwTimeValue = 0;
                m_dwDateValueHK = 0;
                m_dwTimeValueHK = 0;
                WriteGoods();
                int size = 0;
                CopyMemory(m_pFileMapping2, &size, 4);
                return true;
            }

            for (DWORD d=0; d<dwGoods; d++)
            {
                DWORD dwGoodsID = buf.ReadInt();
                if (g_IsGoodsID(dwGoodsID) == FALSE)
                {
                    // 出错后清空 [11/28/2013 frantqian]
                    LOG_ERR("Error Goods %ld Read From Mapping File", dwGoodsID);
                    m_dwDateValue = 0;
                    m_dwTimeValue = 0;
                    m_dwDateValueHK = 0;
                    m_dwTimeValueHK = 0;
                    DeleteGoods();
                    WriteGoods();
                    int size = 0;
                    CopyMemory(m_pFileMapping2, &size, 4);

                    return true;
                }

                CGoods* pGoods = CreateGoods(dwGoodsID);
                if (pGoods)
                    pGoods->Read(buf, wVersion);
            }
        }
        catch(...)
        {
            LOG_ERR("Read %s exception ", mapPath.c_str());
            m_dwDateValue = 0;
            m_dwTimeValue = 0;
            m_dwDateValueHK = 0;
            m_dwTimeValueHK = 0;
            DeleteGoods();
            WriteGoods();
            int size = 0;
            CopyMemory(m_pFileMapping2, &size, 4);
            return false;
        }
    }

    LogInfo("Read %s successed!!", mapPath.c_str());

    SetProfileInt("System", "ReadGoodsStatus", 2);
    return true;
}

bool CMDSApp::ReadGoods2()
{
    LogTrace("%s", __FUNCTION__);

    int fd = -1;
    m_pFileMapping2 = MAP_FAILED;

    std::string mapPath(m_strSharePath);
    CString strGoodsFM = theApp.GetProfileString("System", "NameOfFM2", "GoodsFM2.dat");
    mapPath.append(strGoodsFM.GetData());
    fd = open(mapPath.c_str(), O_CREAT | O_RDWR , 0666);
    if (fd == -1)  
    {
        LOG_ERR("open %s failed, errno = %d ", mapPath.c_str(), errno);
        return false;
    }

    struct stat st;
    fstat(fd,&st);
    lseek(fd,st.st_size-1,SEEK_SET);
    write(fd,"",1);        //必须的，如果不设置，当写入数据的时候会遇到文件结束符，产生SIGBUS信号

    int nSizeOfFM = theApp.GetProfileInt("System", "SizeOfFM2", DATA_FILE_MAPPING_SIZE_2);
    m_pFileMapping2 = mmap(NULL, st.st_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (m_pFileMapping2 == MAP_FAILED)
    {
        LOG_ERR("mmap failed, errno = %d ", errno);
        close(fd);
        fd = -1;
        return false;
    }

    if ( st.st_size == 0)
    {
        LOG_ERR("ReadGoods2 mmap failed, size与配置文件不一致！fdsize: %d", st.st_size);
        close(fd);
        fd = -1;
        return false;
    }

    PBYTE pcFileMapping = (PBYTE)m_pFileMapping2;
    
    int i, nSize, nCount = 0;

    CopyMemory(&nSize, pcFileMapping, 4);
    nCount += 4;
    ASSERT(nSize>=0 && nSize<=211450/*16000*/);

    CGoods2Pos g2p;
    
    for (i=0; i<nSize; i++, nCount+=4)
    {
        CopyMemory(&(g2p.m_gcGoodsID.m_dwGoodsID), pcFileMapping+nCount, 4);
        g2p.m_dwPos = i+1;
        m_aGoods2Pos.Add(g2p);
    }

    LogInfo("Read %s(%d) successed!!", mapPath.c_str(), nSize);

    return true;
}

void CMDSApp::KLine2Redis(CGoods* pGoods, CRedisImporter* pRedisImporter, WORD wDataType, WORD wPeriod)
{

	DWORD dwNum = 0;
	DWORD *ppre = NULL;
	char * strType = NULL;

	switch (wDataType)
	{
	case MIN_DATA:
		if (wPeriod == MIN_30)
		{
			strType = (char *) "min30";
			dwNum = g_dwMin30Redis;
			ppre = &(pGoods->m_dwMin30_s);
		}
		else if (wPeriod == MIN_60)
		{
			strType = (char *) "min60";
			dwNum = g_dwMin60Redis;
			ppre = &(pGoods->m_dwMin60_s);
		}
		if (*ppre > 0 && (g_dwSysHMS < 100000 || g_dwSysHMS > 151500))
		{
			return;
		}
		break;
	case DAY_DATA:
		if (wPeriod == DAY_PERIOD)
		{
			strType = (char *) "day";
			dwNum = g_dwDayRedis;
			ppre = &(pGoods->m_dwDay_s);
		}
		else if (wPeriod == WEEK_PERIOD)
		{
			strType = (char *) "week";
			dwNum = g_dwWeekRedis;
			ppre = &(pGoods->m_dwWeek_s);
		}
		else if (wPeriod == MONTH_PERIOD)
		{
			strType = (char *) "month";
			dwNum = g_dwMonthRedis;
			ppre = &(pGoods->m_dwMonth_s);
		}
		if (*ppre > 0 && (g_dwSysHMS < 150000 || g_dwSysHMS > 151500))
		{
			return;
		}
		break;
	} 

	CArrayFDayMobile aFDay;
	pGoods->LoadDay(aFDay, wDataType, wPeriod, 2);
	DWORD now = aFDay.GetSize();
	if (now > 0)
	{
		if (wDataType == DAY_DATA && (g_dwSysHMS>=90000 && g_dwSysHMS<=150200))
		{
			now -= 1;
		}
	    else if (wDataType == MIN_DATA && 
				((g_dwSysHMS>=90000 && g_dwSysHMS<=113200) || (g_dwSysHMS>=130000 && g_dwSysHMS<=150200)))
		{
			now -= 1;
		}
	}


    int iTemp = (int)now - (int)dwNum;
	if (iTemp > 0 && (int)*ppre < iTemp)
		*ppre = iTemp;
	if (now > *ppre)
	{
		for (DWORD i=*ppre; i<now; i++)
		{
			pRedisImporter->WriteKline(pGoods, aFDay[i], strType);
		}
		*ppre = now;
		pRedisImporter->ExecutePipeline();
	}
}

void CMDSApp::Min1ToRedis(CGoods* pGoods, CRedisImporter *pRedisImporter)
{

    CArrayFDayMobile aFDay;
    pGoods->LoadToday(aFDay);
    int now = aFDay.GetSize();
    int pre = pGoods->m_wMin1_s;

    DWORD dwPauseHMS = 0;
    DWORD dwStopHMS = 0;

    if (pGoods->IsHK())
    {
        dwPauseHMS = 120300;
        dwStopHMS = 160300;
    }
    else if (pGoods->IsGZQH())
    {
        dwPauseHMS = 113300;
        dwStopHMS = 151800;
    }
    else
    {
        dwPauseHMS = 113300;
        dwStopHMS = 150300;
    }
    
	if (now > 0)  
	{
		if (g_dwSysHMS <= dwPauseHMS || (g_dwSysHMS >= 130000 && g_dwSysHMS  <= dwStopHMS))
			now -= 3;
	}
    if (now > pre)
    {
        for (int i=pre; i<now; i++)
        {
            pRedisImporter->WriteKline(pGoods, aFDay[i], "min1");
        }
        pGoods->m_wMin1_s = now;
        pRedisImporter->ExecutePipeline();
    }
}

void CMDSApp::FlushAllDB()
{
	if (!m_pRedisImporterDB)
		return;

	if (g_bSyncCQ)
	{
		m_pRedisImporterDB->ClearCQ();
	}

	if (g_bSyncKM)
	{
		m_pRedisImporterDB->ClearKM();
	}
	if (g_bSyncKL)
	{
		m_pRedisImporterDB->ClearKL();
	}

	theApp.m_wrGoods.AcquireReadLock();
	DWORD dwGoods = theApp.m_aGoods.GetSize();
	for (DWORD i=0; i<dwGoods; i++)
	{
		CGoods * pGoods = (CGoods *)theApp.m_aGoods[i];
		if (g_bSyncKM)
		{
			pGoods->m_wMin1_s = 0;
		}

		if (g_bSyncKL)
		{
			pGoods->m_dwMin30_s = 0;
			pGoods->m_dwMin60_s = 0;
			pGoods->m_dwDay_s = 0;
			pGoods->m_dwWeek_s = 0;
			pGoods->m_dwMonth_s = 0;
		}
	}
	theApp.m_wrGoods.ReleaseReadLock();
}

void CMDSApp::FlushSub(SyncType type)
{

	if (type == SYNC_CQ)
	{
		if (!m_pRedisImporterCQ)
			return;

		m_pRedisImporterCQ->ClearCQ();
		return;
	}
	else
	{
		if (type == SYNC_KM)
		{
			m_pRedisImporterKM->ClearKM();
		}
		else if (type == SYNC_KL)
		{
			m_pRedisImporterKL->ClearKL();
		}

		DWORD dwGoods = theApp.m_aGoods.GetSize();
		for (DWORD i=0; i<dwGoods; i++)
		{
			CGoods * pGoods = (CGoods *)theApp.m_aGoods[i];
			RWLock_scope_rdlock  rwlock(pGoods->m_rwGood);
			if (type == SYNC_KM)
			{
				pGoods->m_wMin1_s = 0;
			}
			else if (type == SYNC_KL)
			{
				pGoods->m_dwMin30_s = 0;
				pGoods->m_dwMin60_s = 0;
				pGoods->m_dwDay_s = 0;
				pGoods->m_dwWeek_s = 0;
				pGoods->m_dwMonth_s = 0;
			}
		}
	}
}
void CMDSApp::WriteGoods()
{
    if (m_pFileMapping1 != MAP_FAILED)
    {
        PBYTE pcFileMapping = (PBYTE)m_pFileMapping1;

        CBuffer buf;
        buf.SetBuffer(pcFileMapping+sizeof(int), 0);

        WORD wVersion = 5;
        m_wrGoods.AcquireReadLock();

        try
        {
            buf.WriteShort(wVersion);

            CTradeSession pTradeSession[4];
            CopyMemory(pTradeSession, theApp.m_pTradeSession, 4*sizeof(CTradeSession));

            for (int i=0; i<4; i++)
            {
                buf.WriteChar(pTradeSession[i].m_cNum);
                for (int c=0; c<pTradeSession[i].m_cNum; c++)
                {
                    buf.WriteShort(pTradeSession[i].m_pwTimeBegin[c]);
                    buf.WriteShort(pTradeSession[i].m_pwTimeEnd[c]);
                }
            }

            buf.WriteInt(m_dwDateValue);
            buf.WriteInt(m_dwTimeValue);

            buf.WriteInt(m_dwDateValueHK);
            buf.WriteInt(m_dwTimeValueHK);

            UINT uSizePos = buf.GetBufferLen();

            DWORD dwSize = 0;
            buf.WriteInt(dwSize);

            DWORD dwGoods = m_aGoods.GetSize();
            for (DWORD d=0; d<dwGoods; d++)
            {
                CGoods* pGoods = (CGoods*)m_aGoods[d];
                if (pGoods)
                {
                    if (pGoods->IsBad()==FALSE)
                    {
                        buf.WriteInt(pGoods->GetID());
                        pGoods->Write(buf, wVersion);
                        dwSize++;
                    }
                }
            }
        
            CopyMemory(buf.GetBuffer(uSizePos), &dwSize, sizeof(int));
        }
        catch(...)
        {
            LOG_ERR("Write %s exception ", theApp.GetProfileString("system", "NameOfFM1", "Data/GoodsFM1.dat").GetData());
        }

        m_wrGoods.ReleaseReadLock();

        DWORD dwBuffer = buf.GetBufferLen();
        CopyMemory(m_pFileMapping1, &dwBuffer, sizeof(int));
    }
}

void CMDSApp::DeleteGoods()
{
    m_wrGoods.AcquireWriteLock();

    int nGoods = m_aGoods.GetSize();
    for(int i=0; i<nGoods; i++)
    {
        CGoods* p = (CGoods*)m_aGoods[i];
        if (p)
        {
            delete p;
            p = 0;
        }
    }

    m_aGoods.RemoveAll();

    m_pSH = NULL;
    m_pSZ = NULL;
    m_p300 = NULL;
    m_p016 = NULL;
    m_p905 = NULL;

    m_wrGoods.ReleaseWriteLock();
}

int CMDSApp::AddGoods(CGoods* pGoods)
{
    m_wrGoods.AcquireWriteLock();

    DWORD dwGoodsID = pGoods->GetID();

    int nRet = -1;
    int nLeft = 0;
    int nRight = m_aGoods.GetSize()-1;
        
    if (nRight<nLeft)
    {
        m_aGoods.Add(pGoods);
        nRet = 0;
    }
    else
    {
        for ( ; nLeft<=nRight; )
        {
            CGoods* pG = (CGoods*)m_aGoods[nLeft];
            int nCompare = g_CompareGoods(dwGoodsID, pGoods->m_pcNameCode, pG->GetID(), pG->m_pcNameCode);

            if (nCompare<0)
            {
                m_aGoods.InsertAt(nLeft, pGoods);
                nRet = nLeft;
                break;
            }
            else if (nCompare==0)
                break;
        
            pG = (CGoods*)m_aGoods[nRight];
            nCompare = g_CompareGoods(dwGoodsID, pGoods->m_pcNameCode, pG->GetID(), pG->m_pcNameCode);

            if (nCompare>0)
            {
                m_aGoods.InsertAt(nRight+1, pGoods);
                nRet = nRight+1;
                break;
            }
            else if (nCompare==0)
                break;

            int nMid = (nLeft+nRight)/2;
            pG = (CGoods*)m_aGoods[nMid];
            nCompare = g_CompareGoods(dwGoodsID, pGoods->m_pcNameCode, pG->GetID(), pG->m_pcNameCode);

            if (nCompare>0)
                nLeft = nMid + 1;
            else if (nCompare==0)
                break;
            else
                nRight = nMid-1;
        }
    }

    if (nRet>=0)
    {
        if (dwGoodsID==1)
            m_pSH = pGoods;
    }
    
    m_wGoods = WORD(m_aGoods.GetSize());

    m_wrGoods.ReleaseWriteLock();

    return nRet;
}

CGoods* CMDSApp::GetGoods(char cStation, DWORD dwNameCode)
{
    return GetGoods(g_GetGoodsID(cStation, dwNameCode));
}

CGoods* CMDSApp::GetGoods(DWORD dwGoodsID)
{
    return GetGoods(CGoodsCode(dwGoodsID));
}

CGoods* CMDSApp::GetGoods(const CGoodsCode& gcGoodsID)
{
    m_wrGoods.AcquireReadLock();

    CGoods* pRet = NULL;
    int nLeft = 0;
    int nRight = m_aGoods.GetSize()-1;
    for ( ; nLeft<=nRight; )
    {
        int nMid = (nLeft+nRight)/2;
            
            CGoods* pGoods = (CGoods*)m_aGoods[nMid];
            int nCompare = g_CompareGoods(gcGoodsID.m_dwGoodsID, gcGoodsID.m_pcCode, pGoods->GetID(), pGoods->m_pcNameCode);
            if (nCompare==0)
            {
                pRet = pGoods;
                break;
            }
            else if (nCompare<0)
                nRight = nMid-1;
            else
                nLeft = nMid+1;
    }

    m_wrGoods.ReleaseReadLock();
    
    return pRet;
}

CGoods* CMDSApp::CreateGoods(DWORD dwGoodsID, BOOL bLoadDay)
{
    return CreateGoods(CGoodsCode(dwGoodsID), bLoadDay);
}

CGoods* CMDSApp::CreateGoods(const CGoodsCode& gcGoodsID, BOOL bLoadDay)
{
    if (gcGoodsID.m_dwGoodsID==0)
        return NULL;
    CGoods* pGoods = GetGoods(gcGoodsID);
    if (pGoods==NULL)
    {
        pGoods = new CGoods;
        if (pGoods)
        {
            pGoods->m_cStation = g_ID2Station(gcGoodsID.m_dwGoodsID);
            pGoods->m_dwNameCode = g_ID2NameCode(gcGoodsID.m_dwGoodsID);
            CopyMemory(pGoods->m_pcNameCode, gcGoodsID.m_pcCode, LEN_STOCKCODE);
            
            pGoods->CreateWR();
            pGoods->CreateData();

            if (m_pFileMapping2 != MAP_FAILED)
            {
                PBYTE pcFileMapping = (PBYTE)m_pFileMapping2;

                m_wrGoods2Pos.AcquireWriteLock();

                CGoods2Pos g2p;
                g2p.m_gcGoodsID = gcGoodsID;
                int nPos = m_aGoods2Pos.Find(g2p);
                if (nPos>=0)
                {
                    DWORD dwPos = m_aGoods2Pos[nPos].m_dwPos;
                    pGoods->SetBufferBase((pcFileMapping+GOODS_BUFFER*(dwPos+8)));
                    //pGoods->SetBufferBase((char*)(pcFileMapping+G2P_HEAD+GOODS_BUFFER*dwPos));

                    if (bLoadDay)
                        pGoods->InitDay();
                }
                else
                {
                    g2p.m_dwPos = (DWORD)m_aGoods2Pos.GetSize();
                    m_aGoods2Pos.Add(g2p);

                    pGoods->SetBufferBase((pcFileMapping+GOODS_BUFFER*(g2p.m_dwPos+8)));

                    g2p.m_dwPos++;
                    CopyMemory(pcFileMapping, &(g2p.m_dwPos), 4);
                    CopyMemory(pcFileMapping+g2p.m_dwPos*4, &(gcGoodsID.m_dwGoodsID), 4);
                }
                /*
                else
                {
                    g2p.m_dwPos = (DWORD)m_aGoods2Pos.GetSize();
                    m_aGoods2Pos.Add(g2p);

                    pGoods->SetBufferBase((pcFileMapping+GOODS_BUFFER*(g2p.m_dwPos+8)));
                    pGoods->SetBufferBase((char*)(m_pcGoodsFM2+G2P_HEAD+GOODS_BUFFER*g2p.m_dwPos));

                    g2p.m_dwPos++;
                    CopyMemory(pcFileMapping, &(g2p.m_dwPos), 4);
                    CopyMemory(pcFileMapping+g2p.m_dwPos*4, &dwGoodsID, 4);
                    
                    CopyMemory(m_pcGoodsFM2+28, &(g2p.m_dwPos), 4);

                    int nPos = g2p.m_dwPos*32;

                    CopyMemory(m_pcGoodsFM2+nPos, &gcGoodsID.m_dwGoodsID, 4);
                    CopyMemory(m_pcGoodsFM2+nPos+4, gcGoodsID.m_pcCode, LEN_STOCKCODE);
                }
                */
                m_wrGoods2Pos.ReleaseWriteLock();
            }

            pGoods->NewGoods();

            AddGoods(pGoods);

            if (gcGoodsID.m_dwGoodsID == 1)
                m_pSH = pGoods;
            else if (gcGoodsID.m_dwGoodsID == 300)
                m_p300 = pGoods;
            else if (gcGoodsID.m_dwGoodsID == 16)
                m_p016= pGoods;
            else if (gcGoodsID.m_dwGoodsID == 905)
                m_p905= pGoods;
            else if (gcGoodsID.m_dwGoodsID == 1399001)
                m_pSZ = pGoods;
            else{}
            if (gcGoodsID.m_dwGoodsID/1000==2002)
                AddHY(gcGoodsID.m_dwGoodsID);
        }
    }

    return pGoods;
}


void CMDSApp::SaveGoodsTxt()
{
    LogTrace("%s", __FUNCTION__);

    m_wrGoods.AcquireReadLock();

    std::string strFile(m_strSharePath);
    strFile.append("Data/L2DS_Goods.txt");
    FILE* pFile = fopen(strFile.c_str(), "w");
    if (pFile)
    {
        try
        {
            CString str;
            
            str = "代码    名称      昨收盘  开盘价  最新价  最高价  最低价    成交量    成交额\n";
            fwrite(str.GetData(), 1, str.GetLength(), pFile);

            int nGoods = m_aGoods.GetSize();
            for (int i=0; i<nGoods; i++)
            {
                CGoods* p = (CGoods*)m_aGoods[i];
                str.Format("%06d  %s  %d  %d  %d  %d  %d  %d  %d\n", 
                    p->m_dwNameCode, (LPCTSTR)CGoods::Name2String(p->m_pcName), p->m_dwClose, p->m_dwOpen, 
                    p->m_dwPrice, p->m_dwHigh, p->m_dwLow,  INT64(p->m_xVolume)/10000, INT64(p->m_xAmount)/10000);
                fwrite(str.GetData(), 1, str.GetLength(), pFile);
            }

        }
        catch(...)
        {
        }

        fclose(pFile);
    }
    else
    {
        LOG_ERR("open %s fail! ", strFile.c_str());
    }

    m_wrGoods.ReleaseReadLock();
}


/**
 * 开盘
 */
bool CMDSApp::InitDate(DWORD dwDate, char cType)
{
    LogTrace("%s", __FUNCTION__);
    if (cType<1 || cType>3)
        return false;

    if (m_bInitingDate)
        return false;

    if (g_dwSysWeek==0 || g_dwSysWeek==6)
        return false;

    m_bInitingDate = true;

    m_dwTime = 0;

    if (cType&0x01)
    {
        m_dwDateValue = dwDate;
        m_dwTimeValue = 0;

        m_dwInitDate = dwDate;

        // 开盘时清空计数器 [4/22/2013 frantqian]
        m_nDMCheckCount = 0;
        m_nSaveDayCount = 0;
        m_nNoSaveDayCount = 0;
        m_dwMinKErrCount = 0;
        m_dwMinKErrValue = 0;
        m_dwErrThreadCount = 0;
        m_mapNotSaveGoodsID.clear();
        m_mapDelDateCount.clear();
        m_mapSaveGoodsID.clear();
    }

    if (cType&0x02)
    {
        m_dwDateValueHK = dwDate;
        m_dwTimeValueHK = 0;

        m_dwInitDateHK = dwDate;
    }

    m_thInitDay = new CMyInitDayThread(false);
    if (m_thInitDay == NULL)
    {
        m_bInitingDate = false;
        LOG_ERR("alloc init day thread fail!!!!");
        return m_bInitingDate;
    }

    m_thInitDay->m_cType = cType;
    if (!m_thInitDay->Start())
    {
        m_bInitingDate = false;
        SAFE_DELETE(m_thInitDay);
        LOG_ERR("start init day thread fail!!!!");
    }

    return m_bInitingDate;
}

/**
 * 收盘
 */
bool CMDSApp::SaveDay(BOOL bHK, BOOL bAuto)
{
    LogTrace("%s", __FUNCTION__);
    if (bAuto && (g_dwSysWeek==0 || g_dwSysWeek==6))
        return false;

    if (bHK)
    {
        if (m_bSaveingDateHK)//正在收盘
            return false;

        if (bAuto && m_dwSaveDateHK==g_dwSysYMD)
            return false;

        m_bSaveingDateHK = true;

        m_thSaveDayHK = new CMySaveDayThread(false);
        if (m_thSaveDayHK == NULL)
        {
            m_bSaveingDateHK = false;
            LOG_ERR("alloc save HK day thread fail!!!!");
            return m_bSaveingDateHK;
        }

        m_thSaveDayHK->m_bHK = bHK;
        if (!m_thSaveDayHK->Start())
        {
            m_bSaveingDateHK = false;
            SAFE_DELETE(m_thSaveDayHK);
            LOG_ERR("start save HK day thread fail!!!!");
        }

        return m_bSaveingDateHK;
    }
    else
    {
        if (m_bSaveingDate)//正在收盘
            return false;

        if (bAuto && m_dwSaveDate==g_dwSysYMD)
            return false;

        m_bSaveingDate = true;

        m_thSaveDay = new CMySaveDayThread(false);
        if (m_thSaveDay == NULL)
        {
            m_bSaveingDate = false;
            LOG_ERR("alloc save day thread fail!!!!");
            return m_bSaveingDate;
        }

        m_thSaveDay->m_bHK = bHK;
        if (!m_thSaveDay->Start())
        {
            m_bSaveingDate = false;
            SAFE_DELETE(m_thSaveDay);
            LOG_ERR("start save day thread fail!!!!");
        }

        return m_bSaveingDate;
    }
}

//拼音
void CMDSApp::LoadHZ()
{
    LogTrace("%s", __FUNCTION__);

    m_aHZ.RemoveAll();

    std::string strFile(m_strSharePath);
    strFile.append("HZNew.txt");
    FILE* pFile = fopen(strFile.c_str(), "r");
    if (pFile)
    {
        try
        {
            char pcLine[1024];
            while (fgets(pcLine, 1024, pFile)!= NULL) 
            {
                CString str = pcLine;
                m_aHZ.Add(str);
            }
        }
        catch(...)
        {
        }

        fclose(pFile);
    }
    else
    {
        LOG_ERR("open %s fail! ", strFile.c_str());
    }
}

void CMDSApp::LoadDYZ()
{
    LogTrace("%s", __FUNCTION__);

    m_aDYZ.RemoveAll();

    std::string strFile(m_strSharePath);
    strFile.append("DYZ.txt");
    FILE* pFile = fopen(m_strSharePath+"DYZ.txt", "r");
    if (pFile)
    {
        try
        {
            char pcLine[1024];
            while (fgets(pcLine, 1024, pFile)!= NULL) 
            {
                CString str = pcLine;
                m_aDYZ.Add(str);
            }
        }
        catch(...)
        {
        }

        fclose(pFile);
    }
    else
    {
        LOG_ERR("open %s fail! ", strFile.c_str());
    }
}
//拼音

//乾坤图
void CMDSApp::LoadQKT()
{
    //1绿,2红
    LogTrace("%s", __FUNCTION__);

    m_vQKT.clear();

    std::string strFile(m_strSharePath);
    strFile.append("QKT.txt");
    FILE* pFile = fopen(m_strSharePath+"QKT.txt", "r");
    if (pFile)
    {
        try
        {
            char pcLine[1024];
            CQktCwb fQkt;
            fQkt.m_cStatus = 1;
            while (fgets(pcLine, 1024, pFile)!= NULL) 
            {
                fQkt.m_dwTime = (DWORD)atoi(pcLine);
                m_vQKT.push_back(fQkt);

                if(fQkt.m_cStatus == 1)
                    fQkt.m_cStatus = 2;
                else if(fQkt.m_cStatus == 2)
                    fQkt.m_cStatus = 1;
            }
        }
        catch(...)
        {
        }

        fclose(pFile);
    }
    else
    {
        LOG_ERR("open %s fail! ", strFile.c_str());
    }
}

void CMDSApp::ClearDataFile(char cType)
{
    if (cType==3)
    {
        m_dfBargainSPQH.Clear();
        m_dfMinuteSPQH.Clear();
    }
    else if (cType==2)
    {
        m_dfBargainHK.Clear();
        m_dfMinuteHK.Clear();
    }
    else
    {
        for (int i=0; i<10; i++)
        {
            m_pdfTrade[i].Clear();
            m_pdfBargain[i].Clear();
            m_pdfPartOrder[i].Clear();
            m_pdfMinute[i].Clear();
        }
    }
}


CDataFile_Bargain* CMDSApp::GetBargainFile(char cStation, DWORD dwNameCode)
{
    CDataFile_Bargain* pDF = NULL;

    switch (cStation)
    {
    case 0 : 
    case 1 : 
    case 2 : 
    case 3 : 
    case 4 : 
    case L1SH_STATION : 
        pDF = m_pdfBargain+dwNameCode%10;
        break;
    case 5 : 
        pDF = &m_dfBargainHK;
        break;
    case 7 :
        pDF = &m_dfBargainSPQH;
        break;
    default : 
        pDF = &m_dfBargainWI;
        break;
    }

    return pDF;
}

CDataFile_Minute* CMDSApp::GetMinuteFile(char cStation, DWORD dwNameCode)
{
    CDataFile_Minute* pDF = NULL;

    switch (cStation)
    {
    case 0 : 
    case 1 : 
    case 2 : 
    case 3 : 
    case 4 : 
    case L1SH_STATION : 
        pDF = m_pdfMinute+dwNameCode%10;
        break;
    case 5 : 
        pDF = &m_dfMinuteHK;
        break;
    case 7 :
        pDF = &m_dfMinuteSPQH;
        break;
    default : 
        pDF = &m_dfMinuteWI;
        break;
    }

    return pDF;
}


CDataFile_HisMin* CMDSApp::GetHisMinFile(char cStation, DWORD dwNameCode)
{
    CDataFile_HisMin* pDF = NULL;

    pDF = m_pdfHisMin+dwNameCode%10;
    return pDF;
}


//add by liuchengzhu 20091124
int CMDSApp::GetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault)
{
    return m_conf.GetProfileInt(lpszSection,lpszEntry,nDefault);
}

void CMDSApp::SetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nVal)
{
    m_conf.SetProfileInt(lpszSection, lpszEntry, nVal);
}


CString CMDSApp::GetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry,LPCTSTR lpszDefault)
{
    return m_conf.GetProfileString(lpszSection,lpszEntry,lpszDefault);
}

void CMDSApp::SetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszVal)
{
    m_conf.SetProfileString(lpszSection, lpszEntry, lpszVal);
}

//加载配置
bool CMDSApp::Load_Config(bool bReload)
{
    LogTrace("%s", __FUNCTION__);
    if (!bReload)
    {
        std::string IniFile(m_ModulePath.GetData());
        IniFile.append("/");
        IniFile.append(INI_FILE_NAME);

        if (m_conf.Load_Config(IniFile) != 0)
        {
            LOG_ERR("Load config file %s fail!", IniFile.c_str());
            return false;
        }
    }

    LogInfo("Load config file successfully!");

    return true;
}

//输出所有配置
void CMDSApp::ListAll(void)
{
#ifdef _DEBUG
    m_conf.ListAll();
#endif
}
//end add by liuchengzhu 20091124


//{ memo
#define MAX_LINE_LEN 1024
CString CMDSApp::LoadMemo(const CString& strFile)
{
    CString strMemo("");

    try
    {
        CString strFilePath(m_strSharePath+"Memo/"+strFile);
        FILE* fp = fopen(strFilePath.GetData(), "r");
        if (fp)
        {
            char* buf;
            int length;

            fseek(fp, 0, SEEK_END);
            length = ftell(fp);
            buf = new char[length + 1];
            memset(buf, 0, length+1);
            fseek(fp, 0, SEEK_SET);

            fread(buf, length, 1, fp);
            buf[length] = '\0';
            std::string buffer = std::string(buf);
            delete [] buf;

            fclose(fp);

            strMemo = buffer.c_str();
        }
        else
        {
            LOG_ERR("open %s fail! ", strFilePath.GetData());
        }
    }
    catch(...)
    {
    }

    return strMemo;
}

void CMDSApp::LoadMemo()
{
    LogTrace("%s", __FUNCTION__);

    m_strUpdate = LoadMemo("Update.txt");

    m_strFeeSMSL1 = LoadMemo("FeeSMSL1.txt");//短信缴费
    m_strFeeSMSL2 = LoadMemo("FeeSMSL2.txt");//短信缴费
    m_strFeeCarL1 = LoadMemo("FeeCarL1.txt");//年卡缴费
    m_strFeeCarL2 = LoadMemo("FeeCarL2.txt");//年卡缴费
    m_strFeePostL1 = LoadMemo("FeePostL1.txt");//邮局汇款
    m_strFeePostL2 = LoadMemo("FeePostL2.txt");//邮局汇款
    m_strFeeBankL1 = LoadMemo("FeeBankL1.txt");//银行汇款
    m_strFeeBankL2 = LoadMemo("FeeBankL2.txt");//银行汇款
    m_strFeeNet = LoadMemo("FeeNet.txt");//网上支付
    m_strMobileLoyaltyRuleL1 = LoadMemo("LoyaltyRuleL1.txt");//推荐好友积分规则
    m_strMobileLoyaltyRuleL2 = LoadMemo("LoyaltyRuleL2.txt");//推荐好友积分规则
    m_strFeeMS = LoadMemo("FeeMS.txt");//充值点卡门店

    m_strYHXY = LoadMemo("YHXY.txt");//用户协议
    m_strRJSM = LoadMemo("RJSM.txt");//软件说明
    LoadRJSM_Ex("RJSM_EX.txt");//帮助
}

void CMDSApp::LoadRJSM_Ex(const CString& strFile)
{
    CString strFilePath(m_strSharePath+"Memo/"+strFile);

    try
    {
        FILE* fp = fopen(strFilePath.GetData(), "r");
        if (fp)
        {
            char * buf;
            int length;

            fseek(fp, 0, SEEK_END);
            length = ftell(fp);
            buf = new char[length + 1];
            memset(buf, 0, length+1);
            fseek(fp, 0, SEEK_SET);

            fread(buf, length, 1, fp);
            buf[length] = '\0';
            std::string buffer = std::string(buf);
            delete [] buf;

            fclose(fp);

            std::string strMemo;
            int count = 0;

            while (true)
            {
                std::string::size_type end = buffer.find("\n");
                if (end == std::string::npos) 
                    break;

                std::string line = buffer.substr(0, end);
                buffer.erase(0, end + 1);

                std::string::size_type cr = line.find("\r");
                if (cr != std::string::npos)
                {
                    line.erase(cr, 1);
                }

                //去掉行首空格
                while(line.size() && (*line.begin() == ' ' || *line.begin() == '\t'))
                    line.erase(line.begin());

                //跳过注释
                if (line[0] == '/' || line[0] == '#')
                    continue;

                if (line.empty() && !strMemo.empty())
                {
                    std::string::size_type indexLeft = strMemo.find_first_of("<");
                    std::string::size_type indexRight = strMemo.find_first_of(">");
                    if (indexLeft >= 0 && indexRight >= 1)
                    {
                        std::string strNum = strMemo.substr(indexLeft + 1, indexRight - 1);
                        count = atoi(strNum.c_str());
                        strMemo.erase(indexLeft, indexRight + 2);
                        while(line.size() && (*line.begin() == ' ' || *line.begin() == '\t' || *line.begin() == '\n'))
                            line.erase(line.begin());

                        m_mapRJSM[count] += strMemo.c_str();
                    }

                    strMemo = "";
                    continue;
                }
                else if (line.empty())
                {
                    continue;
                }

                strMemo += line;
                strMemo += "\n";
            }

            if (!strMemo.empty())
            {
                std::string::size_type indexLeft = strMemo.find_first_of("<");
                std::string::size_type indexRight = strMemo.find_first_of(">");
                if (indexLeft >= 0 && indexRight >= 1)
                {
                    std::string strNum = strMemo.substr(indexLeft + 1, indexRight - 1);
                    count = atoi(strNum.c_str());
                    strMemo.erase(indexLeft, indexRight + 1);
                    while(strMemo.size() && (*strMemo.begin() == ' ' || *strMemo.begin() == '\t' || *strMemo.begin() == '\n'))
                        strMemo.erase(strMemo.begin());

                    m_mapRJSM[count] += strMemo.c_str();
                }
            }
        }
        else
        {
            LOG_ERR("open %s fail! ", strFilePath.GetData());
        }
    }
    catch(...)
    {
        LOG_ERR("open %s exception! ", strFilePath.GetData());
    }
}

const CString& CMDSApp::GetRJSM(int index) const
{
    CIterMapRJSM CIter = m_mapRJSM.find (index);
    if (CIter != m_mapRJSM.end())
    {
        return CIter->second;
    }
    else
    {
        LOG_ERR("can't find index = %d of info in RJSM map!", index);
    }

    static CString tmp("帮助");

    return tmp;
}


// 保存配置
int CMDSApp::Save_Config(void)
{
    return m_conf.Save_Config();
}

//load 热点板块  [11/6/2012 frantqian]
void CMDSApp::LoadHotGroup()
{
    LogTrace("%s", __FUNCTION__);
    CString strPath = theApp.m_strSharePath+"Data/Group/HotPk.txt";
    int  ID = 0;
    char pcName[32];

    CString strClass;//类型名字

    std::string strLine;
    std::ifstream infile(strPath.GetData());
    if ( !infile )
        return ;

    m_wrHotGroup.AcquireWriteLock();
    m_mapHotGroup.clear();
    m_wrHotGroup.ReleaseWriteLock();
    while ( getline(infile, strLine, '\n') )
    {
        ZeroMemory(pcName, 32);
        sscanf(strLine.c_str(), "%d %s", &ID, pcName);

        m_wrHotGroup.AcquireWriteLock();
        m_mapHotGroup.insert(std::make_pair(ID,pcName));
        m_wrHotGroup.ReleaseWriteLock();
        
        if (ID < 2000)
        {
            strClass = "概念板块";
        }
        else if (ID < 3000)
        {
            strClass = "行业板块2";
        }
        else if (ID < 4000)
        {
            strClass = "地区板块";
        }
        else
        {
            strClass = "三级概念";
        }
        
        CGroup* pGroup = GetGroup(strClass, pcName);
        if (pGroup != NULL)
        {
            CGroup* pNewGroup = new CGroup;
            if (pNewGroup)
            {
                pNewGroup->m_nGroup = 26;
                pNewGroup->m_strClass = "热点板块";
                pNewGroup->m_strGroup = pGroup->m_strGroup;
                pNewGroup->m_strGroupFile = pGroup->m_strGroupFile;
                pNewGroup->SetGroup(-1);

                AddGroup(pNewGroup);
            }
        }
    }


    LOG_ERR("LOAD热点板块数量：%d!\n", m_mapHotGroup.size());
    infile.close();
}



void CMDSApp::ClearHotGroup()
{
    LogTrace("%s", __FUNCTION__);
    m_wrGroup.AcquireWriteLock();
    std::vector<CGroup*>::iterator groupItor = m_vecGroup.begin();
    while(groupItor != m_vecGroup.end())
    {
        CGroup* p = (CGroup*)(*groupItor);
        if (p->m_strClass == "热点板块")
        {
            p->ClearGoodsBK();
            delete p;
            groupItor = m_vecGroup.erase(groupItor);
        }
        else
        {
            groupItor++;
        }
    }
    m_wrGroup.ReleaseWriteLock();
}

//add by liuchengzhu 20100112
//获取板块明细
CGroup* CMDSApp::GetGroup(const CString& strClass, const CString& strGroup)
{
    LogTrace("%s", __FUNCTION__);
    CGroup* pGroup = NULL;

    m_wrGroup.AcquireReadLock();

    int nGroup = m_vecGroup.size();
    for(int i=0; i<nGroup; i++)
    {
        CGroup* p = (CGroup*)m_vecGroup[i];
        if ((strClass.IsEmpty() || strClass==p->m_strClass) && p->m_strGroup.Find(strGroup)>=0)
        {
            pGroup = p;
            break;
        }
    }

    m_wrGroup.ReleaseReadLock();

    return pGroup;
}

void CMDSApp::LoadGroup(int BKType, const CString& strClass)
{
    LogTrace("%s", __FUNCTION__);
    CString strPath = theApp.m_strSharePath+"Data/Group/"+strClass+"/";

    DIR *dp = 0;
    struct dirent *entry = 0;

    dp = opendir(strPath.GetData());
    if(dp)
    {
        struct stat st;
        std::string currfile;
        while((entry = readdir(dp))!=NULL)
        {
            if (strcmp(entry->d_name, ".")==0 || strcmp(entry->d_name, "..")==0)
            {
                continue;
            }

            currfile = strPath;
            currfile.append(entry->d_name);
            stat(currfile.c_str(), &st); 
            if (S_ISDIR(st.st_mode))
            {
                continue;
            }

            //判断后缀是否为.BLK blk
            char *pFind=NULL;
            pFind=strstr(entry->d_name,".BLK");
            if(pFind==NULL)
            {
                pFind=strstr(entry->d_name,".blk");
            }
            
            //后缀名不符合规范退出
            if(pFind==NULL)
            {   
                continue;
            }

            char name_buf[64]={0};
            memcpy(name_buf,entry->d_name,strlen(entry->d_name)-4);
            CString strGroup = name_buf;

            LogDbg("Load Group =>%s", strGroup.GetData());
            CGroup* pGroup = GetGroup(strClass, strGroup);
            if (pGroup==NULL)
            {
                pGroup = new CGroup;
                if (pGroup)
                {
                    pGroup->m_nGroup = BKType;
                    pGroup->m_strClass = strClass;
                    pGroup->m_strGroup = strGroup;
                    pGroup->m_strGroupFile = currfile.c_str();
                    pGroup->SetGroup(-1);

                    AddGroup(pGroup);
                }
            }
        }

        closedir(dp);

        LogInfo("Load %s successfully!", strPath.GetData());
    }
    else
        LOG_ERR("Load %s fail!", strPath.GetData());
}

void CMDSApp::AddGroup(CGroup* pGroup)
{
    LogTrace("%s", __FUNCTION__);
    m_wrGroup.AcquireWriteLock();

    m_vecGroup.push_back(pGroup);

    m_wrGroup.ReleaseWriteLock();
}

//end add by liuchengzhu 20100112

void CMDSApp::DeleteGroup()
{
    LogTrace("%s", __FUNCTION__);
    m_wrGroup.AcquireWriteLock();

    int nGroup = m_vecGroup.size();
    for(int i=0; i<nGroup; i++)
    {
        CGroup* p = (CGroup*)m_vecGroup[i];
        p->ClearGoodsBK();
        delete p;
    }
    m_vecGroup.clear();

    m_wrGroup.ReleaseWriteLock();
}

void CMDSApp::DeleteCalcGroup()
{
    m_wrCalcGroup.AcquireWriteLock();

    int nCalcGroup = (int)m_aCalcGroup.GetSize();
    for(int i=0; i<nCalcGroup; i++)
    {
        CCalcGroup* p = (CCalcGroup*)m_aCalcGroup[0];
        m_aCalcGroup.RemoveAt(0);
        delete p;
    }

    m_wrCalcGroup.ReleaseWriteLock();
}

void CMDSApp::AddCalcGroup(CCalcGroup* pCalcGroup)
{
    m_wrCalcGroup.AcquireWriteLock();

    m_aCalcGroup.Add(pCalcGroup);

    m_wrCalcGroup.ReleaseWriteLock();
}

CCalcGroup* CMDSApp::GetCalcGroup(DWORD dwGroupID)
{
    CCalcGroup* pCalcGroup = NULL;

    m_wrCalcGroup.AcquireReadLock();

    int nCalcGroup = (int)m_aCalcGroup.GetSize();
    for(int i=0; i<nCalcGroup; i++)
    {
        CCalcGroup* p = (CCalcGroup*)m_aCalcGroup[i];
        if (p->m_dwGroupID==dwGroupID)
        {
            pCalcGroup = p;
            break;
        }
    }

    m_wrCalcGroup.ReleaseReadLock();

    return pCalcGroup;
}

CCalcGroup* CMDSApp::CreateCalcGroup(DWORD dwGroupID)
{
    CCalcGroup* pCalcGroup = GetCalcGroup(dwGroupID);
    if (pCalcGroup==NULL)
    {
        pCalcGroup = new CCalcGroup;

        pCalcGroup->m_dwGroupID = dwGroupID;

        AddCalcGroup(pCalcGroup);
    }

    return pCalcGroup;
}


//////////////////////////////////////////////////////////////////////////
void CMDSApp::CalcCurBK()
{
    CSortArray<DWORD> aHY;
    m_wrCurBK.AcquireReadLock();
    aHY.Copy(m_aHY);
    m_wrCurBK.ReleaseReadLock();

    CHisBK_Calc hbc;

    if (m_pSH)
    {
        RWLock_scope_rdlock  rdlock(m_pSH->m_rwGood);   
        m_pSH->m_rwGoodcount[5] += 1;
        hbc.m_xZJJL = m_pSH->GetValue(BIGAMT);
        m_pSH->m_rwGoodcount[5] -= 1;
    }

    int nSize = aHY.GetSize();
    for (int i=0; i<nSize; i++)
    {
        CZDF_ZJJL zz;
        zz.m_dwID = aHY[i];

        CGoods* pGoods = GetGoods(zz.m_dwID);
        if (pGoods )
        {
            RWLock_scope_rdlock  rdlock(pGoods->m_rwGood);  
            pGoods->m_rwGoodcount[6] += 1;
            try
            {
                zz.m_nZDF = int(pGoods->GetValue(ZDF));
                zz.m_xZJJL = pGoods->GetValue(BIGAMT);
            }
            catch (...)
            {
            }

            pGoods->m_rwGoodcount[6] -= 1;
        
            hbc.m_aZZ.Add(zz);
        }
    }

    int nNum = hbc.m_aZZ.GetSize();
    if (nNum>=6)
    {
        m_wrCurBK.AcquireWriteLock();

        m_CurBK.m_dwDate = m_dwDateValue;
        m_CurBK.m_xZJJL = hbc.m_xZJJL;

        for (int j=0; j<3; j++)
        {
            CZDF_ZJJL& zzTop = hbc.m_aZZ[nNum-1-j];
            CZDF_ZJJL& zzBottom = hbc.m_aZZ[j];

            m_CurBK.m_Top[j] = zzTop;
            m_CurBK.m_Bottom[j] = zzBottom;
        }

        m_wrCurBK.ReleaseWriteLock();
    }
}

//////////////////////////////////////////////////////////////////////////
void CMDSApp::AddHY(DWORD dwID)
{
    m_wrCurBK.AcquireWriteLock();

    m_aHY.Add(dwID);

    m_wrCurBK.ReleaseWriteLock();
}

//////////////////////////////////////////////////////////////////////////
CHisBK CMDSApp::FindHisBK(DWORD dwDate)
{
    CHisBK hb;

    m_wrCurBK.AcquireReadLock();
    DWORD dwCurBKDate = m_CurBK.m_dwDate;
    m_wrCurBK.ReleaseReadLock();

    if (dwDate==0 || dwDate == dwCurBKDate)
    {
        m_wrCurBK.AcquireReadLock();

        hb = m_CurBK;
        
        m_wrCurBK.ReleaseReadLock();
    }
    else
    {
        m_wrHisBK.AcquireReadLock();

        hb.m_dwDate = dwDate;
        int nPos = m_aHisBK.Find(hb);
        if (nPos>=0)
            hb = m_aHisBK[nPos];
        else
            hb.m_dwDate = 0;
        
        m_wrHisBK.ReleaseReadLock();
    }

    return hb;
}

//////////////////////////////////////////////////////////////////////////
CTradeSession* CMDSApp::GetTradeSession(char cStation, BOOL bIncludeVirtual)
{
    if (cStation==4)
        return m_pTradeSession+2;
    else if (cStation==3 || cStation==5)
        return m_pTradeSession+3;
    else if (bIncludeVirtual)
        return m_pTradeSession+1;
    else
        return m_pTradeSession;
}
//////////////////////////////////////////////////////////////////////////
// 字符串解析 [9/3/2012 frantqian]
static int StringSplit(const std::string& strRes, std::vector<std::string>& vecDest, const std::string opSplit)
{
    int _offset = 0;
    int _npos = 0;
    while (_npos != -1)
    {
        _npos = strRes.find(opSplit, _offset);
        std::string _strSub = "";
        _strSub = strRes.substr(_offset, _npos-_offset);
        vecDest.push_back(_strSub);
        _offset = _npos +1;
    }
    return vecDest.size();
}


int CMDSApp::LoadDynamicGroupTxt()
{
    LogTrace("%s", __FUNCTION__);
    CString strPath = theApp.m_strSharePath+"Data/Group/DatUpdate.txt";
    int  DatTime = 0;
    char pcDatName[32];

    CString strClass;//类型名字

    std::string strLine;
    std::ifstream infile(strPath.GetData());
    if ( !infile )
    {
        LOG_ERR("test strPath:%s load faile!",strPath.GetData());
        return -1;
    }

    m_DynamicGroupUpdateTimeMap.clear();

    while ( getline(infile, strLine, '\n') )
    {
        memset(pcDatName,0, 32);
        sscanf(strLine.c_str(), "%s %d", pcDatName, &DatTime);
        m_DynamicGroupUpdateTimeMap.insert(std::make_pair(pcDatName, DatTime));
    }

    infile.close();
}

int CMDSApp::UpdateDynamicGroupTxt()
{
    LogTrace("%s", __FUNCTION__);
    CString strPath = theApp.m_strSharePath+"Data/Group/DatUpdate.txt";

    ofstream SaveFile(strPath.GetData(), ios::ate);
    if ( !SaveFile )
    {
        LOG_ERR("test save strPath:%s failed!",strPath.GetData());
        return -1;
    }
    
    std::map<string, int>::iterator itor ;
    char temp[256];
    itor = m_DynamicGroupUpdateTimeMap.begin();

    for (; itor != m_DynamicGroupUpdateTimeMap.end(); itor++)
    {
        memset(temp,0,256);
        sprintf(temp, "%s %d\n", itor->first.c_str(), itor->second);
        SaveFile << temp;
    }

    SaveFile.close();   
}

int CMDSApp::UpdateGroupData()
{
    LoadDynamicGroupTxt();
    CString dyGroupDayDFpath = theApp.m_strSharePath+"Data/Group/Data/";

    DIR *dp = 0;
    struct dirent *entry = 0;

    dp = opendir(dyGroupDayDFpath.GetData());
    if(dp)
    {
        std::map<std::string, std::string> mapDyGrroup;

        struct stat st;
        std::string currfile;
        while((entry = readdir(dp))!=NULL)
        {
            if (strcmp(entry->d_name, ".")==0 || strcmp(entry->d_name, "..")==0)
            {
                continue;
            }

            currfile = dyGroupDayDFpath;
            currfile.append(entry->d_name);
            stat(currfile.c_str(), &st); 
            if (S_ISDIR(st.st_mode))
            {
                continue;
            }
            //判断后缀是否为.BLK blk
            char *pFind=NULL;
            pFind=strstr(entry->d_name,".DAT");
            if(pFind==NULL)
            {
                pFind=strstr(entry->d_name,".dat");
            }
            //后缀名不符合规范退出
            if(pFind==NULL)
            {   
                continue;
            }
            //对文件名字排序，默认顺序是按key升序。
            mapDyGrroup.insert(make_pair((entry->d_name),(LPCTSTR)(dyGroupDayDFpath+entry->d_name)));
        }

        time_t now;
        struct  tm  tm_s, *timenow;
        time(&now);
        timenow = localtime_r(&now, &tm_s);

        INT64 nNowTime = timenow->tm_mon*100000000 +timenow->tm_mday*1000000+ timenow->tm_hour*10000 +timenow->tm_min*100 +timenow->tm_sec;

        for (map<string,string>::iterator it = mapDyGrroup.begin(); it != mapDyGrroup.end(); ++it)
        {   
            CDataFile_Day dfDynamicBKDayTemp;
            std::map<string, int>::iterator timeItor = m_DynamicGroupUpdateTimeMap.begin();
            
            timeItor = m_DynamicGroupUpdateTimeMap.find(it->first);

            //没读过的才读取
            if (timeItor == m_DynamicGroupUpdateTimeMap.end())
            {
                dfDynamicBKDayTemp.Initial(it->second.c_str());
                WriteNewDat2DayFile((void*)&dfDynamicBKDayTemp); //把新的DAT文件内容写入DAY.DAT
                m_DynamicGroupUpdateTimeMap.insert(make_pair(it->first, nNowTime));//刷新txt对应的MAP
            }
        } 

        closedir(dp);
        LogInfo("Load %s successfully!", dyGroupDayDFpath.GetData());
    }
    UpdateDynamicGroupTxt();
    return 0;
}

int CMDSApp::WriteNewDat2DayFile( void* pVoidDayDF )
{
    CDataFile_Day*  pDayDF = (CDataFile_Day*)pVoidDayDF;
    int nGoodsNum = pDayDF->m_dfh.m_dfi.m_dwNumOfGoods;
    DWORD m_dwGoodsID = 0;
    //std::map<DWORD, CDataFile_Day*>::iterator dyDFItor = NULL;

    for (int i = 0; i < nGoodsNum; i++)
    {   
        //m_dwGoodsID = pDayDF->m_dfh.m_pDFG[i].m_dwGoodsID;

        CGoods* pGoods =  GetGoods(pDayDF->m_dfh.m_pDFG[i].m_dwGoodsID);
        if (NULL == pGoods)
        {
            pGoods = CreateGoods(pDayDF->m_dfh.m_pDFG[i].m_dwGoodsID);
            if (NULL == pGoods)
            {
                break;
            }
        }
        pGoods->LoadDayFromSpecialFile(pDayDF);
    }


}
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
char CMDSApp::GetQKTValue(DWORD dwTime,WORD dataType)
{
    //0无效状态,1绿,2红
    char retValue = 0;

    if(dataType == MIN_DATA)
        dwTime = 20000000 + dwTime / 10000;

    if(m_dwStartYMD == dwTime)
        if(m_cHorsePower >= 4)
            retValue = 2;
        else
            retValue = 1;
    else
        for(int i = m_vQKT.size()-1;i >= 0;i--)
        {
            if(dwTime >= m_vQKT[i].m_dwTime)
            {
                retValue = m_vQKT[i].m_cStatus;
                break;
            }
        }

    return retValue;
}

#define TIMETHREAD_TIMEOUT  10000  //ontime线程超时时间
#define CREATE_THREAD_ERROR_NUM 100
// 线程检查函数，检查一些关键线程是否还在正常运行，如果不能正常运行，则再合适的时间重启MDS [4/26/2013 frantqian]
void CMDSApp::CheckThread()
{
    time_t timeDiff = time(NULL) - CMDSApp::g_tsL2DMTime;
    if (g_dwSysHMS > 90000 && g_dwSysHMS < 160000 && timeDiff> 90 && g_tL2DMTime != 0 || theApp.m_thDM.m_bWantRestart )
    {
        LogApp("L2DM CheckThread:%d,%d",CMDSApp::g_tL2DMTime,CMDSApp::g_dwSysHMS);
        CMDSApp::g_tL2DMTime = CMDSApp::g_dwSysHMS;
        CMDSApp::g_tsL2DMTime = time(NULL);
        LogErr("L2DM线程超过1分钟,重启L2DM线程");
        //theApp.m_thDM.End();
        if (true == theApp.m_thDM.ReStart())
        {
            LogErr("L2DM线程超过1分钟,重启L2DM线程,完成");
            theApp.m_thDM.m_bWantRestart = FALSE;
        }else
        {
            LogErr("L2DM线程超过1分钟,重启L2DM线程,失败,重启");
            m_brunning = false;
            m_thTimer.SetJoinable(false);
            theApp.m_thDM.SetJoinable(false);
        }
    }
    

    // 只在下午4点以后发现time线程出问题才重启MDS
    if (g_dwSysHMS > 160000 && (g_dwSysHMS >dwUpdateTime_TimeThread + TIMETHREAD_TIMEOUT))
    {
        LOG_ERR("time thread err:%d, %d, %s, restart",g_dwSysHMS,dwUpdateTime_TimeThread, sTimeThreadInfo.c_str());
        m_brunning = false;
        m_thTimer.SetJoinable(false);
    }

    // 当线程创建失败超过次数后直接重启 [4/27/2013 frantqian]
    if (theApp.m_dwErrThreadCount > CREATE_THREAD_ERROR_NUM )
    {
        LOG_ERR("create thread err, restart");
        m_brunning = false;
    }

    //m_mds.PrintListenPortInfo();
}

//根据上面2个map生成告警信息
void CMDSApp::CreateCheckAlertInfo( std::string& strAlertInfo )
{
    std::map<DWORD,DWORD>::iterator itor;
    int nWriteCount = 0;

    char strGoodsID[12] = "";
    char strCount[12] = "";
    strAlertInfo +="Not Saved Goods:";
    if (m_mapNotSaveGoodsID.size() > 0)
    {
        for (itor = m_mapNotSaveGoodsID.begin(); itor != m_mapNotSaveGoodsID.end(); itor++)
        {
            memset(strGoodsID, 0, 12);
            //_itoa(itor->second,strGoodsID,10);
            sprintf(strGoodsID, "%d",itor->second);
            strAlertInfo += strGoodsID;
            strAlertInfo += ",";
            nWriteCount++;
            if (nWriteCount > 10)
            {
                break;
            }
        }
    }

    strAlertInfo +="| Del Date Info:";
    if (m_mapDelDateCount.size() > 0)
    {
        for (itor = m_mapDelDateCount.begin(); itor != m_mapDelDateCount.end(); itor++)
        {
            if (itor->second > 10)
            {
                memset(strGoodsID, 0, 12);
                //_itoa(itor->first,strGoodsID,10);
                sprintf(strGoodsID, "%d",itor->first);
                strAlertInfo += strGoodsID;
                strAlertInfo += ":";
                memset(strCount, 0, 12);
                //_itoa(itor->second,strCount,10);
                sprintf(strCount, "%d",itor->second);
                strAlertInfo += strCount;
                strAlertInfo += ";";
            }
        }
    }

    strAlertInfo +="| MINK ERR:";
    memset(strGoodsID, 0, 12);
    //_itoa(itor->first,strGoodsID,10);
    sprintf(strGoodsID, "%d",m_dwMinKErrCount);
    strAlertInfo += strGoodsID;

    memset(strGoodsID, 0, 12);
    //_itoa(itor->first,strGoodsID,10);
    sprintf(strGoodsID, "%d",m_dwMinKErrCount);
    strAlertInfo += strGoodsID;

}

CDataFile_Day* CMDSApp::GetDataFile( char cStation, short sDataType, short sPeriod )
{
    CDataFile_Day* pDF = NULL;

    switch (cStation)
    {
    case 0 : 
    case 1 : 
    case 2 : 
    case 4 : 
        if (sDataType==DAY_DATA)
            pDF = &m_dfDay;
        else if (sDataType==MIN_DATA)
        {
            if (sPeriod && sPeriod%30 == 0)
                pDF = &m_dfMin30;
            else if (sPeriod && sPeriod%5 == 0)
                pDF = &m_dfMin5;
            else
                pDF = &m_dfMin1;
        }
        break;
    case 3 : 
    case 5 : 
        if (sDataType==DAY_DATA)
            pDF = &m_dfDay_HK;
        else if (sDataType==MIN_DATA)
        {
            if (sPeriod && sPeriod%30 == 0)
                pDF = &m_dfMin30_HK;
            else if (sPeriod && sPeriod%5 == 0)
                pDF = &m_dfMin5_HK;
            else
                pDF = &m_dfMin1_HK;
        }
        break;
    
    case 7 :
        if (sDataType==DAY_DATA)
            pDF = &m_dfDay_SPQH;
        else if (sDataType==MIN_DATA)
        {
            if (sPeriod%30==0)
                pDF = &m_dfMin30_SPQH;
            else if (sPeriod%5==0)
                pDF = &m_dfMin5_SPQH;
            else
                pDF = &m_dfMin1_SPQH;
        }
        break;

    default : 
        if (sDataType==DAY_DATA)
            pDF = &m_dfDay_WI;
        else if (sDataType==MIN_DATA)
        {
            if (sPeriod && sPeriod%30 == 0)
                pDF = &m_dfMin30_WI;
            else if (sPeriod && sPeriod%5 == 0)
                pDF = &m_dfMin5_WI;
            else
                pDF = &m_dfMin1_WI;
        }
        break;
    }

    return pDF;
}


bool CMDSApp::IsNeedEncodeGoodsIDVec( BYTE marketgroup )
{
    return true;//先全开压缩
    if (0 == marketgroup || 1 == marketgroup || 3 == marketgroup || 21 == marketgroup)
        return true;
    
    return false;
}


bool CMDSApp::IsNeedRefreshCodeMap( BYTE marketgroup, std::vector<DWORD>& vecGoods )
{
    //检查是否需要更新码表内容
    m_wrCode.AcquireReadLock();
    bool returnvalue = true;
    std::map<BYTE, int>::iterator marketitor;
    marketitor = m_marketinfo.find(marketgroup);
    if (marketitor == m_marketinfo.end())
    {
        returnvalue = true;
    }
    else
    {
        if(marketitor->second != vecGoods.size())
            returnvalue =  true;
        else
            returnvalue =  false;
    }
    m_wrCode.ReleaseReadLock();
    return returnvalue;
}


bool CMDSApp::RefreshCodeMap( BYTE marketgroup, std::vector<DWORD>& vecGoods )
{
    if (IsNeedRefreshCodeMap(marketgroup, vecGoods) != true )
        return true;
    
    m_wrCode.AcquireWriteLock();
    std::map<BYTE, std::map<WORD, BYTE> >::iterator allcodeitor;
    allcodeitor = m_AllCodemap.find(marketgroup);
    if (allcodeitor != m_AllCodemap.end())
    {
        m_AllCodemap.erase(allcodeitor);
    }

    std::map<BYTE, int>::iterator highestitor;
    highestitor = m_HighestLowmap.find(marketgroup);
    if (highestitor != m_HighestLowmap.end())
    {
        m_HighestLowmap.erase(highestitor);
    }

    std::map<WORD, BYTE> codemap;
    //更新码表内容
    WORD codehigh = 0;
    int  codelow = 0;
    std::map<WORD, BYTE>::iterator codeitor;
    int highest = 0;
    for (int i = 0; i < vecGoods.size(); i++)
    {
        codehigh = vecGoods[i]/10000;
        codelow = vecGoods[i]%10000;
        codeitor = codemap.find(codehigh);
        if (codeitor == codemap.end())
        {
            codemap.insert(std::make_pair(codehigh, codemap.size()));
            //取最大的那个codelow值
            highest = highest > codelow ? highest : codelow;
        }
        else
        {   //取最大的那个codelow值
            highest = highest > codelow ? highest : codelow;
        }
    }
    m_AllCodemap.insert(std::make_pair(marketgroup, codemap));
    m_HighestLowmap.insert(std::make_pair(marketgroup, highest));

    //更新市场信息
    std::map<BYTE, int>::iterator marketitor;
    marketitor = m_marketinfo.find(marketgroup);
    if (marketitor == m_marketinfo.end())
    {
        m_marketinfo.insert(std::make_pair(marketgroup,vecGoods.size()));
    }
    else
    {
        marketitor->second = vecGoods.size();
    }

    m_wrCode.ReleaseWriteLock();
}

std::map<WORD, BYTE> CMDSApp::GetCodeMap( BYTE marketgroup, std::vector<DWORD>& vecGoods, int& highestLow)
{
    RefreshCodeMap( marketgroup, vecGoods);

    std::map<WORD, BYTE> codemap;
    m_wrCode.AcquireReadLock();
    std::map<BYTE, std::map<WORD, BYTE> >::iterator itor;
    itor = m_AllCodemap.find(marketgroup);
    if (itor != m_AllCodemap.end())
        codemap = itor->second;


    std::map<BYTE, int>::iterator highestitor;
    highestitor = m_HighestLowmap.find(marketgroup);
    if (highestitor != m_HighestLowmap.end())
        highestLow = highestitor->second;

    m_wrCode.ReleaseReadLock();

    return codemap;
}

void CMDSApp::CheckAH2HK()
{
    int size = 0;
    m_wrGoodsAH2HK.AcquireReadLock();
    size = m_aGoodsAH.GetSize();
    m_wrGoodsAH2HK.ReleaseReadLock();

    if( size ){
        m_wrGoodsAH2HK.AcquireWriteLock();
        size = m_aGoodsAH.GetSize();
        for( int i=size; i>0; i--){
            int index = i - 1;
            CGoods* pGoodsAH = (CGoods *)m_aGoodsAH[index];
            CGoods* pGoodsHK = GetGoods(g_GetGoodsID(5,pGoodsAH->m_dwNameCode));
            if( pGoodsHK ){
                pGoodsHK->SetGroupMask(GROUP_HK);
                m_aGoodsAH.RemoveAt(index);
            }
        }
        m_wrGoodsAH2HK.ReleaseWriteLock();
    }
}

void CMDSApp::AddGoodsAH(CGoods* pGoods)
{
    m_wrGoodsAH2HK.AcquireWriteLock();
    m_aGoodsAH.Add(pGoods);
    m_wrGoodsAH2HK.ReleaseWriteLock();
}



CHisMinDF* CMDSApp::GetHisMinDF(DWORD dwDate)
{
    if (dwDate<20090901 || dwDate>g_dwSysYMD)
        return NULL;

    dwDate /= 100;

    CHisMinDF* pRet = NULL;

    m_wrHisMinDF.AcquireWriteLock();

    int nSize = (int)m_aHisMinDF.GetSize();
    for(int i=0; i<nSize; i++)
    {
        CHisMinDF* p = (CHisMinDF*)m_aHisMinDF[i];
        if (p->m_dwDate==dwDate)
        {
            pRet = p;
            break;
        }
    }

    if (pRet==NULL)
    {
        pRet = new CHisMinDF;
        if (pRet)
        {
            pRet->m_dwDate = dwDate;

            CString strFile;
            strFile.Format("Data/HM%d.dat", dwDate);
            pRet->m_dfHisMin.Initial(m_strSharePath+strFile);

            m_aHisMinDF.Add(pRet);
        }
    }

    m_wrHisMinDF.ReleaseWriteLock();

    return pRet;
}

void CMDSApp::DeleteHisMinDF()
{
    m_wrHisMinDF.AcquireWriteLock();

    int nSize = (int)m_aHisMinDF.GetSize();
    for(int i=0; i<nSize; i++)
    {
        CHisMinDF* p = (CHisMinDF*)m_aHisMinDF[0];
        m_aHisMinDF.RemoveAt(0);
        delete p;
    }

    m_wrHisMinDF.ReleaseWriteLock();
}

//////////////////////////////////////////////////////////////////////////
void CMDSApp::SyncRedis(SyncType type)
{
	DWORD dwGoods = m_aGoods.GetSize();
	for (DWORD d=0; d<dwGoods; d++)
	{
		CGoods* pGoods = (CGoods*)m_aGoods[d];
		if (pGoods)
		{
			if (!pGoods->IsBad())
			{
				RWLock_scope_rdlock  rwlock(pGoods->m_rwGood);
				if (type == SYNC_CQ)
				{
					m_pRedisImporterCQ->WriteNameCode(pGoods);
					m_pRedisImporterCQ->WriteQuote(pGoods);
				}
				// min1
				else if (type == SYNC_KM)
				{
					Min1ToRedis(pGoods, m_pRedisImporterKM);
				}
				else if (type == SYNC_KL)
				{
					KLine2Redis(pGoods, m_pRedisImporterKL, MIN_DATA, MIN_30);
					KLine2Redis(pGoods, m_pRedisImporterKL, MIN_DATA, MIN_60);
					KLine2Redis(pGoods, m_pRedisImporterKL, DAY_DATA, DAY_PERIOD);
					KLine2Redis(pGoods, m_pRedisImporterKL, DAY_DATA, WEEK_PERIOD);
					KLine2Redis(pGoods, m_pRedisImporterKL, DAY_DATA, MONTH_PERIOD);
				}
			}
		}
	}
	if (type == SYNC_CQ)
	{
		m_pRedisImporterCQ->WriteSpec();
		m_pRedisImporterCQ->WriteCodeInfo();
	}
}
