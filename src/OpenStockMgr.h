#ifndef OPENSTOCKMGR_H
#define OPENSTOCKMGR_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)
#include <set>

#include "MyThread.h"
#include "RWLock.h"
#include "SysGlobal.h"


class OpenStockMgr : public CMyThread
{
public:
	OpenStockMgr();
	virtual ~OpenStockMgr();

	bool IsOpen(DWORD dwGoodID);

	virtual const char* Name() {return "OpenStockMgr";}

protected:
	virtual void Run();

private:
	RWLock m_rwGoods;
	std::set<DWORD> m_setGoods;
	DWORD m_dwTime;
};


#endif

