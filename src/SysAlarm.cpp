#include "SysAlarm.h"

#include <fstream>

#include "MDS.h"

const int SYSALARM_BUFFER_SIZE = 1024;

void CSysAlarmAll::Init(CString strPath)
{
	m_dfAlarm.Initial(strPath+"Data/SysAlarm.dat");
	m_dfAlarm.LoadData(0, m_alarm);

	CArray<CString, CString&> aName;
	std::string strName;

	m_strNameFile = strPath+"Data/SysAlarm.txt";

	std::ifstream infile;
	infile.open(m_strNameFile.GetData());
	if (infile.is_open())
	{
		try
		{
			CString tmp;
			while(infile >> strName)
			{
				tmp = strName.c_str();
				aName.Add(tmp);
			}
		}
		catch(...)
		{
		}

		infile.close();
	}

	m_MutexName.Lock();
	m_aName.Copy(aName);
	m_MutexName.UnLock();
}

void CSysAlarmAll::Exit()
{
	m_alarmTemp.m_wr.AcquireWriteLock();

	if (m_alarmTemp.m_dwSize>0)
	{
		CSysAlarm* pBuffer = m_alarmTemp.GetBuffer();
		if (pBuffer)
		{
			m_dfAlarm.SaveData(0, pBuffer, m_alarmTemp.m_dwSize, 0, TRUE);
		}
	}

	m_alarmTemp.m_wr.ReleaseWriteLock();

	CArray<CString, CString&> aName;

	m_MutexName.Lock();
	aName.Copy(m_aName);
	m_MutexName.UnLock();

	std::ofstream outfile;
	outfile.open(m_strNameFile.GetData());
	if (outfile.is_open())
	{
		try
		{
			int nSize = (int)aName.GetSize();
			for (int i=0; i<nSize; i++)
			{
				outfile << aName[i].GetData() << "\r\n";
			}
		}
		catch(...)
		{
		}

		outfile.close();
	}
}

void CSysAlarmAll::AddAlarm(CSysAlarmArray& aAlarm)
{
	int nSize = (int)aAlarm.GetSize();
	CSysAlarm* p = aAlarm.GetData();
	if (nSize==0 || p==NULL)
		return;

	m_alarmTemp.m_wr.AcquireWriteLock();

	m_alarmTemp.Alloc(m_alarmTemp.m_dwSize+nSize);
	CSysAlarm* pBuffer = m_alarmTemp.GetBuffer();
	if (pBuffer)
	{
		CopyMemory(pBuffer+m_alarmTemp.m_dwSize, p, nSize*sizeof(CSysAlarm));
		m_alarmTemp.m_dwSize += nSize;

		if (m_alarmTemp.m_dwSize>SYSALARM_BUFFER_SIZE)
		{
			m_dfAlarm.SaveData(0, pBuffer, m_alarmTemp.m_dwSize, 0, TRUE);
			m_alarmTemp.m_dwSize = 0;
		}
	}

	m_alarmTemp.m_wr.ReleaseWriteLock();

	m_alarm.m_wr.AcquireWriteLock();

	m_alarm.Alloc(m_alarm.m_dwSize+nSize);
	pBuffer = m_alarm.GetBuffer();
	if (pBuffer)
	{
		CopyMemory(pBuffer+m_alarm.m_dwSize, p, nSize*sizeof(CSysAlarm));
		m_alarm.m_dwSize += nSize;
	}

	m_alarm.m_wr.ReleaseWriteLock();
}

void CSysAlarmAll::ClearAlarm()
{
	m_alarmTemp.Init();
	m_alarm.Init();

	m_dfAlarm.Clear();

	m_dwClearCount++;	//Mutex?
}

void CSysAlarmAll::GetName(CArray<CString, CString&>& aName)
{
	m_MutexName.Lock();
	aName.Copy(m_aName);
	m_MutexName.UnLock();
}


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
void CIndSelGoodsAll::Init(CString strPath)
{
	m_dfIndGoods.Initial(strPath+"Data/IndSelGoods.dat");
	m_dfIndGoods.LoadData(0, m_indSelGoods);

	CArray<CString, CString&> aName;
	std::string strName;

	m_strNameFile = strPath+"Data/IndSelGoods.txt";

	std::ifstream infile;
	infile.open(m_strNameFile.GetData());
	if (infile.is_open())
	{
		try
		{
			CString tmp;
			while(infile >> strName)
			{
				tmp = strName.c_str();
				aName.Add(tmp);
			}
		}
		catch(...)
		{
		}

		infile.close();
	}

	m_MutexName.Lock();
	m_aName.Copy(aName);
	m_MutexName.UnLock();
}

void CIndSelGoodsAll::Exit()
{
	m_dfIndGoods.SaveData(0, m_indSelGoods.GetBuffer(), m_indSelGoods.m_dwSize, 0, FALSE);

	CArray<CString, CString&> aName;

	m_MutexName.Lock();
	aName.Copy(m_aName);
	m_MutexName.UnLock();

	std::ofstream outfile;
	outfile.open(m_strNameFile.GetData());
	if (outfile.is_open())
	{
		try
		{
			int nSize = (int)aName.GetSize();
			for (int i=0; i<nSize; i++)
				outfile << aName[i].GetData() << "\r\n";
		}
		catch(...)
		{
		}

		outfile.close();
	}
}

void CIndSelGoodsAll::AddSelGoods(CIndSelGoodsArray& aIndGoods)
{
	int nSize = (int)aIndGoods.GetSize();
	CIndSelGoods* p = aIndGoods.GetData();
	if (nSize==0 || p==NULL)
		return;

	m_indSelGoods.m_wr.AcquireWriteLock();

	m_indSelGoods.Alloc(m_indSelGoods.m_dwSize+nSize);
	CIndSelGoods* pBuffer = m_indSelGoods.GetBuffer();
	if (pBuffer)
	{
		CopyMemory(pBuffer+m_indSelGoods.m_dwSize, p, nSize*sizeof(CIndSelGoods));
		m_indSelGoods.m_dwSize += nSize;
	}

	if (aIndGoods.GetSize() > 0)
		AddHitchGoods(aIndGoods.GetSize(),aIndGoods.GetData());

	m_indSelGoods.m_wr.ReleaseWriteLock();
}

void CIndSelGoodsAll::ClearAll()
{
	m_indSelGoods.Init();
	m_dfIndGoods.Clear();

	m_aHitchGoods.SetSize(0,500);
	m_aHitchGroup.SetSize(0,500);

	m_aOverSellGoods.SetSize(0,500);
	m_dwClearCount++;	//Mutex?
}


void CIndSelGoodsAll::SeparatePool()
{
	int nSize = m_indSelGoods.m_dwSize;
	CIndSelGoods *pIndGoods = m_indSelGoods.GetBuffer();

	AddHitchGoods(nSize,pIndGoods);
	AddOverSellGoods(nSize,pIndGoods);
}

void CIndSelGoodsAll::AddHitchGoods(int nSize, CIndSelGoods *pIndGoods)
{
	for (int i = 0; i < nSize;i++)
	{
		if (pIndGoods[i].m_wType == XGTYPE_ZLBTB)
		{
			m_aHitchGoods.Add(*(CHitchGoods*)(&pIndGoods[i]));
			CGoods *pGoods = theApp.GetGoods(pIndGoods[i].m_dwGoodsID);
			if (pGoods)
			{
				WORD wID = 2000 + pGoods->m_wGroupHY;
				m_aHitchGroup.Change(wID);
				for (int i = 0;i < pGoods->m_aGroupGN.GetSize();i++)
				{
					wID = 1000 + pGoods->m_aGroupGN[i];
					m_aHitchGroup.Change(wID);
				}
			}
		}
	}
}

int CIndSelGoodsAll::FindHitchGoods( DWORD dwGoodsID )
{
	CHitchGoods hitGoods;
	hitGoods.m_dwGoodsID = dwGoodsID;
	int nFind = m_aHitchGoods.Find(hitGoods);
	return nFind;
}

void CIndSelGoodsAll::AddOverSellGoods( int nSize, CIndSelGoods *pIndGoods )
{
	for (int i = 0; i < nSize;i++)
	{
		if (pIndGoods[i].m_wType >= XGTYPE_OVERSELL && pIndGoods[i].m_wType <= (XGTYPE_OVERSELL+2))
		{
			COverSellGoods osGoods = *(COverSellGoods*)(&pIndGoods[i]);

			//为了简化处理,在ds端重新修改类型,将状态修改为类型,避免重复(入池和入榜的个股可以重复,但入选时间不同)
			if(osGoods.m_wType == XGTYPE_OVERSELL)
			{
				if (osGoods.m_dwStatus == 1)
					osGoods.m_wType = XGTYPE_OVERSELL + 4;
				else if (osGoods.m_dwStatus == 2)
					osGoods.m_wType = XGTYPE_OVERSELL + 5;
				else //status = 0
					osGoods.m_wType = XGTYPE_OVERSELL + 3;
			}

			m_aOverSellGoods.Add(osGoods);
		}
	}
}

//查超跌池
int CIndSelGoodsAll::FindOverSellGoods( DWORD dwGoodsID)
{
	COverSellGoods osGoods;
	osGoods.m_dwGoodsID = dwGoodsID;
	osGoods.m_wType = XGTYPE_OVERSELL + 3;//超跌池
	int nFind = m_aOverSellGoods.Find(osGoods);
	return nFind;
}

//查英雄榜或者观察池
int CIndSelGoodsAll::FindOverSellGoods2( DWORD dwGoodsID)
{
	COverSellGoods osGoods;
	osGoods.m_dwGoodsID = dwGoodsID;
	osGoods.m_wType = XGTYPE_OVERSELL + 4;
	int nFind = m_aOverSellGoods.Find(osGoods);
	if (nFind < 0)
	{
		osGoods.m_wType = XGTYPE_OVERSELL + 5;
		nFind = m_aOverSellGoods.Find(osGoods);
	}
	return nFind;
}

void CIndSelGoodsAll::GetOverSellGoodsId( CString &strGroup, CDWordArray &aGoodsId )
{
	DWORD dwSize = m_aOverSellGoods.GetSize();
	if (strGroup[0] < '1' || strGroup[0] > '5')
		return;

	int nType = atoi(strGroup);
	if (nType <= 5)
	{
		nType += XGTYPE_OVERSELL;
		for(int i = 0;i < dwSize;i++)
		{
			if (m_aOverSellGoods[i].m_wType == nType)
				aGoodsId.Add(m_aOverSellGoods[i].m_dwGoodsID);
		}
	}	
}