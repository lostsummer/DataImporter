#ifndef DATAHEAD_H
#define DATAHEAD_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "SysGlobal.h"

const WORD DATAHEAD_ID = 0x9988;

#pragma pack(push, 1)

//////////////////////////////////////////////////////////////////////

class CDataHead  
{
public:
	CDataHead();

	void Initial();
	DWORD Recv(char* pcData, bool bNetworkOrder);
	DWORD Send(char* pcData, bool bNetworkOrder);

	static volatile int s_DataID;

public:
	WORD m_wHeadID;
	WORD m_wDataType;
	DWORD m_dwDataID;
	DWORD m_dwDataLength;
	WORD m_wChecksum;
	WORD m_wReserved;
};

#define _DataHeadLength_	16

//////////////////////////////////////////////////////////////////////

const char DATAHEAD_MOBILE_CHAR_EMCRYPT = 0x31;
const char DATAHEAD_MOBILE_CHAR1 = 0x11;
const char DATAHEAD_MOBILE_CHAR2 = 0x22;

class CDataHeadMobile
{
public:
	CDataHeadMobile();

	void Initial();
	DWORD Recv(char* pcData, bool bNetworkOrder);
	DWORD Send(char* pcData, bool bNetworkOrder);

	static volatile int s_DataID;

public:
	WORD m_wHeadID;
	WORD m_wDataType;
	DWORD dwSessionID;
	DWORD m_dwDataLength;
};

#define _DataHeadMobileLength_	12

#pragma pack(pop)

#endif


