#ifndef APPGLOBAL_H
#define APPGLOBAL_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "SysGlobal.h"
#include "String.h"
#include "Buffer.h"

// application global functions
extern DWORD MulDiv(DWORD dwMulti1, DWORD dwMulti2, DWORD dwDivisor);
extern char *_strupr(char* pcSrc);
extern char *_strlwr(char* pcSrc);

extern DWORD GetSecondsDiff(DWORD dwTime1, DWORD dwTime2);		// hhmmss
extern int GetSecondsDiff_S(DWORD dwTime1, DWORD dwTime2);		// hhmmss

extern DWORD AddSeconds(DWORD dwTime1, DWORD dwAddSeconds);
extern DWORD MinusSeconds(DWORD dwTime1, DWORD dwMinusSeconds);

extern WORD GetMinutesDiff(WORD wTime1, WORD wTime2);			// hhmm
extern WORD AddMinutes(WORD wTime1, WORD wAddMinutes);

extern BYTE GetBytesCheckSum(PBYTE pcData, int nNumBytes);

extern BOOL g_IsGoodsID(DWORD dwGoodsID);

extern int g_SetNonBlocking(int nSocket);

extern CString g_Value2String(INT64 hValue);

extern BOOL g_IsDateGood(DWORD);

extern int g_CodeConvert(const char* pcCharsetFrom, const char* pcCharsetTo, char* pcInBuf, int nInBufLen, char* pcOutBuf, int nOutBufLen);
extern WORD g_GetUnicodeString(const CString& str, WORD*& pwUnicode);

WORD g_GetCRC(BYTE*, DWORD);
BYTE g_GetCRC8(BYTE * ucPtr, BYTE ucLen);

// 转码函数 [2/22/2013 frantqian]
void UTF8toANSI(const std::string &from ,char* outbuf);

#define CATCH_BEGIN try{
#define CATCH_END } catch(...){}
// application global data

// 替换MKTIME函数，该函数无法计算tm_wday和 tm_yday[11/6/2013 frantqian]
long kernel_mktime(struct tm * tm);

// 计算tm_wday [11/6/2013 frantqian]
int CaculateWeekDay(int year,int month, int day);

#endif

