#include <stdio.h>
#include <unistd.h> 
#include <sys/resource.h>


#include "MDS.h"

int main(int argc, char **argv)
{
	bool bDaemon = false;
	int result;
	while((result = getopt(argc, argv, "dD")) != -1)
	{
		switch (result)
		{
		case 'D':
		case 'd': bDaemon=true;	break;
		default:break;
		}
	}

	if (bDaemon)
		daemon(1, 1);

	rlimit rl;
	if (getrlimit(RLIMIT_CORE, &rl) == -1)
		printf("getrlimit of core failed. This could be problem.\n");
	else
	{
		rl.rlim_cur = rl.rlim_max;
		if (setrlimit(RLIMIT_CORE, &rl) == -1)
			printf("setrlimit of core failed. Server may not save core.dump files.\n");
	}

	if (getrlimit(RLIMIT_NOFILE, &rl) == -1)
		printf("getrlimit of file num failed. This could be problem.\n");
	else
	{
		rl.rlim_cur = rl.rlim_max = 60000;
		if (setrlimit(RLIMIT_NOFILE, &rl) == -1)
			printf("setrlimit of NOFILE failed. Server may not create many files' description.\n");
	}
	
	if (theApp.InitInstance())
	{
		theApp.Run();
	}

	theApp.ExitInstance();

	return 0;
}


