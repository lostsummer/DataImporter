#include "Alarm.h"
#include "MDS.h"

const int ALARM_BUFFER_SIZE = 1024;

void CAlarmAll::Init(CString strPath)
{
	m_dfAlarm.Initial(strPath+"Data/Alarm.dat");
	m_dfAlarm.LoadData(0, m_alarm);
}

void CAlarmAll::Exit()
{
	m_alarmTemp.m_wr.AcquireWriteLock();

	if (m_alarmTemp.m_dwSize>0)
	{
		CAlarm* pBuffer = m_alarmTemp.GetBuffer();
		if (pBuffer)
		{
			m_dfAlarm.SaveData(0, pBuffer, m_alarmTemp.m_dwSize, 0, TRUE);
		}
	}

	m_alarmTemp.m_wr.ReleaseWriteLock();
}

void CAlarmAll::AddAlarm(CAlarmArray& aAlarm)
{
	int nSize = (int)aAlarm.GetSize();
	CAlarm* p = aAlarm.GetData();
	if (nSize==0 || p==NULL)
		return;

	m_alarmTemp.m_wr.AcquireWriteLock();

	m_alarmTemp.Alloc(m_alarmTemp.m_dwSize+nSize);
	CAlarm* pBuffer = m_alarmTemp.GetBuffer();
	if (pBuffer)
	{
		CopyMemory(pBuffer+m_alarmTemp.m_dwSize, p, nSize*sizeof(CAlarm));
		m_alarmTemp.m_dwSize += nSize;

		if (m_alarmTemp.m_dwSize>ALARM_BUFFER_SIZE)
		{
			m_dfAlarm.SaveData(0, pBuffer, m_alarmTemp.m_dwSize, 0, TRUE);
			m_alarmTemp.m_dwSize = 0;
		}
	}

	m_alarmTemp.m_wr.ReleaseWriteLock();

	m_alarm.m_wr.AcquireWriteLock();

	m_alarm.Alloc(m_alarm.m_dwSize+nSize);
	pBuffer = m_alarm.GetBuffer();
	if (pBuffer)
	{
		CopyMemory(pBuffer+m_alarm.m_dwSize, p, nSize*sizeof(CAlarm));
		m_alarm.m_dwSize += nSize;
	}

	m_alarm.m_wr.ReleaseWriteLock();
}

void CAlarmAll::ClearAlarm()
{
	m_alarmTemp.Init();
	m_alarm.Init();

	m_dfAlarm.Clear();

	m_dwClearCount++;	//Mutex?
}

//////////////////////////////////////////////////////////////////////////

BOOL operator > (CYJGoods &first, CYJGoods &second)
{
	return first.m_dwGoodsID > second.m_dwGoodsID;
}

BOOL operator == (CYJGoods &first, CYJGoods &second)
{
	return first.m_dwGoodsID == second.m_dwGoodsID;
}

BOOL operator < (CYJGoods &first, CYJGoods &second)
{
	return first.m_dwGoodsID < second.m_dwGoodsID;
}

const CYJGoods& CYJGoods::operator=(const CYJGoods& src)
{
	m_dwGoodsID = src.m_dwGoodsID;
	m_cPeriod = src.m_cPeriod;
	m_cBS = src.m_cBS;

	return *this;
}
//////////////////////////////////////////////////////////////////////////
BOOL operator > (CCPXStat &first, CCPXStat &second)
{
	if (first.m_dwGoodsID!=second.m_dwGoodsID)
		return (first.m_dwGoodsID > second.m_dwGoodsID);
	return first.m_dwDate > second.m_dwDate;
}

BOOL operator == (CCPXStat &first, CCPXStat &second)
{
	return (first.m_dwGoodsID==second.m_dwGoodsID && first.m_dwDate == second.m_dwDate);
}

BOOL operator < (CCPXStat &first, CCPXStat &second)
{
	if (first.m_dwGoodsID!=second.m_dwGoodsID)
		return (first.m_dwGoodsID < second.m_dwGoodsID);
	return first.m_dwDate < second.m_dwDate;
}

//////////////////////////////////////////////////////////////////////////
const DWORD g_dwCpxIdx[3] = {1, 1399001, 2002001};

CGoodsCPXStat::CGoodsCPXStat()
{
	for (int i = 0; i < 3;i++)
		m_aCPXStat[i].m_dwGoodsID = g_dwCpxIdx[i];
}

CGoodsCPXStat::~CGoodsCPXStat()
{

}

void CGoodsCPXStat::AddToday()
{
	m_wrCpxStat.AcquireWriteLock();
	for (int i = 0; i < 3;i++)
	{
		m_aCPXStat[i].m_dwDate = theApp.m_dwDateValue;
		m_aCPXStat[i].m_dwGoodsID = g_dwCpxIdx[i];
		m_aGoodsCpxStat[i].Change(m_aCPXStat[i]);
	}
	m_wrCpxStat.ReleaseWriteLock();
}

bool CGoodsCPXStat::Read()
{
	CString strFile = theApp.m_strSharePath+"Data/CpxStat.dat";

	m_wrCpxStat.AcquireWriteLock();
	short sVer;
	CString str;
	try
	{
		CBuffer buffer;
		buffer.m_bSingleRead = true;
		buffer.Initialize(4096, true);

		buffer.FileRead(strFile);
		buffer.Read(&sVer, 2);
		buffer.ReadString(str);

		if (sVer==1 && str == "CPXSTAT")
		{
			buffer.Read(&m_dwUpdateDate, 4);
			buffer.Read(&m_dwUpdateTime, 4);
			for (int i = 0; i < 3;i++)
			{
				int idx = buffer.ReadInt();				
				int nSize = buffer.ReadInt();

				m_aGoodsCpxStat[idx].SetSize(nSize);
				buffer.Read(m_aGoodsCpxStat[idx].GetData(),nSize * sizeof(CCPXStat));
				// BYTE * p = new BYTE[nSize * sizeof(CCPXStat)];
				// buffer.Read(p,nSize * sizeof(CCPXStat));
				//	delete p;
			}
		}
	}
	catch(int)
	{
		LOG_ERR("exception %s: ver=>%d date=>%d time=>%d", strFile.GetData(), sVer, m_dwUpdateDate, m_dwUpdateTime);
		int nDataFileSwitch = theApp.GetProfileInt("System", "DataFileSwitch", 0);
		if (0==nDataFileSwitch)
		{
			m_wrCpxStat.ReleaseWriteLock();
			return false;
		}
	}

	m_wrCpxStat.ReleaseWriteLock();
	return true;
}

void CGoodsCPXStat::Write(BOOL bFree)
{
	AddToday();
	CString strFile = theApp.m_strSharePath+"Data/CpxStat.dat";

	m_wrCpxStat.AcquireReadLock();

	try
	{
		CBuffer buffer;
		buffer.Initialize(4096, true);
		short sVer = 1;
		buffer.WriteShort(1);

		CString str = "CPXSTAT";
		buffer.WriteString(str);

		buffer.WriteInt(CMDSApp::g_dwSysYMD);
		buffer.WriteInt(CMDSApp::g_dwSysHMS);

		for (int i = 0; i < 3;i++)
		{
			buffer.WriteInt(i);
			int nSize = m_aGoodsCpxStat[i].GetSize();
			buffer.WriteInt(nSize);
			buffer.Write(m_aGoodsCpxStat[i].GetData(),nSize * sizeof(CCPXStat));
		}

		buffer.FileWrite(strFile);
	}
	catch(int)
	{
	}
	m_wrCpxStat.ReleaseReadLock();

	if (bFree)
	{
		for (int i = 0; i < 3;i++)
			m_aGoodsCpxStat[i].RemoveAll();
	}
}

void CGoodsCPXStat::Get( DWORD dwDate,CCPXStatArray &aCpxStat )
{
	m_wrCpxStat.AcquireReadLock();

	for (int i = 0; i < 3;i++)
	{
		int nSize = m_aGoodsCpxStat[i].GetSize();
		for (int j = 0;j < nSize;j++)
		{
			CCPXStat &cpxstat = m_aGoodsCpxStat[i][j];
			if (cpxstat.m_dwDate > dwDate)
				aCpxStat.Add(cpxstat);
		}
	}

	m_wrCpxStat.ReleaseReadLock();
}

CASAlerts g_Alerts;
CGoodsCPXStat g_CpxStat;
