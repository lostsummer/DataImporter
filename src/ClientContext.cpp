#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>

#include "ClientContext.h"
#include "AppLogger.h"
#include "MDS.h"

CClientContext::CClientContext()
{
	m_nConnectID = -1;

	memset(&m_epollEvent, 0, sizeof(m_epollEvent));
	m_epollEvent.data.ptr = this;

	m_bWriteReady = FALSE;
	memset(m_pcAddress,0,16);
}

CClientContext::~CClientContext()
{
	Close();
}

void CClientContext::Close()
{
	m_nConnectID = -1;
	m_epollEvent.events = 0;

	m_bWriteReady = FALSE;

	m_bLogined = FALSE;

	m_dwRecvPack = 0;
	m_dwRecvByte = 0;
	m_dwSendPack = 0;
	m_dwSendByte = 0;
	m_dwRecvRecordtime_5m     = 0;
	m_dwSendRecordtime_5m     = 0;
	m_dwRecordtime_10s    =0;
	m_dwRecordtime_5s     =0;

	m_dwActiveTime = 0;
	m_dwFirstTime = 0;

	if (m_bConnected)
	{
		m_bConnected = FALSE;
		struct linger lngr = {1, 0}; 
		if (setsockopt(m_socket, SOL_SOCKET, SO_LINGER,
			(const void *) &lngr, sizeof(struct linger)) == -1)
		{
			LOG_ERR("CClientContext::Close setsockopt SO_LINGER error");
		}
		::shutdown(m_socket,SHUT_RDWR);
		::close(m_socket);
	}
}

void CClientContext::AfterConnect()
{
	CMySocket::AfterConnect();

	m_nConnectID = -1;
	m_epollEvent.events = 0;

	m_bWriteReady = TRUE;
}

int CClientContext::DoSend(CSockThreadData*)
{
	int nRet = 0;

	if (m_wStatus != WantSend && m_wStatus != WantAll)
		return nRet;

	int nSndDataLen = m_bufWrite.GetBufferLen();
	char* pcSndBuf = (char*)m_bufWrite.GetBuffer();
	if (m_bWriteReady && nSndDataLen>0)
	{
		int nSndBytes = ::send(m_socket, pcSndBuf, nSndDataLen, MSG_NOSIGNAL);
		if (nSndBytes == -1)
		{
			if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
			{
				m_wStatus = CMySocket::WantDelete;
				LOG_ERR("send error from %d, error msg:%s !", m_socket, strerror(errno));

				m_bWriteReady = FALSE;
				nRet = -1;
			}
		}
		else
		{
			m_bufWrite.Delete(nSndBytes);
			if (nSndBytes < nSndDataLen)
				m_bWriteReady = FALSE;

			SetTime();
			CMDSApp::g_llSysSendByte += nSndBytes;
			m_dwSendByte += nSndBytes;
		}
	}

	return nRet;
}

int CClientContext::HandleClientEvent(unsigned int unEvents, CSockThreadData* pThreadData)
{
	int nRet = 0;
	
	//错误 | 挂断
	if(unEvents & (EPOLLERR | EPOLLHUP))
	{
		//错误
		if(unEvents & EPOLLERR)
		{
			LogDbg("EPOLLERR from %d", m_socket);
		}

		//挂断
		if(unEvents & EPOLLHUP)
		{
			LogDbg("EPOLLHUP from %d", m_socket);
			int nRcv = RCV_BUFF_LEN;
			while (nRcv>=RCV_BUFF_LEN)
				nRcv = ::recv(m_socket, pThreadData->m_pcRcvBuf, RCV_BUFF_LEN, MSG_NOSIGNAL);

			if(nRcv == -1)
				LOG_DBG("recv after EPOLLHUP error! error msg:%s! ", strerror(errno));
		}

		nRet = -1;
	}
	else
	{
		//可读
		if(unEvents & EPOLLIN)
		{
			nRet = DoRecv(pThreadData);
		}
		
		//可写
		if(unEvents & EPOLLOUT)
		{
			m_bWriteReady = TRUE;

			nRet = DoSend(pThreadData);
		}

		if (nRet == 0)
		{
			//发送完毕
			if(m_bWriteReady)
				m_epollEvent.events &= (~EPOLLOUT);		//清除可写事件
			else
				m_epollEvent.events |= EPOLLOUT;		//增加可写事件
		}
	}

	return nRet;
}


