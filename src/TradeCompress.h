#ifndef __TRADECOMPRESS_H__
#define __TRADECOMPRESS_H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "Goods.h"
#include "BitStream.h"

class CDataBargain;
class CTradeCompress  
{
public:
	static int CompressBargainOld(CGoods* pGoods, CBargain* pBargain, int nBargainNum, CBitStream&);
	static int CompressTradeOld(CGoods* pGoods, CTrade* pTrade, int nTradeNum, CBitStream&);
	static int CompressDayOld(CGoods* pGoods, CDay* pDay, int nDayNum, CBitStream&);

	static int ExpandOrder(CBitStream& stream, CGoods* pGoods, DWORD dwNum);
	static int ExpandBargain(CBitStream& stream, CGoods* pGoods, DWORD dwNum);
	static int ExpandTrade(CBitStream& stream, CGoods* pGoods, DWORD dwNum);
	static int ExpandPartOrder(CBitStream& stream, CGoods* pGoods, DWORD dwNum, BOOL bCurrent);

	//Steven New Begin
	static int ExpandBargain6(CBitStream& stream, CGoods* pGoods, DWORD dwNum);
	//Steven New End

	static int CompressBargain2(CBitStream&, CGoods* pGoods, CBargain* pBargain, DWORD dwNum);
	static int CompressTrade2(CBitStream&, CGoods* pGoods, CTrade* pTrade, DWORD dwNum);

	static int CompressOrder(CBitStream&, CGoods* pGoods, COrder* pOrder, DWORD dwNum);
	static int CompressBargain(CBitStream& stream, CDataBargain* pBargain, int nRecvNum, short sWant, bool bRolling = false);
	static int CompressTrade(CBitStream&, CGoods* pGoods, CTrade* pTrade, DWORD dwNum);
	static int CompressPartOrder(CBitStream& stream, CPartOrder* pPartOrder, DWORD dwNum);

	static void CompressNum(CBitStream&, DWORD dwNum);
	static DWORD ExpandNum(CBitStream& stream);

	static int CompressTradePV(CBitStream&, CGoods* pGoods, CTradePV* pTradePV, DWORD dwNum);
};


#endif


