#ifndef MYSOCKET_H
#define MYSOCKET_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netinet/in.h>

#include "DataHead.h"
#include "PtrArray.h"
#include "Buffer.h"
#include "String.h"
#include "Noncopyable.h"
#include "SockData.h"

const int RCV_BUFF_LEN = 8192;
#define _SendBuffer_Size_	8192

const int SIZEOF_SOCKADDR = sizeof(sockaddr_in);

class CSockData;

struct CSockThreadData
{
	char m_pcRcvBuf[RCV_BUFF_LEN];
	CBuffer	m_bufTemp;

	CSockThreadData()
	{
		m_bufTemp.m_bSingleRead = true;
		m_bufTemp.Initialize(PAGE_SIZE, true);
	}
};

//////////////////////////////////////////////////////////////////////

class CMySocket : private Noncopyable
{
public:
	CMySocket();
	virtual ~CMySocket();

	virtual void AfterConnect();
	virtual void Close();

	virtual int DoRecv(CSockThreadData* pThreadData);
	virtual int DoSend(CSockThreadData* pThreadData);
	virtual void MakeSend(CBuffer& bufTemp);
	virtual void ReadPacket(CDataHead&, CBuffer& bufTemp);
	virtual void ReadPacket(CDataHeadMobile&, CBuffer& bufTemp);

	virtual void CheckData();

	virtual WORD GetDataNum();
	virtual void AddData(CSockData*);
	virtual void InsertData(CSockData*);
	virtual void DeleteData(DWORD);

	virtual BOOL IsIdle();
	
	virtual int EncodeDataPack(CSockData* pSockData, CBuffer& bufPack);
	virtual int DecodeDataPack(CDataHead& head, CBuffer& bufPack);

protected:
	void DeleteData();
	
	void DecodePack(CDataHead& head, CBuffer& bufTemp);

	virtual int Decode(CBuffer& bufTemp);
	virtual void Encode(CBuffer& bufTemp);

	void SetTime();

public:
	int	m_socket;

	DWORD m_dwRecvPack;		// 接收的包数
	long long m_dwRecvByte;		// 接收的字节数
	DWORD m_dwSendPack;		// 发送的包数
	long long m_dwSendByte;		// 发送的字节数
	DWORD m_dwRecvRecordtime_5m;// 5分钟刷新一次收包信息
	DWORD m_dwSendRecordtime_5m;// 5分钟刷新一次发包信息

	CString	m_strAddress1;
	WORD m_wPort1;

	CString	m_strAddress2;
	WORD m_wPort2;

	CString m_strCurAddr;
	WORD m_wCurPort;

	enum { WantNone = 0, WantSend, WantRecv, WantAll, WantDelete };
	WORD m_wStatus;

	CString	m_strMessage;

	volatile DWORD m_dwActiveTime;
	DWORD m_dwFirstTime;
	
	volatile BOOL m_bConnected;
	BOOL m_bLogined;

protected:
	CBuffer	m_bufRead;
	CBuffer	m_bufWrite;

	CPtrArray m_aData;

	char m_cHttp;	// -1:SOCK Old Head 0:SOCK DataHead 1:POST 2:RESP
	

public:
	//上次开始计时时间--10S清一次
	DWORD m_dwRecordtime_10s;
	//10S内收数据大小
	DWORD m_dwRecvDataSize_in10s;

	//上次开始计时时间--5秒清一次
	DWORD m_dwRecordtime_5s;
	//5S内收数据大小
	DWORD m_dwRecvDataSize_in5s;
};


#endif


