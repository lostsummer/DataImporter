#include "TradeCompress.h"
#include "BitStream.h"
#include "MDS.h"
#include "Data.h"
#include "DynaCompress.h"

//////////////////////////////////////////////////////////////////////

static CBitCode BC_TRADE_NUMBER[] = 
{
	{0x00,	1,	'C',	0,		1},					//0				= 1
	{0x02,	2,	'D',	7,		2},					//10	+7Bit	= 7Bit+2
	{0x06,	3,	'D',	12,		130},				//110	+12Bit	= 12Bit+128+2
	{0x0E,	4,	'D',	16,		4226},				//1110	+16Bit	= 16Bit+4096+128+2
	{0x0F,	4,	'O',	32,		0},					//1111	+32Bit	= 32Bit Org
};
static int BCNUM_TRADE_NUMBER = sizeof(BC_TRADE_NUMBER)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_TIMEDIFF[] = 
{
	{0x00,	1,	'C',	0,		3},			// 0				= 3s
	{0x06,	3,	'C',	0,		6},			// 110				= 6s
	{0x02,	2,	'D',	4,		0},			// 10	+4Bit		= 4Bit
	{0x0E,	4,	'D',	8,		16},		// 1110	+8Bit		= 8Bit+16
	{0x0F,	4,	'O',	18,		0},			// 1111	+18Bit		= 18Bit Org
};
static int BCNUM_BARGAIN_TIMEDIFF = sizeof(BC_BARGAIN_TIMEDIFF)/sizeof(CBitCode);

static CBitCode BC_TRADE_PRICE[] = 
{
	{0x02,	2,	'D',	8,		0},			//10	+8Bit		= 8Bit
	{0x00,	1,	'D',   16,		256},		//0		+16Bit		= 16Bit+256
	{0x06,	3,	'D',   24,		65792},		//110	+24Bit		= 24Bit+65536+256
	{0x07,	3,	'O',   32,		0},			//111	+32Bit		= 32Bit Org
};
static int BCNUM_TRADE_PRICE = sizeof(BC_TRADE_PRICE)/sizeof(CBitCode);

static CBitCode BC_TRADE_PRICE_DIFFS[] = 
{
	{0x00,	1,	'C',	0,		0},			// 0				= 0
	{0x02,	2,	'I',	2,		0},			// 10	+2Bit		= 2Bit
	{0x06,	3,	'I',	4,		2},			// 110	+4Bit		= 4Bit+2
	{0x0E,	4,	'I',	8,		10},		// 1110	+8Bit		= 8Bit+8+2
	{0x1E,	5,	'I',	16,		138},		// 11110+16Bit		= 16Bit+128+8+2
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_TRADE_PRICE_DIFFS = sizeof(BC_TRADE_PRICE_DIFFS)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_VOL[] = 
{
	{0x00,	1,	'H',	4,		0},			// 0	+4Bit		= (4Bit)*100
	{0x02,	2,	'H',	8,		16},		// 10	+8Bit		= (8Bit+16)*100
	{0x0C,	4,	'H',	12,		272},		// 1100	+12Bit		= (12Bit+256+16)*100
	{0x0D,	4,	'D',	12,		0},			// 1101+12Bit		= 12Bit
	{0x0E,	4,	'D',	16,		4096},		// 1110+16Bit		= 16Bit+4096
	{0x1E,	5,	'D',	24,		69632},		// 11110+24Bit		= 24Bit+65536+4096
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_BARGAIN_VOL = sizeof(BC_BARGAIN_VOL)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_QH_VOL[] = 
{
	{0x00,	1,	'D',	12,		0},			// 0	+12Bit		= 12Bit
	{0x02,	2,	'D',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x06,	3,	'D',	24,		69632},		// 110	+24Bit		= 24Bit+65536+4096
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_BARGAIN_QH_VOL = sizeof(BC_BARGAIN_QH_VOL)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_QH_OI[] = 
{
	{0x00,	1,	'I',	12,		0},			// 0	+12Bit		= 12Bit
	{0x02,	2,	'I',	16,		2048},		// 10	+16Bit		= 16Bit+2048
	{0x06,	3,	'I',	24,		34816},		// 110	+24Bit		= 24Bit+32768+2048
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_BARGAIN_QH_OI = sizeof(BC_BARGAIN_QH_OI)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_TRADENUM_DIFF[] = 
{
	{0x1E,	5,	'C',	0,		0},			// 11110			= 0
	{0x00,	2,	'C',	0,		1},			// 00				= 1
	{0x01,	2,	'C',	0,		2},			// 01				= 2
	{0x02,	2,	'D',	4,		3},			// 10	+4Bit		= 4Bit+3
	{0x06,	3,	'D',	6,		19},		// 110	+6Bit		= 6Bit+16+3
	{0x0E,	4,	'D',	8,		83},		// 1110	+8Bit		= 8Bit+64+16+3
	{0x3E,	6,	'D',	14,		339},		// 111110+8Bit		= 14Bit+256+64+16+3
	{0x3F,	6,	'O',	32,		0},			// 111111+32Bit		= 32Bit Org
};
static int BCNUM_BARGAIN_TRADENUM_DIFF = sizeof(BC_BARGAIN_TRADENUM_DIFF)/sizeof(CBitCode);

static CBitCode BC_BARGAIN_TRADENUM_DIFF_OLD[] = 
{
	{0x1E,	5,	'C',	0,		0},			// 11110			= 0
	{0x00,	2,	'C',	0,		1},			// 00				= 1
	{0x01,	2,	'C',	0,		2},			// 01				= 2
	{0x02,	2,	'D',	4,		3},			// 10	+4Bit		= 4Bit+3
	{0x06,	3,	'D',	6,		19},		// 110	+6Bit		= 6Bit+16+3
	{0x0E,	4,	'D',	8,		83},		// 1110	+8Bit		= 8Bit+64+16+3
	{0x1F,	5,	'O',	24,		0},			// 11111+24Bit		= 24Bit Org
};
static int BCNUM_BARGAIN_TRADENUM_DIFF_OLD = sizeof(BC_BARGAIN_TRADENUM_DIFF_OLD)/sizeof(CBitCode);

static CBitCode BC_TRADE_TRADENUM[] = 
{
	{0x06,	3,	'D',	8,		0},			// 110	+8Bit		= 8Bit
	{0x02,	2,	'D',	12,		256},		// 10	+12Bit		= 12Bit+256
	{0x00,	1,	'D',	16,		4352},		// 0	+16Bit		= 16Bit+4096+256
	{0x0E,	4,	'D',	20,		69888},		// 1110	+20Bit		= 20Bit+65536+4096+256
	{0x1E,	5,	'D',	24,		1118464},	// 11110+24Bit		= 24Bit+1048576+65536+4096+256
	{0x1F,	5,	'O',	32,		0},			// 11111+32Bit		= 32Bit Org
};
static int BCNUM_TRADE_TRADENUM = sizeof(BC_TRADE_TRADENUM)/sizeof(CBitCode);

static CBitCode BC_TRADE_TRADENUM_OLD[] = 
{
	{0x06,	3,	'D',	8,		0},			// 110	+8Bit		= 8Bit
	{0x02,	2,	'D',	12,		256},		// 10	+12Bit		= 12Bit+256
	{0x00,	1,	'D',	16,		4352},		// 0	+16Bit		= 16Bit+4096+256
	{0x0E,	4,	'D',	20,		69888},		// 1110	+20Bit		= 20Bit+65536+4096+256
	{0x0F,	4,	'O',	24,		0},			// 1111	+24Bit		= 24Bit Org
};
static int BCNUM_TRADE_TRADENUM_OLD = sizeof(BC_TRADE_TRADENUM_OLD)/sizeof(CBitCode);

//////////////////////////////////////////////////////////////////////

static CBitCode BC_DAY_PRICE_DIFF[] = 
{
	{0x00,	1,	'I',	4,		0},			// 0	+4Bit		= 4Bit
	{0x02,	2,	'I',	6,		8},			// 10	+6Bit		= 6Bit+8
	{0x06,	3,	'I',	8,		40},		// 110	+8Bit		= 8Bit+32+8
	{0x0E,	4,	'I',	16,		168},		// 1110	+16Bit		= 16Bit+128+32+8
	{0x0F,	4,	'O',	32,		0},			// 1111 +32Bit		= 32Bit Org
};
static int BCNUM_DAY_PRICE_DIFF = sizeof(BC_DAY_PRICE_DIFF)/sizeof(CBitCode);

static CBitCode BC_DAY_VOL[] = 
{
	{0x06,	3,	'D',	12,		0},			// 110	+12Bit		= 12Bit
	{0x02,	2,	'D',	16,		4096},		// 10	+16Bit		= 16Bit+4096
	{0x00,	1,	'D',	24,		69632},		// 0	+24Bit		= 24Bit+65536+4096
	{0x07,	3,	'O',	32,		0},			// 111  +32Bit		= 32Bit Org
};
static int BCNUM_DAY_VOL = sizeof(BC_DAY_VOL)/sizeof(CBitCode);

static CBitCode BC_DAY_AMNT[] = 
{
	{0x00,	2,	'I',	16,		0},			// 00	+16Bit		= 16Bit
	{0x01,	2,	'I',	24,		32768},		// 01	+24Bit		= 24Bit+32768
	{0x02,	2,	'I',	28,		8421376},	// 10	+28Bit		= 28Bit+8388608+32768
	{0x03,	2,	'O',	32,		0},			// 11	+32Bit		= 32Bit Org
};
static int BCNUM_DAY_AMNT = sizeof(BC_DAY_AMNT)/sizeof(CBitCode);

static CBitCode BC_DAY_TRADENUM[] = 
{
	{0x02,	2,	'D',	8,		0},			// 10	+8Bit		= 8Bit
	{0x00,	1,	'D',	12,		256},		// 0	+12Bit		= 12Bit+256
	{0x06,	3,	'D',	16,		4352},		// 110	+16Bit		= 16Bit+4096+256
	{0x07,	3,	'O',	24,		0},			// 111	+24Bit		= 24Bit Org
};
static int BCNUM_DAY_TRADENUM = sizeof(BC_DAY_TRADENUM)/sizeof(CBitCode);

//////////////////////////////////////////////////////////////////////

int CTradeCompress::ExpandBargain(CBitStream& stream, CGoods* pGoods, DWORD dwNum)
{
	if (pGoods==NULL || dwNum==0)
		return -1;

	BOOL bIsGZQH = pGoods->IsGZQH();

	BYTE cCheckSum = 0;

	try
	{
		DWORD PRICE_DIVIDE;
		BOOL bDiv10 = stream.ReadDWORD(1);
		if (bDiv10==FALSE)
			PRICE_DIVIDE = 1;
		else
			PRICE_DIVIDE = 10;
		//		pGoods->m_nPriceDiv = PRICE_DIVIDE;

		CBargain bar;		

		DWORD dwCurPrice, dwLastPrice;
		XInt32 xOI, xLastOI;

		for (DWORD dw=0; dw<dwNum; dw++)
		{
			if (dw==0)
			{
				bar.m_dwTime = stream.ReadDWORD(18);

				stream.DecodeData(dwCurPrice, BC_TRADE_PRICE, BCNUM_TRADE_PRICE);
				if (bIsGZQH==FALSE)
					stream.DecodeData(bar.m_dwTradeNum, BC_TRADE_TRADENUM, BCNUM_TRADE_TRADENUM);
			}
			else
			{
				DWORD dwSecondsDiff = 0;
				stream.DecodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);
				bar.m_dwTime = AddSeconds(bar.m_dwTime, dwSecondsDiff);

				stream.DecodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);

				if (bIsGZQH==FALSE)
				{
					DWORD dwLastTradeNum = bar.m_dwTradeNum;
					stream.DecodeData(bar.m_dwTradeNum, BC_BARGAIN_TRADENUM_DIFF, BCNUM_BARGAIN_TRADENUM_DIFF, &dwLastTradeNum);
				}
			}

			if (bIsGZQH==FALSE)
			{
				stream.DecodeXInt32(bar.m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
				bar.m_cBS = (char)stream.ReadDWORD(2) - 1;
			}
			else
			{
				stream.DecodeXInt32(bar.m_xVolume, BC_BARGAIN_QH_VOL, BCNUM_BARGAIN_QH_VOL);
				bar.m_cBS = (char)stream.ReadDWORD(2) - 1;

				if (dw==0)
					stream.DecodeXInt32(xOI, BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI);
				else
					stream.DecodeXInt32(xOI, BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI, &xLastOI);
				bar.m_dwTradeNum = xOI.GetRawData();
				xLastOI = xOI;
			}

			bar.m_dwPrice = dwCurPrice * PRICE_DIVIDE;

			pGoods->AddBargain(bar);
			cCheckSum += bar.GetCheckSum(PRICE_DIVIDE);

			dwLastPrice = dwCurPrice;
		}

		BYTE cCheck = (BYTE)stream.ReadDWORD(8);
		if (cCheck!=cCheckSum)
			return ERR_COMPRESS_CHECK;
	}
	catch (int)
	{
		return -2;
	}

	return 0;
}

int CTradeCompress::CompressBargainOld(CGoods* pGoods, CBargain* pBargain, int nBargainNum, CBitStream& stream)
{
	if (pGoods==NULL || pBargain==NULL || nBargainNum<=0)
		return -1;

	if (pGoods->m_dwClose==0)
		return -1;

	int nNum = 0;
	char cCheckSum = 0;

	try
	{
		CBargain* pCurBar = pBargain;

		// time
		stream.WriteDWORD(pCurBar->m_dwTime, 18);

		// price
		DWORD PRICE_DIVIDE = pGoods->GetPriceDivideOld();

		DWORD dwLastPrice = pGoods->m_dwClose/PRICE_DIVIDE;
		DWORD dwCurPrice = pCurBar->m_dwPrice/PRICE_DIVIDE;
		stream.EncodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);

		// volumn, tradenum, cBS
		stream.EncodeXInt32(pCurBar->m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
		stream.EncodeData(pCurBar->m_dwTradeNum, BC_TRADE_TRADENUM_OLD, BCNUM_TRADE_TRADENUM_OLD);
		stream.WriteDWORD(pCurBar->m_cBS+1, 2);

		nNum++;
		cCheckSum += pCurBar->GetCheckSumOld();

		CBargain* pLastBar = NULL;
		for(int i = 1; i < nBargainNum; i++)
		{
			pLastBar = pCurBar;
			pCurBar++;

			DWORD dwSecondsDiff = GetSecondsDiff(pLastBar->m_dwTime, pCurBar->m_dwTime);
			stream.EncodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);

			dwLastPrice = dwCurPrice;
			dwCurPrice = pCurBar->m_dwPrice/PRICE_DIVIDE;
			stream.EncodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);

			stream.EncodeXInt32(pCurBar->m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
			stream.EncodeData(pCurBar->m_dwTradeNum, BC_BARGAIN_TRADENUM_DIFF_OLD, BCNUM_BARGAIN_TRADENUM_DIFF_OLD, &pLastBar->m_dwTradeNum);
			stream.WriteDWORD(pCurBar->m_cBS+1, 2);

			nNum++;
			cCheckSum += pCurBar->GetCheckSumOld();
		}

		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int nErrCode)
	{
		if (nErrCode!=ERROR_BITSTREAM_NOMEM)
			return -2;
	}

	return nNum;
}

int CTradeCompress::CompressDayOld(CGoods* pGoods, CDay* pDay, int nDayNum, CBitStream& stream)
{
	if (pGoods==NULL || pDay==NULL || nDayNum<=0)
		return -1;

	int nNum = 0;
	char cCheckSum = 0;

	try
	{
		//		DWORD PRICE_DIVIDE = pGoods->GetPriceDivide();
		DWORD PRICE_DIVIDE = 1;
		DWORD dwCurPrice, dwLastPrice;
		CDay* pCurDay = pDay;
		CDay day;

		for (int i=0; i<nDayNum; i++)
		{
			stream.WriteDWORD(pCurDay->m_dwTime, 32);
			day.m_dwTime = pCurDay->m_dwTime;

			dwCurPrice = pCurDay->m_dwClose/PRICE_DIVIDE;
			if (i==0)
				stream.WriteDWORD(dwCurPrice, 32);					///??? more compress
			else
				stream.EncodeData(dwCurPrice, BC_DAY_PRICE_DIFF, BCNUM_DAY_PRICE_DIFF, &dwLastPrice);
			day.m_dwClose = pCurDay->m_dwClose;
			dwLastPrice = dwCurPrice;

			// volumn
			stream.EncodeXInt32(pCurDay->m_xVolume, BC_DAY_VOL, BCNUM_DAY_VOL);
			if (pCurDay->m_xVolume.GetValue()>0)
			{
				stream.EncodeData(pCurDay->m_dwOpen/PRICE_DIVIDE, BC_DAY_PRICE_DIFF, BCNUM_DAY_PRICE_DIFF, &dwLastPrice);
				stream.EncodeData(pCurDay->m_dwHigh/PRICE_DIVIDE, BC_DAY_PRICE_DIFF, BCNUM_DAY_PRICE_DIFF, &dwLastPrice);
				stream.EncodeData(pCurDay->m_dwLow/PRICE_DIVIDE, BC_DAY_PRICE_DIFF, BCNUM_DAY_PRICE_DIFF, &dwLastPrice);

				stream.EncodeXInt32(pCurDay->m_xAmount, BC_DAY_AMNT, BCNUM_DAY_AMNT);

				//				stream.EncodeData(pCurDay->m_dwTradeNum, BC_DAY_TRADENUM, BCNUM_DAY_TRADENUM);
				stream.WriteDWORD(pCurDay->m_dwTradeNum, 32);		///???

				stream.EncodeXInt32(pCurDay->m_xNeiPan, BC_DAY_VOL, BCNUM_DAY_VOL);

				day.m_dwOpen = pCurDay->m_dwOpen;
				day.m_dwHigh = pCurDay->m_dwHigh;
				day.m_dwLow = pCurDay->m_dwLow;

				day.m_xAmount = pCurDay->m_xAmount;
				day.m_dwTradeNum = pCurDay->m_dwTradeNum;
				day.m_xNeiPan = pCurDay->m_xNeiPan;
			}
			else
			{
				day.m_dwOpen = day.m_dwHigh = day.m_dwLow = day.m_dwClose;
			}
			day.m_xVolume = pCurDay->m_xVolume;

			if (pCurDay->m_dwNumOfBuy+pCurDay->m_dwNumOfSell>0)
			{
				stream.WriteDWORD(1, 1);

				stream.EncodeData(pCurDay->m_dwNumOfBuy, BC_DAY_TRADENUM, BCNUM_DAY_TRADENUM);
				stream.EncodeData(pCurDay->m_dwNumOfSell, BC_DAY_TRADENUM, BCNUM_DAY_TRADENUM);

				day.m_dwNumOfBuy = pCurDay->m_dwNumOfBuy;
				day.m_dwNumOfSell = pCurDay->m_dwNumOfSell;

				for (int j=0; j<4; j++)
				{
					stream.EncodeXInt32(pCurDay->m_pxVolOfBuy[j], BC_DAY_VOL, BCNUM_DAY_VOL);
					stream.EncodeXInt32(pCurDay->m_pxVolOfSell[j], BC_DAY_VOL, BCNUM_DAY_VOL);

					day.m_pxVolOfBuy[j] = pCurDay->m_pxVolOfBuy[j];
					day.m_pxVolOfSell[j] = pCurDay->m_pxVolOfSell[j];
				}
			}
			else
				stream.WriteDWORD(0, 1);

			///???			cCheckSum += day.GetCheckSumOld();

			pCurDay++;
			nNum++;
		}

		stream.WriteDWORD(cCheckSum, 8);
	}
	catch (int nErrCode)
	{
		if (nErrCode!=ERROR_BITSTREAM_NOMEM)
			return -2;
	}

	return nNum;
}

int CTradeCompress::CompressBargain(CBitStream& stream, CDataBargain* pBargain, int nRecvNum, short sWant, bool bRolling)
{
	if (pBargain==NULL)
		return -1;

	//BYTE cCheckSum = 0;
	try
	{
		XInt32 xOI, xLastOI;
		DWORD dwLastPrice;
		DWORD dwCurPrice;

		int nFirst = 0;
		int nNum = 0;

		DWORD dwTotal = pBargain->m_dwSize;
		//if (pBargain->m_pGoods->m_dwBargainTotal > 0 && dwTotal > pBargain->m_pGoods->m_dwBargainTotal)
		//	dwTotal = pBargain->m_pGoods->m_dwBargainTotal;

		BOOL bIsGZQH = pBargain->m_pGoods->IsGZQH();
		stream.EncodeData(dwTotal, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);

		if (bRolling)
		{
			if (nRecvNum > 0 && nRecvNum > dwTotal)
			{
				stream.WriteBOOL(TRUE);
				nRecvNum = dwTotal;
			}
			else
				stream.WriteBOOL(FALSE);

			if (nRecvNum < 0)
			{
				sWant = sWant > 0 ? sWant : -sWant; 
				nRecvNum = dwTotal;
			}
			else if (nRecvNum == 0)
			{
				sWant = sWant < 0 ? -sWant : sWant;
				nRecvNum += sWant;
			}
			else
			{
				nRecvNum += sWant;
				if (nRecvNum <= 0) nRecvNum += abs(sWant);
				if (nRecvNum > dwTotal) nRecvNum = dwTotal;
			}

			nFirst = nRecvNum - abs(sWant);
			if (nFirst < 0)
			{
				nRecvNum = abs(sWant);
				nFirst = 0;
			}
			nNum = dwTotal - nFirst;
			if (nNum > abs(sWant))	
				nNum = abs(sWant);
		}
		else
		{
			if (nRecvNum > dwTotal)
			{
				stream.WriteBOOL(TRUE);
				nRecvNum = 0;
			}
			else
				stream.WriteBOOL(FALSE);
			nFirst = nRecvNum;
			nNum = dwTotal - nFirst;
			if (nNum > (int)sWant)
			{
				nNum = (int)sWant;
				nFirst = dwTotal - nNum;
			}
		}
		
		DWORD PRICE_DIVIDE = pBargain->m_pGoods->GetPriceDiv2();
		stream.WriteBOOL(PRICE_DIVIDE!=1);

		CBargain* pCurBar = pBargain->m_pBuffer+nFirst;
		CBargain* pLastBar = NULL;
		if (nFirst > 0)
			pLastBar = pBargain->m_pBuffer+(nFirst-1);
		else
			pLastBar = pBargain->m_pBuffer+nFirst;

		if (bRolling)
			stream.EncodeData(nRecvNum, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);

		stream.EncodeData(nNum, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);
		for (int n=0; n<nNum; n++)
		{
			dwCurPrice = pCurBar->m_dwPrice/PRICE_DIVIDE;

			if (n==0)
			{
				stream.WriteDWORD(pCurBar->m_dwTime, 18);

				stream.EncodeData(dwCurPrice, BC_TRADE_PRICE, BCNUM_TRADE_PRICE);
				//if (bIsGZQH==FALSE)
				//	stream.EncodeData(pCurBar->m_dwTradeNum, BC_TRADE_TRADENUM, BCNUM_TRADE_TRADENUM);
			}
			else
			{
				DWORD dwSecondsDiff = GetSecondsDiff(pLastBar->m_dwTime, pCurBar->m_dwTime);
				stream.EncodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);

				stream.EncodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);
				//if (bIsGZQH==FALSE)
				//	stream.EncodeData(pCurBar->m_dwTradeNum, BC_BARGAIN_TRADENUM_DIFF, BCNUM_BARGAIN_TRADENUM_DIFF, &pLastBar->m_dwTradeNum);
			}

			if (bIsGZQH==FALSE)
			{
				DWORD dwTradeNum = pCurBar->m_dwTradeNum - pLastBar->m_dwTradeNum;
				stream.EncodeData(dwTradeNum, BC_BARGAIN_TRADENUM_DIFF, BCNUM_BARGAIN_TRADENUM_DIFF);
				stream.EncodeXInt32(pCurBar->m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
				stream.WriteDWORD(pCurBar->m_cBS+1, 2);
			}
			else
			{
				stream.EncodeXInt32(pCurBar->m_xVolume, BC_BARGAIN_QH_VOL, BCNUM_BARGAIN_QH_VOL);
				stream.WriteDWORD(pCurBar->m_cBS+1, 2);

				xOI.SetRawData(pCurBar->m_dwTradeNum);
				if (n==0)
				{
					stream.EncodeXInt32(XInt32(pLastBar->m_dwTradeNum), BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI);
					stream.EncodeXInt32(xOI, BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI);
				}
				else
					stream.EncodeXInt32(xOI, BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI, &xLastOI);
				xLastOI = xOI;
			}

			//cCheckSum += pCurBar->GetCheckSum();

			dwLastPrice = dwCurPrice;
			pLastBar = pCurBar;
			pCurBar++;
		}

		//stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return -2;
	}

	return 0;
}

int CTradeCompress::CompressBargain2(CBitStream& stream, CGoods* pGoods, CBargain* pBargain, DWORD dwNum)
{
	if (pGoods==NULL || pBargain==NULL || dwNum==0)
		return -1;

	BYTE cCheckSum = 0;

	try
	{
		DWORD PRICE_DIVIDE = pGoods->GetPriceDiv2();

		CBargain* pCurBar = pBargain;
		CBargain* pLastBar = NULL;

		DWORD dwLastPrice;
		DWORD dwCurPrice;

		for (DWORD dw=0; dw<dwNum; dw++)
		{
			dwCurPrice = pCurBar->m_dwPrice/PRICE_DIVIDE;

			if (dw==0)
			{
				stream.WriteDWORD(pCurBar->m_dwTime, 18);

				stream.EncodeData(dwCurPrice, BC_TRADE_PRICE, BCNUM_TRADE_PRICE);
				stream.EncodeData(pCurBar->m_dwTradeNum, BC_TRADE_TRADENUM_OLD, BCNUM_TRADE_TRADENUM_OLD);
			}
			else
			{
				DWORD dwSecondsDiff = GetSecondsDiff(pLastBar->m_dwTime, pCurBar->m_dwTime);
				stream.EncodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);

				stream.EncodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);
				stream.EncodeData(pCurBar->m_dwTradeNum, BC_BARGAIN_TRADENUM_DIFF_OLD, BCNUM_BARGAIN_TRADENUM_DIFF_OLD, &pLastBar->m_dwTradeNum);
			}

			stream.EncodeXInt32(pCurBar->m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
			stream.WriteDWORD(pCurBar->m_cBS+1, 2);

			cCheckSum += pCurBar->GetCheckSum(PRICE_DIVIDE);

			dwLastPrice = dwCurPrice;
			pLastBar = pCurBar;
			pCurBar++;
		}

		stream.WriteDWORD(cCheckSum, 8);
	}
	catch(int)
	{
		return -2;
	}

	return 0;
}

void CTradeCompress::CompressNum(CBitStream& stream, DWORD dwNum)
{
	stream.EncodeData(dwNum, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);
}


DWORD CTradeCompress::ExpandNum(CBitStream& stream)
{
	DWORD dwNum = 0;
	stream.DecodeData(dwNum, BC_TRADE_NUMBER, BCNUM_TRADE_NUMBER);

	return dwNum;
}
//////////////////////////////////////////////////////////////////////

int CTradeCompress::ExpandBargain6(CBitStream& stream, CGoods* pGoods, DWORD dwNum)
{

	if (pGoods==NULL || dwNum==0)
		return -1;

	BYTE cCheckSum = 0;

	try
	{
		char cDiv = (char)stream.ReadDWORD(3);
		DWORD dwDiv = g_CheckDiv(cDiv);
		if (dwDiv==0)
			return ERR_COMPRESS_DIV;

		BOOL bIsQH = stream.ReadDWORD(1);

		CBargain bar;		

		DWORD dwCurPrice, dwLastPrice;
		XInt32 xOI, xLastOI;

		for (DWORD dw=0; dw<dwNum; dw++)
		{
			if (stream.ReadDWORD(1))
			{
				if (stream.ReadDWORD(1))
					bar.m_dwDate = CDynaCompress::ExpandDate(stream);
				else
					bar.m_dwDate = 0;

				bar.m_dwTime = stream.ReadDWORD(18);
			}
			else
			{
				DWORD dwSecondsDiff = 0;
				stream.DecodeData(dwSecondsDiff, BC_BARGAIN_TIMEDIFF, BCNUM_BARGAIN_TIMEDIFF);
				bar.m_dwTime = AddSeconds(bar.m_dwTime, dwSecondsDiff);
			}

			if (dw==0)
			{
				stream.DecodeData(dwCurPrice, BC_TRADE_PRICE, BCNUM_TRADE_PRICE);
				if (bIsQH==FALSE)
					stream.DecodeData(bar.m_dwTradeNum, BC_TRADE_TRADENUM, BCNUM_TRADE_TRADENUM);
			}
			else
			{
				stream.DecodeData(dwCurPrice, BC_TRADE_PRICE_DIFFS, BCNUM_TRADE_PRICE_DIFFS, &dwLastPrice);

				if (bIsQH==FALSE)
				{
					DWORD dwLastTradeNum = bar.m_dwTradeNum;
					stream.DecodeData(bar.m_dwTradeNum, BC_BARGAIN_TRADENUM_DIFF, BCNUM_BARGAIN_TRADENUM_DIFF, &dwLastTradeNum);
				}
			}

			if (bIsQH==FALSE)
			{
				stream.DecodeXInt32(bar.m_xVolume, BC_BARGAIN_VOL, BCNUM_BARGAIN_VOL);
				bar.m_cBS = (char)stream.ReadDWORD(2) - 1;
			}
			else
			{
				stream.DecodeXInt32(bar.m_xVolume, BC_BARGAIN_QH_VOL, BCNUM_BARGAIN_QH_VOL);
				bar.m_cBS = (char)stream.ReadDWORD(2) - 1;

				if (dw==0)
					stream.DecodeXInt32(xOI, BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI);
				else
					stream.DecodeXInt32(xOI, BC_BARGAIN_QH_OI, BCNUM_BARGAIN_QH_OI, &xLastOI);
				bar.m_dwTradeNum = xOI.GetRawData();
				xLastOI = xOI;
			}

			bar.m_dwPrice = dwCurPrice * dwDiv;

			pGoods->AddBargain(bar);
			cCheckSum += bar.GetCheckSum(dwDiv);

			dwLastPrice = dwCurPrice;
		}

		BYTE cCheck = (BYTE)stream.ReadDWORD(8);
		if (cCheck!=cCheckSum)
			return ERR_COMPRESS_CHECK;
	}
	catch (int)
	{
		return -2;
	}

	return 0;

}


