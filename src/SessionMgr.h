
/************************************
  REVISION LOG ENTRY
    Revision By: liuchengzhu
	  Revised on 2009-11-13 10:31:53
	    Comments: 会话管理模块
		 ************************************/

#ifndef SESSIONMGR_H
#define SESSIONMGR_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


//Win API
#include <time.h>
//C++ STL
#include <string>
#include <map>
#include <list>

//
#include "Mutex.h"
using namespace std;

//功能点信息
struct st_FuncInfo
{
	int bitmask;
	string name;
};

//会话信息结构
struct st_session
{
	//会话标识
	DWORD session_id;
	//手机号码
	string user_id;
	//开始时间
	DWORD begin_time;
	//结束时间
	DWORD end_time;
	//厂商类型
	int Vendor;		
	//用户权限位图
	int UserRight;				
};

//会话管理者
class CSessionMgr
{
public:
	//构造函数
	CSessionMgr();

	//析构函数
	~CSessionMgr();

	//增加会话(根据用户类型和服务类型生成会话)
	DWORD NewSession(string const& UserId,short UserType,short ServiceType);

	//增加会话
	void AddUserSession(string const& UserId,DWORD const& SessionId,DWORD const& Vendor,DWORD const& UserRight);

	//更新会话最新活动时间
	void Fresh(DWORD const& SessionId);

	//检查超时会话
	void Check(int sTimeOut);

	//映射表会话总数
	size_t size();

	//清除映射表会话
	void clear();

	//查询用户级别(L1,L2)
	short GetSessionUserLevel(DWORD const& SessionId);

	//查询会话用户类型
	short GetSessionUserType(DWORD const& SessionId);

	//查询会话服务类型
	short GetSessionServiceType(DWORD const& SessionId);


private:
	//获取时间
	void GetTime(time_t	*lt);

	//根据用户标识和用户类型产生会话标识
	void CreateNewRandomString(string const& UserId,short UserType,short ServiceType,DWORD& out_Random);


	//获取格式化时间串
	string GetFormatTime(time_t *lt);

private:
	//会话标识--会话信息映射表
	map<DWORD,st_session *>	map_Session2User;

	//会话标识--会话信息映射表 临界区
	Mutex 	m_Lock;


	//超时会话列表
	list<st_session *>	lst_TimeOutSession;


	//功能编号--功能
	map<WORD,st_FuncInfo>  m_mapFuncNoToInfo;

public:
	//生成新的匿名用户名
	string NewUserName(void);

	//add by liuchengzhu 20100417
	//获取用户功能权限情况
	int GetRight(int SessionId, int FuncNo);
};

#endif

