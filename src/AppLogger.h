/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file AppLogger.h
* @brief 
*
* $Author: panlishen$
* $Date: Wed Nov 18 19:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef APPLOGGER_H
#define APPLOGGER_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <list>
#include <vector>
#include <stdlib.h>
#include <stdio.h>

#include "Mutex.h"
#include "SimpleFile.h"
#include "SysGlobal.h"
#include "Util.h"

//cross platform :)

/**
* @brief log target
*/
enum LogTarget {LOG_TO_FILE = 0, LOG_TO_SERVER = 1, LOG_TO_DEBUGGER = 2, LOG_TO_MAX = 3};

/**
* @brief log type
*/
enum LogType {LOG_APP = 0, LOG_ERROR = 1, LOG_INFO = 2, LOG_TRACE = 3, LOG_TYPE_MAX = 4};

/**
* @brief pre declare
*/
class  LogData;
typedef std::list<LogData*> LogDataList;

/**
* @brief class LogData define
*/
class LogData
{
public:
	enum { DEFAULT_BUFSIZE = 512 };

	/**
	* @brief constructor
	*/
	LogData(PBYTE Buffer, size_t BufferSize);

	/**
	* @brief destructor
	*/
	~LogData();

	/**
	* @brief get buffer of log data
	* @return address of buffer pointer
	*/
	char* GetBuffer() const { return (char*)m_Buffer; }

	/**
	* @brief get buffer size of log data
	* @return buffer size of log data
	*/
	size_t GetSize() const { return m_BuferSize; }

	/**
	* @brief get type of log data
	* @return type of log data
	*/
	LogType GetType() { return m_Type; }

	/**
	* @brief set type of log data
	* @param tp
	*/
	void SetType(LogType tp) { m_Type = tp; };

	/**
	* @brief set message size of log data
	* @param hs
	*/
	void SetMsgSize(size_t hs) { m_MsgSize = hs; }

	/**
	* @brief get message size of log data
	* @return message size of log data
	*/
	size_t GetMsgSize() { return m_MsgSize; }

	void SetMsgBuf(PBYTE mb) { m_MsgBuf = mb; }
	PBYTE GetMsgBuf() { return m_MsgBuf; }

	void SetLength(size_t len) { m_Length = len; }
	size_t GetLength() const { return m_Length; }

	/**
	* @brief readjust buffer size of log data
	* @return return a LogData that contain old buffer and this contain new buffer
	* @return return 0 if memory allocate failed and this remain unchanged
	*/
	LogData* Resize(size_t NewSize);

	/**
	* @brief get Container of log data
	*/
	LogDataList::iterator& GetContainer() { return m_Iter; }

	/**
	* @brief set Container of log data
	*/
	void SetContainer(LogDataList::iterator iter) { m_Iter = iter; }

	bool operator <= (const LogData& a ) { return GetSize() <= a.GetSize(); }

private:
	LogData(){;}

private:
	LogDataList::iterator  m_Iter;
	PBYTE		   		   m_Buffer;	// buffer of log data
	size_t				   m_BuferSize;	// buffer size
	LogType 		   	   m_Type;		// type of log data
	size_t				   m_Length;
	size_t				   m_MsgSize;	// msg size
	PBYTE				   m_MsgBuf;	// msg buffer
};

//////////////////////////////////////////////////////////////////////////
/**
* @brief class LogBase define
*/
class LogBase
{
public:
	/**
	* @brief constructor
	*/
	LogBase(){}
	
	/**
	* @brief destructor
	*/
	virtual ~LogBase(){}

	/**
	* @brief output log data
	* @param logData : a pointer of log data
	*/
	virtual void Log(LogData* logData)
	{
#ifdef _DEBUG

	#ifdef WIN32
		OutputDebugString(logData->GetBuffer());
	#else
		printf("%s", logData->GetBuffer());
	#endif
		
#endif
	}
};

//////////////////////////////////////////////////////////////////////////
/**
* @brief struct LogSocketInfo define
*/
struct LogSocketInfo
{
	std::string IP;
	int Port;
};

/**
* @brief class LogToSocketServer define(not implement)
*/
class LogToSocketServer
{
public:
	/**
	* @brief constructor
	*/
	LogToSocketServer(){}
		
	/**
	* @brief destructor
	*/
	virtual ~LogToSocketServer(){}

	LogSocketInfo& GetLogInfo() { return m_Info; }

	/**
	* @brief initialize(such as connect to log server etc.)
	*/
	bool Initialize(){return false;}

	/**
	* @brief terminate(such as shut down connection etc.)
	*/
	void Terminate(){}

	/**
	* @brief send log data to server.
	*/
	virtual void Log(LogData* LogData){}

private:
	LogSocketInfo m_Info; // log socket info
};

//////////////////////////////////////////////////////////////////////////
/**
* @brief struct LogFileInfo define
*/
struct LogFileInfo
{
	std::string 		FileNameBase;
	std::string 		Dir;
	bool 		SeperateByDate;
	size_t 		MaxFileSize;
	size_t 		MaxFileNumb;
};

/**
* @brief class LogToFile define
*/
class LogToFile
{
public:
	/**
	* @brief constructor
	*/
	LogToFile(){}

	/**
	* @brief destructor
	*/
	virtual ~LogToFile(){}

	LogFileInfo& GetLogInfo() { return m_Info; }

	/**
	* @brief initialize : create log path / log files
	*/
	bool Initialize();

	/**
	* @brief terminate : close log file
	*/
	void Terminate();

	/**
	* @brief log to file
	*/
	virtual void Log(LogData* LogData);

	/**
	* @brief get full path
	*/
	virtual std::string GetFullPath() { return m_File.GetFileName(); }


private:
	/**
	* @brief Create Next File when file size same as the setting. or other need to when condition done. 
	*/
	void CreateNextFile();

private:
	LogFileInfo m_Info;
	SimpleFile	m_File;
	Mutex m_FileChangeLocker;
	time_t m_FileNameTime;
};

//////////////////////////////////////////////////////////////////////////
struct LogEnableFlag
{
	DWORD Target[LOG_TO_MAX];
};

class LogManager;

/**
* @brief class LogConfig define
*/
class LogConfig
{
	friend class LogManager;// for calling Initialize, Terminate, GetLogEnableFlag
public:
	/**
	* @brief constructor
	*/
	LogConfig(){}

	/**
	* @brief destructor
	*/
	virtual ~LogConfig(){}

	virtual void OnNewLogFile(std::string& FileNameBase, std::string& FileName, bool Succeeded) {}
	
	/**
	* @brief return how many log file are needed
	* @brief because of one file serve a log_type, the valid return value must range from 0 to LOG_TYPE_MAX - 1
	*/
	virtual size_t GetLogFileCount() = 0;

	/**
	* @brief FileID rang from 1 to GetLogFileCount();
	*/
	virtual void GetLogFileInfo(size_t FileID, LogFileInfo& FileInfo) = 0;

	/**
	* @brief if no log server supported, return false
	*/
	virtual bool GetLogSocketServerInfo(LogSocketInfo& ServerSocketInfo) = 0;

	/**
	* @brief for PLogType == LOG_TO_SERVER, TargetID is unused
	* @brief PLogType == LOG_TO_DEBUGGER, TargetID is unused
	* @brief PLogType == LOG_TO_FILE, TargetID rang from 1 to GetLogFileCount();
	*/
	virtual bool IsLogEnabled(LogType, LogTarget, int TargetID) = 0;

	void EnableLog(LogType, DWORD LogTargetBitMask, int TargetID);
	void DisableLog(LogType, DWORD LogTargetBitMask);

private:
	bool Initialize();
	void Terminate();
	LogEnableFlag& GetLogEnableFlag(LogType);

private:
	LogEnableFlag	m_LogEnableFlag[LOG_TYPE_MAX];
	LogManager*		m_Manager;
};

//////////////////////////////////////////////////////////////////////////
/**
* @brief class LogManager define
*/
class LogManager : public YSingleton<LogManager>
{                                                             
public:
	/**
	* @brief constructor
	*/
	LogManager();

	/**
	* @brief destructor
	*/
	~LogManager();

	void init(LogConfig* config);
	LogConfig* GetLogConfig() { return m_LogConfig; }
	LogData* GetData(size_t DataSize);
	void FreeData(LogData*, bool bRemoveFromContainer);

	bool DoLog(LogData*);
	
	/**
	* @brief prepare log data, add log header, log tail.
	*/
	LogData* PrepareLogData(LogType LogType, size_t MsgBufSize, const char* format_str, va_list argp);

	bool Log(LogType LogType, const char* format, va_list argp);

private:
	bool IsLogEnabled(LogType logType)
	{
		if (m_LogConfig)
		{
			LogEnableFlag& flags = m_LogConfig->GetLogEnableFlag(logType);
			for (size_t i = 0; i < LOG_TO_MAX; i++)
				if (flags.Target[i])
					return true;
		}

		return false;
	}

private:
	LogConfig* 		 		 m_LogConfig;
	LogDataList 			 m_FreeData; 	// descend ordered, begin() is biggest
	LogDataList 			 m_Data;
	Mutex					 m_Locker;
public:
	std::vector<LogToFile*>		 m_FileLoggers;
private:
	LogToSocketServer		 m_SocketLogger;
	LogBase					 m_DebuggerLogger;
	volatile bool			 m_init;
	volatile int			 m_LogID[LOG_TYPE_MAX];
	Mutex					 m_logIDLocker;
};

//////////////////////////////////////////////////////////////////////////
/**
* @brief at first you should inherit LogConfig class to set your setting of log, then call this function.
*/
void LogInitialize(LogConfig*);

/**
* @brief when application exit you should call this function to free memory.
*/
void LogTerminate();

size_t LogApp(const char* format_str, ...);
size_t LogDbg(const char* format_str, ...);
size_t LogTrace(const char* format_str, ...);
size_t LogErr(const char* format_str, ...);
size_t LogInfo(const char* format_str, ...);

#define LOG_APP(format, ...)  LogApp(format "[%s %s:%d]", ##__VA_ARGS__, __FUNCTION__, __FILE__, __LINE__)
#define LOG_DBG(format, ...)  LogDbg(format "[%s %s:%d]", ##__VA_ARGS__, __FUNCTION__, __FILE__, __LINE__)
#define LOG_TRACE(format, ...)  LogTrace(format "[%s %s:%d]", ##__VA_ARGS__, __FUNCTION__, __FILE__, __LINE__)
#define LOG_ERR(format, ...)  LogErr(format "[%s %s:%d]", ##__VA_ARGS__, __FUNCTION__, __FILE__, __LINE__)
#define LOG_INFO(format, ...)  LogInfo(format "[%s %s:%d]", ##__VA_ARGS__, __FUNCTION__, __FILE__, __LINE__)

#endif


