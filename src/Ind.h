#ifndef IND_H
#define IND_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

//#include <loki/SmallObj.h>
#include "XInt32.h"

class CGoods;
struct CFDayMobile;
typedef enum 
{
	ECWX_BEGIN,
	ECWX_L2,
	ECWX_L1,
	ECWX_H1,
	ECWX_H2,

} E_CWXLEVEL;
class CInd //: public  Loki::SmallObject<Loki::ClassLevelLockable>
{
public:
	CInd();
	CInd(CGoods* pGoods);
	virtual ~CInd();

public:
	virtual void Calc(CFDayMobile* pFDay, int nNum) = 0;

protected:
	virtual char GetGroup();

	CGoods* m_pGoods;

public:
	char m_cParamSize;
	short m_psParam[6];

	char m_cExpSize; //返回的指标根数，最多六根，一般在calc里面设置
	int m_pnFirst[6];
};

class CInd_MA : public CInd
{
public:
	CInd_MA();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_VMA : public CInd
{
public:
	CInd_VMA();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_MACD : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_KDJ : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_RSI : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_WR : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_VR : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//资金流变
class CIndL2_ZJLB : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//资金博弈
class CIndL2_ZJBY : public CInd
{
public:
	CIndL2_ZJBY(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//大单比率
class CIndL2_DDBL : public CInd
{
public:
	CIndL2_DDBL(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//筹码聚散
class CIndL2_CMJS : public CInd
{
public:
	CIndL2_CMJS(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//超级资金
class CIndL2_CJZJ : public CInd
{
public:
	CIndL2_CJZJ(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//趋向指标
class CInd_DMI : public CInd
{
public:
	CInd_DMI();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//平均差
class CInd_DMA : public CInd
{
public:
	CInd_DMA();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//三重指数平均线
class CInd_TRIX : public CInd
{
public:
	CInd_TRIX();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//情绪指标
class CInd_BRAR : public CInd
{
public:
	CInd_BRAR();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//带状能量线
class CInd_CR : public CInd
{
public:
	CInd_CR();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//累计能量线
class CInd_OBV : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//振动升降指标
class CInd_ASI : public CInd
{
public:
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//简易波动指标
class CInd_EMV : public CInd
{
public:
	CInd_EMV();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//商品路径指标
class CInd_CCI : public CInd
{
public:
	CInd_CCI();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//变动率指标
class CInd_ROC : public CInd
{
public:
	CInd_ROC();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//动量线
class CInd_MTM : public CInd
{
public:
	CInd_MTM();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//心理线
class CInd_PSY : public CInd
{
public:
	CInd_PSY();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//布林线
class CInd_BOLL : public CInd
{
public:
	CInd_BOLL();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//分水岭
class CInd_FSL : public CInd
{
public:
	CInd_FSL(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};


//抛物线指标
class CInd_SAR : public CInd
{
public:
	CInd_SAR();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

////市场成本
class CInd_MCST : public CInd
{
public:
	CInd_MCST(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//散户资金
class CIndL2_SHZJ : public CInd
{
public:
	CIndL2_SHZJ(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

//大户资金
class CIndL2_DHZJ : public CInd
{
public:
	CIndL2_DHZJ(CGoods* pGoods) : CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CDayCostPV
{
public:
	CDayCostPV()
	{
		Initial();
	}

	void Initial()
	{
		m_fPrice = 0;
		m_fRedVolume = 0;
		m_fGreenVolume = 0;
	};

	double m_fPrice;
	double m_fRedVolume;
	double m_fGreenVolume;
};

class CInd_ABJB : public CInd
{
public:
	CInd_ABJB(CGoods* pGoods);
	int CompareFloat(double f);
	double GetLTG();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};


class CInd_LTSH : public CInd
{
public:
	CInd_LTSH(CGoods* pGoods);
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};


class CInd_QSDD : public CInd
{
public:
	CInd_QSDD(CGoods* pGoods);
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_CWX : public CInd
{
public:
	CInd_CWX(CGoods* pGoods): CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

/*
class CInd_Fee_CWX : public CInd
{
public:
	CInd_Fee_CWX(CGoods* pGoods): CInd(pGoods){}
	virtual void Calc(CFDayMobile* pFDay, int nNum);
	BOOL CalcDiff(int nToday,int nYesterday,int& iInfoIndex);
	E_CWXLEVEL GetCWXLevel( UINT actVal );
};
*/

// 增加EXPMA, SLOWKD, ENE, BIAS指标 [8/19/2016 qianyifan]
class CInd_EMA : public CInd
{
public:
	CInd_EMA();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_SLOWKD : public CInd
{
public:
	CInd_SLOWKD();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};

class CInd_ENE : public CInd
{
public:
	CInd_ENE();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};


class CInd_BIAS : public CInd
{
public:
	CInd_BIAS();
	virtual void Calc(CFDayMobile* pFDay, int nNum);
};
#endif
