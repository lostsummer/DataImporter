/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file Mutex.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef _MUTEXT_H_
#define _MUTEXT_H_

#include <pthread.h>
#include "Noncopyable.h"


class IMutex
{
public:
	friend class Condition;
	enum type
	{
#if defined(__FreeBSD__) ||  defined(__APPLE_CC__) || defined(__OpenBSD__) || defined(__DragonFly__)
		fast = PTHREAD_MUTEX_ADAPTIVE,
		recursive = PTHREAD_MUTEX_RECURSIVE,
		timed = PTHREAD_MUTEX_TIMED,
		errorcheck = PTHREAD_MUTEX_ERRORCHECK
#else
		fast = PTHREAD_MUTEX_ADAPTIVE_NP,
		recursive = PTHREAD_MUTEX_RECURSIVE_NP,
		timed = PTHREAD_MUTEX_TIMED_NP,
		errorcheck = PTHREAD_MUTEX_ERRORCHECK_NP
#endif
	};
	
	virtual ~IMutex(){}
	virtual void Lock() = 0;
	virtual void UnLock() = 0;

protected:
	pthread_mutex_t mutex;
};

class FastMutex : public IMutex
{
public:
	FastMutex()
	{
		pthread_mutexattr_t attr;
		::pthread_mutexattr_init(&attr);
		::pthread_mutexattr_settype(&attr, fast);
		::pthread_mutex_init(&mutex, &attr);
		::pthread_mutexattr_destroy(&attr);
	}

	~FastMutex()
	{
		::pthread_mutex_destroy(&mutex); 
	}

	void Lock()
	{
		::pthread_mutex_lock(&mutex);
	}

	void UnLock()
	{
		::pthread_mutex_unlock(&mutex);
	}
};

/**
* @brief class Mutex define 
*/
class Mutex : public IMutex
{
public:
	/**
	* @brief Initializes a mutex class, with InitializeCriticalSection / pthread_mutex_init
	*/
	Mutex();

	/**
	* @brief Deletes the associated critical section / mutex
	*/
	~Mutex();

	/**
	* @brief Acquires this mutex. If it cannot be acquired immediately, it will block.
	*/
	void Lock()
	{
		::pthread_mutex_lock(&mutex);
	}

	/**
	* @brief Releases this mutex. No error checking performed
	*/
	void UnLock()
	{
		::pthread_mutex_unlock(&mutex);
	}

	/** 
	 * @brief Attempts to acquire this mutex. If it cannot be acquired (held by another thread) it will return false.
	 * @return false if cannot be acquired, true if it was acquired.
	 */
	bool TryLock()
	{
		return (::pthread_mutex_trylock(&mutex) == 0);
	}

protected:
	static bool attr_initalized;
	static pthread_mutexattr_t attr;
};


class Mutex_scope_lock : private Noncopyable
{
public:
	Mutex_scope_lock(IMutex &m): mlock(m) { mlock.Lock();}

	~Mutex_scope_lock() { mlock.UnLock(); }

private:
	IMutex &mlock;
};

template <bool locker = true>
class MutexT
{
public:
	void Lock() { _lock.Lock();}

	void UnLock() { _lock.UnLock();}
private:
	Mutex _lock;
};

template<>
class MutexT<false>
{
public:
	void Lock() {}
	void UnLock() {}
};

#endif

