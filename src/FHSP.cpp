#include "MDS.h"
#include "FHSP.h"
#include "XInt32.h"

//////////////////////////////////////////////////////////////////////

CFHSP::CFHSP()
{
	m_dwDate = 0;
	
	m_hHL = 0;
	m_hSG = 0;
	m_hZZ = 0;
	m_hPG = 0;
	m_dwPGJ = 0;

	m_dwUpdateDate = 0;
	m_dwUpdateTime = 0;
}

void CFHSP::Read(CBuffer& buf, short)
{
	buf.Read(&m_dwGoodsID, 4);
	buf.Read(&m_dwDate, 4);
	
	XInt32 xValue;

	buf.Read(&xValue, 4);
	m_hHL = xValue;

	buf.Read(&xValue, 4);
	m_hSG = xValue;

	buf.Read(&xValue, 4);
	m_hZZ = xValue;

	buf.Read(&xValue, 4);
	m_hPG = xValue;

	buf.Read(&m_dwPGJ, 4);

	buf.Read(&m_dwUpdateDate, 4);
	buf.Read(&m_dwUpdateTime, 4);
}

void CFHSP::Write(CBuffer& buf, short)
{
	buf.Write(&m_dwGoodsID, 4);
	buf.Write(&m_dwDate, 4);

	XInt32 xValue;

	xValue = m_hHL;
	buf.Write(&xValue, 4);

	xValue = m_hSG;
	buf.Write(&xValue, 4);

	xValue = m_hZZ;
	buf.Write(&xValue, 4);

	xValue = m_hPG;
	buf.Write(&xValue, 4);

	buf.Write(&m_dwPGJ, 4);

	buf.Write(&m_dwUpdateDate, 4);
	buf.Write(&m_dwUpdateTime, 4);
}

void CFHSP::DoPriceCQ(char cCQ, double& fA, double& fB)
{
	if (cCQ==1 || cCQ==2)
	{
		double fHL = 0;
		if (m_hHL>0)
			fHL = m_hHL/10000000.0f;
		double fSG = 0;
		if (m_hSG>0 || m_hZZ>0)
			fSG = (m_hSG+m_hZZ)/10000000.0f;
		double fPG = 0;
		double fPGJ = 0;
		if (m_hPG>0 && m_dwPGJ>=0)
		{
			fPG = m_hPG/10000000.0f;
			fPGJ = m_dwPGJ/1000.0f;
		}

		if (cCQ==2)
		{
			fA = 1/(1+fSG+fPG);
			fB = (fPG*fPGJ-fHL)/(1+fSG+fPG);
		}
		else
		{
			fA = 1+fSG+fPG;
			fB = fHL-fPG*fPGJ;
		}
	}
}

BOOL operator > (const CFHSP& first, const CFHSP& second)
{
	if (first.m_dwGoodsID>second.m_dwGoodsID)
		return TRUE;
	else if (first.m_dwGoodsID==second.m_dwGoodsID)
	{
		if (first.m_dwDate>second.m_dwDate)
			return TRUE;
	}
	return FALSE;
}

BOOL operator == (const CFHSP& first, const CFHSP& second)
{
	return (first.m_dwGoodsID==second.m_dwGoodsID && first.m_dwDate==second.m_dwDate);
}

BOOL operator < (const CFHSP& first, const CFHSP& second)
{
	if (first.m_dwGoodsID<second.m_dwGoodsID)
		return TRUE;
	else if (first.m_dwGoodsID==second.m_dwGoodsID)
	{
		if (first.m_dwDate<second.m_dwDate)
			return TRUE;
	}
	return FALSE;
}

//////////////////////////////////////////////////////////////////////

CGoodsFHSP::CGoodsFHSP()
{
	m_dwGoodsID = 0;	
}

CGoodsFHSP::CGoodsFHSP(const CGoodsFHSP& src)
{
	*this = src;
}

CGoodsFHSP::~CGoodsFHSP()
{
	m_aFHSP.RemoveAll();
}

const CGoodsFHSP& CGoodsFHSP::operator=(const CGoodsFHSP& src)
{
	m_dwGoodsID = src.m_dwGoodsID;
	m_aFHSP.Copy(src.m_aFHSP);

	return *this;
}

BOOL operator > (const CGoodsFHSP& first, const CGoodsFHSP& second)
{
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

BOOL operator == (const CGoodsFHSP& first, const CGoodsFHSP& second)
{
	return first.m_dwGoodsID==second.m_dwGoodsID;
}

BOOL operator < (const CGoodsFHSP& first, const CGoodsFHSP& second)
{
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

//////////////////////////////////////////////////////////////////////

CFHSPAll::CFHSPAll()
{
}

CFHSPAll::~CFHSPAll()
{
	m_aGoodsFHSP.RemoveAll();
	m_aFHSP.RemoveAll();
}

bool CFHSPAll::Read()
{
	m_wrFHSP.AcquireWriteLock();

	m_aGoodsFHSP.RemoveAll();
	m_aFHSP.RemoveAll();

	CString strFile = theApp.m_strSharePath+"Data/fhspL2.dat";

	CBuffer buffer;
	buffer.m_bSingleRead = true;
	buffer.Initialize(4096, true);

	buffer.FileRead(strFile);
	short sVer;
	int nSize;
	try
	{
		buffer.Read(&sVer, 2);

		buffer.Read(&m_dwDate, 4);
		buffer.Read(&m_dwTime, 4);
		
		buffer.Read(&nSize, 4);

		LogInfo("%s: ver=>%d date=>%d time=>%d size=>%d", strFile.GetData(), sVer, m_dwDate, m_dwTime, nSize);

		CFHSP fhsp;

		for (int i=0; i<nSize; i++)
		{
			fhsp.Read(buffer, sVer);

			AddFHSP(fhsp);
		}
	}
	catch(int)
	{
		LOG_ERR("exception %s: ver=>%d date=>%d time=>%d size=>%d", strFile.GetData(), sVer, m_dwDate, m_dwTime, nSize);
		int nDataFileSwitch = theApp.GetProfileInt("System", "DataFileSwitch", 0);
		if (0==nDataFileSwitch)
		{
			m_wrFHSP.ReleaseWriteLock();
			return false;
		}
	}
	
	m_wrFHSP.ReleaseWriteLock();
	return true;
}

void CFHSPAll::AddFHSP(const CFHSP& fhsp)
{
	CGoodsFHSP gfFind;
	gfFind.m_dwGoodsID = fhsp.m_dwGoodsID;
	int nFind = m_aGoodsFHSP.Find(gfFind);
	if (nFind < 0)
		nFind = m_aGoodsFHSP.Add(gfFind);

	m_aGoodsFHSP[nFind].m_aFHSP.Add(fhsp);
	m_aFHSP.Add(fhsp);
}

void CFHSPAll::Get(DWORD dwGoodsID, CSortArray<CFHSP>& aFHSP)
{
	m_wrFHSP.AcquireReadLock();

	CGoodsFHSP gfFind;
	gfFind.m_dwGoodsID = dwGoodsID;
	int nFind = m_aGoodsFHSP.Find(gfFind);
	if (nFind>=0)
	{
		aFHSP.Copy(m_aGoodsFHSP[nFind].m_aFHSP);
	}

	m_wrFHSP.ReleaseReadLock();
}

void CFHSPAll::Get(DWORD dwDate, DWORD dwTime, CSortArray<CFHSP>& aFHSP)
{
	m_wrFHSP.AcquireReadLock();

	int nSize = int(m_aFHSP.GetSize());
	for (int i=0; i<nSize; i++)
	{
		CFHSP& fhsp = m_aFHSP[i];
		if (fhsp.m_dwUpdateDate>dwDate || (fhsp.m_dwUpdateDate==dwDate && fhsp.m_dwUpdateTime>dwTime))
		{
			aFHSP.Add(fhsp);
		}
	}

	m_wrFHSP.ReleaseReadLock();
}



