#include "MDS.h"
#include "SockThreads.h"
#include "Sockets.h"

/////////////////////////////////////////////////

CL2DMThread::CL2DMThread() : CMyClientThread()
{
	m_pSocket = new CL2DMSocket;
	m_bWantRestart = FALSE;
}

void CL2DMThread::ForThreadCheck()
{
    CMDSApp::g_tL2DMTime = CMDSApp::g_dwSysHMS;
    CMDSApp::g_tsL2DMTime = time(NULL);
}


bool CL2DMThread::ReStart()
{
	End();
	m_dwFailConnet = 101;
	return CMyThread::Start();
}

