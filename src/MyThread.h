#ifndef MYTHREAD_H
#define MYTHREAD_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <pthread.h>
#include <semaphore.h>

#include "Condition.h"
//////////////////////////////////////////////////////////////////////

enum SyncType
{
    SYNC_CQ = 0,
    SYNC_KM = 1,
    SYNC_KL = 2
};

class CRunnable
{
public:
	virtual ~CRunnable(){}
	virtual void Run() = 0;
	virtual void init(){}
	virtual void destroy(){}
};

class CMyThread : public CRunnable, private Noncopyable
{
public:
	virtual const char* Name() { return "CMyThread"; }

	CMyThread(bool joinbale = true);
	CMyThread(CRunnable* runnable, bool joinbale = true);

	virtual ~CMyThread();

	virtual bool Start(double delaySec = 1.0);
	virtual void End();

	const bool isAlive() const;
	const bool isJoinable() const;
	void SetJoinable(bool joinable);

	pthread_t getMyThreadId();

	static pthread_t getCurrentThreadId();

	static void atomicAdd(volatile int *val, int x);

	static void atomicInc(volatile int *val);

	static void atomicDec(volatile int *val);

	static void sleep(const int sec);

	static void msleep(int millis);

	static void usleep(const int usec);

protected:
	virtual void Run();
	static void* s_ThreadProcFunc(void* pParam);

protected:
	pthread_t m_nThreadID;
	sem_t m_semExit;
	pthread_attr_t m_attr; 
	int m_delaySec;
	int m_delayNSec;
	
	CRunnable* m_runnable;

	bool m_bJoinable;

	volatile bool m_bAlive;
	FastMutex m_mutex;
	Condition m_cond;
};

//////////////////////////////////////////////////////////////////////

class CMyTimerThread : public CMyThread
{
public:
	virtual const char* Name() { return "CMyTimerThread"; }
	

protected:
	virtual void Run();
};

class CMyInitDayThread : public CMyThread
{
public:
	CMyInitDayThread(bool bJoinable);
	~CMyInitDayThread();
	virtual const char* Name() { return "CMyInitDayThread"; }

public:
	char m_cType;	// 1==大陆 2==HK 3==ALL 20110629

protected:
	virtual void Run();
};

//////////////////////////////////////////////////////////////////////

class CMySaveDayThread : public CMyThread
{
public:
	CMySaveDayThread(bool bJoinable);
	~CMySaveDayThread();
	virtual const char* Name() { return "CMySaveDayThread"; }

public:
	bool m_bHK;

protected:
	virtual void Run();
};

//////////////////////////////////////////////////////////////////////

class CMySaveGoodsThread : public CMyThread
{
public:
	virtual const char* Name() { return "CMySaveGoodsThread"; }

protected:
	virtual void Run();
};

//////////////////////////////////////////////////////////////////////////
class CMySyncRedisThread : public CMyThread
{
public:
    CMySyncRedisThread(SyncType stype) { m_SyncType = stype; }
	virtual const char* Name() { return "CMySyncRedisThread "; }

protected:
	virtual void Run();
public:
    SyncType m_SyncType;
};

//////////////////////////////////////////////////////////////////////////
class CMyDeleteDataThread : public CMyThread
{
public:
	virtual const char* Name() { return "CMyDeleteDataThread"; }

protected:
	virtual void Run();
};

//////////////////////////////////////////////////////////////////////////
class CMyLoadDataThread : public CMyThread
{
public:
	virtual const char* Name() { return "CMyLoadDataThread"; }

protected:
	virtual void Run();
};


//////////////////////////////////////////////////////////////////////////
class CMyCurBKThread : public CMyThread
{
public:
	virtual const char* Name() { return "CMyCurBKThread"; }

protected:
	virtual void Run();
};

class CLuaThread : public CMyThread
{
public:
	virtual const char* Name() { return "CLuaThread"; }

protected:
	virtual void Run();
};

#endif



