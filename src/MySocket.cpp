#include <sys/types.h>
#include <sys/socket.h>

#include "MDS.h"
#include "MySocket.h"
#include "SockData.h"
#include <sys/syscall.h>

CMySocket::CMySocket()
{
	m_strAddress1 = "";
	m_wPort1 = 0;
	m_wPort2 = 0;
	m_wCurPort = 0;

	m_socket = -1;

	m_dwRecvPack = 0;
	m_dwRecvByte = 0;
	m_dwSendPack = 0;
	m_dwSendByte = 0;
	m_dwRecvRecordtime_5m =0;
	m_dwSendRecordtime_5m =0;

	m_wStatus = CMySocket::WantNone;

	m_dwFirstTime = m_dwActiveTime = 0;

	m_bConnected = FALSE;
	m_bLogined = FALSE;

	m_bufRead.m_bSingleRead = true;
	m_bufRead.Initialize(PAGE_SIZE, true);

	m_bufWrite.m_bSingleRead = true;
	m_bufWrite.Initialize(PAGE_SIZE, true);

	m_cHttp = 0;

	m_dwRecordtime_10s	= 0;
	m_dwRecvDataSize_in10s= 0;
	m_dwRecordtime_5s		= 0;
	m_dwRecvDataSize_in5s	= 0;
}

CMySocket::~CMySocket()
{
		//DeleteData();
}

void CMySocket::SetTime()
{
	m_dwActiveTime = CMDSApp::g_dwSysSecond;
	if (m_dwFirstTime==0)
		m_dwFirstTime = m_dwActiveTime;
}

void CMySocket::DeleteData()
{
	for(int i = m_aData.GetSize()-1; i >= 0; i--)
	{
		CSockData* pData = (CSockData*)m_aData[i];
		if(pData)
			delete pData;
	}

	m_aData.RemoveAll();
}

int CMySocket::DoRecv(CSockThreadData* pThreadData)
{
	int nRet = 0;

	while (TRUE)
	{
		int nRcvLen = ::recv(m_socket, pThreadData->m_pcRcvBuf, RCV_BUFF_LEN, MSG_NOSIGNAL);
		if (nRcvLen == 0)
		{
			m_wStatus = CMySocket::WantDelete;
			LOG_DBG("recv 0 m_wStatus WantDelete");
			nRet = -1;
			break;
		}
		else if (nRcvLen == -1)
		{
			if(errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
			{
				m_wStatus = CMySocket::WantDelete;
				LOG_ERR("recv error from %d errno=%d errno msg:%s", m_socket, errno, strerror(errno));
				nRet = -1;
			}
			break;
		}
		else
		{
			//接收数据后更新活动时间
			SetTime();
			m_dwRecvByte += nRcvLen;
			CMDSApp::g_llSysRecvByte +=nRcvLen;
			if (CMDSApp::g_dwSysSecond > m_dwRecordtime_10s &&  CMDSApp::g_dwSysSecond - m_dwRecordtime_10s > 10)
			{
				if (m_dwRecordtime_10s == 0)
				{
					m_dwRecordtime_10s = CMDSApp::g_dwSysSecond;
				}
				else
				{
					m_dwRecordtime_10s = m_dwRecordtime_5s;
				}
				m_dwRecvDataSize_in10s = m_dwRecvDataSize_in5s;
			}

			if (CMDSApp::g_dwSysSecond > m_dwRecordtime_5s && CMDSApp::g_dwSysSecond - m_dwRecordtime_5s > 5)
			{
				m_dwRecvDataSize_in5s = 0;
				m_dwRecordtime_5s = CMDSApp::g_dwSysSecond;
			}

			m_dwRecvDataSize_in10s += nRcvLen;
			m_dwRecvDataSize_in5s += nRcvLen;

			m_bufRead.Write(pThreadData->m_pcRcvBuf, nRcvLen);

			nRet = Decode(pThreadData->m_bufTemp);
		}
	}

	return nRet;
}

int CMySocket::DoSend(CSockThreadData* pThreadData)
{
	if (m_wStatus!=WantSend && m_wStatus!=WantAll)
		return 0;

	MakeSend(pThreadData->m_bufTemp);

	int nRet = 0;

	int nSndDataLen = m_bufWrite.GetBufferLen();
	char* pcSndBuf = (char*)m_bufWrite.GetBuffer();

	if (nSndDataLen>0)
	{
		int nSndBytes = ::send(m_socket, pcSndBuf, nSndDataLen, MSG_NOSIGNAL);
		if (nSndBytes == -1)
		{
			if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
			{
				m_wStatus = CMySocket::WantDelete;
				LOG_ERR("send error from %d errno=%d errno msg:%s", m_socket, errno, strerror(errno));
				nRet = -1;
			}
		}
		else
		{
			m_bufWrite.Delete(nSndBytes);

			//发送数据后更新活动时间
			SetTime();
			CMDSApp::g_llSysSendByte += nSndBytes;
			m_dwSendByte += nSndBytes;
		}
	}

	return nRet;
}

void CMySocket::AfterConnect()
{
	DeleteData();
	m_wStatus = CMySocket::WantAll;
	
	m_bConnected = TRUE;

	//连接成功建立后更新活动时间
	SetTime();

	m_bufRead.ClearBuffer();
	m_bufWrite.ClearBuffer();
}

void CMySocket::ReadPacket(CDataHead& head, CBuffer& bufTemp)
{
}

void CMySocket::ReadPacket(CDataHeadMobile& head, CBuffer& bufTemp)
{
}

void CMySocket::MakeSend(CBuffer& bufTemp)
{
	if (m_bufWrite.GetBufferLen()==0)
	{
		int nData = m_aData.GetSize();
		for (WORD w=0; w<3; w++)
		{
			for (int i=0; i<nData; i++)
			{
				CSockData* pData = (CSockData*)m_aData[i];
				if (pData && pData->m_wPriority==w)
				{
					if (pData->m_wStatus==CSockData::WantSend || pData->m_wStatus==CSockData::WantAll)
					{
						bufTemp.ClearBuffer();
						pData->Send(bufTemp);

						if (EncodeDataPack(pData, bufTemp) == 0)
						{
							Encode(bufTemp);

							if (m_bufWrite.GetBufferLen()>_SendBuffer_Size_)
								break;
						}
					}
				}
			}
		}
	}
}

void CMySocket::Close()
{
	m_bConnected = FALSE;
	m_bLogined = FALSE;

	m_dwRecvPack = 0;
	m_dwRecvByte = 0;
	m_dwSendPack = 0;
	m_dwSendByte = 0;
	m_dwRecvRecordtime_5m = 0;
	m_dwSendRecordtime_5m = 0;
	m_dwRecordtime_10s =0;
	m_dwRecordtime_5s =0;

	m_dwActiveTime = 0;
	m_dwFirstTime = 0;

	::shutdown(m_socket, SHUT_RDWR);
	::close(m_socket);
}

void CMySocket::CheckData()
{
	int nData = m_aData.GetSize();

	//add by liuchengzhu 20091211
	DWORD now;
	#ifdef _WIN32
		now = GetTickCount();
	#else
		time_t tm = time(NULL);
		struct tm tm_s;
		struct tm* p = localtime_r(&tm, &tm_s);
		now = p->tm_hour*3600+p->tm_min*60+p->tm_sec;
	#endif
	//end add by liuchengzhu 20091211

	for (int i=nData-1; i>=0; i--)
	{
		CSockData* pData = (CSockData*)m_aData[i];
		if (pData)
		{
			if (pData->m_wStatus==CSockData::WantDelete)
			{			
				m_aData.RemoveAt(i);
				delete pData;

				continue;
			}

			
			//add by liuchengzhu 20091211
			#define g_time_out 1500

			if(pData->m_wStatus == CSockData::WantRecv && pData->m_time_start > 0)
			{
				if(pData->m_time_start + g_time_out < now || pData->m_time_start > now)
				{
					try {
						pData->OnTime();
					}
					catch (...)
					{
						LOG_ERR("Data OnTime function throw exception! ");
					}
					pData->m_time_start = 0;
				}
			}
			//end add by liuchengzhu 20091211
		}
		else
			m_aData.RemoveAt(i);
	}
}

WORD CMySocket::GetDataNum()
{
	return m_aData.GetSize();
}

void CMySocket::AddData(CSockData* pData)
{
	if (pData)
	{
		pData->m_pSocket = this;
		m_aData.Add(pData);
	}
	else
		m_wStatus = CMySocket::WantDelete;
}

void CMySocket::InsertData(CSockData* pData)
{
	if (pData)
	{
		pData->m_pSocket = this;
		m_aData.InsertAt(0, pData);
	}
	else
		m_wStatus = CMySocket::WantDelete;
}

void CMySocket::DeleteData(DWORD dwDataID)
{
	int nPos = 0;
	int nData = m_aData.GetSize();
	for (int i=0; i<nData; i++)
	{
		CSockData* pData = (CSockData*)m_aData[nPos];
		if (pData)
		{
			if (pData->m_head.m_dwDataID==dwDataID)
			{
				m_aData.RemoveAt(nPos);
				delete pData;
			}
			else
				nPos++;
		}
		else
			m_aData.RemoveAt(nPos);
	}
}

BOOL CMySocket::IsIdle()
{
	DWORD tSysTime = CMDSApp::g_dwSysSecond - CMDSApp::m_dwConIdle;
	return m_dwActiveTime < tSysTime;
}

int CMySocket::Decode(CBuffer& bufTemp)
{
	int nRet = 0;

	if (m_cHttp==0)
	{
		UINT uBufLen;
		while ((uBufLen = m_bufRead.GetBufferLen()) >= _DataHeadLength_)
		{
			char* pcRecv = (char*)m_bufRead.GetBuffer();

			CDataHead head;
			head.Recv(pcRecv, bufTemp.m_bNetworkOrder);

			if (head.m_wHeadID != 0x9988 && head.m_wHeadID & 0x1122 != 0x1122)
			{
				LOG_ERR("非法数据包 headID=%d ! ", head.m_wHeadID);
				m_wStatus = CMySocket::WantDelete;
				nRet = -1;
				break;
			}

			if (uBufLen>=_DataHeadLength_+head.m_dwDataLength)
			{
				m_bufRead.Delete(_DataHeadLength_);
				
				bufTemp.ClearBuffer();
				bufTemp.Write(m_bufRead.GetBuffer(), head.m_dwDataLength);

				m_bufRead.Delete(head.m_dwDataLength);

				DecodePack(head, bufTemp);
			}
			else
				break;
		}
	}
	else if (m_cHttp==-1)
	{
		UINT uBufLen;
		while ((uBufLen = m_bufRead.GetBufferLen()) >= _DataHeadMobileLength_)
		{
			char* pcRecv = (char*)m_bufRead.GetBuffer();

			CDataHeadMobile headFee;
			headFee.Recv(pcRecv, bufTemp.m_bNetworkOrder);

			if (headFee.m_wHeadID & 0x1122 != 0x1122)
			{
				LOG_ERR("非法数据包 headID=%d! ", headFee.m_wHeadID);
				m_wStatus = CMySocket::WantDelete;
				nRet = -1;
				break;
			}

			if (uBufLen>=_DataHeadMobileLength_+headFee.m_dwDataLength)
			{
				m_bufRead.Delete(_DataHeadMobileLength_);
				
				bufTemp.ClearBuffer();
				bufTemp.Write(m_bufRead.GetBuffer(), headFee.m_dwDataLength);

				m_bufRead.Delete(headFee.m_dwDataLength);

				ReadPacket(headFee, bufTemp);
				//DecodePack(headFee, bufTemp);
			}
			else
				break;
		}
	}

	return nRet;
}

void CMySocket::Encode(CBuffer& bufTemp)
{
	DWORD dwSend = bufTemp.GetBufferLen();
	if (dwSend>0)
	{
		m_dwSendPack++;
		CMDSApp::g_dwSendPack++;
		if (m_cHttp==0 || m_cHttp==-1)
			m_bufWrite.Write(bufTemp.GetBuffer(), dwSend);

		//发送的包数记录在ini文件byzhaolin 2014-07-03
		if (CMDSApp::g_dwSysSecond<30)
		{
			//每天零点清零,重新计数
			CMDSApp::g_dwSendPack =0;
			CMDSApp::g_llSysSendByte =0;
			CMDSApp::g_llSysRecvByte =0;
			CMDSApp::g_dwRecvPack =0;
		}
		if (CMDSApp::g_dwSysSecond > m_dwSendRecordtime_5m &&  CMDSApp::g_dwSysSecond - m_dwSendRecordtime_5m > 302)
		{
			time_t tmp = time(NULL);
			struct tm tDate;
			localtime_r(&tmp,&tDate);
			CString strTime;
			strTime.Format("%04d%02d%02d%02d%02d%02d",(tDate.tm_year+1900), (tDate.tm_mon+1), tDate.tm_mday,
				tDate.tm_hour, tDate.tm_min, tDate.tm_sec);
			CString strSendByte;
			strSendByte.Format("%lld",CMDSApp::g_llSysSendByte);

			m_dwSendRecordtime_5m = CMDSApp::g_dwSysSecond;
			theApp.SetProfileInt("System", "SendPackCount", CMDSApp::g_dwSendPack);
			theApp.SetProfileString("System", "SendPackSize",  strSendByte.GetData());
			theApp.SetProfileString("System", "SendPackTime",  strTime.GetData());
			//LogInfo("发送的包数:%ld 发送的总字节数:%s 发送时间:%s %s %d ",CMDSApp::g_dwSendPack,strSendByte.GetData(),strTime.GetData(),__FUNCTION__,__LINE__);
		}
	}
}

void CMySocket::DecodePack(CDataHead& head, CBuffer& bufTemp)
{
	if (DecodeDataPack(head, bufTemp) != 0)
	{
		LOG_ERR("非法数据包 headID=%d ! ", head.m_wHeadID);
		m_wStatus = CMySocket::WantDelete;
		return;
	}

	try
	{
		ReadPacket(head, bufTemp);
	}
	catch(int n)
	{
		if ((n==DATA_LACK || n==DATA_ERROR)==false)
		{
			LOG_ERR("DecodePack error! ");
		}
	}

	CMDSApp::g_dwRecvPack++;
	m_dwRecvPack++;

	//收到的包数记录在ini文件byzhaolin 2014-06-17
	
	if (CMDSApp::g_dwSysSecond<40)
	{
		//每天零点清零,重新计数
		CMDSApp::g_dwSendPack =0;
		CMDSApp::g_llSysSendByte =0;
		CMDSApp::g_llSysRecvByte =0;
		CMDSApp::g_dwRecvPack =0;
	}
	if (CMDSApp::g_dwSysSecond > m_dwRecvRecordtime_5m &&  CMDSApp::g_dwSysSecond - m_dwRecvRecordtime_5m > 300)
	{
		time_t tmp = time(NULL);
		struct tm tDate;
		localtime_r(&tmp,&tDate);
		CString strTime;
		strTime.Format("%04d%02d%02d%02d%02d%02d",(tDate.tm_year+1900), (tDate.tm_mon+1), tDate.tm_mday,
			tDate.tm_hour, tDate.tm_min, tDate.tm_sec);
		CString strRecvByte;
		strRecvByte.Format("%lld",CMDSApp::g_llSysRecvByte);

		m_dwRecvRecordtime_5m = CMDSApp::g_dwSysSecond;
		theApp.SetProfileInt("System", "RecvPackCount", CMDSApp::g_dwRecvPack);
		theApp.SetProfileString("System", "RecvPackSize",  strRecvByte.GetData());
		theApp.SetProfileString("System", "RecvPackTime",  strTime.GetData());
		//LogInfo("接收的包数:%ld 接收的总字节数:%s 接收时间:%s %s %d ",CMDSApp::g_dwRecvPack,strRecvByte.GetData(),strTime.GetData(),__FUNCTION__,__LINE__);
	}
}

int CMySocket::DecodeDataPack(CDataHead& head, CBuffer& bufPack)
{
	return 0;
}


int CMySocket::EncodeDataPack(CSockData* pSockData, CBuffer& bufPack)
{
	return 0;
}



