#ifndef CALCCPX_H
#define CALCCPX_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "Data.h"

class CCalcCPX
{
public:
	CCalcCPX();
	~CCalcCPX();

	void Alloc();
	void Free();
	void Change();

	void CalcCPX(DWORD dwGoodsID, double fZT, WORD wDataType, WORD wPeriod, CFDayMobile* pFDay, int nDay);
	CString PredictCPX(int nPos);

	WORD m_wDataType;
	WORD m_wPeriod;

	CFDayMobile* m_pDay;
	int m_nDay;

	int m_nFirst;
	int m_nLast;

	double* m_pfSAR;
	char* m_pcSARSign;
	double* m_pfAve5;
	double* m_pfAve10;
	double* m_pfAve20;
	double* m_pfAveV5;
	double* m_pfAveV20;
	short* m_psAVX;
	char* m_pcBaoZhang;

	short m_sFlag;
	double m_fXS;

	double* m_pfSaveAve;

	CFDayMobile m_dayBuy;
	BOOL m_bDaZhang;

	DWORD m_dwGoodsID;

	double m_fZT;

	double m_fCPXBuyP;
	double m_fCPXBuyV;
	double m_fCPXSellP;

private:
	void CalcCPX(int nFirst, int nLast);
	void CalcSAR(int nFirst, int nLast);
	void MAClose(double* pfValue, int nAve, int nFirst, int nLast);
	void MAVolume(double* pfValue, int nAve, int nFirst, int nLast);
};

#endif

