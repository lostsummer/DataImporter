#ifndef __STRING__H__
#define __STRING__H__

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <alloca.h> 
#include <stdarg.h> 
#include <stdio.h>
#include <string.h>

#include "SysGlobal.h"
#include "Array.h"

#define _tcslen		strlen
#define _tcscpy		strcpy
#define _tcscmp		strcmp
#define _tcsncmp	strncmp
#define _tcscat		strcat
#define _tcschr		strchr
#define _tcsstr		strstr
#define _stprintf	sprintf
#define _vstprintf	vsprintf
#define _alloca		alloca

#define _ttoi		atoi
#define _istdigit	isdigit

class CString  
{
public:
	CString();
	CString(const CString&);
	CString(LPCWSTR lpsz);
	~CString();

	const CString& operator=(const CString&);
	const CString& operator=(LPCTSTR);
	const CString& operator=(LPTSTR);
	operator LPCTSTR() const;
	const CString& operator+=(LPCTSTR);
	const CString& operator+=(LPTSTR);
	const CString& operator+=(const CString&);
	const CString& operator+=(const TCHAR);
	TCHAR operator[](int) const;

	friend CString operator+(const CString& string1, const CString& string2);
	friend CString operator+(const CString& string, LPCTSTR lpsz);
	friend CString operator+(const CString& string, LPTSTR lpsz);
	friend CString operator+(LPCTSTR lpsz, const CString& string);
	friend CString operator+(LPTSTR lpsz, const CString& string);

	int Compare(LPCTSTR) const;

	int GetLength() const;
	TCHAR* GetData() const;
	BOOL IsEmpty() const;
	void Empty();

	// find character starting at left, -1 if not found
	int Find(TCHAR ch) const;
	// find character starting at zero-based index and going right
	int Find(TCHAR ch, int nStart) const;
	// find first instance of substring
	int Find(LPCTSTR lpszSub) const;
	// find first instance of substring starting at zero-based index
	int Find(LPCTSTR lpszSub, int nStart) const;

	void Format(LPCTSTR lpszFormat, ...);
	void Format(LPTSTR lpszFormat, ...);

	TCHAR* AllocBuffer(int);

	//By Steven on 2013-04-09  ->
	CString Left( size_t );
	CString Right( size_t );
	CString Mid( unsigned int, size_t = 0 );

	void TrimLeft( LPCTSTR pSpace = "\t\n\r " );
	void TrimRight( LPCTSTR pSpace = "\t\n\r " );
	void Trim( LPCTSTR pSpace = "\t\n\r " );
	//By Steven on 2013-04-09  <-
	//By Steven on 2013-04-15 ->
	CString SpanExcluding(const char * pSubstr);
	const CString & Replace( const char orChar, const char newChar );

public:
	void AllocCopy(int);
	void ConcatCopy(int nSrc1Len, LPCTSTR lpszSrc1Data, int nSrc2Len, LPCTSTR lpszSrc2Data);
	void ConcatInPlace(int, LPCTSTR);
	void FreeData();
	static int SafeStrlen(LPCTSTR lpsz);

public:
	TCHAR* m_pcData;
	int m_nAllocLength;
};

inline CString::CString()
{
	m_pcData = NULL;
	m_nAllocLength = 0;
}

inline CString::CString(const CString& strSrc)
{
	m_pcData = NULL;
	m_nAllocLength = 0;

	*this = strSrc;
}

inline CString::CString(LPCWSTR lpsz)
{
	m_pcData = NULL;
	m_nAllocLength = 0;

	*this = lpsz;
}

inline CString::~CString()
{ FreeData(); }

inline int CString::GetLength() const
{ 
	if (m_pcData==NULL)
		return 0;
	else
		return _tcslen(m_pcData);
}

inline TCHAR* CString::GetData() const
{ return m_pcData; }

inline BOOL CString::IsEmpty() const
{ return GetLength()==0; }

inline void CString::Empty()
{ 
	if (m_pcData!=NULL)
		m_pcData[0] = 0; 
}

inline CString::operator LPCTSTR() const
{ return m_pcData; }

inline TCHAR CString::operator[](int nIndex) const
{
	if (nIndex>=0 && nIndex<GetLength())
		return m_pcData[nIndex];
	else
		return 0;
}

inline int CString::Compare(LPCTSTR lpsz) const
{
	if (lpsz!=NULL && m_pcData!=NULL)
		return _tcscmp(m_pcData, lpsz);
	else
		return 0;
}

inline int CString::SafeStrlen(LPCTSTR lpsz)
	{ return (lpsz == NULL) ? 0 : strlen(lpsz); }

inline BOOL operator==(const CString& s1, const CString& s2)
	{ return s1.Compare(s2) == 0; }
inline BOOL operator==(const CString& s1, LPCTSTR s2)
	{ return s1.Compare(s2) == 0; }
inline BOOL operator==(LPCTSTR s1, const CString& s2)
	{ return s2.Compare(s1) == 0; }
inline BOOL operator!=(const CString& s1, const CString& s2)
	{ return s1.Compare(s2) != 0; }
inline BOOL operator!=(const CString& s1, LPCTSTR s2)
	{ return s1.Compare(s2) != 0; }
inline BOOL operator!=(LPCTSTR s1, const CString& s2)
	{ return s2.Compare(s1) != 0; }
inline BOOL operator<(const CString& s1, const CString& s2)
	{ return s1.Compare(s2) < 0; }
inline BOOL operator<(const CString& s1, LPCTSTR s2)
	{ return s1.Compare(s2) < 0; }
inline BOOL operator<(LPCTSTR s1, const CString& s2)
	{ return s2.Compare(s1) > 0; }
inline BOOL operator>(const CString& s1, const CString& s2)
	{ return s1.Compare(s2) > 0; }
inline BOOL operator>(const CString& s1, LPCTSTR s2)
	{ return s1.Compare(s2) > 0; }
inline BOOL operator>(LPCTSTR s1, const CString& s2)
	{ return s2.Compare(s1) < 0; }
inline BOOL operator<=(const CString& s1, const CString& s2)
	{ return s1.Compare(s2) <= 0; }
inline BOOL operator<=(const CString& s1, LPCTSTR s2)
	{ return s1.Compare(s2) <= 0; }
inline BOOL operator<=(LPCTSTR s1, const CString& s2)
	{ return s2.Compare(s1) >= 0; }
inline BOOL operator>=(const CString& s1, const CString& s2)
	{ return s1.Compare(s2) >= 0; }
inline BOOL operator>=(const CString& s1, LPCTSTR s2)
	{ return s1.Compare(s2) >= 0; }
inline BOOL operator>=(LPCTSTR s1, const CString& s2)
	{ return s2.Compare(s1) <= 0; }

typedef CArray<CString, CString&> CStringArray;


#endif


