#ifndef MDS_H
#define MDS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <map>

#include "SysGlobal.h"
#include "Goods.h"
#include "PtrArray.h"
#include "MyThread.h"
#include "RWLock.h"
#include "SockThreads.h"
#include "String.h"
#include "DataFile.h"
#include "FHSP.h"
#include "GoodsJBM.h"
#include "HisGBAll.h"
#include "MDSConfig.h"
#include "SessionMgr.h"
#include "Group.h"
#include "OpenStockMgr.h"
#ifndef _DEBUG
#include "LockFile.h"
#endif
#include "Alarm.h"
#include "SysAlarm.h"
#include "CalcGroup.h"
#include "MyDefine.h"
#include "AppLogger.h"
#include "MyLogAdapter.h"
#include <fcntl.h>
#include "GoodsLua.h"

SUBVERSION;
BUILDTIME;

// Compress
const int ERR_COMPRESS_PARAM = -1;
const int ERR_COMPRESS_BITBUF = -2;
const int ERR_COMPRESS_SRCDATA = -3;
const int ERR_COMPRESS_ENCODEDATA = -4;
const int ERR_COMPRESS_CHECK = -5;
const int ERR_COMPRESS_DIV = -6;
const int ERR_COMPRESS_NODATA = -100;
// Compress


extern CSessionMgr _mgr;

extern time_t g_transdb_status;

//用户类型标识
const int USER_TYPES = 10;
const int USER_TYPESTAG[] = {3001,3010,3011,3012,3020,3021,3022,3030,3031,3032};

extern DWORD g_dwStartDateGZIC;
extern DWORD g_dwStartDateGZIH;
extern DWORD g_dwStartDateGZQH;
extern DWORD g_dwStartDateTF;
extern DWORD g_dwStartDateT;

extern DWORD g_dwStartDateOptions;

extern BOOL g_bWantSaveStation;

extern BOOL g_bReCreate;

const int G2P_HEAD = 1600*1024;         //1600K
const int G2P_MAX  = G2P_HEAD/32-1;     //51200-1  //第一个32字节， 0-27:标识+版本号 28-31:个数

struct CGoods2Pos
{
    CGoodsCode m_gcGoodsID;
    DWORD m_dwPos;
};

inline BOOL operator > (CGoods2Pos& first, CGoods2Pos& second)
{
    return first.m_gcGoodsID > second.m_gcGoodsID;
}

inline BOOL operator == (CGoods2Pos& first, CGoods2Pos& second)
{
    return first.m_gcGoodsID == second.m_gcGoodsID;
}

inline BOOL operator < (CGoods2Pos& first, CGoods2Pos& second)
{
    return first.m_gcGoodsID < second.m_gcGoodsID;
}

struct CQktCwb
{
    DWORD m_dwTime;
    char  m_cStatus;
    CQktCwb()
    {
        m_dwTime = 0;
        m_cStatus = 0;
    }
};

struct CHisMinDF
{
    DWORD m_dwDate;
    CDataFile_HisMin m_dfHisMin;
};


class CMDSApp
{
public:
    CMDSApp();
    ~CMDSApp();

    BOOL InitInstance();
    void ExitInstance();

    void Run(); 

    void PrintInfo();

    static int _kbhit(); 
    static void _OnSignal(int s); 
    static void _DumpStack();
    static void _Dump2GDB(int s);

//Config Manage
    //获取数值型配置
    int GetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nDefault=0);
    void SetProfileInt(LPCTSTR lpszSection, LPCTSTR lpszEntry, int nVal=0);

    //获取字符串型配置
    CString GetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszVal="");
    void SetProfileString(LPCTSTR lpszSection, LPCTSTR lpszEntry, LPCTSTR lpszVal="");

    //加载配置
    bool Load_Config(bool bReload=false);

    //输出所有设置
    void ListAll(void);


    WORD m_wPortApp;//服务器监听端口
    WORD m_wMaxFDS;
    WORD m_wThreadNum;


//定时器
    CMyTimerThread m_thTimer;
    void OnTimer();

//Redis
    CLuaThread m_thWriteRedis;

//路径
    CString m_ModulePath;
    CString m_strSharePath;
//路径

// DM
    CL2DMThread m_thDM;
// DM


//Goods & Index
    RWLock m_wrGoods;
    CPtrArray m_aGoods;

    RWLock m_wrGoodsAH2HK;
    CPtrArray m_aGoodsAH;
    
    WORD m_wGoods;
    CGoods* m_pSH;
    CGoods* m_pSZ;
    CGoods* m_p300;
    CGoods* m_p016;//上证50
    CGoods* m_p905;//中证500

    RWLock m_wrGoods2Pos;
    CSortArray<CGoods2Pos> m_aGoods2Pos;

    RWLock m_wrvecDapan;

    bool ReadGoods1();
    bool ReadGoods2();
    void Min1ToRedis(CGoods* pGoods, CRedisImporter* pRedisImporter);
    void KLine2Redis(CGoods* pGoods, CRedisImporter* pRedisImporter, WORD wDataType, WORD wPeriod);
    void FlushAllDB();
    void FlushSub(SyncType);
    void WriteGoods();
    void SyncRedis(SyncType type);
    void DeleteGoods();
    int AddGoods(CGoods* pGoods);
    CGoods* GetGoods(char cStation, DWORD dwNameCode);
    CGoods* GetGoods(DWORD dwGoodsID);
    CGoods* GetGoods(const CGoodsCode& gcGoodsID);
    CGoods* CreateGoods(DWORD dwGoodsID, BOOL bLoadDay = TRUE);
    CGoods* CreateGoods(const CGoodsCode& gcGoodsID, BOOL bLoadDay = TRUE);

    void CheckAH2HK();
    void AddGoodsAH(CGoods* pGoods);
    

    void SaveGoodsTxt();
//Goods & Index


//开盘、收盘
    CMyInitDayThread* m_thInitDay;
    CMySaveDayThread* m_thSaveDay;
    CMySaveDayThread* m_thSaveDayHK;

    volatile DWORD m_dwInitDate;
    volatile DWORD m_dwInitDateHK;
    volatile DWORD m_dwSaveDate;
    volatile DWORD m_dwSaveDateHK;

    volatile bool m_bInitingDate;
    volatile bool m_bSaveingDate;
    volatile bool m_bSaveingDateHK;
    
    bool InitDate(DWORD dwDate, char cType);
    bool SaveDay(BOOL bHK, BOOL bAuto);

    volatile DWORD m_dwDateValue;
    volatile DWORD m_dwTimeValue;
    volatile DWORD m_dwDateValueHK;
    volatile DWORD m_dwTimeValueHK;
    //Steven New Begin
    volatile DWORD m_dwDateValueSPQH;
    volatile DWORD m_dwTimeValueSPQH;
    //Steven New End
    volatile DWORD m_dwTime;
    volatile bool m_bGoodsChanged;

    volatile DWORD m_dwTimeSH;
    volatile DWORD m_dwTimeSZ;
    volatile DWORD m_dwTimeSH_bar;
    volatile DWORD m_dwTimeSZ_bar;
    volatile DWORD m_dwTimeSH_minute;
    volatile DWORD m_dwTimeSZ_minute;
//开盘、收盘

//Lua
    CRedisImporter * m_pRedisImporterDB;   //供初始化
    CRedisImporter * m_pRedisImporterCQ;   //处理码表和快照
    CRedisImporter * m_pRedisImporterKM;   //处理1分钟K线
    CRedisImporter * m_pRedisImporterKL;   //处理30分钟以上跨度K线

//历史数据
    //////////////////////////////////////////////////////////////////////////
    // 动态板块新实现 [11/7/2012 frantqian]
    std::map<string, int> m_DynamicGroupUpdateTimeMap;  //每个DAT文件读取时间，防止重复读取
    int LoadDynamicGroupTxt();
    int UpdateDynamicGroupTxt();
    int UpdateGroupData();//检查GROUP/DATA文件夹是否有更新
    int WriteNewDat2DayFile(void* pDayDF);      //把新的*.DAT文件内容写入到DAY.DAT文件中


    // 动态板块相关.dat文件 [11/1/2012 frantqian]
//  CDataFile_Day m_dfDynamicBKDay[DYNAMIC_GROUP_MAX];
//  int m_DynamicBKDayNum;  //动态数据已经使用的数量
//  std::vector<std::string> m_vecDatName;  //保存已经有CDataFile_Day的文件名字，防止重复加载
//  std::map<DWORD, CDataFile_Day*> m_DynamicGroupDFMap;//动态板块中的股票对应的CDataFile_Day
//  int InitDynamicGroup(); //程序初始化时调用
//  int UpdateDynamicGroup();   //每天开盘调用
//  int AnalyzeDFGroupID(CDataFile_Day* pDayDF);    //更新m_DynamicGroupDFMap  [11/1/2012 frantqian]
//  CDataFile_Day*  GetGroupDayFile(const DWORD& goodsID); //返回指定ID板块对应的CDataFile_Day指针

    //////////////////////////////////////////////////////////////////////////
    CDataFile_Day m_dfDay;
    CDataFile_Day m_dfMin1;
    CDataFile_Day m_dfMin5;
    CDataFile_Day m_dfMin30;

    CDataFile_Day m_dfDay_HK;
    CDataFile_Day m_dfMin1_HK;
    CDataFile_Day m_dfMin5_HK;
    CDataFile_Day m_dfMin30_HK;

    CDataFile_Day m_dfDay_SPQH;
    CDataFile_Day m_dfMin1_SPQH;
    CDataFile_Day m_dfMin5_SPQH;
    CDataFile_Day m_dfMin30_SPQH;

    CDataFile_Day m_dfDay_WI;
    CDataFile_Day m_dfMin1_WI;
    CDataFile_Day m_dfMin5_WI;
    CDataFile_Day m_dfMin30_WI;

    CDataFile_Day m_dfTodayMin1;

    CDataFile_Bargain m_dfBargain;

    CDataFile_HisBK m_dfHisBK;

    CDataFile_Trade m_pdfTrade[10];
    CDataFile_Bargain m_pdfBargain[10];
    CDataFile_PartOrder m_pdfPartOrder[10];
    CDataFile_Minute m_pdfMinute[10];

    CDataFile_Bargain m_dfBargainHK;
    CDataFile_Minute m_dfMinuteHK;

    CDataFile_Bargain m_dfBargainSPQH;
    CDataFile_Minute m_dfMinuteSPQH;

    CDataFile_Bargain m_dfBargainWI;
    CDataFile_Minute m_dfMinuteWI;

    void ClearDataFile(char cType);
    CDataFile_Bargain* GetBargainFile(char cStation, DWORD dwNameCode);
    CDataFile_Minute* GetMinuteFile(char cStation, DWORD dwNameCode);

    CMyDeleteDataThread m_thDelData;
//历史数据


// 数据出错问题处理 [4/27/2013 frantqian]
    // 出错线程计数器，当连续失败XX次以后服务器重启 [4/27/2013 frantqian]
    DWORD m_dwErrThreadCount;
    // 线程检查函数，检查一些关键线程是否还在正常运行，如果不能正常运行，则再合适的时间重启MDS [4/26/2013 frantqian]
    void CheckThread();

    DWORD dwUpdateTime_TimeThread; // 该值由线程更新，CheckThread监控 [4/26/2013 frantqian]
    std::string sTimeThreadInfo;//调试参数，检查运行到哪里出问题，以后关闭

    //DMcheck 个股数量, 每日开盘时清零  [4/22/2013 frantqian]
    INT32 m_nDMCheckCount;

    // 每日收盘个股数量,用于check时做比对，每日开盘清零 [4/22/2013 frantqian]
    INT32 m_nSaveDayCount;
    INT32 m_nNoSaveDayCount;
    // 30分钟K线缺失或者有错误的股票个数 [4/27/2013 frantqian]
    DWORD m_dwMinKErrCount;
    DWORD m_dwMinKErrValue;


    std::map<DWORD,DWORD> m_mapSaveGoodsID; //记录有哪些股票ID在checkday列表中但最后没有存盘
    std::map<DWORD,DWORD> m_mapNotSaveGoodsID;  //记录有哪些股票ID在checkday列表中但最后没有存盘
    std::map<DWORD,DWORD> m_mapDelDateCount;//记录有删除日线日期的具体删除次数，用于报警
    void CreateCheckAlertInfo(std::string& alertinfo);//根据上面2个map生成告警信息

    CDataFile_Day* GetDataFile(char cStation, short sDataType, short sPeriod);

    CDataFile_HisMin m_pdfHisMin[10];
    CDataFile_HisMin* GetHisMinFile(char cStation, DWORD dwNameCode);
    
    RWLock m_wrHisMinDF;
    CPtrArray m_aHisMinDF;
    CHisMinDF* GetHisMinDF(DWORD dwDate);
    void DeleteHisMinDF();

    // GRID码表信息 [7/2/2013 frantqian]
    RWLock m_wrCode;
    std::map<BYTE, std::map<WORD, BYTE> > m_AllCodemap; //各类码表内容,(值-编号)对
    std::map<BYTE, int> m_HighestLowmap; //各码表低位最大值
    std::map<BYTE, int> m_marketinfo; //市场大小，用来检测是否需要更新市场信息

    //是否需要压缩码表,group类型见CMDS_GridData
    bool IsNeedEncodeGoodsIDVec(BYTE marketgroup);

    bool IsNeedRefreshCodeMap(BYTE marketgroup, std::vector<DWORD>& vecGoods);
    bool RefreshCodeMap(BYTE marketgroup, std::vector<DWORD>& vecGoods);
    std::map<WORD, BYTE> GetCodeMap(BYTE marketgroup, std::vector<DWORD>& vecGoods, int& highestLow );

//FileMapping
    void* m_pFileMapping1;
    void* m_pFileMapping2;
    CMySaveGoodsThread m_thSaveGoods;
//FileMapping

    CHisGBAll m_hisGB;
    CFHSPAll m_fhsp;        //分红送配
    CJBMAll  m_jbm;         //基本面
    CDPTJAll m_dptj;        //道破天机
    DWORD    m_dwDPTJDate;

//Redis
   CMySyncRedisThread * m_thSyncCQ;
   CMySyncRedisThread * m_thSyncKM;
   CMySyncRedisThread * m_thSyncKL;

//拼音
    CStringArray m_aHZ;
    CStringArray m_aDYZ;    //多音字
    void LoadHZ();
    void LoadDYZ();
//拼音

//乾坤图
    std::vector<CQktCwb> m_vQKT;
    void LoadQKT();
    char GetQKTValue(DWORD dwTime,WORD dataType);
//{ time
    static volatile time_t g_tSysTime;

    static volatile DWORD g_dwSysYMD;
    static volatile DWORD g_dwSysHMS;
    static volatile DWORD g_dwSysSecond;
    static volatile DWORD g_dwSysWeek;
    static volatile long long g_llSysRecvByte; //接收的大小
    static volatile long long g_llSysSendByte; //发送的大小
    static volatile DWORD g_dwSendPack;        //发送的包数
    static volatile DWORD g_dwRecvPack;        //接收的包数
    static volatile int g_iSysGnBkRiseTotal; //概念版块上涨个数
    static volatile int g_iSysHyBkRiseTotal; 
    static volatile int g_iSysDqBkRiseTotal; 
    static volatile int g_iSysLzBkRiseTotal; 
    static volatile int g_iSysSzbBkRiseTotal;
    //static volatile bool g_bTestOpenLog;  //监控界面菜单控制是否打开log打印开关
    static volatile int g_currCwValue;
    static volatile int g_currCwIndex;
    static volatile char m_cHorsePowerLast;
    static volatile DWORD g_lastSysYMD;
    static volatile DWORD g_tL2DMTime;
    static volatile time_t g_tsL2DMTime;

    static void ChangeTime();
//}


//{ memo
    CString m_strUpdate;
    CString m_strFeeSMSL1;
    CString m_strFeeSMSL2;
    CString m_strFeeCarL1;
    CString m_strFeeCarL2;
    CString m_strFeePostL1;
    CString m_strFeePostL2;
    CString m_strFeeBankL1;
    CString m_strFeeBankL2;
    CString m_strFeeNet;
    CString m_strMobileLoyaltyRuleL1;
    CString m_strMobileLoyaltyRuleL2;
    CString m_strFeeMS;
    CString m_strYHXY;
    CString m_strRJSM;

    void LoadMemo();
    CString LoadMemo(const CString&);
    const CString& GetRJSM(int index) const;

    RWLock m_wrMainInfo;

    CString m_strMainInfoIdx;
    CString m_strMainInfoCnt;

    int m_lMainInfoTime[USER_TYPES];
    int m_lMainInfoGoodsID[USER_TYPES];
    CString m_strMainInfoTitle[USER_TYPES];
    CString m_strMainInfoText[USER_TYPES];
    int m_nMainInfoOpType[USER_TYPES];
    CString m_strMainInfoOpContent[USER_TYPES];
    int m_nMainInfoLevel[USER_TYPES];


    // 保存配置
    int Save_Config(void);

    DWORD m_dwStartYMD;
    DWORD m_dwStartHMS;
    DWORD m_dwSysStartTime;

    // load热点板块 [11/6/2012 frantqian]
    RWLock m_wrHotGroup;
    std::map<int, std::string> m_mapHotGroup;

    void LoadHotGroup();
    void ClearHotGroup();

//{板块明细处理
    RWLock m_wrGroup;
    std::vector<CGroup*> m_vecGroup;
    

    void LoadGroup(int BKType, const CString& strClass);
    void AddGroup(CGroup*);
    CGroup* GetGroup(const CString&, const CString&);
    void DeleteGroup();
//}

    //板块
    RWLock m_wrCalcGroup;
    CPtrArray m_aCalcGroup;

    void DeleteCalcGroup();
    void AddCalcGroup(CCalcGroup*);
    CCalcGroup* GetCalcGroup(DWORD dwGroupID);
    CCalcGroup* CreateCalcGroup(DWORD dwGroupID);
    //板块

    //板块统计数据
    RWLock m_wrCalcGroupStat;
    CPtrArray m_aCalcGroupStat; 

    void DeleteCalcGroupStat();
    void AddCalcGroupStat(CCalcGroupStat*);
    CCalcGroupStat* CreateCalcGroupStat(DWORD);
    CCalcGroupStat* GetCalcGroupStat(DWORD);
    //板块统计数据

// 短线预警
    CAlarmAll m_Alarm;
// 短线预警

    char m_cHorsePower;
    char m_cHorsePower1;
    char m_cHorsePowerMA;
// 特色预警
    CSysAlarmAll m_SysAlarm;

    CIndSelGoodsAll m_IndGoods;
// 特色预警

    //CheckDay
    DWORD m_dwCheckDate;
    DWORD m_dwCheckSaveDate;
    DWORD m_dwCheckDayDate;
    BOOL  m_bEnableCheckDay;
    CDataFile_DayCheck m_dfDayCheck;
    //CheckDay

//{CurBK
    RWLock m_wrCurBK;
    CSortArray<DWORD> m_aHY;
    CHisBK m_CurBK;
    volatile int m_nCountCurBK;
    CMyCurBKThread m_thCurBK;

    void CalcCurBK();
    void AddHY(DWORD dwID);
//}

//{HisBK
    RWLock m_wrHisBK;
    CSortArray<CHisBK> m_aHisBK;

    CHisBK FindHisBK(DWORD dwDate);
//}

//
    CTradeSession m_pTradeSession[4];
    CTradeSession* GetTradeSession(char cStation, BOOL bIncludeVirtual);
//

    int m_nMaxNumOfHisQSend;
    
    static volatile DWORD m_dwConIdle;

    OpenStockMgr m_OpenStockMgr;

    int m_nFilterNumYear;
    int m_nFilterNumHfYear;
    int m_nFilterNumQuarter;
    int m_nFilterNumMon;
    int m_nFilterNumWeek;
    int m_nFilterNumDay;
    int m_nFilterNumMin1;
    int m_nFilterNumMin5;
    int m_nFilterNumMin15;
    int m_nFilterNumMin30;
    int m_nFilterNumMin60;

    std::string LogPath() const{return m_LogAdapter.GetLogPath();}
    LogConfig* GetLogConfig(void){ return &m_LogAdapter; }
    static void FinishService(){m_brunning=false;}
    
    bool isRunning(){return m_brunning;};
private:
    MyLogAdapter m_LogAdapter;

    static volatile bool m_brunning;
    void CheckExit();

    // <帮助编号，帮助内容>
    typedef std::map<int, CString> MapRJSM;
    typedef MapRJSM::const_iterator CIterMapRJSM;
    MapRJSM m_mapRJSM;  // software's introduction list
    void LoadRJSM_Ex(const CString&); // load software's introduction
    //} memo

    CMDSConfig m_conf;
#ifndef _DEBUG
    CLockFile m_LockFile;
#endif

    CMyLoadDataThread m_thLoadData;
};


//定义用户类型
enum USER_LEVEL
{
    UT_LELVE_1 = 0, //level1
    UT_LELVE_2 = 1, //level2
    UT_LELVE_3 = 2  //pad
};

enum USER_TYPE
{
    UT_OVERTIME_USER = 0,           //系统登录处理超时
    UT_CHARGE_USER = 1,             //手机收费用户
    UT_FREE_USER = 2,               //手机免费用户
    UT_ANONYMITY_USER = 3           //手机匿名用户
};

//定义服务类型
enum SERVICE_TYPE
{
    ST_OVERDUE = 0,                 //过期
    ST_ONSERVICE = 1,               //未到期
    ST_WILL_EXPIRED = 2,            //快到期(差7天到期)
    ST_UNEXPER = 3                  //未体验
};

extern CMDSApp theApp;

///20120319 sjp
/////////////////////////////////////////////////////////////////////////////

struct CExpConfig
{
    CExpConfig()
    {
        Init();
    }

    void Init()
    {
        m_nNo = 0;
        m_strName = "";
        m_nType = 0;
        m_color = -1;
        m_cLevel = 1;
    }

    enum { Other = 0, Price, Percent, Number };

    int m_nNo;          //转换为ID -2000 - -3000
    CString m_strName;  //指标名
    int m_nType;        //0,1:三位小数,2:两位小数,3:无小数
    DWORD m_color;  //-1 按原始定义的颜色显示,其他,按指定颜色显示
    char m_cLevel;      //2=L2,其他=L1
};

inline BOOL operator > (CExpConfig& first, CExpConfig& second)
{
    return first.m_nNo>second.m_nNo;
}

inline BOOL operator == (CExpConfig& first, CExpConfig& second)
{
    return first.m_nNo==second.m_nNo;
}

inline BOOL operator < (CExpConfig& first, CExpConfig& second)
{
    return first.m_nNo<second.m_nNo;
}
///20120319 sjp
/////////////////////////////////////////////////////////////////////////////

inline DWORD g_CheckDiv(char cDiv)
{
    if (cDiv>=0 && cDiv<=6)
    {
        if (cDiv==0)
            return 1;

        DWORD dwDiv = 1;
        for (char c=0; c<cDiv; c++)
            dwDiv *= 10;

        return dwDiv;
    }
    else
        return 0;   // Error
}
//Steven New End

#endif

