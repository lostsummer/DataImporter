#ifndef SOCKETS_THREAD_H
#define SOCKETS_THREAD_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "MyClientThread.h"
#include "Mutex.h"
/////////////////////////////////////////////////

class CL2DMThread : public CMyClientThread
{
public:
	CL2DMThread();
	
	bool ReStart();

    void ForThreadCheck();

	virtual const char* Name() { return "CL2DMThread"; }
	bool m_bWantRestart;

};



//////////////////////////////////////////////////////////////////////////
class CMSThread : public CMyClientThread
{
public:
	CMSThread();
	virtual ~CMSThread();
	virtual const char* Name() { return "CMSThread"; }
};


#endif


