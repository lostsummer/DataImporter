#include <arpa/inet.h>

#include "DataHead.h"
#include "Mutex.h"
#include "MyThread.h"

//////////////////////////////////////////////////////////////////////
volatile int CDataHead::s_DataID = 0;
CDataHead::CDataHead()
{
	ZeroMemory(this, sizeof(CDataHead));
}

void CDataHead::Initial()
{
	ZeroMemory(this, sizeof(CDataHead));
	CMyThread::atomicInc(&s_DataID);

	m_wHeadID = DATAHEAD_ID;//0x9988
	m_dwDataID = s_DataID;
}

DWORD CDataHead::Recv(char* pcData, bool bNetworkOrder)
{
	CopyMemory(this, pcData, _DataHeadLength_);
	if (bNetworkOrder)
	{
		m_wHeadID = ntohs(m_wHeadID);
		m_wDataType = ntohs(m_wDataType);
		m_dwDataID = ntohl(m_dwDataID);
		m_dwDataLength = ntohl(m_dwDataLength);
		m_wChecksum = ntohs(m_wChecksum);
		m_wReserved = ntohs(m_wReserved);
	}
	return _DataHeadLength_;
}

DWORD CDataHead::Send(char* pcData, bool bNetworkOrder)
{
	if (bNetworkOrder)
	{
		CDataHead head;
		head.m_wHeadID = htons(m_wHeadID);
		head.m_wDataType = htons(m_wDataType);
		head.m_dwDataID = htonl(m_dwDataID);
		head.m_dwDataLength = htonl(m_dwDataLength);
		head.m_wChecksum = htons(m_wChecksum);
		head.m_wReserved = htons(m_wReserved);
		CopyMemory(pcData, &head, _DataHeadLength_);
	}
	else
		CopyMemory(pcData, this, _DataHeadLength_);
	return _DataHeadLength_;
}

//////////////////////////////////////////////////////////////////////
volatile int CDataHeadMobile::s_DataID = 0;
CDataHeadMobile::CDataHeadMobile()
{
	ZeroMemory(this, sizeof(CDataHeadMobile));
}

void CDataHeadMobile::Initial()
{
	ZeroMemory(this, sizeof(CDataHeadMobile));
	CMyThread::atomicInc(&s_DataID);

	m_wHeadID = 0x1122;
	dwSessionID = s_DataID;
}

DWORD CDataHeadMobile::Recv(char* pcData, bool bNetworkOrder)
{
	CopyMemory(this, pcData, _DataHeadMobileLength_);
	if (bNetworkOrder)
	{
		m_wHeadID = ntohs(m_wHeadID);
		m_wDataType = ntohs(m_wDataType);
		dwSessionID = ntohl(dwSessionID);
		m_dwDataLength = ntohl(m_dwDataLength);
	}
	return _DataHeadMobileLength_;
}

DWORD CDataHeadMobile::Send(char* pcData, bool bNetworkOrder)
{
	if (bNetworkOrder)
	{
		CDataHeadMobile head;
		head.m_wHeadID = htons(m_wHeadID);
		head.m_wDataType = htons(m_wDataType);
		head.dwSessionID = htonl(dwSessionID);
		head.m_dwDataLength = htonl(m_dwDataLength);
		CopyMemory(pcData, &head, _DataHeadMobileLength_);
	}
	else
		CopyMemory(pcData, this, _DataHeadMobileLength_);
	return _DataHeadMobileLength_;
}

//////////////////////////////////////////////////////////////////////

