// GoodsJBM.cpp: implementation of the CGoodsJBM class.

#include "MDS.h"
#include "GoodsJBM.h"
#include "FPlatDataStock.h"
//////////////////////////////////////////////////////////////////////

CGoodsJBM::CGoodsJBM()
{
	Clear();
}

void CGoodsJBM::Clear()
{
	m_dwGoodsID = 0;
	m_dwDate = 0;

	ZeroMemory(m_pxJBM, JBM_TOTAL*4);

	m_dwUpdateDate = 0;
	m_dwUpdateTime = 0;

	m_dwDateFHSP = 0;
	m_strFHSP.Empty();
	m_fpd.Clear();
}

 const CGoodsJBM& CGoodsJBM::operator=(const CGoodsJBM& src)
 {
 	m_dwGoodsID = src.m_dwGoodsID;
 	m_dwDate = src.m_dwDate;
 
 	CopyMemory(m_pxJBM, src.m_pxJBM, JBM_TOTAL*4);
 
 	m_dwUpdateDate = src.m_dwUpdateDate;
 	m_dwUpdateTime = src.m_dwUpdateTime;
 
 	m_dwDateFHSP = src.m_dwDateFHSP;
 	m_strFHSP = src.m_strFHSP;
 	m_fpd = src.m_fpd;
 	return *this;
 }

void CGoodsJBM::ReadFHSP(CBuffer& buffer, short)
{
	buffer.Read(&m_dwGoodsID, 4);
	buffer.Read(&m_dwDate, 4);

	buffer.Read(m_pxJBM, JBM_TOTAL*4);

	buffer.ReadString(m_strFHSP);
}

void CGoodsJBM::Read(CBuffer& buffer, short)
{
	buffer.Read(&m_dwGoodsID, 4);
	buffer.Read(&m_dwDate, 4);

	buffer.Read(&m_pxJBM, JBM_TOTAL*4);

	buffer.Read(&m_dwUpdateDate, 4);
	buffer.Read(&m_dwUpdateTime, 4);
}

void CGoodsJBM::Write(CBuffer& buffer, short)
{
	buffer.Write(&m_dwGoodsID, 4);
	buffer.Write(&m_dwDate, 4);

	buffer.Write(&m_pxJBM, JBM_TOTAL*4);

	buffer.Write(&m_dwUpdateDate, 4);
	buffer.Write(&m_dwUpdateTime, 4);
}

BOOL operator > (CGoodsJBM& first, CGoodsJBM& second)
{
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

BOOL operator == (CGoodsJBM& first, CGoodsJBM& second)
{
	return first.m_dwGoodsID==second.m_dwGoodsID;
}

BOOL operator < (CGoodsJBM& first, CGoodsJBM& second)
{
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

XInt32 CGoodsJBM::GetValue(short sID)
{
	XInt32 xValue;
	if (sID>=0 && sID<JBM_TOTAL)
		xValue = m_pxJBM[sID];
	else if (sID==J_DATE)
		xValue = m_dwDate;
	else
		xValue = m_dwGoodsID;
	return xValue;
}

void CGoodsJBM::SetValue( XInt32 xValue, short sID )
{
	if (sID>=0 && sID<JBM_TOTAL)
		m_pxJBM[sID] = xValue;
	else if (sID==J_DATE)
		m_dwDate = long(xValue);
}

double CGoodsJBM::GetFValue(short sID)
{
    if (sID>=0 && sID<JBM_TOTAL)
    {
        if (sID==J_MGGJJ || sID==J_MGWFPLR || sID==J_MGSY || sID==J_MGJZC || sID==J_TZMGJZC)
            return double(m_pxJBM[sID]/1000000.0);
        else if (sID==J_GDQYB || sID==J_JZCSYL)
            return double(m_pxJBM[sID]/100.0);
        else if (sID==J_GDRS)
            return double(m_pxJBM[sID]/1.0);
        else
            return double(m_pxJBM[sID]/10000.0);
        return 0;
    }
    else
        return 0;
}

//////////////////////////////////////////////////////////////////////

CJBMAll::CJBMAll()
{
	m_dwNumCols = m_dwStrCols = m_dwDate = m_dwTime = 0;
	m_dwNewDate = m_dwNewTime = 0;
}

CJBMAll::~CJBMAll()
{
	m_aJBM.RemoveAll();
}

bool CJBMAll::Read()
{
	m_wrJBM.AcquireWriteLock();

	m_aJBM.RemoveAll();

	CString strFile = theApp.m_strSharePath+"Data/jbmL2.dat";

	CBuffer buffer;
	buffer.m_bSingleRead = true;
	buffer.Initialize(4096, true);

	buffer.FileRead(strFile);
	short sVer;
	int nSize;
	try
	{
		buffer.Read(&sVer, 2);

		buffer.Read(&m_dwDate, 4);
		buffer.Read(&m_dwTime, 4);

		buffer.Read(&nSize, 4);

		LogInfo("%s: ver=>%d date=>%d time=>%d size=>%d", strFile.GetData(), sVer, m_dwDate, m_dwTime, nSize);

		CGoodsJBM jbm;

		for (int i=0; i<nSize; i++)
		{
			jbm.Read(buffer, sVer);
			m_aJBM.Change(jbm);
		}


		CFPlatData_All fpJbm; //新财务数据
		fpJbm.Read();
	}
	catch(int)
	{
		LOG_ERR("exception %s: ver=>%d date=>%d time=>%d size=>%d", strFile.GetData(), sVer, m_dwDate, m_dwTime, nSize);
		int nDataFileSwitch = theApp.GetProfileInt("System", "DataFileSwitch", 0);
		if (0==nDataFileSwitch)
		{
			m_wrJBM.ReleaseWriteLock();
			return false;
		}
	}

	m_wrJBM.ReleaseWriteLock();

	if (ReadFHSP()==false)
	{
		LOG_ERR("Data/jbm2.dat read exception");
		return false;
	}
	return true;
}

bool CJBMAll::ReadFHSP()
{
	m_wrJBM.AcquireWriteLock();

	CString strFile = theApp.m_strSharePath+"Data/jbm2.dat";

	CBuffer buffer;
	buffer.m_bSingleRead = true;
	buffer.Initialize(4096, true);

	buffer.FileRead(strFile);
	short sVersion;
	int nSize;
	try
	{
		buffer.Read(&sVersion, 2);
		buffer.Read(&nSize, 4);

		LogInfo("%s: ver=>%d size=>%d", strFile.GetData(), sVersion, nSize);

		for (int i=0; i<nSize; i++)
		{
			CGoodsJBM jbmFHSP;
			jbmFHSP.ReadFHSP(buffer, sVersion);

			int nFind = m_aJBM.Find(jbmFHSP);
			if (nFind>=0)
			{
				CGoodsJBM& jbm = m_aJBM[nFind];

				if (jbm.m_dwDateFHSP<=jbmFHSP.m_dwDate)
				{
					jbm.m_dwDateFHSP = jbmFHSP.m_dwDate;
					jbm.m_strFHSP = jbmFHSP.m_strFHSP;
				}
			}
		}
	}
	catch(int)
	{
		LOG_ERR("exception %s: ver=>%d size=>%d", strFile.GetData(), sVersion, nSize);
		int nDataFileSwitch = theApp.GetProfileInt("System", "DataFileSwitch", 0);
		if (0==nDataFileSwitch)
		{
			m_wrJBM.ReleaseWriteLock();
			return false;
		}
	}

	m_wrJBM.ReleaseWriteLock();
	return true;
}

BOOL CJBMAll::Get(DWORD dwGoodsID, CGoodsJBM& jbm)
{
	BOOL bRet = FALSE;

	m_wrJBM.AcquireReadLock();

	CGoodsJBM gjFind;
	gjFind.m_dwGoodsID = dwGoodsID;
	int nFind = m_aJBM.Find(gjFind);
	if (nFind>=0)
	{
		jbm = m_aJBM[nFind];
		bRet = TRUE;
	}
	else
		jbm.Clear();

	m_wrJBM.ReleaseReadLock();

	return bRet;
}

void CJBMAll::Get(DWORD dwDate, DWORD dwTime, CSortArray<CGoodsJBM>& aJBM)
{
	m_wrJBM.AcquireReadLock();

	int nSize = int(m_aJBM.GetSize());
	for (int i=0; i<nSize; i++)
	{
		CGoodsJBM& jbm = m_aJBM[i];
		if (jbm.m_dwUpdateDate>dwDate || (jbm.m_dwUpdateDate==dwDate && jbm.m_dwUpdateTime>dwTime))
		{
			aJBM.Add(jbm);
		}
	}

	m_wrJBM.ReleaseReadLock();
}

//////////////////////////////////////////////////////////////////////

CGoodsDPTJ::CGoodsDPTJ()
{
	Clear();
}

void CGoodsDPTJ::Clear()
{
	ZeroMemory(this, sizeof(CGoodsDPTJ));
}

void CGoodsDPTJ::Read(CBuffer& buffer, short)
{
	buffer.Read(&m_dwGoodsID, 4);

	buffer.Read(&m_sJJJS, 2);
	buffer.Read(&m_hCGSZ, 8);
	buffer.Read(&m_hCGSL, 8);
	buffer.Read(&m_sBL, 2);

	buffer.Read(&m_sJJJS0, 2);
	buffer.Read(&m_hCGSZ0, 8);
	buffer.Read(&m_hCGSL0, 8);
	buffer.Read(&m_sBL0, 2);
}

BOOL operator > (CGoodsDPTJ& first, CGoodsDPTJ& second)
{
	return first.m_dwGoodsID>second.m_dwGoodsID;
}

BOOL operator == (CGoodsDPTJ& first, CGoodsDPTJ& second)
{
	return first.m_dwGoodsID==second.m_dwGoodsID;
}

BOOL operator < (CGoodsDPTJ& first, CGoodsDPTJ& second)
{
	return first.m_dwGoodsID<second.m_dwGoodsID;
}

//////////////////////////////////////////////////////////////////////

CDPTJAll::CDPTJAll()
{
}

CDPTJAll::~CDPTJAll()
{
	m_aDPTJ.RemoveAll();
}

bool CDPTJAll::Read()
{
	m_wrDPTJ.AcquireWriteLock();

	m_aDPTJ.RemoveAll();

	CString strFile = theApp.m_strSharePath+"Data/jjtj.dat";

	CBuffer buffer;
	buffer.m_bSingleRead = true;
	buffer.Initialize(4096, true);

	buffer.FileRead(strFile);
	short sVer;
	int nDate0, nDate1;
	int nSize;
	try
	{	
		buffer.Read(&sVer, 2);

		buffer.Read(&nDate0, 4);
		buffer.Read(&nDate1, 4);
		theApp.m_dwDPTJDate = nDate0;
		
		buffer.Read(&nSize, 4);

		LogInfo("%s: ver=>%d size=>%d nDate0=>%d nDate1=>%d", strFile.GetData(), sVer, nSize, nDate0, nDate1);

		CGoodsDPTJ dptj;

		for (int i=0; i<nSize; i++)
		{
			dptj.Read(buffer, sVer);
			m_aDPTJ.Change(dptj);
		}
	}
	catch(int)
	{
		LOG_ERR("exception %s: ver=>%d size=>%d nDate0=>%d nDate1=>%d", strFile.GetData(), sVer, nSize, nDate0, nDate1);
		int nDataFileSwitch = theApp.GetProfileInt("System", "DataFileSwitch", 0);
		if (0==nDataFileSwitch)
		{
			m_wrDPTJ.ReleaseWriteLock();
			return false;
		}
	}

	m_wrDPTJ.ReleaseWriteLock();
	return true;
}

BOOL CDPTJAll::Get(DWORD dwGoodsID, CGoodsDPTJ& dptj)
{
	BOOL bRet = FALSE;

	m_wrDPTJ.AcquireReadLock();

	CGoodsDPTJ gjFind;
	gjFind.m_dwGoodsID = dwGoodsID;
	int nFind = m_aDPTJ.Find(gjFind);
	if (nFind>=0)
	{
		dptj = m_aDPTJ[nFind];
		bRet = TRUE;
	}
	else
		dptj.Clear();

	m_wrDPTJ.ReleaseReadLock();

	return bRet;
}


