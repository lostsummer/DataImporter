#ifndef HISCOMPRESS_H
#define HISCOMPRESS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include "Goods.h"
#include "Data.h"
#include "BitStream.h"

const WORD MINUTE_BEGIN_TIME = 0;
const int DAY_BEGIN_YEAR = 1980;
const int MINUTE_BEGIN_YEAR = 2000;


class CDataBargain;
class CHisCompress  
{
public:
	static int CompressMinuteVolLevel2(CBitStream& stream, const XInt32& xLTG, CArrayMinute& aMinute, WORD& wPosStart, const XInt32& xRecvAmtDiff = 0);
	static int CompressMinute(CBitStream& stream, BOOL bIndex, CArrayMinute& aMinute, WORD& wPosStart, DWORD PRICE_DIVIDE);
	static int CompressMinute_simple(CBitStream& stream, BOOL bIndex, CArrayMinute& aMinute, WORD& wPosStart, DWORD PRICE_DIVIDE);
	static int ExpandMinute(CBitStream& stream, CGoods* pGoods, CSortArrayMinute& aMinute);

	static int CompressDay(CBitStream& stream, CGoods* pGoods, CSortArrayDayMobile& aDay, WORD wDataType, WORD wPeriod, char cOpenFund, char cIndType, char MaExp, char VMAExp, char IndExp, WORD wVersion);
	static void CompressDayNum(CBitStream& stream, DWORD dwTotal);

	static void CompressDate(CBitStream& stream, DWORD& dwDate);
	static DWORD ExpandDate(CBitStream& stream);

	static int CompressBargain(CBitStream& stream, CDataBargain* pData, DWORD dwRecvNum, WORD wWant);
	static int ExpandBargain(CBitStream& stream, CGoods* pGoods);

	//Steven New Begin
	static int ExpandMinute(CBitStream& stream, CGoods* pGoods, CMinuteSortArray& aMinute, short sVersion);
	static int ExpandMinute6(CBitStream& stream, CGoods* pGoods, CMinuteSortArray& aMinute, short sVersion);
	//Steven New End
	static int CompressHisMin6(CBitStream& stream,  const XInt32& xLTG, CDataHisMin* pData, BOOL bLevel2 = TRUE );
private:
	static DWORD GetDay(DWORD& dwDate);

	static void ExpandOrderCounts(CBitStream& stream, COrderCounts& ocNew, CMinute& min, BOOL bOrder);
};

#endif
