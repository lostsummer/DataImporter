#include "MDS.h"
#include "Sockets.h"
#include "SockData.h"

//////////////////////////////////////////////////////////////////////

void CL2DMSocket::AfterConnect()
{
	CMySocket::AfterConnect();

	CL2DM_LoginData* pData = new CL2DM_LoginData;
	if (pData)
	{
		pData->m_wStatus = CSockData::WantSend;

		pData->m_strName = "sjp";
		pData->m_strPassword = "980302";

		AddData(pData);
	}
	else
	{
		m_strMessage = "内存错.";
		m_wStatus = CMySocket::WantDelete;
	}
}

void CL2DMSocket::ReadPacket(CDataHead& head, CBuffer& bufTemp)
{
	int nData = m_aData.GetSize();
	for (int i=0; i<nData; i++)
	{
		CSockData* pData = (CSockData*)m_aData[i];
		if (pData && pData->m_head.m_dwDataID==head.m_dwDataID)
		{
			try
			{
				pData->Recv(head, bufTemp);
			}
			catch(...)
			{	
				LOG_ERR("解包异常:data type=>%d head id=>%d data len=>%d", head.m_wDataType, head.m_wHeadID, head.m_dwDataLength);
			}
			
			break;
		}
	}
}


BOOL operator > (CCheckItem& first, CCheckItem& second)
{
	return (first.m_cIndex > second.m_cIndex);
}
BOOL operator == (CCheckItem& first, CCheckItem& second)
{
	return (first.m_cIndex == second.m_cIndex);
}
BOOL operator < (CCheckItem& first, CCheckItem& second)
{
	return (first.m_cIndex < second.m_cIndex);
}

CMSSocket::CMSSocket() : CMySocket()
{
	m_dwLastRecvPack = 0;
	m_dwLastRecvPackTime = 0;
	m_nChannel = 1;
	m_nLogOut = 0;
}

CMSSocket::~CMSSocket()
{

}

void CMSSocket::AfterConnect()
{
	CMySocket::AfterConnect();

	/*CMS_LoginData* pData = new CMS_LoginData;
	if (pData)
	{
		pData->m_wStatus = CSockData::WantSend;
		AddData(pData);
	}
	else
	{
		m_wStatus = CMySocket::WantDelete;
	}*/
}

void CMSSocket::ReadPacket(CDataHead& head, CBuffer& bufTemp)
{
	int nData = (int)m_aData.GetSize();
	for (int i=0; i<nData; i++)
	{
		CSockData* pData = (CSockData*)m_aData[i];
		if (pData && pData->m_head.m_dwDataID==head.m_dwDataID)
		{
			pData->Recv(head, bufTemp);
			break;
		}
	}
}

CString g_strCheckItem[] = {"/home","/home","/home",	//CDE盘大小
"BARGAINTIME","TRIDETIME","PACKRITE","MINKCOUNT",	//包速率,分笔,逐笔时间,分钟K根数
"DAYDATACHECK",""};		//内存使用率,日线校验
int g_nCheckValue[] = {500,500,500,60,60,16,60,1,0};
const int g_nCheckItemCount = 8;

DWORD g_dwTimeLastTrade[2] = {0,0};//逐笔
DWORD g_dwTimeLastBargain[2] = {0,0};//分笔


void CMSSocket::ChangeCheckItem( CString &strItem )
{
	return;
	if (strItem=="")
		return;
	//int nPos1 = strItem.Find('#') + 1;
	//int nPos2 = strItem.Find('#',nPos1 + 1);

	//if (nPos1 <= 0 || nPos2 < 0 || nPos1>= nPos2)
	//	return;

	//CString strName = strItem.Mid(nPos1, nPos2 - nPos1);
	//int nVal = atoi((LPCTSTR)strItem+nPos2+1);

	//for (int i = 0;i < g_nCheckItemCount;i++)
	//{
	//	if (strName == g_strCheckItem[i] && i < m_aCheckItem.GetSize())
	//	{
	//		m_aCheckItem[i].m_nMinVal = nVal;
	//		break;
	//	}
	//}
}

//加入检测的初始值
void CMSSocket::InitCheckItem()
{//return;
	m_aCheckItem.SetSize(0, g_nCheckItemCount);
	for (int i = 0; i < g_nCheckItemCount; i++)
	{
		CCheckItem cItem;
		//cItem.m_nMaxVal = 0;
		cItem.m_cIndex = i;
		cItem.m_nMinVal = theApp.GetProfileInt("Monitor", g_strCheckItem[i], g_nCheckValue[i]);	
		m_aCheckItem.Add(cItem);
	}

	m_dwLastRecvPackTime = m_dwLastRecvPack = m_nChannel = 0;
	m_nLogOut =  theApp.GetProfileInt("Monitor", "logout", 0);
}

inline int TimeDiff(DWORD dwTime1,DWORD dwTime2)
{
	DWORD sec1 = dwTime1/10000*3600+dwTime1/100%100*60+dwTime1%100;
	DWORD sec2 = dwTime2/10000*3600+dwTime2/100%100*60+dwTime2%100;
	return int(sec2-sec1);
}

//执行相应项目的检测
//"CDISKSIZE","DDISKSIZE","EDISKSIZE","PACKRITE","BARGAINTIME","TRIDETIME","DAYDATACHECK"
void CMSSocket::DoCheckItem()
{
	//g_nErrCode = 0;
	//g_strErrInfo = "";
	DWORD dwTest[8]={0,0,0,0,0,0,0,0};

	if (m_nChannel != 1)
	{
		m_dwLastRecvPackTime = m_dwLastRecvPack = 0;
		m_nChannel = 1;
		g_dwTimeLastBargain[0] = g_dwTimeLastBargain[1] = 0;
		g_dwTimeLastTrade[0] = g_dwTimeLastTrade[1] = 0;
	}

	char *s_disk[] = {"/home", "/home", "/home"};
	char *s_market[] = {"上海","深圳"};

	int nErrCode = 0;
	CString strErrInfo;
	for (char i = 0;i < m_aCheckItem.GetSize();i++)
	{
		if (i >=3 && i <= 6)
		{
			if ((CMDSApp::g_dwSysHMS<93010)|| (CMDSApp::g_dwSysHMS>113000 && CMDSApp::g_dwSysHMS<130010) || (CMDSApp::g_dwSysHMS>145700))
				continue;
		}

		CCheckItem &cItem = m_aCheckItem[i];
		switch (i)
		{
		case 0:
		case 1:
		case 2:
			{
				/*em_monitor::DiskStatus ds = {0};
				if (CMonitorManager::GetMe().GetDiskStatus(&ds) == 0)
				{
					if (ds.dwDiskFreeSize < cItem.m_nMinVal)
					{
						nErrCode = i+1;
						strErrInfo.Format("硬盘%s空间不足,剩余%dM", s_disk[i], DWORD(ds.dwDiskFreeSize));
					}

					dwTest[i] = ds.dwDiskFreeSize;
				}*/
			}
			break;
		case 3://分笔
			{
				for (int k = 0;k < 2;k++)
				{
					int nDiff = TimeDiff(g_dwTimeLastBargain[k], CMDSApp::g_dwSysHMS);
					dwTest[3+k] = nDiff;

					if (nDiff > cItem.m_nMinVal && nDiff < 9*3600)
					{
						nErrCode = i+1;
						strErrInfo.Format("分笔数据%s延迟%d秒", s_market[k], nDiff);
						//break;
					}
				}
			}
			break;
		case 4://逐笔 
			{
				for (int k = 0;k < 2;k++)
				{
					int nDiff = TimeDiff(g_dwTimeLastTrade[k], CMDSApp::g_dwSysHMS);
					dwTest[5+k] = nDiff;

					if (nDiff > cItem.m_nMinVal && nDiff < 9*3600)
					{
						nErrCode = i+1;
						strErrInfo.Format("逐笔数据%s延迟%d秒", s_market[k], nDiff);
						break;
					}
				}
			}
			break;
		case 5:
			{
				if (CMDSApp::g_dwSysYMD == theApp.m_dwInitDate && theApp.m_thDM.m_pSocket)
				{
					// if ((CMDSApp::g_dwSysHMS > 930 && CMDSApp::g_dwSysHMS < 1130)
					// 		|| (CMDSApp::g_dwSysHMS > 1300 && CMDSApp::g_dwSysHMS < 1500))
					{
						if (m_dwLastRecvPackTime > 0 && int(CMDSApp::g_dwSysSecond - m_dwLastRecvPackTime)>1)
						{
							int nPR = (theApp.m_thDM.m_pSocket->m_dwRecvPack - m_dwLastRecvPack)/int(CMDSApp::g_dwSysSecond - m_dwLastRecvPackTime);
							if (nPR < cItem.m_nMinVal)
							{
								nErrCode = i+1;
								strErrInfo.Format("当前通道%d,收包率低:%d<%d", 1, nPR, cItem.m_nMinVal);
								break;
							}
							dwTest[7] = nPR;
						}
						m_dwLastRecvPack = theApp.m_thDM.m_pSocket->m_dwRecvPack;
						m_dwLastRecvPackTime = CMDSApp::g_dwSysSecond;
					}
				}
			}
			break;
		case 6://逐笔 
			{
				int nMinKCountSH = 0; //当前的上证MIN K线跟数
				int nMinKCountSZ = 0; //当前的深证MIN K线跟数
				int nPosCount = 0; //对应时间相应的K线根数
				CSortArrayMinute aMinute;
				INT ALERTCOUNT = 15;

				if (theApp.m_pSH != NULL && theApp.m_pSZ != NULL)
				{
					theApp.m_pSH->GetMinutes(aMinute, false);
					nMinKCountSH = aMinute.GetSize();
					aMinute.RemoveAll();
					nPosCount += theApp.m_pSH->Min2Pos((theApp.m_dwTimeSH/100)%10000,false) +1 ;

					theApp.m_pSZ->GetMinutes(aMinute, false);
					nMinKCountSZ = aMinute.GetSize();
					aMinute.RemoveAll();
					nPosCount += theApp.m_pSZ->Min2Pos((theApp.m_dwTimeSZ/100)%10000,false) + 1;
				}

				if (nPosCount > nMinKCountSH + nMinKCountSZ+ ALERTCOUNT)
				{
					strErrInfo.Format("分钟K缺失，请检查,SH:%d ,SZ:%d ", nMinKCountSH,nMinKCountSZ);
				}
			}
			break;
		case 7://日线校验
			{
				INT DAYKALERTCOUNT = 50;
				if (cItem.m_nMinVal==1)
				{
					if (CMDSApp::g_dwSysHMS < 180000 && (theApp.m_dwCheckDayDate != theApp.m_dwSaveDate))
					{
						nErrCode = i+1;
						strErrInfo.Format("日线未校验,校验:%d,收盘:%d", theApp.m_dwCheckDayDate, theApp.m_dwSaveDate);
					}

					// 增加校验数量 [4/22/2013 frantqian]
					if (CMDSApp::g_dwSysHMS > 183500 && (theApp.m_dwCheckDayDate == theApp.m_dwSaveDate) && theApp.m_nDMCheckCount > theApp.m_nSaveDayCount + DAYKALERTCOUNT )
					{
						nErrCode = i+1;
						std::string strAlertInfo= "";
						theApp.CreateCheckAlertInfo(strAlertInfo);
						strErrInfo.Format("日线收少于DM校验数,校:%d,收:%d, MINk :%d, %d", theApp.m_nDMCheckCount, theApp.m_nSaveDayCount, theApp.m_dwMinKErrCount, theApp.m_dwMinKErrValue);	
						LogApp("%s",strAlertInfo.c_str());
					}
				}
			}
			break;
		default:
			;
		}

		if (nErrCode>0)
		{
			break;
		}
	}

}
