#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <iconv.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>

#include "SysGlobal.h"
#include "AppGlobal.h"
#include "Goods.h"
#include "Data.h"

DWORD MulDiv(DWORD dwMulti1, DWORD dwMulti2, DWORD dwDivisor)
{
	if(dwDivisor == 0)
		return 0;

	UINT64 u64Mul = dwMulti1;
	u64Mul *= dwMulti2;

	UINT64 u64Div = u64Mul / dwDivisor;
	UINT64 u64Mod = u64Mul % dwDivisor;

	DWORD dwResult = (DWORD)u64Div;
	if(u64Mod > 0x7fffffff)
		dwResult++;

	return dwResult;
}

char *_strupr(char* pcSrc)
{
	int nStrlen = strlen(pcSrc);
	if(nStrlen > 0x40000)		// 256K
		nStrlen = 0x40000;

	char cDiff = 'a' - 'A';
	char* pcEnd = pcSrc + nStrlen;
	for(char* pc = pcSrc; pc < pcEnd; pc++)
	{
		if(*pc >= 'a' && *pc <= 'z')
			*pc -= cDiff;
	}

	return pcSrc;
}

char *_strlwr(char* pcSrc)
{
	int nStrlen = strlen(pcSrc);
	if(nStrlen > 0x40000)		// 256K
		nStrlen = 0x40000;

	char cDiff = 'a' - 'A';
	char* pcEnd = pcSrc + nStrlen;
	for(char* pc = pcSrc; pc < pcEnd; pc++)
	{
		if(*pc >= 'A' && *pc <= 'Z')
			*pc += cDiff;
	}

	return pcSrc;
}

DWORD GetSecondsDiff(DWORD dwTime1, DWORD dwTime2)		// hhmmss
{
	if(dwTime2 < dwTime1)
		return 0;

	return (dwTime2/10000-dwTime1/10000)*3600 + (dwTime2%10000/100-dwTime1%10000/100)*60 + (dwTime2%100-dwTime1%100);
}

int GetSecondsDiff_S(DWORD dwTime1, DWORD dwTime2)		// hhmmss
{
	BOOL bNegative = FALSE;

	if(dwTime2 < dwTime1)
	{
		DWORD dwTemp = dwTime1;
		dwTime1 = dwTime2;
		dwTime2 = dwTemp;

		bNegative = TRUE;
	}

	int nSecondsDiff = (dwTime2/10000-dwTime1/10000)*3600 + (dwTime2%10000/100-dwTime1%10000/100)*60 + (dwTime2%100-dwTime1%100);

	if(bNegative)
		nSecondsDiff = -nSecondsDiff;

	return nSecondsDiff;
}

DWORD AddSeconds(DWORD dwTime1, DWORD dwAddSeconds)		// hhmmss
{
	DWORD dwSeconds = dwTime1/10000*3600 + dwTime1%10000/100*60 + dwTime1%100;

	dwSeconds += dwAddSeconds;

	return dwSeconds/3600*10000 + dwSeconds%3600/60*100 + dwSeconds%60;
}

DWORD MinusSeconds(DWORD dwTime1, DWORD dwMinusSeconds)
{
	DWORD dwSeconds = dwTime1/10000*3600 + dwTime1%10000/100*60 + dwTime1%100;

	if(dwSeconds <= dwMinusSeconds)
		return 0;

	dwSeconds -= dwMinusSeconds;

	return dwSeconds/3600*10000 + dwSeconds%3600/60*100 + dwSeconds%60;
}

WORD GetMinutesDiff(WORD wTime1, WORD wTime2)			// hhmm
{
	if(wTime2 < wTime1)
		return 0;

	return (wTime2/100-wTime1/100)*60 + (wTime2%100-wTime1%100);
}

WORD AddMinutes(WORD wTime1, WORD wAddMinutes)			// hhmm
{
	WORD wHour = wTime1/100;
	WORD wMinute = wTime1%100;

	wMinute += wAddMinutes;
	if(wMinute >= 60)
	{
		wHour += (wMinute/60);
		wMinute = wMinute%60;

		if(wHour >= 24)
			wHour = wHour%24;
	}

	return wHour*100 + wMinute;
}

BYTE GetBytesCheckSum(PBYTE pcData, int nNumBytes)
{
	BYTE cCheckSum = 0;

	PBYTE pcEnd = pcData + nNumBytes;
	for(PBYTE pc = pcData; pc < pcEnd; pc++)
		cCheckSum += *pc;

	return cCheckSum;
}

BOOL g_IsGoodsID(DWORD dwGoodsID)
{
	char cStation = char(dwGoodsID/1000000);
	// 修改 [9/6/2013 frantqian]
	return (cStation>=0 );
	//return (cStation>=0 && cStation<=6);		
}

int g_SetNonBlocking(int nSocket)
{
	int fd_flags;
	int nodelay = 1;

	if (::setsockopt(nSocket,IPPROTO_TCP,TCP_NODELAY, (void *)&nodelay, sizeof(nodelay)))
		return -1;

	fd_flags = ::fcntl(nSocket,F_GETFL,0);

#if defined(O_NONBLOCK)
	fd_flags |= O_NONBLOCK;
#elif defined(O_NDELAY)
	fd_flags |= O_NDELAY;
#elif defined(FNDELAY)
	fd_flags |= O_FNDELAY;
#else
	return -1;
#endif

	if (::fcntl(nSocket,F_SETFL,fd_flags) == -1)
		return -1;

	return 0;
}

CString g_Value2String(INT64 hValue)
{	
	CString strValue, strText;

	if (hValue<0)
	{
		hValue = -hValue;
		strValue = "-";
	}
	else
		strValue = "";

	CString str;

	if (hValue<100000)
	{
		str.Format("%d", int(hValue));
		strText = "";
	}

	else if (hValue<1000000)  //100万
	{
		hValue = (hValue+50)/100;
		str.Format("%d.%02d", int(hValue/100), int(hValue%100));
		strText = "万";
	}
	else if (hValue<10000000)  //1000万
	{
		hValue = (hValue+500)/1000;
		str.Format("%d.%d", int(hValue/10), int(hValue%10));
		strText = "万";
	}
	else if (hValue<100000000)  //1亿
	{
		hValue = (hValue+5000)/10000;
		str.Format("%d", int(hValue));
		strText = "万";
	}
	else if (hValue<1000000000)  //10亿
	{
		hValue = (hValue+50000)/100000;
		str.Format("%d.%03d", int(hValue/1000), int(hValue%1000));
		strText = "亿";
	}
	else if (hValue<INT64(100)*100000000)  //100亿
	{
		hValue = (hValue+500000)/1000000;
		str.Format("%d.%02d", int(hValue/100), int(hValue%100));
		strText = "亿";
	}
	else if (hValue<INT64(1000)*100000000)  //1000亿
	{
		hValue = (hValue+5000000)/10000000;
		str.Format("%d.%d", int(hValue/10), int(hValue%10));
		strText = "亿";
	}
	else
	{
		hValue = (hValue+50000000)/100000000;
		str.Format("%d", int(hValue));
		strText = "亿";
	}
	strValue += str;

	return strValue+strText;
}

BOOL g_IsDateGood(DWORD dwDate)
{
	if (dwDate<19800101 || dwDate>20201231)
		return FALSE;
	DWORD dwDay = dwDate%100;
	if (dwDay<1 || dwDay>31)
		return FALSE;
	DWORD dwMonth = dwDate%10000/100;
	if (dwMonth<1 || dwMonth>12)
		return FALSE;
	return TRUE;
}

int g_CodeConvert(const char* pcCharsetFrom, const char* pcCharsetTo, char* pcInBuf, int nInBufLen, char* pcOutBuf, int nOutBufLen)
{
	int nRet = 0;

	if (pcInBuf == NULL || nInBufLen <= 0 || pcOutBuf == NULL || nOutBufLen <= 0)
		return nRet;

	//int argument = 1;
	iconv_t icv = iconv_open(pcCharsetTo, pcCharsetFrom);
	if (icv == (iconv_t)(-1)) {
		perror("转码出错:");	
		return nRet;
	}

	ZeroMemory(pcOutBuf, nOutBufLen);

	char** ppcInBuf = &pcInBuf;
	size_t nInSize = nInBufLen;

	char** ppcOutBuf = &pcOutBuf;
	size_t nOutSize = nOutBufLen;

	nRet = iconv(icv, ppcInBuf, &nInSize, ppcOutBuf, &nOutSize);
	if (nRet != -1)		// convert success
		nRet = nOutBufLen - (int)nOutSize;
	else
		nRet = 0;

	iconv_close(icv);

	return nRet;
}

WORD g_GetUnicodeString(const CString& str, WORD*& pwUnicode)
{
	if (pwUnicode)
		delete[] pwUnicode;
	WORD wRet = 0;

	WORD wSize = (WORD)str.GetLength();
	if (wSize>0)
	{
		pwUnicode = new(std::nothrow) WORD[wSize+1];
		if (pwUnicode)
		{
			char* pcGB2312 = str.GetData();
			int nDestLen = g_CodeConvert("GB18030", "UNICODE//TRANSLIT", pcGB2312, wSize, (char*)pwUnicode, wSize*2+2);
			if (nDestLen>2)
				wRet = nDestLen/2-1;

			// 修 [8/10/2012 frantqian]
			if (nDestLen == 0)
			{
				wRet = wSize/2; //wcslen((wchar_t *)pwUnicode) + 1;
			}
		}
	}

	return wRet;
}


const WORD l_pbCRCTable[256] =
{
	0, 4129, 8258, 12387, 16516, 20645, 24774, 28903, 33032, 37161, 41290, 
		45419, 49548, 53677, 57806, 61935, 4657, 528, 12915, 8786, 21173, 
		17044, 29431, 25302, 37689, 33560, 45947, 41818, 54205, 50076, 62463, 
		58334, 9314, 13379, 1056, 5121, 25830, 29895, 17572, 21637, 42346, 
		46411, 34088, 38153, 58862, 62927, 50604, 54669, 13907, 9842, 5649, 
		1584, 30423, 26358, 22165, 18100, 46939, 42874, 38681, 34616, 63455, 
		59390, 55197, 51132, 18628, 22757, 26758, 30887, 2112, 6241, 10242, 
		14371, 51660, 55789, 59790, 63919, 35144, 39273, 43274, 47403, 23285, 
		19156, 31415, 27286, 6769, 2640, 14899, 10770, 56317, 52188, 64447, 
		60318, 39801, 35672, 47931, 43802, 27814, 31879, 19684, 23749, 11298, 
		15363, 3168, 7233, 60846, 64911, 52716, 56781, 44330, 48395, 36200, 
		40265, 32407, 28342, 24277, 20212, 15891, 11826, 7761, 3696, 65439, 
		61374, 57309, 53244, 48923, 44858, 40793, 36728, 37256, 33193, 45514, 
		41451, 53516, 49453, 61774, 57711, 4224, 161, 12482, 8419, 20484, 
		16421, 28742, 24679, 33721, 37784, 41979, 46042, 49981, 54044, 58239, 
		62302, 689, 4752, 8947, 13010, 16949, 21012, 25207, 29270, 46570, 
		42443, 38312, 34185, 62830, 58703, 54572, 50445, 13538, 9411, 5280, 
		1153, 29798, 25671, 21540, 17413, 42971, 47098, 34713, 38840, 59231, 
		63358, 50973, 55100, 9939, 14066, 1681, 5808, 26199, 30326, 17941, 
		22068, 55628, 51565, 63758, 59695, 39368, 35305, 47498, 43435, 22596, 
		18533, 30726, 26663, 6336, 2273, 14466, 10403, 52093, 56156, 60223, 
		64286, 35833, 39896, 43963, 48026, 19061, 23124, 27191, 31254, 2801, 
		6864, 10931, 14994, 64814, 60687, 56684, 52557, 48554, 44427, 40424, 
		36297, 31782, 27655, 23652, 19525, 15522, 11395, 7392, 3265, 61215, 
		65342, 53085, 57212, 44955, 49082, 36825, 40952, 28183, 32310, 20053, 
		24180, 11923, 16050, 3793, 7920
};

#define LOBYTE(w)           ((BYTE)((DWORD)(w) & 0xff))
#define HIBYTE(w)           ((BYTE)((DWORD)(w) >> 8))

WORD g_GetCRC(BYTE* pbBuffer, DWORD dwBuffer)
{
	///???	ASSERT(dwBuffer%2==0);	// must have even number! (why?).
	WORD wCRC = 0;
	for (DWORD dw=0; dw<dwBuffer; dw++)
		wCRC = l_pbCRCTable[pbBuffer[dw]^HIBYTE(wCRC)]^LOBYTE(wCRC)<<8;
	return wCRC;
}



//CRC8校验       
//X^8 + X^2 + X^1 + 1       
unsigned char CRC8_TAB[256] = {           
	0x00,0x07,0x0E,0x09,0x1C,0x1B,0x12,0x15,0x38,0x3F,0x36,0x31,0x24,0x23,0x2A,0x2D,       
		0x70,0x77,0x7E,0x79,0x6C,0x6B,0x62,0x65,0x48,0x4F,0x46,0x41,0x54,0x53,0x5A,0x5D,       
		0xE0,0xE7,0xEE,0xE9,0xFC,0xFB,0xF2,0xF5,0xD8,0xDF,0xD6,0xD1,0xC4,0xC3,0xCA,0xCD,       
		0x90,0x97,0x9E,0x99,0x8C,0x8B,0x82,0x85,0xA8,0xAF,0xA6,0xA1,0xB4,0xB3,0xBA,0xBD,       
		0xC7,0xC0,0xC9,0xCE,0xDB,0xDC,0xD5,0xD2,0xFF,0xF8,0xF1,0xF6,0xE3,0xE4,0xED,0xEA,       
		0xB7,0xB0,0xB9,0xBE,0xAB,0xAC,0xA5,0xA2,0x8F,0x88,0x81,0x86,0x93,0x94,0x9D,0x9A,       
		0x27,0x20,0x29,0x2E,0x3B,0x3C,0x35,0x32,0x1F,0x18,0x11,0x16,0x03,0x04,0x0D,0x0A,       
		0x57,0x50,0x59,0x5E,0x4B,0x4C,0x45,0x42,0x6F,0x68,0x61,0x66,0x73,0x74,0x7D,0x7A,       
		0x89,0x8E,0x87,0x80,0x95,0x92,0x9B,0x9C,0xB1,0xB6,0xBF,0xB8,0xAD,0xAA,0xA3,0xA4,       
		0xF9,0xFE,0xF7,0xF0,0xE5,0xE2,0xEB,0xEC,0xC1,0xC6,0xCF,0xC8,0xDD,0xDA,0xD3,0xD4,       
		0x69,0x6E,0x67,0x60,0x75,0x72,0x7B,0x7C,0x51,0x56,0x5F,0x58,0x4D,0x4A,0x43,0x44,       
		0x19,0x1E,0x17,0x10,0x05,0x02,0x0B,0x0C,0x21,0x26,0x2F,0x28,0x3D,0x3A,0x33,0x34,       
		0x4E,0x49,0x40,0x47,0x52,0x55,0x5C,0x5B,0x76,0x71,0x78,0x7F,0x6A,0x6D,0x64,0x63,       
		0x3E,0x39,0x30,0x37,0x22,0x25,0x2C,0x2B,0x06,0x01,0x08,0x0F,0x1A,0x1D,0x14,0x13,       
		0xAE,0xA9,0xA0,0xA7,0xB2,0xB5,0xBC,0xBB,0x96,0x91,0x98,0x9F,0x8A,0x8D,0x84,0x83,       
		0xDE,0xD9,0xD0,0xD7,0xC2,0xC5,0xCC,0xCB,0xE6,0xE1,0xE8,0xEF,0xFA,0xFD,0xF4,0xF3       
}; 

BYTE g_GetCRC8(BYTE * ucPtr, BYTE ucLen) 
{ 
	BYTE ucIndex;		// CRC8校验表格索引 
	BYTE ucCRC8 = 0;	// CRC8字节初始化 

	// 进行CRC8位校验 
	while (ucLen--)
	{ 
		ucIndex = ucCRC8^(*ucPtr++); 
		ucCRC8 = CRC8_TAB[ucIndex]; 
	}

	// 返回CRC8校验数据 
	return (~ucCRC8); 
} 

//utf-8 :2byte, gbk:2-3byte
void UTF8toANSI(const std::string &from ,char* outbuf)
{
	char *inbuf=const_cast<char*>(from.c_str());
	size_t inlen = strlen(inbuf);
	size_t outlen = inlen *2;
	//char *outbuf = (char *)malloc(inlen * 4 );
	bzero( outbuf, inlen * 2);
	char *in = inbuf;
	char *out = outbuf;
	iconv_t cd=iconv_open("GBK","UTF-8");
	iconv(cd,&in,&inlen,&out,&outlen);
	iconv_close(cd);
	//return outbuf;
}

long kernel_mktime(struct tm * tm)
{
	long res;
	int year;

	year = tm->tm_year - 70;
	/* magic offsets (y+1) needed to get leapyears right.*/
	res = YEAR*year + DAY*((year+1)/4);
	res += month[tm->tm_mon];
	/* and (y+2) here. If it wasn't a leap-year, we have to adjust */
	if (tm->tm_mon>1 && ((year+2)%4))
		res -= DAY;
	res += DAY*(tm->tm_mday-1);
	res += HOUR*tm->tm_hour;
	res += MINUTE*tm->tm_min;
	res += tm->tm_sec;
	return res;
}


int CaculateWeekDay( int year,int month, int day )
{
	int week = 0;
	if(month==1)
	{
		month=13;year--;
	}
	if(month==2) 
	{
		month=14;year--;
	}

	// 	if((y<1752)||((y==1752)&&(m<9))||((y==1752)&&(m==9)&&(d<3))) //判断是否在1752年9月3日之前
	// 		week =(d+2*m+3*(m+1)/5+y+y/4+5)%7; //1752年9月3日之前的公式
	// 	else
	//		week=(day+2*month+3*(month+1)/5+year+year/4-year/100+year/400)%7; //1752年9月3日之后的公式

	//转换前 week对应周几 [11/6/2013 frantqian]
	//case 0: {weekstr="Monday"; break;}
	//case 1: {weekstr="Tuesday"; break;}
	//case 2: {weekstr="Wednesday"; break;}
	//case 3: {weekstr="Thursday"; break;}
	//case 4: {weekstr="Friday"; break;}
	//case 5: {weekstr="Saturday"; break;}
	//case 6: {weekstr="Sunday"; break;}

	// 不考虑1752年9月3日之前 [11/6/2013 frantqian]
	week=(day+2*month+3*(month+1)/5+year+year/4-year/100+year/400)%7;

	// 转换成与GetDayOfWeek()一致的值 [11/6/2013 frantqian]
	week += 2;
	week = week > 7? 0:week;

	return week;
}

