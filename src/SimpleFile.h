/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file SimpleFile.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 18 19:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef SIMPLEFILE_H
#define SIMPLEFILE_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <string>

#include "SysGlobal.h"

/**
* @brief class SimpleFile define 
*/
class SimpleFile
{
public:

	/**
	* @brief constructor
	*/
	SimpleFile();

	/**
	* @brief destructor
	*/
	~SimpleFile();

	/**
	* @brief copy create param of file
	* @param aFile
	*/
	void CopyCreateParam(const SimpleFile& aFile);

	/**
	* @brief check a directory whether exists
	* @param fn
	* @return if specify directory exists return true else false 
	*/
	static bool IsDirectoryExists(const char* fn);

	/**
	* @brief check a file whether exists
	* @param fn
	* @return if specify file exists return true else false 
	*/
	static bool IsFileExists(const char* fn);

	/**
	* @brief get file handle
	* @return file handle 
	*/
	int GetHandle();

	/**
	* @brief get file name
	* @return file name 
	*/
	const std::string& GetFileName();

	static UINT64 GetFileSize(int fd);
	static UINT64 GetFileSize(const char* file);

	/**
	* @brief get file size
	* @return file size 
	*/
	size_t GetFileSize();

	/**
	* @brief get last operation file error NO.
	* @return error NO.
	*/
	DWORD GetLastError();

	/**
	* @brief Set file attr Rea & Write
	*/
	void SetReadAndWrite();

	/**
	* @brief Set file attr Read only
	*/
	void SetReadOnly();

	/**
	* @brief Set file attr write only
	*/
	void SetWriteOnly();

	/**
	* @brief Set file name 
	* @param FileName
	*/
	void SetFileName(const char* FileName);
	
	/**
	* @brief get file valid flag
	* @param if file is valid return true else false
	*/
	bool IsValid();

	/**
	* @brief close file
	*/
	void Close();

	/**
	* @brief create file
	* @return if successfully true else false
	*/
	bool DoCreate();

	/**
	* @brief open a file that already existing
	* @return if successfully true else false
	*/
	bool OpenExisting();

	/**
	* @brief open a file, if it not exist create it.
	* @return if successfully true else false
	*/
	bool OpenAlways();

	/**
	* @brief set file pointer
	* @param pos
	* @return if successfully true else false
	*/
	bool SetFilePointer(int Pos);

	/**
	* @brief read data from file
	* @param buffer, buffer_size
	* @return if successfully true else false
	*/
	bool Read(void* buffer, DWORD buffer_size);

	/**
	* @brief write data to file
	* @param buffer, buffer_size
	* @return if successfully true else false
	*/
	bool Write(void* buffer, DWORD buffer_size);

private:
	int         m_hFile;		// file handle
	std::string      m_FileName;		// file name
	int			m_OpenFlag;		// file open attr
	mode_t      m_AccessMode;	// file access mode
	size_t      m_FileSize;		// file size
	DWORD       m_LastError;	// error NO.
};

//////////////////////////////////////////////////////////////////////////
// Inline Function
inline int SimpleFile::GetHandle()
{
	return m_hFile;
}

inline const std::string& SimpleFile::GetFileName()
{
	return  m_FileName;
}

inline void SimpleFile::SetReadOnly()
{
	m_OpenFlag = O_RDONLY;
}

inline void SimpleFile::SetWriteOnly()
{
	m_OpenFlag = O_WRONLY;
}

inline void SimpleFile::SetReadAndWrite()
{
	m_OpenFlag = O_RDWR;
}

inline void SimpleFile::SetFileName(const char* FileName)
{
	m_FileName = FileName;
}

inline DWORD SimpleFile::GetLastError()
{
	return m_LastError;
}

inline bool SimpleFile::IsValid()
{
	return m_hFile != -1;
}



#endif

