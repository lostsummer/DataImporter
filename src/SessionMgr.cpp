
/************************************
  REVISION LOG ENTRY
    Revision By: liuchengzhu
	  Revised on 2009-11-13 10:32:32
	    Comments: 会话管理模块
		 ************************************/

#include <sstream>
#include <algorithm>


//数据协议模块  注意包含 DS.h 头文件
#include "MDS.h"
#include "SockData.h"
#include "MyDefine.h"

#include "SessionMgr.h"

using namespace std;

//定义全局唯一的会话管理者
CSessionMgr _mgr;


// Function name   : CSessionMgr::CSessionMgr
// Description     : 构造函数
// Return type     : 

CSessionMgr::CSessionMgr()
{
	st_FuncInfo funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="年卡缴费";
	m_mapFuncNoToInfo[_DATA_MDS_Card_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="查询到期日";
	m_mapFuncNoToInfo[_DATA_MDS_EndDate_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="修改密码";
	m_mapFuncNoToInfo[_DATA_MDS_Change_PWD_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="查询自选股";
	m_mapFuncNoToInfo[_DATA_MDS_Download_ReferalGoods_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="更新自选股";
	m_mapFuncNoToInfo[_DATA_MDS_Update_ReferalGoods_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="通讯检测";
	m_mapFuncNoToInfo[_DATA_MDS_Ask_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="蹦蹦";
	m_mapFuncNoToInfo[_DATA_MDS_MainInfo_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="用户推荐";
	m_mapFuncNoToInfo[_DATA_MDS_Referal_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="积分查询";
	m_mapFuncNoToInfo[_DATA_MDS_Query_Loyalty_Points_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="积分兑换";
	m_mapFuncNoToInfo[_DATA_MDS_Exchange_Loyalty_Points_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="推荐类型";
	m_mapFuncNoToInfo[_DATA_MDS_Query_Action_Types_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="滚动新闻";
	m_mapFuncNoToInfo[_DATA_MDS_Query_Scroll_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="券商查询";
	m_mapFuncNoToInfo[_DATA_MDS_Query_Trader_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="券商地区";
	m_mapFuncNoToInfo[_DATA_MDS_Query_TraderAreas_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="券商营业部";
	m_mapFuncNoToInfo[_DATA_MDS_Query_TraderBranches_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="券商网站";
	m_mapFuncNoToInfo[_DATA_MDS_Query_TraderWapAddr_Info_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="图片查询";
	m_mapFuncNoToInfo[_DATA_MDS_Query_Screen_Logo_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="行情";
	m_mapFuncNoToInfo[_DATA_MDS_Quote_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="分时";
	m_mapFuncNoToInfo[_DATA_MDS_Cur_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="报价";
	m_mapFuncNoToInfo[_DATA_MDS_Grid_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="成交明细";
	m_mapFuncNoToInfo[_DATA_MDS_Bargain_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="基本面";
	m_mapFuncNoToInfo[_DATA_MDS_JBM_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="新闻正文";
	m_mapFuncNoToInfo[_DATA_MDS_InfoText_]=funcInfo;

	funcInfo.bitmask=0x00000001;
	funcInfo.name="K线操盘线";
	m_mapFuncNoToInfo[_DATA_MDS_His_]=funcInfo;

	funcInfo.bitmask=0x00000002;
	funcInfo.name="明日提示";
	m_mapFuncNoToInfo[_DATA_MDS_Tip_]= funcInfo;

	funcInfo.bitmask=0x00000004;
	funcInfo.name="道破天机";
	m_mapFuncNoToInfo[_DATA_MDS_DPTJ_]=funcInfo;

	funcInfo.bitmask=0x00000008;
	funcInfo.name="新闻列表";
	m_mapFuncNoToInfo[_DATA_MDS_InfoTitle_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="查询平台版自选股";
	m_mapFuncNoToInfo[_DATA_MDS_Download_ReferalGoods_Info_Platform_]=funcInfo;

	funcInfo.bitmask=0;
	funcInfo.name="更新平台版自选股";
	m_mapFuncNoToInfo[_DATA_MDS_Update_ReferalGoods_Info_Platform_]=funcInfo;
}


// Function name   : CSessionMgr::~CSessionMgr
// Description     : 析构函数
// Return type     : 

CSessionMgr::~CSessionMgr()
{
}


// Function name   : CSessionMgr::NewSession
// Description     : 增加会话
// Return type     : void 
// Argument        : string const UserId

DWORD CSessionMgr::NewSession(string const& UserId,short UserType,short ServiceType)
{
	DWORD SessionId;
	st_session *session=new st_session();

	CreateNewRandomString(UserId,UserType,ServiceType,SessionId);

	session->session_id=SessionId;
	session->user_id=UserId;
	
	session->begin_time=CMDSApp::g_tSysTime;
	session->end_time=session->begin_time;

	m_Lock.Lock();
	map_Session2User[SessionId]=session;
	m_Lock.UnLock();

	return session->session_id;
}


// Function name   : AddUserSession
// Description     : 增加会话
// Return type     : void 
// Argument        : DWORD const& SessionId
// Argument        : short UserType

void CSessionMgr::AddUserSession(string const& UserId,DWORD const& SessionId,DWORD const& Vendor,DWORD const& UserRight)
{
	st_session *session=new st_session();

	session->session_id=SessionId;
	session->user_id=UserId;
	
	session->begin_time=CMDSApp::g_tSysTime;
	session->end_time=session->begin_time;

	//add by liuchengzhu 20100417
	session->Vendor=Vendor;
	session->UserRight=UserRight;
	//end add by liuchengzhu 20100417

	m_Lock.Lock();
	map_Session2User[SessionId]=session;
	m_Lock.UnLock();

}


// Function name   : CSessionMgr::Fresh
// Description     : 根据会话标识 更新会话最新活动时间
// Return type     : void 
// Argument        : string const& SessionId

void CSessionMgr::Fresh(DWORD const& SessionId)
{
	DWORD dwSysTime = CMDSApp::g_tSysTime;
	
	m_Lock.Lock();

	//modify by liuchengzhu 20091120
	//会话列表存在，则更新会话最后时间
	if(map_Session2User.find(SessionId)!=map_Session2User.end())
	{
		st_session *session=map_Session2User[SessionId];
		//防止已经被检查删除的情况
		if(session)
			session->end_time=dwSysTime;
	}

	m_Lock.UnLock();

	//end modify by liuchengzhu 20091120
}


// Function name   : CSessionMgr::Check
// Description     : 检查超时会话
// Return type     : void 
// Argument        : int msTimeOut

void CSessionMgr::Check(int sTimeOut)
{

	time_t now_time;
	GetTime(&now_time);


	//会话超时检查
	m_Lock.Lock();

	for (map<DWORD,st_session *>::iterator it_map=map_Session2User.begin();it_map!=map_Session2User.end();it_map++)
	{
		if(it_map->second==NULL)
			continue;

		double diff=difftime(now_time,it_map->second->end_time);

		//超过N秒则认为会话终止
		if(diff>sTimeOut)
		{
			lst_TimeOutSession.push_back(it_map->second);
		}
	}
	m_Lock.UnLock();


	list<st_session *>::iterator it_session=lst_TimeOutSession.begin();
	for(;it_session!=lst_TimeOutSession.end();it_session++)
	{
		st_session *session=*it_session;
		//从Map中删除
		m_Lock.Lock();
		map_Session2User.erase(session->session_id);
		m_Lock.UnLock();

		////发送消息至FM通讯线程
		/*
        CFM_UserLogoutData* pData = new CFM_UserLogoutData();
		if (pData)
		{
			pData->m_strUserName.Format("%s",session->user_id.c_str());
			pData->m_UserSession=session->session_id;
			pData->m_dtBegin = session->begin_time;
			pData->m_dtEnd = session->end_time;

			//add by liuchengzhu 20100505
			//用户行为分析
			int Enable=theApp.GetProfileInt("UA","Enable",1);
			if(Enable!=0)
			{
				//用户行为分析添加记录
				CUserAction::Instance()->Add(pData);
			}
			else
			{
				delete pData;
			}
			//end add by liuchengzhu 20100505
			
			////结束时间与开始时间不相等时才有记录的必要
			//if(pData->m_dtEnd!=pData->m_dtBegin)
			//{
			//	//是否直接通过Fee记录至数据库
			//	int SessionToDB = theApp.GetProfileInt("System", "SessionToDB", 0);
			//	if(SessionToDB>0)
			//	{
			//		//通过FM记录至数据库
			//		theApp.m_thFM.AddRequestData(pData);
			//	}
			//}
		}
        */
		//删除指针
		delete session;
	}		
	lst_TimeOutSession.clear();
}


// Function name   : CSessionMgr::size
// Description     : 获取当前会话映射表大小
// Return type     : size_t 

size_t CSessionMgr::size()
{
	m_Lock.Lock();
	size_t nSize=map_Session2User.size();
	m_Lock.UnLock();
	return nSize;
}


// Function name   : CSessionMgr::clear
// Description     : 清除会话表
// Return type     : void 

void CSessionMgr::clear()
{
	map<DWORD,st_session *>::iterator iter=map_Session2User.begin();
	for (;iter!=map_Session2User.end();iter++)
	{
		delete iter->second;
	}
	map_Session2User.clear();
}

// Function name   : CSessionMgr::GetTime
// Description     : 获取时间
// Return type     : void 
// Argument        : time_t *lt

void CSessionMgr::GetTime(time_t	*lt)
{
	*lt = time(NULL);
}


// Function name   : CSessionMgr::CreateNewRandomString
// Description     : 根据用户标识和用户类型产生会话标识
// Return type     : void 
// Argument        : string const& UserId
// Argument        : short UserType
// Argument        : DWORD& out_Random

void CSessionMgr::CreateNewRandomString(string const& UserId,short UserType,short ServiceType,DWORD& out_Random)
{
	static unsigned int s_count = 0;
	time_t t = time(NULL);
	unsigned int seed = (unsigned int)t + s_count+atol(UserId.c_str());
	srand(seed);


	s_count++;
	if (s_count > 10000000)
	{
		s_count = 0;
	}

	DWORD dw_rnd_h=rand();
	DWORD dw_rnd_l=rand();
	DWORD dw_rnd=(dw_rnd_h<<16)|dw_rnd_l;

	short sRight=(UserType<<2)|ServiceType;
	out_Random=(dw_rnd&0xffffff00)|(sRight);
}


// Function name   : CSessionMgr::GetFormatTime
// Description     : 获取格式化时间串
// Return type     : string 
// Argument        : time_t *lt

string CSessionMgr::GetFormatTime(time_t* lt)
{
	struct tm _tm;
	localtime_r(lt, &_tm);

	char buf[20];
	char format[20]="%Y-%m-%d %H:%M:%S";
	strftime(buf,20,format,&_tm);

	return string(buf);
}

short CSessionMgr::GetSessionUserLevel(DWORD const& SessionId)
{
	short UserType = (SessionId >> 4)&0x00000003;

	return UserType;
}

// Function name   : GetSessionUserType
// Description     : 查询会话用户类型
// Return type     : short 
// Argument        : DWORD const& SessionId

short CSessionMgr::GetSessionUserType(DWORD const& SessionId)
{
	short UserType = (SessionId&0x0000000c) >> 2;

	if(UserType<UT_OVERTIME_USER || UserType>UT_ANONYMITY_USER)
	{
		UserType = UT_OVERTIME_USER;
	}

	return UserType;
}

// Function name   : GetSessionUserType
// Description     : 查询会话服务类型
// Return type     : short 
// Argument        : DWORD const& SessionId

short CSessionMgr::GetSessionServiceType(DWORD const& SessionId)
{
	short ServiceType=SessionId&0x00000003;

	return ServiceType;
}

string CSessionMgr::NewUserName(void)
{
	static unsigned int s_count = 1;
	time_t t = time(NULL);
	stringstream ss;
	ss<< "BN" << t << (s_count+100000)<<"EN";

	s_count++;
	if (s_count > 100000)
	{
		s_count = 0;
	}

	return ss.str();
}



