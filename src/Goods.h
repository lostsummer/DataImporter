#ifndef GOODS_H
#define GOODS_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <map>
#include <vector>
#include <set>
#include <algorithm>

#include "SysGlobal.h"
#include "AppGlobal.h"

#include "Array.h"
#include "BitStream.h"
#include "Buffer.h"
#include "GoodsJBM.h"
#include "HisGBAll.h"
#include "GoodsCode.h"
#include "Mutex.h"
#include "String.h"
#include "XInt32.h"
#include "PtrArray.h"
#include "HisGBAll.h"

const char L1SH_STATION = 15;

#define GOODS_PAGESIZE		4096
#define GOODS_BUFFER		GOODS_PAGESIZE*4

#define WEEKNUMPERYEAR 52

#define	CLOSE		0		//昨收

#define	OPEN		1		//开盘
#define	HIGH		2		//最高
#define	LOW			3		//最低
#define	PRICE		4		//成交

#define	TRADENUM	5		//成交笔数

#define	PBUY		6		//委买价
#define	PSELL		7		//委卖价

#define	P_ZT		8		//涨停价
#define	P_DT		9		//跌停价

#define PCLOSE		10		//前日收盘价(算昨涨跌幅)
#define WCLOSE		11		//上周收盘价(算周涨跌幅)
#define CLOSE5		12		//4日前收盘价(算5日涨跌幅)
#define	STRONG		13
#define PRICE5		14		//5分钟前的价格
#define NORMINAL	15		// 按盘价
#define SETTLEMENT  16		// 结算价
#define PRE_SETTLEMENT  17	// 前结算价

#define	PSELL10		101		//卖价十
#define	PSELL9		102		//卖价九
#define	PSELL8		103		//卖价八
#define	PSELL7		104		//卖价七
#define	PSELL6		105		//卖价六
#define	PSELL5		106		//卖价五
#define	PSELL4		107		//卖价四
#define	PSELL3		108		//卖价三
#define	PSELL2		109		//卖价二
#define	PSELL1		110		//卖价一

#define	PBUY1		111		//买价一
#define	PBUY2		112		//买价二
#define	PBUY3		113		//买价三
#define	PBUY4		114		//买价四
#define	PBUY5		115		//买价五
#define	PBUY6		116		//买价六
#define	PBUY7		117		//买价七
#define	PBUY8		118		//买价八
#define	PBUY9		119		//买价九
#define	PBUY10		120		//买价十

#define	VOLUME		500		//成交量
#define	AMOUNT		501		//成交额

#define	VBUY		502		//总买量
#define	VSELL		503		//总卖量

#define	ZGB			504		//总股本
#define	LTG			505		//流通股

#define	NEIPAN		506		//内盘
#define	CURVOL		507		//现手
#define	SUMVOL4		508		//最近 4日成交量和，算5日换手
#define	AVEVOL5		509		//最近 5日平均成交量，算量比
#define	SUMBIGAMT4	510		//最近 4日机构净买和，算5日净买, 不直接显示

#define	OPENINTEREST 511	//持仓
#define	CUR_OI		512		//增仓
#define	TODAY_OI	513		//日增仓

#define	SUMBIGVOL4	514		//最近 4日机构净买和，算5日大单比率, 不直接显示
#define	SUMAMT4		515		//最近 4日成交额和，算5日净买占比, 不直接显示

#define PRE_OPENINTEREST  516	// 前持仓
#define	CURBS		517		//成交方向
#define	PRICE_BASE	518		//300现价，股指期货算基差
#define CUR_OI_TYPE	519		//开平

#define	I_Num		520		// 样本个数
#define	I_Ave		521		// 样本均价(3)
#define	I_AveGB		522		// 样本平均股本(2) 亿股
#define	I_SumSZ		523		// 总市值(2) 万亿股
#define	I_Percent	524		// 占比%(2)
#define	I_SYL		525		// 静态市盈率(2)
#define	I_JBBS		526		// 级别标识

#define	VSELL10		601		//卖量十
#define	VSELL9		602		//卖量九
#define	VSELL8		603		//卖量八
#define	VSELL7		604		//卖量七
#define	VSELL6		605		//卖量六
#define	VSELL5		606		//卖量五
#define	VSELL4		607		//卖量四
#define	VSELL3		608		//卖量三
#define	VSELL2		609		//卖量二
#define	VSELL1		610		//卖量一

#define	VBUY1		611		//买量一
#define	VBUY2		612		//买量二
#define	VBUY3		613		//买量三
#define	VBUY4		614		//买量四
#define	VBUY5		615		//买量五
#define	VBUY6		616		//买量六
#define	VBUY7		617		//买量七
#define	VBUY8		618		//买量八
#define	VBUY9		619		//买量九
#define	VBUY10		620		//买量十




#define	NAME		-1		//名称
#define NAMECODE	-2		//股票代码

#define	JJJZ_N		-11		//基金最新净值
#define	JJJZ_Z		-12		//基金昨日净值
#define	JJJZ_RFPER	-13		//基金净值涨跌幅%

#define	AVEPRICE	-101

#define	ZHANGDIE	-120	//涨跌
#define	ZHANGSU		-121	//涨速
#define	JICHA		-122	//基差
#define ZHANGSUEFFECT5 -123  //5分钟涨速影响


#define	ZDF			-140	//涨跌幅
#define	ZSF			-141	//涨速幅
#define	ZDF5		-142	//5日涨跌幅
#define	WZDF		-143
#define	PZDF		-144	//
#define	ZHENFU		-145	//震幅

#define	ZDF10		-146	//10日涨跌
#define	ZDF20		-147	//20日涨跌
#define	ZLZC5		-148	//5日主力增仓
#define	ZLZC10		-149	//10日主力增仓
#define	ZLZC20		-150	//20日主力增仓
#define	JICHA_PER	-151	//基差%
#define	ZLQM		-152	//主力强买

#define	CPX_DAY		-153	//日线操盘线
#define CPX_WEEK	-154
#define CPX_MONTH	-155
#define CPX_MIN60	-156
#define CPX_MIN30	-157
#define CPX_MIN15	-158

#define	LIANGBI		-160	//量比
#define	SYL			-161	//市盈率
#define	HSL			-162	//换手率
#define	HSL5		-163	//5日换手率
#define	SJL			-164	//市净率
#define	BIGAMT		-165	//强买=主力净买
#define	BIGAMT5		-166	//5日强买
#define	DDBL		-167	//大单比率
#define	DDBL5		-168	//5日大单比率

#define	WEIBI1		-180	//委比

#define	RISE		-201	//涨家
#define	FALL		-202	//跌家
#define	EQUAL		-203	//平家

#define	SY			-301	//每股收益
#define	GJJ			-302
#define	JZC			-303



#define	WAIPAN		-500	//外盘
#define	AVEVOL		-501
#define	WEIBI2		-502

#define	ZSZ			-601
#define	LTSZ		-602	//流通市值
#define  PJ			-305	//评级

#define	ENAME		-701	//英文简称
#define	PRENAME		-702	//名称前缀
#define	TRADECODE	-703	//交易状态
#define GROUP_HY	-704	//行业板块

#define TTM_SYL		-962 //TTM市盈率
#define TTM_SXL		-963 //TTM市销率

#define XG_CJZJ_ASC			-1001 //超级资金趋势向上
#define XJ_CPX				-1002 //操盘线状态

#define XG_VOL_BURST		-1075 //突然放量

#define OTHER		-9999	//其它

//////////////////////////////////////////////////////////////////////

#define	SELL10		0		//卖十
#define	SELL9		1		//卖九
#define	SELL8		2		//卖八
#define	SELL7		3		//卖七
#define	SELL6		4		//卖六
#define	SELL5		5		//卖五
#define	SELL4		6		//卖四
#define	SELL3		7		//卖三
#define	SELL2		8		//卖二
#define	SELL1		9		//卖一

#define	BUY1		10		//买一
#define	BUY2		11		//买二
#define	BUY3		12		//买三
#define	BUY4		13		//买四
#define	BUY5		14		//买五
#define	BUY6		15		//买六
#define	BUY7		16		//买七
#define	BUY8		17		//买八
#define	BUY9		18		//买九
#define	BUY10		19		//买十

//////////////////////////////////////////////////////////////////////

#define GROUP_SH_INDEX	0x00000001	//指数
#define GROUP_SH_A		0x00000002	//A股
#define GROUP_SH_B		0x00000004	//B股
#define GROUP_SH_JJ		0x00000008	//基金
#define GROUP_SH_ZQ		0x00000010	//债券
#define GROUP_SH_ZZ		0x00000020	//转债
#define GROUP_SH_HG		0x00000040	//回购
#define GROUP_SH_ST		0x00000080	//ST股
#define GROUP_SH_CX		0x00000100	//次新股 
#define GROUP_SH_N		0x00000200	//新股
#define GROUP_SH_OTHER	0x00000400	//其他
#define GROUP_SH_WIT	0x00000800	//国债预发行

#define GROUP_SZ_INDEX	0x00001000	//指数
#define GROUP_SZ_A		0x00002000	//A股
#define GROUP_SZ_B		0x00004000	//B股
#define GROUP_SZ_JJ		0x00008000	//基金
#define GROUP_SZ_ZQ		0x00010000	//债券
#define GROUP_SZ_ZZ		0x00020000	//转债
#define GROUP_SZ_ST		0x00040000	//ST股
#define GROUP_SZ_CX		0x00080000	//次新股 
#define GROUP_SZ_N		0x00100000	//新股
#define GROUP_SZ_HG		0x00200000	//回购
#define GROUP_SZ_OTHER	0x00400000	//其他

#define GROUP_GNBK		0x01000000	//板块指数
#define GROUP_HYBK		0x02000000	//板块指数
#define GROUP_DQBK		0x04000000	//板块指数 
//#define GROUP_ZDYBK		0x08000000	//板块指数sam comments this line because GROUP_ZDYBK is defined by pc client and it is equal to GROUP_RDBK
#define GROUP_RDBK		0x10000000	//板块指数	//-三级概念  frant.qian20121102 

#define GROUP_SZ_ZXB	0x20000000	//深中小版
//#define GROUP_QZ		0x04000000	//权证
#define GROUP_WI		0x40000000  //环球指数

#define GROUP_HK		0x80000000  //H股

#define GROUP_SZ_CYB	0x100000000LL  //深创业板

#define GROUP_GZQH		0x200000000LL  //股指期货

#define GROUP_HKZB		0x400000000LL  //香港主板

#define GROUP_SZ_STB	0x800000000LL	//深三板
#define GROUP_SZ_XSB	0X1000000000LL  //新三板

#define GROUP_SH_Z		0x2000000000LL	//沪退市整理
#define GROUP_SZ_Z		0x4000000000LL	//深退市整理

#define GROUP_SHFE		0x8000000000LL	 //上海期货
#define GROUP_DCE		0x10000000000LL	 //大连商品
#define GROUP_CZCE		0x20000000000LL //郑州商品

//#define GROUP_ZGGN		0x20000000000LL //中国概念 sam comments because this macro is NOT used
#define GROUP_US		0x40000000000LL //

#define GROUP_WPQH		0x80000000000LL //外盘期货
#define GROUP_WH		0x100000000000LL //外汇

#define GROUP_SPQH		(GROUP_SHFE|GROUP_DCE|GROUP_CZCE)

#define GROUP_TF		0x200000000000LL //国债期货

//#define GROUP_HK_INDEX	0x200000000000LL //港股指数 sam comments because this macro is used to set variable only, not for to read. We can ignore it
#define GROUP_CEF		0x1000000000000LL	//封闭式基金
#define GROUP_OEF		0x2000000000000LL	//开放式基金
#define GROUP_ETF		0x4000000000000LL	//ETF式基金
#define GROUP_LOF		0x8000000000000LL	//LOF式基金

#define GROUP_OPTIONS	0x10000000000000LL	//期权

#define GROUP_GZXT1		0x100000000000000LL	//股转系统-两网及退市
#define GROUP_GZXT2		0x200000000000000LL	//股转系统-挂牌公司

#define GROUP_XGNBK		0x1000000000000000LL //小概念	2006xxx
#define GROUP_XHYBK		0x2000000000000000LL //小行业	2005xxx
#define GROUP_WHQH		0x4000000000000000LL	//外汇期货
#define GROUP_MASK_ALL  0xffffffffffffffffLL


#define GROUP_ALL_A		(GROUP_SH_A|GROUP_SZ_A|GROUP_SZ_ZXB|GROUP_SZ_CYB|GROUP_SH_ST|GROUP_SZ_ST) //全部A股
#define GROUP_SPZY_SH   (GROUP_SH_INDEX|GROUP_SH_A|GROUP_SH_B|GROUP_SH_JJ)
#define GROUP_SPZY_SZ   (GROUP_SZ_INDEX|GROUP_SZ_A|GROUP_SZ_B|GROUP_SZ_JJ|GROUP_SZ_ZXB|GROUP_SZ_CYB|GROUP_GZQH)

const int LEN_STOCKNAME = 32;
const BYTE MAX_TRADING_SESSION = 8;
const int LEN_SUSPENDINFO = 512;

enum gridgroupid
{
	_group_shszA = 0,   //沪深Ａ股
	_group_shA = 1,     //上证Ａ股
	_group_shB = 2,     //上证Ｂ股
	_group_szA = 3,     //深证Ａ股
	_group_szB = 4,     //深证Ｂ股
	_group_shJ = 5,     //上证基金
	_group_szJ = 6,     //深证基金
	_group_shZ = 7,     //上证债券
	_group_szZ = 8,     //深证债券
	_group_zxb = 9,     //中小板
	_group_qz = 10,      //权证
	_group_zs = 11,      //指数
	_group_zxg= 12,     //自选股
	_group_zjll=13,    //最近浏览
	_group_gnbk=14,    //概念板块
	_group_hybk=15,    //行业板块
	_group_dqbk=16,    //地区板块
	_group_kfjj=17,    //开放基金
	_group_Allbk=18,   //板块融合 
	_group_gzqh=19,    //股指期货
	_group_HK=20,      //香港H股
	_group_H =21,  
	_group_impzs=22,   // 重点指数
	_group_sb=23,		//三板
	_group_qqzs = 30,//全球指数,
	_group_htszl = 31,//沪退市整理,
	_group_stszl = 32,//深退市整理
	_group_shqh = 33,//上海期货
	_group_dlsp = 34,//大连期货
	_group_zzsp = 35,//郑州期货
	_group_zggn = 36,//中国概念
	_group_wpqh = 37,//外盘期货
	_group_wh = 38,//外汇
	_group_spqhhj = 39,//商品期货集合
	_group_swzs = 40,//申万指数板块
	_group_tf = 41,//国债期货板块
	_group_cyb = -14,   //创业板
	_group_xsb = 42, //新三板
	_group_xgn = -45,  //小概念
	_group_xhy = -46   //小行业
};




//Steven New Begin
//////////////////////////////////////////////////////////////////////

inline DWORD g_MultiPrice(DWORD dwPrice, DWORD dwDiv)
{
	if (dwDiv<=1)
		return dwPrice;
	else
		return dwPrice*dwDiv;
}

inline DWORD g_DivPrice(DWORD dwPrice, DWORD dwDiv)
{
	if (dwDiv<=1)
		return dwPrice;
	else
		return dwPrice/dwDiv;
}

enum SECURITY_TYPE
{
	INDEX = 0,				//指数
	EQUITY = 1,				//股票
	FUND = 2,				//基金
	BOND = 3,				//债券
	WARRANTS = 4,			//权证
	CONVERTIBLE = 5,		//可转债
	REPO = 6,				//回购
	WIT = 7,				//国债预发行
	OPTIONS = 8
};


//Steven New End
//////////////////////////////////////////////////////////////////////////
// 用户小额支付功能点权限位定义[10/17/2012 frantqian]

#define FUNPOINT_CPX		0x00000001		//操盘线
#define FUNPOINT_TIPDATA	0x00000002		//明日提示
#define FUNPOINT_FSBY		0x00000004		//分时博弈=资金净流
#define FUNPOINT_CJZJ		0x00000008		//超级资金
#define FUNPOINT_ZJBY		0x00000010		//资金博弈
#define FUNPOINT_DDBL		0x00000020		//大单比例
#define FUNPOINT_ZJLB		0x00000040		//资金流变
#define FUNPOINT_CMJS		0x00000080		//筹码聚散
#define FUNPOINT_SHZJ		0x00000100		//散户资金
#define FUNPOINT_DHZJ		0x00100000		//大户资金
//////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// 平台版用户小额支付功能点权限位定义[09/29/2014 zhaolin]
#define BITMAP_NEW_APP_CPX		0x00000001LL			//个股BS点
#define BITMAP_NEW_APP_TIPDATA	0x00000002LL			//明日提示
#define BITMAP_NEW_APP_FSBY		0x00000004LL			//分时博弈=资金净流
#define BITMAP_NEW_APP_CJZJ		0x00000008LL			//超级资金
#define BITMAP_NEW_APP_ZJBY		0x00000010LL			//资金博弈
#define BITMAP_NEW_APP_DDBL		0x00000020LL			//大单比例
#define BITMAP_NEW_APP_ZJLB		0x00000040LL			//资金流变
#define BITMAP_NEW_APP_CMJS		0x00000080LL			//筹码聚散
#define BITMAP_NEW_APP_SHZJ		0x00000100LL			//散户资金
#define BITMAP_NEW_APP_DHZJ		0x00100000LL			//大户资金
#define BITMAP_NEW_APP_CPX_PM	0x00000200LL			//自选股排名 BS点天数
#define BITMAP_NEW_APP_CPX_XG	0x00000400LL			//BS点选股
#define BITMAP_NEW_APP_CPX_YJ	0x00000800LL			//BS点预警
#define BITMAP_NEW_APP_SD		0x00001000LL			//十档
#define BITMAP_NEW_APP_ZJSC		0x00002000LL			//资金上穿
#define BITMAP_NEW_APP_ZJXC		0x00004000LL			//资金下穿
#define BITMAP_NEW_APP_YCXG		0x00008000LL			//一财选股
#define BITMAP_NEW_APP_ZTB		0x00010000LL			//涨停板
#define BITMAP_NEW_APP_ZDLH		0x00020000LL			//重大利好
#define BITMAP_NEW_APP_YCDJ		0x00040000LL			//一财独家
#define BITMAP_NEW_APP_VIP_ZX	0x00080000LL			//VIP资讯
#define BITMAP_NEW_APP_GRID_BS_PM 0x00400000LL			//排名列表BS点
//////////////////////////////////////////////////////////////////////////

struct CXGStatData //选股所需数据
{
	char m_bKXXT;			//k线前两日形态，是否一阳一阴
	short m_psDDBL[2];		//前2日大单比率*100
	XInt32 m_pxVol[4];		//4日内成交量数据
	XInt32 m_pxMinVol[4];	//历史,59,119,239，成交最低量
	DWORD m_pdwHighPrice[4];//历史，19,59,239最高成交价
	DWORD m_pdwLowPrice[4];	//历史，19,59,239最低成交价
	DWORD m_dwYHigh;		//昨最高 
	DWORD m_dwYOpen;		//昨开价 用于计算两阳夹一阴走势
	DWORD m_pdwReserved[16]; //
};

//////////////////////////////////////////////////////////////////////
// COrderCounts

struct COrderCounts
{
	// 0: 小 1: 中 2: 大 3: 特大
	DWORD	m_pdwNumOfBuy[4];		//买 单数
	DWORD	m_pdwNumOfSell[4];		//卖 单数
	XInt32	m_pxVolOfBuy[4];		//买 成交量(手)
	XInt32	m_pxVolOfSell[4];		//卖 成交量(手)
	XInt32	m_pxAmtOfBuy[4];		//买 成交额
	XInt32	m_pxAmtOfSell[4];		//卖 成交额

	COrderCounts()
	{
		Clear();
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(COrderCounts));
	}

	BYTE GetChecksum(BOOL bNew, BOOL bOrder)
	{
		if (bNew==FALSE)
			return GetBytesCheckSum((PBYTE)this, 64);
		else
		{
			BYTE cCheckSum = 0;

			for (int j=0; j<4; j++)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwNumOfBuy+j), 4);
				if (j>0 || bOrder)
					cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwNumOfSell+j), 4);

				if (j>0 || bOrder==FALSE)
				{
					cCheckSum += GetBytesCheckSum((PBYTE)(m_pxVolOfBuy+j), 4);
					cCheckSum += GetBytesCheckSum((PBYTE)(m_pxAmtOfBuy+j), 4);
				}

				if (j>0)
				{
					cCheckSum += GetBytesCheckSum((PBYTE)(m_pxVolOfSell+j), 4);
					cCheckSum += GetBytesCheckSum((PBYTE)(m_pxAmtOfSell+j), 4);
				}
			}
			return cCheckSum;
		}
	}

	DWORD GetChecksumDW(BOOL bOrder)
	{
		DWORD dwCheckSum = 0;

		for (int j=0; j<4; j++)
		{
			dwCheckSum += m_pdwNumOfBuy[j];
			if (j>0 || bOrder)
				dwCheckSum += m_pdwNumOfSell[j];

			if (j>0 || bOrder==FALSE)
			{
				dwCheckSum += m_pxVolOfBuy[j].GetRawData();
				dwCheckSum += m_pxAmtOfBuy[j].GetRawData();
			}

			if (j>0)
			{
				dwCheckSum += m_pxVolOfSell[j].GetRawData();
				dwCheckSum += m_pxAmtOfSell[j].GetRawData();
			}
		}

		return dwCheckSum;
	}

};

// COrderCounts
//////////////////////////////////////////////////////////////////////

#pragma pack(push, 1)

struct COrder
{
	COrder()
	{
		ZeroMemory(this, sizeof(COrder));
	}
	void Clear()
	{
		ZeroMemory(this, sizeof(COrder));
	}

	DWORD	m_dwTime;			// HHMMSS
	WORD	m_wNo;

	DWORD	m_dwPrice;
	DWORD	m_dwVolume;

	char	m_cKKKCC;			// 000KKKCC

	BYTE GetCheckSum()
	{
		BYTE cCheckSum = 0;

		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwVolume, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_cKKKCC, 1);
		if (m_cKKKCC==0 || m_cKKKCC==1)
			cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPrice, 4);

		return cCheckSum;
	}
};

//////////////////////////////////////////////////////////////////////////
struct CTrade
{
	CTrade()
	{
		ZeroMemory(this, sizeof(CTrade));
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CTrade));
	}

	DWORD m_dwTime;
	DWORD m_dwPrice;
	DWORD m_dwVolume;
	DWORD m_dwTradeNum;
	WORD m_wNo;
	char m_cBS;

	BYTE GetCheckSum()
	{
		BYTE cCheckSum = 0;

		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPrice, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwVolume, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_cBS, 1);

		return cCheckSum;
	}

	BYTE GetCheckSumOld()
	{
		BYTE cCheckSum = 0;
		BYTE pc[13];
		CopyMemory(pc, &m_dwTime, 4);
		CopyMemory(pc+4, &m_dwPrice, 4);
		CopyMemory(pc+8, &m_dwVolume, 4);
		CopyMemory(pc+12, &m_cBS, 1);
		for (int i=0; i<13; i++)
			cCheckSum += pc[i];
		return cCheckSum;
	}
};

//////////////////////////////////////////////////////////////////////////
struct CTradePV
{
	CTradePV()
	{
		ZeroMemory(this, sizeof(CTradePV));
	}

	DWORD m_dwPrice;
	XInt32 m_xVolume;
	DWORD m_dwTradeNum;

	XInt32 m_xVolBuy;
	DWORD m_dwNumBuy;

	BYTE GetCheckSum()
	{
		BYTE cCheckSum = 0;

		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPrice, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwTradeNum, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolBuy, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwNumBuy, 4);

		return cCheckSum;
	}
};

inline BOOL operator > (const CTradePV& first, const CTradePV& second)
{
	return first.m_dwPrice>second.m_dwPrice;
}

inline BOOL operator == (const CTradePV& first, const CTradePV& second)
{
	return first.m_dwPrice==second.m_dwPrice;
}

inline BOOL operator < (const CTradePV& first, const CTradePV& second)
{
	return first.m_dwPrice<second.m_dwPrice;
}


//////////////////////////////////////////////////////////////////////

struct CDay
{
	CDay()
	{
		ZeroMemory(this, sizeof(CDay));
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CDay));
	}

	DWORD m_dwTime;				//时间

	DWORD m_dwOpen;				//开盘价
	DWORD m_dwHigh;				//最高价
	DWORD m_dwLow;				//最低价
	DWORD m_dwClose;			//收盘价
	DWORD m_dwTradeNum;			//成交单数

	XInt32 m_xVolume;			//总成交量
	XInt32 m_xAmount;			//总成交金额

	XInt32	m_xNeiPan;			//内盘
	
	DWORD	m_dwNumOfBuy;		//买委托成交单数
	DWORD	m_dwNumOfSell;		//卖委托成交单数
	
	XInt32	m_pxVolOfBuy[3];	//买 分类成交量 数组只存放 1-中 2-大 3-特大 三种  0-小 通过总成交量来计算
	XInt32	m_pxVolOfSell[3];   //卖 分类成交量 数组只存放 1-中 2-大 3-特大 三种  0-小 通过总成交量来计算

	XInt32	m_pxAmtOfBuy[3];	//买 分类成交金额 数组只存放 1-中 2-大 3-特大 三种  0-小 通过总成交金额来计算
	XInt32	m_pxAmtOfSell[3];	//卖 分类成交金额 数组只存放 1-中 2-大 3-特大 三种  0-小 通过总成交金额来计算

	WORD	m_wRise;			//涨
	WORD	m_wFall;			//跌

	DWORD m_pdwReserve[1];

	BYTE GetCheckSum(BOOL bNew)
	{
		BYTE cCheckSum = 0;

		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwTime, 4);		// m_dwTime
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwClose, 4);	// m_dwClose
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 4);	// m_xVolume

		if(m_xVolume.GetValue() > 0)
		{
			cCheckSum += GetBytesCheckSum((PBYTE)&m_dwOpen, 12);	// m_dwOpen - m_dwLow, 3*4
			cCheckSum += GetBytesCheckSum((PBYTE)&m_dwTradeNum, 4);	// m_dwTradeNum
			cCheckSum += GetBytesCheckSum((PBYTE)&m_xAmount, 4);	// m_xAmount
			cCheckSum += GetBytesCheckSum((PBYTE)&m_xNeiPan, 4);	// m_xNeiPan
		}

		if (bNew==FALSE)
		{
			if (m_dwNumOfBuy + m_dwNumOfSell > 0)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)&m_dwNumOfBuy, 32);

				XInt32 xVol0 = m_xVolume-(m_pxVolOfBuy[0]+m_pxVolOfBuy[1]+m_pxVolOfBuy[2]);
				cCheckSum += GetBytesCheckSum((PBYTE)&xVol0, 4);

				xVol0 = m_xVolume-(m_pxVolOfSell[0]+m_pxVolOfSell[1]+m_pxVolOfSell[2]);
				cCheckSum += GetBytesCheckSum((PBYTE)&xVol0, 4);
			}
		}
		else
		{
			if(m_dwNumOfBuy + m_dwNumOfSell > 0)
				cCheckSum += GetBytesCheckSum((PBYTE)&m_dwNumOfBuy, 56);// m_dwNumOfBuy - m_pxAmtOfSell 14*4

			cCheckSum += GetBytesCheckSum((PBYTE)&m_wRise, 2);
			cCheckSum += GetBytesCheckSum((PBYTE)&m_wFall, 2);
		}

		if (m_dwTime>=907210000 && m_dwTime<=907211030)
		{
			;
		}

		return cCheckSum;
	}

	BYTE GetCRC()
	{
		return g_GetCRC8((BYTE *)this,sizeof(*this));
	}

};	// 100字节，96有用，4Reserve

inline BOOL operator > (CDay& first, CDay& second)
{
	return first.m_dwTime>second.m_dwTime;
}

inline BOOL operator == (CDay& first, CDay& second)
{
	return first.m_dwTime==second.m_dwTime;
}

inline BOOL operator < (CDay& first, CDay& second)
{
	return first.m_dwTime<second.m_dwTime;
}

typedef CSortArray<CDay> CSortArrayDay;

//////////////////////////////////////////////////////////////////////

struct CFDayMobile
{
	CFDayMobile()
	{
		ZeroMemory(this, sizeof(CFDayMobile));
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CFDayMobile));
	}

	DWORD m_dwTime;

	double m_fOpen;
	double m_fHigh;
	double m_fLow;
	double m_fClose;
	double m_fVolume;
	double m_fAmount;

	DWORD	m_dwNumOfBuy;
	DWORD	m_dwNumOfSell;

	double	m_fAmtOfBuy[3];
	double	m_fAmtOfSell[3];

	double	m_fVolOfBuy[3];
	double	m_fVolOfSell[3];

	double m_pfMA[6];
	double m_pfVMA[6];

	double m_pfInd[6];

	char m_cCPX;
};

struct CDayMobile
{
	CDayMobile()
	{
		ZeroMemory(this, sizeof(CDayMobile));
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CDayMobile));
	}

	DWORD m_dwTime;
	XInt32 m_xVolume;
	XInt32 m_xAmount;

	DWORD m_dwOpen;
	DWORD m_dwHigh;
	DWORD m_dwLow;
	DWORD m_dwClose;

	DWORD m_pdwMA[6];
	XInt32 m_pxVMA[6];
	XInt32 m_pxInd[6];

	char m_cCPX;

	char m_cStatus;		// 1 => SendInd only

	char m_cQKT;		//乾坤图 
	char m_cCWB;		//仓位

	DWORD m_dwHSL;
	XInt32 m_xZJJL;
};

inline BOOL operator > (CDayMobile& first, CDayMobile& second)
{
	return first.m_dwTime>second.m_dwTime;
}

inline BOOL operator == (CDayMobile& first, CDayMobile& second)
{
	return first.m_dwTime==second.m_dwTime;
}

inline BOOL operator < (CDayMobile& first, CDayMobile& second)
{
	return first.m_dwTime<second.m_dwTime;
}

typedef CArray<CFDayMobile, CFDayMobile&> CArrayFDayMobile;
typedef CSortArray<CDayMobile> CSortArrayDayMobile;

//////////////////////////////////////////////////////////////////////

struct CMinute
{
	CMinute()
	{
		ZeroMemory(this, sizeof(CMinute));
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CMinute));
	}

	DWORD m_wTime;					// hhmm

	DWORD m_dwOpen;
	DWORD m_dwHigh;
	DWORD m_dwLow;
	DWORD m_dwClose;

	XInt32 m_xVolume;				// 成交量
	XInt32 m_xAmount;				// 成交额
	DWORD m_dwTradeNum;				// 股指期货时=m_xOpenInterest

	DWORD m_dwAve;

	//有Index的指数和Level2个股
	DWORD m_dwSellPrice;			// 委托卖出均价
	DWORD m_dwBuyPrice;				// 委托买入均价
	XInt32	m_xSellVol;				// 委托卖出总量
	XInt32	m_xBuyVol;				// 委托买入总量
	//有Index的指数和Level2个股

	//Level2个股
	COrderCounts m_ocOrder;			// 成交委托单统计 (s m l xl)
	COrderCounts m_ocTrade;			// 每笔统计 (s m l xl)

	XInt32 m_pxNewOrder[2];			// 0:买入新增 1:卖出新增
	XInt32 m_pxDelOrder[2];			// 0:买入撤单 1:卖出撤单
	//Level2个股

	//有Index的指数专有
	int m_nStrong;

	WORD m_wRise;
	WORD m_wFall;
	//有Index的专有

	//5档买卖盘量的和, 算Level1的委比
	XInt32	m_xSellVol5;			// 5档卖盘量的和
	XInt32	m_xBuyVol5;				// 5档买盘量的和
	//5档买卖盘的量的和, 算Level1的委比
	DWORD m_dwCount;				// 刷新计数

	BYTE GetCheckSum(BOOL bQH, BOOL bIndex, BOOL bIsLevel2, int PRICE_DIVIDE,BOOL bClient = FALSE)
	{
		BYTE cCheckSum = 0;

		WORD wTime = (WORD)(m_wTime%10000);
		cCheckSum += GetBytesCheckSum((PBYTE)&wTime, 2);		// m_wTime

		DWORD dwPrice;

		dwPrice = m_dwOpen/PRICE_DIVIDE*PRICE_DIVIDE;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
		dwPrice = m_dwHigh/PRICE_DIVIDE*PRICE_DIVIDE;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
		dwPrice = m_dwLow/PRICE_DIVIDE*PRICE_DIVIDE;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
		dwPrice = m_dwClose/PRICE_DIVIDE*PRICE_DIVIDE;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);

		if (bQH)
		{
			cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 16);
		}
		else
		{
			if (bClient)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 24);	// m_xVolume - m_xBuyVol, 6*4
				if (bIsLevel2)
					cCheckSum += GetBytesCheckSum((PBYTE)&m_xSellVol, 8);
				else
					cCheckSum += GetBytesCheckSum((PBYTE)&m_xSellVol5, 8);
			}
			else
				cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 32);	// m_xVolume - m_xBuyVol, 8*4

			if(bIndex)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)&m_nStrong, 4);
				cCheckSum += GetBytesCheckSum((PBYTE)&m_wRise, 2);
				cCheckSum += GetBytesCheckSum((PBYTE)&m_wFall, 2);
			}

			if(bIsLevel2)
			{
				cCheckSum += m_ocOrder.GetChecksum(TRUE, TRUE);
				cCheckSum += m_ocTrade.GetChecksum(TRUE, FALSE);

				cCheckSum += GetBytesCheckSum((PBYTE)m_pxNewOrder, 16);		// m_pxNewOrder - m_pxDelOrder, 4*4
			}
		}

		return cCheckSum;
	}


	DWORD GetCheckSumDW(BOOL bQH, BOOL bIndex, BOOL bIsLevel2, int PRICE_DIVIDE)
	{
		DWORD dwCheckSum = 0;

		WORD wTime = (WORD)(m_wTime%10000);
		dwCheckSum += wTime;

		dwCheckSum += m_dwOpen/PRICE_DIVIDE*PRICE_DIVIDE;
		dwCheckSum += m_dwHigh/PRICE_DIVIDE*PRICE_DIVIDE;
		dwCheckSum += m_dwLow/PRICE_DIVIDE*PRICE_DIVIDE;
		dwCheckSum += m_dwClose/PRICE_DIVIDE*PRICE_DIVIDE;

		if (bQH)
		{
			dwCheckSum += m_xVolume.GetRawData();
			dwCheckSum += m_xAmount.GetRawData();
			dwCheckSum += m_dwTradeNum;
			dwCheckSum += m_dwAve;
		}
		else
		{
			dwCheckSum += m_xVolume.GetRawData();
			dwCheckSum += m_xAmount.GetRawData();
			dwCheckSum += m_dwTradeNum;
			dwCheckSum += m_dwAve;

			dwCheckSum += m_dwSellPrice;
			dwCheckSum += m_dwBuyPrice;
			dwCheckSum += m_xSellVol.GetRawData();
			dwCheckSum += m_xBuyVol.GetRawData();

			if(bIndex)
			{
				dwCheckSum += m_nStrong;
				dwCheckSum += m_wRise;
				dwCheckSum += m_wFall;
			}

			if(bIsLevel2)
			{
				dwCheckSum += m_ocOrder.GetChecksumDW(TRUE);
				dwCheckSum += m_ocTrade.GetChecksumDW(FALSE);

				dwCheckSum += m_pxNewOrder[0].GetRawData();
				dwCheckSum += m_pxNewOrder[1].GetRawData();
				dwCheckSum += m_pxDelOrder[0].GetRawData();
				dwCheckSum += m_pxDelOrder[1].GetRawData();
			}
		}

		return dwCheckSum;
	}

	BYTE GetCheckSum2Stock(BOOL bQH, BOOL bIndex, BOOL bIsLevel2, BOOL bNew, int PRICE_DIVIDE)
	{
		BYTE cCheckSum = 0;

		WORD wTime = (WORD)(m_wTime%10000);
		cCheckSum += GetBytesCheckSum((PBYTE)&wTime, 2);		// m_wTime

		DWORD dwPrice = m_dwClose/PRICE_DIVIDE*PRICE_DIVIDE;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);

		if (bQH)
		{
			cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 16);
		}
		else
		{
			cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 24);	// m_xVolume - m_dwBuyPrice, 6*4

			if (bIsLevel2)
				cCheckSum += GetBytesCheckSum((PBYTE)&m_xSellVol, 8);
			else
				cCheckSum += GetBytesCheckSum((PBYTE)&m_xSellVol5, 8);

			if(bIndex)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)&m_nStrong, 4);
				cCheckSum += GetBytesCheckSum((PBYTE)&m_wRise, 2);
				cCheckSum += GetBytesCheckSum((PBYTE)&m_wFall, 2);
			}

			if(bIsLevel2)
			{
				cCheckSum += m_ocOrder.GetChecksum(bNew, TRUE);
				cCheckSum += m_ocTrade.GetChecksum(bNew, FALSE);

				cCheckSum += GetBytesCheckSum((PBYTE)m_pxNewOrder, 16);		// m_pxNewOrder - m_pxDelOrder, 4*4
			}
		}

		return cCheckSum;
	}

	DWORD GetCheckSumDW2Stock(BOOL bQH, BOOL bIndex, BOOL bIsLevel2, int PRICE_DIVIDE = 1)
	{
		DWORD dwCheckSum = 0;

		WORD wTime = (WORD)(m_wTime%10000);
		dwCheckSum += wTime;

		dwCheckSum += m_dwClose/PRICE_DIVIDE*PRICE_DIVIDE;

		if (bQH)
		{
			dwCheckSum += m_xVolume.GetRawData();
			dwCheckSum += m_xAmount.GetRawData();
			dwCheckSum += m_dwTradeNum;
			dwCheckSum += m_dwAve;
		}
		else
		{
			dwCheckSum += m_xVolume.GetRawData();
			dwCheckSum += m_xAmount.GetRawData();
			dwCheckSum += m_dwTradeNum;
			dwCheckSum += m_dwAve;

			dwCheckSum += m_dwSellPrice;
			dwCheckSum += m_dwBuyPrice;
			dwCheckSum += m_xSellVol.GetRawData();
			dwCheckSum += m_xBuyVol.GetRawData();

			if(bIndex)
			{
				dwCheckSum += m_nStrong;
				dwCheckSum += m_wRise;
				dwCheckSum += m_wFall;
			}

			if(bIsLevel2)
			{
				dwCheckSum += m_ocOrder.GetChecksumDW(TRUE);
				dwCheckSum += m_ocTrade.GetChecksumDW(FALSE);

				dwCheckSum += m_pxNewOrder[0].GetRawData();
				dwCheckSum += m_pxNewOrder[1].GetRawData();
				dwCheckSum += m_pxDelOrder[0].GetRawData();
				dwCheckSum += m_pxDelOrder[1].GetRawData();
			}
		}

		return dwCheckSum;
	}

	void CopyEmptyMinute(CMinute& minSave, BOOL bIsGZQH)
	{
		m_dwOpen = m_dwHigh = m_dwLow = m_dwClose = minSave.m_dwClose;
		m_dwAve = minSave.m_dwAve;

		m_dwSellPrice = minSave.m_dwSellPrice;
		m_dwBuyPrice = minSave.m_dwBuyPrice;

		m_xSellVol = minSave.m_xSellVol;
		m_xBuyVol = minSave.m_xBuyVol;

		m_nStrong = minSave.m_nStrong;
		m_wRise = minSave.m_wRise;
		m_wFall = minSave.m_wFall;

		if (bIsGZQH)
			m_dwTradeNum = minSave.m_dwTradeNum;
	}

};

inline BOOL operator > (CMinute& first, CMinute& second)
{
	return first.m_wTime>second.m_wTime;
}

inline BOOL operator == (CMinute& first, CMinute& second)
{
	return first.m_wTime==second.m_wTime;
}

inline BOOL operator < (CMinute& first, CMinute& second)
{
	return first.m_wTime<second.m_wTime;
}

typedef CArray<CMinute, CMinute&> CArrayMinute;
typedef CSortArray<CMinute> CSortArrayMinute;


class CDayCheck
{
public:
	WORD m_wDay;
	BYTE m_bCheckSum; 
	static WORD DateToWord(DWORD dwDate);
	static DWORD WordToDate(WORD wDay);
private:	
	static const int DAY_BEGIN_YEAR;
};

inline BOOL operator > (CDayCheck& first, CDayCheck& second)
{
	return first.m_wDay>second.m_wDay;
}

inline BOOL operator == (CDayCheck& first, CDayCheck& second)
{
	return first.m_wDay==second.m_wDay;
}

inline BOOL operator < (CDayCheck& first, CDayCheck& second)
{
	return first.m_wDay<second.m_wDay;
}

typedef CSortArray<CDayCheck> CDayCheckArray;

//////////////////////////////////////////////////////////////////////
struct CHisMin
{
	CHisMin()
	{
		ZeroMemory(this, sizeof(CHisMin));
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CHisMin));
	}

	DWORD m_dwTime;					// YYMMDDhhmm

	DWORD m_dwPrice;
	DWORD m_dwAve;					// 均价

	XInt32 m_xVolume;				// 成交量
	XInt32 m_xZJJL;					// 资金净流/期货持仓量

	char GetCheckSum(int PRICE_DIVIDE, BOOL bLevel2 = TRUE)
	{
		BYTE cCheckSum = 0;

		WORD wTime = WORD(m_dwTime%10000);	// hhmm

		cCheckSum += GetBytesCheckSum((PBYTE)&wTime, 2);
		DWORD dwPrice = m_dwPrice/PRICE_DIVIDE*PRICE_DIVIDE;
		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwAve, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 4);

		if (bLevel2)
			cCheckSum += GetBytesCheckSum((PBYTE)&m_xZJJL, 4);

		return cCheckSum;
	}

	char GetCheckSum6()
	{
		BYTE cCheckSum = 0;

		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwTime, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwPrice, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwAve, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xZJJL, 4);

		return cCheckSum;
	}
};

inline BOOL operator > (CHisMin& first, CHisMin& second)
{
	return first.m_dwTime>second.m_dwTime;
}

inline BOOL operator == (CHisMin& first, CHisMin& second)
{
	return first.m_dwTime==second.m_dwTime;
}

inline BOOL operator < (CHisMin& first, CHisMin& second)
{
	return first.m_dwTime<second.m_dwTime;
}


typedef CSortArray<CMinute> CMinuteSortArray;
typedef CSortArray<CHisMin> CHisMinArray;

//////////////////////////////////////////////////////////////////////////
struct CBargain
{
	CBargain()
	{
		ZeroMemory(this, sizeof(CBargain));
	}

	DWORD m_dwDate;
	DWORD m_dwTime;
	DWORD m_dwPrice;
	XInt32 m_xVolume;
	DWORD m_dwTradeNum;
	char m_cBS;

	BYTE GetCheckSum(int PRICE_DIVIDE)
	{
		BYTE cCheckSum = 0;
		DWORD dwPrice = m_dwPrice/PRICE_DIVIDE*PRICE_DIVIDE;

		cCheckSum += GetBytesCheckSum((PBYTE)&dwPrice, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_xVolume, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwTradeNum, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_cBS, 1);

		return cCheckSum;
	}

	DWORD GetCheckSumDW(int PRICE_DIVIDE)
	{
		DWORD dwCheckSum = 0;
		DWORD dwPrice = m_dwPrice/PRICE_DIVIDE*PRICE_DIVIDE;

		dwCheckSum += dwPrice;
		dwCheckSum += m_xVolume.GetRawData();
		dwCheckSum += m_dwTradeNum;
		dwCheckSum += m_cBS;

		return dwCheckSum;
	}

	BYTE GetCheckSumOld()
	{
		BYTE cCheckSum = 0;
		BYTE* pc = (BYTE*)this;
		int nSize = int(sizeof(CBargain));
		for (int i=0; i<nSize; i++)
			cCheckSum += pc[i];
		return cCheckSum;
	}
};

typedef CArray<CBargain, CBargain&> CArrayBargain;

//////////////////////////////////////////////////////////////////////
struct COQ_Record
{
	COQ_Record()
	{
		Clear();
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(COQ_Record));
	}

	//接收数据
	DWORD m_dwTime;
	DWORD m_dwPrice;

	DWORD m_dwNum;		// 总共有几个，<=50时==m_bNum

	BYTE m_bNum;		// 有几个，最多50
	DWORD m_pdwVol[50];
	//接收数据

	void Read(CBuffer& buf)
	{
		buf.Read(&m_dwTime, 4);
		buf.Read(&m_dwPrice, 4);
		buf.Read(&m_dwNum, 4);

		buf.Read(&m_bNum, 1);
		if (m_bNum>0)
			buf.Read(m_pdwVol, m_bNum*4);
	}

	void Write(CBuffer& buf)
	{
		buf.Write(&m_dwTime, 4);
		buf.Write(&m_dwPrice, 4);
		buf.Write(&m_dwNum, 4);

		buf.Write(&m_bNum, 1);
		if (m_bNum>0)
			buf.Write(m_pdwVol, m_bNum*4);
	}

	friend BOOL operator > (COQ_Record& first, COQ_Record& second)
	{
		return first.m_dwPrice>second.m_dwPrice;
	}

	friend BOOL operator == (COQ_Record& first, COQ_Record& second)
	{
		return first.m_dwPrice==second.m_dwPrice;
	}

	friend BOOL operator < (COQ_Record& first, COQ_Record& second)
	{
		return first.m_dwPrice<second.m_dwPrice;
	}
};


#pragma pack(pop)

//////////////////////////////////////////////////////////////////////
const CHAR ORDER_BID = 1;
const CHAR ORDER_OFFER = 2;

///////////////////////////////////////////////////////////////////////
const int NUMOF_RCT_TRADEORDER = 10;

// COrderStatus
enum ORDER_CMP
{
	ORDERCMP_NOCHG = 0,
	ORDERCMP_TRADE = 1,
	ORDERCMP_PARTTRADE = 2,
	ORDERCMP_CANCEL = 3,
	ORDERCMP_ADD = 4,
	ORDERCMP_NOTSURE = 5,
};

struct COrderStatus
{
	DWORD		m_dwOrderVol;		// 委托单量
	ORDER_CMP	m_status;			// 委托单状态

	COrderStatus()
	{
		Clear();
	}

	COrderStatus(DWORD dwVol, ORDER_CMP status)
	{
		m_dwOrderVol = dwVol;
		m_status = status;
	}

	COrderStatus(const COrderStatus& rth)
	{
		m_dwOrderVol = rth.m_dwOrderVol;
		m_status = rth.m_status;
	}

	COrderStatus& operator=(const COrderStatus& rth)
	{
		if (this != &rth)
		{
			m_dwOrderVol = rth.m_dwOrderVol;
			m_status = rth.m_status;
		}

		return *this;
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(COrderStatus));
	}

	void Read(CBuffer& buf)
	{
		buf.Read(&m_dwOrderVol, 4);
		buf.Read(&m_status, 1);
	}

	void Write(CBuffer& buf)
	{
		buf.Write(&m_dwOrderVol, 4);
		buf.Write(&m_status, 1);
	}
};

typedef CArray<COrderStatus, COrderStatus&> COrderStatusArray;

//////////////////////////////////////////////////////////////////////////
// CPartOrder
// 20080424
const CHAR PART_BEGIN = 0;
const CHAR PART_MEDIUM = 1;
const CHAR PART_END = 2;
const CHAR PART_ALL = 3;

const CHAR PART_ACTIVE = 4;
const CHAR PART_OFFER = 8;
// 20080424

struct CPartOrder
{
	DWORD		m_dwBeginTradeID;	// 起始交易单号
	DWORD		m_dwEndTradeID;		// 结束交易单号
	DWORD		m_dwOriginalVol;	// 部分成交的原始委托单量
	CHAR		m_cPartStatus;		// 部分成交的原始委托单状态

	CPartOrder()
	{
		Clear();
	}

	CPartOrder(DWORD dwTradeID, DWORD dwOrigVol, CHAR cPs)
	{
		m_dwBeginTradeID = dwTradeID;
		m_dwEndTradeID = dwTradeID;
		m_dwOriginalVol = dwOrigVol;
		m_cPartStatus = cPs;
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CPartOrder));
	}

	void Read(CBuffer& buf)
	{
		buf.Read(&m_dwBeginTradeID, 4);
		buf.Read(&m_dwEndTradeID, 4);
		buf.Read(&m_dwOriginalVol, 4);
		buf.Read(&m_cPartStatus, 1);
	}

	void Write(CBuffer& buf)
	{
		buf.Write(&m_dwBeginTradeID, 4);
		buf.Write(&m_dwEndTradeID, 4);
		buf.Write(&m_dwOriginalVol, 4);
		buf.Write(&m_cPartStatus, 1);
	}

	BYTE GetCheckSum()
	{
		BYTE cCheckSum = 0;

		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwBeginTradeID, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwEndTradeID, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_dwOriginalVol, 4);
		cCheckSum += GetBytesCheckSum((PBYTE)&m_cPartStatus, 1);

		return cCheckSum;
	}
};

typedef CArray<CPartOrder, CPartOrder&> CPartOrderArray;

const BYTE OUTPUT_ACTIVE = 8;	// compress
//////////////////////////////////////////////////////////////////////////
// CHisOrderQueue

struct CHisOrderQueue
{
	// received data
	DWORD		m_dwTime;			// 时间
	DWORD		m_dwPrice;			// 价位
	CHAR		m_cBuySell;			// 买1/卖2
	DWORD		m_dwNumOfOrders;	// 总单数

	//CDWordArray	m_adwOrderVol;		// 前最多50笔委托量
	std::vector<DWORD>	m_adwOrderVol;		// 前最多50笔委托量

	char		m_cActive;			// 1: 表示当前的买一/卖一, 下面的数据有效

	// calculation...
	DWORD		m_dwOriginalVol;	// 分拆单的原始委托单量
	DWORD		m_dwPartTradeVol;	// 分拆单上的已确认成交量
	DWORD		m_dwPartNewVol;		// 分拆单上的已确认的最新成交量

	int			m_nNumOfRcntTrade;	// 最近成交单个数
	DWORD		m_adwRcntTrade[NUMOF_RCT_TRADEORDER];	// 最近成交单

	//COrderStatusArray	m_aOrderStatus;				// 最新委托单交易状态变化
	std::vector<COrderStatus>	m_aOrderStatus;		// 最新委托单交易状态变化

	BYTE		m_bWantDelete;

	CHisOrderQueue()
	{
		m_dwTime = 0;
		m_dwPrice = 0;
		m_cBuySell = 0;
		m_dwNumOfOrders = 0;

		m_dwOriginalVol = 0;
		m_dwPartTradeVol = 0;
		m_dwPartNewVol = 0;

		m_nNumOfRcntTrade = 0;
		ZeroMemory(m_adwRcntTrade, sizeof(m_adwRcntTrade));
	}

	//CHisOrderQueue::~CHisOrderQueue()
	//{
	//	m_adwOrderVol.clear();
	//	m_aOrderStatus.clear();
	//}

	const CHisOrderQueue& operator=(const CHisOrderQueue& src);

	void Read(CBuffer&);
	void Write(CBuffer&);
	void Write2OldStock(CBuffer& buf, char cBuySell1);

	BYTE GetCheckSum(BOOL bBuySell1);

	friend BOOL operator > (CHisOrderQueue& first, CHisOrderQueue& second)
	{
		return first.m_dwPrice < second.m_dwPrice;
	}

	friend BOOL operator == (CHisOrderQueue& first, CHisOrderQueue& second)
	{
		return first.m_dwPrice == second.m_dwPrice;
	}

	friend BOOL operator < (CHisOrderQueue& first, CHisOrderQueue& second)
	{
		return first.m_dwPrice > second.m_dwPrice;
	}
};



typedef CSortArray<CHisOrderQueue> CHisOrderQueueSArray;

//////////////////////////////////////////////////////////////////////////
// CStatCounts
struct CStatCounts
{
	DWORD m_dwCount;				// 更新次数

	DWORD m_pdwNumOfBuy[25];		// 累计笔数or单数
	XInt32 m_pxVolOfBuy[25];		// 累计成交量
	DWORD m_pdwNumOfSell[25];		// 累计笔数or单数
	XInt32 m_pxVolOfSell[25];		// 累计成交量
	DWORD m_dwMaxVolOfBuy;			// 单个最大成交量(笔或单)
	DWORD m_dwMaxVolOfSell;			// 单个最大成交量(笔或单)

	CStatCounts()
	{
		Clear();
	}

	void Clear()
	{
		ZeroMemory(this, sizeof(CStatCounts));
	}

	BYTE GetCheckSum()
	{
		return GetBytesCheckSum((PBYTE)m_pdwNumOfBuy, 408);	// m_pdwNumOfBuy - m_dwMaxVolOfSell, 102*4
	}
};
// CStatCounts
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
// 增加一个停牌消息结构 [2/19/2013 frantqian]
struct CSuspendInfo
{
	DWORD m_dwInitDay;	//停牌信息初始化时间
	BOOL m_SuspendType; //停牌类型：FALSE：未停牌，true：停牌
	char m_SuspendInfo[LEN_SUSPENDINFO]; //停牌原因 I

	CSuspendInfo()
	{
		ZeroMemory(this, sizeof(CSuspendInfo));
	}

	void clear()
	{
		ZeroMemory(this, sizeof(CSuspendInfo));
	}
};


//////////////////////////////////////////////////////////////////////////
class CGoods;

//////////////////////////////////////////////////////////////////////
// CQueueMatrix
class CQueueMatrix  
{
public:
	CQueueMatrix();
	virtual ~CQueueMatrix();

	void Clear();

	void Read(CBuffer&);
	void Write(CBuffer&);
	void Write2OldStock(CBuffer&);

public:
	CGoods* m_pGoods;					// 股票代码
	DWORD m_dwCount;					// 变化计数

	CHisOrderQueueSArray	m_saHisOrderBuyArray;	// 委托买队列矩阵
	DWORD					m_dwDynamicTimeBuy;

	CHisOrderQueueSArray	m_saHisOrderSellArray;	// 委托卖队列矩阵
	DWORD					m_dwDynamicTimeSell;

	COrderCounts m_ocSumOrder;			// 累计成交委托单统计
	COrderCounts m_ocSumTrade;			// 累计每笔统计

	CStatCounts m_scSumOrder;			// 累计成交委托单统计
	CStatCounts m_scSumTrade;			// 累计每笔统计

	char m_cHisOrderTime; //// CHisOrderQueue 是否需要传时间 time

	BYTE GetCheckSum(BYTE cBuySell, BOOL bActiveOnly);

//	RWLock m_MatrixLock;
};
// CQueueMatrix
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// dynaCompress

struct CDynamicValue
{
	DWORD		m_dwCountValue;
	DWORD		m_dwDate;
	DWORD		m_dwTime;

	DWORD		m_dwOpen;
	DWORD		m_dwHigh;
	DWORD		m_dwLow;
	DWORD		m_dwPrice;

	XInt32		m_xVolume;
	XInt32		m_xAmount;
	DWORD		m_dwTradeNum;
	XInt32		m_xOpenInterest;	// 持仓量

	BYTE		m_cNumBuy;
	BYTE		m_cNumSell;
	DWORD		m_pdwMMP[20];
	XInt32		m_pxMMPVol[20];

	DWORD		m_dwPBuy;
	XInt32		m_xVBuy;
	DWORD		m_dwPSell;
	XInt32		m_xVSell;

	XInt32		m_xNeiPan;	// 内盘
	XInt32		m_xCurVol;	// 现手
	BYTE		m_cCurVol;	// 现手方向 -1 0 1
	XInt32		m_xCurOI;	// 持仓量变化

	DWORD		m_dwPrice5;	// 5分钟前的价格
	int			m_nStrong;	// 强弱(对指数的影响，有正负)

	COrderCounts m_ocSumOrder;			// 累计成交委托单统计 (s m l xl)
	COrderCounts m_ocSumTrade;			// 累计每笔统计 (s m l xl)

	DWORD		m_dwNorminal;		// 按盘
	//Steven New Begin
	DWORD       m_dwAvePrice;   // 均价
	//Steven New End

	BYTE		m_cVirtual;		// 1: 集合竞价

	CDynamicValue()
	{
		ZeroMemory(this, sizeof(CDynamicValue));
	}

	//Steven New Begin
	BYTE GetDynaChecksumOld(BOOL bIsIndex, BOOL bIsSimple);
//	BYTE GetChecksumH();
//	BYTE GetChecksumQH(BOOL bNeedMMP, BOOL bLevel2);
	BYTE GetChecksumPush(BOOL bQH, WORD wVersion);

//	BOOL IsSimple();	// 集合竞价

	void CreateOldDynamic(CDynamicValue* pDyna, int nDiv, int nDivOld);

	void Multi(DWORD dwDiv);
	void Div(DWORD dwDiv);
	BYTE GetChecksum6(BOOL bNeedMMP);

	void CheckMMP(int nNum);
	void DeletePV();
	void DeleteOC();
	//Steven New End
	BYTE GetChecksum(BOOL bNeedMMP, BOOL bNew, BOOL bLevel2, BOOL bPV)
	{
		BYTE cCheckSum = 0;

		DWORD* pdw = &m_dwTime;
		for (int i=0; i<8; i++)
			cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

		if (bNeedMMP)
		{
			int NUM_OF_MMP;
			if (bLevel2)
				NUM_OF_MMP = 10;
			else
				NUM_OF_MMP = 5;
			for (int i=10-NUM_OF_MMP; i<10+NUM_OF_MMP; i++)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwMMP+i), 4);
				cCheckSum += GetBytesCheckSum((PBYTE)(m_pxMMPVol+i), 4);
			}
		}

		if (bPV)
		{
			pdw = &m_dwPBuy;
			for (int i=0; i<4; i++)
				cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);
		}

		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xNeiPan), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xCurVol), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_cCurVol), 1);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwPrice5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_nStrong), 4);

		if (bLevel2)
		{
			cCheckSum += m_ocSumOrder.GetChecksum(bNew, TRUE);
			cCheckSum += m_ocSumTrade.GetChecksum(bNew, FALSE);
		}

		return cCheckSum;
	}

	BYTE GetChecksumH()
	{
		int i;
		BYTE cCheckSum = 0;

		DWORD* pdw = &m_dwTime;
		for (i=0; i<8; i++)
			cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

		cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwNorminal), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwMMP+BUY1), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwMMP+SELL1), 4);

		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xNeiPan), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xCurVol), 4);

		cCheckSum += GetBytesCheckSum((PBYTE)(&m_cCurVol), 1);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwPrice5), 4);

		return cCheckSum;
	}

	BYTE GetChecksumQH(BOOL bNeedMMP, BOOL bLevel2)
	{
		BYTE cCheckSum = 0;

		DWORD* pdw = &m_dwTime;

		for (int i=0; i<8; i++)
			cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xOpenInterest), 4);

		if (bNeedMMP)
		{
			int NUM_OF_MMP;
			if (bLevel2)
				NUM_OF_MMP = 5;
			else
				NUM_OF_MMP = 1;
			for (int i=10-NUM_OF_MMP; i<10+NUM_OF_MMP; i++)
			{
				cCheckSum += GetBytesCheckSum((PBYTE)(m_pdwMMP+i), 4);
				cCheckSum += GetBytesCheckSum((PBYTE)(m_pxMMPVol+i), 4);
			}
		}

		pdw = &m_dwPBuy;
		for (int i=0; i<6; i++)
			cCheckSum += GetBytesCheckSum((PBYTE)(pdw+i), 4);

		cCheckSum += GetBytesCheckSum((PBYTE)(&m_cCurVol), 1);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_xCurOI), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_dwPrice5), 4);
		cCheckSum += GetBytesCheckSum((PBYTE)(&m_nStrong), 4);

		return cCheckSum;
	}

	BOOL IsSimple()	// 集合竞价
	{
		return	m_xVolume.m_nBase==0 && m_dwPrice>0 && 
			(m_pxMMPVol[9]==m_pxMMPVol[10] && m_pxMMPVol[9]>0) && 
			(m_pxMMPVol[8].m_nBase==0 || m_pxMMPVol[11].m_nBase==0);
	}
};		// MMP5:33*4=132+17bytes; MMP10:53*4=212+17bytes

const int NUMOF_DYNAS = 5;

// dynaCompress
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// CIndex
class CIndex
{
public:
	CIndex();

	void InitDay();

	void Read(CBuffer& buf);
	void Write(CBuffer& buf);

public:
	DWORD m_dwGroup;

	char m_cRD;
	int m_plRD[10];

	WORD m_wRise;
	WORD m_wEqual;
	WORD m_wFall;

	WORD m_wRN;
	WORD m_wDN;
	WORD m_wRD;
};
// CIndex


//板块指数 专有
struct CGroupData
{
	CGroupData()
	{
		ZeroMemory(this, sizeof(CGroupData));
	}

	DWORD m_dwNumAll;		// 股票数量
	DWORD m_dwNumRise;		// 上涨家数
	DWORD m_dwNumFall;		// 下跌家数

	DWORD m_dwID_QS;		// 当日强势股
	DWORD m_dwID_QS5;		// 5日强势股
	DWORD m_dwID_RS;		// 当日弱势股
	DWORD m_dwID_RS5;		// 5日弱势股

	int m_nStrongSH;		// 对沪指的贡献程度
	int m_nStrongSZ;		// 对深指的贡献程度

	DWORD m_dwAvePrice;		// 平均股价
	XInt32 m_xAveSY;		// 平均每股收益
	XInt32 m_xAveGJJ;		// 平均每股公积金
	XInt32 m_xAveJZC;		// 平均每股净资产
	XInt32 m_xAveSYL;		// 平均市盈率
	XInt32 m_xAveSJL;		// 平均市净率
};
//板块指数 专有


//////////////////////////////////////////////////////////////////////
// 交易时段数	max==8
struct CTradeSession
{
	CTradeSession()
	{
		ZeroMemory(this, sizeof(CTradeSession));
	}

	char m_cNum;
	WORD m_pwTimeBegin[MAX_TRADING_SESSION];
	WORD m_pwTimeEnd[MAX_TRADING_SESSION];

public:
	BOOL Read(CBuffer& buf, WORD /*wVersion*/ = 0)
	{
		BYTE cNum = buf.ReadChar();

		if (cNum<0 || cNum>MAX_TRADING_SESSION)
		{
			m_cNum = 0;
			return FALSE;
		}

		m_cNum = cNum;
		for (BYTE j=0; j<m_cNum; j++)
		{
			m_pwTimeBegin[j] = buf.ReadShort();
			m_pwTimeEnd[j] = buf.ReadShort();
		}

		return TRUE;
	}

	void Write(CBuffer& buf, WORD /*wVersion*/ = 0)
	{
		BYTE cNum = m_cNum;

		if (cNum<0 || cNum>MAX_TRADING_SESSION)
			cNum = 0;

		buf.WriteChar(cNum);

		for (BYTE j=0; j<cNum; j++)
		{
			buf.WriteShort(m_pwTimeBegin[j]);
			buf.WriteShort(m_pwTimeEnd[j]);
		}
	}
};

// 分钟统计数据
struct CMinStatData
{
	CMinStatData(){
		Clear();
	}
	void Clear(){ZeroMemory(this,sizeof(*this));}
	XInt32 m_xAmtOfSuperBuy5;//5分钟买单额 
	XInt32 m_xAmtOfSuperSell5;//5分钟卖单额
	XInt32 m_xVolOfSuperBuy5;//5分钟买单量
	XInt32 m_xVolOfSuperSell5;//5分钟卖单量
	DWORD m_dwVolRateOfSB5;		//5 分钟主力买单比率 (5分钟超单+大单买单量)/5分钟总成交量
	DWORD m_dwVolRateOfSBSell5; //5 分钟主力卖单比率 (5分钟超单+大单卖单量)/5分钟总成交量
};

// 日统计数据
struct CDayStatData
{
	CDayStatData(){
		Clear();
	}
	void Clear(){ZeroMemory(this,sizeof(*this));}
	XInt32	m_xAmtOf1Super;		//机构昨日净买
	XInt32	m_xAmtOf4Super;		//机构前4日净买量 算5日净买，下面类推
	XInt32	m_xAmtOf9Super;
	BYTE	m_bJGZC4;			//机构前4日增仓天数，算5日增仓
	BYTE	m_bJGZC9;
	BYTE	m_bJGZC19;

	XInt32	m_xAmtOf1Big;		//游资昨日净买
	XInt32	m_xAmtOf4Big;		//游资前4日净买量 算5日净买，下面类推
	XInt32	m_xAmtOf9Big;
	BYTE	m_bYZZC4;			//游资前4日增仓天数，算5日增仓
	BYTE	m_bYZZC9;
	BYTE	m_bYZZC19;
	//2011.4.11新增
	BYTE	m_bJGQM;		// 机构强买个数
	BYTE	m_bDKBY9;		// 多空博弈 //乾坤版
};

//////////////////////////////////////////////////////////////////////
// CGoods
class CData;
//Steven New Begin
struct CHisMinDF;
//class CChannel;

///20120319 sjp
/////////////////////////////////////////////////////////////////////////////

struct CExpValue
{
	short m_sNo;
	XInt32 m_xValue;
};

inline BOOL operator > (CExpValue& first, CExpValue& second)
{
	return first.m_sNo>second.m_sNo;
}

inline BOOL operator == (CExpValue& first, CExpValue& second)
{
	return first.m_sNo==second.m_sNo;
}

inline BOOL operator < (CExpValue& first, CExpValue& second)
{
	return first.m_sNo<second.m_sNo;
}
///20120319 sjp

/////////////////////////////////////////////////////////////////////////////
//Steven New End

//sam copy from pc client begin

struct CStaticOptions
{
	CStaticOptions();

	void Read(CBuffer& buf, WORD wVersion = 1);
	void Write(CBuffer& buf, WORD wVersion = 1);

	CGoodsCode m_gcBaseID;				// 标的证券代码

	CHAR	m_cOptionType;					// C1 若为欧式期权，则本字段为“E”；若为美式期权，则本字段为“A”
	CHAR	m_cCallOrPut;					// 认购认沽 C1: C–认购, P–认沽

	DWORD	m_dwExercisePrice;				// 期权行权价 N11(3) 经过除权除息调整后

	DWORD	m_dwExerciseDate;				// 期权行权日 C8
	DWORD	m_dwDeliveryDate;				// 行权交割日 C8
	DWORD	m_dwExpireDate;					// 期权到期日 C8
	CHAR	m_cUpdateVersion;				// 合约版本号 C1

	INT64	m_hMarginUnit;					// 单位保证金 N16(2)
	WORD	m_wMarginRatioParam1;			// 保证金计算比例参数一 N3 单位：%
	WORD	m_wMarginRatioParam2;			// 保证金计算比例参数二 N3 单位：%

	INT64	m_hLmtOrdMinFloor;				// 单笔限价申报下限 N12
	INT64	m_hLmtOrdMaxFloor;				// 单笔限价申报上限 N12
	INT64	m_hMktOrdMinFloor;				// 单笔市价申报下限 N12
	INT64	m_hMktOrdMaxFloor;				// 单笔市价申报上限 N12

	CHAR	m_pcStatusFlag[8];				// 期权合约状态信息标签 C8， ' '–无定义
											// 第1位: 0–可开仓, 1–限制卖出开仓和买入开仓
											// 第2位: 0–未连续停牌, 1–连续停牌
											// 第3位: 0–未临近到期日, 1–距离到期日不足10个交易日
											// 第4位: 0–近期未做调整, 1–最近10个交易日内合约发生过调整
											// 第5位: A–当日新挂牌合约, E–存续合约, D–当日摘牌合约
											// 按位取值，非0结尾
};

//sam copy from pc client end


class CMyThread;
class CGoods  
{
public:
	CGoods();
	~CGoods();

	DWORD GetID()
	{ 
		return g_GetGoodsID(m_cStation, m_dwNameCode); 
	}
	CString GetName();
	CGoodsCode GetGoodsCode()
	{
		return CGoodsCode(GetID(), m_pcNameCode);
	}

	DWORD GetMinuteTime(BOOL bIncludeVirtual = FALSE);
	CString GetPreName();
	void GetPreName(CStringArray& aPreName);
	CString GetTradeCode();
	CString GetHYName();
    CString GetCode();
    int GetGroup();
	void InitDay();
	void InitDynamic();
	CString GetString();

	void Read(CBuffer& buffer, WORD wVersion);
	void Write(CBuffer& buffer, WORD wVersion);

	void AddBargain(CBargain&);
	void ClearBargain(BOOL bClearFile, DWORD dwDate);
	void ReadBargainParam();
	void WriteBargainParam();

	void AddMinutes(CSortArrayMinute& aMinute);
	//Steven New Begin
///20120327 sjp 解决新股停牌没横线问题
	void AddEmptyMinute();	//CHisCompress::ExpandMinute没收到aMinute，但CData需要AddEmptyMinute
///20120327 sjp 解决新股停牌没横线问题
	//Steven New End
	void ClearMinute(BOOL bClearFile, DWORD dwDate);
	void ReadMinuteParam();
	void WriteMinuteParam();
	void GetMinutes(CSortArrayMinute& aMinute, BOOL bIncludeVirtual);

	void AddDay();
	void GetToday(CDay& day);
	BOOL WantSaveToday(BOOL bSPZY);
	BOOL IsDayGood(CDay& day);	
	
	void ClearDay();

	WORD MinInDay();
	WORD Time2Min(DWORD dwTime, BOOL bIncludeVirtual);
	WORD Min2Pos(WORD wMin, BOOL bIncludeVirtual);
	WORD Pos2Min(WORD wPos, BOOL bIncludeVirtual);
	BOOL InSamePeriod(DWORD dwPrev, DWORD dwNow, WORD wDataType, WORD wPeriod);

	BOOL CrossDay();

	static BOOL IsIndex(DWORD);
	BOOL IsIndex();
	static BOOL IsSHB(DWORD);
	BOOL IsSHB();
	static BOOL IsQZ(DWORD);
	BOOL IsQZ();
	static BOOL IsSHZQ(DWORD);
	BOOL IsSHZQ();
	static BOOL IsSZZQ(DWORD);
	BOOL IsSZZQ();
	BOOL IsHK();
	static BOOL IsGZQH(DWORD dwGoodsID);
	BOOL IsGZQH();
	static BOOL IsIF(DWORD dwGoodsID);
	BOOL IsIF();
	static BOOL IsTF(DWORD dwGoodsID);
	BOOL IsTF();
	static BOOL IsT(DWORD dwGoodsID);
	BOOL IsT();

	BOOL IsIC();
	BOOL IsIH();

	//Steven New Begin
	static BOOL IsSPQH(DWORD dwGoodsID);
	BOOL IsSPQH();
	static BOOL IsQH(DWORD dwGoodsID);
	static BOOL IsQH(char cStation);
	BOOL IsQH();
	//Steven New End
	static BOOL IsWorldIndex(DWORD dwGoodsID);
	BOOL IsWorldIndex();

	static BOOL IsChinaA(DWORD dwGoodsID);		//20110620
	static BOOL IsChinaB(DWORD dwGoodsID);		//20110620
	static BOOL IsChinaJJ(DWORD dwGoodsID);		//20110620
	
	WORD AmountDiv();
	static WORD AmountDiv(DWORD);

	WORD LastMin();

	INT64 TransVol2Hand(INT64 hVolume);	// 股变手

	char Compare(CGoods*, short, char);

	static BOOL IsValueXInt32(short sID);
	INT64 GetValue(short sID);

	void Goods2Group();

	void SaveToday();
	void SaveHisMin(CHisMinDF* pHMDF, DWORD dwDate);

//Data
	void DeleteData();
	//CData* LoadData(WORD wDataType, WORD wPeriod);
	void FreeData(CData*);
	void FreeData(CMyThread* pthDelete);

	CData* LoadData2(WORD wDataType, WORD wPeriod);
	CData* TryLoadData(WORD wDataType, WORD wPeriod);
	CData* TryLoadToday();
//Data

	void NewGoods();

	void SaveBuffer();
	
	//////////////////////////////////////////////////////////////////////////
	BOOL IsJJ();
	BOOL IsZQ();
    double Long2Float( DWORD dwGoodsID, short sID, INT64 hValue );
    INT64 TransVol2Hand( INT64 hVolume, DWORD dwGoodsID );
    double GetFValue( short sID );
	static CString Name2String(char*);
	
	CFDayMobile Day2Mobile(CDay& day);
	CDayMobile DayMobile_F2L(CFDayMobile& fd, char cIndType, char MaExp, char VMAExp, char IndExp);

	// 停牌信息 [3/20/2013 frantqian]
	void CheckSuspendInfo();
	
public:
	int m_rwGoodcount[100];
	//int m_rwGoodcount; //RW计数器 
	//Steven New Begin
	RWLock* m_pwrTrade;
	RWLock* m_pwrPartOrder;
	//Steven New End
	RWLock m_rwGood;
	RWLock m_rwMinute;
	RWLock m_rwBargain;
	RWLock m_rwHisMin;

	// 增加停牌信息结构 [2/19/2013 frantqian]
	CSuspendInfo m_SuspendInfo;

	//1:非6开头的A股，2:板块，3:港股，4:股指期货，5:延时港股，6:环球指数(DM,空数据)
	//7:环球指数(DB) ,8:中国概念股(DB) , 9: 外汇(DB)
	char m_cStation;
	DWORD m_dwNameCode;
	char m_pcNameCode[LEN_STOCKCODE+1];

	DWORD m_dwA_H;		// A股:对应的H股代码
						// H股:对应的A股代码
	int m_nPriceDiv;
	//sam copy from pc client begin
	DWORD m_dwVolDiv;
	//sam copy from pc client end

	//sam copy from pc client begin
	char m_cSecurityType;	//SECURITY_TYPE
	//sam copy from pc client end

	//Steven New Begin
/*
	char m_cDecimal;	//小数位数
	long m_lParValue;	//面值
	long m_lStatus;		//交易状态
	char m_pcBaseName[8];	//基础证券名称
*/

	char m_cTradingStatus;				// 交易状态
										// 2 = 停牌（Trading Halt）
										// 3 = 恢复（Resume）
										// 101 = 首日上市
										// 102 = 增发新股
										// 103 = 正常状态
										// 104 = 上网定价发行
										// 105 = 上网竞价发行
										// 106 = 国债挂牌分销
										// 107 = 长期停牌

	char m_cSecurityProperties;			// 证券属性	
										// N = 正常 
										// S = ST股 
										// P = PT股 
										// T = 代办转让证券 
										// L = 上市开放型基金（LOF）
										// O = 仅揭示净值的开放式基金
										// F = 非交易型开放式基金 
										// E = ETF
										
	CHAR m_pcTradeCode[4];				// 行情状态， ' '–无定义
										// 第1位: S-启动 C-集合竞价 T-连续交易
										//		  B-休市 E-闭市 V-波动性中断 P-临时停牌
										// 第2位: 0-未连续停牌 1-连续停牌
										// 按位取值，非0结尾

	//Steven New End
	char m_cLevel;

	DWORD m_dwDate;
	DWORD m_dwTime;
	DWORD m_dwDateSave;
	
	DWORD GetDateValue();
	DWORD GetTimeValue();

//Static
	char m_pcName[LEN_STOCKNAME];
	char m_pcCode[LEN_STOCKCODE+1];

	short m_sTimeZone;			// 时区 -12*60 <-> +12*60

	CTradeSession m_tradeSession; //m_TS for PC side

	//sam copy from pc client begin
	//attention: pc client removes m_cDecimalPosition field
	char m_cDotShow;		//显示几位小数
	char m_cDotData;		//数据几位小数,缺省3
	//sam copy from pc client end
	char m_cDecimalPosition;	// 小数位数
	//Steven New Begin
	WORD m_wVolInHand;

	char m_cPriceDiv;
	DWORD m_dwPriceDiv;
	//Steven New End

	CString m_strEName;		// 英文名称
	char m_pcPreName[4];	// 名称前缀
	char m_cTradeCode;		// 交易状态
	DWORD m_dwCountStatus;

	DWORD m_dwClose;

	XInt32 m_xLTG;			// 流通股数
	XInt32 m_xZFX;			// 总发行量

	DWORD m_dwPriceZT;		// 涨停价
	DWORD m_dwPriceDT;		// 跌停价

	DWORD m_dwPreSettlement;	// 昨结算
	XInt32 m_xPreOpenInterest;	// 昨持仓量

	DWORD m_dwSettlement;	// 结算
	
	//Calc
	DWORD m_dwFirstDate;	// 上市日
	DWORD m_dwLastDate;		//对于国债预发行产品，为最后交易日期

	DWORD m_dwPClose;		// 前日收盘价(算昨涨跌幅)
	DWORD m_dwWClose;		// 上周收盘价(算周涨跌幅)
	DWORD m_dw5Close;		// 4日前收盘价(算5日涨跌幅)

	XInt32 m_xAveVol5;		// 5日均量(算量比)
	XInt32 m_xSumVol4;		// 前4日成交量的和(算5日换手)
	XInt32 m_xSumAmt4;		// 前4日成交额的和(算5日净买占比)

	XInt32 m_xSumBigVol4;	// 前4日特大单净买量的和(算5日大单比率)
	XInt32 m_xSumBigAmt4;	// 前4日特大单净买的和(算5日大额)

	WORD m_wGroupHY;		// 行业板块ID
	WORD m_wGroupDQ;		// 地区板块ID
	CWordArray m_aGroupGN;	// 概念

	BYTE m_bZLZC4;			// 前4日主力增仓个数(算5日增仓榜)
	BYTE m_bZLZC9;			// 前9日主力增仓个数(算10日增仓榜)
	BYTE m_bZLZC19;			// 前19日主力增仓个数(算20日增仓榜)
	BYTE m_bZLQM;			// 前19日主力强买个数(算主力强买榜)
	DWORD m_dwClose9;		// 9日前收盘价(算10日涨跌幅)
	DWORD m_dwClose19;		// 19日前收盘价(算20日涨跌幅)
	//Calc

	// 增加股票52周高低点数据 [2/26/2013 frantqian]
	DWORD m_dwWeekHigh52;	//52周高点
	DWORD m_dwWeekLow52;	//52周低点
	DWORD m_dwCalcTime;		//计算日期，每日算一次

//Static

//重点指数
	DWORD m_dwI_Num;		// 样本个数
	DWORD m_dwI_Close;		// 收盘点位(3)
	DWORD m_dwI_Ave;		// 样本均价(3)
	DWORD m_dwI_Amount;		// 成交金额(2) 亿元
	DWORD m_dwI_AveGB;		// 样本平均股本(2) 亿股
	DWORD m_dwI_SumSZ;		// 总市值(2) 万亿股
	DWORD m_dwI_Percent;	// 占比%(2)
	DWORD m_dwI_SYL;		// 静态市盈率
	DWORD m_dwI_JBBS;		// 级别标识
//重点指数

//Dynamic
	DWORD m_dwNorminal;		// 按盘

	DWORD m_dwOpen;			// 开盘
	DWORD m_dwHigh;			// 最高
	DWORD m_dwLow;			// 最低
	DWORD m_dwPrice;		// 成交

	XInt32 m_xVolume;		// 成交量
	XInt32 m_xAmount;		// 成交额

	DWORD m_dwTradeNum;		// 成交笔数
	XInt32 m_xOpenInterest;	// 持仓量

	DWORD m_pdwMMP[20];		// 卖10-买10价
	XInt32 m_pxMMPVol[20];	// 卖10-买10价量

	DWORD m_dwBuyPrice;		// 买盘均价
	DWORD m_dwSellPrice;	// 卖盘均价

	XInt32 m_xBuyVol;		// 买盘总量
	XInt32 m_xSellVol;		// 卖盘总量
	//Calc
	XInt32 m_xNeiPan;		// 内盘
	XInt32 m_xCurVol;		// 现手
	char m_cCurVol;			// 现手方向 -1 0 1
	XInt32 m_xCurOI;		// 持仓量变化

	DWORD m_dwPrice5;		// 5分钟前的价格
	//Calc

	//sam copy from pc client begin
	DWORD m_dwAuctionPrice;
	INT64 m_hAuctionQty;
	//sam copy from pc client end

	int m_nStrong;			// 强弱(对指数的影响)
	int m_nBKStrong;		//对行业板块指数的影响

	// 股票评级 [9/3/2012 frantqian]
	std::string	m_goodsScore;
//Dynamic

	CDayStatData m_DayStat;	//日线统计数据 

// dynaCompress
	CDynamicValue m_aDynamicValue[NUMOF_DYNAS];

	int GetPriceDiv2();

	int GetPriceDivide();
	int GetPriceDivideOld();
	BOOL IsVirtual();	// 集合竞价

	void SaveNewDynamicValue();
	void SaveDataToDynamicValue(CDynamicValue& dyna);
	void LoadDataFromDynamicValue(CDynamicValue& dyna);

	int ExpandDynamicValue(CBitStream& stream);
	int ExpandDynamicValue6(CBitStream& stream);

	int CompressQuote(CBitStream& stream, DWORD dwLastCount, DWORD dwCount, BOOL bNeedMMP, BOOL bNew);

// dynaCompress

	char* m_pcBufferBase;
	void SetBufferBase(PBYTE pcBase);

//Bargain
	char* m_pcBargainParam;
	CBargain* m_pBargainBuffer;
	WORD m_wBargainBuffer;
	WORD m_wBargain;

	DWORD m_dwBargainTotal;
	BYTE m_cBargainCheckSum;
	DWORD m_dwBargainCheckSum;
	DWORD m_dwDateBargain;
//Bargain

// Minute
	char* m_pcMinuteParam;
	CMinute* m_pMinuteBuffer;
	WORD m_wMinuteBuffer;
	WORD m_wMinute;
	WORD m_wMin1_s;
	DWORD m_dwMin30_s;
	DWORD m_dwMin60_s;
	DWORD m_dwDay_s;
	DWORD m_dwWeek_s;
	DWORD m_dwMonth_s;

	WORD m_wMinuteInDisk;
	WORD m_wLastMinInDisk;
	BYTE m_cMinuteCheckSumInDisk;
	DWORD m_dwMinuteCheckSumInDisk;
	DWORD m_dwDateMinute;
// Minute

	CQueueMatrix m_matrix;

	DWORD m_dwCountStatic;
	DWORD m_dwCountDynamic;
	DWORD m_dwCountMinute;
	DWORD m_dwCountPartOrder;

	char m_cGroup;
	//DWORD m_dwGroup;
	UINT64 m_hMaskGroup;

	void SetGroupMask(UINT64 mask){m_hMaskGroup = m_hMaskGroup|mask;};

	BOOL IsBad();

//基本面
	CGoodsJBM m_jbm;
	CGoodsDPTJ m_dptj;
	void LoadJBM();
//基本面

//主力增仓榜/主力强买榜
	int m_nOldZLZC10;
	int m_nOldZLQM;

	void CheckMainTrack();
//主力增仓榜/主力强买榜

	char m_cPool1;
	DWORD m_dwPricePool1;
	DWORD m_dwDatePool1;
	DWORD m_dwPricePool1_2;
	DWORD m_dwDatePool1_2;
	DWORD m_dwHighPool1_2;
	DWORD m_dwCountPool1;

//CPX
	DWORD m_dwCountCPX;

	int m_nCPX_Day;
	DWORD m_dwCPX_Close;

	int m_nCPX_Min60;	//20110722

	int m_nCPX_Month;	//20110722
	int m_nCPX_Week;	//20110722

	int m_nCPX_Min1;	//20110722
	int m_nCPX_Min5;	//20110722
	int m_nCPX_Min15;	//20110722
	int m_nCPX_Min30;	//20110722
//CPX

//二号池
	DWORD m_dwCountPool2;

	char m_cPool2;
	DWORD m_dwDatePool2;
	DWORD m_dwPricePool2;
	DWORD m_dwDatePool2_2;
	DWORD m_dwPricePool2_2;
	DWORD m_dwHighPool2_2;
//二号池

	CString m_strPreName;
	CString m_strHYName;

//Index
	CIndex* m_pIndex;
//Index

	CXGStatData m_XGStat;	//快速选股用的数据

	BOOL CheckVolume(XInt32 xVolume);
	
	CGroupData m_GroupValue;

	XInt32 m_xSZ;			// 市值		(百万元)
	XInt32 m_xLTSZ;			// 流通市值 (百万元)

	//计算checksum
	DWORD m_dwCheckSum;//不缓存该值，每次必须从服务器同步，高2字节为DM收盘总校验和，第3B本地当日校验和(需要收盘),第4B为DM当日校验和
	DWORD m_dwCheckCnt;//
	DWORD  m_dwCheckOK; //校验是否成功

	DWORD m_dwSysAlert;//鹰眼看盘 20110907

	DWORD m_dwXGStatus;//超级资金趋势状态 快速选股用

	void CreateData();
	void CreateWR();

	// Y版动态DAT文件处理 [11/7/2012 frantqian]
	void	LoadDayFromSpecialFile(void* pDF);

	// 好股计算股票52周高低点 [2/26/2013 frantqian]
	void	CalcWeek52Info();

private:
	RWLock* GetWR(WORD wDataType);
	CData* CreateData(WORD wDataType, WORD wPeriod, bool bGuard = FALSE);
	CData** GetDataPtr(WORD wDataType, WORD wPeriod);
	BOOL CheckFree(WORD wDataType, WORD wPeriod);

	CData* m_pDataDay;
	CData* m_pDataMin30;
	CData* m_pDataMin5;
	CData* m_pDataMin1;

	CData* m_pDataMinute0;
	CData* m_pDataMinute1;

	CData* m_pDataBargain;

	CData* m_pDataHisMin;

///20120319 sjp
public:
	CSortArray<CExpValue> m_aExpValue;
	//sam copy from pc client begin
	CStaticOptions m_Options;
	//sam copy from pc client end
public:
	static volatile int s_nCacheDay;
	static volatile int s_nCacheMin30;
	static volatile int s_nCacheMin5;
	static volatile int s_nCacheMin1;
	static volatile int s_nCacheMinute0;
	static volatile int s_nCacheMinute1;
	static volatile int s_nCacheBargain;
	static volatile int s_nCacheHisMin;
	static void TraceCache();
	//////////////////////////////////////////////////////////////////////////
	bool IsClosed(DWORD dwHHMMSS);		// true:收盘时间
	static bool IsClosed(char cStation, DWORD dwHHMMSS);

	bool InMobileGroup(char cGroup);
	int LoadDay(CArrayFDayMobile& aFDay, WORD wDataType, WORD wPeriod, char isNeedCQ = 0);// 除权方式 0:下除权，1：上除权，2：不除权 [11/9/2012 frantqian]
	// LoadDay优化版本，多传入需要K线数量，只对这些数量的K线进行后续的除权，合并等计算 [11/22/2013 frantqian]
	int LoadDay_op(CArrayFDayMobile& aFDay, WORD wDataType, WORD wPeriod, char isNeedCQ,DWORD dwKNum, std::map<DWORD, DWORD>* pMapHSL = NULL);// 除权方式 0:下除权，1：上除权，2：不除权 [11/9/2012 frantqian]
	CData* TryLoadData2(WORD wDataType, WORD wPeriod);

    int LoadToday(CArrayFDayMobile& aFDay);

//拼音
	CStringArray m_aPY;

	void MakePY();
	void AddPY(char);
	void AddPY(CByteArray&);
//拼音

	double GetZDTBL();

	bool QuerySystemGroup(CBuffer& buf);
	bool QueryDefineGroup(CBuffer& buf);
	void AddBKInfo(int BKType, const std::string& strClass, const std::string& strGroup);
	void ClearBKInfo();

	void GetCurDayNews(DWORD& dwTime, INT64& llNewsID);

	XInt32 GetHisLTG(CGoodsHisGB& hisGB, DWORD dwDate,XInt32 defValue);

private:
	static RWLock m_rwBK;
	std::map<int, std::string, std::greater<int> > m_mapBK;
	std::map<int, std::set<std::string>, std::greater<int> > m_mapSubBK;
private:
	CTradeSession* CheckTS(BOOL bIncludeVirtual = FALSE);
};
// CGoodsF
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////
struct CZDF_ZJJL
{
	CZDF_ZJJL()
	{
		ZeroMemory(this, sizeof(CZDF_ZJJL));
	}

	DWORD m_dwID;
	int m_nZDF;
	XInt32 m_xZJJL;
};

inline BOOL operator > (CZDF_ZJJL& first, CZDF_ZJJL& second)
{
	if (first.m_xZJJL > second.m_xZJJL)
		return TRUE;
	else if (first.m_xZJJL < second.m_xZJJL)
		return FALSE;
	else
		return first.m_dwID > second.m_dwID;
}

inline BOOL operator == (CZDF_ZJJL& first, CZDF_ZJJL& second)
{
	return first.m_nZDF == second.m_nZDF && first.m_dwID == second.m_dwID;
}

inline BOOL operator < (CZDF_ZJJL& first, CZDF_ZJJL& second)
{
	if (first.m_xZJJL < second.m_xZJJL)
		return TRUE;
	else if (first.m_xZJJL > second.m_xZJJL)
		return FALSE;
	else
		return first.m_dwID < second.m_dwID;
}

//////////////////////////////////////////////////////////////////////

class CHisBK
{
public:
	CHisBK()
	{
		ZeroMemory(this, sizeof(CHisBK));
	}

public:
	DWORD m_dwDate;
	XInt32 m_xZJJL;
	CZDF_ZJJL m_Top[3];
	CZDF_ZJJL m_Bottom[3];
};

inline BOOL operator > (CHisBK& first, CHisBK& second)
{
	return first.m_dwDate > second.m_dwDate;
}

inline BOOL operator == (CHisBK& first, CHisBK& second)
{
	return first.m_dwDate == second.m_dwDate;
}

inline BOOL operator < (CHisBK& first, CHisBK& second)
{
	return first.m_dwDate < second.m_dwDate;
}

//////////////////////////////////////////////////////////////////////

class CHisBK_Calc : public CHisBK
{
public:
	const CHisBK_Calc& operator=(const CHisBK_Calc& src)
	{
		m_dwDate = src.m_dwDate;
		m_xZJJL = src.m_xZJJL;
		CopyMemory(m_Top, src.m_Top, sizeof(CZDF_ZJJL)*3);
		CopyMemory(m_Bottom, src.m_Bottom, sizeof(CZDF_ZJJL)*3);
		m_aZZ.Copy(src.m_aZZ);

		return *this;
	}

public:
	CSortArray<CZDF_ZJJL> m_aZZ;
};

struct CZyhg
{
    DWORD id;
    DWORD dayType;
    DWORD nhsyl;
};

class CZyhgHandle
{
public:
	CZyhgHandle();
    std::vector<CZyhg> GetFullList();           // 所有产品列表，按天数品种排序
    std::vector<CZyhg> GetBestList(bool bLt1w);           // 精选产品列表, 见 "精选列表逻辑.png"
private:
    void fillNhsyl();
	static bool compNhsyl(const CZyhg &s1, const CZyhg &s2);
private:
    std::vector<CZyhg> _zyhglist;
};


#endif
