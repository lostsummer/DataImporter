/** *************************************************************************
* MDS
* Copyright 2009 by emoney
* All rights reserved.
*
** ************************************************************************/
/**@file AutoMutex.h
* @brief  
*
* $Author: panlishen$
* $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
* $Revision$
*/
/* *********************************************************************** */
#ifndef _NONCOPYABLE_H_
#define _NONCOPYABLE_H_

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
# pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)


class Noncopyable
{
public:
	Noncopyable() {};
	~Noncopyable() {};

public:
	Noncopyable(const Noncopyable&);

	const Noncopyable & operator=(const Noncopyable &);
};

#endif




