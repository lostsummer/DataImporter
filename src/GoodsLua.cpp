#include "GoodsLua.h"

CRedisImporter::CRedisImporter()
{
    //m_cScriptName = "sender.lua";
    m_pLuaState = lua_open();
    luaL_openlibs(m_pLuaState);
    lua_tinker::dofile(m_pLuaState, "sender.lua");
}

CRedisImporter::~CRedisImporter()
{
    lua_close(m_pLuaState);
}

bool CRedisImporter::WriteNameCode(CGoods * pGoods)
{
    if (!m_pLuaState)
        return false;

     lua_tinker::table goods = lua_tinker::get<lua_tinker::table>(m_pLuaState, "goods");
     goods.set("emcode",pGoods->GetID());
     goods.set("code", pGoods->GetCode().GetData());
     goods.set("name", pGoods->m_pcName);
     //goods.set("group", pGoods->m_cGroup);
     lua_tinker::table pinyin = goods.get<lua_tinker::table>("pinyin");
     int size_arr = pGoods->m_aPY.GetSize();
     for (int i=0; i < size_arr; i++)
     {
         pinyin.set(i + 1, pGoods->m_aPY[i].GetData());
     }
     

     return lua_tinker::call<bool>(m_pLuaState, "writecodemap");
}

bool CRedisImporter::WriteSpec()
{
    if (!m_pLuaState)
        return false;

     lua_tinker::call<void>(m_pLuaState, "writecategory");
     lua_tinker::call<void>(m_pLuaState, "writestation");
     return lua_tinker::call<bool>(m_pLuaState, "writequotespec");
}

bool CRedisImporter::WriteCodeInfo()
{
    if (!m_pLuaState)
        return false;

    return lua_tinker::call<bool>(m_pLuaState, "writecodeinfo");
}

void CRedisImporter::WriteKline(CGoods * pGoods, CFDayMobile & day, const char * pcDataType)
{
    if (!m_pLuaState)
        return;

    lua_tinker::table goods = lua_tinker::get<lua_tinker::table>(m_pLuaState, "goods");

    DWORD time = day.m_dwTime;
    double open = day.m_fOpen;
    double high = day.m_fHigh;
    double low = day.m_fLow;
    double close = day.m_fClose;
    double volume = day.m_fVolume;
    double amount = day.m_fAmount;
    goods.set("emcode",pGoods->GetID());
    goods.set("code", pGoods->m_pcNameCode);
    //goods.set("group", pGoods->m_cGroup);
    goods.set("time", time);
    goods.set("open", open);
    goods.set("high", high);
    goods.set("low", low);
    goods.set("close", close);
    goods.set("volume", volume);
    goods.set("amount", amount);
    lua_tinker::call<void>(m_pLuaState, "writekline", pcDataType);
}

bool CRedisImporter::ExecutePipeline()
{
    if (!m_pLuaState)
        return false;

    return lua_tinker::call<bool>(m_pLuaState, "exepipeline");
}

bool CRedisImporter::WriteQuote(CGoods * pGoods)
{
    if (!m_pLuaState)
        return false;

    lua_tinker::table goods = lua_tinker::get<lua_tinker::table>(m_pLuaState, "goods");

    DWORD time = pGoods->m_dwTime;
    double preClose = pGoods->GetFValue(CLOSE);
    double open = pGoods->GetFValue(OPEN);
    double high = pGoods->GetFValue(HIGH);
    double low = pGoods->GetFValue(LOW);
    double price = pGoods->GetFValue(PRICE);
    double fAve = pGoods->GetFValue(AVEPRICE);
    double fVolume = pGoods->GetFValue(VOLUME);
    double fAmount = pGoods->GetFValue(AMOUNT);
    double fVBuy = pGoods->GetFValue(VBUY);
    double fVSell = pGoods->GetFValue(VSELL);
    double fPBuy = pGoods->GetFValue(PBUY);
    double fPSell = pGoods->GetFValue(PSELL);
    double fTradeNum = pGoods->GetFValue(TRADENUM);
    //double fVTrade = fVolume / fTradeNum;
    double fZT = pGoods->GetFValue(P_ZT);
    double fDT = pGoods->GetFValue(P_DT);
    double fZD = pGoods->GetFValue(ZHANGDIE);
    double fZDF = pGoods->GetFValue(ZDF);
    double fZDF5 = pGoods->GetFValue(ZDF5);
    double fZDF10 = pGoods->GetFValue(ZDF10);
    double fZDF20 = pGoods->GetFValue(ZDF20);
    double fPZDF = pGoods->GetFValue(PZDF);
    double fWZDF = pGoods->GetFValue(WZDF);
    double fZF = pGoods->GetFValue(ZHENFU);
    double fHSL = pGoods->GetFValue(HSL);
    double fHSL5 = pGoods->GetFValue(HSL5);
    double fCurVol = pGoods->GetFValue(CURVOL);
    double fBigAmt = pGoods->GetFValue(BIGAMT);
    double fBigAmt5 = pGoods->GetFValue(BIGAMT5);
    double fDDBL = pGoods->GetFValue(DDBL);
    double fDDBL5 = pGoods->GetFValue(DDBL5);
    double fWP = pGoods->GetFValue(WAIPAN);
    double fNP = pGoods->GetFValue(NEIPAN);
    double fLTG = pGoods->GetFValue(LTG);
    double fLB = pGoods->GetFValue(LIANGBI);
    double fQR = pGoods->GetFValue(STRONG);
    double fSYL = pGoods->GetFValue(TTM_SYL);  // 滚动市盈率，保持和PC版界面相同字段
    double fZLZC5 = pGoods->GetFValue(ZLZC5);
    double fZLZC10 = pGoods->GetFValue(ZLZC10);
    double fZLZC20 = pGoods->GetFValue(ZLZC20);
    double fZLQM = pGoods->GetFValue(ZLQM);
    double fCPXDAY = pGoods->GetFValue(CPX_DAY);
    double fRise = pGoods->GetFValue(RISE);
    double fFall = pGoods->GetFValue(FALL);
    double fEqual = pGoods->GetFValue(EQUAL);
    double fLTSZ = pGoods->GetFValue(LTSZ);
    double fZSZ = pGoods->GetFValue(ZSZ);
    double fPB[10];
    fPB[0] = pGoods->GetFValue(PBUY1);
    fPB[1] = pGoods->GetFValue(PBUY2);
    fPB[2] = pGoods->GetFValue(PBUY3);
    fPB[3] = pGoods->GetFValue(PBUY4);
    fPB[4] = pGoods->GetFValue(PBUY5);
    fPB[5] = pGoods->GetFValue(PBUY6);
    fPB[6] = pGoods->GetFValue(PBUY7);
    fPB[7] = pGoods->GetFValue(PBUY8);
    fPB[8] = pGoods->GetFValue(PBUY9);
    fPB[9] = pGoods->GetFValue(PBUY10);
    double fPS[10];
    fPS[0] = pGoods->GetFValue(PSELL1);
    fPS[1] = pGoods->GetFValue(PSELL2);
    fPS[2] = pGoods->GetFValue(PSELL3);
    fPS[3] = pGoods->GetFValue(PSELL4);
    fPS[4] = pGoods->GetFValue(PSELL5);
    fPS[5] = pGoods->GetFValue(PSELL6);
    fPS[6] = pGoods->GetFValue(PSELL7);
    fPS[7] = pGoods->GetFValue(PSELL8);
    fPS[8] = pGoods->GetFValue(PSELL9);
    fPS[9] = pGoods->GetFValue(PSELL10);
    double fVB[10];
    fVB[0] = pGoods->GetFValue(VBUY1);
    fVB[1] = pGoods->GetFValue(VBUY2);
    fVB[2] = pGoods->GetFValue(VBUY3);
    fVB[3] = pGoods->GetFValue(VBUY4);
    fVB[4] = pGoods->GetFValue(VBUY5);
    fVB[5] = pGoods->GetFValue(VBUY6);
    fVB[6] = pGoods->GetFValue(VBUY7);
    fVB[7] = pGoods->GetFValue(VBUY8);
    fVB[8] = pGoods->GetFValue(VBUY9);
    fVB[9] = pGoods->GetFValue(VBUY10);
    double fVS[10];
    fVS[0] = pGoods->GetFValue(VSELL1);
    fVS[1] = pGoods->GetFValue(VSELL2);
    fVS[2] = pGoods->GetFValue(VSELL3);
    fVS[3] = pGoods->GetFValue(VSELL4);
    fVS[4] = pGoods->GetFValue(VSELL5);
    fVS[5] = pGoods->GetFValue(VSELL6);
    fVS[6] = pGoods->GetFValue(VSELL7);
    fVS[7] = pGoods->GetFValue(VSELL8);
    fVS[8] = pGoods->GetFValue(VSELL9);
    fVS[9] = pGoods->GetFValue(VSELL10);

    goods.set("emcode",pGoods->GetID());
    goods.set("code", pGoods->m_dwNameCode);
    goods.set("name", pGoods->m_pcName);
    //goods.set("group", pGoods->m_cGroup);
    goods.set("time", time);
    goods.set("pclose", preClose);
    goods.set("open", open);
    goods.set("high", high);
    goods.set("low", low);
    goods.set("price", price);
    goods.set("volume", fVolume);
    goods.set("amount", fAmount);
    goods.set("aveprice", fAve);
    goods.set("vbuy", fVBuy);
    goods.set("vsell", fVSell);
    goods.set("pbuy", fPBuy);
    goods.set("psell", fPSell);
    goods.set("tradenum", fTradeNum);
    //goods.set("vtrade", fVTrade);
    goods.set("zt", fZT);
    goods.set("dt", fDT);
    goods.set("zd", fZD);
    goods.set("zdf", fZDF);
    goods.set("zdf5", fZDF5);
    goods.set("zdf10", fZDF10);
    goods.set("zdf20", fZDF20);
    goods.set("pzdf", fPZDF);
    goods.set("wzdf", fWZDF);
    goods.set("zf", fZF);
    goods.set("hsl", fHSL);
    goods.set("hsl5", fHSL5);
    goods.set("curvol", fCurVol);
    goods.set("bigamt", fBigAmt);
    goods.set("bigamt5", fBigAmt5);
    goods.set("ddbl", fDDBL);
    goods.set("ddbl5", fDDBL5);
    goods.set("wp", fWP);
    goods.set("np", fNP);
    goods.set("ltg", fLTG);
    goods.set("lb", fLB);
    goods.set("qr", fQR);
    goods.set("syl", fSYL);
    goods.set("zlzc5", fZLZC5);
    goods.set("zlzc10", fZLZC10);
    goods.set("zlzc20", fZLZC20);
    goods.set("zlqm", fZLQM);
    goods.set("cpxday", fCPXDAY);
    goods.set("rise", fRise);
    goods.set("fall", fFall);
    goods.set("equal", fEqual);
    goods.set("ltsz", fLTSZ);
    goods.set("zsz", fZSZ);
    
    lua_tinker::table pbuy10 = goods.get<lua_tinker::table>("pbuy10");

    for (int i=0; i<10; i++)
    {
        pbuy10.set(i+1, fPB[i]);
    }
    
    lua_tinker::table psell10 = goods.get<lua_tinker::table>("psell10");

    for (int i=0; i<10; i++)
    {
        psell10.set(i+1, fPS[i]);
    }

    lua_tinker::table vbuy10 = goods.get<lua_tinker::table>("vbuy10");

    for (int i=0; i<10; i++)
    {
        vbuy10.set(i+1, fVB[i]);
    }
    
    lua_tinker::table vsell10 = goods.get<lua_tinker::table>("vsell10");

    for (int i=0; i<10; i++)
    {
        vsell10.set(i+1, fVS[i]);
    }

    return lua_tinker::call<bool>(m_pLuaState, "writequote");
}

bool CRedisImporter::ClearKeysInDB(const char * pcKeyPrefix, int dbnum)
{
    return lua_tinker::call<bool>(m_pLuaState, "clearkeysindb", pcKeyPrefix, dbnum);
}

bool CRedisImporter::ClearCQ()
{
    return lua_tinker::call<bool>(m_pLuaState, "clearcq");
}

bool CRedisImporter::ClearKM()
{
    return lua_tinker::call<bool>(m_pLuaState, "clearkm");
}

bool CRedisImporter::ClearKL()
{
    return lua_tinker::call<bool>(m_pLuaState, "clearkl");
}
