#!/bin/bash

if [ $# -lt 1 ]; then
    echo "less arg. debug or release?"
    exit 1
fi

if [ "$1" = "debug" ] || [ "$1" = "release" ]; then
    DIR=$(pwd)
    /usr/bin/docker run --rm  -v $DIR:$DIR --name mds_compile  dockerhub.emoney.cn/centos6_mds sh -c "cd $DIR && make clean && make Mode=$1 -j4 2>&1|tee build.log"
    rm -rf "$1_ct6" 
    mv "$1" "$1_ct6"
else
    echo "wrong arg!"
    exit -1
fi

