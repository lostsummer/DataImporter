#!/bin/bash

HOST=172.31.25.100
PORT=6391
DB=1

R_CONN="redis-cli -h $HOST -p $PORT -n $DB"
R_MIN1_CMD="lrange min1:0600000 -1 -1"
R_CMINFO_CMD="get codemap:info"
R_CMLEN_CMD="hlen codemap:codes"
R_QUOTE_CMD="get quote:0600000"

WATCH_MIN1_CMD="watch \"$R_CONN $R_MIN1_CMD|python -m json.tool\""
WATCH_CMINFO_CMD="watch \"$R_CONN $R_CMINFO_CMD\""
WATCH_CMLEN_CMD="watch \"$R_CONN $R_CMLEN_CMD\""
WATCH_QUOTE_CMD="watch \"$R_CONN $R_QUOTE_CMD|python -m json.tool|grep time\""

SESS=monitor

tmux a -t $SESS
if [ $? -ne 0 ]
then
    tmux new-session -s $SESS -d "$WATCH_MIN1_CMD" \; \
    split-window -h -d "$WATCH_CMINFO_CMD" \; \
    split-window  "$WATCH_CMLEN_CMD" \; \
    select-pane -R \; \
    split-window  "$WATCH_QUOTE_CMD" \; \
    attach
fi

