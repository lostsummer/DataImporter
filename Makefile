###########################################################################
# MDS
# Copyright 2009 by emoney
# All rights reserved.
#
###########################################################################
# @file Makefile
# @brief  
#
# $Author: panlishen$
# $Date: Wed Nov 24 10:13:45 UTC+0800 2009$
# $Revision$
#
###########################################################################

# build sample: make REV=1.50 Mode=release -j8

CXX = g++
SubVersion="const char SubVersion[]=\"SubVersion: $(REV)\""
BuildTime="const char BuildTime[]=\"BuildTime: $(shell TZ="Asia/Shanghai" date  +%Y%m%d-%H:%M:%S)\""

OBJ_PATH = ./obj
SRC_PATH = ./src
DBG_PATH = ./debug
RLS_PATH = ./release
APP_NAME = MDS

INC_PATH = -I $(SRC_PATH) 
LIB_PATH = -L ./lib
LIBS = -pthread -lrt -lstdc++ -lCalcCPX -llua

CFLAGS += -DSUBVERSION=$(SubVersion) -DBUILDTIME=$(BuildTime)
CFLAGS += -Wall 
LDFLAGS += -D_REENTRANT 

ifeq ($(Mode), release)
	CFLAGS += -DNDEBUG
	CFLAGS += -DEM_MOBILE_MAIN
	CFLAGS += -D_POSIX_PTHREAD_H
	CFLAGS += -O2 -rdynamic
	TARGET = $(RLS_PATH)/$(APP_NAME)
	OUT_PATH  = $(RLS_PATH)
else
	CFLAGS += -D_DEBUG
	CFLAGS += -DEM_MOBILE_MAIN
	CFLAGS += -D_POSIX_PTHREAD_H
	CFLAGS += -g
	CFLAGS += -rdynamic
	TARGET = $(DBG_PATH)/$(APP_NAME)
	OUT_PATH = $(DBG_PATH)
endif

CPP_SOURCES = $(foreach d,$(SRC_PATH),$(wildcard $(d)/*.cpp) ) 
CPP_OBJFILES = $(patsubst %.cpp,$(OBJ_PATH)/%.o,$(notdir $(CPP_SOURCES)))

vpath %.cpp $(dir $(CPP_SOURCES))
vpath %.o $(OBJ_PATH)

.PHONY: all clean

all: $(TARGET)
	
$(CPP_OBJFILES): $(OBJ_PATH)/%.o: %.cpp
	@mkdir -p $(OBJ_PATH)
	$(CXX) -c $(CFLAGS) -o $@ $< $(INC_PATH)
	
$(TARGET): $(CPP_OBJFILES)
	@mkdir -p $(OUT_PATH)
	$(CXX) $^ -o $@ $(LIB_PATH) $(LIBS) $(LDFLAGS) 

clean:
	rm -rf $(OBJ_PATH)
