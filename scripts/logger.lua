require "logging.file"
local cfg = require 'config'
local logger = logging.file(cfg.log.dir .. "/%s_LUA.log", "%Y%m%d_%H%M%S", "[%date] [%level] %message\n")
logger:setLevel(cfg.log.level)
return logger
