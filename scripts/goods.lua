require 'common'

local goods={
    emcode = 5512003,
    code = 5512003,
    name = "test",
    time = 0,
    open = 0,       -- 开
    high = 0,       -- 高
    low = 0,        -- 低
    close = 0,      -- 收盘价
    pclose = 0,     -- 昨收盘价
    price = 0,      -- 最新价
    aveprice = 0,   -- 均价
    volume = 0,     -- 总成交量  (总手)
    amount = 0,     -- 总成交额  (金额)
    vbuy = 0,       -- 总买量
    vsell = 0,      -- 总卖量
    pbuy = 0,       -- 委托买入均价 (均买)
    psell = 0,      -- 委托卖出均价 (均卖)
    tradenum = 0,   -- 成交笔数
    --vtrade = 0,     -- 每笔均量     (均笔)
    zt = 0,         -- 涨停
    dt = 0,         -- 跌停
    zd = 0,         -- 涨跌
    zdf = 0,        -- 涨跌幅
    zdf5 = 0,       -- 5日涨跌幅
    zdf10 = 0,      -- 10日涨跌幅
    zdf20 = 0,      -- 20日涨跌幅
    pzdf = 0,       -- 昨日涨跌幅
    wzdf = 0,       -- 周涨跌幅
    zf = 0,         -- 振幅
    hsl = 0,        -- 换手率
    hsl5 = 0,       -- 5日换手率
    curvol = 0,     -- 现手
    bigamt = 0,     -- 主力净买
    bigamt5 = 0,    -- 5日主力净买
    ddbl = 0,       -- 大单比率
    ddbl5 = 0,      -- 5日大单比率
    wp = 0,         -- 外盘
    np = 0,         -- 内盘
    ltg = 0,        -- 流通股数
    lb = 0,         -- 量比
    qr = 0,         -- 强弱
    syl = 0,        -- 市盈率
    zlzc5 = 0,      -- 5日主力增仓
    zlzc10 = 0,     -- 10日主力增仓
    zlzc20 = 0,     -- 20日主力增仓
    zlqm = 0,       -- 主力强买
    cpxday = 0,     -- 日操盘线
    rise = 0,       -- 涨家
    fall = 0,       -- 跌家
    equal = 0,      -- 平家
    ltsz = 0,       -- 流通市值
    zsz = 0,        -- 总市值
    pbuy10 = {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    },
    psell10 = {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    },
    vbuy10 = {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    },
    vsell10 = {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    },
    pinyin = {
    }
}

local stationName = {
    [0] = 'SH',
    [1] = 'SZ',
    [2] = 'BK',
    [3] = 'AH',
    [4] = 'GZ',
    [5] = 'HK',
    [6] = 'WI',
    [7] = 'SP',
    [8] = 'WP',
    [9] = 'US',
    [10] = 'US',
    [11] = 'WH',
    [110] = 'QQ'
}

function goods.codemap(self)
    local n, _ = math.modf(self.emcode/1000000)
    local station = stationName[n] 
    local code = self.code
    local emcode = string.format("%07d", self.emcode)
    local name = gbk2utf8(self.name)
    local pinyin = self.pinyin
    for i = 1, #pinyin do
        if pinyin[i] == "" then
            table.remove(pinyin, i)
        end
    end
    local group = getcategory(self.emcode)
    local codemap = {station=station, code=code,emcode=emcode, name=name, pinyin=pinyin, category={group}}
    return codemap
end

function goods.kline(self)
    local emcode = self.emcode
    local time = self.time
    local open = self.open
    local high = self.high
    local low = self.low
    local close = self.close
    local volume = self.volume
    local amount = self.amount
    local day = {
        time=time,
    open=open,
    high=high,
    low=low,
    close=close,
    volume=volume,
    amount=amount
    }
    return day
end

function goods.quote(self)
    local n, _ = math.modf(self.emcode/1000000)
    local station = stationName[n] 
    local emcode = string.format("%07d", self.emcode)
    local code = string.format("%06d", self.code)
    local name = gbk2utf8(self.name)
    local gd = {
        emcode = emcode,
        station = station,
        code   = code,
        name   = name,
        time   = self.time,
        pclose  = self.pclose,    -- 昨收价
        open   = self.open,
        high   = self.high,
        low    = self.low,
        price = self.price,       -- 最新价
        aveprice = self.aveprice,   -- 均价
        volume = self.volume,     -- 总成交量  (总手)
        amount = self.amount,     -- 总成交额  (金额)
        vbuy   = self.vbuy,       -- 总买量
        vsell  = self.vsell,      -- 总卖量
        pbuy   = self.pbuy,       -- 委托买入均价 (均买)
        psell  = self.psell,      -- 委托卖出均价 (均卖)
        tradenum = self.tradenum,   -- 成交笔数
        zt     = self.zt,         -- 涨停
        dt     = self.dt,         -- 跌停
        zd     = self.zd,         -- 涨跌
        zdf    = self.zdf,        -- 涨跌幅
        zdf5   = self.zdf5,       -- 5日涨跌幅
        zdf10  = self.zdf10,      -- 10日涨跌幅
        zdf20  = self.zdf20,      -- 20日涨跌幅
        pzdf   = self.pzdf,       -- 昨日涨跌幅
        wzdf   = self.wzdf,       -- 周涨跌幅
        zf     = self.zf,         -- 振幅
        hsl    = self.hsl,        -- 换手率
        hsl5   = self.hsl5,       -- 5日换手率
        curvol = self.curvol,     -- 现手
        bigamt = self.bigamt,     -- 主力净买
        bigamt5 = self.bigamt5,    -- 5日主力净买
        ddbl   = self.ddbl,       -- 大单比率
        ddbl5  = self.ddbl5,      -- 5日大单比率
        wp     = self.wp,         -- 外盘
        np     = self.np,         -- 内盘
        ltg    = self.ltg,        -- 流通股数
        lb     = self.lb,         -- 量比
        qr     = self.qr,         -- 强弱
        syl    = self.syl,        -- 市盈率
        zlzc5  = self.zlzc5,      -- 5日主力增仓
        zlzc10 = self.zlzc10,     -- 10日主力增仓
        zlzc20 = self.zlzc20,     -- 20日主力增仓
        zlqm   = self.zlqm,       -- 主力强买
        cpxday = self.cpxday,     -- 日操盘线
        rise   = self.rise,       -- 涨家
        fall   = self.fall,       -- 跌家
        equal  = self.equal,      -- 平家
        ltsz   = self.ltsz,       -- 流通市值
        zsz    = self.zsz,        -- 总市值
        pbuy10 = self.pbuy10,
        psell10 = self.psell10,
        vbuy10 = self.vbuy10,
        vsell10 = self.vsell10,
    }
    return gd
end

return goods
