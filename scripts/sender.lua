
require 'common'
require 'spec'
local cfg = require 'config'
local logger = require 'logger'
goods = require 'goods'
json = require 'json'
redis = require 'redis'

local cli = redis.connect(cfg.redis.host, cfg.redis.port)

function checkconnection()
    if cli == nil then
        s, cli = pcall(redis.connect, cfg.redis.host, cfg.redis.port)
        return s
    else
        s, r = pcall(cli.ping, cli)
        if s and r then
            return true
        else
            s, cli = pcall(redis.connect, cfg.redis.host, cfg.redis.port)
            return s
        end
    end
end

pminparam={}

function inblock()
    local group = getcategory(goods.emcode)
    return in_array(group, cfg.block_group)
end

function writecategory()
    local k = 'codemap:category'
    local v = json.encode(codemap_category)
    local s, rep = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.cqdb)
                             p:set(k,v)
                         end)
    if s == false then
        logger:error("fail to write codemap:category")
        if checkconnection() then
            return writecategory()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        return true
    end
end

function writestation()
    local k = 'codemap:station'
    local v = json.encode(codemap_station)
    local s, rep = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.cqdb)
                             p:set(k,v)
                         end)
    if s == false then
        logger:error("fail to write codemap:station")
        if checkconnection() then
            return writestation()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        return true
    end
end

function writequotespec()
    local k = 'quote:spec'
    local v = json.encode(quote_spec)
    local s, rep = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.cqdb)
                             p:set(k,v)
                         end)
    if s == false then
        logger:error("fail to write quote:spec")
        if checkconnection() then
            return writequotespec()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        return true
    end
end

function writecodemap()
    if inblock() then
        return
    end
    local mk = 'codemap:codes'
    local emcode = string.format("%07d", goods.emcode)
    local sk = emcode
    local v = json.encode(goods:codemap())
    local s, rep = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.cqdb)
                             p:hset(mk, sk, v)
                         end)
    if s == false then
        logger:error("fail to write codemap:codes " .. sk)
        if checkconnection() then
            return writecodemap()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        goods.pinyin = {}
        return true
    end
end

function writecodeinfo()
    local k = "codemap:info"
    local v = json.encode({time = os.date("%Y-%m-%d %H:%M:%S")})
    local s, rep = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.cqdb)
                             p:set(k,v)
                         end)
    if s == false then
        logger:error("fail to write codemap:info ")
        if checkconnection() then
            return writecodeinfo()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        return true
    end
end

function exepipeline()
    local s, rep = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.kldb)
                             for _, param in ipairs(pminparam) do
                                 p:rpush(param.k, param.v)
                             end
                         end)
    if s == false then
        logger:error("fail to exepipeline ")
        if checkconnection() then
            return exepipeline()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        pminparam = {}
        return true
    end
end


function writekline(datatype)
    if inblock() then
        return
    end
    local emcode = string.format("%07d", goods.emcode)
    local k = datatype .. ":" .. emcode
    local data = goods:kline()
    if datatype == "min1" or datatype == "min30" or datatype == "min60" then
        data.time = 200000000000 + data.time
    end

    local v = json.encode(data) 
    table.insert(pminparam, {k=k, v=v})
end

function clearkeysindb(k_prefix, n)
    if not checkconnection() then
        logger:fatal("lost redis connectiont!")
        return false
    end
    local s, rep = pcall(cli.select, cli, n)
    if not s then
        return false
    end
    s, rep = pcall(cli.keys,
                    cli,
                    k_prefix .. ':*')
    if not s then
        return false
    end
    local keys = rep
    if #keys > 0 then
        cli:del(keys)
    end
    logger:info("clear " .. k_prefix .. " in db" .. n)
    return true
end

function clearkm()
    clearkeysindb("min1", cfg.redis.kldb)
end

function clearkl()
    clearkeysindb("min30", cfg.redis.kldb)
    clearkeysindb("min60", cfg.redis.kldb)
    clearkeysindb("day", cfg.redis.kldb)
    clearkeysindb("week", cfg.redis.kldb)
    clearkeysindb("month", cfg.redis.kldb)
end

function clearcq()
    clearkeysindb("codemap", cfg.redis.cqdb)
    clearkeysindb("quote", cfg.redis.cqdb)
end

function writequote()
    if inblock() then
        return
    end
    local emcode = string.format("%07d", goods.emcode)
    local k = "quote:" .. emcode
    local v = json.encode(goods:quote())
    local s, cli = pcall(cli.pipeline,
                         cli,
                         function(p)
                             p:select(cfg.redis.cqdb)
                             p:set(k,v)
                         end)
    if s == false then
        logger:error("fail to writequote " .. k)
        if checkconnection() then
            return writequte()
        else
            logger:fatal("lost redis connectiont!")
            return false
        end
    else
        goods.pinyin = {}
        return true
    end
end
