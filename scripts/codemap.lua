#!/usr/bin/env lua

require 'common'
require 'spec'
local cfg = require 'config'
local logger = require 'logger'
json = require 'json'
redis = require 'redis'

local cli = redis.connect(cfg.redis.host, cfg.redis.port)

cli:select(cfg.redis.cqdb)

if cli == nil then
    logger:error("connect redis error!")
end

local ks = cli:hkeys("codemap:codes")

local codename = {}

for i = 1, #ks do
    k = ks[i]
    vjstr = cli:hget("codemap:codes", k)
    decoded = json.decode(vjstr)
    name = decoded.name
    code = decoded.code
    codename[k] = {name=name, code=code}
end

jstr = json.encode(codename)

local file = io.open("codemap.json", "w")
assert(file)
file:write(jstr)
file:close()

