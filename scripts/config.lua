require "logging"
local config = {
    redis = {
        host = "192.168.8.211",
        port = 6379,
        cqdb = 1,
        kldb = 1,
    },
    block_group = {
        160,    -- 环球指数
        170,    -- 商品期货
        180,    -- 外盘期货
        190,    -- 中国概念
        200,    -- 外汇
        999     -- 非交易或未知分类
    },
    log = {
        level = logging.INFO,
        dir = 'log'
    }
}

return config
