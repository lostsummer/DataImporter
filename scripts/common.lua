
local iconv = require("iconv")

function trans(from,to,text)
    local cd = iconv.new(to .. "//TRANSLIT", from)
    local ostr, err = cd:iconv(text)
    if err == iconv.ERROR_INCOMPLETE then
        return "ERROR: Incomplete input.", false
    elseif err == iconv.ERROR_INVALID then
        return "ERROR: Invalid input.", false
    elseif err == iconv.ERROR_NO_MEMORY then
        return "ERROR: Failed to allocate memory.", false
    elseif err == iconv.ERROR_UNKNOWN then
        return "ERROR: There was an unknown error.", false
    end
        return ostr, true
end

function gbk2utf8(text)
    ostr, success = trans('gbk', 'utf-8', text)
    if success then
        return ostr
    end
end

function in_array(value, list)
    for k,v in ipairs(list) do
        if v == value then
            return true;
        end
    end
    return false;
end 

function getcategory(emcode)
    local n, _ = math.modf(emcode/1000)
    if n == 0 then
        group = 0       -- 上交所:指数
    elseif n >= 9 and n <= 20 then
        group = 1       -- 上交所:国债
    elseif n >= 100 and n <= 113 then
        group = 2       -- 上交所:可转债
    elseif (n >= 120 and n <= 129) or (n >= 132 and n <= 139) then
        group = 3       -- 上交所:公司企业债
    elseif (n == 130) or (n >= 140 and n <= 149) then
        group = 4       -- 上交所:地方政府债
    elseif n == 201 then
        group = 10      -- 上交所:国债回购
    elseif n == 202 then
        group = 11      -- 上交所:企业债回购
    elseif n == 203 then
        group = 12      -- 上交所:国债买断式回购
    elseif n == 204 then
        group = 13      -- 上交所:新质押式国债回购
    elseif n == 205 then
        group = 14      -- 上交所:国信金天利质押式报价回购
    elseif n == 500 or n == 505 then
        group = 20      -- 上交所:封闭式基金
    elseif n == 501 then
        group = 21      -- 上交所:LOF(上市型开放式基金)
    elseif n == 502 then
        group = 22      -- 上交所:分级LOF
    elseif n >= 510 and n <= 518 then
        group = 23      -- 上交所:ETF(交易型开放式指数证券投资基金)
    elseif n == 519 then
        group = 24      -- 上交所:开放式基金申赎
    elseif n >= 600 and n <= 699 then
        group = 30      -- 上交所:A股证券
    elseif n == 900 then
        group = 40      -- 上交所:B股证券
    elseif n == 4000 or n == 4030 or n ==4020 then
        group = 50      -- 上交所:股指期货
    elseif n == 4010 or n == 4310 then
        group = 51      -- 上交所:国债期货
    elseif n >= 110000 and n <= 119999 then
        group = 60      -- 上交所:期权
    elseif n == 1000 or n == 1001 then
        group = 70      -- 深交所:A股主板证券
    elseif n == 1002 then
        group = 71      -- 深交所:A股中小板证券
    elseif n == 1100 or n == 1101 then
        group = 80      -- 深交所:国债
    elseif n == 1106 or n == 1107 or n == 1109 then
        group = 81      -- 深交所:地方政府债
    elseif n == 1108 then
        group = 82      -- 深交所:贴债
    elseif n >= 1111 and n <= 1119 then
        group = 83      -- 深交所:公司企业债
    elseif n >= 1120 and n <= 1129 then
        group = 84      -- 深交所:可转债
    elseif n >= 1130 and n <= 1139 then
        group = 85      -- 深交所:回购
    elseif n >= 1150 and n <= 1169 then
        group = 86      -- 深交所:开放式基金
    elseif n >= 1180 and n <= 1189 then
        group = 87      -- 深交所:封闭式基金
    elseif n >= 1200 and n <= 1299 then
        group = 90      -- 深交所:B股证券
    elseif n == 1300 then
        group = 100     -- 深交所:创业板
    elseif n >= 1390 and n <= 1399 then
        group = 110     -- 深交所:指数
    elseif (n >= 1400 and n <= 1439) or (n >= 1830 and n <= 1839) or (n >= 1870 and n <= 1879) then
        group = 120     -- 深交所:新三板股票
    elseif n == 1899 then
        group = 130     -- 深交所:新三板指数
    elseif n >= 3000 and n <= 3009 then
        group = 140     -- AH股
    elseif n >= 5000 and n <= 5999 then
        group = 150     -- 港股
    elseif n >= 6000 and n <= 6999 then
        group = 160     -- 环球指数
    elseif n >= 7000 and n <= 7999 then
        group = 170     -- 商品期货
    elseif n >= 8000 and n <= 8999 then
        group = 180     -- 外盘期货
    elseif n >= 9000 and n <= 10999 then
        group = 190     -- 中国概念
    elseif n >= 11000 and n <= 11999 then
        group = 200     -- 外汇
    elseif n >=2000 and n<= 2009 then
        group = 210     -- 板块指数
    else
        group = 999     -- 非交易或未知分类
    end

    return group

end

