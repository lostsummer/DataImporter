# 行情数据缓存导入器
—— 基于移动加强版MDS改造的写行情入Redis服务程序

## 历史前身

移动加强版MDS --> 券商加强版MDS --> DataImpoter

移动加强版(r2786)SVN地址:
https://192.168.0.36/svn/MobileServer/linux/MDS

券商加强版(r15854)SVN地址:
https://192.168.0.36/svn/SecurityDept/EMMobileCombine/NetServer/MDS

## [设计目标](http://git.emoney.cn/EMStockData/DataImporter/wikis/difficulties-and-goals)

## [接口说明](http://git.emoney.cn/EMStockData/DataImporter/wikis/home)

## 系统平台及包依赖

OS：

- CentOS6
- CentOS7


### Yum 安装必备软件包

#### 下载，解压工具

- wget
- unzip

#### 运行环境

- lua

#### 编译，构建，调试，开发包

- gcc-c++
- gdb
- make
- lua-devel

#### 安装命令

```shell
yum install -y wget unzip gcc-c++ make gdb lua lua-devel
```

### LuaRocks 安装 Lua 库

#### lua包管理工具

- luarocks

#### lua库

- redis-lua
- json4lua  (json 解析)
- lua-iconv (文本编码转换)
- lualogging

#### 安装命令

```shell
wget https://luarocks.org/releases/luarocks-2.3.0.tar.gz
tar -zxvf luarocks-2.3.0.tar.gz
cd luarocks-2.3.0
./configure --prefix=/usr && make build && make install
echo 'lib_modules_path="/lib64/lua/"..lua_version' >> /etc/luarocks/config-5.1.lua
luarocks install redis-lua
luarocks install json4lua
luarocks install lua-iconv
luarocks install lualogging
```

luarocks安装的json4lua版本有个单引号加转义的bug，港股中有股票名称含有单引号,避免这个问题，手动改一下代码

```shell
vim /usr/share/lua/5.1/json.lua

# 334行注释掉
  -- s = string.gsub(s,"'","\\'")
```

## 构建

###  CentOS7

```shell
make Mode=debug -j4
make Mode=release -j4
输出可执行程序
debug/MDS
release/MDS
```

### CentOS6

需要预先build docker镜像

```shell
cd docker_img
./build.sh
```

编译

```shell
./docker_ct6_compile.sh debug
./docker_ct6_compile.sh release
输出可执行程序目录
debug_ct6/MDS
release_ct6/MDS
```


## 部署目录


#### 辅助程序

- FMDog: map 文件生成工具，项目 https://192.168.0.36/svn/em_sysdev/trunk/DS/L2DS_Linux/FSClient2.6
- FSClient: 静态文件同步工具，项目 https://192.168.0.36/svn/em_sysdev/trunk/DS/L2DS_Linux/FMDog

注意：

FSClient程序测试下来有没有正确关闭连接的bug，运行时间久会大量占用系统文件描述符。
目前的措施是crontab里每天定时启动和关闭FSClient。


可执行程序、配置文件、脚本文件在目标机上的部署位置

- 主目录: /usr/local/EMoney
- MDS.ini: /usr/local/EMoney/MDS/MDS.ini
- MDS: /usr/local/EMoney/MDS/MDS
- scripts: /usr/local/EMoney/MDS/*.lua
- FSClient2.6: /usr/local/EMoney/FSClient/FSClient2.6
- FSClient2.6.ini: /usr/local/EMoney/FSClient/FSClient2.6.ini
- FMDog: /usr/local/EMoney/FMDog/FMDog
- fmdog.conf: /usr/local/EMoney/FMDog/fmdog.conf


## 配置

### MDS.ini

```
[DM]
Address1=180.153.25.248
Port1=38850
[Redis]
SyncCQ=1                      #是否加载码表和实时行情到redis
SyncKM=1                      #是否加载min1 K线到redis
SyncKL=1                      #启动是否加载min30以上跨度K线到redis
NumMin30=60                   #30分K线根数
NumMin60=30                   #60分K线根数
NumDay=20                     #日K线根数
NumWeek=10                    #周K线根数
NumMonth=6                    #月K线根数
[System]
PrivatePath=/usr/local/EMoney/
SharePath=/usr/local/EMoney/
SHF10Path=/usr/local/EMoney/zx/GA/ml45/SYSDATA/HISTORY/SHASE/BASE/BASE/
SZF10Path=/usr/local/EMoney/zx/GA/ml45/SYSDATA/HISTORY/SZNSE/BASE/BASE/
ZXPath=/usr/local/EMoney/zx/GA/ml45/SYSDATA/best/gainfo/
NewsPath=/usr/local/EMoney/zx/GA/ml45/SYSDATA/NEWS/
InitDate=20171025
SaveDate=20171025
NameOfConFM=ConInfo.dat
NameOfFM1=GoodsFM1.dat
NameOfFM2=GoodsFM2.dat
CheckDayDate=0
EnableCheckDay=1
FreeDataDelay=11800
FreeDayData=1
FreeDayData=1
FreeMin5Data=1
FreeMin30Data=1
FreeMinuteData=1
FreeBargainData=1
DataFileSwitch=0
```

### config.lua

```
local config = {
    redis = {                                 -- redis 连接参数
        host = "192.168.42.83",
        port = 6379,
        cqdb = 0,                             -- 码表和实时行情db
        kldb = 0                              -- K线db
    },
    block_group = {                           -- 屏蔽不入redis的分类
        160,                                  -- 环球指数
        170,                                  -- 商品期货
        180,                                  -- 外盘期货
        190,                                  -- 中国概念
        200,                                  -- 外汇
        999,                                  -- 非交易或未知分类
    },
    log = {
        level = logging.ERROR,              -- 日志级别
        dir = "log"                         -- 日志目录
    }
}

return config
```

## 运行控制

### 启动

```
cd /usr/local/EMoney/MDS
./MDS -d
```

### 关闭

```
killall MDS
```

###  重加载

```
killall -HUP MDS
```
